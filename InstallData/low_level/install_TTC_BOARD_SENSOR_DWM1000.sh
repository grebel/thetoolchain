#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a single Low-Level Driver
#  for board devices.
#
#  Created from template _install_TTC_DEVICE_ARCHITECTURE.sh revision 37 at 20181211 13:34:00 UTC
#
#  Supported Architecture: sensor_dwm1000
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$ScriptName - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="sensor_dwm1000"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards "extensions"
findFolderUpwards extensions; DirLL_Extensions="$MyStartDir/$ThisArchitecture/$FoundFolder"
findFolderUpwards scripts;    Start_Path="$MyStartDir/$ThisArchitecture/${FoundFolder}.."

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$ScriptName: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$ScriptName - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for board on sensor_dwm1000
  
    DriverName="${RANK_LOWLEVEL}_board_sensor_dwm1000"
    createExtensionSourcefileHead ${DriverName}  #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile


END_OF_SOURCEFILE
    createExtensionSourcefileTail ${DriverName} #}    
    createExtensionMakefileHead ${DriverName}    #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += board_sensor_dwm1000.o  # ttc-lib/board/board_sensor_dwm1000.c

COMPILE_OPTS += -DTTC_BOARD

# there is only one board so we define it as number 1
COMPILE_OPTS += -DTTC_BOARD1=E_ttc_board_architecture_sensor_dwm1000 # Line generated automatically. Remove if not wanted!

# Activate microcontroller being placed on your board.
# This list has been extracted from all ttc-lib/cpu/cpu_*_types.h files.
# Adding a new cpu requires to add an entry to one of these files.

# This list of CPU-Variants has been extracted from ttc_cpu_types.h by ./createNewProject.pl on Sa 2. Jan 03:33:52 CET 2016
# Activate exactly one of these lines:
#  TTC_CPU_VARIANT_stm32f051r8t6   = 1
#  TTC_CPU_VARIANT_stm32f100rbt6   = 1
#  TTC_CPU_VARIANT_stm32f103r6t6a  = 1
#  TTC_CPU_VARIANT_stm32f103r8t6   = 1
#  TTC_CPU_VARIANT_stm32f103rbt6   = 1
#  TTC_CPU_VARIANT_stm32f103rdy6tr = 1
#  TTC_CPU_VARIANT_stm32f103zet6   = 1
#  TTC_CPU_VARIANT_stm32f105rct6   = 1
#  TTC_CPU_VARIANT_stm32f107vct6   = 1
#  TTC_CPU_VARIANT_stm32f302r8t6   = 1
#  TTC_CPU_VARIANT_stm32l053r8t6   = 1
TTC_CPU_VARIANT_stm32l100rbt6   = 1
#  TTC_CPU_VARIANT_stm32l100rct6   = 1
#  TTC_CPU_VARIANT_stm32l152rbt6   = 1
#  TTC_CPU_VARIANT_stm32w108ccu    = 1
#InsertCpuVariants above

# define frequency of external high-speed crystal (mandatory if installed)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=8000000


# Define all internal devices which are usable on your current printed circuit board (PCB).
# To find correct gpio pin for a certain device, check schematic of your PCB.
#
# Note: All GPIO pin definitions have to use TTC_GPIO_<BANK><Number> names.
#       E.g.: GPIO bank A pin number 3 -> TTC_GPIO_A3
#       See enumeration ttc_gpio_pin_e for a complete list of valid pin names.
#

COMPILE_OPTS += -DBOARD_SENSOR_DWM1000_CAN_STBY=TTC_GPIO_A1 # =1: CAN Transceiver disabled; =0: CAN Transceiver is enabled for TTC_USART1
COMPILE_OPTS += -DTTC_RTC_CLOCK_SOURCE_LSE

#{ RADIO1
COMPILE_OPTS += -DTTC_RADIO1_INDEX_SPI=1                   # using TTC_SPI1 to communicate with DW1000
COMPILE_OPTS += -DTTC_RADIO1_INDEX_SPI_NSS=1               # using TTC_SPI1_NSS1 as SPI Slave Select
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_EXT_ON=TTC_GPIO_C4 # GPIO1
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_RESET=TTC_GPIO_A4  # GPIO pin connected to /reset pin of DW1000
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_SPIPHA=TTC_GPIO_C9 # GPIO pin connected to SPIPHASE pin of DW1000
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_SPIPOL=TTC_GPIO_C6 # (GPIO3)     GPIO pin connected to SPIPOL pin of DW1000
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_IRQ=TTC_GPIO_A2    # (IRQ1)      GPIO pin connected to IRQ output pin of DW1000
COMPILE_OPTS += -DTTC_RADIO1_DW1000_TX_POWER=0x15355575    # transmitter power level (yet uncalibrated)

#}
#{ USB interface
COMPILE_OPTS += -DTTC_USB_ENABLE=TTC_GPIO_A8
COMPILE_OPTS += -DTTC_USB_DP=TTC_GPIO_A12
COMPILE_OPTS += -DTTC_USB_DM=TTC_GPIO_A11
#}
#{ LEDs
COMPILE_OPTS += -DTTC_LED1=TTC_GPIO_B12         # GPIO pin connected to LED #1
COMPILE_OPTS += -DTTC_LED1_LOWACTIVE=0     # ==1: gpio pin has to output 0 to activate LED; ==0: gpio pin has to output 1 to activate LED
#}LEDs
#{ Switches (input with pull up resistor)

# COMPILE_OPTS += -DTTC_SWITCH1=             # GPIO pin connected to press button switch #1
# COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=   # ==1: gpio pin reads 1 if button is pressed; ==0: gpio pin reads 0 if button is pressed
# COMPILE_OPTS += -DTTC_SWITCH2=             # GPIO pin connected to press button switch #1
# COMPILE_OPTS += -DTTC_SWITCH2_LOWACTIVE=   # ==1: gpio pin reads 1 if button is pressed; ==0: gpio pin reads 0 if button is pressed

#}
#{ USART1 - define serial port #1
  COMPILE_OPTS += -DTTC_USART1=ttc_device_1 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
  COMPILE_OPTS += -DTTC_USART1_TX=TTC_GPIO_A9    # GPIO pin used to send out serial sata
  COMPILE_OPTS += -DTTC_USART1_RX=TTC_GPIO_A10   # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART1_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART1_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART1
#{ USART2 - define serial port #2
#  COMPILE_OPTS += -DTTC_USART2=ttc_device_2 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_USART2_TX=          # GPIO pin used to send out serial sata via CAN Transceiver
#  COMPILE_OPTS += -DTTC_USART2_RX=          # GPIO pin used to read in serial sata via CAN Transceiver
#  COMPILE_OPTS += -DTTC_USART2_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART2_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART2
#{ USART3 - define serial port #3
#  COMPILE_OPTS += -DTTC_USART3=ttc_device_3 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_USART3_TX=          # GPIO pin used to send out serial sata
#  COMPILE_OPTS += -DTTC_USART3_RX=          # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART3_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART3_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART3
#{ USART4 - define serial port #4
#  COMPILE_OPTS += -DTTC_USART4=ttc_device_4 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_USART4_TX=          # GPIO pin used to send out serial sata
#  COMPILE_OPTS += -DTTC_USART4_RX=          # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART4_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART4_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART4
#{ USART5 - define serial port #5
#  COMPILE_OPTS += -DTTC_USART5=ttc_device_5 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_USART5_TX=          # GPIO pin used to send out serial sata
#  COMPILE_OPTS += -DTTC_USART5_RX=          # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART5_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART5_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART5
#{ SPI1 - define serial peripheral interface port #1
  COMPILE_OPTS += -DTTC_SPI1=ttc_device_1   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
  COMPILE_OPTS += -DTTC_SPI1_MOSI=TTC_GPIO_A7         # GPIO-Pin used for Master Out Slave In
  COMPILE_OPTS += -DTTC_SPI1_MISO=TTC_GPIO_A6         # GPIO-Pin used for Master In Slave Out
  COMPILE_OPTS += -DTTC_SPI1_SCK=TTC_GPIO_A5          # GPIO-Pin used for Serial Clock 
  COMPILE_OPTS += -DTTC_SPI1_NSS1=TTC_GPIO_B1         # GPIO-Pin used for Slave Select
#}SPI1
#{ SPI2 - define serial peripheral interface port #2
#  COMPILE_OPTS += -DTTC_SPI2=ttc_device_2   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_SPI2_MOSI=          # GPIO-Pin used for Master Out Slave In
#  COMPILE_OPTS += -DTTC_SPI2_MISO=          # GPIO-Pin used for Master In Slave Out
#  COMPILE_OPTS += -DTTC_SPI2_SCK=           # GPIO-Pin used for Serial Clock 
#  COMPILE_OPTS += -DTTC_SPI2_NSS=           # GPIO-Pin used for Slave Select
#}SPI2
#{ SPI3 - define serial peripheral interface port #3
#  COMPILE_OPTS += -DTTC_SPI3=ttc_device_3   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_SPI3_MOSI=          # GPIO-Pin used for Master Out Slave In
#  COMPILE_OPTS += -DTTC_SPI3_MISO=          # GPIO-Pin used for Master In Slave Out
#  COMPILE_OPTS += -DTTC_SPI3_SCK=           # GPIO-Pin used for Serial Clock 
#  COMPILE_OPTS += -DTTC_SPI3_NSS=           # GPIO-Pin used for Slave Select
#}SPI3
#{ I2C1 - define inter-integrated circuit interface #1
#  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_I2C1_SDA=           # GPIO-Pin used for SPI Serial Data
#  COMPILE_OPTS += -DTTC_I2C1_SCL=           # GPIO-Pin used for SPI Serial Clock
#  COMPILE_OPTS += -DTTC_I2C1_SMBAL=         # GPIO-Pin used for SPI SMBus Alert (if configured to use SMBus mode)
#}I2C1
#{ I2C2 - define inter-integrated circuit interface #2
#  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_I2C2_SDA=           # GPIO-Pin used for SPI Serial Data
#  COMPILE_OPTS += -DTTC_I2C2_SCL=           # GPIO-Pin used for SPI Serial Clock
#  COMPILE_OPTS += -DTTC_I2C2_SMBAL=         # GPIO-Pin used for SPI SMBus Alert (if configured to use SMBus mode)
#}I2C2
#{ I2C3 - define inter-integrated circuit interface #3
#  COMPILE_OPTS += -DTTC_I2C3=ttc_device_3   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_I2C3_SDA=           # GPIO-Pin used for SPI Serial Data
#  COMPILE_OPTS += -DTTC_I2C3_SCL=           # GPIO-Pin used for SPI Serial Clock
#  COMPILE_OPTS += -DTTC_I2C3_SMBAL=         # GPIO-Pin used for SPI SMBus Alert (if configured to use SMBus mode)
#}I2C2

# I2S- (Inter-IC Sound-) Interface              ToDo: define constant names

# CAN- (Controller Area Network-) Bus Interface ToDo: define constant names


END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$ScriptName 2" "wireless sensor node combining stm32l100 + decawave dwm1000 ranging ultrawideband transceiver" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "${RANK_LOWLEVEL}_board_sensor_dwm1000" "$cASH_InfoText"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  # board_sensor_dwm1000_prepare() typically ist called ttc_board_prepare() during preparation of all extensions.
  # It should not be required to activate source.${RANK_LOWLEVEL}_*.c here.

  #X if [ -e \$Dir_Extensions/source.${DriverName}.c ]; then
  #X   createLink \$Dir_Extensions/source.${DriverName}.c \$Dir_ExtensionsActive/ '' QUIET
  #X fi

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers activate only if a certain file exists inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.${RANK_LOWLEVEL}_ethernet_ste101p -> allows activate.500_ethernet.sh to enable its low-level driver ${RANK_LOWLEVEL}_ethernet_ste101p.
  # enableFeature NAME  # creates extensions.active/feature.NAME
  # See activate_project.sh of a fresh created project for a list of all available features! 
  
  enableFeature 450_math_software_float                     # Software Floating Point Library
  enableFeature 450_radio_dw1000                            # Ultra wideband radio transceiver DW1000
  enableFeature 450_packet_802154                           # network packets of type IEEE 802.15.4
  enableFeature 450_cpu_cortexm3                            # Common driver for all microcontrollers with CortexM3 core
  enableFeature 450_cpu_stm32l1xx                           # Driver for STM32L1xx microcontrollers
  enableFeature 450_interrupt_cortexm3                      # interrupt handling for all microcontrollers with CortexM3 core
  enableFeature 450_interrupt_stm32f1xx                     # interrupt handling for STM32F1xx family of microcontrollers

  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  #}
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$ScriptName 2" "PLACE YOUR INFO HERE FOR $ScriptName" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  if [ -e \$Dir_Extensions/source.${Name}.c ]; then
    createLink \$Dir_Extensions/source.${Name}.c \$Dir_ExtensionsActive/ '' QUIET
  fi

#}
#{ ACTIVATE_SECTION_E provide default features required to compile this example

  # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.  

  # enableFeature 450_math_software_float   # enable math driver (single precision)
  # enableFeature 450_math_software_double  # enable math driver (double precision)

#}
#{ ACTIVATE_SECTION_F call other activate-scripts

#activate.500_ttc_gpio.sh QUIET "\$ScriptName"

#}
#{ ACTIVATE_SECTION_G copy source code into project folder if required

if [ ! -d regressions ]; then
  mkdir regressions
fi
ExampleFound=\`ls regressions/regression_${EXTENSION_NAME}_${ThisArchitecture}*\`
if [ "\$ExampleFound" == "" ]; then
  echo "copying missing regression_${EXTENSION_NAME}_${ThisArchitecture}*.."
  cp -v ~/Source/TheToolChain/Template/regressions/regression_${EXTENSION_NAME}_${ThisArchitecture}* regressions/
fi
#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "1" ]; then #{ recommended: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}_${ThisArchitecture}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}_${ThisArchitecture}.h"
example_${EXTENSION_NAME}_${ThisArchitecture}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}_${ThisArchitecture}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$ScriptName 2" "Usage example for extension ${EXTENSION_NAME}_${ThisArchitecture}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/source.${Name}.c ]; then
  createLink \$Dir_Extensions/source.${Name}.c \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#{ ACTIVATE_SECTION_E provide default features required to compile this example

  # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.
  enableFeature $DriverName  # enable low-level driver to be tested

  enableFeature 450_math_software_float     # Software Floating Point Library
  enableFeature 450_packet_802154           # network packets of type IEEE 802.15.4

#}
#{ ACTIVATE_SECTION_F call other activate-scripts

  activate.${Install_Dir}.sh QUIET "\$ScriptName" # activate high-level driver
  activate.500_ttc_math.sh          QUIET "$0"
  activate.500_ttc_packet.sh        QUIET "$0"
  activate.500_ttc_spi.sh           QUIET "$0"
  activate.500_ttc_register.sh      QUIET "$0"
  activate.500_ttc_states.sh        QUIET "$0"

#}
#{ ACTIVATE_SECTION_G copy source code into project folder if required

if [ ! -d examples ]; then
  mkdir examples
fi
ExampleFound=\`ls examples/example_${EXTENSION_NAME}_${ThisArchitecture}*\`
if [ "\$ExampleFound" == "" ]; then
  echo "copying missing example_${EXTENSION_NAME}.."
  cp -v ~/Source/TheToolChain/Template/examples/example_${EXTENSION_NAME}_${ThisArchitecture}* examples/
fi
#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                               #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
