#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a single Low-Level Driver
#  for cpu devices.
#
#  Created from template _install_NN_TTC_DEVICE_ARCHITECTURE.sh revision 27 at 20150806 10:29:19 UTC
#
#  Supported Architecture: stm32w1xx
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$0 - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="stm32w1xx"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards extensions; DirLL_Extensions="$MyStartDir/$ThisArchitecture/$FoundFolder"
findFolderUpwards scripts;    Start_Path="$MyStartDir/$ThisArchitecture/${FoundFolder}.."

Rank_CPU="200" # rank of cpu STM32W1xx
Rank_MAC="251" # rank of SimpleMAC library for STM32W1xx
Rank_HAL="253" # rank of Hardware Abstraction Layer library for STM32W1xx

# High-Level extension names (low-level extensions will add similar extension names showing a version number)
DriverName_MAC="${Rank_MAC}_stm32w1xx_simple_mac"
DriverName_HAL="${Rank_HAL}_stm32w1xx_hal"

LinkFirmware="SimpleMac-Firmware"
LinkLibrary="SimpleMAC_110"
DriverName_CPU="${Rank_CPU}_cpu_stm32w1xx"
DriverName_MAC="${Rank_MAC}_stm32w1xx_simple_mac110"
DriverName_HAL="${Rank_HAL}_stm32w1xx_hal110"

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  #getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/        CD00262415.pdf UM0894-STM32W-SK_and_STM32W-EXT_starter_and_extension_kits.pdf                  uC/STM32W1xx/
  #Not available anymore: getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00271698.pdf AN3211-Using_the_simulated_EEPROM_with_the_STM32W108_platform.pdf               uC/STM32W1xx/
  #Not available anymore: getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00269334.pdf AN3188-Preparing_custom_devices_for_the_STM32W108_platform.pdf                  uC/STM32W1xx/

  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/PROGRAMMING_MANUAL/ CD00280769.pdf PM0073-STM32W1xx_Programming_Manual.pdf                                         uC/STM32W1xx/
  getDocumentation http://www.st.com/web/en/resource/technical/document/user_manual/                           CD00262415.pdf UM0894-STM32W-SK_and_STM32W-EXT_starter_and_extension_kits.pdf                  uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/        CD00280375.pdf UM0978-Using_the_Simple_MAC_nodetest_application.pdf                            uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/        CD00264812.pdf UM0909-STM32W108xx_ZigBee_RF4CE_library.pdf                                     uC/STM32W1xx/
  getDocumentation http://www.po-star.com/public/uploads/                                              20120227121858_397.pdf AN3211-Using_the_simulated_EEPROM_with_the_STM32W108_platform.pdf               uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00282958.pdf AN3262-Using_the_over-the-air_bootloader_with_STM32W108_devices.pdf             uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00270908.pdf AN3206-PCB_design_guidelines_for_the_STM32W108_platform.                        uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   DM00024648.pdf AN3359-Low_cost_PCB_antenna_for_2.4GHz_radio_Meander_design.pdf                 uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00004479.pdf AN1709-EMC_design_guide_for_ST_microcontrollers.pdf                             uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/APPLICATION_NOTE/   CD00274443.pdf AN3218-Adjacent_channel_rejection_measurements_for_the_STM32W108_platform.pdf   uC/STM32W1xx/
  getDocumentation http://images.icbuy.com/tec/STM2011/STM32W_Docs/                                            CD00269334.pdf AN3188-Preparing_custom_devices_for_the_STM32W108_platform.pdf                  uC/STM32W1xx/                                            
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/DATA_BRIEF/         CD00237927.pdf DB0837-High-performance_802.15.4_wireless_system-on-chip_databrief.pdf          uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/DATASHEET/          CD00248316.pdf DS6473-High-performance_IEEE_802.15.4_wireless_system-on-chip_Datasheet.pdf     uC/STM32W1xx/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/       CD00262339.pdf UM0893-STM32W108xx_SimpleMAC_library.pdf                                        uC/STM32W1xx/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/       DM00065100.pdf UM1576-Description_of_STM32W108xx_Standard_Peripheral_Library.pdf               uC/STM32W1xx/
  getDocumentation http://www.st.com/internet/com/SALES_AND_MARKETING_RESOURCES/MARKETING_PRESENTATIONS/PRODUCT_PRESENTATION/ stm32w_marketing_pres.pdf  STM32W-Product_presentation.pdf                      uC/STM32W1xx/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/release_note/      CD00262343.pdf RN0046_SimpleMAC_library_for_STM32W108xx_kits.pdf                               uC/STM32W1xx/
  getDocumentation http://www.st.com/web/en/resource/technical/document/application_note/                      DM00060426.pdf AN4142-STM32W108xx_datasheet_bit_and_register_naming_migration.pdf              uC/STM32W1xx/
  getDocumentation http://www.st.com/web/en/resource/technical/document/errata_sheet/                          DM00032130.pdf STM32W108xx-Errata_sheet-STM32W108xx_device_limitations                         uC/STM32W1xx/
  getDocumentation http://www.st.com/web/en/resource/technical/document/application_note/                      DM00059814.pdf AN4141-Migration_and_compatibility_guidelines_for_STM32W108xx_microcontroller_applications_based_on_the_HAL.pdf uC/STM32W1xx/

  getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    EM35x.pdf       Ember_Manual_EM35x_rev13.pdf                                                    uC/STM32W1xx/
  getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    UG103.pdf       Ember_UG103.0_Application_Development_Fundamentals.pdf                          uC/STM32W1xx/
	getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN698.pdf       Ember_AN698_PCB_Design_with_an_EM35x.pdf                                        uC/STM32W1xx/
	getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN703.pdf       Ember_AN703_Using_the_Simulated_EEPROM.pdf                                      uC/STM32W1xx/
	getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN710.pdf       Ember_AN710_Bringing_up_Custom_Devices_for_the_EM35x_SoC_Platform.pdf           uC/STM32W1xx/
	getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN713.pdf       Ember_AN713_Measuring_EM35x_Power_Consumption.pdf                               uC/STM32W1xx/
	getDocumentation http://www.silabs.com/Support%20Documents/TechnicalDocs/                                    AN715.pdf       Ember_AN715_Using_the_EM35x_ADC.pdf                                             uC/STM32W1xx/
	getDocumentation http://www.telegesis.com/downloads/general/                                     tg-etrx35x-pm-010-107.pdf   Telegesis_Product_Manual_ETRX35x_ZigBee_Module.pdf                              uC/STM32W1xx/
	getDocumentation http://www.telegesis.com/downloads/general/                      TG-PM-0505-CI-AT-Command-Manual%20r1.pdf   Telegesis_AT_Command_Set_for_Combined_Interface.pdf                             uC/STM32W1xx/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`
    Path2Library_CPU="$Install_Path/SimpleMac-Firmware"
    Path2Library_MAC="$Install_Path/SimpleMac-Library"
    Path2Library_HAL="$Install_Path/hal_library"
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    if [ ! -d "$LinkLibrary" ]; then
      getFile http://www.thetoolchain.com/mirror/ ${LinkLibrary}.tgj ${LinkLibrary}.tgj
      untgj "${LinkLibrary}" ${LinkLibrary}.tgj
    fi
    if [ ! -d "${LinkLibrary}" ]; then
      echo "$0 - ERROR: Could not download file ${LinkLibrary}.tgj (missing directory ${LinkLibrary})"
      exit 12
    fi
    createLink SimpleMAC_110/STM32W108/simplemac/      ${Path2Library_MAC}
    createLink SimpleMAC_110/STM32W108/hal/            ${Path2Library_HAL}
    createLink SimpleMAC_110/STM32W108/                $LinkFirmware
    addDocumentationFolder SimpleMAC_110/STM32W108/docs/hal/html   HAL-Library_110        uC STM32W1xx
    addDocumentationFolder SimpleMAC_110/STM32W108/docs/simplemac  SimpleMAC-Library_110  uC STM32W1xx
  
    if [ "1" == "1" ]; then #{ unpack simple mac library
      PrevDir=`pwd`
      cd "$Path2Library_MAC/library"
      mkdir gnu
      cd gnu
      ar x ../libsimplemac-library-gnu.a
      cd "$PrevDir"
    fi #}
    function moveDuplicateFile() { # renames given file to a unique filename
      BaseDir="$1"      # no trailing slash!
      SubDir="$2"       # no trailing/ leading slash!
      OldFileName="$3"
      NewFileName="$4"
      
      OldFile="$BaseDir/$SubDir/$OldFileName"
      NewFile="$BaseDir/$NewFileName"
      
      if [ -e "$OldFile" ]; then #{
        echo "Renaming file with duplicate filename '${OldFileName}' -> '${NewFileName}':" 
        echo -n "    "
        mv -v "$OldFile" "$NewFile"
        PatchFiles=`find ./ -type f -name *.[ch] -exec grep -l $SubDir/$OldFileName {} \;`
        #echo "    PatchFiles='find `pwd` -type f -name *.[ch] -exec grep -l $SubDir/$OldFileName {} \;'"; echo " = '$PatchFiles'" #D 
        for PatchFile in $PatchFiles; do #{ replace all "stm32w108/${Name}.h" -> "${Name}_stm32w108.h"
          if [ ! -e $PatchFile ]; then
            ERROR="1"
            echo "ERROR: File not found: '$PatchFile'!"
          else
            echo "    patching file '$PatchFile': '$SubDir/$OldFileName' -> '$NewFileName'"
            replaceInFile "$PatchFile" "$SubDir/$OldFileName" "$NewFileName"
          fi
        done #}
      else
        if [ ! -e "$NewFile" ]; then
          echo "$0 - ERROR: moveDuplicateFile($BaseDir, $SubDir, $OldFileName, $NewFileName)"
          echo "    Cannot find file '$OldFile'!"
          echo "    moveDuplicateFile() was called from line `caller`"
          exit 31
        else
          echo "File already moved -> '$NewFile'"
        fi
      fi #}
    }
    
    # Files with duplicate filenames are a NOGO in TTC world!
    moveDuplicateFile ${Path2Library_HAL}/micro          cortexm3  adc.h          adc_cortexm3.h
    moveDuplicateFile ${Path2Library_HAL}/micro          cortexm3  micro-common.c micro-common_cortexm3.c
    moveDuplicateFile ${Path2Library_HAL}/micro          cortexm3  micro-common.h micro-common_cortexm3.h
    moveDuplicateFile ${Path2Library_HAL}/micro/cortexm3 stm32w108 memmap.h       memmap_stm32w108.h
    
    PatchFile=`find ./ -name adc.c`
    replaceInFile "$PatchFile"   '"hal/micro/adc.h"' '"adc.h" //TTC fixed include'

    PatchFile=`find ./ -name micro.c`
    replaceInFile "$PatchFile"  "#pragma pack"           "// TheToolChain (causes compiler warning) #pragma pack"
    replaceInFile "$PatchFile"  "#pragma pack"           "// TheToolChain (causes compiler warning) #pragma pack"
    replaceInFile "$PatchFile"  "halInternalBlockUntilXtal();" "#ifndef DISABLE_IDLING_DELAY   //TTC\nhalInternalBlockUntilXtal();\n#endif //TTC\n"
    #{
    #{
    replaceInFile "$PatchFile"  "\} appSwitchStructType;" "\} __attribute__((__packed__)) appSwitchStructType; // TheToolChain GCC compliant packed structure"
    
    if [ "$MissingFiles" == "" ]; then #{
      touch OK.Install
    else
      echo "$0 - ERROR: missing files: $MissingFiles"
    fi #}
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for cpu on stm32w1xx
  
    DriverName="450_cpu_stm32w1xx"
    createExtensionSourcefileHead ${DriverName}   #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile


END_OF_SOURCEFILE
    createExtensionSourcefileTail ${DriverName} #}    
    createExtensionMakefileHead ${DriverName}     #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += cpu_stm32w1xx.o # low-level driver: ttc-lib/cpu/cpu_stm32w1xx.c

# All STM32W1xx have same memory start addresses
COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_START=0x20000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_ACCESS=rxw
COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_START=0x08000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_ACCESS=rx
COMPILE_OPTS += -DTTC_MEMORY_REGION_FIB_START=0x08040000
COMPILE_OPTS += -DTTC_MEMORY_REGION_FIB_ACCESS=rw
COMPILE_OPTS += -DTTC_MEMORY_REGION_FIB_SIZEK=2
COMPILE_OPTS += -DTTC_MEMORY_REGION_CIB_START=0x08040800
COMPILE_OPTS += -DTTC_MEMORY_REGION_CIB_ACCESS=rw
COMPILE_OPTS += -DTTC_MEMORY_REGION_CIB_SIZEK=2

#{ Define memory regions for each CPU Variant here to allow automatic linker script generation
# Note: This information is taken from cpu_stm32l1xx_types.h and must be kept in sync!

ifdef TTC_CPU_VARIANT_stm32w108ccu
  COMPILE_OPTS += -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32w108ccu
  COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_SIZEK=12
  COMPILE_OPTS += -DTTC_MEMORY_REGION_ROM_SIZEK=192
endif

#}


# some compiled libraries (e.g. ST SimpleMAC for STM32W) use 2 byte wchars
#? COMPILE_OPTS += -fshort-wchar

# Setting CPU define is required by gnu.h (SimpleMAC v1.1.0)
COMPILE_OPTS += -DCORTEXM3_STM32W108

EM357_SIMPLEMAC = 1
INCLUDE_DIRS += -I additionals/$DriverName_MAC/include/
#(Missing in v1.10)    INCLUDE_DIRS += -I additionals/${DriverName_CPU}/$IncludeDir_CPU

# SimpleMAC library is only available as precompiled binary :-(
LD_LIBS += -mthumb additionals/$DriverName_MAC/library/libsimplemac-library-gnu.a

#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/phy-common.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/phy.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/aes.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/analogue-filter.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/analogue-mod-dac.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/analogue-vco.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/eui64.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/phy-library.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/phy-util.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/analogue-cell-bias.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/analogue-lna.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/analogue.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/byte-utilities.o
#MAIN_OBJS += additionals/$DriverName_MAC/library/gnu/weak_defs.o

# SimpleMAC Library <=v2.0.0
#X (We already have our own linker script!) LDFLAGS+= -Tadditionals/$DriverName_HAL/micro/cortexm3/stm32w108/gnu-stm32w108.ld

# define rules for assembler-files which may be compiled into objects
context-switch.o: context-switch.s
context-switch_gnu.o: context-switch_gnu.s
MAIN_OBJS += context-switch.o

# Special Purpose Mask Registers manipulation routines
spmr.o: spmr.s
MAIN_OBJS += spmr.o

MAIN_OBJS += crt_stm32w108.o

# Setting this switch is a workaround for endless loop in halCommonSwitchToXtal() on ETRX357 boards
#? COMPILE_OPTS += -DDISABLE_IDLING_DELAY

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "low-level cpu driver for stm32w1xx architecture" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "$DriverName" "Driver for STM32W1xx microcontrollers"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
    createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
  fi

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers require a certain file to exist inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.450_ethernet_ste101p -> allows activate.500_ethernet.sh to enable 450_ethernet_ste101p.
  # enableFeature NAME  # creates extensions.active/feature.NAME 
  
  # this is also a Cortex M3 CPU
  enableFeature 450_cpu_cortexm3
  enableFeature 450_cpu_stm32w1xx
  activate.450_cpu_cortexm3.sh "\$0" QUIET

  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  # activate.500_ttc_memory.sh QUIET "\$0"
  
  #}
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#  ACTIVATE_SECTION_E regressions do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

#activate.500_ttc_gpio.sh QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ optional: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#  ACTIVATE_SECTION_E examples do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  findFolderUpwards 999_open_ocd; Folder_OpenOCD="$FoundFolder"
  ConfigFile="$Folder_OpenOCD/share/openocd/scripts/target/em357_jtag.cfg"
  if [ ! -e "$ConfigFile" ]; then #{ create OpenOCD target configuration for OnChip Debugging
    #if [ ! -e "$ConfigFile" ]; then
      echo "creating cfg-file '`pwd`/$ConfigFile'.."
      cat <<END_OF_CONFIG >$ConfigFile #{
# http://TheToolchain.com
#
# OpenOCD target definition stm32w108 (former EM357)
# created by $0

if { [info exists CHIPNAME] } {
   set  _CHIPNAME \$CHIPNAME
} else {
   set  _CHIPNAME em357
}

if { [info exists ENDIAN] } {
   set  _ENDIAN \$ENDIAN
} else {
   set  _ENDIAN little
}

# Work-area is a space in RAM used for flash programming
# By default use 16kB
if { [info exists WORKAREASIZE] } {
   set  _WORKAREASIZE \$WORKAREASIZE
} else {
   #set  _WORKAREASIZE 0x4000
   set  _WORKAREASIZE 0x2000
}

# JTAG speed should be <= F_CPU/6. F_CPU after reset is 8MHz, so use F_JTAG = 1MHz
adapter_khz 200

#adapter_nsrst_delay 400
jtag_ntrst_delay 100

#verify_ircapture disable 
#verify_jtag disable


#jtag scan chain
if { [info exists CPUTAPID ] } {
   set _CPUTAPID \$CPUTAPID
} else {
  # See STM Document RM0008
  # Section 26.6.3
   set _CPUTAPID 0x3ba00477
}
jtag newtap \$_CHIPNAME cpu -irlen 4 -ircapture 0x1 -irmask 0xf -expected-id \$_CPUTAPID

if { [info exists BSTAPID ] } {
   # FIXME this never gets used to override defaults...
   set _BSTAPID \$BSTAPID
} else {
  # See STM Document RM0008
  # Section 29.6.2
  # Low density devices, Rev A
  #set _BSTAPID1 0x06412041
  # Medium density devices, Rev A
  #set _BSTAPID2 0x06410041
  # Medium density devices, Rev B and Rev Z
  #set _BSTAPID3 0x16410041
  #set _BSTAPID4 0x06420041
  # High density devices, Rev A
  #set _BSTAPID5 0x06414041
  #set _BSTAPID5 0x06418041
  # Connectivity line devices, Rev A and Rev Z
  #set _BSTAPID6 0x06418041
  # XL line devices, Rev A
  #set _BSTAPID7 0x06430041
  
  
  # set tap ID for stm32w1xx.bs
  # set _BSTAPID 0x269a862b
  
  # set tap ID for em357.bs in Telegesis ETRX357 radio module
  set _BSTAPID 0x069a962b
}

#jtag newtap \$_CHIPNAME bs -irlen 4 -ircapture 0x0e -irmask 0xf  -expected-id \$_BSTAPID
jtag newtap \$_CHIPNAME bs -irlen 4 -ircapture 0x01 -irmask 0xf  -expected-id \$_BSTAPID

	#	jtag newtap \$_CHIPNAME bs -irlen 5 -expected-id \$_BSTAPID1 \
#	-expected-id \$_BSTAPID2 -expected-id \$_BSTAPID3 \
#	-expected-id \$_BSTAPID4 -expected-id \$_BSTAPID5 \
#	-expected-id \$_BSTAPID6 -expected-id \$_BSTAPID7



set _TARGETNAME \$_CHIPNAME.cpu
#\$_TARGETNAME configure -work-area-phys 0x20000000 -work-area-size \$_WORKAREASIZE -work-area-backup 0

target create \$_TARGETNAME cortex_m -endian \$_ENDIAN -chain-position \$_TARGETNAME

\$_TARGETNAME configure -work-area-phys 0x20000000 -work-area-size \$_WORKAREASIZE -work-area-backup 0

# flash size will be probed
set _FLASHNAME \$_CHIPNAME.flash
#flash bank \$_FLASHNAME stm32f1x 0x08000000 0 0 0 \$_TARGETNAME
flash bank \$_FLASHNAME em357 0x08000000 0x00040000 0 0 \$_TARGETNAME

# if srst is not fitted use SYSRESETREQ to
# perform a soft reset
cortex_m reset_config sysresetreq

END_OF_CONFIG
#}
  fi #}

  ConfigFile="$Folder_OpenOCD/share/openocd/scripts/target/em357_flash.cfg"
  if [ ! -e "$ConfigFile" ]; then #{ create OpenOCD configuration to flash STM32W1xx
    echo "creating cfg-file '`pwd`/$ConfigFile'.."
    cat <<END_OF_CONFIG >$ConfigFile #{
# http://TheToolchain.com
#
# OpenOCD flash script for  stm32w108 (former EM357)
# created by $0


# finish configuration stage + proceed to run stage
init

# hardware reset processor with halt (2 times is safer than only once)
reset
soft_reset_halt

# check target state stm32w
poll

# list all found flash banks
flash banks

# identify the flash
flash probe 0

em357 unlock 0

# erasing all flash
em357 mass_erase 0

# write file to flash number 0
flash write_bank 0 main.bin 0

# hardware processor reset with run
reset run

#shutdown openocd daemon
shutdown

END_OF_CONFIG
#}
  fi #}

    ConfigFile="$Folder_OpenOCD/share/openocd/scripts/target/stm32w1xx_jtag.cfg"
    echo "creating cfg-file '`pwd`/$ConfigFile'.."
    cat <<END_OF_CONFIG >$ConfigFile #{
# http://TheToolchain.com
#
# OpenOCD target definition stm32w108 (former EM357)
# created by $0

if { [info exists CHIPNAME] } {
 set  _CHIPNAME \$CHIPNAME
} else {
 set  _CHIPNAME em357
}

if { [info exists ENDIAN] } {
 set  _ENDIAN \$ENDIAN
} else {
 set  _ENDIAN little
}

# Work-area is a space in RAM used for flash programming
# By default use 16kB
if { [info exists WORKAREASIZE] } {
 set  _WORKAREASIZE \$WORKAREASIZE
} else {
 #set  _WORKAREASIZE 0x4000
 set  _WORKAREASIZE 0x2000
}
set  _WORKAREASIZE 0x1000

# JTAG speed should be <= F_CPU/6. F_CPU after reset is 8MHz, so use F_JTAG = 1MHz
adapter_khz 200

#adapter_nsrst_delay 400
jtag_ntrst_delay 100

#verify_ircapture disable 
#verify_jtag disable


#jtag scan chain
if { [info exists CPUTAPID ] } {
 set _CPUTAPID \$CPUTAPID
} else {
# See STM Document RM0008
# Section 26.6.3
 set _CPUTAPID 0x3ba00477
}
jtag newtap \$_CHIPNAME cpu -irlen 4 -ircapture 0x1 -irmask 0xf -expected-id \$_CPUTAPID

if { [info exists BSTAPID ] } {
 # FIXME this never gets used to override defaults...
 set _BSTAPID \$BSTAPID
} else {
# See STM Document RM0008
# Section 29.6.2
# Low density devices, Rev A
#set _BSTAPID1 0x06412041
# Medium density devices, Rev A
#set _BSTAPID2 0x06410041
# Medium density devices, Rev B and Rev Z
#set _BSTAPID3 0x16410041
#set _BSTAPID4 0x06420041
# High density devices, Rev A
#set _BSTAPID5 0x06414041
#set _BSTAPID5 0x06418041
# Connectivity line devices, Rev A and Rev Z
#set _BSTAPID6 0x06418041
# XL line devices, Rev A
#set _BSTAPID7 0x06430041


set _BSTAPID1 0x269a862b
# set tap ID for stm32w.bs in Telegesis ETRX357 radio module
set _BSTAPID 0x269a862b

# set tap ID for em357.bs in Telegesis ETRX357 radio module
set _BSTAPID2 0x069a962b
}

jtag newtap \$_CHIPNAME bs -irlen 4 -ircapture 0x0e -irmask 0xf  -expected-id \$_BSTAPID -expected-id \$_BSTAPID2

#	jtag newtap \$_CHIPNAME bs -irlen 5 -expected-id \$_BSTAPID1 \
#	-expected-id \$_BSTAPID2 -expected-id \$_BSTAPID3 \
#	-expected-id \$_BSTAPID4 -expected-id \$_BSTAPID5 \
#	-expected-id \$_BSTAPID6 -expected-id \$_BSTAPID7



set _TARGETNAME \$_CHIPNAME.cpu
#\$_TARGETNAME configure -work-area-phys 0x20000000 -work-area-size \$_WORKAREASIZE -work-area-backup 0

target create \$_TARGETNAME cortex_m -endian \$_ENDIAN -chain-position \$_TARGETNAME

\$_TARGETNAME configure -work-area-phys 0x20000000 -work-area-size \$_WORKAREASIZE -work-area-backup 0

# flash size will be probed
set _FLASHNAME \$_CHIPNAME.flash

# stm32w108 has 128kB Flash
#flash bank \$_FLASHNAME em357 0x08000000 0 0 0 \$_TARGETNAME
flash bank \$_FLASHNAME em357 0x08000000 0x00020000 0 0 \$_TARGETNAME

# em357 has 128kB Flash
#flash bank \$_FLASHNAME em357 0x08000000 0x00040000 0 0 \$_TARGETNAME

# if srst is not fitted use SYSRESETREQ to
# perform a soft reset
cortex_m reset_config sysresetreq

END_OF_CONFIG
#}

  ConfigFile="$Folder_OpenOCD/share/openocd/scripts/target/stm32w1xx_flash.cfg"
  echo "creating cfg-file '`pwd`/$ConfigFile'.."
  cat <<END_OF_CONFIG >$ConfigFile #{
# http://TheToolchain.com
#
# OpenOCD flash script for  stm32w108 (former EM357)
# created by $0


# finish configuration stage + proceed to run stage
init

# hardware reset processor with halt (2 times is safer than only once)
reset
soft_reset_halt

# check target state stm32w
poll

# list all found flash banks
flash banks


# identify the flash
flash probe 0

em357 unlock 0

# erasing all flash
em357 mass_erase 0

# write file to flash number 0
flash write_bank 0 main.bin 0

# hardware processor reset with run
reset run

#shutdown openocd daemon
shutdown

END_OF_CONFIG
#}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                               #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
