#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a single Low-Level Driver
#  for radio devices.
#
#  Created from template _install_NN_TTC_DEVICE_ARCHITECTURE.sh revision 25 at 20150309 14:18:02 UTC
#
#  Supported Architecture: dw1000
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$0 - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="dw1000"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards "extensions"
DirLL_Extensions="$FoundFolder"

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK\.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download doc-file + move to Documentation/ + create symbolic link into Documentation/ right here 
  # File="STM32F10x_Standard_Peripherals_Library.html"
  # getFile http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library $File
  # addDocumentationFile $File STM

  for File in "aph005_dw1000_power_source_selection.pdf" \
              "aph006_pcb_layout.zip" \
              "aph007_dw1000_antenna_selection.pdf" \
              "aph010_dw1000_inter_channel_interference.pdf" \
              "apr003_certification_guide_europe_v1.0.pdf" \
              "aps001_system_related_aspects_of_dw1000_power_consumption_130821.pdf" \
              "aps003_dw1000_rtls_introduction.pdf" \
              "aps004_increasing_range_of_dw1000_using_lna_v1.4.pdf" \
              "aps006_channel_effects_on_range_accuracy.pdf" \
              "aps007_wired_sync_rtls_with_the_dw1000.pdf" \
              "aps009_dw1000_under_laes_v1.3.pdf" \
              "aps010_dw1000_wsn.pdf" \
              "aps011_sources_of_error_in_twr.pdf" \
              "aps013_dw1000_and_two_way_ranging.pdf" \
              "aps014-antennadelaycalibrationofdw1000-basedproductsandsystems_v1.01.pdf" \
              "aps017_max_range_in_dw1000_systems.pdf" \
              "aps019_driving_dw1000_from_8-bit_mcu_v1.0.pdf" \
              "APS022_Debugging_DW1000_based_products_systems.pdf" \
              "apu001_dsconfigurationmode1-16.pdf" \
              "apu002_healthcare.pdf" \
              "apu003_agriculture.pdf" \
              "apu007_automotive_kes.pdf" \
              "comparison_of_wireless_clock_synchronization_algorithms_for_indoor_rtls.pdf" \
              "decawave_paper_on_rtls_0.pdf" \
              "dw1000_user_manual_2.09.pdf" \
              "experimental_impulse_radio_ieee_802.15.4a_uwb_0.pdf" \
              "uwb_wireless_positioning_systems_technical_report.pdf" \
              "dw1000-datasheet-v2.10.pdf" \
              "evb1000_schematic.pdf" \
              "dwm1000-datasheet-v1.4.pdf"
  do
    getFile http://thetoolchain.com/mirror/dw1000/ $File 
    addDocumentationFile $File Devices/Radio/DW1000
  done

  if [ -e "evb1000_schematic.pdf" ]; then
    createLink evb1000_schematic.pdf Board_DecaWave_EVB1000.pdf
    addDocumentationFile "Board_DecaWave_EVB1000.pdf" Boards/
  fi

  if [ "$Get_Fails" == "" ]; then #{ check get() inside scripts/installFuncs.sh
    touch OK\.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then        #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then        #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for radio on dw1000
  
    DriverName="450_radio_dw1000"
    createExtensionMakefileHead ${DriverName}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

#{ RADIO1 - define radio transceiver
  COMPILE_OPTS += -DTTC_RADIO1=0
  COMPILE_OPTS += -DTTC_RADIO1_DRIVER=ta_radio_dw1000
  COMPILE_OPTS += -DTTC_RADIO1_BOOST_MODE=0
  COMPILE_OPTS += -DTTC_RADIO1_ALTERNATE_TX_PATH=0
  COMPILE_OPTS += -DTTC_RADIO1_PIN_TX_ACTIVE=E_ttc_gpio_pin_c5
  COMPILE_OPTS += -DTTC_RADIO1_PIN_TX_LOWACTIVE=0

  # enable ranging fields in pack
  COMPILE_OPTS += -DTTC_PACKET_RANGING=1
  
  #MAIN_OBJS += deca_device.o
  #MAIN_OBJS += deca_spi.o
  #MAIN_OBJS += deca_port.o
  #MAIN_OBJS += deca_mutex.o
  #MAIN_OBJS += deca_params_init.o
  #MAIN_OBJS += deca_range_tables.o
  
  #MAIN_OBJS += instance_common.o
  #MAIN_OBJS += instance.o
  
  #MAIN_OBJS += instance_calib.o
  
  
  #MAIN_OBJS += instance_fast2wr_t.o
  #MAIN_OBJS += instance_fast2wr_c.o
  #MAIN_OBJS += instance_fast2wr_a.o
  
  MAIN_OBJS += radio_dw1000.o
#}

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "low-level radio Driver for dw1000 architecture" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level Driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "450_radio_dw1000" "Ultra wideband radio transceiver DW1000"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  # low-level drivers typically are called from their high-level 500_ttc_<device>_prepare() function.

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers require a certain file to exist inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.450_ethernet_ste101p -> allows activate.500_ethernet.sh to enable 450_ethernet_ste101p.
  
  # enableFeature 450_ethernet_ste101p
  enableFeature 450_packet_802154
  
  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  activate.500_ttc_spi.sh           QUIET "\$0"
  activate.450_fake_sbrk_support.sh QUIET "\$0" 
  
  #}
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  fi #}low-level driver
  if [ "1" == "1" ]; then #{ board UWB-Sensor rev10
  
    DriverName="100_board_uwb_sensor10"
    createExtensionMakefileHead ${DriverName}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

COMPILE_OPTS += -DTTC_BOARD

# Activate microcontroller being placed on your board.
# This list has been extracted from all ttc-lib/cpu/cpu_*_types.h files.
# Adding a new cpu requires to add an entry to one of these files.

# This list of CPU-Variants has been extracted from ttc_cpu_types.h by ./createNewProject.pl on Di 18. Aug 11:35:54 CEST 2015
# Activate exactly one of this lines:
#  TTC_CPU_VARIANT_stm32f051r8t6   = 1
#  TTC_CPU_VARIANT_stm32f100rbt6   = 1
#  TTC_CPU_VARIANT_stm32f103r6t6a  = 1
#  TTC_CPU_VARIANT_stm32f103r8t6   = 1
#  TTC_CPU_VARIANT_stm32f103rbt6   = 1
#  TTC_CPU_VARIANT_stm32f103rdy6tr = 1
#  TTC_CPU_VARIANT_stm32f103zet6   = 1
#  TTC_CPU_VARIANT_stm32f105rct6   = 1
#  TTC_CPU_VARIANT_stm32f107vct6   = 1
#  TTC_CPU_VARIANT_stm32f302r8t6   = 1
#  TTC_CPU_VARIANT_stm32l053r8t6   = 1
 TTC_CPU_VARIANT_stm32l100rbt6   = 1
#  TTC_CPU_VARIANT_stm32l100rct6   = 1
#  TTC_CPU_VARIANT_stm32l152rbt6   = 1
#  TTC_CPU_VARIANT_stm32w108ccu    = 1
#InsertCpuVariants above

# define frequency of external high-speed crystal (mandatory if installed)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=8000000

# Define all internal devices which are usable on your current printed circuit board (PCB).
# To find correct gpio pin for a certain device, check schematic of your PCB.
#
# Note: All GPIO pin definitions have to use E_ttc_gpio_pin_p<BANK><Number> names.
#       E.g.: GPIO bank A pin number 3 -> E_ttc_gpio_pin_pa3
#       See enumeration e_ttc_gpio_pin for a complete list of valid pin names.
#

#{ LEDs
#COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_b12
#COMPILE_OPTS += -DTTC_LED1_LOWACTIVE=0      # ==1: gpio pin has to output 0 to activate LED; ==0: gpio pin has to output 1 to activate LED
#} LEDs

#{ Radio Transceiver DecaWave DW1000
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_EXT_ON=E_ttc_gpio_pin_c4                 # GPIO1
#(Not available on DWM1000) COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_SYNC=E_ttc_gpio_pin_b0                   # TIMER1
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_WAKEUP=E_ttc_gpio_pin_c5                 # GPIO2
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_RX_OK=E_ttc_gpio_pin_c7                  # GPIO4
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_START_FRAME_DETECTED=E_ttc_gpio_pin_c8   # GPIO5
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_EXTPA=E_ttc_gpio_pin_b10                 # I2C2_SCL
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_SPIPOL=E_ttc_gpio_pin_c6       # (GPIO3)     GPIO pin connected to SPIPOL pin of DW1000
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_IRQ=E_ttc_gpio_pin_a2          # (IRQ1)      GPIO pin connected to IRQ output pin of DW1000
#COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_IRQ2=E_ttc_gpio_pin_c13       # WakeUp (connected to TTC_RADIO1_DW1000_PIN_IRQ on this board)
COMPILE_OPTS += -DTTC_RADIO1_DW1000_TX_POWER=0x15355575     # transmitter power level (yet uncalibrated)

# configure SPI device used to communicate with dw1000
COMPILE_OPTS += -DTTC_RADIO1_INDEX_SPI=1                          # logical index of SPI device to use to communicate with radio #1
COMPILE_OPTS += -DTTC_RADIO1_INDEX_SPI_NSS=1                      # logical index of slave select pin to use
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_RESET=E_ttc_gpio_pin_a3        # (USART2_RX) GPIO pin connected to /reset pin of DW1000
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_SPIPHA=E_ttc_gpio_pin_c9       # (GPIO6)     GPIO pin connected to SPIPHASE pin of DW1000
#}
#{ SPI interfaces
COMPILE_OPTS += -DTTC_SPI1=ttc_device_1                     # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a7                      # GPIO-Pin used for Master Out Slave In
COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a6                      # GPIO-Pin used for Master In Slave Out
COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a5                       # GPIO-Pin used for Serial Clock
COMPILE_OPTS += -DTTC_SPI1_NSS1=E_ttc_gpio_pin_b1                      # GPIO-Pin used for Slave Select #1
#}
#{ USB interface
COMPILE_OPTS += -DTTC_USB_ENABLE=E_ttc_gpio_pin_a8
COMPILE_OPTS += -DTTC_USB_DP=E_ttc_gpio_pin_a12
COMPILE_OPTS += -DTTC_USB_DM=E_ttc_gpio_pin_a11
#}

COMPILE_OPTS += -DTTC_RTC_CLOCK_SOURCE_LSE

#{ Switches (input with pull up resistor)

# COMPILE_OPTS += -DTTC_SWITCH1=             # GPIO pin connected to press button switch #1
# COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=   # ==1: gpio pin reads 1 if button is pressed; ==0: gpio pin reads 0 if button is pressed
# COMPILE_OPTS += -DTTC_SWITCH2=             # GPIO pin connected to press button switch #1
# COMPILE_OPTS += -DTTC_SWITCH2_LOWACTIVE=   # ==1: gpio pin reads 1 if button is pressed; ==0: gpio pin reads 0 if button is pressed

#}
#{ USART1 - define serial port #1
  COMPILE_OPTS += -DTTC_USART1=ttc_device_1 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
  COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_a9     # GPIO pin used to send out serial sata
  COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_a10    # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART1_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART1_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART1
#{ USART2 - define serial port #2
#  COMPILE_OPTS += -DTTC_USART2=ttc_device_2 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_USART2_TX=          # GPIO pin used to send out serial sata
#  COMPILE_OPTS += -DTTC_USART2_RX=          # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART2_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART2_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART2
#{ USART3 - define serial port #3
#  COMPILE_OPTS += -DTTC_USART3=ttc_device_3 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_USART3_TX=          # GPIO pin used to send out serial sata
#  COMPILE_OPTS += -DTTC_USART3_RX=          # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART3_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART3_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART3
#{ USART4 - define serial port #4
#  COMPILE_OPTS += -DTTC_USART4=ttc_device_4 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_USART4_TX=          # GPIO pin used to send out serial sata
#  COMPILE_OPTS += -DTTC_USART4_RX=          # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART4_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART4_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART4
#{ USART5 - define serial port #5
#  COMPILE_OPTS += -DTTC_USART5=ttc_device_5 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_USART5_TX=          # GPIO pin used to send out serial sata
#  COMPILE_OPTS += -DTTC_USART5_RX=          # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART5_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART5_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART5
#{ SPI2 - define serial peripheral interface port #2
#  COMPILE_OPTS += -DTTC_SPI2=ttc_device_2   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_SPI2_MOSI=          # GPIO-Pin used for Master Out Slave In
#  COMPILE_OPTS += -DTTC_SPI2_MISO=          # GPIO-Pin used for Master In Slave Out
#  COMPILE_OPTS += -DTTC_SPI2_SCK=           # GPIO-Pin used for Serial Clock 
#  COMPILE_OPTS += -DTTC_SPI2_NSS=           # GPIO-Pin used for Slave Select
#}SPI2
#{ SPI3 - define serial peripheral interface port #3
#  COMPILE_OPTS += -DTTC_SPI3=ttc_device_3   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_SPI3_MOSI=          # GPIO-Pin used for Master Out Slave In
#  COMPILE_OPTS += -DTTC_SPI3_MISO=          # GPIO-Pin used for Master In Slave Out
#  COMPILE_OPTS += -DTTC_SPI3_SCK=           # GPIO-Pin used for Serial Clock 
#  COMPILE_OPTS += -DTTC_SPI3_NSS=           # GPIO-Pin used for Slave Select
#}SPI3
#{ I2C1 - define inter-integrated circuit interface #1
#  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_I2C1_SDA=           # GPIO-Pin used for SPI Serial Data
#  COMPILE_OPTS += -DTTC_I2C1_SCL=           # GPIO-Pin used for SPI Serial Clock
#  COMPILE_OPTS += -DTTC_I2C1_SMBAL=         # GPIO-Pin used for SPI SMBus Alert (if configured to use SMBus mode)
#}I2C1
#{ I2C2 - define inter-integrated circuit interface #2
#  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_I2C2_SDA=           # GPIO-Pin used for SPI Serial Data
#  COMPILE_OPTS += -DTTC_I2C2_SCL=           # GPIO-Pin used for SPI Serial Clock
#  COMPILE_OPTS += -DTTC_I2C2_SMBAL=         # GPIO-Pin used for SPI SMBus Alert (if configured to use SMBus mode)
#}I2C2
#{ I2C3 - define inter-integrated circuit interface #3
#  COMPILE_OPTS += -DTTC_I2C3=ttc_device_3   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_I2C3_SDA=           # GPIO-Pin used for SPI Serial Data
#  COMPILE_OPTS += -DTTC_I2C3_SCL=           # GPIO-Pin used for SPI Serial Clock
#  COMPILE_OPTS += -DTTC_I2C3_SMBAL=         # GPIO-Pin used for SPI SMBus Alert (if configured to use SMBus mode)
#}I2C2

# I2S- (Inter-IC Sound-) Interface              ToDo: define constant names

# CAN- (Controller Area Network-) Bus Interface ToDo: define constant names


END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "Wireless sensor node combining STM32l100 + DecaWave DWM1000 Ranging Ultrawideband Transceiver" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level Driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "*"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

#  if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
#    createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
#  fi

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers require a certain file to exist inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.450_ethernet_ste101p -> allows activate.500_ethernet.sh to enable 450_ethernet_ste101p.
  
  #{ provide features that allow to activate certain extensions  (at least provide one 450_cpu feature!)
  #
  # This list of features has been generated by ./createNewProject.pl from all activate scripts on Di 18. Aug 11:35:54 CEST 2015
  # enableFeature 150_board_extension_dualradio_cc1101_cc1190 # Example extension for external radio transceiver cc1101 connected via SPI bus - architectures:
  # enableFeature 150_board_extension_radio_1_cc1101          # Example extension for external radio transceiver cc1101 connected via SPI bus - architectures:
  # enableFeature 150_board_extension_radio_2_cc1101          # Example extension for external radio transceiver cc1101 connected via SPI bus - architectures:
  # enableFeature 150_board_extension_radio_3_cc1101          # Example extension for external radio transceiver cc1101 connected via SPI bus - architectures:
  # enableFeature 150_board_extension_radio_4_cc1101          # Example extension for external radio transceiver cc1101 connected via SPI bus - architectures:
  # enableFeature 150_board_extension_radio_5_cc1101          # Example extension for external radio transceiver cc1101 connected via SPI bus - architectures:
  # enableFeature 450_gfx_ili93xx                            # 320x240 Color LCD using interface chip ILI93xx
  # enableFeature 450_accelerometer_bno055                    # Inertial sensor BNO055
  # enableFeature 450_accelerometer_lis3lv02dl                # 3-axis accelerometer LIS3LV02DL
  enableFeature 450_cpu_cortexm3                            # Common driver for all microcontrollers with CortexM3 core
  # enableFeature 450_cpu_stm32f1xx                           # Driver for STM32F1xx microcontrollers
  # enableFeature 450_cpu_stm32f4xx                           # Driver for STM32F4xx microcontrollers
  # enableFeature 450_cpu_stm32l0xx                           # Driver for STM32L0xx microcontrollers
  enableFeature 450_cpu_stm32l1xx                           # Driver for STM32L1xx microcontrollers
  # enableFeature 450_cpu_stm32w1xx                           # Driver for STM32W1xx microcontrollers
  # enableFeature 450_ethernet                                # TCP/IP-Ethernet Stack
  # enableFeature 450_gyroscope_mpu6050                       # Intertial Sensor MPU6050
  # enableFeature 450_interface_ste101p                       # Ethernet Interface STE101P
  enableFeature 450_math_software_float                       # Software Floating Point Library
  enableFeature 450_radio_dw1000                              # Ultra wideband radio transceiver DW1000
  enableFeature 450_packet_802154                             # network packets of type IEEE 802.15.4
  # enableFeature 450_touchpad_analog4                        # Touchpad with 4 pin analog interface
#InsertFeatures above
#}provide features

  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  activate.500_ttc_math.sh          QUIET "\$0"
  activate.500_ttc_spi.sh           QUIET "\$0"
  activate.500_ttc_register.sh      QUIET "\$0"
  #}
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  fi #}low-level driver
  if [ "1" == "1" ]; then #{ board Sensor-DWM rev12
  
    DriverName="100_board_sensor_dwm1000_12"
    createExtensionMakefileHead ${DriverName}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

COMPILE_OPTS += -DTTC_BOARD

# Activate microcontroller being placed on your board.
# This list has been extracted from all ttc-lib/cpu/cpu_*_types.h files.
# Adding a new cpu requires to add an entry to one of these files.

# This list of CPU-Variants has been extracted from ttc_cpu_types.h by ./createNewProject.pl on Di 18. Aug 11:35:54 CEST 2015
# Activate exactly one of this lines:
#  TTC_CPU_VARIANT_stm32f051r8t6   = 1
#  TTC_CPU_VARIANT_stm32f100rbt6   = 1
#  TTC_CPU_VARIANT_stm32f103r6t6a  = 1
#  TTC_CPU_VARIANT_stm32f103r8t6   = 1
#  TTC_CPU_VARIANT_stm32f103rbt6   = 1
#  TTC_CPU_VARIANT_stm32f103rdy6tr = 1
#  TTC_CPU_VARIANT_stm32f103zet6   = 1
#  TTC_CPU_VARIANT_stm32f105rct6   = 1
#  TTC_CPU_VARIANT_stm32f107vct6   = 1
#  TTC_CPU_VARIANT_stm32f302r8t6   = 1
#  TTC_CPU_VARIANT_stm32l053r8t6   = 1
 TTC_CPU_VARIANT_stm32l100rbt6   = 1
#  TTC_CPU_VARIANT_stm32l100rct6   = 1
#  TTC_CPU_VARIANT_stm32l152rbt6   = 1
#  TTC_CPU_VARIANT_stm32w108ccu    = 1
#InsertCpuVariants above

# define frequency of external high-speed crystal (mandatory if installed)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=8000000

# Define all internal devices which are usable on your current printed circuit board (PCB).
# To find correct gpio pin for a certain device, check schematic of your PCB.
#
# Note: All GPIO pin definitions have to use E_ttc_gpio_pin_p<BANK><Number> names.
#       E.g.: GPIO bank A pin number 3 -> E_ttc_gpio_pin_pa3
#       See enumeration e_ttc_gpio_pin for a complete list of valid pin names.
#

#{ LEDs
COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_b12
COMPILE_OPTS += -DTTC_LED1_LOWACTIVE=0      # ==1: gpio pin has to output 0 to activate LED; ==0: gpio pin has to output 1 to activate LED
#} LEDs

#{ Radio Transceiver DecaWave DW1000
#(Not available on DWM1000) COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_SYNC=E_ttc_gpio_pin_b0                   # TIMER1
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_EXT_ON=E_ttc_gpio_pin_c4                 # GPIO1
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_WAKEUP=E_ttc_gpio_pin_c5                 # GPIO2
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_RX_OK=E_ttc_gpio_pin_c7                  # GPIO4
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_START_FRAME_DETECTED=E_ttc_gpio_pin_c8   # GPIO5
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_EXTPA=E_ttc_gpio_pin_b10                 # I2C2_SCL
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_IRQ=E_ttc_gpio_pin_c13         # (WakeUp1)   GPIO pin connected to IRQ output pin of DW1000 (pin has changed from rev 10 -> 12)
COMPILE_OPTS += -DTTC_RADIO1_DW1000_TX_POWER=0x15355575     # transmitter power level (yet uncalibrated)

# configure SPI device used to communicate with dw1000
COMPILE_OPTS += -DTTC_RADIO1_INDEX_SPI=1                          # logical index of SPI device to use to communicate with radio #1
COMPILE_OPTS += -DTTC_RADIO1_INDEX_SPI_NSS=1                      # logical index of slave select pin to use (pin will be defined as TTC_SPI1_NSS1 below)
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_RESET=E_ttc_gpio_pin_a4        # (           GPIO pin connected to /reset pin of DW1000 (pin has changed from rev 10 -> 12)
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_SPIPHA=E_ttc_gpio_pin_c9       # (GPIO6)     GPIO pin connected to SPIPHASE pin of DW1000
COMPILE_OPTS += -DTTC_RADIO1_DW1000_PIN_SPIPOL=E_ttc_gpio_pin_c6       # (GPIO3)     GPIO pin connected to SPIPOL pin of DW1000
#}
#{ SPI interfaces
COMPILE_OPTS += -DTTC_SPI1=ttc_device_1                     # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a7                      # GPIO-Pin used for Master Out Slave In
COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a6                      # GPIO-Pin used for Master In Slave Out
COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a5                       # GPIO-Pin used for Serial Clock
COMPILE_OPTS += -DTTC_SPI1_NSS1=E_ttc_gpio_pin_b1                      # GPIO-Pin used for Slave Select #1
#}
#{ USB interface
COMPILE_OPTS += -DTTC_USB_ENABLE=E_ttc_gpio_pin_a8
COMPILE_OPTS += -DTTC_USB_DP=E_ttc_gpio_pin_a12
COMPILE_OPTS += -DTTC_USB_DM=E_ttc_gpio_pin_a11
#}
#{ PCB Connector Pins

# Connector P5 "SWD+UART+CAN" and P2 "SWD+UART+CAN_2" 
COMPILE_OPTS += -DPCB_CON_P5_SWCLK=E_ttc_gpio_pin_a14
COMPILE_OPTS += -DPCB_CON_P5_SWDIO=E_ttc_gpio_pin_a13
COMPILE_OPTS += -DPCB_CON_P5_TxD=E_ttc_gpio_pin_a9
COMPILE_OPTS += -DPCB_CON_P5_RxD=E_ttc_gpio_pin_a10

# Connector P4 GPIO
COMPILE_OPTS += -DPCB_CON_P4_ADC1=E_ttc_gpio_pin_b7
COMPILE_OPTS += -DPCB_CON_P4_ADC2=E_ttc_gpio_pin_a15
COMPILE_OPTS += -DPCB_CON_P4_SCK=E_ttc_gpio_pin_c10
COMPILE_OPTS += -DPCB_CON_P4_MOSI=E_ttc_gpio_pin_c12
COMPILE_OPTS += -DPCB_CON_P4_MISO=E_ttc_gpio_pin_c11
COMPILE_OPTS += -DPCB_CON_P4_TIMER1=E_ttc_gpio_pin_b4
COMPILE_OPTS += -DPCB_CON_P4_TIMER2=E_ttc_gpio_pin_b3
COMPILE_OPTS += -DPCB_CON_P4_GPIO2=E_ttc_gpio_pin_d2
COMPILE_OPTS += -DPCB_CON_P4_GPIO5=E_ttc_gpio_pin_b8
#}

COMPILE_OPTS += -DTTC_RTC_CLOCK_SOURCE_LSE

#{ Switches (input with pull up resistor)

# COMPILE_OPTS += -DTTC_SWITCH1=             # GPIO pin connected to press button switch #1
# COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=   # ==1: gpio pin reads 1 if button is pressed; ==0: gpio pin reads 0 if button is pressed
# COMPILE_OPTS += -DTTC_SWITCH2=             # GPIO pin connected to press button switch #1
# COMPILE_OPTS += -DTTC_SWITCH2_LOWACTIVE=   # ==1: gpio pin reads 1 if button is pressed; ==0: gpio pin reads 0 if button is pressed

#}
#{ USART1 - define serial port #1
  COMPILE_OPTS += -DTTC_USART1=ttc_device_1 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
  COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_a9     # GPIO pin used to send out serial sata
  COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_a10    # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART1_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART1_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART1
#{ USART2 - define serial port #2
#  COMPILE_OPTS += -DTTC_USART2=ttc_device_2 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_USART2_TX=          # GPIO pin used to send out serial sata
#  COMPILE_OPTS += -DTTC_USART2_RX=          # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART2_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART2_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART2
#{ USART3 - define serial port #3
#  COMPILE_OPTS += -DTTC_USART3=ttc_device_3 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_USART3_TX=          # GPIO pin used to send out serial sata
#  COMPILE_OPTS += -DTTC_USART3_RX=          # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART3_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART3_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART3
#{ USART4 - define serial port #4
#  COMPILE_OPTS += -DTTC_USART4=ttc_device_4 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_USART4_TX=          # GPIO pin used to send out serial sata
#  COMPILE_OPTS += -DTTC_USART4_RX=          # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART4_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART4_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART4
#{ USART5 - define serial port #5
#  COMPILE_OPTS += -DTTC_USART5=ttc_device_5 # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_USART5_TX=          # GPIO pin used to send out serial sata
#  COMPILE_OPTS += -DTTC_USART5_RX=          # GPIO pin used to read in serial sata
#  COMPILE_OPTS += -DTTC_USART5_RTS=         # GPIO pin used to send out Request to Send signal
#  COMPILE_OPTS += -DTTC_USART5_CTS=         # GPIO pin used to read in Clear to Send signal
#}USART5
#{ SPI2 - define serial peripheral interface port #2
#  COMPILE_OPTS += -DTTC_SPI2=ttc_device_2   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_SPI2_MOSI=          # GPIO-Pin used for Master Out Slave In
#  COMPILE_OPTS += -DTTC_SPI2_MISO=          # GPIO-Pin used for Master In Slave Out
#  COMPILE_OPTS += -DTTC_SPI2_SCK=           # GPIO-Pin used for Serial Clock 
#  COMPILE_OPTS += -DTTC_SPI2_NSS=           # GPIO-Pin used for Slave Select
#}SPI2
#{ SPI3 - define serial peripheral interface port #3
#  COMPILE_OPTS += -DTTC_SPI3=ttc_device_3   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_SPI3_MOSI=          # GPIO-Pin used for Master Out Slave In
#  COMPILE_OPTS += -DTTC_SPI3_MISO=          # GPIO-Pin used for Master In Slave Out
#  COMPILE_OPTS += -DTTC_SPI3_SCK=           # GPIO-Pin used for Serial Clock 
#  COMPILE_OPTS += -DTTC_SPI3_NSS=           # GPIO-Pin used for Slave Select
#}SPI3
#{ I2C1 - define inter-integrated circuit interface #1
#  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_I2C1_SDA=           # GPIO-Pin used for SPI Serial Data
#  COMPILE_OPTS += -DTTC_I2C1_SCL=           # GPIO-Pin used for SPI Serial Clock
#  COMPILE_OPTS += -DTTC_I2C1_SMBAL=         # GPIO-Pin used for SPI SMBus Alert (if configured to use SMBus mode)
#}I2C1
#{ I2C2 - define inter-integrated circuit interface #2
#  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_I2C2_SDA=           # GPIO-Pin used for SPI Serial Data
#  COMPILE_OPTS += -DTTC_I2C2_SCL=           # GPIO-Pin used for SPI Serial Clock
#  COMPILE_OPTS += -DTTC_I2C2_SMBAL=         # GPIO-Pin used for SPI SMBus Alert (if configured to use SMBus mode)
#}I2C2
#{ I2C3 - define inter-integrated circuit interface #3
#  COMPILE_OPTS += -DTTC_I2C3=ttc_device_3   # map logical device to internal physical device (ttc_device_1 = first physical device, ...)
#  COMPILE_OPTS += -DTTC_I2C3_SDA=           # GPIO-Pin used for SPI Serial Data
#  COMPILE_OPTS += -DTTC_I2C3_SCL=           # GPIO-Pin used for SPI Serial Clock
#  COMPILE_OPTS += -DTTC_I2C3_SMBAL=         # GPIO-Pin used for SPI SMBus Alert (if configured to use SMBus mode)
#}I2C2

# I2S- (Inter-IC Sound-) Interface              ToDo: define constant names

# CAN- (Controller Area Network-) Bus Interface ToDo: define constant names


END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "Wireless sensor node combining STM32l100 + DecaWave DWM1000 Ranging Ultrawideband Transceiver" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level Driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "*"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

#  if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
#    createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
#  fi

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers require a certain file to exist inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.450_ethernet_ste101p -> allows activate.500_ethernet.sh to enable 450_ethernet_ste101p.
  
  #{ provide features that allow to activate certain extensions  (at least provide one 450_cpu feature!)
  #
  # This list of features has been generated by ./createNewProject.pl from all activate scripts on Di 18. Aug 11:35:54 CEST 2015
  # enableFeature 150_board_extension_dualradio_cc1101_cc1190 # Example extension for external radio transceiver cc1101 connected via SPI bus - architectures:
  # enableFeature 150_board_extension_radio_1_cc1101          # Example extension for external radio transceiver cc1101 connected via SPI bus - architectures:
  # enableFeature 150_board_extension_radio_2_cc1101          # Example extension for external radio transceiver cc1101 connected via SPI bus - architectures:
  # enableFeature 150_board_extension_radio_3_cc1101          # Example extension for external radio transceiver cc1101 connected via SPI bus - architectures:
  # enableFeature 150_board_extension_radio_4_cc1101          # Example extension for external radio transceiver cc1101 connected via SPI bus - architectures:
  # enableFeature 150_board_extension_radio_5_cc1101          # Example extension for external radio transceiver cc1101 connected via SPI bus - architectures:
  # enableFeature 450_gfx_ili93xx                            # 320x240 Color LCD using interface chip ILI93xx
  # enableFeature 450_accelerometer_bno055                    # Inertial sensor BNO055
  # enableFeature 450_accelerometer_lis3lv02dl                # 3-axis accelerometer LIS3LV02DL
  enableFeature 450_cpu_cortexm3                            # Common driver for all microcontrollers with CortexM3 core
  # enableFeature 450_cpu_stm32f1xx                           # Driver for STM32F1xx microcontrollers
  # enableFeature 450_cpu_stm32f4xx                           # Driver for STM32F4xx microcontrollers
  # enableFeature 450_cpu_stm32l0xx                           # Driver for STM32L0xx microcontrollers
  enableFeature 450_cpu_stm32l1xx                           # Driver for STM32L1xx microcontrollers
  # enableFeature 450_cpu_stm32w1xx                           # Driver for STM32W1xx microcontrollers
  # enableFeature 450_ethernet                                # TCP/IP-Ethernet Stack
  # enableFeature 450_gyroscope_mpu6050                       # Intertial Sensor MPU6050
  # enableFeature 450_interface_ste101p                       # Ethernet Interface STE101P
  enableFeature 450_math_software_float                       # Software Floating Point Library
  enableFeature 450_radio_dw1000                              # Ultra wideband radio transceiver DW1000
  enableFeature 450_packet_802154                             # network packets of type IEEE 802.15.4
  # enableFeature 450_touchpad_analog4                        # Touchpad with 4 pin analog interface
#InsertFeatures above
#}provide features

  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  activate.500_ttc_math.sh          QUIET "\$0"
  activate.500_ttc_spi.sh           QUIET "\$0"
  activate.500_ttc_register.sh      QUIET "\$0"
  #}
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

#if [ -e \$Dir_Extensions/$ExtensionSourceFile ]; then
#  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET
#fi

#}
#  ACTIVATE_SECTION_E regressions do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

#activate.500_ttc_gpio.sh QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ optional: create usage example for your extension
  
    NameExample="example_${EXTENSION_NAME}_${ThisArchitecture}"
    Name="600_$NameExample"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/${NameExample}.h"
${NameExample}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += ${NameExample}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "Usage example for extension ${NameExample}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/source.600_${NameExample}.c ]; then
  createLink \$Dir_Extensions/source.600_${NameExample}.c \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#  ACTIVATE_SECTION_E examples do not provide features 
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$0"
activate.500_ttc_memory.sh QUIET "\$0"
activate.500_ttc_spi.sh    QUIET "\$0"
activate.500_ttc_usart.sh  QUIET "\$0"
activate.500_ttc_rtc.sh    QUIET "\$0"
activate.500_ttc_gpio.sh   QUIET "\$0"

#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                                #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

