#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a single Low-Level Driver
#  for board devices.
#
#  Created from template _install_TTC_DEVICE_ARCHITECTURE.sh revision 38 at 20180123 14:03:24 UTC
#
#  Supported Architecture: olimex_stm32_p107
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$ScriptName - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="olimex_stm32_p107"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards "extensions"
findFolderUpwards extensions; DirLL_Extensions="$MyStartDir/$ThisArchitecture/$FoundFolder"
findFolderUpwards scripts;    Start_Path="$MyStartDir/$ThisArchitecture/${FoundFolder}.."

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$ScriptName: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$ScriptName - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://olimex.com/dev/pdf/ARM/ST/                              STM32-P107.pdf                      Board_STM32-P107.pdf                Boards/Olimex_P107
  getDocumentation https://www.olimex.com/Products/ARM/ST/STM32-P107/resources/   STM32_P107_revision_C_examples.zip  STM32_P107_revision_C_examples.zip  Boards/Olimex_P107
    
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for board on olimex_stm32_p107_revA
  
    DriverNameBase="${RANK_LOWLEVEL}_board_olimex_stm32_p107"
    DriverRevision="_revA"                            # you may add a suffix to the driver name (e.g. "_RevA")
    DriverName="${DriverNameBase}${DriverRevision}"
    createExtensionSourcefileHead ${DriverName}  #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile


END_OF_SOURCEFILE
    createExtensionSourcefileTail ${DriverName} #}    
    createExtensionMakefileHead ${DriverName} "" ${DriverNameBase}   #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

#X EXTENSION_${DriverNameBase} = 1
#X COMPILE_OPTS += -DEXTENSION_{$DriverNameBase}=1

MAIN_OBJS += board_olimex_stm32_p107.o  # ttc-lib/board/board_olimex_stm32_p107.c

COMPILE_OPTS += -DTTC_BOARD1=E_ttc_board_architecture_olimex_stm32_p107

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f107vct6 = 1

# system clock setup (-> ttc_sysclock_types.h)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=25000000
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768


# Define port pins
# reference RevA: http://olimex.com/dev/pdf/ARM/ST/STM32-P107-Rev.A-schematic.pdf

COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c6
COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_c7

COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_c13                # press button switch 1 (named Tamper)
COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=1
COMPILE_OPTS += -DTTC_SWITCH2=E_ttc_gpio_pin_a0                 # press button switch 2 (named WKUP)
COMPILE_OPTS += -DTTC_SWITCH2_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH2_LOWACTIVE=0

COMPILE_OPTS += -DTTC_USART1=INDEX_USART3       # USART connected to RS232 connector 9-pin female
COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_d8   # RS232 connector 9-pin female TX-pin
COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_d9   # RS232 connector 9-pin female RX-pin
COMPILE_OPTS += -DTTC_USART1_RTS=E_ttc_gpio_pin_d12 # RS232 connector 9-pin female RTS-pin
COMPILE_OPTS += -DTTC_USART1_CTS=E_ttc_gpio_pin_d11 # RS232 connector 9-pin female CTS-pin
  
COMPILE_OPTS += -DTTC_USART2=INDEX_USART2       # USART connected to 10-pin male header connector
COMPILE_OPTS += -DTTC_USART2_TX=E_ttc_gpio_pin_d5   # RS232 on 10-pin male header connector TX-Pin
COMPILE_OPTS += -DTTC_USART2_RX=E_ttc_gpio_pin_d6   # RS232 on 10-pin male header connector RX-Pin


#{ define pin configuration of GPIOs on the UEXT Conncector
  COMPILE_OPTS += -DUEXT1_USART_TX=E_ttc_gpio_pin_d5 # USART2
  COMPILE_OPTS += -DUEXT1_USART_RX=E_ttc_gpio_pin_d6 # USART2
  COMPILE_OPTS += -DUEXT1_I2C_SCL=E_ttc_gpio_pin_b6
  COMPILE_OPTS += -DUEXT1_I2C_SDA=E_ttc_gpio_pin_b7
  COMPILE_OPTS += -DUEXT1_SPI_MISO=E_ttc_gpio_pin_c11
  COMPILE_OPTS += -DUEXT1_SPI_MOSI=E_ttc_gpio_pin_c12
  COMPILE_OPTS += -DUEXT1_SPI_NSCK=E_ttc_gpio_pin_c10
  COMPILE_OPTS += -DUEXT1_SPI_NSS=E_ttc_gpio_pin_b15
  
  COMPILE_OPTS += -DUEXT1_SPI_INDEX=3
  COMPILE_OPTS += -DUEXT1_USART_INDEX=2
  COMPILE_OPTS += -DUEXT1_I2C_INDEX=1
#}
#{ define Controller Area Network (CAN) devices
  COMPILE_OPTS += -DTTC_CAN1=0               # physical device index
  COMPILE_OPTS += -DTTC_CAN1_PIN_RX=E_ttc_gpio_pin_b8  # pin used for receive
  COMPILE_OPTS += -DTTC_CAN1_PIN_TX=E_ttc_gpio_pin_b9  # pin used for transmit
  COMPILE_OPTS += -DTTC_CAN2=1               # physical device index
  COMPILE_OPTS += -DTTC_CAN2_PIN_RX=E_ttc_gpio_pin_b12 # pin used for receive
  COMPILE_OPTS += -DTTC_CAN2_PIN_TX=E_ttc_gpio_pin_b13 # pin used for transmit
#}CAN
#{ SDCARD storage interface
COMPILE_OPTS += -DTTC_SDCARD1=E_ttc_sdcard_architecture_spi
COMPILE_OPTS += -DTTC_SDCARD1_SPI_LOGICAL_INDEX=1                   # using TTC_SPI1 to connect to SDCARD
COMPILE_OPTS += -DTTC_SDCARD1_SPI_SLAVE_INDEX=1                     # SDCARD slave select configured as TTC_SPI1_NSS1
COMPILE_OPTS += -DTTC_SDCARD1_VOLTAGE_MIN=E_ttc_sdcard_voltage_3V5  # minimum supported voltage in this sdcard slot is 3.5V
COMPILE_OPTS += -DTTC_SDCARD1_VOLTAGE_MAX=E_ttc_sdcard_voltage_3V6  # maximum supported voltage in this sdcard slot is 3.6V

#}
#{ parallel ports (8 or 16 consecutive pins in same bank)
#
# Every 8 available, consecutive bits in same GPIO bank can form an 8-bit wide port.
# A 16-bit wide port requires 16 available, consecutive bits (obviously).
# A pin is available if it not driven externally or used for something else.
# Check circuit of your board for details.

# 8-bit wide parallel port #1 starting at pin 0 of bank E
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_BANK=TTC_GPIO_BANK_E
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_FIRSTPIN=0

# 8-bit wide parallel port #2 starting at pin 8 of bank D (overlaps with USART2, USART3)
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_BANK=TTC_GPIO_BANK_D
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_FIRSTPIN=8


#}parallel ports
#{ SPI1
COMPILE_OPTS += -DTTC_SPI1=ttc_device_3          # SPI connected to RS232 10-pin male header UEXT and SDCARD slot
COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_c12  # UEXT pin 8
COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_c11  # UEXT pin 7
COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_c10   # UEXT pin 9
COMPILE_OPTS += -DTTC_SPI1_NSS1=E_ttc_gpio_pin_a4   # UEXT pin 10 (CS_MMC line used as slave select for SDCARD connector)
#}SPI1
#{ I2C1 - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b9    # UEXT-6
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b8    # UEXT-5
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=E_ttc_gpio_pin_b5  # 
#}I2C1
#{ Ethernet Interface

# configure ethernet transceiver
COMPILE_OPTS += -DTTC_ETHERNET1=0                                 # there is one ethernet interface on this board (->ttc_ethernet_types.h)
COMPILE_OPTS += -DTTC_ETHERNET1_DRIVER=ta_ethernet_stm32f107      # type of ethernet transceicver     (->ttc_ethernet_types.h:e_ttc_ethernet_architecture)
COMPILE_OPTS += -DTTC_ETHERNET1_INTERFACE=ethernet_interface_rmii # type of interface to transceicver (->ttc_ethernet_types.h:e_ttc_ethernet_interface)
#}

COMPILE_OPTS += -DTTC_SDCARD1_SPI=1 # Define at least TTC_SDCARD1 as logical index of SPI device to use (1=TTC_SPI1, ...)

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$ScriptName 2" "prototype board with stm32f107vct6 256kb flash 64 kb ram usb2.0 10/100 ethernet mac sdcard" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "$DriverName" "$cASH_InfoText"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  # board_olimex_stm32_p107_prepare() typically ist called ttc_board_prepare() during preparation of all extensions.
  # It should not be required to activate source.${RANK_LOWLEVEL}_*.c here.

  #X if [ -e \$Dir_Extensions/source.${DriverName}.c ]; then
  #X   createLink \$Dir_Extensions/source.${DriverName}.c \$Dir_ExtensionsActive/ '' QUIET
  #X fi

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers activate only if a certain file exists inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.${RANK_LOWLEVEL}_ethernet_ste101p -> allows activate.500_ethernet.sh to enable its low-level driver ${RANK_LOWLEVEL}_ethernet_ste101p.
  # enableFeature NAME  # creates extensions.active/feature.NAME
  # See activate_project.sh of a fresh created project for a list of all available features! 
  
  enableFeature 450_ethernet_ste101p                        # Ethernet MAC controller
  enableFeature 450_sdcard_spi                              # SDCARD being connected via SPI bus
  enableFeature 450_cpu_stm32f1xx                           # Driver for STM32F1xx microcontrollers (will also enable all stm32f1xx features)

  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  activate.500_ttc_spi.sh      QUIET "\$ScriptName"
  activate.500_ttc_sdcard.sh   QUIET "\$ScriptName"
  activate.500_ttc_ethernet.sh QUIET "\$ScriptName"
  
  #}
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression_${ThisArchitecture}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# define devices to use in this regression
COMPILE_OPTS += -DTTC_BOARD1=E_ttc_board_architecture_olimex_stm32_p107 # Line generated automatically. Remove if not wanted!

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$ScriptName 2" "PLACE YOUR INFO HERE FOR $ScriptName" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  if [ -e \$Dir_Extensions/source.${Name}.c ]; then
    createLink \$Dir_Extensions/source.${Name}.c \$Dir_ExtensionsActive/ '' QUIET
  fi

#}
#{ ACTIVATE_SECTION_E provide default features required to compile this example

  # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.  

  # enableFeature 450_math_software_float   # enable math driver (single precision)
  # enableFeature 450_math_software_double  # enable math driver (double precision)

#}
#{ ACTIVATE_SECTION_F call other activate-scripts

#activate.500_ttc_gpio.sh QUIET "\$ScriptName"

#}
#{ ACTIVATE_SECTION_G copy source code into project folder if required

if [ ! -d regressions ]; then
  mkdir regressions
fi
ExampleFound=\`ls regressions/regression_${EXTENSION_NAME}_${ThisArchitecture}*\`
if [ "\$ExampleFound" == "" ]; then
  echo "copying missing regression_${EXTENSION_NAME}_${ThisArchitecture}*.."
  cp -v ~/Source/TheToolChain/Template/regressions/regression_${EXTENSION_NAME}_${ThisArchitecture}* regressions/
fi
#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ recommended: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}_${ThisArchitecture}_${ThisArchitecture}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}_${ThisArchitecture}.h"
example_${EXTENSION_NAME}_${ThisArchitecture}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

# additional constant defines
#COMPILE_OPTS += -D

# define devices to use in this example
COMPILE_OPTS += -DTTC_BOARD1=E_ttc_board_architecture_olimex_stm32_p107 # Line generated automatically. Remove if not wanted!

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}_${ThisArchitecture}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$ScriptName 2" "Usage example for extension ${EXTENSION_NAME}_${ThisArchitecture}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/source.${Name}.c ]; then
  createLink \$Dir_Extensions/source.${Name}.c \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#{ ACTIVATE_SECTION_E provide default features required to compile this example

  # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.
  enableFeature $DriverNameBase  # enable low-level driver to be tested

  # enableFeature 450_math_software_float   # enable math driver (single precision)
  # enableFeature 450_math_software_double  # enable math driver (double precision)

#}
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$ScriptName" # activate high-level driver

#}
#{ ACTIVATE_SECTION_G copy source code into project folder if required

if [ ! -d examples ]; then
  mkdir examples
fi
ExampleFound=\`ls examples/example_${EXTENSION_NAME}_${ThisArchitecture}*\`
if [ "\$ExampleFound" == "" ]; then
  echo "copying missing example_${EXTENSION_NAME}.."
  cp -v ~/Source/TheToolChain/Template/examples/example_${EXTENSION_NAME}_${ThisArchitecture}* examples/
fi
#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                               #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
