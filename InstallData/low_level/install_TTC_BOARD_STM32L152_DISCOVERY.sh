#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a single Low-Level Driver
#  for board devices.
#
#  Created from template _install_TTC_DEVICE_ARCHITECTURE.sh revision 36 at 20180420 15:55:39 UTC
#
#  Supported Architecture: stm32l152_discovery
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$ScriptName - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="stm32l152_discovery"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards "extensions"
findFolderUpwards extensions; DirLL_Extensions="$MyStartDir/$ThisArchitecture/$FoundFolder"
findFolderUpwards scripts;    Start_Path="$MyStartDir/$ThisArchitecture/${FoundFolder}.."

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$ScriptName: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$ScriptName - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/ DM00027954.pdf Board_STM32-L152_Discovery.pdf Boards

  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for board on stm32l152_discovery
  
    DriverName="${RANK_LOWLEVEL}_board_stm32l152_discovery"
    createExtensionSourcefileHead ${DriverName}  #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile


END_OF_SOURCEFILE
    createExtensionSourcefileTail ${DriverName} #}    
    createExtensionMakefileHead ${DriverName}    #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += board_stm32l152_discovery.o  # ttc-lib/board/board_stm32l152_discovery.c

COMPILE_OPTS += -DTTC_BOARD1=E_ttc_board_architecture_stm32l152_discovery

#X COMPILE_OPTS += -DTTC_BOARD=$Name
#X BOARD=$Name

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32l152rbt6 = 1

#{ Define port pins
  # reference ../../Documentation/Boards/Board_STM32-L1Disc.pdf

  #{LEDs
  COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_b7
  COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_b6
  #}

  #{Buttons
  #COMPILE_OPTS += -DTTC_SWITCH1=PIN_NRST                # press button switch 1 (named Tamper)
  #COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  #COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=1
  COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_a0                 # press button switch 2 (named WKUP)
  COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
  COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=0
  #}

  #{Oszillator
  COMPILE_OPTS += -DTTC_RTC_CLOCK_SOURCE_LSE           #Use external 32.768 kHz Crystal
  #}
  
  #{TODO: define pin configuration of Serial peripherals
  #{USART1
  COMPILE_OPTS += -DTTC_USART1=INDEX_USART1
  COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_b6   
  COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_b7   
  COMPILE_OPTS += -DTTC_USART1_RTS=E_ttc_gpio_pin_a12  
  COMPILE_OPTS += -DTTC_USART1_CTS=E_ttc_gpio_pin_a11 
  COMPILE_OPTS += -DTTC_USART1_CK=E_ttc_gpio_pin_a8
  #}
  
  #{USART2
  COMPILE_OPTS += -DTTC_USART2=INDEX_USART2
  COMPILE_OPTS += -DTTC_USART2_TX=E_ttc_gpio_pin_a2  
  COMPILE_OPTS += -DTTC_USART2_RX=E_ttc_gpio_pin_a3   
  COMPILE_OPTS += -DTTC_USART2_RTS=E_ttc_gpio_pin_a1  
  COMPILE_OPTS += -DTTC_USART2_CTS=E_ttc_gpio_pin_a0 
  COMPILE_OPTS += -DTTC_USART2_CK=E_ttc_gpio_pin_a4 
  #}
  
  #{USART3
  COMPILE_OPTS += -DTTC_USART3=INDEX_USART3
  COMPILE_OPTS += -DTTC_USART3_TX=E_ttc_gpio_pin_b10  
  COMPILE_OPTS += -DTTC_USART3_RX=E_ttc_gpio_pin_b11   
  COMPILE_OPTS += -DTTC_USART3_RTS=E_ttc_gpio_pin_b14  
  COMPILE_OPTS += -DTTC_USART3_CTS=E_ttc_gpio_pin_b13 
  COMPILE_OPTS += -DTTC_USART3_CK=E_ttc_gpio_pin_b12 

  #}
  

  #{SPI1 - define serial peripheral interface #1
    COMPILE_OPTS += -DTTC_SPI1=ttc_device_1          # 
    COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a12  # P2-19
    COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a11  # P2-20
    COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_b3   # P2-11
    COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_a15   # P2-16
  #}
  
  #{SPI2 - define serial peripheral interface #2
    COMPILE_OPTS += -DTTC_SPI2=ttc_device_2           # 
    COMPILE_OPTS += -DTTC_SPI2_MOSI=E_ttc_gpio_pin_b15  # P1-27
    COMPILE_OPTS += -DTTC_SPI2_MISO=E_ttc_gpio_pin_b14  # P1-26
    COMPILE_OPTS += -DTTC_SPI2_SCK=E_ttc_gpio_pin_b13   # P1-25
    COMPILE_OPTS += -DTTC_SPI2_NSS=E_ttc_gpio_pin_b12   # P1-24
  #}
  COMPILE_OPTS += -DTTC_I2C_AMOUNT=2
  #{ I2C1    - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b7    # P2-7
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b6    # P2-8
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=E_ttc_gpio_pin_b5  # P2-9
  #}I2C1
  
  #{ I2C2    - define inter-integrated circuit interface #2
  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2           # internal device being used for this port
    COMPILE_OPTS += -DTTC_I2C2_SDA=E_ttc_gpio_pin_b11   # P1-23
    COMPILE_OPTS += -DTTC_I2C2_SCL=E_ttc_gpio_pin_b10   # P1-22
    COMPILE_OPTS += -DTTC_I2C2_SMBAL=E_ttc_gpio_pin_b12 # P1-24
    #}I2C2
  #{CAN
  #}
  #{DMA
  COMPILE_OPTS += -DTTC_DMA1=ttc_device_1
  COMPILE_OPTS += -DTTC_NCHANNELS=TTC_NUMBER_OF_CHANNELS 
  #}
  #{USB
  #}
  #}

#} END Define port pins
  

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$ScriptName 2" "discovery board from ST Microelectronics with low power stm32l152rbt6, simple lcd and four sensor fields" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "${RANK_LOWLEVEL}_board_stm32l152_discovery" "$cASH_InfoText"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  # board_stm32l152_discovery_prepare() typically ist called ttc_board_prepare() during preparation of all extensions.
  # It should not be required to activate source.${RANK_LOWLEVEL}_*.c here.

  #X if [ -e \$Dir_Extensions/source.${DriverName}.c ]; then
  #X   createLink \$Dir_Extensions/source.${DriverName}.c \$Dir_ExtensionsActive/ '' QUIET
  #X fi

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers activate only if a certain file exists inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.${RANK_LOWLEVEL}_ethernet_ste101p -> allows activate.500_ethernet.sh to enable its low-level driver ${RANK_LOWLEVEL}_ethernet_ste101p.
  # enableFeature NAME  # creates extensions.active/feature.NAME
  # See activate_project.sh of a fresh created project for a list of all available features! 
  
  # enableFeature 450_ethernet_ste101p
  enableFeature 450_cpu_stm32l1xx

  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  # activate.500_ttc_memory.sh QUIET "\$ScriptName"
  
  #}
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# define devices to use in this regression
COMPILE_OPTS += -DTTC_BOARD1=E_ttc_board_architecture_stm32l152_discovery # Line generated automatically. Remove if not wanted!

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$ScriptName 2" "PLACE YOUR INFO HERE FOR $ScriptName" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  if [ -e \$Dir_Extensions/source.${Name}.c ]; then
    createLink \$Dir_Extensions/source.${Name}.c \$Dir_ExtensionsActive/ '' QUIET
  fi

#}
#{ ACTIVATE_SECTION_E provide default features required to compile this example

  # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.  

  # enableFeature 450_math_software_float   # enable math driver (single precision)
  # enableFeature 450_math_software_double  # enable math driver (double precision)

#}
#{ ACTIVATE_SECTION_F call other activate-scripts

#activate.500_ttc_gpio.sh QUIET "\$ScriptName"

#}
#{ ACTIVATE_SECTION_G copy source code into project folder if required

if [ ! -d regressions ]; then
  mkdir regressions
fi
ExampleFound=\`ls regressions/regression_${EXTENSION_NAME}_${ThisArchitecture}*\`
if [ "\$ExampleFound" == "" ]; then
  echo "copying missing regression_${EXTENSION_NAME}_${ThisArchitecture}*.."
  cp -v ~/Source/TheToolChain/Template/regressions/regression_${EXTENSION_NAME}_${ThisArchitecture}* regressions/
fi
#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ recommended: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}_${ThisArchitecture}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}_${ThisArchitecture}.h"
example_${EXTENSION_NAME}_${ThisArchitecture}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

# additional constant defines
#COMPILE_OPTS += -D

# define devices to use in this example
COMPILE_OPTS += -DTTC_BOARD1=E_ttc_board_architecture_stm32l152_discovery # Line generated automatically. Remove if not wanted!

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}_${ThisArchitecture}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$ScriptName 2" "Usage example for extension ${EXTENSION_NAME}_${ThisArchitecture}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/source.${Name}.c ]; then
  createLink \$Dir_Extensions/source.${Name}.c \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#{ ACTIVATE_SECTION_E provide default features required to compile this example

  # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.
  enableFeature $DriverName  # enable low-level driver to be tested

  # enableFeature 450_math_software_float   # enable math driver (single precision)
  # enableFeature 450_math_software_double  # enable math driver (double precision)

#}
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$ScriptName" # activate high-level driver

#}
#{ ACTIVATE_SECTION_G copy source code into project folder if required

if [ ! -d examples ]; then
  mkdir examples
fi
ExampleFound=\`ls examples/example_${EXTENSION_NAME}_${ThisArchitecture}*\`
if [ "\$ExampleFound" == "" ]; then
  echo "copying missing example_${EXTENSION_NAME}.."
  cp -v ~/Source/TheToolChain/Template/examples/example_${EXTENSION_NAME}_${ThisArchitecture}* examples/
fi
#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                               #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
