#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
#
#  Install script for a single Low-Level Driver
#  for usb devices.
#
#  Created from template _install_NN_TTC_DEVICE_ARCHITECTURE.sh revision 21 at 20140411 07:54:11 UTC
#
#  Supported Architecture: stm32f1xx
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Fabian Kröger
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$0 - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="stm32f1xx"
findFolderUpwards "extensions"
DirLL_Extensions="$FoundFolder"
dir "$ThisArchitecture"
cd "$ThisArchitecture"

if [ ! -e OK.Docs ];    then    #{ (download documentation)

  Get_Fails=""
  
  #cd ../../Documentation/uC/STM32F1xx
  # download doc-file + move to Documentation/ + create symbolic link into Documentation/ right here 
   File="CD00158241.pdf"
   getFile http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/ $File STM32USB-FS-Device_development_kit.pdf || Error="1"
   #createLink $Install_PathLL/$File ../../Documentation/uC/STM32F1xx/"$File"
   
  
  
  if [ "$Get_Fails" == "" ]; then #{ check get() inside scripts/installFuncs.sh 
    touch OK.Docs
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    #Version 4.0.0 Device Lib
    #http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/firmware/stsw-stm32121.zip
    Archive="stsw-stm32121.zip"
    
    URL_Prefix="http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/firmware/"
   
    getFile $URL_Prefix $Archive
    unZIP STM-USB_FS_DevKit $Archive || mv -f $Archive ${Archive}_bad
    rm current 2>/dev/null
    FullLibraryDir=`find ./  -maxdepth 1 -mindepth 1 -name STM32_USB-FS-Device_Lib_* -type d -print`
    if [ "$FullLibraryDir" == "" ]; then
      echo "$0 - ERROR: Cannot extract $Archive !"
      exit 10
    fi
    
    LibVersion="4.0.0"
    DeviceLibDir="STM32_USB-FS-Device_Lib_V$LibVersion"
   
    dir Library_USB
    cd Library_USB
    createLink ../${DeviceLibDir}/Libraries/STM32_USB-FS-Device_Driver/ Driver
    createLink ../$FullLibraryDir/Projects/Virtual_COM_Port             VCP

  #createLink ../$FullLibraryDir/                                      Complete
  #createLink ../$FullLibraryDir/Libraries/STM32_USB_HOST_Library/     Host
  #createLink ../$FullLibraryDir/Libraries/STM32_USB_OTG_Driver/       OTG
  #cd ..
   
   
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    Install_PathLL=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for usb on stm32f1
  
    DriverName="450_usb_stm32f1xx_vcp"
    createExtensionMakefileHead ${DriverName}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 
  ############Configuration Files ${Name} ################
      ###############required Paths  #################
      INCLUDE_DIRS +=  -I ttc-lib/usb/ \\
      -I ttc-lib/usb/virtual_com_port/ \\
      -I ttc-lib/usb/virtual_com_port/inc/  
              vpath %.c 	ttc-lib/usb/ \\
         ttc-lib/usb/virtual_com_port/ \\
        ttc-lib/usb/virtual_com_port/src/ 

      ############Configuration Files ################
      ###############required Objects #################
      MAIN_OBJS += 	hw_config.o 

      MAIN_OBJS += 	usb_desc.o \\
                    usb_endp.o \\
                    usb_istr.o \\
                    usb_prop.o \\
                    usb_pwr.o \\
		
      #activate this to use your own device descriptor		
      COMPILE_OPTS += -DVirtual_Com_PortDevice_Descriptor_external

      # append some object files to be compiled
      //MAIN_OBJS += example_usb_vcp.o

      
          # additional constant defines
      #COMPILE_OPTS += -D
      
      # additional directories to search for header files
      INCLUDE_DIRS += -Ittc-lib/usb/
      
      # additional directories to search for C-Sources
      vpath %.c ttc-lib/usb/
      
      # additional object files to be compiled
      #MAIN_OBJS += ttc_usb_stm32f1xx.o
MAIN_OBJS += usb_stm32f1xx.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    findFolderUpwards "extensions"
    DirLL_Extensions="$FoundFolder"
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "low-level usb Driver for stm32f1 architecture" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
   \$Dir_Extensions        -> extensions/
   \$Dir_ExtensionsLocal   -> extensions.local/
   \$Dir_ExtensionsActive  -> extensions.active/
   \$Dir_Additionals       -> additionals/
   \$Dir_Configs           -> configs/

# Low-Level Driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "400_stm32_usb_fs_device_lib" "USB Filesystem Device Library"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  # ACTIVATE_SECTION_A remove activated variants of same type
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  # ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  # ACTIVATE_SECTION_D call other activate-scripts
  # activate.500_ttc_memory.sh QUIET "\$0"
  
activate.250_CPAL_STM32F10x_StdPeriph_Driver.sh QUIET \"\$0\" || exit 10
activate.250_stm_std_peripherals__rcc.sh        QUIET \"\$0\" || exit 10
activate.250_stm_std_peripherals__usart.sh      QUIET \"\$0\" || exit 10
//activate.400_stm32_usb_fs_device_lib.sh         QUIET \"\$0\" || exit 10
activate.500_ttc_usart.sh                       QUIET \"\$0\" || exit 10
activate.500_ttc_usb.sh                         QUIET \"\$0\" || exit 10
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
     addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o







END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${Install_Dir}.sh QUIET "\$0"


END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ optional: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${Install_Dir}.sh QUIET "\$0"

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  echo "Installed successfully: $DriverName"
#}
else                            #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------


