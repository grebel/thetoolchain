#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a single Low-Level Driver
#  for usb devices.
#
#  Created from template _install_NN_TTC_DEVICE_ARCHITECTURE.sh revision 22 at 20140512 13:58:16 UTC
#
#  Supported Architecture: stm32f1xx
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$0 - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="stm32f1xx"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards "extensions"
DirLL_Extensions="$FoundFolder"

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    Archive="stsw-stm32121.zip"
    
    URL_Prefix="http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/firmware/"
  
    getFile $URL_Prefix $Archive
    
    unZIP STM-USB_FS_DevKit $Archive || mv -f $Archive ${Archive}_bad
    rm current 2>/dev/null
    FullLibraryDir=`find ./  -maxdepth 1 -mindepth 1 -name STM32_USB-FS-Device_Lib_* -type d -print`
    if [ "$FullLibraryDir" == "" ]; then
      echo "$0 - ERROR: Cannot extract $Archive !"
      exit 10
    fi
    
    LibVersion="4.0.0"
    DeviceLibDir="STM32_USB-FS-Device_Lib_V$LibVersion"
    
    dir Library_USB
    cd Library_USB
    createLink ../${DeviceLibDir}/Libraries/STM32_USB-FS-Device_Driver  Core_Driver
    createLink ../$FullLibraryDir/Projects/Virtual_COM_Port             VCP_Driver
    cd VCP_Driver 
    rm -r EWARM
    rm -r MDK-ARM
    rm -r RIDE
    rm -r TASKING
    rm -r TrueSTUDIO
    echo "removed unnessesary stuff"
    cd ..
    File="Core_Driver/inc/usb_type.h"
      if [ ! -e ${File}_orig ]; then
          echo "patching ${File}.."
            mv -v ${File} ${File}_orig
              cat <<END_OF_SOURCE >$File #{
#ifndef __USB_TYPE_H
#define __USB_TYPE_H

/*********************************************************************
 *
 * The ToolChain 
 * written 2012 by Gregor Rebel
 *
 * Basic definitions required by USB-Library from STMicroelectronics.
 * Based on ${File}_orig
 *
 * $URL_Prefix$Archive
 *
 */


/* Includes ------------------------------------------------------------------*/
//#include "usb_conf.h"
#include "ttc-lib/ttc_basic.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#ifndef NULL
#define NULL ((void *)0)
#endif

#if !defined (__STM32F10x_H) && !defined(__STM32L1XX_H)

/*
typedef signed long      s32;
typedef signed short     s16;
typedef signed char      s8;

typedef volatile signed long      vs32;
typedef volatile signed short     vs16;
typedef volatile signed char      vs8;

typedef unsigned long       u32;
typedef unsigned short      u16;
typedef unsigned char       u8;

typedef unsigned long  const    uc32;  
typedef unsigned short const    uc16;  
typedef unsigned char  const    uc8;   

typedef volatile unsigned long      vu32;
typedef volatile unsigned short     vu16;
typedef volatile unsigned char      vu8;

typedef volatile unsigned long  const    vuc32;  
typedef volatile unsigned short const    vuc16;  
typedef volatile unsigned char  const    vuc8;   

typedef enum { RESET = 0, SET   = !RESET } FlagStatus, ITStatus;

typedef enum { DISABLE = 0, ENABLE  = !DISABLE} FunctionalState;

typedef enum { ERROR = 0, SUCCESS  = !ERROR} ErrorStatus;

*/
#endif /* __STM32F10x_H && __STM32L1XX_H */


#endif

END_OF_SOURCE
#}
         
              else
              echo "already patched: `pwd`/${File}"
              fi
  # remove defines from file from hw_config.c
  # USART_InitTypeDef USART_InitStructure;
  # replace in hw_config.c:
  # t_u8
  # t_u32
  pwd
   replaceInFile VCP_Driver/src/hw_config.c  "uint8_t" "t_u8"
   replaceInFile VCP_Driver/src/hw_config.c  "uint32_t" "t_u32"
   replaceInFile VCP_Driver/inc/usb_prop.h   "uint8_t" "t_u8"
   replaceInFile VCP_Driver/inc/usb_prop.h   "uint16_t" "t_u16" 
   replaceInFile VCP_Driver/inc/usb_prop.h   "uint32_t" "t_u32" 
   replaceInFile VCP_Driver/src/usb_prop.c   "uint8_t" "t_u8"
   replaceInFile VCP_Driver/src/usb_prop.c   "uint16_t" "t_u16" 
   replaceInFile VCP_Driver/src/usb_prop.c   "uint32_t" "t_u32" 
   
   replaceInFile VCP_Driver/inc/usb_desc.h   "uint8_t" "t_u8"
   replaceInFile VCP_Driver/inc/usb_desc.h   "uint16_t" "t_u16" 
   replaceInFile VCP_Driver/inc/usb_desc.h   "uint32_t" "t_u32" 
   replaceInFile VCP_Driver/src/usb_desc.c   "uint8_t" "t_u8"
   replaceInFile VCP_Driver/src/usb_desc.c   "uint16_t" "t_u16" 
   replaceInFile VCP_Driver/src/usb_desc.c   "uint32_t" "t_u32" 
   
   
   
   replaceInFile VCP_Driver/inc/usb_pwr.h   "uint8_t" "t_u8"
   replaceInFile VCP_Driver/inc/usb_pwr.h   "uint32_t" "t_u32" 
   replaceInFile VCP_Driver/src/usb_pwr.c   "uint8_t" "t_u8"
   replaceInFile VCP_Driver/src/usb_pwr.c   "uint32_t" "t_u32" 
   
   replaceInFile VCP_Driver/src/usb_istr.c   "uint8_t" "t_u8"
   replaceInFile VCP_Driver/src/usb_istr.c   "uint16_t" "t_u16" 
   replaceInFile VCP_Driver/src/usb_istr.c   "uint32_t" "t_u32" 
   
   
   
   replaceInFile Core_Driver/inc/usb_mem.h   "uint8_t"  "t_u8"
   replaceInFile Core_Driver/inc/usb_mem.h   "uint16_t" "t_u16"
   replaceInFile Core_Driver/inc/usb_mem.h   "uint32_t" "t_u32"
   
   replaceInFile Core_Driver/src/usb_mem.c   "uint8_t"  "t_u16"
   replaceInFile Core_Driver/src/usb_mem.c   "uint16_t" "t_u16"
   replaceInFile Core_Driver/src/usb_mem.c   "uint32_t" "t_u32"
   
   replaceInFile Core_Driver/inc/usb_regs.h  "uint8_t"  "t_u8" 
   replaceInFile Core_Driver/inc/usb_regs.h  "uint16_t" "t_u16"
   
   replaceInFile Core_Driver/src/usb_regs.c  "uint8_t"  "t_u8" 
   replaceInFile Core_Driver/src/usb_regs.c  "uint16_t" "t_u16"
   
   replaceInFile Core_Driver/src/usb_sil.c   "uint8_t"  "t_u8" 
   replaceInFile Core_Driver/src/usb_sil.c   "uint16_t" "t_u16"
   replaceInFile Core_Driver/src/usb_sil.c   "uint32_t" "t_u32"
   
   replaceInFile Core_Driver/inc/usb_sil.h   "uint8_t"  "t_u8" 
   replaceInFile Core_Driver/inc/usb_sil.h   "uint16_t" "t_u16"
   replaceInFile Core_Driver/inc/usb_sil.h   "uint32_t" "t_u32"
   
   replaceInFile Core_Driver/src/usb_core.c  "uint8_t"  "t_u8" 
   replaceInFile Core_Driver/src/usb_core.c  "uint16_t" "t_u16"
   replaceInFile Core_Driver/src/usb_core.c  "uint32_t" "t_u32"
   
   replaceInFile Core_Driver/inc/usb_core.h  "uint8_t"  "t_u8" 
   replaceInFile Core_Driver/inc/usb_core.h  "uint16_t" "t_u16"
   replaceInFile Core_Driver/inc/usb_core.h  "uint32_t" "t_u32"
   
   replaceInFile Core_Driver/inc/usb_int.h   "uint8_t"  "t_u8" 
   replaceInFile Core_Driver/inc/usb_int.h   "uint16_t" "t_u16"
   replaceInFile Core_Driver/inc/usb_int.h   "uint32_t" "t_u32"
   
   
   
   
   replaceInFile Core_Driver/inc/usb_init.h "uint16_t" "t_u16"
   cd ..
   
  pwd
   
  # 
  # replaceInFile $FullLibraryDir/Libraries/STM32_USB_OTG_Driver/inc/usb_regs.h "t_u32 "     "unsigned "
  # replaceInFile $FullLibraryDir/Libraries/STM32_USB_OTG_Driver/inc/usb_regs.h "unsigned d32"  "t_u32 d32"
  # replaceInFile $FullLibraryDir/Libraries/STM32_USB_OTG_Driver/inc/usb_regs.h "__IO unsigned" "__IO t_u32"
  # 
  # replaceInFile $FullLibraryDir/Libraries/STM32_USB_HOST_Library/Core/src/usbh_core.c "String(" "String((void*)"
  #createLink ../../$FullLibraryDir/                                      Complete
  #createLink ../../$FullLibraryDir/Libraries/STM32_USB_HOST_Library/     Host
  #createLink ../../$FullLibraryDir/Libraries/STM32_USB_OTG_Driver/       OTG
  #cd ..
  #cd ..
  
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for usb on stm32f1xx
  
    DriverName="450_usb_stm32f1xx"
    createExtensionMakefileHead ${DriverName}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += usb_stm32f1xx.o
############Configuration Files ${Name} ################
      ###############required Paths  #################
      INCLUDE_DIRS +=  -I ttc-lib/usb/
      INCLUDE_DIRS +=  -I additionals/500_ttc_usb/Core_Driver/inc
      INCLUDE_DIRS +=  -I additionals/500_ttc_usb/VCP_Driver/inc
      vpath %.c 	additionals/500_ttc_usb/Core_Driver/src
      vpath %.c 	additionals/500_ttc_usb/VCP_Driver/src
      vpath %.c 	ttc-lib/usb/ 
      ############Configuration Files ################
      ###############required Objects #################
      MAIN_OBJS += 	hw_config.o \\
                    usb_desc.o \\
                    usb_endp.o \\
                    usb_istr.o \\
                    usb_prop.o \\
                    usb_pwr.o \\
                    usb_regs.o \\
                    usb_core.o \\
                    usb_sil.o \\
                    usb_init.o \\
                    usb_int.o \\
                    usb_mem.o \\
                    
      #activate this to use your own device descriptor		
      //COMPILE_OPTS += -DVirtual_Com_PortDevice_Descriptor_external
      

      # append some object files to be compiled
      //MAIN_OBJS += example_usb_vcp.o

      
          # additional constant defines
      #COMPILE_OPTS += -D
      
    
END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$0 2" "low-level usb Driver for stm32f1xx architecture" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level Driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "450_cpu_stm32f1xx"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  # ACTIVATE_SECTION_A remove activated variants of same type
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  # ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  # ACTIVATE_SECTION_D call other activate-scripts
  # activate.500_ttc_memory.sh QUIET "\$0"
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  fi #}low-level driver
  if [ "1" == "1" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
#activate.500_ttc_gpio.sh QUIET "\$0"


END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ optional: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${Install_Dir}.sh QUIET "\$0"

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                            #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
