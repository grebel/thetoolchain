#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
#
#  Install script for a single Low-Level Driver
#  for board devices.
#
#  Created from template _install_TTC_DEVICE_ARCHITECTURE.sh revision 37 at 20180119 01:41:26 UTC
#
#  Supported Architecture: olimex_stm32_lcd
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: Gregor Rebel
#

if [ "$EXTENSION_NAME" == "" ]; then #{ ERROR: globals must be defined by high-level install script!
  echo "`pwd`/$ScriptName - ERROR: Script must be called from high-level install script!"
  return 10
fi #}

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

# create sub-directory for each low-level driver
MyStartDir=`pwd`
ThisArchitecture="olimex_stm32_lcd"
dir "$ThisArchitecture"
cd "$ThisArchitecture"
findFolderUpwards "extensions"
findFolderUpwards extensions; DirLL_Extensions="$MyStartDir/$ThisArchitecture/$FoundFolder"
findFolderUpwards scripts;    Start_Path="$MyStartDir/$ThisArchitecture/${FoundFolder}.."

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$ScriptName: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$ScriptName - ERROR: Cannot find newest GCC version!"
#   return 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://olimex.com/dev/pdf/ARM/ST/                           STM32-LCD.pdf                   Board_STM32-LCD.pdf          Boards/Olimex_STM32-LCD
  getDocumentation http://www.olimex.com/dev/pdf/PIC/                          "FS-K320QVB-V1-01-3T%20(3).pdf" LCD_FS-K320QVB-V1-01-3T.pdf  Boards/Olimex_STM32-LCD
  getDocumentation https://www.olimex.com/Products/ARM/ST/STM32-LCD/resources/ ILI9325_new_displays.pdf        ILI9325_new_displays.pdf     Boards/Olimex_STM32-LCD
  getDocumentation http://olimex.com/dev/                                      stm32-lcd.html                  stm32-lcd.html               Boards/Olimex_STM32-LCD
  getDocumentation https://www.olimex.com/Products/ARM/ST/STM32-LCD/resources/ STM32-LCD_demo.zip              STM32-LCD_demo.zip           Boards/Olimex_STM32-LCD
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then       #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir/$ThisArchitecture ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then       #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ low-level driver for board on olimex_stm32_lcd
  
    DriverName="${RANK_LOWLEVEL}_board_olimex_stm32_lcd"
    createExtensionSourcefileHead ${DriverName}  #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile


END_OF_SOURCEFILE
    createExtensionSourcefileTail ${DriverName} #}    
    createExtensionMakefileHead ${DriverName}    #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

MAIN_OBJS += board_olimex_stm32_lcd.o  # ttc-lib/board/board_olimex_stm32_lcd.c

COMPILE_OPTS += -DTTC_BOARD1=E_ttc_board_architecture_olimex_stm32_lcd

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f103zet6 = 1

# system clock setup (-> ttc_sysclock_types.h)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=8000000
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768

# Define port pins
# reference: http://olimex.com/dev/ARM/ST/STM32-LCD/STM32-LCD-schematic.pdf
#{ ADC     - define Analog Input Pins

COMPILE_OPTS += -DTTC_ADC1=E_ttc_gpio_pin_c4 # pin 38 on connector UEXT40
COMPILE_OPTS += -DTTC_ADC2=E_ttc_gpio_pin_c5 # pin 40 on connector UEXT40

#}
#{ parallel ports (8 or 16 consecutive pins in same bank)
#
# Every 8 available, consecutive bits in same GPIO bank can form an 8-bit wide port.
# A 16-bit wide port requires 16 available, consecutive bits (obviously).
# A pin is available if it not driven externally or used for something else.
# Check circuit of your board for details.

# 16-bit wide parallel port #1 using whole bank F (16-bit always starts a pin 0)
COMPILE_OPTS += -DTTC_GPIO_PARALLEL16_1_BANK=TTC_GPIO_BANK_F

# 16-bit wide parallel port #2 using whole bank G (16-bit always starts a pin 0)
COMPILE_OPTS += -DTTC_GPIO_PARALLEL16_2_BANK=TTC_GPIO_BANK_G


# 8-bit wide parallel port #1 starting at pin 0 of bank F
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_BANK=TTC_GPIO_BANK_F
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_FIRSTPIN=0

# 8-bit wide parallel port #2 starting at pin 8 of bank F
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_BANK=TTC_GPIO_BANK_F
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_FIRSTPIN=8

# 8-bit wide parallel port #3 starting at pin 0 of bank G
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_3_BANK=TTC_GPIO_BANK_G
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_3_FIRSTPIN=0

# 8-bit wide parallel port #4 starting at pin 8 of bank G
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_4_BANK=TTC_GPIO_BANK_G
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_4_FIRSTPIN=8

#}parallel ports
#{ USART1  - define universal synchronous asynchronous receiver transmitter #1
COMPILE_OPTS += -DTTC_USART1=INDEX_USART1       # RS232 on 10-pin male header connector UEXT1 connected USART
COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_a9   # UEXT1-3, UEXT40-3
COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_a10  # UEXT1-4, UEXT40-4
COMPILE_OPTS += -DTTC_USART1_CK=E_ttc_gpio_pin_a8   # EXT-40
#}USART1
#{ USART2  - define universal synchronous asynchronous receiver transmitter #2
  COMPILE_OPTS += -DTTC_USART2=INDEX_USART2      # RS232 on 10-pin male header connector UEXT2 connected USART
  COMPILE_OPTS += -DTTC_USART2_TX=E_ttc_gpio_pin_a2  # UEXT2-3
  COMPILE_OPTS += -DTTC_USART2_RX=E_ttc_gpio_pin_a3  # UEXT2-4
  COMPILE_OPTS += -DTTC_USART2_CK=E_ttc_gpio_pin_a4  # UEXT1-10
#}USART2
#{ SPI1    - define serial peripheral interface #1
  COMPILE_OPTS += -DTTC_SPI1=ttc_device_1          # 
  COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a7  # UEXT1-8,  UEXT40-8
  COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a6  # UEXT1-7,  UEXT40-7
  COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a5   # UEXT1-9,  UEXT40-9
  COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_a4   # UEXT1-10, UEXT40-10
#}SPI1
#{ SPI2    - define serial peripheral interface #2
  COMPILE_OPTS += -DTTC_SPI2=ttc_device_2          # 
  COMPILE_OPTS += -DTTC_SPI2_MOSI=E_ttc_gpio_pin_b15 # UEXT1-8
  COMPILE_OPTS += -DTTC_SPI2_MISO=E_ttc_gpio_pin_b14 # UEXT1-7
  COMPILE_OPTS += -DTTC_SPI2_SCK=E_ttc_gpio_pin_b13  # UEXT1-9
  COMPILE_OPTS += -DTTC_SPI2_NSS=E_ttc_gpio_pin_b12  # UEXT2-10
#}SPI2
#{ I2C1    - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_AMOUNT_I2CS=3       #
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b7    # UEXT2-5/ UEXT40-6
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b6    # UEXT2-6/ UEXT40-5
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=E_ttc_gpio_pin_b5  # EXT-10
#}I2C1
#{ I2C2    - define inter-integrated circuit interface #2
  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C2_SDA=E_ttc_gpio_pin_b11   # connected to accelerometer
  COMPILE_OPTS += -DTTC_I2C2_SCL=E_ttc_gpio_pin_b10   # connected to accelerometer
#}I2C2
#{ CAN1    - define Controller Area Network (CAN) devices
  COMPILE_OPTS += -DTTC_CAN1=0               # physical device index
  COMPILE_OPTS += -DTTC_CAN1_PIN_RX=E_ttc_gpio_pin_b8  # pin used for receive
  COMPILE_OPTS += -DTTC_CAN1_PIN_TX=E_ttc_gpio_pin_b9  # pin used for transmit
#}CAN

#{ define pin configuration of GPIOs on the UEXT1 Conncector
  COMPILE_OPTS += -DUEXT1_USART_TX=E_ttc_gpio_pin_a9
  COMPILE_OPTS += -DUEXT1_USART_RX=E_ttc_gpio_pin_a10
  COMPILE_OPTS += -DUEXT1_I2C_SCL=E_ttc_gpio_pin_b6
  COMPILE_OPTS += -DUEXT1_I2C_SDA=E_ttc_gpio_pin_b7
  COMPILE_OPTS += -DUEXT1_SPI_MISO=E_ttc_gpio_pin_a6
  COMPILE_OPTS += -DUEXT1_SPI_MOSI=E_ttc_gpio_pin_a7
  COMPILE_OPTS += -DUEXT1_SPI_SCK=E_ttc_gpio_pin_a5
  COMPILE_OPTS += -DUEXT1_SPI_NSS=E_ttc_gpio_pin_a4
  
  COMPILE_OPTS += -DUEXT1_SPI_INDEX=1
  COMPILE_OPTS += -DUEXT1_USART_INDEX=1
  COMPILE_OPTS += -DUEXT1_I2C_INDEX=1
#}
#{ define pin configuration of GPIOs on the UEXT2 Conncector
  COMPILE_OPTS += -DUEXT2_USART_TX=E_ttc_gpio_pin_a3
  COMPILE_OPTS += -DUEXT2_USART_RX=E_ttc_gpio_pin_a2
  COMPILE_OPTS += -DUEXT2_I2C_SCL=E_ttc_gpio_pin_b8
  COMPILE_OPTS += -DUEXT2_I2C_SDA=E_ttc_gpio_pin_b9
  COMPILE_OPTS += -DUEXT2_SPI_MISO=E_ttc_gpio_pin_b14
  COMPILE_OPTS += -DUEXT2_SPI_MOSI=E_ttc_gpio_pin_b15
  COMPILE_OPTS += -DUEXT2_SPI_SCK=E_ttc_gpio_pin_a13
  COMPILE_OPTS += -DUEXT2_SPI_NSS=E_ttc_gpio_pin_a12
  COMPILE_OPTS += -DUEXT2_SPI_INDEX=2
  COMPILE_OPTS += -DUEXT2_USART_INDEX=2
  COMPILE_OPTS += -DUEXT2_I2C_INDEX=1
#}
#{ ACCELEROMETER MPU6050
 COMPILE_OPTS += -DTTC_ACCELEROMETER1=ttc_device_1
#}
#{ GYROSCOPE
COMPILE_OPTS += -DTTC_GYROSCOPE1=ttc_device_1
#}
#{ LCD Graphic Display 320x240 using ILI93xx controller

  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D00=E_ttc_gpio_pin_d14
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D01=E_ttc_gpio_pin_d15
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D02=E_ttc_gpio_pin_d0
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D03=E_ttc_gpio_pin_d1
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D04=E_ttc_gpio_pin_e7
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D05=E_ttc_gpio_pin_e8
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D06=E_ttc_gpio_pin_e9
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D07=E_ttc_gpio_pin_e10
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D08=E_ttc_gpio_pin_e11
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D09=E_ttc_gpio_pin_e12
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D10=E_ttc_gpio_pin_e13
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D11=E_ttc_gpio_pin_e14
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D12=E_ttc_gpio_pin_e15
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D13=E_ttc_gpio_pin_d8
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D14=E_ttc_gpio_pin_d9
  COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_D15=E_ttc_gpio_pin_d10

#}LCD

# ttc_gfx driver settings for board Olimex-LCD
# reference: http://olimex.com/dev/ARM/ST/STM32-LCD/STM32-LCD-schematic.pdf

# LCD-Display 320x240x24 - define settings required by low-level driver to run on current board
COMPILE_OPTS += -DTTC_GFX1=1
COMPILE_OPTS += -DTTC_GFX1_WIDTH=240
COMPILE_OPTS += -DTTC_GFX1_HEIGHT=320
COMPILE_OPTS += -DTTC_GFX1_DEPTH=24
COMPILE_OPTS += -DTTC_GFX1_DRIVER=tgd_ILI93xx    # one from ttc_gfx_types.h:e_ttc_gfx_driver
COMPILE_OPTS += -DTTC_GFX_STM32_FSMC=1

# define gpio pins connected to ili93xx gfx controller (->lcd_ili93xx.h)
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_NOT_CHIPSELECT=E_ttc_gpio_pin_d7
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_NOT_WRITE_STROBE=E_ttc_gpio_pin_d5
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_REGISTER_SELECT=E_ttc_gpio_pin_e3
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_NOT_RESET=E_ttc_gpio_pin_e2
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_LCDLED=E_ttc_gpio_pin_d13
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_NBL0=E_ttc_gpio_pin_e0
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_NBL1=E_ttc_gpio_pin_e1
COMPILE_OPTS += -DTTC_GFX1_ILI93XX_PIN_NOT_READ_STROBE=E_ttc_gpio_pin_d4

END_OF_MAKEFILE
    createExtensionMakefileTail ${DriverName} #}
    createActivateScriptHead $DriverName $DirLL_Extensions/ "$ScriptName 2" "prototype board olimex stm32 lcd with 320x240 pixel color touch display and 144 pin stm32f103" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# Low-Level driver has to check if required architecture driver has been activated before.
# This is done by checking existence of a corresponding makefile inside \$Dir_Extensions/.
#
checkFeature "${RANK_LOWLEVEL}_board_olimex_stm32_lcd" "$cASH_InfoText"
if [ "\$?" == "1" ]; then #{ architecture for this driver has been activated: activate low-level driver 
  #{ ACTIVATE_SECTION_A remove activated variants of same type
  
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*
  
  #}
  #  ACTIVATE_SECTION_B no low-level drivers available
  #{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
  
  createLink \$Dir_Extensions/makefile.$DriverName \$Dir_ExtensionsActive/makefile.$DriverName '' QUIET
  
  #}
  #{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  # board_olimex_stm32_lcd_prepare() typically ist called ttc_board_prepare() during preparation of all extensions.
  # It should not be required to activate source.${RANK_LOWLEVEL}_*.c here.

  #X if [ -e \$Dir_Extensions/source.${DriverName}.c ]; then
  #X   createLink \$Dir_Extensions/source.${DriverName}.c \$Dir_ExtensionsActive/ '' QUIET
  #X fi

  #}
  #{ ACTIVATE_SECTION_E enable features provided by this extension
  #
  # Most low-level drivers activate only if a certain file exists inside extensions.active/.
  # This can be makefile.* or feature.* files. Feature files are empty files that simply allow to activate 
  # a corresponding extension.
  # The name of the feature file shall match the name of its corresponding extension.
  # E.g.: feature.${RANK_LOWLEVEL}_ethernet_ste101p -> allows activate.500_ethernet.sh to enable its low-level driver ${RANK_LOWLEVEL}_ethernet_ste101p.
  # enableFeature NAME  # creates extensions.active/feature.NAME
  # See activate_project.sh of a fresh created project for a list of all available features! 
  
  
  # enable features of this board (allows activation of low-level drivers)
  enableFeature 450_gfx_ili93xx                             # driver for graphic controller ILI93xx
  enableFeature 450_sdcard_sdio                             # driver for SDCARD connected via SDIO bus
  enableFeature 450_accelerometer_lis3lv02dl                # builtin 3-axis accelerometer connected via I2C bus
  enableFeature 450_touchpad_analog4                        # driver for 4 pin analog touchpad
  enableFeature 450_input_touchpad                          # driver for touchpad input devices
  enableFeature 450_cpu_cortexm3                            # Common driver for all microcontrollers with CortexM3 core
  enableFeature 450_cpu_stm32f1xx                           # Driver for STM32L1xx microcontrollers

  #}
  #{ ACTIVATE_SECTION_F call other activate-scripts
  
  activate.250_CPAL_STM32F10x_StdPeriph_Driver.sh  QUIET "\$ScriptName"
  activate.250_stm_std_peripherals__fsmc.sh        QUIET "\$ScriptName"

  #}
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $DriverName $DirLL_Extensions/
  #}
  
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ optional: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# define devices to use in this regression
COMPILE_OPTS += -DTTC_BOARD1=E_ttc_board_architecture_olimex_stm32_lcd # Line generated automatically. Remove if not wanted!

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$ScriptName 2" "PLACE YOUR INFO HERE FOR $ScriptName" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

  if [ -e \$Dir_Extensions/source.${Name}.c ]; then
    createLink \$Dir_Extensions/source.${Name}.c \$Dir_ExtensionsActive/ '' QUIET
  fi

#}
#{ ACTIVATE_SECTION_E provide default features required to compile this example

  # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.  

  # enableFeature 450_math_software_float   # enable math driver (single precision)
  # enableFeature 450_math_software_double  # enable math driver (double precision)

#}
#{ ACTIVATE_SECTION_F call other activate-scripts

#activate.500_ttc_gpio.sh QUIET "\$ScriptName"

#}
#{ ACTIVATE_SECTION_G copy source code into project folder if required

if [ ! -d regressions ]; then
  mkdir regressions
fi
ExampleFound=\`ls regressions/regression_${EXTENSION_NAME}_${ThisArchitecture}*\`
if [ "\$ExampleFound" == "" ]; then
  echo "copying missing regression_${EXTENSION_NAME}_${ThisArchitecture}*.."
  cp -v ~/Source/TheToolChain/Template/regressions/regression_${EXTENSION_NAME}_${ThisArchitecture}* regressions/
fi
#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}
  if [ "1" == "0" ]; then #{ recommended: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}_${ThisArchitecture}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "examples/example_${EXTENSION_NAME}_${ThisArchitecture}.h"
example_${EXTENSION_NAME}_${ThisArchitecture}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

# additional constant defines
#COMPILE_OPTS += -D

# define devices to use in this example
COMPILE_OPTS += -DTTC_BOARD1=E_ttc_board_architecture_olimex_stm32_lcd # Line generated automatically. Remove if not wanted!

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}_${ThisArchitecture}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name $DirLL_Extensions/ "$ScriptName 2" "Usage example for extension ${EXTENSION_NAME}_${ThisArchitecture}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

#{ ACTIVATE_SECTION_A remove activated variants of same type

# rm 2>/dev/null \$Dir_ExtensionsActive/*${Install_Dir}_*

#}
#  ACTIVATE_SECTION_B no low-level drivers available
#{ ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project

createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

#}
#{ ACTIVATE_SECTION_D activate initialization source-code for this extension 

if [ -e \$Dir_Extensions/source.${Name}.c ]; then
  createLink \$Dir_Extensions/source.${Name}.c \$Dir_ExtensionsActive/ '' QUIET
fi

#}
#{ ACTIVATE_SECTION_E provide default features required to compile this example

  # Read Documentation/TheToolChain-Manual/chapter_Extensions.tml for a description of feature enabling.
  enableFeature $DriverName  # enable low-level driver to be tested

  # enableFeature 450_math_software_float   # enable math driver (single precision)
  # enableFeature 450_math_software_double  # enable math driver (double precision)

#}
#{ ACTIVATE_SECTION_F call other activate-scripts

activate.${Install_Dir}.sh QUIET "\$ScriptName" # activate high-level driver

#}
#{ ACTIVATE_SECTION_G copy source code into project folder if required

if [ ! -d examples ]; then
  mkdir examples
fi
ExampleFound=\`ls examples/example_${EXTENSION_NAME}_${ThisArchitecture}*\`
if [ "\$ExampleFound" == "" ]; then
  echo "copying missing example_${EXTENSION_NAME}.."
  cp -v ~/Source/TheToolChain/Template/examples/example_${EXTENSION_NAME}_${ThisArchitecture}* examples/
fi
#}

END_OF_ACTIVATE
    createActivateScriptTail $Name $DirLL_Extensions/
    #}

  fi #}

  cd "$OldPWD"
  echo "Installed successfully: $DriverName"
#}
else                               #{ Install failed
  #echo "failed to install $DriverName"
  return 10
fi #}

cd "$MyStartDir"
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------
