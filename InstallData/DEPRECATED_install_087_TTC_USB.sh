#!/bin/bash
#
#
#  Install script for generic USART support 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Initial Script written by Gregor Rebel 2010-2012.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "500_ttc_usb" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

  Name="$Install_Dir"
  
  Driver_stm32f1xx="450_usb_stm32f1xx"
  
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

  # additional directories to search for header files
INCLUDE_DIRS += -Ittc-lib/usb/
  
# additional directories to search for C-Sources
vpath %.c ttc-lib/usb/ 
  
# additional object files to be compiled
MAIN_OBJS += ttc_usb.o


END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$0" "Architecture independent support for Universal Synchronous Asynchronous Serial Receiver Transmitter (USART)" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_Additionals/${Name}_*

Architecture=""
if [ -e extensions/activate.250_stm_std_peripherals__usart.sh ]; then
  activate.250_stm_std_peripherals__usart.sh QUIET \"\$0\"
  Architecture="stm32"
fi

if [ "\$Architecture" != "" ]; then
  activate.500_ttc_interrupt.sh QUIET \"\$0\"
  activate.500_ttc_gpio.sh QUIET \"\$0\"
  activate.500_ttc_queue.sh QUIET \"\$0\"
  activate.500_ttc_task.sh QUIET \"\$0\"
  activate.500_ttc_string.sh QUIET \"\$0\"
  activate.500_ttc_usart.sh QUIET \"\$0\"
  
  # create links into extensions.active/
  createLink "\${Dir_Extensions}/makefile.$Name" "\${Dir_ExtensionsActive}/makefile.$Name" '' QUIET
else
  echo "$0 - ERROR: no supported architecture found!"
fi

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  echo "Installed successfully: $Name"

  cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
