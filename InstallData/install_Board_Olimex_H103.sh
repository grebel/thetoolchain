#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="100"
EXTENSION_SHORT="olimex_h103"
EXTENSION_PREFIX="board"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://olimex.com/dev/pdf/ARM/ST/ STM32-H103.pdf Board_STM32-H103.pdf Boards/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
  Dir=`pwd`

  Name="${Install_Dir}" #{
  createExtensionMakefileHead ${Name} #{
  File="${Dir_Extensions}makefile.${Name}" 
  cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DTTC_BOARD=$Name
BOARD=$Name

# define microcontroller used on board
#X STM32F10X_MD  =1
#X COMPILE_OPTS += -DSTM32F10X_MD

# Define exactly one CPU variant used on current board
# See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
TTC_CPU_VARIANT_stm32f103rbt6 = 1 


# system clock setup (-> ttc_sysclock_types.h)
COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=8000000
#COMPILE_OPTS += -DTTC_SYSCLOCK_EXTERNAL_LOWSPEED_CRYSTAL=32768

#{ individual port pins

# reference: http://olimex.com/dev/images/ARM/ST/STM32-H103-sch.gif

COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_c12
COMPILE_OPTS += -DTTC_LED2=E_ttc_gpio_pin_c12      # using same LED as LED1

COMPILE_OPTS += -DTTC_SWITCH1=E_ttc_gpio_pin_a0    # press button switch 1 (named BUT)
COMPILE_OPTS += -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating # -> ttc_gpio_types.h
COMPILE_OPTS += -DTTC_SWITCH1_LOWACTIVE=1

#}
#{ parallel ports (8 or 16 consecutive pins in same bank)
#
# Every 8 available, consecutive bits in same GPIO bank can form an 8-bit wide port.
# A 16-bit wide port requires 16 available, consecutive bits (obviously).
# A pin is available if it not driven externally or used for something else.
# Check circuit of your board for details.

# 16-bit wide parallel port #1 using whole bank B (overlaps with JTDO, JRST)
COMPILE_OPTS += -DTTC_GPIO_PARALLEL16_1_BANK=TTC_GPIO_BANK_B

# 16-bit wide parallel port #2 using whole bank C (overlaps with USB_P, DISC, LED)
COMPILE_OPTS += -DTTC_GPIO_PARALLEL16_2_BANK=TTC_GPIO_BANK_C

# 8-bit wide parallel port #1 starting at pin 0 of bank C
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_BANK=TTC_GPIO_BANK_C
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_1_FIRSTPIN=0

# 8-bit wide parallel port #2 starting at pin 5 of bank B
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_BANK=TTC_GPIO_BANK_B
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_2_FIRSTPIN=5

# 8-bit wide parallel port #3 starting at pin 0 of bank A
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_3_BANK=TTC_GPIO_BANK_A
COMPILE_OPTS += -DTTC_GPIO_PARALLEL08_3_FIRSTPIN=0


#}parallel ports
#{ USART1  - define universal synchronous asynchronous receiver transmitter #1
  COMPILE_OPTS += -DTTC_USART1=INDEX_USART1       # 
  COMPILE_OPTS += -DTTC_USART1_TX=E_ttc_gpio_pin_a9   # EXT1-4
  COMPILE_OPTS += -DTTC_USART1_RX=E_ttc_gpio_pin_a10  # EXT1-7
  
  # Pins for RTS,CTS are also used for USBDM,USBDP!
  #COMPILE_OPTS += -DTTC_USART1_CTS=E_ttc_gpio_pin_a11 # EXT1-1
  #COMPILE_OPTS += -DTTC_USART1_RTS=E_ttc_gpio_pin_a12 # EXT1-3
#}USART1
#{ USART2  - define universal synchronous asynchronous receiver transmitter #2
  COMPILE_OPTS += -DTTC_USART2=INDEX_USART2       # RS232 connector connected USART
  COMPILE_OPTS += -DTTC_USART2_TX=E_ttc_gpio_pin_a2   # EXT2-7
  COMPILE_OPTS += -DTTC_USART2_RX=E_ttc_gpio_pin_a3   # EXT2-10
  COMPILE_OPTS += -DTTC_USART2_CTS=E_ttc_gpio_pin_a0  # EXT2-4
  COMPILE_OPTS += -DTTC_USART2_RTS=E_ttc_gpio_pin_a1  # EXT2-8
#}USART2
#{ SPI1    - define serial peripheral interface #1
  COMPILE_OPTS += -DTTC_SPI1=ttc_device_1          # 
  COMPILE_OPTS += -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a7  # 
  COMPILE_OPTS += -DTTC_SPI1_MISO=E_ttc_gpio_pin_a6  # 
  COMPILE_OPTS += -DTTC_SPI1_SCK=E_ttc_gpio_pin_a5   # 
  COMPILE_OPTS += -DTTC_SPI1_NSS=E_ttc_gpio_pin_a4   # 
#}SPI1
#{ I2C1    - define inter-integrated circuit interface #1
  COMPILE_OPTS += -DTTC_I2C1=ttc_device_1           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C1_SDA=E_ttc_gpio_pin_b7    # EXT1-15
  COMPILE_OPTS += -DTTC_I2C1_SCL=E_ttc_gpio_pin_b6    # EXT1-13
  COMPILE_OPTS += -DTTC_I2C1_SMBAL=E_ttc_gpio_pin_b5  # EXT1-12
#}I2C1
#{ I2C2    - define inter-integrated circuit interface #2
  COMPILE_OPTS += -DTTC_I2C2=ttc_device_2           # internal device being used for this port
  COMPILE_OPTS += -DTTC_I2C2_SDA=E_ttc_gpio_pin_b11   # EXT2-15
  COMPILE_OPTS += -DTTC_I2C2_SCL=E_ttc_gpio_pin_b10   # EXT2-14
  COMPILE_OPTS += -DTTC_I2C2_SMBAL=E_ttc_gpio_pin_b12 # EXT2-17
#}I2C2
#{ define Controller Area Network (CAN) devices
  COMPILE_OPTS += -DTTC_CAN1=0               # physical device index
  COMPILE_OPTS += -DTTC_CAN1_PIN_RX=E_ttc_gpio_pin_a11  # pin used for receive
  COMPILE_OPTS += -DTTC_CAN1_PIN_TX=E_ttc_gpio_pin_a12  # pin used for transmit
#}CAN

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Protoboard H103 from Olimex -> http://olimex.com/dev/stm32-h103.html"  #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
  
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
  
  #X activate.200_cpu_stm32f10x.sh QUIET "\$ScriptName"
  enableFeature 450_cpu_stm32f1xx


END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

#}
  echo "Installed successfully: $Install_Dir"

cd ..
rmdir 2>/dev/null $Install_Dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
