#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script Template written by Gregor Rebel 2010-2018.
#  install_28_STLink_V2.sh - changes from template written by Greg Knoll 2013.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="350"
EXTENSION_SHORT="stlink_v2"
EXTENSION_PREFIX="programmer"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions
Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$ScriptName: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$ScriptName - ERROR: Cannot find newest GCC version!"
#   exit 10
# fi #}
#}look for newest versions

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe 'which autoreconf' autoreconf
    installPackageSafe "find /lib/ -name libusb*" libusb-1.0
    installPackageSafe "find /lib/ -name libusb*" libusb-1.0.0 # On KUbuntu 16.04, the libusb package suddenly has been renamed libusb-1.0 -> libusb-1.0.0
    installPackageSafe "which pkg-config" pkg-config
    installPackageSafe "which autoreconf" autoreconf
    installPackageSafe "which autoconf"   autoconf 
    installPackageSafe "which aclocal"    aclocal
    installPackageSafe "which automake"   automake
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    Archive_STLink="stlink.tgj"
    
    getFile http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/driver/ stsw-link009.zip stsw-link009_Windows_USB-Driver.zip
    getFile http://thetoolchain.com/mirror/ $Archive_STLink # loading git snapshot of tested version from TTC-mirror to reduce load on original git repository 
    # Note: Disable this line and delete file ~/Source/TheToolChain.Contents/stlink.tgj to force cloning newest git version!
    
    rm -Rf stlink
    if [ ! -d "stlink" ]; then
      if [ -e $Archive_STLink ]; then
        untgj "stlink" $Archive_STLink
      fi
      if [ -d "stlink" ]; then
        echo "Successfully downloaded Git snapshot of stlink from mirror website"
      else
        echo "Could not download git snapshot from mirror website. Will clone from original git repository.."
        git clone git://github.com/texane/stlink.git
      fi
    fi
    cd "stlink"
     echo "compiling in `pwd`"
    ./autogen.sh &&  ./configure &&  make  && sudo make install #chained compile commands (if arg to left of && doesn't succeed, arg on right not run)
    cp README STLink.readme
    chmod 644 STLink.readme
    cd ..
    
    Binary="stlink/st-util"
    if [ -e "$Binary" ]; then
      sudo cp stlink/49-stlink*rules /etc/udev/rules.d/    #so that OS recognizes STLink
      sudo udevadm control --reload-rules		   #reset OS uDev
      addDocumentationFile stlink/STLink.readme Programmer/
      
      if [ ! -e $Archive_STLink ]; then
        cd stlink 
        make clean
        cd ..
        echo "compressing stlink git to archive $Archive_STLink for upload to mirror site.."
        tar cjf $Archive_STLink stlink
        echo "You may now upload this file to mirror website: `pwd`/$Archive_STLink"
        echo ""
      fi
      
      echo "" >OK.Install
    else
      echo "$ScriptName - ERROR: missing file '$Binary' in `pwd`"
    fi
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts

  createLink "../$Install_Dir/stlink/st-util" "../bin/st-util"  #create link in bin to st-util (used by debug_stlinkv2.sh)
  createLink "../$Install_Dir/stlink/st-flash" "../bin/st-flash"  #create link in bin to st-util (used by flash_stlinkv2.sh)

  Name="${Install_Dir}"  
  createExtensionMakefileHead ${Name}      #{ (create makefile)
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
#MAIN_OBJS += 

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "STLinkV2 programmer/debugger using stlink software" #{ (create activate script)
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# ACTIVATE_SECTION_A remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$ScriptName"

# update link to current flash script
createLink flash_stlinkv2.sh _/flash_current.sh
createLink debug_stlinkv2.sh _/debug_current.sh


END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  
  # (addLine) 
  # create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
  #
  # Activate line below and change SUBFOLDER to your needs
  #addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"


  if [ "1" == "0" ]; then #{ create regression test for your extension
  
    Name="${Install_Dir}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

//  #include "YOUR_SOURCE_HERE.h"
//  YOUR_START_HERE();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
#MAIN_OBJS += 

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "PLACE YOUR INFO HERE FOR $ScriptName" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$ScriptName"


END_OF_ACTIVATE
    createActivateScriptTail $Name ${Dir_Extensions}
    #}

  fi #}
    
  cd "$OldPWD"
  echo "Installed successfully: $Install_Dir"
#}
else                            #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
