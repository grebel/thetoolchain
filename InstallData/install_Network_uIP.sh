#!/bin/bash

#
#  Install script for Ethernet module STE101P on Olimex prototype board STM32-P107.
#  URL: http://olimex.com/dev/stm32-p107.html
#
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="400"
EXTENSION_SHORT="uip_ste101p"
EXTENSION_PREFIX="network"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  #getDocumentation 'http://www.sics.se/~adam/download/?f=' uip-1.0-refman.pdf "TCP-IP_Stack_uIP_Manual.pdf" Network
  getFile https://github.com/adamdunkels/uip/archive/ uip-1-0.tar.gz uip-1-0.tar.gz
  untgz "uip-uip-1-0" uip-1-0.tar.gz
  addDocumentationFile uip-uip-1-0/doc/uip-refman.pdf Network
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then #{
  echo "installing in $Install_Dir ..."
  Install_Path=`pwd`
  
  InstallInHomeDir="1"
  if [ "$InstallInHomeDir" == "1" ]; then
    Prefix=`pwd`  # install into subfolder of InstallData/ for current user only
  else
    findPrefix    # install for all users
  fi
  
  DirName="STM32-P107_ethernet_demo_5.50"
  ZipFile=${DirName}\.zip
  #getFile http://olimex.com/dev/soft/arm/ST/STM-P107/ $ZipFile
  getFile https://www.olimex.com/Products/ARM/ST/STM32-P107/resources/ $ZipFile
  getFile http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/firmware/stsw-stm32010.zip
  
  unZIP  $DirName $ZipFile
  #add2CleanUp ../cleanup.sh "rm -Rf $Install_Dir/$File"
  
  if [ ! -d ${DirName} ]; then
    echo "ERROR: Cannot download file (folder '${DirName}/' not found in `pwd`)!"
    exit 5
  fi
  #cd "${DirName}/"
  if [ -d ${DirName} ]; then
    cd "${DirName}"
    File="Ethernet/app/uip-conf.h"
    if [ ! -e "$File" ]; then #{
      echo "$ScriptName - ERROR: Cannot find file `pwd`/$File"
      exit 11
    fi #}
    if [ ! -e "${File}_orig" ]; then #{
        makeOrig ${File}
        echo "`pwd`> modifing ${File}.. (disabling double definitions)"
    
        replaceInFile ${File} 'typedef t_u8 t_u8;'     '#include "ttc-lib/ttc_basic_types.h" //provides definitions of basic datatypes'
        replaceInFile ${File} 'typedef ut_int16 t_u16;'   '//'
        replaceInFile ${File} '#include "webserver.h"'    '// #include "webserver.h"'
        #replaceInFile ${File} ''                         '//TheToolChain already define in STM32_io.h  '
        addLine "${File}" '#include "tcpip_uip_types.h"'
        mv -v "${File}" "${File}_TTC"   # move header file away to avoid conflicts with TTC-Librasy/tcpip/uip-conf.h
    fi #}
    
    File="Ethernet/app/ethernet.h"
    if [ ! -e "$File" ]; then #{
      echo "$ScriptName - ERROR: Cannot find file `pwd`/$File"
      exit 11
    fi #}
    if [ ! -e "${File}_orig" ]; then #{
      makeOrig ${File}
      echo "`pwd`> modifing ${File}.. (disabling double definitions)"
  
      replaceInFile ${File} '#include "httpd.h"'     '//TheToolChain httpd not required for minimal TCP/IP - #include "httpd.h"'
      
    fi #}
    cd "$Install_Path"

    File="STM32-P107_ethernet_demo_5.50/Ethernet/uip/pt.h"
    if [ ! -e "$File" ]; then #{
      echo "$ScriptName - ERROR: Cannot find file `pwd`/$File"
      exit 11
    fi #}
    if [ ! -e "${File}_orig" ]; then #{
      makeOrig ${File}
      echo "`pwd`> modifing ${File}.."
  
      replaceInFile ${File} 'volatile char PT_YIELD_FLAG = 1; '     'volatile char PT_YIELD_FLAG = 1; (void) PT_YIELD_FLAG; '
      
    fi #}

    File="STM32-P107_ethernet_demo_5.50/Ethernet/uip/uip.c"
    if [ ! -e "$File" ]; then #{
      echo "$ScriptName - ERROR: Cannot find file `pwd`/$File"
      exit 11
    fi #}
    if [ ! -e "${File}_orig" ]; then #{
      makeOrig ${File}
      echo "`pwd`> modifing ${File}.."
  
      replaceInFile ${File} 'ip_send_nolen:'      '//ip_send_nolen:'
      replaceInFile ${File} "#pragma "            "//TTC #pragma "
      
    fi #}
    cat >replace.sh <<END_OF_REPLACE
#!/bin/bash

source \$HOME/Source/TheToolChain/InstallData/scripts/installFuncs.sh

echo "\$ScriptName patching file \$1 ..."
replaceInFile "\$1" "#include <stdio.h>"  "// #include <stdio.h> // TTC provides optimized version of this library"
replaceInFile "\$1" "#include <stdlib.h>" "// #include <stdlib.h> // TTC provides optimized version of this library"
replaceInFile "\$1" "#include <string.h>"  '#include "ttc-lib/ttc_memory.h" // #include <string.h>  // TTC provides optimized version of this library'

END_OF_REPLACE
    find ./ -name "*.c" -exec bash ./replace.sh {} \; 

    File="STM32-P107_ethernet_demo_5.50/Ethernet/uip/uip_arp.c"
    if [ ! -e "$File" ]; then #{
      echo "$ScriptName - ERROR: Cannot find file `pwd`/$File"
      exit 11
    fi #}
    if [ ! -e "${File}_orig" ]; then #{
      makeOrig ${File}
      echo "`pwd`> modifing ${File}.."
  
      # fix for "warning: declaration of 'ipaddr' shadows a global declaration [-Wshadow]"
      replaceInFile ${File} 'uip_arp_update(t_u16 \*ipaddr, struct uip_eth_addr \*ethaddr)'     'uip_arp_update(t_u16 \*my_ipaddr, struct uip_eth_addr \*ethaddr) //TTC-fix'
      replaceInFile ${File} 'ipaddr\[0\] == tabptr->ipaddr\[0\] \&\&'     'my_ipaddr\[0\] == tabptr->ipaddr\[0\] \&\& //TTC-fix'
      replaceInFile ${File} 'ipaddr\[1\] == tabptr->ipaddr\[1\]) {'     'my_ipaddr\[1\] == tabptr->ipaddr\[1\]) { //TTC-fix'
      replaceInFile ${File} 'memcpy(tabptr->ipaddr, ipaddr, 4);'     'memcpy(tabptr->ipaddr, my_ipaddr, 4); //TTC-fix'
      
    fi #}


    cd "$Install_Path"    
    echo "" >OK.Install
  fi
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then #{
  Name="${Install_Dir}"
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

  #include "stm32_eth.h"
  #include "ethernet.h"
  #ifdef EXTENSION_http_server
      # include "httpd.h"
  #endif

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

# EXTENSION_HTTP_SERVER=1   # activate to include this feature!

ifdef EXTENSION_$Name
#   ifndef STM32F10X_CL
#     ERROR: STM32F10X_CL_must_be_set!
#   endif
 
INCLUDE_DIRS += -I additionals/$Name/Libraries/STM32_ETH_Driver/inc/ \\
                -I additionals/$Name/Ethernet/uip/ \\
                -I additionals/$Name/Ethernet/app/ \\
                -I additionals/$Name/Ethernet/uip/httpd/

VPATH += additionals/$Name/Libraries/STM32_ETH_Driver/src/ \\
         additionals/$Name/Ethernet/app \\
         additionals/$Name/Ethernet/uip \\
         additionals/$Name/Ethernet/uip/httpd/

MAIN_OBJS += stm32_eth.o clock-arch.o timer.o uip.o uip_arp.o uiplib.o
#X MAIN_OBJS += ethernet.o 

ifdef EXTENSION_HTTP_SERVER

COMPILE_OPTS += -DEXTENSION_HTTP_SERVER

VPATH += $Name/Ethernet/uip/httpd/
MAIN_OBJS += httpd-fs.o http-strings.o httpd.o httpd-cgi.o psock.o

endif

endif
END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "TCP/IP-stack uIP (only one socket; manual resend; no multitasking required)" #{
      cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.400_network_*

# ACTIVATE_SECTION_B create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
#addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/${Install_Dir}/ $Install_Dir"

# ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_E call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$ScriptName"
activate.450_fake_sbrk_support.sh        QUIET "\$ScriptName"
activate.250_stm_std_peripherals__tim.sh QUIET "\$ScriptName"
activate.500_ttc_gpio.sh                 QUIET "\$ScriptName"

END_OF_ACTIVATE

  # create links into extensions.active/
  echo "createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ${Dir_Extensions} 
  #}

  addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir;  createLink \$Source/TheToolChain/InstallData/$Install_Dir/STM32-P107_ethernet_demo_5.50/  $Install_Dir"
  echo "Installed successfully: $Install_Dir"
#}
else
  echo "failed to install $Install_Dir"
  exit 10
fi

cd ..

#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
