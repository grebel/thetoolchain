#!/bin/bash
#                 TheToolChain
#
#  Install script for CodeSourcery G++ compiler toolchain 
#  for CortexM3 microcontrollers from CodeSourcery.
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="050"
EXTENSION_SHORT="sourcery_gpp"
EXTENSION_PREFIX="compiler"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

Folder="arm-2010q1"
BinFolder="`pwd`/$Folder/bin"
ToolChainBinFolder="Source/TheToolChain/InstallData/bin"
GenericFolder="\$HOME/Source/TheToolChain/InstallData/$Install_Dir/$Folder/bin"

if [ ! -e OK.Install ]; then #{

echo "installing in $Install_Dir ..."

Archive="arm-2010q1-188-arm-none-eabi-i686-pc-linux-gnu.tar.bz2"
if [ -e $Archive ]; then
  Error=""
  bzip2 -t $Archive || Error="1"
  if [ "$Error" != "" ]; then
    echo "Archive corrupt: $Archive"
    rm -v $Archive
  fi
fi
Iterations=0
while [ "`bzip2 -t $Archive 2>&1`" != "" ]; do
  echo "trying to download $Archive ..."
  #getFile http://www.codesourcery.com/sgpp/lite/arm/portal/package6493/public/arm-none-eabi/ $Archive
  getFile http://hlb-labor.de/cs/ $Archive
  Iterations=$(( $Iterations + 1 ))
  if [ $Iterations -gt 10 ]; then
    break
  fi
done


untgj Folder $Archive
# add2CleanUp ../cleanup.sh "rm -Rf $Install_Dir/$Folder"

#getFile http://www.codesourcery.com/sgpp/lite/arm/portal/package6492/public/arm-none-eabi/ arm-2010q1-188-arm-none-eabi.src.tar.bz2
#untgj arm-2010q1-188-arm-none-eabi .src.tar.bz2

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.codesourcery.com/sgpp/lite/arm/portal/doc7609/ as.pdf               Compiler/CodeSourcery
  getDocumentation http://www.codesourcery.com/sgpp/lite/arm/portal/doc7608/ binutils.pdf         Compiler/CodeSourcery
  getDocumentation http://www.codesourcery.com/sgpp/lite/arm/portal/doc7616/ libc.pdf             Compiler/CodeSourcery
  getDocumentation http://www.codesourcery.com/sgpp/lite/arm/portal/doc7612/ gcc.pdf              Compiler/CodeSourcery
  getDocumentation http://www.codesourcery.com/sgpp/lite/arm/portal/doc7614/ gdb.pdf              Compiler/CodeSourcery
  getDocumentation http://www.codesourcery.com/sgpp/lite/arm/portal/doc7615/ getting-started.pdf  Compiler/CodeSourcery
  getDocumentation http://www.codesourcery.com/sgpp/lite/arm/portal/doc7610/ ld.pdf               Compiler/CodeSourcery
  getDocumentation http://www.codesourcery.com/sgpp/lite/arm/portal/doc7617/ libm.pdf             Compiler/CodeSourcery
  getDocumentation http://www.codesourcery.com/sgpp/lite/arm/portal/doc7613/ cpp.pdf              Compiler/CodeSourcery
  getDocumentation http://www.codesourcery.com/sgpp/lite/arm/portal/doc7611/ gprof.pdf            Compiler/CodeSourcery
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

OldDir=`pwd`

if [ -e "$BinFolder/arm-none-eabi-gcc" ]; then
  createExtensionMakefileHead ${Install_Dir} #{
  createExtensionMakefileTail ${Install_Dir} #}
  echo "" >OK.Install
#X  $BinFolder/arm-none-eabi-gcc --version | grep gcc
fi
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then #{
  #X addLine ../scripts/SourceMe.sh    "export PATH=\"\$PATH:$GenericFolder/\" #$ScriptName"
  addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir;                      createLink \$Source/TheToolChain/InstallData/$Install_Dir/arm-2010q1/  $Install_Dir"
  
  Name="${Install_Dir}"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

COMPILE_OPTS += -DSOURCE_COMPILER=$Name

INCLUDE_DIRS += -I additionals/$Name/arm-none-eabi/include/ \\
                -I additionals/$Name/lib/gcc/arm-none-eabi/4.4.1/include/

# binaries from CodeSourcery must be somewhere in $PATH
CC =arm-none-eabi-gcc
GS =arm-none-eabi-size
AR =arm-none-eabi-ar
CP =arm-none-eabi-objcopy
OD =arm-none-eabi-objdump
#LD =arm-none-eabi-ld -v
LD =arm-none-eabi-gcc                                       # workaround: floating-point bug (http://fun-tech.se/stm32/OlimexBlinky/ld_float.php)
#AS =arm-none-eabi-as
AS =\$(CC) \$(AS_OPTS) -x assembler-with-cpp -c \$(TARGET)  # workaround: .o-files compiled by as cannot be linked by ld

CFLAGS =-std=gnu99 -c -fno-common -fno-builtin -ffreestanding -funsigned-char \$(COMPILE_OPTS) \$(TARGET) 
LDFLAGS=-Wl,-Map=\$(MAIN_MAP) -nostartfiles \$(TARGET)
#X LDFLAGS += -Wl,-\( \${filter %.a,\$^} \$(LD_LIBS) -Wl,-\\)      # workaround: make pre-compiled libraries available 
CPFLAGS=-Obinary
ODFLAGS	= -S
ASFLAGS=\$(COMPILE_OPTS)
# ASFLAGS=-gdwarf2 -mcpu=\$(ARM_CPU) -mthumb-interwork -o\$@

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Compiler SourceyG++ (Closed Source, Binary only)" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile

rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.050_compiler_*

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

#DEPRECATED rm \$HOME/$ToolChainBinFolder/arm-none-eabi-*
#DEPRECATED ln -sv $BinFolder/arm-none-eabi-* \$HOME/$ToolChainBinFolder/

cd \$Dir_Bin
find ../additionals/$Install_Dir/bin/ -name "arm-none-eabi*" -exec ln -sfv {} . \;

# create TTC compiler set (certain scripts require binaries with fixed names)
ln -sfv ../additionals/$Install_Dir/bin/arm-none-eabi-as      assembler
ln -sfv ../additionals/$Install_Dir/bin/arm-none-eabi-gcc     compiler
ln -sfv ../additionals/$Install_Dir/bin/arm-none-eabi-gcc     linker
ln -sfv ../additionals/$Install_Dir/bin/arm-none-eabi-objcopy objcopy
ln -sfv ../additionals/$Install_Dir/bin/arm-none-eabi-objdump objdump

END_OF_ACTIVATE

  createActivateScriptTail $Name ${Dir_Extensions} 
  #}
  
  #X # create symlink for compatibility to old versions of activate_project.sh <v1.0.39
  #X cd ${Dir_Extensions}
  #X createLink activate.${Name}.sh activate.050_compiler_sourcery_g++.sh
  
  echo "Installed successfully: $Install_Dir"
#}
else
  echo "failed to install $Install_Dir"
  exit 10
fi

#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
