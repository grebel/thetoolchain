#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="600"
EXTENSION_SHORT="gfx"
EXTENSION_PREFIX="example"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

  Name="${Install_Dir}_printf"
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension

  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "example_gfx_printf.h"
  example_gfx_printf_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

# we have to define at least one font to make ttc_font.h happy
COMPILE_OPTS += -DTTC_FONT1=font_type1_16x24

INCLUDE_DIRS += -I examples/
vpath %.c examples/

MAIN_OBJS += example_gfx_printf.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Example of how to print on LCD-screen" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.600_example_*
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

if [ "\$TEST" != "" ]; then # Testmode: activate board being required to successfully compile this example 
    activate.100_board_olimex_lcd.sh
fi
activate.500_ttc_gpio.sh   QUIET "\$ScriptName"
activate.500_ttc_string.sh QUIET "\$ScriptName"
activate.500_ttc_queue.sh  QUIET "\$ScriptName"
activate.500_ttc_gfx.sh    QUIET "\$ScriptName"

# at least one font must be activated
activate.510_ttc_font__font_type1_16x24.sh QUIET "\$ScriptName"

END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  Name="${Install_Dir}_benchmark"
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension

  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "example_gfx_benchmark.h"
  example_gfx_benchmark_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

# we have to define at least one font to make ttc_font.h happy
COMPILE_OPTS += -DTTC_FONT1=font_type1_16x24

INCLUDE_DIRS += -I examples/
vpath %.c examples/

MAIN_OBJS += example_gfx_benchmark.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Performance measure for current graphic driver" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.600_example_*
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

if [ "\$TEST" != "" ]; then # Testmode: activate board being required to successfully compile this example 
    activate.100_board_olimex_lcd.sh
fi
activate.500_ttc_gpio.sh   QUIET "\$ScriptName"
activate.500_ttc_string.sh QUIET "\$ScriptName"
#? activate.500_ttc_usart.sh  QUIET "\$ScriptName"
activate.500_ttc_gfx.sh    QUIET "\$ScriptName"
activate.510_ttc_font__font_type1_16x24.sh QUIET "\$ScriptName"

END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  Name="${Install_Dir}_boxes"
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension

  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "example_gfx_boxes.h"
  example_gfx_boxes_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

INCLUDE_DIRS += -I examples/
vpath %.c examples/

MAIN_OBJS += example_gfx_boxes.o

# we have to define at least one font to make ttc_font.h happy
COMPILE_OPTS += -DTTC_FONT1=font_type1_16x24

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Example of how to draw moving boxes with multiple tasks" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.600_example_*
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

enableFeature 450_math_software_float   # single precision floating point mathematics (binary32)
#enableFeature 450_math_software_double  # double precision floating point mathematics (binary64)

activate.500_ttc_random.sh QUIET "\$ScriptName"
activate.500_ttc_queue.sh  QUIET "\$ScriptName"
activate.500_ttc_gfx.sh    QUIET "\$ScriptName"
activate.500_ttc_math.sh   QUIET "\$ScriptName"
activate.510_ttc_font__font_type1_16x24.sh QUIET "\$ScriptName"

END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  echo "Installed successfully: $Install_Dir"

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
