#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
#
#  Install script for a set of High- and Low-Level Drivers
#  for timer devices.
#
#  Created from template _install_NN_TTC_DEVICE.sh revision 20 at 20140204 09:25:54 UTC
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: <AUTHOR>
#

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

StartPath="`pwd`"
RANK="500"
EXTENSION_NAME="ttc_timer"
INSTALLPATH="${RANK}_${EXTENSION_NAME}"
dir "$INSTALLPATH"
cd "$INSTALLPATH"
InstallDir=`pwd`

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   exit 10
# fi #}
#}look for newest versions

if [ ! -e OK.Docs ];    then    #{ (download documentation)

  Get_Fails=""
  
  # download doc-file + move to Documentation/ + create symbolic link into Documentation/ right here 
  # File="STM32F10x_Standard_Peripherals_Library.html"
  # getFile http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library $File
  # addDocumentationFile $File STM
  
  if [ "$Get_Fails" == "" ]; then #{ check get() inside scripts/installFuncs.sh 
    touch OK.Docs
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $INSTALLPATH ..."
    InstallDir=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ high-level driver for timer
    Name="${INSTALLPATH}"  
  
    createExtensionSourcefileHead ${Name}    #{ (create initializer code)
    
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

    #include "../ttc_timer.h"
    ttc_timer_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines

# Timers for STM32F1
ifdef EXTENSION_200_cpu_stm32f10x
COMPILE_OPTS += -DTTC_TIMER1=1
COMPILE_OPTS += -DTTC_TIMER2=2
COMPILE_OPTS += -DTTC_TIMER3=3
COMPILE_OPTS += -DTTC_TIMER4=4
COMPILE_OPTS += -DTTC_TIMER5=5
COMPILE_OPTS += -DTTC_TIMER6=6
COMPILE_OPTS += -DTTC_TIMER7=7
COMPILE_OPTS += -DTTC_INTERRUPT_TIMER_AMOUNT=7
endif

# Timers for STM32L1
ifdef EXTENSION_200_cpu_stm32l1
COMPILE_OPTS += -DTTC_TIMER2=2
COMPILE_OPTS += -DTTC_TIMER3=3
COMPILE_OPTS += -DTTC_TIMER4=4
COMPILE_OPTS += -DTTC_TIMER6=6
COMPILE_OPTS += -DTTC_TIMER7=7
COMPILE_OPTS += -DTTC_TIMER9=9
COMPILE_OPTS += -DTTC_TIMER10=10
COMPILE_OPTS += -DTTC_TIMER11=11
COMPILE_OPTS += -DTTC_INTERRUPT_TIMER_AMOUNT=8
endif

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
INCLUDE_DIRS += -Ittc-lib/timer/

# additional directories to search for C-Sources
vpath %.c ttc-lib/timer/

# additional object files to be compiled
MAIN_OBJS += ttc_timer.o ttc_timer_interface.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ../extensions/ "$0 2" "high-level timer Driver" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${INSTALLPATH}_*

for LowLevelActivate in \`ls \$Dir_Extensions/activate.450_timer_*\`; do
  
  # try to activate this low-level driver
  \$LowLevelActivate QUIET "\$0"
  
  # extract driver name from script name
  LowLevelDriver=\`perl -e "print substr('\$LowLevelActivate', 9, -3);"\`
  
  #X #DISABLED echo "\$0 - LowLevelDriver='\$LowLevelDriver'"
  DriverFound="1"
done

if [ "\$DriverFound" != "" ]; then

  # ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
  createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

  # ACTIVATE_SECTION_D call other activate-scripts
  activate.500_ttc_memory.sh QUIET "\$0"
  activate.500_ttc_list.sh QUIET "\$0"
#  activate.250_stm_std_peripherals__tim.sh QUIET "\$0"
  
else
  echo "\$0 - ERROR: No low-level timer Driver found: Skipping activation!"
fi

END_OF_ACTIVATE
    createActivateScriptTail $Name ../extensions/
    #}
  
  fi #}high-level driver
  if [ "1" == "1" ]; then #{ call install scripts of low-level drivers for timer
    LowLevelInstalls=`ls 2>/dev/null ../installs_low_level/install_*_TIMER_*.sh`
    echo "found LowLevelInstalls: $LowLevelInstalls"
    echo    "running `pwd`/450_timer_stm32f1xx ...  "
    echo -n "        "
    source "../installs_low_level/install_005_TTC_TIMER_STM32F1XX.sh"
 
#    for LowLevelInstall in $LowLevelInstalls
#    do
#      cd "$InstallDir"
#      echo    "running `pwd`/$LowLevelInstall ...  "
#      echo -n "        "
#      source $LowLevelInstall
#    done
    cd "$InstallDir"
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../scripts/createLinks.sh "rm 2>/dev/null $INSTALLPATH; ln -sv \$Source/TheToolChain/InstallData/${INSTALLPATH}/SUBFOLDER $INSTALLPATH"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ recommended: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D
COMPILE_OPTS += -DTTC_TIMER_PIN_PUSH_BACK_SINGLE=TTC_LED1
COMPILE_OPTS += -DTTC_TIMER_PIN_PULL_FRONT_SINGLE=TTC_LED2

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I
INCLUDE_DIRS += -I regressions/

# additional directories to search for C-Sources
#vpath %.c 
vpath %.c regressions/

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ../extensions/ "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${INSTALLPATH}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${INSTALLPATH}.sh QUIET "\$0"


END_OF_ACTIVATE
    createActivateScriptTail $Name ../extensions/
    #}

  fi #}
  if [ "1" == "1" ]; then #{ recommended: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ../extensions/ "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${INSTALLPATH}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
  activate.${INSTALLPATH}.sh QUIET "\$0"
  activate.500_ttc_timer.sh QUIET "\$0"
  activate.500_ttc_gpio.sh QUIET "\$0"
  activate.500_ttc_memory.sh QUIET "\$0"
  activate.500_ttc_list.sh QUIET "\$0"
#  activate.250_CPAL_STM32F10x_StdPeriph_Driver.sh QUIET "\$0"
#  activate.250_stm_std_peripherals__exti.sh QUIET "\$0"
#  activate.250_stm_std_peripherals__rcc.sh QUIET "\$0"
#  activate.250_stm_std_peripherals__tim.sh QUIET "\$0"

END_OF_ACTIVATE
    createActivateScriptTail $Name ../extensions/
    #}

  fi #}

  cd "$InstallDir"
  echo "Installed successfully: $INSTALLPATH"
#}
else                            #{ Install failed
  echo "failed to install $INSTALLPATH"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$StartPath"
applyTargetUserAndGroup "$INSTALLPATH"

exit 0
