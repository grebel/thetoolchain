#!/bin/bash
#
#  Install script for ST Stdandard Peripherals Library
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "../Documentation" "$1" # sets $Start_Path, $Install_Dir, $Install_Path

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW --------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://home.comcast.net/~mcatudal/index_fichiers/ CrossDevHowto_STM32_Mandriva.pdf HowTo_Using_Open_Source_Tools_for_STM32_under_Linux.pdf Compiler/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ -e OK.Documentation ]; then
  echo "Installed successfully: $Install_Dir"
fi

#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
