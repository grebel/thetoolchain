#!/bin/bash

#
#  Install script for combined Microcontroller + 2,4GHz radio STM32L1xx 
#  with ARM Cortex M3 core.
#
#  Script written by Greg Knoll 2013
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "200_cpu_stm32l1xx" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.st.com/web/en/resource/technical/document/datasheet/                            CD00277537.pdf STM32L15xxx-Datasheet.pdf                                       uC/STM32L1xx/
  getDocumentation http://www.st.com/web/en/resource/technical/document/datasheet/                            DM00078075.pdf STM32L100xx-Datasheet.pdf                                       uC/STM32L1xx/
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/reference_manual/ CD00240193.pdf RM0038_STM32l1xx-Reference-Manual                               uC/STM32L1xx/
  getDocumentation http://www.st.com/web/en/resource/technical/document/application_note/                     CD00273528.pdf AN3216_Getting_started_with_STM32L1xxx_hardware_development.pdf uC/STM32L1xx/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
  if [ ! -e OK.Install ]; then #{
    Error=""
      
    Archive="stsw-stm32077.zip"
    getFile http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/firmware/ $Archive $Archive
    
    if [ ! -e $Archive ]; then
        Error="Cannot download $Archive!"
    fi
    
    unZIP STM32L1xx_StdPeriph_Lib_V1.2.0 $Archive || mv -f $Archive ${Archive}_bad
    
    rm current 2>/dev/null
    
    LibraryDir=`find ./  -maxdepth 1 -mindepth 1 -type d -print`
    createLink $LibraryDir current
    cd current
    #X dir ../../../Documentation/STM
    #X mv *.chm ../../../Documentation/STM/
    for File in `ls *.chm`; do
      addDocumentationFile "$File" uC/STM32L1xx
    done
    
    File="Libraries/CMSIS/Device/ST/STM32L1xx/Include/stm32l1xx.h"
    if [ -e ${File}_orig ]; then
          mv -f ${File}_orig $File 
    fi
    mv -f $File ${File}_orig
    
    echo "  May need to create macro assert_param in $File"
    Note="  Note: If this is done, manually remove assert_param from your stm32l1xx_conf.h !"
      echo $Note
      addLine ../Notes.txt $Note
      addLine ../Notes.txt "StdPeripheralLibrary-> http://www.mikrocontroller.net/articles/STM32F10x_Standard_Peripherals_Library"
      
    
    DestFolder="Libraries/CMSIS/Device/ST/STM32L1xx/Include"
    if [ ! -e $DestFolder/system_stm32l1xx.c ]; then
        # system_stm32l1xx.c is required to be found by the compiler so we put it aside its header file
        #cp -v Project/STM32F10x_StdPeriph_Template/system_stm32f10x.c $DestFolder
        addLine ../Notes.txt "system_stm32f10x.c NOT FOUND. Move beside its header file (-> https://my.st.com/public/STe2ecommunities/mcu/Lists/ARM%20CortexM3%20STM32/Flat.aspx?RootFolder=%2Fpublic%2FSTe2ecommunities%2Fmcu%2FLists%2FARM%20CortexM3%20STM32%2FMissing%20file%20in%20new%203.4.0%20libraries%20system_stm32f10x.c&FolderCTID=0x01200200770978C69A1141439FE559EB459D758000626BE2B829C32145B9EB5739142DC17E&currentviews=320)"
    fi
    
    
    cd ..
    
    #{Was install successful?
    if [ "$Error" == "" ]; then
        touch OK.Install
      else
        echo "$0 - ERROR: $ERROR!"
        exit 10
    fi
    #}------------------------
      
  fi #}END ! -e OK.Install
  if [   -e OK.Install ]; then #{
    Architecture="stm32l1xx"
    Name="${Install_Dir}"
    
    #{Write config file for openocd
    ConfigFile="../999_open_ocd/share/openocd/scripts/target/stm32l1xx_flash.cfg"
    if [ ! -e "_$ConfigFile" ]; then
      echo "creating cfg-file '`pwd`/$ConfigFile'.."
      cat <<'END_OF_CONFIG' >$ConfigFile #{Code added by $0
# openocd flash script for stm32l1xx

# finish configuration stage + proceed to run stage
init

# hardware reset processor with halt
reset halt

# check target state
poll

# list all found falsh banks
flash banks

# identify the flash
flash probe 0

# unprotect flash for writing
#DEPRECATED flash protect_check 0 0

# erasing all flash
stm32lx mass_erase 0

# write file to flash number 0
flash write_bank 0 main.bin 0

# hardware processor reset with run
reset run

#shutdown openocd daemon
shutdown
#}END Code added by $0

END_OF_CONFIG
    else
      echo "cfg-file already exists: '`pwd`/$ConfigFile'.."
    fi
    #}END Write config file for openocd
    
    LibPrefix="250_stm_std_peripherals_"
    LibName="${LibPrefix}l1xx"
    
    createExtensionMakefileHead ${Name} #{ ----Create Makefile
    File="${Dir_Extensions}makefile.${Name}" 
    cat <<END_OF_MAKEFILE >>$File 

#ARM_CPU     =arm7tdmi
TARGET      =-mcpu=cortex-m3 -mthumb
TARGET_ARCHITECTURE_STM32L1xx=1
TARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_PREFIX=STM32
COMPILE_OPTS += -DTARGET_ARCHITECTURE_STM32L1xx
COMPILE_OPTS += -DTARGET_DATA_WIDTH=32
COMPILE_OPTS += -DUSE_STDPERIPH_DRIVER
COMPILE_OPTS += -DUSE_FULL_ASSERT=1
COMPILE_OPTS += -DTTC_REAL_TIME_CLOCK_AMOUNT=1
COMPILE_OPTS += -DTTC_REAL_TIME_CLOCK1
COMPILE_OPTS += -DTTC_INTERRUPT_GPIO_AMOUNT=16
# architecture specific support code
INCLUDE_DIRS += -Ittc-lib/stm32/ -Ittc-lib/stm32l1/
vpath %.c ttc-lib/stm32/ ttc-lib/resgister/
vpath %.s additionals/270_CPAL_CMSIS/CM3/DeviceSupport/ST/STM32L1xx/startup/gcc_ride7

#{ Timer #1
# okular ~/Source/TheToolChain/Documentation/uC/STM32L1xx/STM32L100xx-Datasheet.pdf (DocID024295 Rev.3) p.32-34

COMPILE_OPTS += -DTTC_TIMER1=ttc_device_2       # no TIM1 available on STM32
COMPILE_OPTS += -DTTC_TIMER1_CHANNELS=4         # amount of channels available
COMPILE_OPTS += -DTTC_TIMER1_WIDTH=16           # 16-bit wide timer counter
COMPILE_OPTS += -DTTC_TIMER1_UP=1               # timer can count upwards
COMPILE_OPTS += -DTTC_TIMER1_DOWN=1             # timer can count downwards
COMPILE_OPTS += -DTTC_TIMER1_UP_DOWN=1          # timer can count up- and downwards
COMPILE_OPTS += -DTTC_TIMER1_PRESCALE_MIN=1     # minimum prescaler value
COMPILE_OPTS += -DTTC_TIMER1_PRESCALE_MAX=65535 # maximum prescaler value

COMPILE_OPTS += -DTTC_TIMER1_PWM=4              # amount of PWM output pins connected to this timer
COMPILE_OPTS += -DTTC_TIMER1_PWM1_PIN1=tgp_a15 # gpio pin #1 connected to pwm channel #1
COMPILE_OPTS += -DTTC_TIMER1_PWM2_PIN1=tgp_b3  # gpio pin #1 connected to pwm channel #2
COMPILE_OPTS += -DTTC_TIMER1_PWM3_PIN1=tgp_b10 # gpio pin #1 connected to pwm channel #3
COMPILE_OPTS += -DTTC_TIMER1_PWM4_PIN1=tgp_b11 # gpio pin #1 connected to pwm channel #4
# STM32L1xx may change pin layout via alternate function:
COMPILE_OPTS += -DTTC_TIMER1_PWM1_PIN2=tgp_a0  # gpio pin #2 connected to pwm channel #1
COMPILE_OPTS += -DTTC_TIMER1_PWM2_PIN2=tgp_a1  # gpio pin #2 connected to pwm channel #2
COMPILE_OPTS += -DTTC_TIMER1_PWM3_PIN2=tgp_a2  # gpio pin #2 connected to pwm channel #3
COMPILE_OPTS += -DTTC_TIMER1_PWM4_PIN2=tgp_a3  # gpio pin #2 connected to pwm channel #4

COMPILE_OPTS += -DTTC_TIMER1_CAPTURES=4         # amount of capture channels of this timer
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE1_PIN1=tgp_a15 # gpio pin #1 connected to capture channel #1
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE2_PIN1=tgp_b3  # gpio pin #1 connected to capture channel #2
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE3_PIN1=tgp_b10 # gpio pin #1 connected to capture channel #3
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE4_PIN1=tgp_b11 # gpio pin #1 connected to capture channel #4
# STM32L1xx may change pin layout via alternate function:
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE1_PIN2=tgp_a0  # gpio pin #2 connected to capture channel #1
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE2_PIN2=tgp_a1  # gpio pin #2 connected to capture channel #2
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE3_PIN2=tgp_a2  # gpio pin #2 connected to capture channel #3
COMPILE_OPTS += -DTTC_TIMER1_CAPTURE4_PIN2=tgp_a3  # gpio pin #2 connected to capture channel #4

#}
# ToDo: Complete timer configuration for all timers...

COMPILE_OPTS += -DTTC_TIMER2=ttc_device_3
COMPILE_OPTS += -DTTC_TIMER3=ttc_device_4
COMPILE_OPTS += -DTTC_TIMER4=ttc_device_6
COMPILE_OPTS += -DTTC_TIMER5=ttc_device_7
COMPILE_OPTS += -DTTC_TIMER6=ttc_device_9


COMPILE_OPTS += -DTTC_TIMER7=ttc_device_10
COMPILE_OPTS += -DTTC_TIMER7_PWM=1              # amount of PWM output pins connected to this timer
COMPILE_OPTS += -DTTC_TIMER7_PWM1_PIN1=tgp_b12 # gpio pin #1 connected to pwm channel #1
# STM32L1xx may change pin layout via alternate function:
COMPILE_OPTS += -DTTC_TIMER7_PWM1_PIN2=tgp_b98  # gpio pin #2 connected to pwm channel #1

COMPILE_OPTS += -DTTC_TIMER7_CAPTURES=1         # amount of capture channels of this timer
COMPILE_OPTS += -DTTC_TIMER7_CAPTURE1_PIN1=tgp_b12 # gpio pin #1 connected to capture channel #1
COMPILE_OPTS += -DTTC_TIMER7_CAPTURE1_PIN2=tgp_b8  # gpio pin #2 connected to capture channel #1

COMPILE_OPTS += -DTTC_TIMER8=ttc_device_11
COMPILE_OPTS += -DTTC_TIMER8_PWM=1              # amount of PWM output pins connected to this timer
COMPILE_OPTS += -DTTC_TIMER8_PWM1_PIN1=tgp_b15 # gpio pin #1 connected to pwm channel #1
# STM32L1xx may change pin layout via alternate function:
COMPILE_OPTS += -DTTC_TIMER8_PWM1_PIN2=tgp_b9  # gpio pin #2 connected to pwm channel #1

COMPILE_OPTS += -DTTC_TIMER8_CAPTURES=1         # amount of capture channels of this timer
COMPILE_OPTS += -DTTC_TIMER8_CAPTURE1_PIN1=tgp_b15 # gpio pin #1 connected to capture channel #1
# STM32L1xx may change pin layout via alternate function:
COMPILE_OPTS += -DTTC_TIMER8_CAPTURE1_PIN2=tgp_b9  # gpio pin #2 connected to capture channel #1

MAIN_OBJS += system_stm32l1xx.o 

# define linker script to use
LDFLAGS += -Tconfigs/memory_stm32l1xx.ld -mthumb -mcpu=cortex-m3

#{Startup codes for STM32L1xx family

startup_stm32l1xx_md.o:		startup_stm32l1xx_md.s
startup_stm32l1xx_mdp.o:	startup_stm32l1xx_mdp.s
startup_stm32l1xx_hd.o:		startup_stm32l1xx_hd.s

#}

#{ define uController to use (required to include stm32l1xx.h) ---------------

# Microcontroller Class: activate only one uC!
# - Medium density plus STM32L15xxC devices are microcontrollers with a Flash memory density of 256 Kbytes.

#STM32L1XX_MD   =1       # Medium density devices
#STM32L1XX_MDP  =1       # Medium density plus Line devices
#STM32L1XX_HD   =1       # High density devices



ifdef STM32L1XX_MD
  ifdef uCONTROLLER
    ERROR: STM32L1XX_MD=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32L1XX_MD
  COMPILE_OPTS += -DuCONTROLLER=STM32L1XX_MD
  COMPILE_OPTS += -DSTM32L1XX_MD
  MAIN_OBJS += startup_stm32l1xx_md.o
endif

ifdef STM32L1XX_MDP
  ifdef uCONTROLLER
    ERROR: STM32L1XX_MDP=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32L1XX_MDP
  COMPILE_OPTS += -DuCONTROLLER=STM32L1XX_MDP
  COMPILE_OPTS += -DSTM32L1XX_MDP
  MAIN_OBJS += startup_stm32l1xx_mdp.o
endif

ifdef STM32L1XX_HD
  ifdef uCONTROLLER
    ERROR: STM32L1XX_HD=1 - Only one uController may be selected!
  endif
  uCONTROLLER=STM32L1XX_HD
  COMPILE_OPTS += -DuCONTROLLER=STM32L1XX_HD
  COMPILE_OPTS += -DSTM32L1XX_HD
  MAIN_OBJS += startup_stm32l1xx_hd.o
endif

ifndef uCONTROLLER
  ERROR: No microcontroler class selected. Define one from (STM32L1XX_MD, STM32L1XX_MDP, STM32L1XX_HD)
endif

#}END define uController


END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}---- END Create Makefile
    
    createActivateScriptHead $Name ${Dir_Extensions} $0 "Standard Peripheral Library for CPU STM32L1xx" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{Code added by install_15_CPU_STM32L1xx.sh

# remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.200_cpu_*

activate.190_cpu_cortexm3.sh QUIET \"\$0\"

# create links into extensions.active/
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# activate corresponding std_peripheral_library
activate.250_CPAL_STM32L1xx_StdPeriph_Driver.sh QUIET

# create link to target script being required by openocd to initialize target cpu
cd \$Dir_Configs
createLink ../additionals/999_open_ocd/target/stm32l.cfg openocd_target.cfg
createLink ../additionals/999_open_ocd/target/stm32l1xx_flash.cfg openocd_flash.cfg
cd ..
#}END Code added by install_15_CPU_STM32L1xx.sh

activate.190_cpu_cortexm3.sh                    QUIET "\$0"
#X activate.500_ttc_register.sh                    QUIET "\$0"
activate.250_CPAL_STM32L1xx_StdPeriph_Driver.sh QUIET "\$0"
activate.250_stm_std_peripherals__rcc.sh        QUIET "\$0"
activate.250_stm_std_peripherals__exti.sh       QUIET "\$0"
activate.250_stm_std_peripherals__syscfg.sh     QUIET "\$0"
#activate.450_interrupt_cortexm3.sh             QUIET "\$0"

# create link to target script being required by openocd to initialize target cpu
cd \$Dir_Configs
createLink ../additionals/999_open_ocd/target/stm32l1.cfg openocd_target.cfg
createLink ../additionals/999_open_ocd/target/stm32l1xx_flash.cfg openocd_flash.cfg
cd ..

END_OF_ACTIVATE
    createActivateScriptTail $Name ${Dir_Extensions}
    #}

    echo "Installed successfully: $Install_Dir"
  fi #}END OK.Install
  
  cd ..
  rmdir 2>/dev/null $Install_Dir  # removes only empty install dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
