#!/bin/bash

#{  Install script for Open On-Chip Debugger tool made by Dominic Rath.
#  Script written by Gregor Rebel 2010-2018.
#  OpenOCD - On Chip Debugger
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64
# openSuSE 12.1 x86
#}

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"
setInstallDir "999_open_ocd" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

USB_OpenGLink_VID="0403"
USB_OpenGLink_PID="bafa"

# Select which OpenOCD release to install
#Version_OpenOCD="0.5.0"
#Version_OpenOCD="0.6.1"
#Version_OpenOCD="0.7.0" 
#Version_OpenOCD="0.8.0-rc1" # does not work with stlink-v2
#Version_OpenOCD="0.8.0"
#Version_OpenOCD="0.8.0+L0" # special version with STM32L0 support
#Version_OpenOCD="0.9.0"
Version_OpenOCD="0.10.0"

# Used as prefix to download openocd sources
#URL_Prefix="http://download.berlios.de/openocd/"
URL_Prefix="https://netix.dl.sourceforge.net/project/openocd/openocd/${Version_OpenOCD}/"

InstallInHomeDir="1"
if [ "$InstallInHomeDir" == "1" ]; then
  Prefix=`pwd`  # install into subfolder of InstallData/ for current user only
else
  findPrefix    # install for all users
fi
PrefixOO=$Prefix

Install_Path=`pwd`

identifyOS
getToolChainVersion
if [ "$OS_ARCHITECTURE" == "x86_64" ]; then
  ArchiveOpenOCD="open_ocd_x64_${VersionTTC}.tar.bz2"
else
  ArchiveOpenOCD="open_ocd_x86_${VersionTTC}.tar.bz2"
fi

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.goepel.com/fileadmin/downloads/ einfrg_bscan.pdf                                            Einfuehrung_Boundary_Scan.pdf                                   Programmer
  getDocumentation http://www.arm.com/files/pdf/              Low_Pin-Count_Debug_Interfaces_for_Multi-device_Systems.pdf SWD_Low_Pin-Count_Debug_Interfaces_for_Multi-device_Systems.pdf Programmer
  getDocumentation http://elmicro.com/files/elmicro/          jtag-swd.pdf                                                SWD_Ein_neues_Debuginterface_fuer_Cortex-M.pdf                  Programmer
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then #{

  echo "installing in $Install_Dir ..."
  
  getFileFromMirror $ArchiveOpenOCD $ArchiveOpenOCD
  if [ "1" == "0" ]; then
    echo "removing downloaded file to force recompilation!"
    rm -v $ArchiveOpenOCD
  fi
  if [ -e "$ArchiveOpenOCD" ]; then
    unTBZ bin $ArchiveOpenOCD
  fi
  
  if [ -d bin ]; then #{ successfully downloaded prepacked openocd: remove some OK-files
    echo "successfully downloaded prepacked openocd: remove some OK-files"
    rm -vf OK.Install OK.Recompiled
  #}
  else                #{ could not download prepacked openocd: compile it on our own
    echo "could not download prepacked openocd: compiling it from source"

    if [ ! -e OK.Packages ]; then #{
      installPackage texlive
      installPackage texinfo
      installPackageSafe "which ddd" ddd
      touch OK.Packages
    fi #}
    
    if [ "`pwd | grep TheToolChain.install`" != "" ]; then #{ installation running inside .install folder: do a trick to compile it correctly
      echo "compiling inside TheToolChain.install: creating temporary link..."
      DisabledToolChain="$HOME/Source/TheToolChain.disabled_for_compilation"
      mv "$HOME/Source/TheToolChain" "$DisabledToolChain"
      
    fi #}

    
    DirName="openocd-$Version_OpenOCD"
    if [ "$Version_OpenOCD" == "0.5.0" ]; then #{ old source from berlios
      getFile $URL_Prefix ${DirName}.tar.bz2
      untgj ${DirName} ${DirName}.tar.bz2
      #}
    else
      if [ "$Version_OpenOCD" == "0.8.0+L0" ]; then #{ special version with early support for STM32L0xx
        if [ ! -d ${DirName} ]; then
          git clone http://openocd.zylin.com/openocd refs/changes/17/2317/4
        fi
        createLink refs/changes/17/2317/4/ ${DirName}
        #}
      else                                  #{ stable release: download source code from sourceforge 
        # getFile http://garr.dl.sourceforge.net/project/openocd/openocd/$Version_OpenOCD/ ${DirName}.tar.bz2
        getFile http://vorboss.dl.sourceforge.net/project/openocd/openocd/$Version_OpenOCD/ openocd-$Version_OpenOCD.tar.bz2
        untgj ${DirName} ${DirName}.tar.bz2
      fi #}
    fi
    
    #add2CleanUp ../cleanup.sh "rm -Rf $Install_Dir/$DirName"
    
    if [ ! -d ${DirName} ]; then
      echo "ERROR: Cannot download file (folder ${DirName} not found in `pwd`)!"
      exit 5
    fi
    cd "${DirName}/"
    echo "compiling $DirName ----------------------------------------------"
    echo "clean..." && make clean >clean.log 2>&1
  
    if [ "$Version_OpenOCD" == "0.5.0" ]; then #{
      File="src/flash/nor/stm32w.c"
      if [ ! -e "${File}_orig" ]; then #{ create modified stm32w.c for stm32w108 (thanks to Frederik Hoffmann and Sascha Poggemann)
        makeOrig "${File}"
        cat <<'END_OF_SOURCE' >"${File}" #{
/***************************************************************************
*   Copyright (C) 2005 by Dominic Rath                                    *
*   Dominic.Rath@gmx.de                                                   *
*                                                                         *
*   Copyright (C) 2008 by Spencer Oliver                                  *
*   spen@spen-soft.co.uk                                                  *
*
*   Copyright (C) 2011 by Erik Botö
*   erik.boto@pelagicore.com
* 
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "imp.h"
#include <helper/binarybuffer.h>
#include <target/algorithm.h>
#include <target/armv7m.h>

/* stm32w register locations */

#define EM357_FLASH_ACR		0x40008000
#define EM357_FLASH_KEYR	0x40008004
#define EM357_FLASH_OPTKEYR	0x40008008
#define EM357_FLASH_SR		0x4000800C
#define EM357_FLASH_CR		0x40008010
#define EM357_FLASH_AR		0x40008014
#define EM357_FLASH_OBR		0x4000801C
#define EM357_FLASH_WRPR	0x40008020

#define EM357_FPEC_CLK		0x4000402c
/* option byte location */

#define EM357_OB_RDP		0x08040800
#define EM357_OB_WRP0		0x08040808
#define EM357_OB_WRP1		0x0804080A
#define EM357_OB_WRP2		0x0804080C

/* FLASH_CR register bits */

#define FLASH_PG		(1 << 0)
#define FLASH_PER		(1 << 1)
#define FLASH_MER		(1 << 2)
#define FLASH_OPTPG		(1 << 4)
#define FLASH_OPTER		(1 << 5)
#define FLASH_STRT		(1 << 6)
#define FLASH_LOCK		(1 << 7)
#define FLASH_OPTWRE	(1 << 9)

/* FLASH_SR register bits */

#define FLASH_BSY		(1 << 0)
#define FLASH_PGERR		(1 << 2)
#define FLASH_WRPRTERR	(1 << 4)
#define FLASH_EOP		(1 << 5)

/* EM357_FLASH_OBR bit definitions (reading) */

#define OPT_ERROR		0
#define OPT_READOUT		1

/* register unlock keys */

#define KEY1			0x45670123
#define KEY2			0xCDEF89AB

struct stm32w_options
{
ut_int16 RDP;
ut_int16 user_options;
ut_int16 protection[3];
};

struct stm32w_flash_bank
{
struct stm32w_options option_bytes;
struct working_area *write_algorithm;
int ppage_size;
int probed;
};

static int stm32w_mass_erase(struct flash_bank *bank);

/* flash bank stm32w <base> <size> 0 0 <target#>
*/
FLASH_BANK_COMMAND_HANDLER(stm32w_flash_bank_command)
{
struct stm32w_flash_bank *stm32w_info;

if (CMD_ARGC < 6)
{
  LOG_WARNING("incomplete flash_bank stm32w configuration");
  return ERROR_FLASH_BANK_INVALID;
}

stm32w_info = malloc(sizeof(struct stm32w_flash_bank));
bank->driver_priv = stm32w_info;

stm32w_info->write_algorithm = NULL;
stm32w_info->probed = 0;

return ERROR_OK;
}

static inline int stm32w_get_flash_status(struct flash_bank *bank, t_u32 *status)
{
struct target *target = bank->target;
return target_read_u32(target, EM357_FLASH_SR, status);
}

static int stm32w_wait_status_busy(struct flash_bank *bank, int timeout)
{
struct target *target = bank->target;
t_u32 status;
int retval = ERROR_OK;

/* wait for busy to clear */
for (;;)
{
  retval = stm32w_get_flash_status(bank, &status);
  if (retval != ERROR_OK)
    return retval;
  LOG_DEBUG("status: 0x%" PRIx32 "", status);
  if ((status & FLASH_BSY) == 0)
    break;
  if (timeout-- <= 0)
  {
    LOG_ERROR("timed out waiting for flash");
    return ERROR_FAIL;
  }
  alive_sleep(1);
}

if (status & FLASH_WRPRTERR)
{
  LOG_ERROR("stm32w device protected");
  retval = ERROR_FAIL;
}

if (status & FLASH_PGERR)
{
  LOG_ERROR("stm32w device programming failed");
  retval = ERROR_FAIL;
}

/* Clear but report errors */
if (status & (FLASH_WRPRTERR | FLASH_PGERR))
{
  /* If this operation fails, we ignore it and report the original
   * retval
   */
  target_write_u32(target, EM357_FLASH_SR, FLASH_WRPRTERR | FLASH_PGERR);
}
return retval;
}

static int stm32w_read_options(struct flash_bank *bank)
{
t_u32 optiondata;
struct stm32w_flash_bank *stm32w_info = NULL;
struct target *target = bank->target;

stm32w_info = bank->driver_priv;

/* read current option bytes */
int retval = target_read_u32(target, EM357_FLASH_OBR, &optiondata);
if (retval != ERROR_OK)
  return retval;

stm32w_info->option_bytes.user_options = (ut_int16)0xFFFC | ((optiondata >> 2) & 0x03);
stm32w_info->option_bytes.RDP = (optiondata & (1 << OPT_READOUT)) ? 0xFFFF : 0x5AA5;

if (optiondata & (1 << OPT_READOUT))
  LOG_INFO("Device Security Bit Set");

/* each bit refers to a 4bank protection */
retval = target_read_u32(target, EM357_FLASH_WRPR, &optiondata);
if (retval != ERROR_OK)
  return retval;

stm32w_info->option_bytes.protection[0] = (ut_int16)optiondata;
stm32w_info->option_bytes.protection[1] = (ut_int16)(optiondata >> 8);
stm32w_info->option_bytes.protection[2] = (ut_int16)(optiondata >> 16);

return ERROR_OK;
}

static int stm32w_erase_options(struct flash_bank *bank)
{
struct stm32w_flash_bank *stm32w_info = NULL;
struct target *target = bank->target;

stm32w_info = bank->driver_priv;

/* read current options */
stm32w_read_options(bank);

/* unlock flash registers */
int retval = target_write_u32(target, EM357_FLASH_KEYR, KEY1);
if (retval != ERROR_OK)
  return retval;

retval = target_write_u32(target, EM357_FLASH_KEYR, KEY2);
if (retval != ERROR_OK)
  return retval;

/* unlock option flash registers */
retval = target_write_u32(target, EM357_FLASH_OPTKEYR, KEY1);
if (retval != ERROR_OK)
  return retval;
retval = target_write_u32(target, EM357_FLASH_OPTKEYR, KEY2);
if (retval != ERROR_OK)
  return retval;

/* erase option bytes */
retval = target_write_u32(target, EM357_FLASH_CR, FLASH_OPTER | FLASH_OPTWRE);
if (retval != ERROR_OK)
  return retval;
retval = target_write_u32(target, EM357_FLASH_CR, FLASH_OPTER | FLASH_STRT | FLASH_OPTWRE);
if (retval != ERROR_OK)
  return retval;

retval = stm32w_wait_status_busy(bank, 10);
if (retval != ERROR_OK)
  return retval;

/* clear readout protection and complementary option bytes
 * this will also force a device unlock if set */
stm32w_info->option_bytes.RDP = 0x5AA5;

return ERROR_OK;
}

static int stm32w_write_options(struct flash_bank *bank)
{
struct stm32w_flash_bank *stm32w_info = NULL;
struct target *target = bank->target;

stm32w_info = bank->driver_priv;

/* unlock flash registers */
int retval = target_write_u32(target, EM357_FLASH_KEYR, KEY1);
if (retval != ERROR_OK)
  return retval;
retval = target_write_u32(target, EM357_FLASH_KEYR, KEY2);
if (retval != ERROR_OK)
  return retval;

/* unlock option flash registers */
retval = target_write_u32(target, EM357_FLASH_OPTKEYR, KEY1);
if (retval != ERROR_OK)
  return retval;
retval = target_write_u32(target, EM357_FLASH_OPTKEYR, KEY2);
if (retval != ERROR_OK)
  return retval;

/* program option bytes */
retval = target_write_u32(target, EM357_FLASH_CR, FLASH_OPTPG | FLASH_OPTWRE);
if (retval != ERROR_OK)
  return retval;

retval = stm32w_wait_status_busy(bank, 10);
if (retval != ERROR_OK)
  return retval;

/* write protection byte 1 */
retval = target_write_u16(target, EM357_OB_WRP0, stm32w_info->option_bytes.protection[0]);
if (retval != ERROR_OK)
  return retval;

retval = stm32w_wait_status_busy(bank, 10);
if (retval != ERROR_OK)
  return retval;

/* write protection byte 2 */
retval = target_write_u16(target, EM357_OB_WRP1, stm32w_info->option_bytes.protection[1]);
if (retval != ERROR_OK)
  return retval;

retval = stm32w_wait_status_busy(bank, 10);
if (retval != ERROR_OK)
  return retval;

/* write protection byte 3 */
retval = target_write_u16(target, EM357_OB_WRP2, stm32w_info->option_bytes.protection[2]);
if (retval != ERROR_OK)
  return retval;

retval = stm32w_wait_status_busy(bank, 10);
if (retval != ERROR_OK)
  return retval;

/* write readout protection bit */
retval = target_write_u16(target, EM357_OB_RDP, stm32w_info->option_bytes.RDP);
if (retval != ERROR_OK)
  return retval;

retval = stm32w_wait_status_busy(bank, 10);
if (retval != ERROR_OK)
  return retval;

retval = target_write_u32(target, EM357_FLASH_CR, FLASH_LOCK);
if (retval != ERROR_OK)
  return retval;

return ERROR_OK;
}

static int stm32w_protect_check(struct flash_bank *bank)
{
struct target *target = bank->target;
struct stm32w_flash_bank *stm32w_info = bank->driver_priv;

t_u32 protection;
int i, s;
int num_bits;
int set;

if (target->state != TARGET_HALTED)
{
  LOG_ERROR("Target not halted");
  return ERROR_TARGET_NOT_HALTED;
}

/* each bit refers to a 4bank protection (bit 0-23) */
int retval = target_read_u32(target, EM357_FLASH_WRPR, &protection);
if (retval != ERROR_OK)
  return retval;

/* each protection bit is for 4 * 2K pages */
num_bits = (bank->num_sectors / stm32w_info->ppage_size);

for (i = 0; i < num_bits; i++)
{
  set = 1;
  if (protection & (1 << i))
    set = 0;

  for (s = 0; s < stm32w_info->ppage_size; s++)
    bank->sectors[(i * stm32w_info->ppage_size) + s].is_protected = set;
}

return ERROR_OK;
}

static int stm32w_erase(struct flash_bank *bank, int first, int last)
{
struct target *target = bank->target;
int i;

if (bank->target->state != TARGET_HALTED)
{
  LOG_ERROR("Target not halted");
  return ERROR_TARGET_NOT_HALTED;
}

if ((first == 0) && (last == (bank->num_sectors - 1)))
{
  return stm32w_mass_erase(bank);
}

/* unlock flash registers */
int retval = target_write_u32(target, EM357_FLASH_KEYR, KEY1);
if (retval != ERROR_OK)
  return retval;
retval = target_write_u32(target, EM357_FLASH_KEYR, KEY2);
if (retval != ERROR_OK)
  return retval;

for (i = first; i <= last; i++)
{
  retval = target_write_u32(target, EM357_FLASH_CR, FLASH_PER);
  if (retval != ERROR_OK)
    return retval;
  retval = target_write_u32(target, EM357_FLASH_AR,
      bank->base + bank->sectors[i].offset);
  if (retval != ERROR_OK)
    return retval;
  retval = target_write_u32(target, EM357_FLASH_CR, FLASH_PER | FLASH_STRT);
  if (retval != ERROR_OK)
    return retval;

  retval = stm32w_wait_status_busy(bank, 100);
  if (retval != ERROR_OK)
    return retval;

  bank->sectors[i].is_erased = 1;
}

retval = target_write_u32(target, EM357_FLASH_CR, FLASH_LOCK);
if (retval != ERROR_OK)
  return retval;

return ERROR_OK;
}

static int stm32w_protect(struct flash_bank *bank, int set, int first, int last)
{
struct stm32w_flash_bank *stm32w_info = NULL;
struct target *target = bank->target;
ut_int16 prot_reg[4] = {0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF};
int i, reg, bit;
int status;
t_u32 protection;

stm32w_info = bank->driver_priv;

if (target->state != TARGET_HALTED)
{
  LOG_ERROR("Target not halted");
  return ERROR_TARGET_NOT_HALTED;
}

if ((first % stm32w_info->ppage_size) != 0)
{
  LOG_WARNING("aligned start protect sector to a %d sector boundary",
      stm32w_info->ppage_size);
  first = first - (first % stm32w_info->ppage_size);
}
if (((last + 1) % stm32w_info->ppage_size) != 0)
{
  LOG_WARNING("aligned end protect sector to a %d sector boundary",
      stm32w_info->ppage_size);
  last++;
  last = last - (last % stm32w_info->ppage_size);
  last--;
}

/* each bit refers to a 4bank protection */
int retval = target_read_u32(target, EM357_FLASH_WRPR, &protection);
if (retval != ERROR_OK)
  return retval;

prot_reg[0] = (ut_int16)protection;
prot_reg[1] = (ut_int16)(protection >> 8);
prot_reg[2] = (ut_int16)(protection >> 16);

for (i = first; i <= last; i++)
{
  reg = (i / stm32w_info->ppage_size) / 8;
  bit = (i / stm32w_info->ppage_size) - (reg * 8);

  LOG_WARNING("reg, bit: %d, %d", reg, bit);
  if (set)
    prot_reg[reg] &= ~(1 << bit);
  else
    prot_reg[reg] |= (1 << bit);
}

if ((status = stm32w_erase_options(bank)) != ERROR_OK)
  return status;

stm32w_info->option_bytes.protection[0] = prot_reg[0];
stm32w_info->option_bytes.protection[1] = prot_reg[1];
stm32w_info->option_bytes.protection[2] = prot_reg[2];

return stm32w_write_options(bank);
}

static int stm32w_write_block(struct flash_bank *bank, t_u8 *buffer,
  t_u32 offset, t_u32 count)
{
struct stm32w_flash_bank *stm32w_info = bank->driver_priv;
struct target *target = bank->target;
t_u32 buffer_size = 16384;
struct working_area *source;
t_u32 address = bank->base + offset;
struct reg_param reg_params[4];
struct armv7m_algorithm armv7m_info;
int retval = ERROR_OK;

/* see contib/loaders/flash/stm32x.s for src, the same is used here except for 
 * a modified *_FLASH_BASE */

static const t_u8 stm32w_flash_write_code[] = {
                /* #define EM357_FLASH_CR_OFFSET	0x10 */
                /* #define EM357_FLASH_SR_OFFSET	0x0C */
                /* write: */
  0x08, 0x4c,					/* ldr	r4, EM357_FLASH_BASE */
  0x1c, 0x44,					/* add	r4, r3 */
                /* write_half_word: */
  0x01, 0x23,					/* movs	r3, #0x01 */
  0x23, 0x61,					/* str	r3, [r4, #EM357_FLASH_CR_OFFSET] */
  0x30, 0xf8, 0x02, 0x3b,		/* ldrh	r3, [r0], #0x02 */
  0x21, 0xf8, 0x02, 0x3b,		/* strh	r3, [r1], #0x02 */
                /* busy: */
  0xe3, 0x68,					/* ldr	r3, [r4, #EM357_FLASH_SR_OFFSET] */
  0x13, 0xf0, 0x01, 0x0f,		/* tst	r3, #0x01 */
  0xfb, 0xd0,					/* beq	busy */
  0x13, 0xf0, 0x14, 0x0f,		/* tst	r3, #0x14 */
  0x01, 0xd1,					/* bne	exit */
  0x01, 0x3a,					/* subs	r2, r2, #0x01 */
  0xf0, 0xd1,					/* bne	write_half_word */
                /* exit: */
  0x00, 0xbe,					/* bkpt	#0x00 */
  0x00, 0x80, 0x00, 0x40,		/* EM357_FLASH_BASE: .word 0x40008000 */
};

/* flash write code */
if (target_alloc_working_area(target, sizeof(stm32w_flash_write_code),
    &stm32w_info->write_algorithm) != ERROR_OK)
{
  LOG_WARNING("no working area available, can't do block memory writes");
  return ERROR_TARGET_RESOURCE_NOT_AVAILABLE;
};

if ((retval = target_write_buffer(target, stm32w_info->write_algorithm->address,
    sizeof(stm32w_flash_write_code),
    (t_u8*)stm32w_flash_write_code)) != ERROR_OK)
  return retval;

/* memory buffer */
while (target_alloc_working_area_try(target, buffer_size, &source) != ERROR_OK)
{
  buffer_size /= 2;
  if (buffer_size <= 256)
  {
    /* if we already allocated the writing code, but failed to get a
     * buffer, free the algorithm */
    if (stm32w_info->write_algorithm)
      target_free_working_area(target, stm32w_info->write_algorithm);

    LOG_WARNING("no large enough working area available, can't do block memory writes");
    return ERROR_TARGET_RESOURCE_NOT_AVAILABLE;
  }
};

armv7m_info.common_magic = ARMV7M_COMMON_MAGIC;
armv7m_info.core_mode = ARMV7M_MODE_ANY;

init_reg_param(&reg_params[0], "r0", 32, PARAM_OUT);
init_reg_param(&reg_params[1], "r1", 32, PARAM_OUT);
init_reg_param(&reg_params[2], "r2", 32, PARAM_OUT);
init_reg_param(&reg_params[3], "r3", 32, PARAM_IN_OUT);

while (count > 0)
{
  t_u32 thisrun_count = (count > (buffer_size / 2)) ?
      (buffer_size / 2) : count;

  if ((retval = target_write_buffer(target, source->address,
      thisrun_count * 2, buffer)) != ERROR_OK)
    break;

  buf_set_u32(reg_params[0].value, 0, 32, source->address);
  buf_set_u32(reg_params[1].value, 0, 32, address);
  buf_set_u32(reg_params[2].value, 0, 32, thisrun_count);
  buf_set_u32(reg_params[3].value, 0, 32, 0);

  if ((retval = target_run_algorithm(target, 0, NULL, 4, reg_params,
      stm32w_info->write_algorithm->address,
      0,
      10000, &armv7m_info)) != ERROR_OK)
  {
    LOG_ERROR("error executing stm32w flash write algorithm");
    break;
  }

  if (buf_get_u32(reg_params[3].value, 0, 32) & FLASH_PGERR)
  {
    LOG_ERROR("flash memory not erased before writing");
    /* Clear but report errors */
    target_write_u32(target, EM357_FLASH_SR, FLASH_PGERR);
    retval = ERROR_FAIL;
    break;
  }

  if (buf_get_u32(reg_params[3].value, 0, 32) & FLASH_WRPRTERR)
  {
    LOG_ERROR("flash memory write protected");
    /* Clear but report errors */
    target_write_u32(target, EM357_FLASH_SR, FLASH_WRPRTERR);
    retval = ERROR_FAIL;
    break;
  }

  buffer += thisrun_count * 2;
  address += thisrun_count * 2;
  count -= thisrun_count;
}

target_free_working_area(target, source);
target_free_working_area(target, stm32w_info->write_algorithm);

destroy_reg_param(&reg_params[0]);
destroy_reg_param(&reg_params[1]);
destroy_reg_param(&reg_params[2]);
destroy_reg_param(&reg_params[3]);

return retval;
}

static int stm32w_write(struct flash_bank *bank, t_u8 *buffer,
  t_u32 offset, t_u32 count)
{
struct target *target = bank->target;
t_u32 words_remaining = (count / 2);
t_u32 bytes_remaining = (count & 0x00000001);
t_u32 address = bank->base + offset;
t_u32 bytes_written = 0;
int retval;

if (bank->target->state != TARGET_HALTED)
{
  LOG_ERROR("Target not halted");
  return ERROR_TARGET_NOT_HALTED;
}

if (offset & 0x1)
{
  LOG_WARNING("offset 0x%" PRIx32 " breaks required 2-byte alignment", offset);
  return ERROR_FLASH_DST_BREAKS_ALIGNMENT;
}

/* unlock flash registers */
retval = target_write_u32(target, EM357_FLASH_KEYR, KEY1);
if (retval != ERROR_OK)
  return retval;
retval = target_write_u32(target, EM357_FLASH_KEYR, KEY2);
if (retval != ERROR_OK)
  return retval;

/* multiple half words (2-byte) to be programmed? */
if (words_remaining > 0)
{
  /* try using a block write */
  if ((retval = stm32w_write_block(bank, buffer, offset, words_remaining)) != ERROR_OK)
  {
    if (retval == ERROR_TARGET_RESOURCE_NOT_AVAILABLE)
    {
      /* if block write failed (no sufficient working area),
       * we use normal (slow) single dword accesses */
      LOG_WARNING("couldn't use block writes, falling back to single memory accesses");
    }
  }
  else
  {
    buffer += words_remaining * 2;
    address += words_remaining * 2;
    words_remaining = 0;
  }
}

if ((retval != ERROR_OK) && (retval != ERROR_TARGET_RESOURCE_NOT_AVAILABLE))
  return retval;

while (words_remaining > 0)
{
  ut_int16 value;
  memcpy(&value, buffer + bytes_written, sizeof(ut_int16));

  retval = target_write_u32(target, EM357_FLASH_CR, FLASH_PG);
  if (retval != ERROR_OK)
    return retval;
  retval = target_write_u16(target, address, value);
  if (retval != ERROR_OK)
    return retval;

  retval = stm32w_wait_status_busy(bank, 5);
  if (retval != ERROR_OK)
    return retval;

  bytes_written += 2;
  words_remaining--;
  address += 2;
}

if (bytes_remaining)
{
  ut_int16 value = 0xffff;
  memcpy(&value, buffer + bytes_written, bytes_remaining);

  retval = target_write_u32(target, EM357_FLASH_CR, FLASH_PG);
  if (retval != ERROR_OK)
    return retval;
  retval = target_write_u16(target, address, value);
  if (retval != ERROR_OK)
    return retval;

  retval = stm32w_wait_status_busy(bank, 5);
  if (retval != ERROR_OK)
    return retval;
}

return target_write_u32(target, EM357_FLASH_CR, FLASH_LOCK);
}

static int stm32w_probe(struct flash_bank *bank)
{
struct target *target = bank->target;
struct stm32w_flash_bank *stm32w_info = bank->driver_priv;
int i;
ut_int16 num_pages;
t_u32 device_id;
int page_size;
t_u32 base_address = 0x08000000;
int retval;

stm32w_info->probed = 0;

/* Enable FPEC CLK */
retval = target_write_u32(target, EM357_FPEC_CLK, 0x00000001);
if (retval != ERROR_OK)
  return retval;

/* read stm32 device id register */
retval = target_read_u32(target, 0xE0042000, &device_id);
if (retval != ERROR_OK)
  return retval;
LOG_INFO("device id = 0x%08" PRIx32 "", device_id);

/* get flash size from target. */
retval = target_read_u16(target, 0x1FFFF7E0, &num_pages);
if (retval != ERROR_OK)
{
  LOG_WARNING("failed reading flash size, default to max target family");
  /* failed reading flash size, default to max target family */
  num_pages = 0xffff;
}

page_size = 1024;
stm32w_info->ppage_size = 4;
num_pages = 128;

LOG_INFO("flash size = %dkbytes", num_pages*page_size/1024);

if (bank->sectors)
{
  free(bank->sectors);
  bank->sectors = NULL;
}

bank->base = base_address;
bank->size = (num_pages * page_size);
bank->num_sectors = num_pages;
bank->sectors = malloc(sizeof(struct flash_sector) * num_pages);

for (i = 0; i < num_pages; i++)
{
  bank->sectors[i].offset = i * page_size;
  bank->sectors[i].size = page_size;
  bank->sectors[i].is_erased = -1;
  bank->sectors[i].is_protected = 1;
}

stm32w_info->probed = 1;

return ERROR_OK;
}

static int stm32w_auto_probe(struct flash_bank *bank)
{
struct stm32w_flash_bank *stm32w_info = bank->driver_priv;
if (stm32w_info->probed)
  return ERROR_OK;
return stm32w_probe(bank);
}


static int get_stm32w_info(struct flash_bank *bank, char *buf, int buf_size)
{
int printed;
printed = snprintf(buf, buf_size, "stm32w\n");
buf += printed;
buf_size -= printed;
return ERROR_OK;
}

COMMAND_HANDLER(stm32w_handle_lock_command)
{
struct target *target = NULL;
struct stm32w_flash_bank *stm32w_info = NULL;

if (CMD_ARGC < 1)
{
  command_print(CMD_CTX, "stm32w lock <bank>");
  return ERROR_OK;
}

struct flash_bank *bank;
int retval = CALL_COMMAND_HANDLER(flash_command_get_bank, 0, &bank);
if (ERROR_OK != retval)
  return retval;

stm32w_info = bank->driver_priv;

target = bank->target;

if (target->state != TARGET_HALTED)
{
  LOG_ERROR("Target not halted");
  return ERROR_TARGET_NOT_HALTED;
}

if (stm32w_erase_options(bank) != ERROR_OK)
{
  command_print(CMD_CTX, "stm32w failed to erase options");
  return ERROR_OK;
}

/* set readout protection */
stm32w_info->option_bytes.RDP = 0;

if (stm32w_write_options(bank) != ERROR_OK)
{
  command_print(CMD_CTX, "stm32w failed to lock device");
  return ERROR_OK;
}

command_print(CMD_CTX, "stm32w locked");

return ERROR_OK;
}

COMMAND_HANDLER(stm32w_handle_unlock_command)
{
struct target *target = NULL;

if (CMD_ARGC < 1)
{
  command_print(CMD_CTX, "stm32w unlock <bank>");
  return ERROR_OK;
}

struct flash_bank *bank;
int retval = CALL_COMMAND_HANDLER(flash_command_get_bank, 0, &bank);
if (ERROR_OK != retval)
  return retval;

target = bank->target;

if (target->state != TARGET_HALTED)
{
  LOG_ERROR("Target not halted");
  return ERROR_TARGET_NOT_HALTED;
}

if (stm32w_erase_options(bank) != ERROR_OK)
{
  command_print(CMD_CTX, "stm32w failed to unlock device");
  return ERROR_OK;
}

if (stm32w_write_options(bank) != ERROR_OK)
{
  command_print(CMD_CTX, "stm32w failed to lock device");
  return ERROR_OK;
}

command_print(CMD_CTX, "stm32w unlocked.\n"
    "INFO: a reset or power cycle is required "
    "for the new settings to take effect.");

return ERROR_OK;
}

static int stm32w_mass_erase(struct flash_bank *bank)
{
struct target *target = bank->target;

if (target->state != TARGET_HALTED)
{
  LOG_ERROR("Target not halted");
  return ERROR_TARGET_NOT_HALTED;
}

/* unlock option flash registers */
int retval = target_write_u32(target, EM357_FLASH_KEYR, KEY1);
if (retval != ERROR_OK)
  return retval;
retval = target_write_u32(target, EM357_FLASH_KEYR, KEY2);
if (retval != ERROR_OK)
  return retval;

/* mass erase flash memory */
retval = target_write_u32(target, EM357_FLASH_CR, FLASH_MER);
if (retval != ERROR_OK)
  return retval;
retval = target_write_u32(target, EM357_FLASH_CR, FLASH_MER | FLASH_STRT);
if (retval != ERROR_OK)
  return retval;

retval = stm32w_wait_status_busy(bank, 100);
if (retval != ERROR_OK)
  return retval;

retval = target_write_u32(target, EM357_FLASH_CR, FLASH_LOCK);
if (retval != ERROR_OK)
  return retval;

return ERROR_OK;
}

COMMAND_HANDLER(stm32w_handle_mass_erase_command)
{
int i;

if (CMD_ARGC < 1)
{
  command_print(CMD_CTX, "stm32w mass_erase <bank>");
  return ERROR_OK;
}

struct flash_bank *bank;
int retval = CALL_COMMAND_HANDLER(flash_command_get_bank, 0, &bank);
if (ERROR_OK != retval)
  return retval;

retval = stm32w_mass_erase(bank);
if (retval == ERROR_OK)
{
  /* set all sectors as erased */
  for (i = 0; i < bank->num_sectors; i++)
  {
    bank->sectors[i].is_erased = 1;
  }

  command_print(CMD_CTX, "stm32w mass erase complete");
}
else
{
  command_print(CMD_CTX, "stm32w mass erase failed");
}

return retval;
}

static const struct command_registration stm32w_exec_command_handlers[] = {
{
  .name = "lock",
  .handler = stm32w_handle_lock_command,
  .mode = COMMAND_EXEC,
  .usage = "bank_id",
  .help = "Lock entire flash device.",
},
{
  .name = "unlock",
  .handler = stm32w_handle_unlock_command,
  .mode = COMMAND_EXEC,
  .usage = "bank_id",
  .help = "Unlock entire protected flash device.",
},
{
  .name = "mass_erase",
  .handler = stm32w_handle_mass_erase_command,
  .mode = COMMAND_EXEC,
  .usage = "bank_id",
  .help = "Erase entire flash device.",
},
COMMAND_REGISTRATION_DONE
};

static const struct command_registration stm32w_command_handlers[] = {
{
  .name = "stm32w",
  .mode = COMMAND_ANY,
  .help = "stm32w flash command group",
  .chain = stm32w_exec_command_handlers,
},
COMMAND_REGISTRATION_DONE
};

struct flash_driver stm32w_flash = {
.name = "stm32w",
.commands = stm32w_command_handlers,
.flash_bank_command = stm32w_flash_bank_command,
.erase = stm32w_erase,
.protect = stm32w_protect,
.write = stm32w_write,
.read = default_flash_read,
.probe = stm32w_probe,
.auto_probe = stm32w_auto_probe,
.erase_check = default_flash_mem_blank_check,
.protect_check = stm32w_protect_check,
.info = get_stm32w_info,
};

END_OF_SOURCE
#}
      fi #}
    fi #}
    
    IncludeDir="`pwd`/../../999_libftdi/"
    OK=""
    #./configure  --prefix=$PrefixOO --enable-usbprog --enable-ioutil --enable-oocd_trace --enable-jlink --enable-ft2232_ftd2xx && make -j2 && sudo make install && touch Install_OK
    # --enable-maintainer-mode  
    # --enable-stlink (implementation broken)
    DeprecatedOptions=""
    if [ "$Version_OpenOCD" == "0.5.0" ]; then #{
      DeprecatedOptions="--enable-ft2232_libftdi"
    fi #}
    if [ "$Version_OpenOCD" == "0.6.1" ]; then #{
      DeprecatedOptions="--enable-ft2232_libftdi"
    fi #}
    if [ "$Version_OpenOCD" == "0.7.0" ]; then #{
      DeprecatedOptions="--enable-ft2232_libftdi"
    fi #}
    
    Options="--prefix=$PrefixOO --enable-ioutil --enable-oocd_trace --enable-jlink --enable-stlink"
    Options="$Options --enable-ftdi $DeprecatedOptions"
    #Options="$Options --with_ftd2xx --enable-ft2232_ftd2xx --with_ftd2xx_linux_tardir=../../999_libftd2xx/"
    if [ -x "bootstrap" ]; then
      echo "`pwd`/bootstrap" >configure.log
      ./bootstrap 2>&1      >>configure.log
    fi
    
    echo "`pwd` > ./configure $Options" >>configure.log
    echo ""                             >>configure.log
    echo "`pwd` > ./configure $Options"
    ./configure $Options >>configure.log 2>&1  && OK="1"
        
    if [ "$OK" == "" ]; then
      echo "Error during configuring ${DirName}!"
      exit 10
    fi
    for Makefile in $( find ./ -name Makefile ); do
      echo "replacing Werror in $Makefile .."
      replaceInFile $Makefile "-Werror" " "
    done
    if [ "$AmountCPUs" == "" ]; then
      AmountCPUs="1"
    fi
    replaceInFile src/jtag/drivers/libusb1_common.c "if (!matched)" 'if (!matched ) LOG_DEBUG("\\nFound serial number \\"\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\" (%s)\\n!= requested serial \\"\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\\\x%02x\\" (%s)", (unsigned char) desc_string[0], (unsigned char) desc_string[1], (unsigned char) desc_string[2], (unsigned char) desc_string[3], (unsigned char) desc_string[4], (unsigned char) desc_string[5], (unsigned char) desc_string[6], (unsigned char) desc_string[7], (unsigned char) desc_string[8], (unsigned char) desc_string[9], (unsigned char) desc_string[10], (unsigned char) desc_string[11], desc_string, (unsigned char) string[0], (unsigned char) string[1], (unsigned char) string[2], (unsigned char) string[3], (unsigned char) string[4], (unsigned char) string[5], (unsigned char) string[6], (unsigned char) string[7], (unsigned char) string[8], (unsigned char) string[9], (unsigned char) string[10], (unsigned char) string[11], string ); if (0)	//TTC: print more usefull debug message (serial number is printed in same format as required for config files)'
    AmountJobs=$(( $AmountCPUs * 2 ))
    echo "make -j${AmountJobs}  >make.log..."      && make -j$AmountJobs >make.log 2>&1 && \
    echo "pdf       >pdf.log..."                   && make pdf -j$AmountJobs >pdf.log 2>&1 ; \
    echo "install   >install.log..."               && sudo make install >install.log 2>&1 && \
    touch Install_OK

    if [ ! -e Install_OK ]; then #{
      echo "Error during compiling ${DirName}!"
      echo "Check log files:"
      find `pwd`/ -name "*.log" -exec echo "  less {}" \;
      find `pwd`/ -name "*.err" -exec echo "  less {}" \;
      exit 10
    fi #}
      
    cd ..
    touch OK.Recompiled
  fi #}
    
  #? Chowning openocd creates problems in using it later..
  #? sudo chown -R $USER: .
  sudo chown -R $USER: share/openocd/scripts
  Binary="$PrefixOO/bin/openocd"
  if [ -e $Binary ]; then #{
      IsInstalled=`$Binary --version  2>&1 | grep Debugger`
      if [ "$IsInstalled" != "" ]; then
        sudo chmod 4755 $Binary
        echo "Installed version: $IsInstalled"
        addLine ../../cleanup.sh "rm -Rf `pwd`/${DirName}/"
        cd "$Install_Path"
        
        echo "" >OK.Install
      else
        echo "$0 ERROR: `$Binary --version  2>&1 `"
      fi
  else
    echo "$0 - ERROR: Cannot find binary '$Binary'!"
  fi #}
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then #{

  cd "$Install_Path"  
  OpenOCD_Folder=`find ./ -maxdepth 1 -mindepth 1 -name "openocd-*" -type d`
  if [ -e "$OpenOCD_Folder/doc/openocd.pdf" ]; then
    cd "$OpenOCD_Folder/doc"
    addDocumentationFile openocd.pdf Programmer
    #X cp -v $OpenOCD_Folder/doc/*.pdf ../${DocFolder}/
    cd ../..
  fi

  ConfigFolder="$Install_Path/share/openocd/scripts/interface/"
  OldDir=`pwd`
  cd "$ConfigFolder"

  #? sudo chown $USER: . # requires sudo password on every install (even after first install)
  # ensure that current folder is writable
  #X Version_OpenOCD="$Version"
  UseFT2232=""
  if [ "$Version_OpenOCD" == "0.5.0" ]; then #{
    UseFT2232="1"
  fi #}
  if [ "$Version_OpenOCD" == "0.6.1" ]; then #{
    UseFT2232="1"
  fi #}
  if [ "$Version_OpenOCD" == "0.7.0" ]; then #{
    UseFT2232="1"
  fi #}
  
  if [ "$UseFT2232" == "1" ]; then #{ using old buggy ft2232 driver
    #{ cat
    cat <<END_OF_CFG >openglink_r11.cfg
#
# based on Joern Kaipf's OOCDLink
# extended by Gregor Rebel with additional SWD-support
#
# http://TheToolChain.com
#


interface ft2232
ft2232_device_desc "OpenGLink_r11"
ft2232_layout oocdlink
ft2232_vid_pid 0x$USB_OpenGLink_VID 0x$USB_OpenGLink_PID

adapter_khz 1000
reset_config trst_and_srst separate trst_push_pull srst_push_pull

END_OF_CFG
#}
#}
  else                             #{ using newer ftdi driver
    #{ cat
    cat <<END_OF_CFG >openglink_r11.cfg
#
# based on Joern Kaipf's OOCDLink
# extended by Gregor Rebel with additional SWD-support
#
# http://TheToolChain.com
#
# Configuration file for OpenOCD v0.8.0 and newer

interface ftdi
ftdi_vid_pid 0x$USB_OpenGLink_VID 0x$USB_OpenGLink_PID
ftdi_device_desc "OpenGLink_r11"
ftdi_layout_init 0x0508 0x0f1b
ftdi_layout_signal nTRST -data 0x0200 -noe 0x0100
ftdi_layout_signal nSRST -data 0x0800 -noe 0x0400

adapter_khz 1000
reset_config trst_and_srst separate trst_push_pull srst_push_pull

END_OF_CFG
#}
    for ConfigFile in olimex-arm-usb-ocd.cfg  olimex-arm-usb-ocd-h.cfg  olimex-arm-usb-tiny-h.cfg  olimex-jtag-tiny.cfg; do
      IsOldConfig=`grep ft2232 $ConfigFile`
      if [ "$IsOldConfig" != "" ]; then
        echo "patching old config file from ft2232->ftdi: $ConfigFile"  
        replaceInFile $ConfigFile ft2232 ftdi
        replaceInFile $ConfigFile ftdi_layout ftdi_layout_init
        replaceInFile $ConfigFile olimex-jtag "0x0508 0x0f1b"
      fi
    done
  fi #}
  
  Prefix="350_programmer_openocd_"
  InterfacesBaseDir=`pwd`
  for SubDir in "." `ls */ -d`; do
    echo "parsing interfaces in $InterfacesDir/ .."
    cd "$InterfacesBaseDir/$SubDir"
    InterfacesDir=`pwd`
    Interfaces=`ls *.cfg`
    cd "$OldDir"
    Rules="46-usb-jtag_programmers_TheToolChain.rules"
    cat <<END_OF_RULES >$Rules #{ create udev-rules file
# This file has been created by $ScriptName on `date`
#
# Note: read info of current udev device
# udevadm info -a -p $(udevadm info -q path -n ttyUSB0)

# Examples:
# ATTR{idVendor}=="15ba", ATTR{idProduct}=="0004", GROUP="dialout", MODE="0660" # Olimex Ltd. OpenOCD JTAG TINY
# ATTR{idVendor}=="15ba", ATTR{idProduct}=="002b", GROUP="dialout", MODE="0660" # Olimex Ltd. OpenOCD ARM_USB_OCD_H

# Lines below have been collected from different openocd config files
END_OF_RULES
  
  #}
    for Interface in $Interfaces; do #{
      echo "Interface '$Interface' ($InterfacesDir/$Interface)"
      InterfaceName="`perl -e \"print substr('${Interface}', 0, -4);\"`"  # remove .cfg from $Interface
      InterfaceName="`perl -e \"my \\$B='$InterfaceName'; \\$B=~s/-/_/g; print \\$B;\"`" # convert all - -> _
      Name="${Prefix}$InterfaceName"
      createExtensionMakefileHead ${Name}      #{ (create makefile)
      cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# no special settings required here

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
      createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "programmer/ debugger interface using OpenOCD: $InterfaceName" #{ (create activate script)
cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# ACTIVATE_SECTION_A remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/*350*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
#createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$ScriptName"

# update link to current flash script
createLink flash_openocd.sh _/flash_current.sh
createLink debug_openocd.sh _/debug_current.sh
createLink ../additionals/999_open_ocd/interface/$SubDir/$Interface configs/openocd_interface.cfg

END_OF_ACTIVATE
#}
      createActivateScriptTail $Name ${Dir_Extensions}
      
      ContainsVidPid=`grep -v ^# $InterfacesDir/$Interface | grep vid_pid` # check if file contains string vid_pid in a line that does not start with #
      if [ "$ContainsVidPid" != "" ]; then #{ extract data for udev rule
        VID=`echo "$ContainsVidPid" | awk '{ print \$2 }'`
        PID=`echo "$ContainsVidPid" | awk '{ print \$3 }'`
        
        # remove 0x prefix from hex numbers
        VID=`perl -e "my \\\$A='$VID'; \\\$A=~s/0x//; print lc(\\\$A);"`
        PID=`perl -e "my \\\$A='$PID'; \\\$A=~s/0x//; print lc(\\\$A);"`
        
        Description=`grep device_desc "$InterfacesDir/$Interface"`
        if [ "$Description" != "" ]; then
          Description=`perl -e "my \\\$P=index('$Description', ' '); print substr('$Description', \\\$P+1);"`
        fi
        
        # remove double quotes from description
        Description=`perl -e "my \\\$A='$Description'; \\\$A=~s/\"//g; print \\\$A;"`
        
        #echo "creating udev rule for PID=$PID VID=$VID for $Description"
        cat <<END_OF_RULE >>$Rules
ATTR{idVendor}=="$VID", ATTR{idProduct}=="$PID", GROUP="dialout", MODE="0660" # $Description
END_OF_RULE
      fi #}
    done #}

  done

  sudo cp $Rules /etc/udev/rules.d/
  sudo udevadm control --reload-rules

  Activate=`find ${Dir_Extensions} -name "activate.350_programmer_openocd_stlink_v1.sh"`
  #{
  #{
  #? replaceInFile "$Activate" "#}USER_PART" "createLink openocd_target_stlinkv1.cfg configs/openocd_target.cfg\n#}USER_PART"
  chmod +x "$Activate"
  Activate=`find ${Dir_Extensions} -name "activate.350_programmer_openocd_stlink_v2.sh"`
  #{
  #{
  #? replaceInFile "$Activate" "#}USER_PART" "createLink openocd_target_stlinkv2.cfg configs/openocd_target.cfg\n#}USER_PART"
  chmod +x "$Activate"
  
  if [ "1" == "1" ]; then #{ create special activate for OpenGLink programmer
    Name="${Prefix}openglink_r11"
    createExtensionMakefileHead ${Name}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# no special settings required here

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "JTAG programmer/ debugger using OpenOCD: OpenGLink rev1.1" #{ (create activate script)
cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

if [ "\`lsmod | grep ftdi_sio\`" == "" ]; then
  if [ "\`lsusb | grep '${USB_OpenGLink_VID}:${USB_OpenGLink_PID}'\`" == "" ]; then # OpenGlink is connected to USB port
    echo "\$ScriptName: OpenGlink not found on USB (looking for VID=${USB_OpenGLink_VID}, PID=${USB_OpenGLink_PID})"
  else
    echo "Missing kernel module ftdi_sio - reloading kernel module ftdi_sio (required to accept special USB pid-vid combination used by OpenGLink)"
    sudo modprobe -r ftdi_sio
    sleep 1
    sudo modprobe ftdi_sio
    sleep 1
  fi
fi
if [ "\`cat /sys/bus/usb-serial/drivers/ftdi_sio/new_id | grep '${USB_OpenGLink_VID} ${USB_OpenGLink_PID}'\`" == "" ]; then
  echo "Configuring ftdi_sio module to accept VID=${USB_OpenGLink_VID} PID=${USB_OpenGLink_PID}"
  sudo chmod 666 /sys/bus/usb-serial/drivers/ftdi_sio/new_id
  sudo echo "${USB_OpenGLink_VID} ${USB_OpenGLink_PID}" >/sys/bus/usb-serial/drivers/ftdi_sio/new_id
fi  

# ACTIVATE_SECTION_A remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/*${Prefix}*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
#createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$ScriptName"

# update link to current flash script
createLink flash_openocd.sh         _/flash_current.sh
createLink debug_openocd.sh         _/debug_current.sh
createLink ../additionals/999_open_ocd/interface/openglink_r11.cfg configs/openocd_interface.cfg

echo ""
echo -n "Mounted serial devices using module ftdi_sio: "
OldDir="\`pwd\`"
cd /sys/bus/usb-serial/drivers/ftdi_sio/
ls -d ttyUSB*
cd \$OldDir

if [ "`groups | grep dialout`" == "" ]; then #{ current user is missing in group dialout
  echo "\$ScriptName - WARNING: User \$USER is not a member of group dialout. This may prevent you to open serial ports."
  echo "   1) Add this user to group dialout (should already been done during installation of The ToolChain)"
  echo "   2) log out and log in again (or simply reboot)"
fi #}

END_OF_ACTIVATE
    createActivateScriptTail $Name ${Dir_Extensions}
#}
  fi #}
  
  IsAlreadyMember=`grep $USER /etc/group | grep dialout`
  if [ "$IsAlreadyMember" == "" ]; then
    echo "adding user $USER to group dialout to allow use of FTDI based JTAG programmer.."
    sudo usermod $USER -a -G dialout
  fi
  if [ "$InstallInHomeDir" == "1" ]; then #{
    createLink ../$Install_Dir/bin/openocd ../bin/openocd
    addLine $Dir_Extensions/../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir;  ln -sv \$Source/TheToolChain/InstallData/$Install_Dir/share/openocd/scripts/  $Install_Dir"

    #X addLine ../scripts/SourceMe.sh "export PATH=\"\$HOME/Source/TheToolChain/InstallData/$Install_Dir/bin/:\$PATH\" #$ScriptName"
#}
  else #{
    if [ "$InstallInHomeDir" == "1" ]; then
      createLink $HOME/Source/TheToolChain/InstallData/bin/openocd ../bin/openocd
    else
      createLink $PrefixOO/bin/openocd ../bin/openocd
    fi
    #X addLine ../scripts/SourceMe.sh "export PATH=\"$PrefixOO/bin/:\$PATH\" #$ScriptName"
  fi #}
  
  if [ "$DisabledToolChain" != "" ]; then #{ restore disabled toolchain
    echo "restoring disabled ToolChain ..."
    rm -Rf "$HOME/Source/TheToolChain"
    mv "$DisabledToolChain" "$HOME/Source/TheToolChain"
  fi #}

  if [ -e "OK.Recompiled" ]; then #{ openocd was compiled from source: create tarball for upload to mirror-server
    if [ ! -e $ArchiveOpenOCD ]; then
      echo "creating compressed archive for upload to mirror website: $ArchiveOpenOCD"  
      cd "$Install_Path"
      tar cjf $ArchiveOpenOCD *
      Script="upload2mirror_${ArchiveOpenOCD}.sh"
      cat <<END_OF_SCRIPT >$Script
#!/bin/bash

UserAtHost="\$1"
if [ "\$UserAtHost" == "" ]; then
  echo "\$ScriptName USER@SERVER"
  exit 10
fi

Dir="www/thetoolchain/mirror/${VersionTTC}"
echo "creating directory \$Dir ..."
ssh -X \${UserAtHost} "mkdir \$Dir"

echo "uploading $ArchiveOpenOCD -> \${UserAtHost}:\${Dir}/ ..." 
scp $ArchiveOpenOCD \${UserAtHost}:\${Dir}/

END_OF_SCRIPT
      chmod +x $Script
    fi
  fi #}

  echo "Installed successfully: $Install_Dir"
#}
else #{ failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..

#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
