#!/bin/bash
#
#  Install script for ST USB FS Device Library
#  Script written by Sascha Poggemann 2012.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:


source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

StartPath="`pwd`"
INSTALLPATH="400_stm32_usb_fs_device_lib"
dir "$INSTALLPATH"
cd "$INSTALLPATH"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

DocDir="../../Documentation/STM/USB_FS_Device_Library"

if [ ! -e OK.Documentation ]; then #{
  dir "$Dir"
  getFile http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/USER_MANUAL/        CD00289278.pdf UM1021_USB_On-The-Go_host_and_device_library.pdf  $DocDir/                                  || ERROR="1"

  if [ "$Error" == "" ]; then
    echo "Documentation complete: $INSTALLPATH."
    touch OK.Documentation
  fi
fi #}
if [ ! -e OK.Install ]; then #{

  if [ ! -e OK.Packages ]; then #{
    touch OK.Packages
  fi #}

  #{ Device Library ---------------------------------------------------------------------

  #old version 2.1
  Archive="stm32_f105-07_f2_f4_usb-host-device_lib.zip"
  #Version 3.4.0 Device Lib
  #Archive="stm32_usb-fs-device_lib.zip"
  
  # http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/stm32_usb-fs-device_lib.zip
  URL_Prefix="http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/"
  getFile $URL_Prefix $Archive
  
  # find name ofu only directory
  # std_peripherals_library/STM32F10x_StdPeriph_Lib_V3.4.0/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h
  

  unZIP ST-USB_FS_Device_Library $Archive || mv -f $Archive ${Archive}_bad
  rm current 2>/dev/null
  FullLibraryDir=`find ./  -maxdepth 1 -mindepth 1 -name STM32_USB-Host-Device_Lib_* -type d -print`
  if [ "$FullLibraryDir" == "" ]; then
    echo "$0 - ERROR: Cannot extract $Archive !"
    exit 10
  fi
  #X mv "$FullLibraryDir/*.chm" ../${DocDir}/
  
  echo "installed USB Driver"
  echo "start to install new USB Device Driver 3.4.0" 
  
  #Version 3.4.0 Device Lib
  Archive="stm32_usb-fs-device_lib.zip"
  
  # http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/stm32_usb-fs-device_lib.zip
  getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ $Archive
  
  # find name ofu only directory
  # std_peripherals_library/STM32F10x_StdPeriph_Lib_V3.4.0/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h
  
  LibVersion="3.4.0"
  DeviceLibDir="STM32_USB-FS-Device_Lib_V$LibVersion"
  
  ArchiveBad=`unzip -t $Archive 2>&1 1>/dev/null`
  if [ "$ArchiveBad" != "" ]; then #{
    mv -f $Archive ${Archive}_bad
    echo "$0 - ERROR: Corrupt archive $Archive !"
    exit 10
  fi #}
  unZIP $DeviceLibDir $Archive

  #{ workarounds for compile warnings + errors --------------------------------------------------------------------
  
  File="$DeviceLibDir/Libraries/STM32_USB-FS-Device_Driver/inc/usb_type.h"
  if [ ! -e ${File}_orig ]; then
    echo "patching ${File}.."
    mv -v ${File} ${File}_orig
    cat <<END_OF_SOURCE >$File #{
#ifndef __USB_TYPE_H
#define __USB_TYPE_H

/*********************************************************************
 *
 * The ToolChain 
 * written 2012 by Gregor Rebel
 *
 * Basic definitions required by USB-Library from STMicroelectronics.
 * Based on ${File}_orig
 *
 * $URL_Prefix$Archive
 *
 */


/* Includes ------------------------------------------------------------------*/
#include "usb_conf.h"
#include "ttc_basic.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#ifndef NULL
#define NULL ((void *)0)
#endif

#if !defined (__STM32F10x_H) && !defined(__STM32L1XX_H)

/*
typedef signed long      s32;
typedef signed short     s16;
typedef signed char      s8;

typedef volatile signed long      vs32;
typedef volatile signed short     vs16;
typedef volatile signed char      vs8;

typedef unsigned long       u32;
typedef unsigned short      u16;
typedef unsigned char       u8;

typedef unsigned long  const    uc32;  
typedef unsigned short const    uc16;  
typedef unsigned char  const    uc8;   

typedef volatile unsigned long      vu32;
typedef volatile unsigned short     vu16;
typedef volatile unsigned char      vu8;

typedef volatile unsigned long  const    vuc32;  
typedef volatile unsigned short const    vuc16;  
typedef volatile unsigned char  const    vuc8;   

typedef enum { RESET = 0, SET   = !RESET } FlagStatus, ITStatus;

typedef enum { DISABLE = 0, ENABLE  = !DISABLE} FunctionalState;

typedef enum { ERROR = 0, SUCCESS  = !ERROR} ErrorStatus;

*/
#endif /* __STM32F10x_H && __STM32L1XX_H */


#endif

END_OF_SOURCE
#}
  else
    echo "already patched: `pwd`/${File}"
  fi
  replaceInFile $DeviceLibDir/Libraries/STM32_USB-FS-Device_Driver/inc/otgd_fs_regs.h "uint32_t "     "unsigned "
  replaceInFile $DeviceLibDir/Libraries/STM32_USB-FS-Device_Driver/inc/otgd_fs_regs.h "unsigned d32"  "uint32_t d32"
  replaceInFile $DeviceLibDir/Libraries/STM32_USB-FS-Device_Driver/inc/otgd_fs_regs.h "__IO unsigned" "__IO uint32_t"
  replaceInFile $DeviceLibDir/Libraries/STM32_USB-FS-Device_Driver/src/otgd_fs_cal.c  "fifo = USB_OTG_FS_regs.FIFO" "fifo = (__IO uint32_t *) USB_OTG_FS_regs.FIFO"

  replaceInFile $FullLibraryDir/Libraries/STM32_USB_OTG_Driver/inc/usb_regs.h "uint32_t "     "unsigned "
  replaceInFile $FullLibraryDir/Libraries/STM32_USB_OTG_Driver/inc/usb_regs.h "unsigned d32"  "uint32_t d32"
  replaceInFile $FullLibraryDir/Libraries/STM32_USB_OTG_Driver/inc/usb_regs.h "__IO unsigned" "__IO uint32_t"
  
  replaceInFile $FullLibraryDir/Libraries/STM32_USB_HOST_Library/Core/src/usbh_core.c "String(" "String((void*)"

  for File in usb_mem usb_regs usb_int; do
    FilePath="$DeviceLibDir/Libraries/STM32_USB-FS-Device_Driver/src/${File}.c"
    if [ ! -e ${FilePath}_orig ]; then
      echo "patching `pwd`/$FilePath .."
      cp $FilePath ${FilePath}_orig
      chmod +w $FilePath
      echo "void ${File}_IS_NOT_EMPTY_TRANSLATION_UNIT() { }" >>$FilePath
    fi
  done
  
  #} workarounds for compile warnings + errors
  
  echo "finished to install new USB Device Driver v $LibVersion"
  
  dir Library_USB
  cd Library_USB
  createLink ../${DeviceLibDir}/Libraries/STM32_USB-FS-Device_Driver/ Driver
  createLink ../$FullLibraryDir/                                      Complete
  createLink ../$FullLibraryDir/Libraries/STM32_USB_HOST_Library/     Host
  createLink ../$FullLibraryDir/Libraries/STM32_USB_OTG_Driver/       OTG
  cd ..

  #? ./additionals/400_stm32_usb_fs_device_lib/Driver/inc/usb_type.h
    
#}Device Library
  #{ Host library ----------------------------------------------------------------------

  
#}Host library

  #cd ..   
  echo "" >OK.Install

fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then #{
  Folder="StdPeripheral_Examples"
  #createLink $HOME/TheToolChain/InstallData/200_cpu_stm32f1xx_std_peripherals/current/Project/STM32F10x_StdPeriph_Examples $HOME/TheToolChain/Documentation/ST-StandardPeripheralsLibrary/$Folder
  #createLink $HOME/TheToolChain/InstallData/200_cpu_stm32f1xx_std_peripherals/current/Utilities/STM32_EVAL/Common          $HOME/TheToolChain/Documentation/ST-StandardPeripheralsLibrary/$Folder/Common
  
  Name="${INSTALLPATH}"
  createExtensionMakefileHead ${Name} #{ USB OTG FS PHY CONFIGURATION
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

COMPILE_OPTS += -DEXTENSION_${Name}=1




#/****************** USB OTG FS PHY CONFIGURATION *******************************
#*  The USB OTG FS Core supports one on-chip Full Speed PHY.
#*  
#*  The USE_EMBEDDED_PHY symbol is defined in the project compiler preprocessor 
#*  when FS core is used.
#*******************************************************************************/

#COMPILE_OPTS += -DUSE_USB_OTG_FS


#/****************** USB OTG HS PHY CONFIGURATION *******************************
#*  The USB OTG HS Core supports two PHY interfaces:
#*   (i)  An ULPI interface for the external High Speed PHY: the USB HS Core will 
#*        operate in High speed mode
#*   (ii) An on-chip Full Speed PHY: the USB HS Core will operate in Full speed mode
#*
#*  You can select the PHY to be used using one of these two defines:
#*   (i)  USE_ULPI_PHY: if the USB OTG HS Core is to be used in High speed mode 
#*   (ii) USE_EMBEDDED_PHY: if the USB OTG HS Core is to be used in Full speed mode
#*
#*  Notes: 
#*   - The USE_ULPI_PHY symbol is defined in the project compiler preprocessor as 
#*     default PHY when HS core is used.
#*   - On STM322xG-EVAL and STM324xG-EVAL boards, only configuration(i) is available.
#*     Configuration (ii) need a different hardware, for more details refer to your
#*     STM32 device datasheet.
#*******************************************************************************/


COMPILE_OPTS += -DUSE_USB_OTG_FS
#COMPILE_OPTS += -DUSE_USB_OTG_HS
##COMPILE_OPTS += -DUSE_ULPI_PHY
COMPILE_OPTS += -DUSE_EMBEDDED_PHY


COMPILE_OPTS += -DEVAL_COM1_IRQn=USART3_IRQn
COMPILE_OPTS += -DEVAL_COM1=USART3

#COMPILE_OPTS += -DUSB_DISCONNECT=GPIOD  
#COMPILE_OPTS += -DUSB_DISCONNECT_PIN=GPIO_Pin_9
#COMPILE_OPTS += -DRCC_APB2Periph_GPIO_DISCONNECT=RCC_APB2Periph_GPIOD
#COMPILE_OPTS += -DEVAL_COM1_IRQHandler=USART1_IRQHandler 


COMPILE_OPTS += -DUSE_STM3210C_EVAL=1 # hw_config.c:448 Problem 


############Configuration Files ${Name}################
#must be provided by example or USB Impelementation  
		
		
###############Lib Version 3.4 #################
###############required Paths  ################# 
INCLUDE_DIRS += -I additionals/400_stm32_usb_fs_device_lib/Driver/inc/

vpath %.c additionals/400_stm32_usb_fs_device_lib/Driver/src/

###############Lib Version 3.4 #################
###############required Objects #################

MAIN_OBJS += 	otgd_fs_cal.o \\
		otgd_fs_dev.o \\
		otgd_fs_int.o \\
		otgd_fs_pcd.o \\
		usb_core.o \\
		usb_init.o \\
		usb_int.o \\
		usb_mem.o \\
		usb_regs.o \\
		usb_sil.o  \\
		

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ #{

  # add createLinks for each link required for this extension
  echo "createLink \$ScriptPath/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ../extensions/ 
  #}

  Name="400_stm32_usb_fs_host_lib"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

COMPILE_OPTS += -DEXTENSION_${Name}=1




#/****************** USB OTG FS PHY CONFIGURATION *******************************
#*  The USB OTG FS Core supports one on-chip Full Speed PHY.
#*  
#*  The USE_EMBEDDED_PHY symbol is defined in the project compiler preprocessor 
#*  when FS core is used.
#*******************************************************************************/

#COMPILE_OPTS += -DUSE_USB_OTG_FS


#/****************** USB OTG HS PHY CONFIGURATION *******************************
#*  The USB OTG HS Core supports two PHY interfaces:
#*   (i)  An ULPI interface for the external High Speed PHY: the USB HS Core will 
#*        operate in High speed mode
#*   (ii) An on-chip Full Speed PHY: the USB HS Core will operate in Full speed mode
#*
#*  You can select the PHY to be used using one of these two defines:
#*   (i)  USE_ULPI_PHY: if the USB OTG HS Core is to be used in High speed mode 
#*   (ii) USE_EMBEDDED_PHY: if the USB OTG HS Core is to be used in Full speed mode
#*
#*  Notes: 
#*   - The USE_ULPI_PHY symbol is defined in the project compiler preprocessor as 
#*     default PHY when HS core is used.
#*   - On STM322xG-EVAL and STM324xG-EVAL boards, only configuration(i) is available.
#*     Configuration (ii) need a different hardware, for more details refer to your
#*     STM32 device datasheet.
#*******************************************************************************/


#COMPILE_OPTS += -DUSE_USB_OTG_HS 
COMPILE_OPTS  += -DUSE_USB_OTG_FS
##COMPILE_OPTS += -DUSE_ULPI_PHY
COMPILE_OPTS += -DUSE_EMBEDDED_PHY


COMPILE_OPTS += -DEVAL_COM1_IRQn=USART3_IRQn
COMPILE_OPTS += -DEVAL_COM1=USART3

#COMPILE_OPTS += -DUSB_DISCONNECT=GPIOD  
#COMPILE_OPTS += -DUSB_DISCONNECT_PIN=GPIO_Pin_9
#COMPILE_OPTS += -DRCC_APB2Periph_GPIO_DISCONNECT=RCC_APB2Periph_GPIOD
#COMPILE_OPTS += -DEVAL_COM1_IRQHandler=USART1_IRQHandler 


#COMPILE_OPTS += -DUSE_STM3210C_EVAL=1 # hw_config.c:448 Problem 
COMPILE_OPTS += -DUSE_STM3210C_EVAL=1

############Configuration Files ${Name}################
#must be provided by example or USB Impelementation  
		
		
###############Lib Version 2.1 #################
###############required Paths  ################# 
INCLUDE_DIRS += -I additionals/400_stm32_usb_fs_device_lib/Host/Core/inc/
                   
vpath %.c additionals/400_stm32_usb_fs_device_lib/Host/Core/src/

INCLUDE_DIRS += -I additionals/400_stm32_usb_fs_device_lib/Host/Class/HID/inc/

vpath %.c additionals/400_stm32_usb_fs_device_lib/Host/Class/HID/src/

INCLUDE_DIRS += -I additionals/400_stm32_usb_fs_device_lib/Host/Class/MSC/inc/

vpath %.c additionals/400_stm32_usb_fs_device_lib/Host/Class/MSC/src/

################OTG Driver 
##################################################################


INCLUDE_DIRS += -I additionals/400_stm32_usb_fs_device_lib/OTG/inc/

vpath %.c additionals/400_stm32_usb_fs_device_lib/OTG/src/


###############Lib Version 3.4 #################
###############required Objects #################

################USB Host Class
MAIN_OBJS += 	usbh_hid_core.o \\
		usbh_hid_keybd.o \\
		usbh_hid_mouse.o \\
		
###############USB Host Core		
MAIN_OBJS += 	usbh_core.o \\
		usbh_hcs.o \\
		usbh_ioreq.o \\
		usbh_stdreq.o \\

MAIN_OBJS += 	usb_core.o \\
#		usb_dcd.o \\
#		usb_dcd_int.o \\

MAIN_OBJS +=	usb_hcd.o \\
		usb_hcd_int.o \\
#		usb_otg.o \\

		
#MAIN_OBJS += 	otgd_fs_cal.o \\
#		otgd_fs_dev.o \\
#		otgd_fs_int.o \\
#		otgd_fs_pcd.o \\
#		usb_core.o \\
#		usb_init.o \\
#		usb_int.o \\
#		usb_mem.o \\
#		usb_regs.o \\
#		usb_sil.o  \\	
#		

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ #{

  # add createLinks for each link required for this extension
  echo "createLink \$ScriptPath/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ../extensions/ 
  #}

  
  Name="600_example_usb_vcp"
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension

  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "example_usb_vcp.h"
  example_usb_vcp_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

############Configuration Files ${Name} ################
###############required Paths  #################
INCLUDE_DIRS += -I ttc-lib/usb/ \\
		-I ttc-lib/usb/virtual_com_port/ \\
		-I ttc-lib/usb/virtual_com_port/inc/  
vpath %.c 	ttc-lib/usb/ \\
		ttc-lib/usb/virtual_com_port/ \\
		ttc-lib/usb/virtual_com_port/src/ 

############Configuration Files ################
###############required Objects #################
MAIN_OBJS += 	hw_config.o 

MAIN_OBJS += 	usb_desc.o \\
		usb_endp.o \\
		usb_istr.o \\
		usb_prop.o \\
		usb_pwr.o \\
		
#activate this to use your own device descriptor		
COMPILE_OPTS += -DVirtual_Com_PortDevice_Descriptor_external

# append some object files to be compiled
MAIN_OBJS += example_usb_vcp.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0" "Example of how to use architecture independent RADIO support" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/makefile.600_example_*

# create links into extensions.active/
createLink "\$Dir_Extensions/makefile.$Name" "\$Dir_ExtensionsActive/makefile.$Name" '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

activate.250_CPAL_STM32F10x_StdPeriph_Driver.sh QUIET \"\$0\" || exit 10
activate.250_stm_std_peripherals__rcc.sh        QUIET \"\$0\" || exit 10
activate.250_stm_std_peripherals__usart.sh      QUIET \"\$0\" || exit 10
activate.400_stm32_usb_fs_device_lib.sh         QUIET \"\$0\" || exit 10
activate.500_ttc_usart.sh                       QUIET \"\$0\" || exit 10
activate.500_ttc_usb.sh                         QUIET \"\$0\" || exit 10

END_OF_ACTIVATE

  createActivateScriptTail $Name ../extensions/
  #}
  echo "Installed successfully: $Name"
  
  Name="600_example_usb_data_stream"
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

############Configuration Files ${Name}################
###############required Paths  #################
INCLUDE_DIRS += -I ttc-lib/usb/ \\
		-I ttc-lib/usb/virtual_com_port/ \\
		-I ttc-lib/usb/virtual_com_port/inc/  
vpath %.c 	ttc-lib/usb/ \\
		ttc-lib/usb/virtual_com_port/ \\
		ttc-lib/usb/virtual_com_port/src/ 

############Configuration Files ################
###############required Objects #################
MAIN_OBJS += 	hw_config.o 

MAIN_OBJS += 	usb_desc.o \\
		usb_endp.o \\
		usb_istr.o \\
		usb_prop.o \\
		usb_pwr.o \\

#activate this to use your own device descriptor		
COMPILE_OPTS += -DVirtual_Com_PortDevice_Descriptor_external

# append some object files to be compiled
MAIN_OBJS += example_usb_data_stream.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0" "Example of how to use architecture independent RADIO support" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/makefile.600_example_*


activate.250_CPAL_STM32F10x_StdPeriph_Driver.sh QUIET \"\$0\" || exit 10
activate.250_stm_std_peripherals__rcc.sh        QUIET \"\$0\" || exit 10
activate.250_stm_std_peripherals__usart.sh      QUIET \"\$0\" || exit 10
activate.400_stm32_usb_fs_device_lib.sh         QUIET \"\$0\" || exit 10
activate.500_ttc_usart.sh                       QUIET \"\$0\" || exit 10
activate.500_ttc_usb.sh                         QUIET \"\$0\" || exit 10

# create links into extensions.active/
createLink "\$Dir_Extensions/makefile.$Name" "\$Dir_ExtensionsActive/makefile.$Name" '' QUIET

END_OF_ACTIVATE
  createActivateScriptTail $Name ../extensions/
  #}
  echo "Installed successfully: $Name"
  
  Name="600_example_usb_host_hid"
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension

  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "example_usb_host_hid.h"
  example_usb_host_hid_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

############Configuration Files ${Name} ################
###############required Paths  #################
INCLUDE_DIRS += -I ttc-lib/usb/ \\
		-I ttc-lib/usb/usb_host_hid/ \\
		-I ttc-lib/usb/usb_host_hid/inc/
    vpath %.c 	ttc-lib/usb/ \\
		            ttc-lib/usb/usb_host_hid/ \\
		            ttc-lib/usb/usb_host_hid/src/

############Configuration Files ################
###############required Objects #################
MAIN_OBJS += 	usbh_usr.o \\
		          usb_bsp.o \\

#MAIN_OBJS += 	usb_desc.o \\
#		usb_endp.o \\
#		usb_istr.o \\
#		usb_prop.o \\
#		usb_pwr.o \\
		
#activate this to use your own device descriptor		
#COMPILE_OPTS += -DVirtual_Com_PortDevice_Descriptor_external

# append some object files to be compiled
MAIN_OBJS += example_usb_host_hid.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0" "Example of how to use architecture independent RADIO support" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/makefile.600_example_*

# create links into extensions.active/
createLink "\$Dir_Extensions/makefile.$Name" "\$Dir_ExtensionsActive/makefile.$Name" '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

activate.250_CPAL_STM32F10x_StdPeriph_Driver.sh QUIET \"\$0\" || exit 10
activate.250_stm_std_peripherals__rcc.sh        QUIET \"\$0\" || exit 10
activate.250_stm_std_peripherals__usart.sh      QUIET \"\$0\" || exit 10
activate.250_stm_std_peripherals__tim.sh        QUIET \"\$0\" || exit 10
activate.400_stm32_usb_fs_host_lib.sh           QUIET \"\$0\" || exit 10
activate.500_ttc_usart.sh                       QUIET \"\$0\" || exit 10
activate.500_ttc_usb.sh                         QUIET \"\$0\" || exit 10

END_OF_ACTIVATE
  createActivateScriptTail $Name ../extensions/
  #}
  echo "Installed successfully: $Name"
  

  addLine ../scripts/createLinks.sh "rm 2>/dev/null $INSTALLPATH; createLink \$Source/TheToolChain/InstallData/$INSTALLPATH/Library_Host/  $INSTALLPATH"
  #//ToDo: Don't know how to do this dynamically - Sascha  
  addLine ../scripts/createLinks.sh "rm 2>/dev/null ${INSTALLPATH}_device; createLink \$Source/TheToolChain/InstallData/$INSTALLPATH/current_Device_Driver/ ${INSTALLPATH}_device"

  addLine ../scripts/createLinks.sh "rm 2>/dev/null $INSTALLPATH; createLink \$Source/TheToolChain/InstallData/$INSTALLPATH/Library_USB/  $INSTALLPATH"
#X addLine ../scripts/createLinks.sh "rm 2>/dev/null ${INSTALLPATH}_device; createLink ${INSTALLPATH}/STM32_USB_Device_Library/ ${INSTALLPATH}_device"
#X  addLine ../scripts/createLinks.sh "rm 2>/dev/null ${INSTALLPATH}_host;   createLink ${INSTALLPATH}/STM32_USB_HOST_Library/  ${INSTALLPATH}_host"
#X  addLine ../scripts/createLinks.sh "rm 2>/dev/null ${INSTALLPATH}_otg;    createLink ${INSTALLPATH}/STM32_USB_OTG_Driver/    ${INSTALLPATH}_otg"
  
  #//ToDo: Don't know how to do this dynamically - Sascha  
  addLine ../scripts/createLinks.sh "rm 2>/dev/null ${INSTALLPATH}_device; createLink \$Source/TheToolChain/InstallData/$INSTALLPATH/current_Device_Driver/ ${INSTALLPATH}_device"
  echo "Installed successfully: $INSTALLPATH"
#}
else
  echo "failed to install $INSTALLPATH"
  exit 10
fi


#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$StartPath"
applyTargetUserAndGroup "$INSTALLPATH"

exit 0
