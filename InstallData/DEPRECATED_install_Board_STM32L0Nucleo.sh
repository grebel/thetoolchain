#
#  Script written by Greg Knoll 2013 && Victor Fuentes 2015
# 
#  Feel free do distribute and change to your own needs!


source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="100"
EXTENSION_SHORT="stm32l0_discovery"
EXTENSION_PREFIX="board"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder

  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/data_brief/  DM00105918.pdf Board_STM32-L0_Nucleo_Briefing.pdf Boards
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/ DM00105823.pdf Board_STM32-L0_Nucleo_User_Manual.pdf Boards
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

 Dir=`pwd`
    
  Name="${Install_Dir}" #{Write Board Makefile and Activate Script
  
    createExtensionMakefileHead ${Name} #{
    	File="${Dir_Extensions}makefile.${Name}"
cat <<END_OF_MAKEFILE >>$File
    	
  COMPILE_OPTS += -DTTC_BOARD=$Name
  BOARD=$Name

  #X STM32L1XX_MD=1
  
  # Define exactly one CPU variant used on current board
  # See ifdef TTC_CPU_VARIANT_* lines in active makefile.450_ttc_cpu_* file for a list of available CPU variants 
  TTC_CPU_VARIANT_stm32l053r8t6 = 1

	#{ Define port pins
	  # reference ../../Documentation/Boards/Board_STM32-L0_Nucleo_User_Manual.pdf
	
	  #{LEDs
	  COMPILE_OPTS += -DTTC_LED1=tgp_a5
	  #}
	  
	   #{USART1
           COMPILE_OPTS += -DTTC_USART1=INDEX_USART1
           #}
           #{USART2
           COMPILE_OPTS += -DTTC_USART2=INDEX_USART2
           COMPILE_OPTS += -DTTC_USART2_TX=tgp_a2
           COMPILE_OPTS += -DTTC_USART2_RX=tgp_a3
           #}

           #{SPI1
            COMPILE_OPTS += -DTTC_SPI1=ttc_device_1
            COMPILE_OPTS += -DTTC_SPI1_MOSI=tgp_a7
            COMPILE_OPTS += -DTTC_SPI1_MISO=tgp_a6
            COMPILE_OPTS += -DTTC_SPI1_SCK=tgp_a5
            COMPILE_OPTS += -DTTC_SPI1_NSS=tgp_a4
          #}
	
	#} END Define port pins
  
END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} 
    #}END createExtensionMakefile
    
    createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Nucleo Board for STM32L053R8 from STM " #{
	File="${Dir_Extensions}activate.${Name}.sh"
cat <<END_OF_ACTIVATE >>$ActivateScriptFile    

	#{ From install_11_Board_STM32L0Nucleo.sh
	  rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.100_board_*
	  
	  # create links into extensions.active/
	  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
	  
	  #X activate.200_cpu_stm32l0xx.sh QUIET "\$ScriptName"
	#}END From install_11_Board_STM32L0Nucleo.sh

  enableFeature 450_cpu_stm32l0xx

END_OF_ACTIVATE
    createActivateScriptTail $Name ${Dir_Extensions}
    #}END createActivateScript
    
  echo "Installed successfully: $Install_Dir"
  #} END Makefile and Activate Script    

  
#}  
#  IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------    
    
cd ..
rmdir 2>/dev/null $Install_Dir

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
