#!/bin/bash

#
#  Install script for Microcontroller STM32 F1xx 
#  with ARM Cortex M3 core + Std Peripherals Library for STM32F1xx.
#
#  Script written by Gregor Rebel 2012
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "190_cpu_cortexm3" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.eecs.umich.edu/~prabal/teaching/eecs373-f11/readings/                             ARMv7-M_ARM.pdf                      ARMv7-M_Architecture_Reference_Manual.pdf                                       uC/ARM7
  getDocumentation http://www.arm.com/files/pdf/                                                                IntroToCortex-M3.pdf                 An_Introduction_to_the_ARM_Cortex-M3_Processor.pdf                              uC/CortexM3
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/programming_manual/ CD00228163.pdf                       STM32_PM0056_Cortex-M3_Programming_Manual.pdf                                   uC/CortexM3
  getDocumentation http://www.arm.com/files/pdf/                                                                IntroToCortex-M3.pdf                 An_Introduction_to_the_ARM_Cortex-M3_Processor.pdf                              uC/CortexM3
  getDocumentation http://www.peter-cockerell.net/aalp/resources/pdf/                                           all.pdf                              ARM-Assembly_Language_Programming.pdf                                           uC/ARM7
  getDocumentation http://www.state-machine.com/arm/                                                            Building_bare-metal_ARM_with_GNU.pdf Building_Bare-Metal_ARM-Systems_with_GNU.pdf                                    Compiler
  getDocumentation http://infocenter.arm.com/help/topic/com.arm.doc.dui0552a/                                   DUI0552A_cortex_m3_dgug.pdf          Cortex-M3_Devices_Generic_User_Guide.pdf                                        uC/CortexM3
  getDocumentation http://infocenter.arm.com/help/topic/com.arm.doc.ddi0337g/                                   DDI0337G_cortex_m3_r2p0_trm.pdf      Cortex-M3_Technical_Reference_Manual.pdf                                        uC/CortexM3           
  getDocumentation http://infocenter.arm.com/help/topic/com.arm.doc.ihi0042e/                                   IHI0042E_aapcs.pdf                   Procedure_Call_Standard_for_the_ARM-Architecture.pdf                            Compiler
  getDocumentation http://www.st.com/web/en/resource/technical/document/application_note/                       CD00221665.pdf                       AN2867_Oscillator_design_guide_for_STM8S,_STM8A_and_STM32_microcontrollers.pdf  uC/STM
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

  if [ ! -e OK.Install ];       then #{
    if [ ! -e OK.Packages ]; then #{
      touch OK.Packages
    fi #}
    if [ -e OK.Packages ]; then #{
      Error=""
      
      Archive="CMSIS-SP-00300-r3p1-00rel0.zip"
      #getFile https://silver.arm.com/download/Development_Tools/Keil/Keil:_generic/CMSIS-SP-00300-r3p1-00rel0/ $Archive $Archive
      getFile http://thetoolchain.com/mirror/ $Archive $Archive
      if [ "$Error" == "" ]; then
        testZIP $Archive
      fi
      if [ "$Error" == "" ]; then
        unZIP "foo" $Archive
      else
        Error="Corrupt archive $Archive!"
        mv -v $Archive ${Archive}_broken
      fi
      if [ ! -d CMSIS/ ]; then
        Error=" Cannot download CMSIS library! (missing $Archive)"
      else
        #X cp -v CMSIS/index.html ../../Documentation/uC/CortexM3/CMSIS.html
        #X cp -R CMSIS/Documentation/ ../../Documentation/uC/CortexM3/
        
        createLink CMSIS CortexM-CMSIS
        addDocumentationFolder CortexM-CMSIS uC/
      fi
    
      if [ "$Error" == "" ]; then
        touch OK.Install
      else
        echo "$0 - ERROR: $Error!"
        exit 10
      fi
    fi #}
  fi #}
  if [   -e OK.Install ];       then #{
    Name="${Install_Dir}"
    
    createExtensionMakefileHead ${Name} #{         create makefile for cpu
    File="${Dir_Extensions}makefile.${Name}" 
    cat <<END_OF_MAKEFILE >>$File 

ARM_CPU     =arm7tdmi
TARGET      =-mcpu=cortex-m3 -mthumb
TARGET_ARCHITECTURE_CM3=1
COMPILE_OPTS += -DTARGET_ARCHITECTURE_CM3
COMPILE_OPTS += -DTARGET_DATA_WIDTH=32

# Setting CPU define is required by gnu.h (SimpleMAC v1.1.0)
COMPILE_OPTS += -DCORTEXM3_STM32W108

# some compiled libraries (e.g. ST SimpleMAC for STM32W) use 2 byte wchars
#? COMPILE_OPTS += -fshort-wchar

# define CortexM3 memory regions

COMPILE_OPTS += -DTTC_MEMORY_REGION_CODE_START=0x60000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_CODE_SIZEK=1048576
COMPILE_OPTS += -DTTC_MEMORY_REGION_CODE_ACCESS=rwx

COMPILE_OPTS += -DTTC_MEMORY_REGION_SRAM_START=0x60000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_SRAM_SIZEK=1048576
COMPILE_OPTS += -DTTC_MEMORY_REGION_SRAM_ACCESS=rwx

COMPILE_OPTS += -DTTC_MEMORY_REGION_SRAM_BITBAND_START=0x22000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_SRAM_BITBAND_SIZEK=32768
COMPILE_OPTS += -DTTC_MEMORY_REGION_SRAM_BITBAND_ACCESS=rw

COMPILE_OPTS += -DTTC_MEMORY_REGION_PERIPHERAL_START=0x40000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_PERIPHERAL_SIZEK=1024
COMPILE_OPTS += -DTTC_MEMORY_REGION_PERIPHERAL_ACCESS=rw

COMPILE_OPTS += -DTTC_MEMORY_REGION_PERIPHERAL_BITBAND_START=0x42000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_PERIPHERAL_BITBAND_SIZEK=32768
COMPILE_OPTS += -DTTC_MEMORY_REGION_PERIPHERAL_BITBAND_ACCESS=rw

COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_EXTERNAL_START=0x60000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_EXTERNAL_SIZEK=1048576
COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_EXTERNAL_ACCESS=rwx

COMPILE_OPTS += -DTTC_MEMORY_REGION_DEVICE_EXTERNAL_START=0xa0000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_DEVICE_EXTERNAL_SIZEK=1048576
COMPILE_OPTS += -DTTC_MEMORY_REGION_DEVICE_EXTERNAL_ACCESS=rw

COMPILE_OPTS += -DTTC_MEMORY_REGION_PRIVATE_PERIPHERAL_START=0xe0000000
COMPILE_OPTS += -DTTC_MEMORY_REGION_PRIVATE_PERIPHERAL_SIZEK=1024
COMPILE_OPTS += -DTTC_MEMORY_REGION_PRIVATE_PERIPHERAL_ACCESS=rw

COMPILE_OPTS += -DTTC_MEMORY_REGION_VENDOR_MEMORY_START=0xe0100000
COMPILE_OPTS += -DTTC_MEMORY_REGION_VENDOR_MEMORY_SIZEK=523264
COMPILE_OPTS += -DTTC_MEMORY_REGION_VENDOR_MEMORY_ACCESS=rw

# architecture specific support code
INCLUDE_DIRS += -Ittc-lib/cm3
vpath %.c ttc-lib/cm3

# Basic Cortex M3 support library
INCLUDE_DIRS += -Iadditionals/270_CPAL_CMSIS/CM3/CoreSupport/
vpath %.c additionals/270_CPAL_CMSIS/CM3/CoreSupport/

MAIN_OBJS += core_cm3.o

# suppress warnings about invalid wchar sizes during linker stage
# (not supported by gcc-arm v4.7) LDFLAGS += --no-wchar-size-warning

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} $0 "CPU STM32F10x" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{
#? rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.200_cpu_*

# create links into extensions.active/
createLink ${Dir_Extensions}makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
cd _
createLink gdb_cortexm3.sh gdb.sh
cd ..

#? activate.500_ttc_basic.sh QUIET \"\$0\"

END_OF_ACTIVATE
#}
    createActivateScriptTail $Name ${Dir_Extensions}
    #}

  if [ "1" == "0" ]; then # { CMSIS (currently not usable due to missing C-sources in downloaded archive)

      Name="${Install_Dir}_cmsis"  
    # (addLine) 
    # create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    #
    # Activate line below and change SUBFOLDER to your needs
    addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/${Install_Dir}/CMSIS $Name"

    createExtensionSourcefileHead ${Name}    #{ (create initializer code)
    
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

//  #include "YOUR_SOURCE_HERE.h"
//  YOUR_START_HERE();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
INCLUDE_DIRS += -I additionals/$Name/Include/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
#MAIN_OBJS += 

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
#createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
#X activate.500_ttc_memory.sh QUIET "\$0"


END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  fi #}
    
    echo "Installed successfully: $Install_Dir"
  fi #}
  cd ..
  rmdir 2>/dev/null $Install_Dir  # removes only empty install dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
