#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Initial Script written by Gregor Rebel 2010-2012.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

StartPath="`pwd`"
INSTALLPATH="500_ttc_gfx"
dir "$INSTALLPATH"
cd "$INSTALLPATH"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Docs ];    then    #{ download documentation

  Get_Fails=""
  
  # download doc-file + move to Documentation/ + create symbolic link into Documentation/ right here 
  # File="STM32F10x_Standard_Peripherals_Library.html"
  # getFile http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library $File
  # addDocumentationFile $File STM
  
  if [ "$Get_Fails" == "" ]; then #{ check get() inside scripts/installFuncs.sh 
    touch OK.Docs
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ download + install software
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe 'which autoreconf' autoreconf
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $INSTALLPATH ..."
    InstallDir=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts
  Name="${INSTALLPATH}"
  
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

INCLUDE_DIRS += -I ttc-lib/gfx
vpath %.c ttc-lib/gfx

MAIN_OBJS += ttc_gfx.o gfx_image.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0 2" "Device independent graphic output + input" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_Additionals/$INSTALLPATH_*

Driver="\$Dir_ExtensionsActive/makefile.450_ttc_gfx_driver*"
if [ "\`ls \$Driver 2>/dev/null\`" != "" ]; then
  # create links into extensions.active/
  createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET
  
  activate.500_ttc_math_trigonometry.sh QUIET   "\$0"
  activate.500_ttc_string.sh QUIET "\$0"
else
  echo "$0 - missing gfx-driver (\$Driver)"
fi

END_OF_ACTIVATE
  createActivateScriptTail $Name ../extensions/
  #}

  cd "$OldPWD"
  echo "Installed successfully: $INSTALLPATH"
#}
else                            #{ Install failed
  echo "failed to install $INSTALLPATH"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$StartPath"
applyTargetUserAndGroup "$INSTALLPATH"

exit 0
