#!/usr/bin/perl

my $Installs = `ls install_*.sh`;
my @Installs = grep { $_; } split("\n", $Installs);

@Installs = sort { 
              $a =~ /install_[0-9]+_(.+)\.sh/; my $A = $1; 
              $b =~ /install_[0-9]+_(.+)\.sh/; my $B = $1;
              $A cmp $B;
            } @Installs;

foreach my $Install (@Installs) {
  system("git mv '$Install' '__$Install'");
}
            
my $Enum = 1;
foreach my $Install (@Installs) {
  my $Number = substr('000'.($Enum++), -3);
                                    # "install_024_TTC_MATHEMATICS.sh"
  my @Parts = split("_", $Install); # "install", "024", "TTC", "MATHEMATICS.sh"
  shift(@Parts);                    # "024", "TTC", "MATHEMATICS.sh"
  shift(@Parts);                    # "TTC", "MATHEMATICS.sh"
  my $NewInstall = join("_", "install", $Number, @Parts);
  system("git mv '__$Install' '$NewInstall'");
}
my $PWD = `pwd`; chop($PWD);
system("replace comment \"chg: $PWD/ - renumbered install scripts\"");
