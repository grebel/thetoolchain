#!/bin/bash
#            TheToolChain
#
#  Install script for basic extensions.
#
#  Script written by Gregor Rebel 2012 - 2014
# 
#  Feel free do distribute and change to your own needs!
#

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "060_basic_extensions" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------
  
if [ ! -e OK.Documentation ]; then #{
  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  #getDocumentation http://frama-c.com/download/frama-c-user-manual.pdf frama-c-user-manual.pdf Compiler/Debugging
  getDocumentation http://werner.dichler.at/sites/default/files/attachment/ prj21_CRC%20Einfuehrung.pdf CRC-Einfuehrung.pdf Algorithms
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then #{
  
  if [ ! -e OK.Packages ]; then #{
    # nothing to install (yet)
    Arch=`uname -i`
    if [ "$Arch" == "x86_64" ]; then
      installPackage "ia32-libs"
    fi
    installPackageSafe "which xdotool" "xdotool"
    installPackageSafe "which kdbg"    "kdbg"
    installPackageSafe "which ddd"     "ddd"
    #?installPackageSafe "which frama-c" "frama-c"
    touch OK.Install
    touch OK.Packages
  fi #}
fi #}
if [ -e OK.Install ]; then #{
  
  Name="${Install_Dir}_basic_setup"
  createExtensionMakefileHead ${Name} #{         create makefile for extension
  File="${Dir_Extensions}makefile.${Name}" 
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile 

# Makefile created by $ScriptName 

COMPILE_OPTS += -DEXTENSION_${Name}=1  

# avoid modern gcc to generate lots of useles warnings of type "warning: dereferencing type-punned pointer will break strict-aliasing rules [-Wstrict-aliasing]"
COMPILE_OPTS += -fno-strict-aliasing

# basic software interface of TheToolChain (ttc_XXX() functions)
INCLUDE_DIRS += -I ttc-lib/
INCLUDE_DIRS += -I ttc-lib/scheduler/
INCLUDE_DIRS += -I examples/
INCLUDE_DIRS += -I regressions/
INCLUDE_DIRS += -I extensions.active/

vpath %.c ttc-lib/ ttc-lib/interfaces/ ttc-lib/scheduler/ examples/ regressions/ extensions.active/

# add some basic object files which are always available
MAIN_OBJS += ttc_extensions.o ttc_extensions_active.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName" "Basic toolchain settings required by all architectures and boards." #{ create activate script
  if [ "$Delete" != "" ]; then
    echo "rm 2>/dev/null \${Dir_ExtensionsActive}/${Install_Dir}" >>$ActivateScriptFile
  fi
  #X ../scripts/printBaseExtensions.sh >>$ActivateScriptFile
  cat <<END_OF_ACTTIVATE >>$ActivateScriptFile #{

createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
#? activate.500_ttc_basic.sh QUIET "\$ScriptName"

END_OF_ACTTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions} 
#}
  
  Name="${Install_Dir}_optimize_1_debug"
  createExtensionMakefileHead ${Name}
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile #{

DEBUG=1
OPTIMIZE+=-g3 # enable maximum debug informations (even macros)

END_OF_MAKEFILE
#}
  createExtensionMakefileTail ${Name}
  #createActivateScript "${Dir_Extensions}activate.${Name}.sh" $Name "makefile.${Install_Dir}_optimize_1_*" "$ScriptName" "compile option: do not optimize at all (best visibility during debugging)"
  createActivateScript $Name ${Dir_Extensions} "$ScriptName" "" "compile option: do not optimize at all (best visibility during debugging)"

  Name="${Install_Dir}_optimize_2_smallest"
  createExtensionMakefileHead ${Name}
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile #{
  
OPTIMIZE+=-Os 

END_OF_MAKEFILE
#}
  createExtensionMakefileTail ${Name}
  createActivateScript $Name ${Dir_Extensions} "$ScriptName" "makefile.${Install_Dir}_optimize_2_*" "compiler option: create small code (can degrade execution speed)"

  Name="${Install_Dir}_optimize_2_fastest"
  createExtensionMakefileHead ${Name}
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile #{

OPTIMIZE+=-O9

END_OF_MAKEFILE
#}
  createExtensionMakefileTail ${Name}
  createActivateScript $Name ${Dir_Extensions} "$ScriptName" "makefile.${Install_Dir}_optimize_2_*" "compiler option: create fast code (can increase code- and memory-usage)"
  
  Name="${Install_Dir}_optimize_3_inline"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile 

COMPILE_OPTS += -finline -finline-functions-called-once

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScript $Name ${Dir_Extensions} "$ScriptName" "" "compiler option: try to inline as many functions as possible (can increase code- and memory-usage)"

  Name="${Install_Dir}_optimize_3_remove_unused"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile 

COMPILE_OPTS += -fdata-sections -ffunction-sections
LDFLAGS += -Wl,--gc-sections

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScript $Name ${Dir_Extensions} "$ScriptName" "" "compiler option: remove all unused functions and data elements (reduces resulting code size but may remove functions used during debugging sessions)"

  Name="${Install_Dir}_disable_asserts"
  DynamicMakefile="makefile.000_disable_asserts"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile 

# including a makefile that has been created dynamically
include extensions.local/$DynamicMakefile

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "Disables all Assert_XXX() macros (Makes code faster and binary smaller but more difficult to debug)" #{ (create activate script)
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

if [ ! -e "\${Dir_ExtensionsLocal}/$DynamicMakefile" ]; then
echo "creating file '\${Dir_ExtensionsLocal}/$DynamicMakefile'.."
cat <<END_OF_FILE >\${Dir_ExtensionsLocal}/$DynamicMakefile
#         The ToolChain
#
# This file contains all assert macros found in ttc-lib/ at date of creation.
# This file has been created by \$ScriptName on \`date\`
#
# Simply remove this file and rerun \$ScriptName to create an updated version.
#

END_OF_FILE
_/findAllAsserts.pl >>\${Dir_ExtensionsLocal}/$DynamicMakefile
else
echo "$ScriptName - reusing existing file '\${Dir_ExtensionsLocal}/$DynamicMakefile'"
fi

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  Name="${Install_Dir}_disable_safe_arrays"
  DynamicMakefile="makefile.000_disable_safe_arrays"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile 

# Disabling safe array index checking (interpreted in ttc_basic_types.h)
COMPILE_OPTS += -DENABLE_TTC_BASIC_SAFE_ARRAYS=0

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "Disables index checking on safe array macros A*() for faster and smaller code (see ttc_basic_types.h for a description of safe arrays))" #{ (create activate script)
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

if [ ! -e "\${Dir_ExtensionsLocal}/$DynamicMakefile" ]; then
echo "creating file '\${Dir_ExtensionsLocal}/$DynamicMakefile'.."
cat <<END_OF_FILE >\${Dir_ExtensionsLocal}/$DynamicMakefile
#         The ToolChain
#
# This file contains all assert macros found in ttc-lib/ at date of creation.
# This file has been created by \$ScriptName on \`date\`
#
# Simply remove this file and rerun \$ScriptName to create an updated version.
#

END_OF_FILE
echo "$ScriptName - disabling all safe arrays A*(). Will use plain C arrays instead."
fi

END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  echo "Installed successfully: $Install_Dir"
fi #}
cd ..
rmdir 2>/dev/null $Install_Dir  # removes only empty install dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
