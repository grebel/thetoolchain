#!/bin/bash

#                         The ToolChain
#
#  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
#
#  Install script for a set of High- and Low-Level Drivers
#  for USB FileSystem Device Library.
#
#  Created from template <TEMPLATE_FILE> revision 22 at <DATE>
#
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
#  Authors: <AUTHOR>
#

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="400"
EXTENSION_NAME="usb_stm32f1xx"
setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
Install_Path=`pwd`

findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   exit 10
# fi #}
#}look for newest versions

if [ ! -e OK.Docs ];    then    #{ (download documentation)

  Get_Fails=""
  #cd ../../Documentation/uC/STM32F1xx
  # download doc-file + move to Documentation/ + create symbolic link into Documentation/ right here 
  # File="CD00158241.pdf"
  # getFile http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/ $File STM32USB-FS-Device_development_kit.pdf || Error="1"
   #createLink $Install_Path/$File ../../Documentation/uC/STM32F1xx/"$File"
   
   
  
  if [ "$Get_Fails" == "" ]; then #{ check get() inside scripts/installFuncs.sh 
    touch OK.Docs
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe which
    
    
    
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    Install_Path=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts

  if [ "1" == "1" ]; then #{ high-level driver for STM32F1xx
    Name="${Install_Dir}"  
  
    createExtensionSourcefileHead ${Name}    #{ (create initializer code)
    
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

    #include "ttc_usb.h"
    ttc_stm32f1xx_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 


      ############Configuration Files ${Name} ################
      ###############required Paths  #################
      INCLUDE_DIRS +=  -I ttc-lib/usb/ \\
      -I ttc-lib/usb/virtual_com_port/ \\
      -I ttc-lib/usb/virtual_com_port/inc/  
              vpath %.c 	ttc-lib/usb/ \\
         ttc-lib/usb/virtual_com_port/ \\
        ttc-lib/usb/virtual_com_port/src/ 

      ############Configuration Files ################
      ###############required Objects #################
      MAIN_OBJS += 	hw_config.o 

      MAIN_OBJS += 	usb_desc.o \\
                    usb_endp.o \\
                    usb_istr.o \\
                    usb_prop.o \\
                    usb_pwr.o \\
		
      #activate this to use your own device descriptor		
      COMPILE_OPTS += -DVirtual_Com_PortDevice_Descriptor_external

      # append some object files to be compiled
      //MAIN_OBJS += example_usb_vcp.o

      
          # additional constant defines
      #COMPILE_OPTS += -D
      
      # additional directories to search for header files
      INCLUDE_DIRS += -Ittc-lib/usb/
      
      # additional directories to search for C-Sources
      vpath %.c ttc-lib/usb/
      
      # additional object files to be compiled
      #MAIN_OBJS += ttc_usb_stm32f1xx.o




END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$0 2" "high-level STM32F1xx-USB Driver" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/
#   \$File_LowLevelInstall  !="": user wants to run only this low level install script 


# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/*${Install_Dir}_*

if [ "\$File_LowLevelInstall" != "" ]; then
  \$File_LowLevelInstall QUIET "\$0"

  # extract driver name from script name
  LowLevelDriver=\`perl -e "print substr('\$File_LowLevelInstall', 9, -3);"\`
  
  #X #DISABLED echo "\$0 - LowLevelDriver='\$LowLevelDriver'"
  DriverFound="1"

else
  for LowLevelActivate in \`ls \${Dir_Extensions}/activate.450_usb_fs-device_*\`; do
    
    # try to activate this low-level driver
    \$LowLevelActivate QUIET "\$0"
    
    # extract driver name from script name
    LowLevelDriver=\`perl -e "my \\\\\$P=rindex('\$LowLevelActivate', 'activate.')+9; print substr('\$LowLevelActivate', \\\\\$P, -3);"\`
    #DISABLED echo "\$0 - LowLevelDriver='\$LowLevelDriver'"
    Makefile="\${Dir_ExtensionsActive}/makefile.\$LowLevelDriver"
    if [ -e \$Makefile ]; then
      DriverFound="1"
    fi
  done
fi

if [ "\$DriverFound" != "" ]; then

  # ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

  # ACTIVATE_SECTION_D call other activate-scripts
  # activate.500_ttc_memory.sh QUIET "\$0"
else
  echo "\$0 - ERROR: No low-level usb_fs-device Driver found: Skipping activation!"
fi

END_OF_ACTIVATE
    createActivateScriptTail $Name ${Dir_Extensions}
    #}
  
  fi #}high-level driver
  if [ "1" == "1" ]; then #{ call install scripts of low-level drivers for STM32F1xx USB
    LowLevelInstalls=`ls 2>/dev/null ../installs_low_level/install_024_TTC_USB_STM32F1XX.sh`
    echo "`pwd`> found LowLevelInstalls: $LowLevelInstalls"
    for LowLevelInstall in $LowLevelInstalls
    do
      cd "$Install_Path"
      echo    "running `pwd`/$LowLevelInstall ...  "
      echo -n "        "
      source $LowLevelInstall
    done
    cd "$Install_Path"
  fi #}low-level driver
  if [ "1" == "0" ]; then #{ optional: create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    # Activate line below and change SUBFOLDER to your needs
    # addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; ln -sv \$Source/TheToolChain/InstallData/${Install_Dir}/SUBFOLDER $Install_Dir"
    echo ""
  fi #} 
  if [ "1" == "0" ]; then #{ recommended: create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${Install_Dir}.sh QUIET "\$0"


END_OF_ACTIVATE
    createActivateScriptTail $Name ${Dir_Extensions}
    #}

  fi #}
  if [ "1" == "0" ]; then #{ recommended: create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/*${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${Install_Dir}.sh QUIET "\$0"
activate.500_ttc_memory.sh QUIET "\$0"

END_OF_ACTIVATE
    createActivateScriptTail $Name ${Dir_Extensions}
    #}

  fi #}

  cd "$Install_Path"
  echo "Installed successfully: $Install_Dir"
#}
else                            #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
