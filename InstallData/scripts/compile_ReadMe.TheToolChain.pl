#!/usr/bin/perl

my $ReadMeTemplateFile = shift(@ARGV);
my %Keys = (
 LowLevelDrivers => '<INSERT_LOW_LEVEL_DRIVERS>',
 PrototypeBoards => '<INSERT_PROTOTYPE_BOARDS>',
 DateAndTime     => '<INSERT_DATE_AND_TIME>',
 VersionTTC      => '<INSERT_VERSION_TTC>'
);

unless ($ReadMeTemplateFile) { #
  my $KeysText = "  - ".join("\n  - ", sort values %Keys);
  
  print <<"END_OF_HELP";
$0 TEMPLATE_FILE

TEMPLATE_FILE  filename with . prefix (E.g. '.readme.TheToolChain')

Will read given template file and replace certain text marks by dynamically compiled text:
$KeysText

$0 was written by Gregor Rebel 2015

END_OF_HELP
  exit 0;
  
} #unless($ReadMeTemplateFile)

my $ReadMeTemplate    = readFile($ReadMeTemplateFile);
my @ReadMeTemplate = split("\n", $ReadMeTemplate);
my $ReadMeTemplateDir = extractFileDir($ReadMeTemplateFile);

my $CurrentDir, @CurrentDir;
do { # go upwards until TTC base folder
  $CurrentDir = `pwd`;
  chomp($CurrentDir);
  @CurrentDir = split('/', $CurrentDir);
  if (substr($CurrentDir[-1],0, 12) ne 'TheToolChain') {
    chdir("..");
  } #if()
} until ( ($CurrentDir eq '/') || (substr($CurrentDir[-1],0, 12) eq 'TheToolChain') );
if (substr($CurrentDir[-1],0, 12) ne 'TheToolChain') {
  print STDERR "$0 - ERROR: Must be started from inside TheToolChain/ !\n";
  exit 10;
} #if(($CurrentDir eq '/'))

my $LineNo = findLine($Keys{PrototypeBoards}, \@ReadMeTemplate);
if ($LineNo > -1) { # compile_BoardList
  my $Pos = index($ReadMeTemplate[$LineNo], $Keys{PrototypeBoards});
  my $Indent = substr($ReadMeTemplate[$LineNo], 0, $Pos);
  print "$0: compiling BoardList..\n";
  my $BoardList   = compile_BoardList($Indent);
  $ReadMeTemplate[$LineNo] = $BoardList;
}

$LineNo = findLine($Keys{LowLevelDrivers}, \@ReadMeTemplate);
if ($LineNo > -1) { # compile_DriverTable
  my $Pos = index($ReadMeTemplate[$LineNo], $Keys{LowLevelDrivers});
  my $Indent = substr($ReadMeTemplate[$LineNo], 0, $Pos);
  if (0) { # DriverTable disabled ATM (table grew too big) - ToDo: find a new presentation format
    print "$0: compiling DriverTable..\n";
    $ReadMeTemplate[$LineNo] = compile_DriverTable($Indent);
  }
  else { $ReadMeTemplate[$LineNo] = undef; }
}
my $OutFile = substr($ReadMeTemplateFile, 1);

my $DateAndTime=`date --utc +"%Y%m%d %H:%M:%S UTC"`; chomp($DateAndTime);
replaceText($Keys{DateAndTime}, $DateAndTime, \@ReadMeTemplate);

my $VersionTTC = `cat Version`; chomp($VersionTTC);
replaceText($Keys{VersionTTC}, $VersionTTC, \@ReadMeTemplate);

print "$0: creating readme file $OutFile\n";
writeFile( $OutFile, join("\n", @ReadMeTemplate) );

exit 0;

sub compile_BoardList {   # compile list of supported prototype boards
  my $Indent = shift() || ""; # prefix each line with this string
  
  my $Boards=`ls InstallData/install_*_Board_*sh`;
  my @Boards = split("\n", $Boards);

  @Boards = sort map { 
    # InstallData/install_008_Board_Olimex_H103.sh
    $_ =~ m/install_([0-9]+)_Board_(.+)\.sh/;
    $2;
  } @Boards;

  return $Indent.join("\n$Indent", @Boards)."\n";
}
sub compile_DriverTable { # compile table showing all supported drivers
  my $Indent = shift() || ""; # prefix each line with this string

  my $HighLevelDrivers = `ls TTC-Library/ttc_*.c`;
  my @HighLevelDrivers = split("\n", $HighLevelDrivers);
  my %HighLevelDrivers;
  my %AllArchitectures, @AllArchitecturesSorted;
  my $MaxLength_Name = 0;
 
  map {
    # TTC-Library/ttc_adc.c
    $_ =~ m/TTC-Library\/ttc_(.+)\.c/;
    my $DriverName = $1;
    my $Found = `find TTC-Library/ -wholename "*$DriverName/$DriverName*"`; # find all corresponding low-level driver files
    my @Found = split("\n", $Found);
    my %ArchitectureSupport;
    my @LowLevel_Headers;
    map  { # TTC-Library/adc/adc_stm32l1xx.h
      $_ =~ m/TTC-Library\/$DriverName\/${DriverName}_(.+)\.h/;
      if ( ($1) && 
           (substr($1, -6) ne '_types') && 
           (substr($_, -2) ne '.c')     &&
           (substr($_, -4) ne '_new') 
         ) 
      { # found low level driver header file
        my $ArchitectureName = $1;
        unless ($ArchitectureSupport{$ArchitectureName}) { # new architecture: check its support status
          my $LowLevel_HeaderFile = $_;
          push @LowLevel_Headers, $LowLevel_HeaderFile;
          my $StatusKeyword = 'EXTENSION_STATUS_TTC_'.uc($DriverName).'_'.uc($ArchitectureName);
          my $LowLevel_Header = readFile($LowLevel_HeaderFile);
          my @LowLevel_Header = split("\n", $LowLevel_Header);
          my $FoundAtLine = findLine("#define $StatusKeyword", \@LowLevel_Header);
          my $SupportStatus = '?'; # support status still to be determined
          if ($FoundAtLine == -1) { # missing StatusKeyword in low-level driver: insert it into header file 
            @LowLevel_Header = insertBeforeLine(\@LowLevel_Header, [<<"END_OF_INSERT"], "//InsertTypeDefs");

//{$StatusKeyword
//
// Implementation status of low-level driver support for $DriverName devices on $ArchitectureName
// Note: This block has been automatically inserted by $0

#define $StatusKeyword '?'
// May be one from this list
// '-'  currently no support
// 'o'  limited support
// '+'  full support

#if ($StatusKeyword == '?')
#  warning Missing low level support status. Please set value of $StatusKeyword to one from '-', 'o', '+' 
#endif

//}$StatusKeyword
END_OF_INSERT
          }
          else {                    # found StatusKeyword: extract its value
            my $Line = $LowLevel_Header[$FoundAtLine];
            $Line =~ m/.*'(.+)'.*/; # "#define EXTENSION_STATUS_TTC_HEAP_ZDEFAULT '+'"
            $SupportStatus = $1;
          }
          writeFile("${LowLevel_HeaderFile}", join("\n", @LowLevel_Header) );
          
          $ArchitectureSupport{$ArchitectureName} = $SupportStatus;
          $AllArchitectures{$ArchitectureName} = 1;
        }
      }
    } @Found;
    
    $HighLevelDrivers{$DriverName} = { 
      Architectures    => \%ArchitectureSupport,
      LowLevel_Headers => \@LowLevel_Headers
    };
    if ($MaxLength_DriverName < length($DriverName)) { $MaxLength_DriverName = length($DriverName); }
  } @HighLevelDrivers;
  my @AllArchitecturesSorted = sort keys %AllArchitectures;
  
  my $DividerLine;
  my $Divider = '----------------------------------------------------------------------';
  my $DividerLine = substr($Divider, 0, $MaxLength_DriverName + 1);
  map { # compile $DividerLine
    $DividerLine .= '|'.substr($Divider, 0, length($_));
  } @AllArchitecturesSorted;

  my @Rows;
  push @Rows, substr("                        ", 0, $MaxLength_DriverName).' |'.join('|', @AllArchitecturesSorted);
  push @Rows, $DividerLine;
  map { # compile all Rows
    my @Cells;
    my $Architectures = $HighLevelDrivers{$_}->{Architectures};
    unless (%$Architectures) { # This high level driver has no low-level driver => it supports all architectures
      map {
        $Architectures->{$_} = ' ';
      } @AllArchitecturesSorted
    }

    map { # compile all Cells of one Row
      my $CellWidth = length($_);
      my $Cell = $Architectures->{$_};
      while (length($Cell) < $CellWidth) { $Cell = ' '.$Cell.' '; }
      $Cell = substr($Cell, 0, $CellWidth);
      push @Cells, $Cell 
    } @AllArchitecturesSorted;
    
    push @Rows, substr($_."                        ", 0, $MaxLength_DriverName).' |'.join('|', @Cells);
  } sort keys %HighLevelDrivers;
  push @Rows, $DividerLine;

  
  return $Indent.join("\n$Indent", @Rows)."\n".
         $Indent."Legend: o = Limited Support, + = Full Support, ? = Support Undefined\n";  
}
sub replaceText {         # replaces given string by given stirng in given lines
  my $Search    = shift;
  my $Replace   = shift;
  my $LinesRef  = shift;   # array-ref: lines in which to replace string
  
  foreach my $Line (@$LinesRef) {
    my $Pos = index($Line, $Search);
    if ($Pos > -1) {
      substr($Line, $Pos, length($Search)) = $Replace;
    }
  }
}
sub insertBeforeLine {    # inserts given lines before line containing given string
  my $LinesRef  = shift;   # array-ref: lines to insert into
  my $InsertRef = shift;   # array-ref: lines to insert
  my $String    = shift;   # string to search for
  
  my $FoundLineNo = findLine($String, $LinesRef);
  my @NewLines;
  if ($FoundLineNo == -1) { # append after last line
    @NewLines = @$LinesRef, @$InsertRef;
  }
  else {
    for (my $LineNo = 0; $LineNo < $FoundLineNo; $LineNo++) {
      push @NewLines, $LinesRef->[$LineNo];
    }
    push @NewLines, @$InsertRef;
    for (my $LineNo = $FoundLineNo; $LineNo < scalar @$LinesRef; $LineNo++) {
      push @NewLines, $LinesRef->[$LineNo];
    }
  }
  
  return @NewLines;
}
sub findLine {            # finds number of line containing given string in given @Lines
  my $String   = shift;
  my $LinesRef = shift; # array-ref
  
  my $FoundAtLine = -1;
  my $LineNo      = -1;
  
  foreach my $Line (@$LinesRef) {
    $LineNo++;
    if (index($Line, $String) > -1) {
      $FoundAtLine = $LineNo;
    }
  }
  
  return $FoundAtLine;
}
sub extractFileName {     # dir1/dir2/foo.c -> foo.c
  
  my @FileNames = map {
    my $LastSlashPos = rindex($_, '/');
    substr($_, $LastSlashPos + 1);
  } @_;

  if (wantarray) { return @FileNames; }
  else           { return $FileNames[0]; }
}
sub extractFileDir {      # dir1/dir2/foo.c -> dir1/dir2
  
  my @FileNames = map {
    my $LastSlashPos = rindex($_, '/');
    if ($LastSlashPos > -1) { substr($_, 0, $LastSlashPos); }
    else                    { undef; }
  } @_;

  if (wantarray) { return @FileNames; }
  else           { return $FileNames[0]; }
}
sub readFile {            # read in content of given file
  my $FilePath = shift;
  
  my $Content;
  open(IN, "<$FilePath") or die ("$0 - ERROR: Cannot read from file '$FilePath' ($!)");
  while (<IN>) { $Content .= $_; }
  close(IN);
  
  return $Content;
}
sub writeFile {           # write content to given file
  my $FilePath = shift;
  my $Content  = shift;
  
  open(OUT, ">$FilePath") or die ("$0 - ERROR: Cannot write to file '$FilePath' ($!)");
  print OUT $Content;
  close(OUT);
  
  return $Content;
}
