#!/bin/bash
#
# (c) Gregor Rebel 2010-2018

BinaryFrontEndSuffix="$1"; shift
PortGDB="$1";              shift
FILE="$1";                 shift
Option="$1";               shift
EXTRA_CFG="$1";            shift

echo "$0 BinaryFrontEndSuffix='$BinaryFrontEndSuffix' PortGDB='$PortGDB' FILE='$FILE' Option='$Option' EXTRA_CFG='$EXTRA_CFG'"
source _/installFuncs.sh
GDB_PWD=`pwd`

function killProcesses() {
  ProcessName="$1"
  
  # remove leading path (causes trouble) 
  ProcessName=`perl -e "my \\$I=rindex('$ProcessName', '/'); print substr('$ProcessName', \\$I+1);"`
  
  echo "$0> killall -ewq $ProcessName"
  killall -ewq $ProcessName
}

#BinaryGDB="$GDB_PWD/bin/arm-none-eabi-gdb"
BinaryGDB="bin/arm-none-eabi-gdb"

if [ "$Option" == "DDD" ]; then            #{ configure DDD binary for selected interface
  rm -f bin/ddd # avoid to find bin/ddd instead of /usr/bin/ddd
  BinaryDDD=`which ddd`
  if [ "$BinaryDDD" != "" ]; then
    echo "using DDD binary $BinaryDDD"
    UseDDD="1"
    BinaryFrontEnd="ddd"
    ln -sf $BinaryDDD bin/ddd
  else
    echo "$0 - ERROR: ddd binary not found. Install ddd and retry!"
  fi
fi #}
if [ "$Option" == "KDBG" ]; then           #{ configure KDBG binary for selected interface
  rm -f bin/kdbg # avoid to find bin/kdbg instead of /usr/bin/kdbg
  BinaryKDBG=`which kdbg`
  if [ "$BinaryKDBG" != "" ]; then
    echo "using KDBG binary $BinaryKDBG"
    UseKDBG="1"
    BinaryFrontEnd="kdbg"
    ln -sf $BinaryKDBG bin/kdbg
  else
    echo "$0 - ERROR: kdbg binary not found. Install kdbg and retry!"
  fi
fi #}
if [ "$BinaryFrontEnd" == "" ]; then       #{ no graphical frontend: choose text interface gdb as frontend
  BinaryFrontEnd="arm-none-eabi-gdb"
fi #}
if [ "$BinaryFrontEndSuffix" != "" ]; then #{ create symlink to frontend binary for interface specific task name
  BinaryFrontEnd2="bin/${BinaryFrontEnd}_$BinaryFrontEndSuffix"
  ln -sf $BinaryFrontEnd $BinaryFrontEnd2
  BinaryFrontEnd=$BinaryFrontEnd2
fi #}
killProcesses $BinaryFrontEnd
if [ -x "$GDB_PWD/bin/$BinaryFrontEnd" ]; then
  BinaryFrontEnd="$GDB_PWD/bin/$BinaryFrontEnd"
fi
if [ "$PortGDB" == "" ]; then              #{ no special GDB port given: use default OpenOCD port for GDB connection
  PortGDB="3333"
fi #}
if [ "$FILE" == "" ]; then #{
  echo "$0 TASK_NAME_SUFFIX GDB_PORT ELF_BINARY [DDD|KDBG]"
  echo "Possible binaries: `ls *.elf`"
  echo ""
  echo "Examples:"
  echo "    $0 '' '' main.elf"
  echo "    $0 'interface2' '3334' main.elf"
  echo ""
  
  exit 10
fi #}
if [ ! -e "$FILE" ]; then #{
  echo "$0 - ERROR: Cannot read binary-file '$FILE' in `pwd`!"
  exit 11
fi #}

Script1="configs/debug1_${FILE}.gdb"
if [ ! -e $Script1 ]; then #{
  echo "creating missing gdb start-script '`pwd`/$Script1'"
  cat >>$Script1 <<END_OF_SCRIPT
# GNU Debugger Startscript #1 for symbol file '${FILE}'
#
# This script is automatically given to GDB on every start of $0.
# If deleted, this file will be reconstructed automatically.
#

symbol-file $FILE

END_OF_SCRIPT
fi #}
Script2="configs/debug2_general.gdb"
Date=`date`
if [ ! -e $Script2 ]; then #{
  echo "creating missing gdb start-script '`pwd`/$Script2'"
  cat >>$Script2 <<END_OF_SCRIPT
# GNU Debugger startscript #2 for general debugger setup 
#
# This script is executed by _/gdb.sh for every symbol file.
# It may contain all break points and other setting to be activated at 
# start of each gdb session-
# If deleted, this file will be reconstructed automatically.
#
# Created at Di 12. Apr 09:15:29 CEST 2016

# CHANGE LINE BELOW ACCORDING TO YOUR SETUP!
#target remote 127.0.0.1:3333  # connect to openocd runnning on local machine
#target remote 192.169.0.1:3333 # connect to openocd running on another machine (here 192.169.0.1)

# disable interrupts during stepping makes debugging with scheduler easier
define hook-step
mon cortex_m maskisr on
end
define hookpost-step
mon cortex_m maskisr off
end

# amount of hardware breakpoints/ watchpoints cannot be set by openocd
#set remote hardware-breakpoint-limit 6
set remote hardware-watchpoint-limit 4

monitor reset init
monitor soft_reset_halt
monitor sleep 100
set debug arm

# set output format of values to hexadecimal format (instead of decimal)
set output-radix 16

# gdb will format structures nicely
set print pretty on

# Use of python pretty printers seems to be disabled in arm-none-eabi-gdb v7.1
# -> http://stackoverflow.com/questions/15156555/defining-gdb-print-function-for-c-structures
# source configs/struct_printers.py
# define pstruct
# p ()
# end


# Some interesting places to break at:
break ttc_assert_break_origin   # General fault handler for all self test asserts
#break Reset_Handler             # Startup code (Assembly code that prepares start of C program)
#break ttc_extensions_prepare    # Autostart of all activated extensions
#break main_prepare              # Autostart in main.c
#break ttc_task_start_scheduler  # Preparation of all activated extensions finished. About to enter main event loop. 
#break ttc_task_create           # A new task is to be created

adv main                        # Run to main() function

END_OF_SCRIPT
fi #}

if [ "$UseDDD" == "1" ]; then             #{ start DDD as graphical debugger frontend         
  echo "gdb_cotexm3.sh: \e[33m** Starting GDB via graphical frontend DDD...\e[0m"
  echo "$GDB_PWD> $BinaryFrontEnd --debugger $BinaryGDB --eval-command=\"source $Script1\" --eval-command=\"target remote 127.0.0.1:$PortGDB\" --eval-command=\"source $Script2\""
  $BinaryFrontEnd --debugger $BinaryGDB --eval-command="source $Script1" --eval-command="target remote 127.0.0.1:$PortGDB" --eval-command="source $Script2" >gdb.log 2>&1 &
  PID_BinaryFrontEnd=$!
  echo "kill -9 $PID_BinaryFrontEnd >/dev/null 2>&1">>kill_gdb.sh
  wait $PID_BinaryFrontEnd
  #CMD="$BinaryFrontEnd --debugger $BinaryGDB --eval-command=\"source $Script1\" --eval-command=\"target remote 127.0.0.1:$PortGDB\" --eval-command=\"source $Script2\""  <-- assigning to $CMD destroys command line for unknown reason!
  DebuggerStarted="1"
fi #}
if [ "$UseKDBG" == "1" ]; then            #{ start KDBG as graphical debugger frontend
  echo "\e[33m** Starting GDB via graphical frontend KDBG...\e[0m"
  File_KDBGRC="$HOME/.kde/share/config/kdbgrc"
  replaceLinesInFile "$File_KDBGRC" "DebuggerCmdStr=" "DebuggerCmdStr=$BinaryGDB -q -x $Script1 -ex \"target remote 127.0.0.1:$PortGDB\" -x $Script2 $@ --fullname" # --nx"
  addLineBefore      "$File_KDBGRC" "ExecutableFile=" "DebuggerCmdStr="
  replaceLinesInFile "$File_KDBGRC" "ExecutableFile=" "ExecutableFile=main.elf"
  addLineBefore      "$File_KDBGRC" "TabWidth="       "TTYLevel=0"
  
  #CMD="$BinaryFrontEnd -r 127.0.0.1:$PortGDB main.elf"
  echo "$0> $BinaryFrontEnd -r 127.0.0.1:$PortGDB main.elf"
  #X $BinaryFrontEnd -r 127.0.0.1:$PortGDB main.elf
  $BinaryFrontEnd -t gdb.log main.elf
  DebuggerStarted="1"
fi #}
if [ "$DebuggerStarted" == "" ]; then     #{ no special debugger started: start text interface debugger  
  # CMD="$BinaryFrontEnd -q -x $Script1 -ex \"target remote 127.0.0.1:$PortGDB\" -x $Script2 $@" <-- assigning to $CMD destroys command line for unknown reason!
  echo "gdb.sh> $BinaryFrontEnd -q -x $Script1 -ex \"target remote 127.0.0.1:$PortGDB\" -x $Script2 $@"
  $BinaryFrontEnd -q -x $Script1 -ex "target remote 127.0.0.1:$PortGDB" -x $Script2 $@
fi #}

if [ "$CMD" != "" ]; then #{
#X  echo "gdb.sh> PWD=`pwd`"
  cd "$GDB_PWD"
  echo "gdb.sh> $CMD"
  $CMD 2>/dev/null
fi #}

echo "killing gdb-server 'openocd_$BinaryFrontEndSuffix'..."
killProcesses "openocd_$BinaryFrontEndSuffix"
