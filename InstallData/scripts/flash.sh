#!/bin/bash

Binary="$1"
shift

if [ "$Binary" == "" ]; then
  Binary="main.bin"
fi
if [ ! -e "$Binary" ]; then
  cat <<END_OF_HELP
  
$0 BINARY [INTERFACE.CFG]"

BINARY         binary file to flash onto uC (default: main.bin)
INTERFACE.CFG  extra configuration file passed to low-level flashing application (openocd)

Note: You may provide your own target specific flash script.
      Simply create a shell script named target_flash.sh in the current folder and make it executable.


ERROR: Cannot find binary $Binary!
  
END_OF_HELP
  exit 5
fi

./createLinks.sh || exit 12
source _/SourceMe.sh

if [ -x target_flash.sh ]; then # using project specific flash script
  source target_flash.sh $@
else                            # using default openocd flash script
  source _/flash_current.sh $Binary $@
fi
if [ "$Error" == "" ]; then
  Bytes=`ls -l $Binary | awk '{ print $5 }'`
  echo ""
  echo "Successfully flashed $Binary ($Bytes bytes) onto uC."
  echo ""
else
  exit 10
fi
