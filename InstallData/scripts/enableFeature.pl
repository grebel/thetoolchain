#!/usr/bin/perl

my @Features = @ARGV;

my $Verbose = 1;
if (! -e "required_version" ) {
  print "$0 - ERROR: This script must be run inside a project folder!\n";
  exit 20
}
unless (@Features) {
  print <<"END_OF_HELP";
$0 <FEATURE1> [,<FEATURE2> [...]] | [UPDATE] [LIST]

Enable one or more low-level extensions for later activation.
Most low-level extensions must be enabled before they can be activated.
This script will enable the given feature and all dependend features.

Concept of Automated Activation Low-Level Extensions

The basic idea is that extensions for printed circuit boards can say which features they provide at an early stage of project activation.
When the user then tries to activate a high-level extension like a graphic driver, then this extension shall automatically activate the 
correct low-level driver for the current hardware.
Most extensions consist of one high-level activate script (rank 500) and several low-level install scripts (rank 450).
When the activate.500_* script is run, it will automatically run all of its activate.450_* scripts.
Each activate.450_* script looks for a corresponding file extensions.active/feature.450_*.
If this file is found, then the low-level extension activates itself and creates makefile.* and source.* files in extensions.active/.


Example list of activate scripts for ttc_interrupt:
  extensions/activate.500_ttc_interrupt.sh
  extensions/activate.450_interrupt_cortexm3.sh
  extensions/activate.450_interrupt_stm32l1xx.sh
  extensions/activate.450_interrupt_stm32f1xx.sh
  extensions/activate.450_interrupt_stm32w1xx.sh


Arguments:
  <>          Angular brackets indicate placeholder for a user defined value (do not type brackets!)
  []          Argument is optional                                 (do not type rectangle brackets!)
  |           Logical or. User can choose between different options      (do not type vertical bar!)
 
  <FEATUREn>  <rank number>_<extension name> of a low-level driver to activate.
              Value of <rank number> typically is 450.
              <extension name> can be any of (run in project folder): 
              find extensions/ -name "makefile.450_*" -exec basename {} \;

  UPDATE      if this keyword is given, $0 will look for new features in rank 450 and update itself.
  LIST        if this keyword is given, $0 will list all currently available features
  
Examples:
  $0 450_cpu_stm32f1xx
  $0 450_radio_stm32w1xx 450_cpu_stm32w1xx
  $0 UPDATE
  

This script is part of TheToolChain (thetoolchain.com) written by Gregor Rebel 2010-2018.

END_OF_HELP

  exit 0;
}

my %Dependencies;
my $Module   = "features.pm";
my $Database = "_/$Module";
my $Flag     = "_/features.UPDATE";
if (-e $Flag) { # externally forced database update
    if ($Verbose > 0) { print "Found '$Flag': will update dabase $Database\n"; }
    unshift @Features, 'UPDATE'; 
    unlink($Flag);
}
if (! -e $Database)  { # cause immediate database update
    if ($Verbose > 0) { print "Will create missing database $Database (TODO: Edit this file to configure dependencies!)\n"; }
    unshift @Features, 'UPDATE'; 
}

my $CMD_Update, $CMD_List;
my %UniqueFeatures;
my @UniqueFeatures;
map {
     if ($_ eq 'UPDATE') { $CMD_Update = 1; }
  elsif ($_ eq 'LIST')   { $CMD_List   = 1; }
  elsif (! $UniqueFeatures{$_}) {
    $UniqueFeatures{$_} = 1;
    push @UniqueFeatures, $_;
  }
} @Features;

if (-e $Database) {                  # load database
  if ($Verbose > 1) { print "Loading database $Database..\n"; }
  unshift(@INC, './_');
  require $Module;
  %Dependencies = Features::loadData();
  if ($Verbose > 1) { print "Loaded ".(scalar keys %Dependencies)." features from database.\n"; }
}
if ($CMD_Update) {                   # update database of available features
  if ($Verbose > 1) { print "Updating database $Database..\n"; }
  my $Extensions;
  map {
    $Extensions .= `find extensions/ -name activate.${_}_*`
  } qw(110 450);
  my @Extensions = split("\n", $Extensions);
  
  my @NewExtensions;
  @Extensions = map { # find new extension names
                  unless ($Dependencies{$_}) { 
                    push @NewExtensions, $_;
                    $Dependencies{$_} = undef;
                  }
                }  
                map { # extract extension names
                      $_ =~ m/.+\.(.+)\..+/;
                      $1;
                    } @Extensions;
  
  if (@NewExtensions) {
    if ($Verbose > 0) { print "  Found ".(scalar @NewExtensions)." new extensions:\n    ".
                                       join("\n    ", @NewExtensions)."\n\n";
                      }
  }
  else { if ($Verbose > 1) { print "  Found no new features.\n"; } }

  if (1) { # automatically update all 450_cpu_* dependencies 
    map {
      my $CpuFeature = $_;
      my $CpuType    = substr($CpuFeature, 8);
      my %CpuDependencies = map { $_ => 1 } @{ $Dependencies{$_} };
      
      map { $CpuDependencies{$_} = 1; }       # add feature to dependencies of current cpu feature (if not already done) 
      grep { ($_ ne $CpuFeature); }           # that are no cpu feature
      grep { ( index($_, $CpuType) > -1 ); }  # all features containing this cpu type name
      keys %Dependencies;
      
      my @CpuDependencies = sort keys %CpuDependencies;
      $Dependencies{$_} = \@CpuDependencies;
    }
    grep { substr($_, 0, 8) eq '450_cpu_'; }
    keys %Dependencies;
  }
  (my $Spaces, my $MaxLength) = compileFieldSpaces( keys %Dependencies );
  
  my $Compiled = "      ".
                 join( "\n      ", 
                       map {
                             unless ( ref($Dependencies{$_}) ) { $Dependencies{$_} = []; }
                             my @List = sort 
                                        grep { defined $Dependencies{$_} }  # pass only known features 
                                        grep { $_; }                        # remove empty strings
                                        @{ $Dependencies{$_} };
                             my $List = (@List) ? "[ #{ ],\n                                                       '".
                                                  join("',\n                                                       '", @List).
                                                  "'\n                                                     ], #}" 
                                                : "[  ],";
                             substr("'$_'$Spaces", 0, $MaxLength + 2)." => $List";
                           } sort keys %Dependencies
                     );
  my $TimeStamp = getTimeStamp();
  my $Author = $ENV{USER}.'@'.`hostname`; chomp($Author);
  
  if (@NewExtensions) { # new extensions found: update database file
    writeFile( $Database, <<"END_OF_FILE" ); #{
#
# Database of all features and their dependencies.
#
# This file is part of TheToolChain written by Gregor Rebel 2010-2018.
#
# $Database last updated: $TimeStamp by $Author
#
# Execute this inside a project folder to update this file:
#   $0 UPDATE 

package Features;

use strict;
use Exporter;
use vars qw(\$VERSION \@ISA \@EXPORT \@EXPORT_OK \%EXPORT_TAGS);

\$VERSION     = 1.00;
\@ISA         = qw(Exporter);
\@EXPORT      = qw(loadData);
\@EXPORT_OK   = qw(loadData);
\%EXPORT_TAGS = ( DEFAULT => [qw(&loadData)],
                 Both    => [qw(&loadData)]
               );

sub loadData {
  my \%Dependencies =
  ( #{ features that will enable depending features too
    
#                                              Note: [ Unknown features will be removed on next UPDATE! ] 
$Compiled
   ); #}

  return \%Dependencies;
}

1;

END_OF_FILE
#}
  }
}

if ($CMD_List)   { # list all available features
  print "\n$0 - Available features:\n    ".
        join("\n    ", sort keys %Dependencies)."\n\n";
  exit 0;
}

my @Features = solveDependencies('', @UniqueFeatures);
enableFeatures(@Features);
exit 0;

sub enableFeatures() {            # enable all given features (no dependencies resolved)
  my @Features = @_;
  
  if ($Verbose > 1) { print "trying to enable ".(scalar @Features)." features:\n"; }
  map {
    if ($Verbose > 0) { print "  enabling feature '$_'\n"; }
    writeFile("extensions.active//feature.$_", "");
  } @Features;
}
sub createDirectoryPath {         # creates a multi-level directory path from top to down
  my $DirectoyPath = shift; # relative or absolute directory path (without filename)
  
  my @DirectoyPath = split("/", $DirectoyPath);
  
  my $CurrentPath;
  map {
    unless ($CurrentPath) { $CurrentPath = $_; }
    else                  { $CurrentPath .= '/'.$_; }
    unless (-d $CurrentPath) { mkdir($CurrentPath); }
  } grep { $_; } @DirectoyPath; 
}
sub compileFieldSpaces {          # compiles a string of spaces as long as longest string in given list
  my @Strings = @_; # any amount of strings
  
  # Usage Example:
  #   (my $Spaces, my $MaxLength) = compileFieldSpaces( @Strings );
  #   my @Formatted = map { substr($_.$Spaces, 0, $MaxLength); } @Strings;  
  
  my $MaxLength = 0;
  map { if ($MaxLength < length($_)) { $MaxLength = length($_); } } @Strings;
  
  my $Spaces; 
  while (length($Spaces) < $MaxLength) { $Spaces .= ' '; }

  return ($Spaces, $MaxLength);
}
sub getTimeStamp {                # returns current utc time stamp
  my $TimeStamp = `date --utc`; chomp($TimeStamp);
  utf8::decode($TimeStamp);
  
  return $TimeStamp;
}
sub readFile {                    # read in content of given file
  my $FilePath = shift;
  
  my $Content;
  my $Error;
  open(IN, "<$FilePath") or $Error = "$0 - ERROR: Cannot read from file '$FilePath' ($!)";
  dieOnError($Error);
  while (<IN>) { $Content .= $_; }
  close(IN);
  
  $Content =~ s/\\\n//g; # concatenate multiline strings
  
  return $Content;
}
my %sD_Features;
sub solveDependencies() {         # recursively finds all features depending on given features 
  my $Indent   = shift;
  my @Features = @_;
  
  unless ($Indent) { %sD_Features = (); }
  
  while (@Features) {
    my $Feature = shift(@Features);
    unless ($sD_Features{$Feature}) {
      if ($Dependencies{$Feature}) {
        if ($Verbose > 1) { print $Indent."solveDependencies($Feature)\n"; }
        $sD_Features{$Feature} = 1;
        push @Features, solveDependencies($Indent.'  ', @{ $Dependencies{$Feature} });
      }
      else { print "$0 - ERROR: Unknown feature '$Feature'!\n"; }
    }
  };
  
  @Features = sort keys %sD_Features; 
  return @Features;
}
sub strip {                       # removes invisible characters at begin + end of given string
  my $Line = shift || "";

  my %IsWhiteChar = ( ' ' => 1, "\n" => 1, "\r" => 1, "\t" => 1 ); 

  $Line =~ s/^\s+//;
  $Line =~ s/\s+$//;
  #$Line =~ s/(?:^ +)||(?: +$)//g;
  #$Line =~ s/^\ *([A-Z,a-z,0-9]*)\ *$/\1/g;
  #$Line =~ s/^\s+//;
  #$Line  =~ s/\s+$//;
  return $Line;
  
  my $lIndex = 0;
  while ( $IsWhiteChar{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsWhiteChar{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  return substr($Line, $lIndex, $rIndex - $lIndex + 1);

}
sub writeFile {                   # write content to given file
  my $FilePath     = shift;
  my $Content      = shift;
  my $BackUpSuffix = shift; # if given and original file exists, a backup file is being created using this suffix
  
  # ensure that directoy exists
  my $LastSlashPos = rindex($FilePath, '/');
  if ($LastSlashPos > -1) { # file is to be created in different folder: ensure that folder exists
    my $DirPath = substr($FilePath, 0, $LastSlashPos);
    createDirectoryPath($DirPath);
  }
  
  if ($Verbose>1) { print "Writing to disc: $FilePath\n"; }
  if ($BackUpSuffix) {
    if (-e $FilePath) {
      system("mv \"${FilePath}\" \"${FilePath}$BackUpSuffix\"");
    }
  }
  open(OUT, ">$FilePath") or die ("$0 - ERROR: Cannot write to file '$FilePath' ($!)");
  print OUT $Content;
  close(OUT);
  
  return $Content;
}
