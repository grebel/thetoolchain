#!/usr/bin/perl
#
#                         The ToolChain
#
#  Central management script for global file operations. 
#
#  written by Gregor Rebel 2012-2014
#


use File::Find;

my $LastSlashPos = rindex("$0", "/");
my $MySelf = substr($0, $LastSlashPos + 1);
my @Commands = ( \&cmd_Replace, #{ list of all available commands 
                 \&cmd_MoveFile,
                 \&cmd_RenameFile,
                 \&cmd_AddFile, 
                 \&cmd_DelFile, 
                 \&cmd_WarnDeleted,
                 \&cmd_AddFeature,
                 \&cmd_Comment,
                 \&cmd_Deprecated
                 ); #}

my $Command = shift(@ARGV);
map { # fill %Commands with NAME->FunctionPtr  pairs
  my $CommandName = &{$_}(NAME);
  $Commands{$CommandName} = $_;
} @Commands;

my $Error;
unless ($Error || $Command) {            # missing command
  $Error = "Missing argument COMMAND!";
}
unless ($Error || $Commands{$Command}) { # unknown command 
  $Error = "Unknown command: '$Command'!";
}

my $StartDir = `pwd`; chomp($StartDir);      # directory path from where this script was started
my $BaseDir;           # The ToolChain base directory
my $IsGitRepo;         # ==1: found .git folder in $BaseDir
my $RelativePath;      # path to $StartDir relative to $BaseDir
my $InsideProject = 0; # ==1: started inside a TTC project folder

if (-e "$StartDir/required_version") { # started inside project folder
  $InsideProject = 1;
  $BaseDir = `pwd`; chomp($BaseDir);
}
else {                     # check if started inside The ToolChain base directory
  unless ($Error) {     # move to ttc base directory
      while (! -d "InstallData") {
        chdir("..");
        my $PWD = `pwd`; chomp($PWD);
        print "moving up to '".$PWD."'\n";
        if ( "$PWD" eq '/' ) { last; }
      }
      $BaseDir = `pwd`; chomp($BaseDir);
      
      $IsGitRepo = (-d .git) ? 1 : 0;
    
      # calculate path to $StartDir relative to $BaseDir
      $RelativePath = substr($StartDir, length($BaseDir));
      if (substr($RelativePath, 0, 1) eq '/') { substr($RelativePath, 0, 1) = undef; }
  }
  unless ($IsGitRepo) { # did not start within git repository: try something else
    if ( $ENV{TTC_DIR_REPOSITORY} && (-d $ENV{TTC_DIR_REPOSITORY}) ) { # found environment variable: cd $ENV{TTC_DIR_REPOSITORY}
      chdir($ENV{TTC_DIR_REPOSITORY});
      $IsGitRepo = (-d .git) ? 1 : 0;
      $BaseDir = `pwd`; chomp($BaseDir);
      my $Infix = "TheToolChain_devel/";
      my $Pos = index($StartDir, $Infix);
      if ($Pos > -1) {                          # inside TheToolChain_devel/
        $RelativePath = joinPath( $BaseDir, substr($StartDir, $Pos + length($Infix)) ); 
      }
      elsif (index($StartDir, $BaseDir) == 0) { # inside git repository
        $RelativePath = substr($StartDir, length($BaseDir));
      }
    }
  }
  unless ($BaseDir) { $Error = "Cannot find TheToolChain base directory!"; }
}
if ($Error) {         # print help + exit
  my $MaxNameLength = 0;
  map { my $Length = length( &{$_}(NAME) );
        if ($MaxNameLength < $Length) { $MaxNameLength = $Length; } 
      } @Commands;
  
  my $MaxArgsLength = 0;      
  map { my $Length = length( &{$_}(ARGS) );
        if ($MaxArgsLength < $Length) { $MaxArgsLength = $Length; } 
      } @Commands;

  my $Spaces = '';
  while (length($Spaces) < $MaxNameLength) { $Spaces .= ' '; }
  while (length($Spaces) < $MaxArgsLength) { $Spaces .= ' '; }
  
  my $CommandList = "    ".
                    join("\n    ", 
                      map { substr( &{$_}(NAME).$Spaces, 0, $MaxNameLength).'   '.
                            substr( &{$_}(ARGS).$Spaces, 0, $MaxArgsLength).'   '.
                            &{$_}(INFO);
                      } sort { &{$a}('NAME') cmp &{$b}('NAME') } @Commands);
  
  print <<"END_OF_HELP"; 

$MySelf COMMAND

    Central managing tool for global changes on The ToolChain (-> http://TheToolChain.com).
    Changes will affect all scripts, documentations and source files.
    Changes are automatically added to readme.History and to update scripts.

    Available Commands:
$CommandList

    written by Gregor Rebel 2012-2014


    ERROR: $Error

END_OF_HELP
  exit 10;
}

my $TTC_Version = ($InsideProject) ? strip(`cat required_version`) : strip(`cat Version`); 

print "$0 running inside TTC ".('base', 'project')[$InsideProject]." folder $BaseDir/ (v $TTC_Version)\n";

print "collecting files...\n";
my @IgnoreFiles = ( '\/\.git\/.+', #{ do not operate on these files 
                    '\.pending_commits', 
                    'readme.History', 
                    '\/Updates\/.+',
                    'DEPRECATED_.+'
                  ); #}
if ($InsideProject) { push @IgnoreFiles, ('\/ttc-lib\/.+', '\/additionals\/.+'); }
my @AllFiles = getAllFiles( ".", \@IgnoreFiles );
my @Errors;

print "$BaseDir> $MySelf $Command ",join(" ", @ARGV)."\n";
&{$Commands{$Command}}(@ARGV);

if (@Errors) { # print errors + synopsis
  print "\nERROR:\n    ".join("\n    ", @Errors)."\n\n";
  print "Synopsis: $MySelf $Command ".&{$Commands{$Command}}('ARGS')."\n\n";
  exit 11;
}

exit 0;

sub cmd_Template {
  my $CmdName = 'TEMPLATE';
  
  if ($_[0] eq 'ARGS') { return "ARG1"; }
  if ($_[0] eq 'INFO') { return ""; }
  if ($_[0] eq 'NAME') {
    return $CmdName;
  }

  # read + test arguments here...
  my $ARG1 = shift;
  unless ($ARG1) { push @Errors, "$CmdName: No ARG1 string given!"; }
  if (@Errors) { return; }

  # place your implementation here...  
}
sub cmd_WarnDeleted {
  my $CmdName = 'warn_deleted';
  
  if ($_[0] eq 'ARGS') { return "FILENAME"; }
  if ($_[0] eq 'INFO') { return "ToDo: scans all files + reports warning for every found reference to FILENAME"; }
  if ($_[0] eq 'NAME') {
    return $CmdName;
  }

  # read + test arguments here...
  my $FILENAME = shift;
  unless ($FILENAME) { push @Errors, "$CmdName: No FILENAME string given!"; }
  if (@Errors) { return; }

  # place your implementation here...  
}
sub cmd_Replace {
  my $CmdName = 'replace';
  
  if ($_[0] eq 'ARGS') { return "PATTERN,REPLACE"; }
  if ($_[0] eq 'INFO') { return "replace all occurances of PATTERN by REPLACE in all files"; }
  if ($_[0] eq 'NAME') { return $CmdName; }

  my $Pattern = shift;
  my $Replace = shift;
  
  unless ($Pattern)         { push @Errors, "No PATTERN string given!"; }
  unless ($Replace)         { push @Errors, "No REPLACE string given!"; }
  if ($Pattern eq $Replace) { push @Errors, "PATTERN == REPLACE == '$Pattern'"; }
  if (@Errors) { return; }
  
  print "filtering files against '$Pattern'..\n";
  my @ReplaceFiles = filterFiles(\@AllFiles, WantedFileContents => [ $Pattern ]);
  foreach my $File (@ReplaceFiles) {
    my $Comment = "chg: $File - replace '$Pattern' -> '$Replace'";
    print "$Comment\n";
    my $Content = readFile($File);
    if ( (index($Pattern, '/') > -1) || (index($Replace, '/') > -1) ) { $Content =~ s|$Pattern|$Replace|g; }
    else                                                              { $Content =~ s/$Pattern/$Replace/g; }
    writeFile($File, $Content);
    my $Update = "$MySelf replace \"$Pattern\" \"$Replace\"";
    documentOperation($Comment, $Update);
  }
}
sub cmd_RenameFile  {
  my $CmdName = 'renamefile';
  
  if ($_[0] eq 'ARGS') { return "OLD_FILENAME,NEW_FILENAME"; }
  if ($_[0] eq 'INFO') { return "renames a file at its location"; }
  if ($_[0] eq 'NAME') { return "$CmdName"; }

  my $OldFile     = correctFilePaths( shift() );
  my $NewFile     = correctFilePaths( shift() );
  my $OldFileDir  = extractFileDir($OldFile);
  my $NewFileDir  = extractFileDir($NewFile);
  my $OldFileName = extractFileName($OldFile);
  my $NewFileName = extractFileName($NewFile);
  my @NewError;
  
  my $PWD = `pwd`; chomp($PWD);
  unless ($OldFile)                 { push @NewError, 'missing argument OLD_FILENAME'; }
  unless ($NewFile)                 { push @NewError, 'missing argument NEW_FILENAME'; }
  unless (-e $OldFile)              { push @NewError, "missing file '$OldFile' in $PWD"; }
  if (index($OldFile, '*') > -1)    { push @NewError, "no wildcards '*' allowed (got '$OldFile')"; }
  if (index($NewFile, '*') > -1)    { push @NewError, "no wildcards '*' allowed (got '$NewFile')"; }
  if ($OldFileDir  ne $NewFileDir)  { push @NewError, "command does not allow moving a file ('$OldFileDir/' != '$NewFileDir/')"; }
  if ($OldFileName eq $NewFileName) { push @NewError, "OLD_FILENAME == NEW_FILENAME == '$OldFileName'"; }
  if (@NewError) { push @Errors, @NewError; return; }
  
  print "moving '$OldFile' -> '$NewFile'\n";
  my $CMD = "mv \"$OldFile\" \"$NewFile\" 2>&1 1>/dev/null";
  if ($IsGitRepo) { $CMD = 'git '.$CMD; }
  my $Error = `$CMD`;
  if ($Error) { push @Errors, "could not move '$OldFile' -> '$NewFile' ($Error)\n"; }
  else {
    my $Old = extractFileName($OldFile);
    my $New = extractFileName($NewFile);

    my @Suggestions;
    if (isSourceFile($Old)) {
      my $ObjectOld = $Old; $ObjectOld =~ s/\.c$/\.o/;
      my $ObjectNew = $New; $ObjectNew =~ s/\.c$/\.o/;
      
      push @Suggestions, "$MySelf replace \"$Old\" \"$New\"";
      if ($ObjectOld ne $Old) {
        push @Suggestions, "$MySelf replace \"$ObjectOld\" \"$ObjectNew\"";
      }
      
    }
    documentOperation("ren: '$OldFile' -> '$NewFile'", $Update);
    if (@Suggestions) {
      print "\nSuggestions:\n    ".join("\n    ", @Suggestions)."\n";
    }
  }
}
sub cmd_MoveFile  {
  my $CmdName = 'movefile';
  
  if ($_[0] eq 'ARGS') { return "OLD_FILENAME,NEW_FILENAME"; }
  if ($_[0] eq 'INFO') { return "will move a file to a new location"; }
  if ($_[0] eq 'NAME') { return "$CmdName"; }

  my $OldFile = strip( correctFilePaths( shift() ) );
  my $NewFile = strip( correctFilePaths( shift() ) );
  if (substr($NewFile, -1) eq '/') { $NewFile .= extractFileName($OldFile); }
  my $OldFileName = extractFileName($OldFile);
  my $NewFileName = extractFileName($NewFile);
  
  unless ($OldFile) { push @Errors, 'missing argument OLD_FILEPATH'; }
  unless ($NewFile) { push @Errors, 'missing argument NEW_FILEPATH'; }
  unless (-e $OldFile) { push @Errors, "missing file '$OldFile'"; }
  if (index($OldFile, '*') > -1) { push @Errors, "no wildcards '*' allowed (got '$OldFile')"; }
  if (index($NewFile, '*') > -1) { push @Errors, "no wildcards '*' allowed (got '$NewFile')"; }
  if ($OldFileName ne $NewFileName) { push @Errors, "command does not allow renaming ('$OldFileName' != '$NewFileName')"; }
  if (@Errors) { return; }
  
  print "moving '$OldFile' -> '$NewFile'\n";
  my $CMD = "mv \"$OldFile\" \"$NewFile\" 2>&1 1>/dev/null";
  if ($IsGitRepo) { $CMD = 'git '.$CMD; }
  my $Error = `$CMD`;
  if ($Error) { push @Errors, "could not move '$OldFile' -> '$NewFile' ($Error)\n"; }
  else {
    my $Old = extractFileName($OldFile);
    my $New = extractFileName($NewFile);
    my $Update;
    if (isSourceFile($Old)) {
      if ($Old ne $New) { $Update = "$MySelf replace \"$Old\" \"$New\""; }
    }
    documentOperation("ren: '$OldFile' -> '$NewFile'", $Update);
  }
}
sub cmd_AddFile {
  my $CmdName = 'addfile';
  
  if ($_[0] eq 'ARGS') { return "NEW_FILENAME1 [, NEW_FILENAME2, ..]"; }
  if ($_[0] eq 'INFO') { return "add a new file to git repository"; }
  if ($_[0] eq 'NAME') { return "$CmdName"; }

  foreach my $FilePath ( correctFilePaths(@_) ) {

    unless ($FilePath)     { push @Errors, "missing argument FILE!"; }
    unless (-d ".git") { push @Errors, "must be run inside git repository!"; }
    if (@Errors) { return; }
    
    print "adding '$FilePath'\n";
    my $Error = `git add $FilePath 2>&1 >/dev/null`;
    unless ($Error) { 
      print "added file '$FilePath'\n";
      documentOperation("new: $FilePath"); 
    }
    else { push @Errors, "$Error"; }
  }
}
sub cmd_AddFeature {
  my $CmdName = 'feature';
  my $Feature = join(" ", @_);
  
  if ($_[0] eq 'ARGS') { return "FEATURE_DESCRIPTION"; }
  if ($_[0] eq 'INFO') { return "name a new feature being added by current commit"; }
  if ($_[0] eq 'NAME') { return "$CmdName"; }

  my $Error;
  unless ($Feature) { $Error="$CmdName: Missing argument FEATURE_DESCRIPTION"; }
  unless ($Error) { 
    print "new feature: $Feature\n";
    documentOperation("feature: $Feature"); 
  }
  else { push @Errors, "$Error"; }
}
sub cmd_DelFile {
  my $CmdName = 'delfile';
  
  if ($_[0] eq 'ARGS') { return "FILENAME"; }
  if ($_[0] eq 'INFO') { return "remove a file from git repository"; }
  if ($_[0] eq 'NAME') { return "$CmdName"; }

  my $FilePattern = correctFilePaths( shift() );
  
  unless ($FilePattern)    { push @Errors, 'missing argument FILENAME'; }
  my @DelFiles;
  unless (@Errors) {
    @DelFiles = findFiles($FilePattern);

    map {
      unless (-e $_) { push @Errors, "file not found: '$_'"; }
    } @DelFiles;
  }
  unless (@DelFiles) { push @Errors, "no files found matching '$FilePattern' in ".`pwd`; }
  if (@Errors) { return; }
  
  foreach my $DelFile (@DelFiles) {
    print "deleting '$DelFile'\n";
    my $CMD = "rm \"$DelFile\" 2>&1 >/dev/null";
    if ($IsGitRepo) { $CMD = 'git '.$CMD; }
    my $Error = `$CMD`;
    if ($Error) { push @Errors, "Could not delete file '$DelFile' ($Error)"; }
    else        { documentOperation("del: $DelFile", "$MySelf warn_deleted $DelFile"); }
  }
}
sub cmd_Comment {
  my $CmdName = 'comment';
  
  if ($_[0] eq 'ARGS') { return "COMMENT"; }
  if ($_[0] eq 'INFO') { return "store a comment for next git commit"; }
  if ($_[0] eq 'NAME') { return "$CmdName"; }

  my $Comment = shift;
  if ($Comment) { documentOperation("$Comment"); } 
}
sub cmd_Deprecated {
  my $CmdName = 'deprecatedfile';
  
  if ($_[0] eq 'ARGS') { return "FILENAME1 [,FILENAME2 [,...]]"; }
  if ($_[0] eq 'INFO') { return "rename given filenames to DEPRECATED_*"; }
  if ($_[0] eq 'NAME') { return "$CmdName"; }

  my $MyStartDir = $StartDir;
  foreach my $OldFile (@_) {
    chdir($MyStartDir); 
    $OldFile        = correctFilePaths($OldFile);
    my $OldFileDir  = extractFileDir($OldFile);
    my $OldFileName = extractFileName($OldFile);
    chdir($BaseDir);
    my $DeprecatedFileName = 'DEPRECATED_'.$OldFileName;
    my $DeprecatedFile     = $OldFileDir.'/'.$DeprecatedFileName;
    
    if (substr($OldFileName, 0, 11) ne 'DEPRECATED_') {
      cmd_RenameFile($OldFile, $DeprecatedFile);
    }
  }
}

sub isSourceFile {      # returns true on all .c,.h.s files
  my $File = shift;
  
  my %IsSourceSuffix = ( '.c' => 1 , '.h' => 1, '.s' => 1 );
  return $IsSourceSuffix{extractSuffix($File)};
}
sub extractSuffix {     # foo.C -> .c
  my @Suffixes = map {
    my $LastDotPos = rindex($_, '.');
    if ($LastDotPos > -1) { lc( substr($_, $LastDotPos) ); }
    else { undef; }
  } @_;
  if (wantarray) { return @Suffixes; }
  else           { return $Suffixes[0]; }
}
sub extractFileName {   # dir1/dir2/foo.c -> foo.c
  
  my @FileNames = map {
    my $LastSlashPos = rindex($_, '/');
    my $FileName = substr($_, $LastSlashPos + 1);
    $FileName;
  } @_;

  if (wantarray) { return @FileNames; }
  else           { return $FileNames[0]; }
}
sub extractFileDir {    # dir1/dir2/foo.c -> dir1/dir2
  
  my @FileNames = map {
    my $LastSlashPos = rindex($_, '/');
    if ($LastSlashPos > -1) { substr($_, 0, $LastSlashPos); }
    else                    { undef; }
  } @_;

  if (wantarray) { return @FileNames; }
  else           { return $FileNames[0]; }
}
sub getAllFiles {       # scans local directory + subdirectory for all files
  my $Directory         = shift(@_); # directory path to scan
  my $RemovePatternsRef = shift(@_); # arrary-ref: regex patterns of unwanted files
  
  my @Files;
  
  sub addFile { 
    my $Dir      = $File::Find::dir;
    my $FullPath = $File::Find::name;
    
    my $IgnoreFile = 0;    
    if ($RemovePatternsRef) {
      foreach my $RemovePattern (@$RemovePatternsRef) {
        if ($FullPath =~ m/$RemovePattern/) { $IgnoreFile = 1; } 
      }
    }
    unless ($IgnoreFile) { push @Files, $FullPath; }
  }
  finddepth(\&addFile, $Directory);

  my @RealFiles;
  foreach my $File (@Files) {
    my $IgnoreFile = 0;
    unless (-f "$File") { $IgnoreFile = 1; }
    unless ($IgnoreFile) {
      push @RealFiles, $File;      
    }
  }

  return @RealFiles;
}
sub filterFiles {       # filters given list of files according to given patterns
  #{ Examples
  # 
  # find all file-paths ending with '.h' or '.c'
  # my @HeaderFiles = filterFiles(\@AllFiles, WantedFileNames => [ '.*\.h$', '.*\.c$' ]);
  #
  # find all file-paths containing with '/example_'
  # my @ExampleFiles = filterFiles(\@AllFiles, WantedFileNames => [ '/example_.*' ]);
  #
  # find all file-paths which file contains "benchmark_Line_ortho"
  # my @ExampleFiles = filterFiles(\@AllFiles, WantedFileContents => [ '.*benchmark_Line_ortho.*' ]);
  #}
  
  my $AllFilesRef        = shift; # array-ref: list of file-paths to filter
  my %Args = @_;
  my $RemovePatternsRef  = $Args{UnwantedFileNames};  # arrary-ref: regex patterns of unwanted files
  my $FindPatternsRef    = $Args{WantedFileNames};    # arrary-ref: regex patterns of wanted files
  my $ContentPatternsRef = $Args{WantedFileContents}; # arrary-ref: regex patterns of content to find in files

  my @AfterRemovePatterns;  
  if ($RemovePatternsRef) {
    foreach my $File (@$AllFilesRef) {
      my $UseFile = 1; 
      foreach my $RemovePattern (@$RemovePatternsRef) {
        if ($File =~ m/$RemovePattern/) { $UseFile = 0; } 
      }
      
      if ($UseFile) { push @AfterRemovePatterns, $File; }
    }
  }
  else { @AfterRemovePatterns = @$AllFilesRef; }
  
  my @AfterFindPatterns;
  if ($FindPatternsRef) {
    foreach my $File (@AfterRemovePatterns) {
      my $UseFile = 0; 
      foreach my $FindPattern (@$FindPatternsRef) {
        if ($File =~ m/$FindPattern/) { $UseFile = 1; } 
      }
      
      if ($UseFile) { push @AfterFindPatterns, $File; }
    }
  }
  else { @AfterFindPatterns = @AfterRemovePatterns; }
  
  my @AfterContentPatterns;
  if ($ContentPatternsRef) {  
    foreach my $File (@AfterFindPatterns) {
      
      my $Content = readFile($File);
      $UseFile = 0; 
      foreach my $ContentPattern (@$ContentPatternsRef) {
        if ($Content =~ m/$ContentPattern/) { $UseFile = 1; } 
      }
      
      if ($UseFile) { push @AfterContentPatterns, $File; }
    }
  }
  else { @AfterContentPatterns = @AfterFindPatterns; }
  
  my @FilteredFiles = @AfterContentPatterns;
  
  return @FilteredFiles;
}
sub findFiles {         # finds all files matching given wildcard
  our %FoundFiles;
  foreach my $WildCard (@_) {
    my $Files;
    if (index(${WildCard}, '*') > -1) { $Files = `find ./ -wholename "${WildCard}"`; }
    else                              { $Files = `find ./ -wholename "*${WildCard}*"`; }
    my @Files = split("\n", $Files);
    map { $FoundFiles{$_} = 1; }
    map { if (substr($_, 0, 2) eq './') { substr($_, 0, 2) = undef; } $_; } @Files;
  }
  @FoundFiles = grep { $_ } sort keys %FoundFiles;
  
  if (wantarray) { return @FoundFiles; }
  else           { return $FoundFiles[0]; }
}
sub readFile {          # read in content of given file
  my $FilePath = shift;
  
  my $Content;
  open(IN, "<$FilePath") or die ("$0 - ERROR: Cannot read from file '$FilePath' ($!)");
  while (<IN>) { $Content .= $_; }
  close(IN);
  
  return $Content;
}
sub writeFile {         # write content to given file
  my $FilePath = shift;
  my $Content  = shift;
  
  open(OUT, ">$FilePath") or die ("$0 - ERROR: Cannot write to file '$FilePath' ($!)");
  print OUT $Content;
  close(OUT);
  
  return $Content;
}
sub appendFileUnique {  # appends given lines to file (removes doubles)
  my $File = shift;
  
  my @Lines = (-e $File) ? split("\n", readFile($File)) : ();
  my %IsNewLine = map { $_ => 1 } @Lines;
  
  my $AddedLines = 0;
  foreach my $Line (@_) {
    unless ($IsNewLine{$Line}) {
      push @Lines, $Line;
      $AddedLines++;
    }
  }
  
  if ($AddedLines) { writeFile($File, join("\n", @Lines)."\n"); }
  
  return $AddedLines;
}
sub documentOperation { # document an operation for next git commit and update script
  my $Commitment  = shift;
  my $Update      = shift;
  
  if ($InsideProject) { return; } # not documenting operations inside project folders
  
  if ($Commitment) {
    my $CommitFile = ".pending_commits";
    appendFileUnique($CommitFile, $Commitment);
  }
  if ($Update) {
    if (-d "$BaseDir/Updates") {
      my $UpdateVersion = "$TTC_Version";
      my %IsNumber = (0=>1, 1=>1, 2=>1, 3=>1, 4=>1, 5=>1, 6=>1, 7=>1, 8=>1, 9=>1);
      if (! $IsNumber{ substr($UpdateVersion, -1) } )
      { substr($UpdateVersion, -1) = undef; } # remove trailing a or b
      my $UpdateFile = "$BaseDir/Updates/update_$UpdateVersion.sh";
      unless (-e $UpdateFile) { # create new update file
        open(OUT, ">>$UpdateFile") or print STDERR "\n$0 - ERROR: Cannot write to file '`pwd`/$UpdateFile' ($!)!\n";
        print OUT <<"END_OF_HEADER";
#!/bin/bash
#
# The ToolChain v$UpdateVersion
#
# Update script for changes introduced in v$UpdateVersion of The ToolChain.
#
# This script shall help you in porting your project to compile with a newer release of The ToolChain.
# Updatescripts must be run in order starting at first version released after your current project.
# In addtion to run this script, you should compile and check your project for every version update.
#

END_OF_HEADER
        close(OUT);
        system("chmod +x \"$UpdateFile\"");
        system("git add \"$UpdateFile\"");
        documentOperation("new: $UpdateFile");
      }
      
      if ( appendFileUnique($UpdateFile, $Update) ) { print ">>$UpdateFile: $Update\n"; }
      # else { print "$UpdateFile already contains $Update\n"; }
    }
  }
}
sub joinPath {          # joins file paths parts together

  my $AppendSlash = (substr($_[-1], -1) eq '/') ? 1 : 0;
  
  my $FilePath = join("/", map { 
                                 while (substr($_, -1)   eq '/') { substr($_, -1)   = undef };
                                 while (substr($_, 0, 1) eq '/') { substr($_, 0, 1) = undef };
                                 $_;
                               }
                               grep { $_ } @_
                     );

  if ($AppendSlash) { $FilePath .= '/'; } # restore slash at end of path
  return $FilePath;
}
sub strip {             # removes invisible characters at begin + end of given string
  my $Line = shift || "";

  my %IsWhiteChar = ( ' ' => 1, "\n" => 1, "\r" => 1, "\t" => 1 ); 

  $Line =~ s/^\s+//;
  $Line =~ s/\s+$//;
  #$Line =~ s/(?:^ +)||(?: +$)//g;
  #$Line =~ s/^\ *([A-Z,a-z,0-9]*)\ *$/\1/g;
  #$Line =~ s/^\s+//;
  #$Line  =~ s/\s+$//;
  return $Line;
  
  my $lIndex = 0;
  while ( $IsWhiteChar{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsWhiteChar{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  return substr($Line, $lIndex, $rIndex - $lIndex + 1);

}
sub correctFilePaths {  # remove superfluous characters from given filepaths
  
  my @CorrectedFiles =  map {
    s/^\.\///g;      # ./ ->     (at beginning)
    s/^$BaseDir//;   # remove absolute filepath to base directory of TTC-git 
    unless ( (substr($_, 0, 1) eq '/') || 
             (substr($_, 0, length($RelativePath)) eq $RelativePath)
            ) # convert path relative to pwd to be relative to base directory of TTC-git (where .git folder resides)
    { $_ = joinPath('*/'.$RelativePath, $_); }
    
    s/\/\.\//\//g;   # /./ -> /
    s/\/\//\//g;     # // -> /
    while (m/\/[^\/]+\/\.\.\//) {  # replace all    /foo/../ -> /
      s/\/[^\/]+\/\.\.\//\//g;     # replace single /foo/../ -> /
    }
    
    if (substr($_, 0, 2) eq '*/') { $_= substr($_, 2); } # remove marker
    $_;
  } @_;
  
  if (wantarray()) { return @CorrectedFiles; }
  return $CorrectedFiles[0];
}
sub dieOnError {        # dies if error given + prints caller stack
  my @ErrorMessages = grep { $_; } @_;
  unless (@ErrorMessages) { return; } # no error no die
  
  my $Level = 1;
  my @Stack;
  do {
     ($package, $filename, $line, $subroutine, $hasargs,
      $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
      = caller($Level++);
      if ($subroutine) { push @Stack, "$subroutine() called from ${filename}:$line"; }
  } while ($filename);
  ($package, $filename, $line, $subroutine, $hasargs,
   $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
   = caller(0);
  my $PWD = `pwd`; chomp($PWD);
  push @ErrorMessages, "PWD='$PWD'";
  push @Stack, "dieOnError() called from ${filename}:$line";
  push @ErrorMessages, @Stack;

   

  print( "$0 - ERROR in $subroutine(): ".join("\n    ", @ErrorMessages)."\n" );
  exit 10;
}
