
# CHANGE LINE BELOW ACCORDING TO YOUR SETUP!
target remote 127.0.0.1:3333  # connect to openocd runnning on local machine
#target remote eee:3333 # connect to openocd running on another machine (here eee)

# prevents single steping via next/ step: 
#monitor gdb_breakpoint_override hard

# disable interrupts during stepping makes debugging with scheduler easier
define hook-step
mon cortex_m maskisr on
end
define hookpost-step
mon cortex_m maskisr off
end

# amount of hardware breakpoints/ watchpoints cannot be set by openocd
set remote hardware-breakpoint-limit 6
set remote hardware-watchpoint-limit 6

monitor reset init
monitor soft_reset_halt
symbol-file main.elf

set debug arm
break ttc_assert_halt_origin
break main

