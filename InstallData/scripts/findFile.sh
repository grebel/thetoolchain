#!/bin/bash
#
# (c) Gregor Rebel 2010-2018

File="$1"

if [ "$File" == "" ]; then
  echo "$0 FILE_TO_FIND"
  exit 10
fi

Dir="./"
Found=`find $Dir -follow -name $File`
if [ "$Found" == "" ]; then
    echo "ERROR: Cannot find '$File' in $Dir"
    exit 5
fi

ls $Found


