#!/bin/bash

# make sure that all symbolic links in this folder are valid
./createLinks.sh

# command line options for QtCreator (workaround to allow start even if OpenGL is not fully available)
CreatorOptions="-noload Welcome" 
# These profiles should already being disabled during installation (-> install_QtCreator.sh).
# Providing them as arguments, would cause an error and qtcreator would not start:
# -noload QmlDesigner  -noload QmlProfiler

DirColorCSS="$HOME/.config/QtProject/qtcreator/styles"
ColorCSS="$DirColorCSS/TheToolChain.css"
if [ -e "$ColorCSS" ]; then #{
  echo "found color settings $ColorCSS"
  CreatorOptions="$CreatorOptions -stylesheet=$ColorCSS"
else
    echo "NOT found color settings $ColorCSS"
fi #}

source _/SourceMe.sh
source _/installFuncs.sh
Binary=`which qtcreator`

if [ "$Binary" == "" ]; then #{ Binary=`find /opt/...`
  Binary=`find /opt/ -name qtcreator 2>/dev/null | grep bin`
fi #}
if [ "$Binary" == "" ]; then #{ Binary=`find $HOME/Source/qtcreator/bin/...`
  Binary=`find $HOME/Source/qtcreator/bin/ -name qtcreator 2>/dev/null`
fi #}
if [ "$Binary" == "" ]; then #{ Cannot find executable qtcreator
  echo ""
  echo "$0 - ERROR: Cannot find executable qtcreator somewhere in $PATH"
  echo "$0 - Make sure that QtCreator is installed (download from http://qt-project.org/downloads)"
  echo ""
  exit 10  
fi #}

echo "$0 - Using QtCreator binary $Binary .."

if [ ! -e $HOME/.config/QtProject/QtCreator.ini ]; then #{ First start of QtCreator
  echo "First start of QtCreator on this machine: removing user config files."
  rm -v  QtCreator/*user*
fi #}
for ProjectFile in `find QtCreator/ -name "*.creator"`; do #{ run one qtcreator for each found project file
  ProjectName=`perl -e "my \\\$P1=index('$ProjectFile', '/')+1; my \\\$P2=index('$ProjectFile', '.')+1; print substr('$ProjectFile', \\\$P1, \\\$P2-\\\$P1-1).\\"\\\n\\";"`
  echo "found project $ProjectName"
  UserConfig="QtCreator/${ProjectName}.creator.user"
  ConfigIsOK=`grep compile.sh $UserConfig`
  if [ "$ConfigIsOK" == "" ]; then #{
    echo "Incorrect user config '$UserConfig': Spawning qtcreator to create a new one."
    rm -f $UserConfig
    echo "> $Binary $CreatorOptions \"$ProjectFile\" >QtCreator/qtcreator.log 2>&1"
    $Binary $CreatorOptions "$ProjectFile" >QtCreator/qtcreator.log 2>&1 &
    PID_QtCreator=$!
    echo ""
    echo "PLEASE CLOSE YOUR QTCREATOR WINDOW ONCE IT HAS OPENED!"
    echo ""
    echo "PID_QtCreator='$PID_QtCreator'"
    sleep 5
    
    if [ "`ps -p $PID_QtCreator | grep $PID_QtCreator`" != "" ]; then #{ QtCreator still running: try to kill it
      echo -n "waiting for QtCreator to settle.. "
      _/waitUntilIdle.pl $PID_QtCreator #>/dev/null
      echo ""
      installPackageSafe "which xdotool" xdotool
      if [ "`which xdotool`" != "" ]; then #{ use xdotool to close QtCreator (sending sigkill will not generate correct user configfile)
        WindowIDs=`xdotool search --classname qtcreator`
        WindowID_QtCreator=""
        for WindowID in $WindowIDs; do #{ close all QtCreator windows showing our project
          IsProjectQtCreator=`xdotool getwindowname $WindowID | grep $ProjectName`
          if [ "$IsProjectQtCreator" != "" ]; then
            echo "Trying to close QtCreator via xdotool (WindowID=$WindowID).."
            xdotool windowactivate $WindowID
            xdotool key alt+F4
          fi
        done #}
        _/waitUntilIdle.pl $PID_QtCreator >/dev/null
      fi #}
      #kill -17 $PID_QtCreator
      while [ "`ps -p $PID_QtCreator | grep $PID_QtCreator`" ]; do
        echo "Waiting for QtCreator to shut down..."
        sleep 1
      done
    fi #}

    while [ 1 ]; do
      sleep 1
      if [ -e "$UserConfig" ]; then
        sleep 1
        break
      else
        echo "waiting for file '$UserConfig'.."
      fi
    done
    
    if [ -e "$UserConfig" ]; then
      echo "Bingo! Got a new user config for current platform. Will modify it."
      cp $UserConfig ${UserConfig}_orig
      _/updateUserConfig.pl "$UserConfig"
    fi
    if [ ! -e "$UserConfig" ]; then
      echo "That didn't work. Seems as if you have to configure it on your own!"
    fi
  fi #}
  # echo "> $Binary $CreatorOptions \"$ProjectFile\"  >QtCreator/qtcreator.log"
  INI_FILE="$HOME/.config/QtProject/QtCreator.ini"
  ColorSchemeMissing=` grep 'ColorScheme=' $INI_FILE`
  if [ "$ColorSchemeMissing" == "" ]; then #{ setting default color scheme
    echo "setting default color scheme for QtCreator TextEditor..."
    cat <<END_OF_TEXT >>$INI_FILE

[TextEditor]
ColorScheme=$HOME/.config/QtProject/qtcreator/styles/TheToolChain.xml

END_OF_TEXT
  fi #}
  CMD="$Binary $CreatorOptions $ProjectFile $@"
  $CMD >QtCreator/qtcreator.log 2>&1 &
  EC=$?
  if [ "$EC" != "0" ]; then #{ Cannot start qtcreator
    echo "ERROR: Cannot start qtcreator (EC=$EC)!"
    echo "       > $CMD"
    echo ""
    sleep 5
    cat QtCreator/qtcreator.log
  fi #}
done #}
