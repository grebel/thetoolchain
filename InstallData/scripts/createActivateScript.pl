#!/usr/bin/perl

use strict;
use File::Spec::Functions qw(rel2abs);
use File::Basename;
my $ToolChainDir = dirname(rel2abs($0));

my $ProjectFolder = shift(@ARGV);
my $CompleteScript = { COMPLETE => 1 }->{uc( shift(@ARGV) )};

my %IsDefaultRank = ( # all ranks being added to activate script by default
  '050' => '050', 
  '060' => '060', 
  '100' => '100', 
#X  '110' => '110', 
  '150' => '150', 
  '250' => '250', 
  '300' => '300', 
  '350' => '350', 
  '490' => '490', 
  '500' => '500', 
  '510' => '510', 
  '600' => '600' 
);
my $ScriptName="activate_project.sh";
my $ToolChainVersion;
if (-e Version) { $ToolChainVersion = `cat   Version`; }
else            { $ToolChainVersion = `cat _/Version`; }

unless ($ProjectFolder) {
  print <<"END_OF_HELP";
$0 <PROJECT_FOLDER> [COMPLETE]

Creates a new activate-script in given project folder.

<PROJECT_FOLDER>  relative or absolute path to project folder where to create activate script
COMPLETE          if given, all existing activate scripts are added to activate script
                  if ommitted, only usefull ranks are added
END_OF_HELP
  exit 5;
}


my $AllActivateScripts = `ls $ENV{HOME}/Source/TheToolChain/InstallData/extensions/activate.*`;
my @AllActivateScripts = split("\n", $AllActivateScripts);
@AllActivateScripts = sort { $a cmp $b; } map { my @Parts = split("/", $_); $Parts[-1]; } @AllActivateScripts;
print "    $0 - ".(scalar @AllActivateScripts)." activate scripts found\n";

my %Ranks;
foreach my $ActivateScript (@AllActivateScripts) {
  (my $Rank, my @Rest) = split("_", $ActivateScript);
  my $DotPos = rindex($Rank, '.');
  $Rank = substr($Rank, $DotPos+1);
  unless ($Ranks{$Rank}) {
    $Ranks{$Rank} = { 
      Scripts => [ ],        # all scripts being found in $HOME/Source/TheToolChain/InstallData/extensions/
      Comment => '',         # placed at beginning of each rank-block
      DefaultScripts => []   # list of scripts being printed as non-commented
    };
    #print "new rank: '$Rank'\n"; 
  }
  push @{ $Ranks{$Rank}->{Scripts} }, $ActivateScript;
  #X $Ranks{$Rank}->{DefaultScripts}->[0] = $ActivateScript;
}

#{ set comments + defaults
my $Rank;

$Rank = '050'; $Ranks{$Rank}->{Name}="Choose C-Compiler"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT";
  #
  # Different compilers may be required for certain microcontroller architectures.

END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = ['activate.050_compiler_gcc_arm_binary.sh'];

#}050
$Rank = '060'; $Ranks{$Rank}->{Name}="Basic Compiler Settings"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  # 
  # Basic compilation settings affecting code generation (size, speed, debug-support)
  #
END_OF_COMMENT
$Ranks{$Rank}->{DefaultScripts} = ['activate.060_basic_extensions_basic_setup.sh',
                                   'activate.060_basic_extensions_optimize_1_debug.sh',
                                   'activate.060_compiler_parallel_make.sh'
                                  ];
#}060
$Rank = '100'; $Ranks{$Rank}->{Name}="Board definitions (DEPRECATED)"; #{

$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #  
  # Each board 
  # - activates a CPU
  # - sets memory sizes and locations
  # - activates required standard peripheral library (STM-types only)
  #
  # Note: Exactly one board must be activated for your project!
  #       Be sure to disable all "enableFeature 110_*" lines in rank 000!

#InsertProjectSpecificBoards above

END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}100
$Rank = '110'; $Ranks{$Rank}->{Name}="ttc_board drivers (mandatory)"; #{

$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  # 
  # Board extensions in rank 110 are replacing all board extensions in rank 100.
  # 
  # Each board 
  # - activates a CPU
  # - sets memory sizes and locations
  # - activates required standard peripheral library (STM-types only)
  # - has its own low-level driver in ttc-lib/board/
  # - can implement its own board_*_prepare() function to prepare onboard hardware during startup
  #
  # Note: Exactly one board must be enabled for your project!
  #       The corresponding low-level driver is then activated by running
  #       activate.490_ttc_board.sh

#InsertProjectSpecificBoards above

END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}100
$Rank = '150'; $Ranks{$Rank}->{Name}="Board extensions - Addon modules (optional)"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # Board extensions are PCBs with extra devices like sensors, actors or interfaces that
  # can be connected to the main board being activated in rank 100.
  #
  # Any amount of board extensions may be activated at once.
  # The user is responsible to avoid double definitions and interferences.

END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}100
$Rank = '170'; $Ranks{$Rank}->{Name}="Low Level drivers for onboard chips."; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT";
  # These drivers get activated from ranks 100 and 150. 
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}
$Rank = '190'; $Ranks{$Rank}->{Name}="Generic CPU Family support"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # These libraries provide low-level drivers for features that are common to a family of microcontrollers.
  #
  # Warning: Do not include or call any of these source files from your application unless you exactly know what you are doing!
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}
$Rank = '200'; $Ranks{$Rank}->{Name}="Individual CPU support"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # The CPU should already been activated by the board makefile.
  # You may want to activate a CPU in case you don't yet have a board makefile 
  #
  # Warning: Do not include or call any of these source files from your application unless you exactly know what you are doing!
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}
$Rank = '250'; $Ranks{$Rank}->{Name}="STM Std Peripherals Library"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  # The Standard Peripherals Library has been developed by ST Microelectronics
  # as a support library for their STM32 family pf microcontrollers.
  #
  # The activates provided here will add individual drivers to your project.
  # These drivers are poorly documented and difficult to understand as their
  # usage cannot be derived from the mircocontroller datasheets.
  # These drivers are available for compatibility reasons (e.g. if you want to use a library that requires these drivers).
  #
  # If you want to use a feature in your STM32 that is not covered by TTC drivers, it is highly recommended that you:
  # 1) Read the uC-datasheet (e.g. RM0008 for STM32F1xx or RM0038 for STM32L1xx).
  # 2) Enable line activate.500_ttc_register.sh.
  # 3) Use direct, named register access via register_* variables (see ttc-lib/register/ for details).
  #
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}250
$Rank = '251'; $Ranks{$Rank}->{Name}="Support Libraries for stm32w/stm32w108 architecture"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  # STM32Wxxx architecture (STM32W108) has been developed by Ember Corp. as EM351/EM357. 
  # These chips are not compatible with Standard Peripheral Library.
  # The HAL library can be used instead. 
  # The, binary only, SimpleMAC library uses its own HAL.
  #
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}270
$Rank = '253'; $Ranks{$Rank}->{Name}="Support Libraries for radio transceiver embedded into stm32w/stm32w108 architecture"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # STM32Wxxx architecture (STM32W108) has been developed by Ember Corp. as EM351/EM357. 
  # These chips are not compatible with Standard Peripheral Library.
  # The, binary only, SimpleMAC library provides an interface to make use of the radio transceiver.
  # This library is required as no register descriptions are publicly available for these microcontrollers.
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}253
$Rank = '255'; $Ranks{$Rank}->{Name}="Standard Peripheral Library for stm32w/stm32w108 architecture"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # STM32Wxxx architecture (STM32W108) has been developed by Ember Corp. as EM351/EM357. 
  # These chips are not compatible with STM32Fxxx!
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}253
$Rank = '270'; $Ranks{$Rank}->{Name}="Communication Peripheral Application Library"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # CPAL has been designed on top of std peripherals library.
  # CPAL is suitable for STM32Fxxxx microcontrollers only.
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}270
$Rank = '300'; $Ranks{$Rank}->{Name}="Multitasking scheduler"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # A multitasking scheduler provides basic multithreading features.
  # The activation of one scheduler and a heap type is mandatory for some examples.
  # 
  # It is possible to use several examples without a scheduler.
  # ttc_task will behave different when no scheduler is active:
  # 1) ttc_task_create() will act as a function call to given task function.
  # 2) ttc_task_msleep(), ttc_task_usleep() act as software tuned busy loops. Their delay time may get inaccurate.
  # 3) ttc_task_critical_begin() will return immediately
  # 
  # Check ttc_task.h for a more detailed description of high-level multitasking support.
  #
END_OF_COMMENT
push @{ $Ranks{$Rank}->{DefaultScripts} }, 'activate.300_scheduler_freertos.sh',
                                           'activate.300_scheduler_freertos_heap1.sh';
#}
$Rank = '350'; $Ranks{$Rank}->{Name}="Device programmer/ debugger interfaces"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # A programmer interface allows to program a compiled binary file onto your microcontroller.
  # A debugger interface allows to step through your code on the microcontroller and to inspect memory contents interactively.
  # JTAG is an industry standard for combined programmer/debugger interfaces. Many JTAG interfaces can be controlled
  # with the open source software OpenOCD.
  # SWD is a newer programmer/debugger interface standard introduced by ARM corp. It uses lower pincount and is faster.
  #
END_OF_COMMENT
#push @{ $Ranks{$Rank}->{DefaultScripts} }, 'activate.350_programmer_openocd_olimex_arm_usb_ocd_h.sh';
push @{ $Ranks{$Rank}->{DefaultScripts} }, 'activate.350_programmer_openocd_stlink_v2.sh';

#}
$Rank = '400'; $Ranks{$Rank}->{Name}="External Support Libraries"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}
$Rank = '450'; $Ranks{$Rank}->{Name}="Low-level drivers provided by TheToolChain"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # These libraries provide low-level drivers for all types of functional units for individual architectures.
  # Warning: Do not include or call any of these source files from your application unless you exactly know what you are doing!
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}
$Rank = '490'; $Ranks{$Rank}->{Name}="Early High-level drivers provided by TheToolChain"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # These libraries provide architecture independent high-level drivers for all types of functional units.
  # The extensions listed here require to be activated BEFORE all extensions in rank 500.
  # You may safely include and call any of these source files from your application.
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
push @{ $Ranks{$Rank}->{DefaultScripts} }, 'activate.490_ttc_board.sh';
#}
$Rank = '500'; $Ranks{$Rank}->{Name}="Main High-level drivers provided by TheToolChain"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # These libraries provide architecture independent high-level drivers for all types of functional units.
  # You may safely include and call any of these source files from your application.
  #
  # A high-level activate script will run all of its low-level activate scripts (typically in rank 450).
  # Each low-level activate script will check if prerequisites are fullfilled (e.g. a extensions.active/feature.* file exists).
  # 
  #
END_OF_COMMENT

push @{ $Ranks{$Rank}->{DefaultScripts} }, 'activate.500_ttc_basic.sh';
push @{ $Ranks{$Rank}->{DefaultScripts} }, 'activate.500_ttc_cpu.sh';
push @{ $Ranks{$Rank}->{DefaultScripts} }, 'activate.500_ttc_heap.sh';
push @{ $Ranks{$Rank}->{DefaultScripts} }, 'activate.500_ttc_sysclock.sh';
push @{ $Ranks{$Rank}->{DefaultScripts} }, 'activate.500_ttc_task.sh';
push @{ $Ranks{$Rank}->{DefaultScripts} }, 'activate.500_ttc_register.sh';
#}
$Rank = '510'; $Ranks{$Rank}->{Name}="Late High-level drivers provided by TheToolChain"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # These libraries provide architecture independent high-level drivers for all types of functional units.
  # The extensions listed here require to be activated AFTER all extensions in rank 500.
  # You may safely include and call any of these source files from your application.
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}
$Rank = '600'; $Ranks{$Rank}->{Name}="Examples"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # Examples provide a ready to run set of extensions and an example source code.
  # Every ttc_* extension will provide its own example. These are meant as additional documentation.
  # Each example belongs to the current project and may be changed and used for experiments.
  # You can also use an example as starting point to develop your project.
  #
  # Some examples can run in parallel.
  # Some examples require a scheduler (-> rank 300)
  # Some examples are restricted to certain boards. 
  #     A board provides defines like TTC_LED1, TTC_LED2 other interfaces and CPU.
  #     If a board does not define TTC_LED1 (like 100_board_olimex_lcd) then examples requiring leds will throw a compile error.
  #     In such cases, you may create the missing defines in extensions.local/makefile.700_extra_settings.
  #
  # 600_example_leds is the most simple example and should work on any board that defines TTC_LED1.
  # If 600_example_leds throws an error because TTC_LED1 is not defined, simply add this line to your
  # extensions.local/makefile.700_extra_settings file:
  #     COMPILE_OPTS += -DTTC_LED1=E_ttc_gpio_pin_a10  # modify a10 to any pin you like
  #
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = ['activate.600_example_leds.sh'];
#}
$Rank = '650'; $Ranks{$Rank}->{Name}="Regression Tests"; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 
  #
  # These tests provide test environments to check certain features of corresponding extensions.
  # Most tests only check if a certain extension can be compiled without errors.
  #
  # Intended use of regressions:
  # 1) deactivate all examples
  # 2) activate exactly one regression
  # 3) clean + compile project
  # 4) check for compilation errors
  # 5) restart at 1) for a different regression
  #
  # Note: Regressions are currently still in draft state. None is implemented or tested at all.
  #
END_OF_COMMENT

$Ranks{$Rank}->{DefaultScripts} = [];
#}
$Rank = ''; #{
$Ranks{$Rank}->{Comment} = <<"END_OF_COMMENT"; 

END_OF_COMMENT
#}

#}
unless ($CompleteScript) { # remove all non-default ranks for simplicity
  %Ranks = map { $_ => $Ranks{$_}; }
           grep { $IsDefaultRank{"$_"}; }
           keys %Ranks;
}

my @Activates;
print "    $0 - compiling list of all available activates\n";
foreach my $Rank (sort { $a cmp $b} keys %Ranks) { # compile list of all available activates
  if ($Rank) {
    my $RankRef = $Ranks{$Rank};
    my $Activate = $RankRef->{Comment};
    my %DefaultScripts;
    foreach my $DefaultScript (@{ $RankRef->{DefaultScripts} }) {
      $DefaultScripts{$DefaultScript} = 1;
    }

    foreach my $ActivateScript (@{ $RankRef->{Scripts} }) {
      if (1) { my $Text = `$ActivateScript INFO`;
        my @Text = split("\n", $Text);
        @Text = grep { $_ } @Text; # remove empty lines
        if (@Text) { $Activate .= "\n  # ".join("\n  # ", @Text)."\n"; }
      }
      if (1) { my $Text = `$ActivateScript PROVIDES`;
        my @Text = split("\n", $Text);
        @Text = grep { $_ } @Text; # remove empty lines
        if (@Text) { $Activate .= "  # provides: ".join("\n  # ", @Text)."\n"; }
      }

      my $Suffix = "QUIET \"\$0\" || Error '$ActivateScript'";
      if ($DefaultScripts{$ActivateScript}) { $Activate .= "  $ActivateScript $Suffix\n"; delete $DefaultScripts{$ActivateScript}; }
      else                                  { $Activate .= "  #$ActivateScript $Suffix\n"; }
    }
    $Activate .= "\n";
    foreach my $DefaultScript (@{ $RankRef->{DefaultScripts} }) { # add remaining default scripts
      if ($DefaultScripts{$DefaultScript}) { # script not yet added: add it
        $Activate .= "  $DefaultScript QUIET \"\$0\" || Error '$DefaultScript'\n";
      }
    }

    push @Activates, 
         "if [ true ]; then #{ $Rank - ".$Ranks{$Rank}->{Name}."\n".
         "  Rank='$Rank'\n". # avoids syntax error in case no command is executed between if then ... fi
         $Activate.
         "\nfi #}$Rank";
  }
}
my $Activates = join("\n", @Activates);

my $ReadMeRanks = readFile("$ENV{HOME}/Source/TheToolChain/InstallData/ReadMe_Ranks.txt");
my @ReadMeRanks = split("\n", $ReadMeRanks);
$ReadMeRanks = join("\n", map { '# '.strip($_); } @ReadMeRanks);

print "    $0 - compiling content of activate-script\n";
my $Script = <<"END_OF_SCRIPT"; #{ compile content of activate-script
#!/bin/bash

if [ true ]; then #{ Activation Sequence for TheToolChain revision $ToolChainVersion
echo "$ScriptName revision 11"
#
# The ToolChain consists of several extensions with can be individually activated for current project.
# \$0 must be run from inside a project folder.
#
# This script activates extensions in two stages:
# 1) Enable Features
#    A feature is an empty file named extensions.local/feature.*
#    Features are enabled by calling enableFeature shell function wich will pass the call to
#    _/enableFeature.pl. This perl script may automatically enable additional, depending features.
#    Enabling features will not add code to your project. This is done in the second stage.
#
# 2) Run Activate Scripts
#    Every extension can create one or more extensions/activate*.sh scripts.
#    Each activate script is a shell script that
#    a) Checks if it should be activated by looking for a corresponding feature.
#    b) Creates symbolic links
#       extensions.active/makefile.*   - constants, source files and paths for compilation
#       extensions.active/source.*     - code to add to autostart sequence
#    c) Can create files to provide command line tools (e.g. different compiler) or libraries.
#    d) Can call other activate scripts if required.
#
#
$ReadMeRanks
#
# Minimum setup
# - 000 basic setup
# - 050 exactly one compiler
# - 060 optional: compiler settings
# - 110 exactly one board (be sure to set the correct!)
# - 150 optional: one or more extension boards
# - 350 correct programmer device
# - 600 optional: one or more examples (some will interfere with each other)
#
fi #}
if [ true ]; then #{  prepare activation process
if [ "\`echo \$PATH | grep extensions.local\`" == "" ]; then
   echo "\$0 - ERROR: Incomplete PATH variable. You may"
   echo "    - Start a new shell (also known as konsole or terminal)"
   echo "    - source _/SourceMe.sh"
   echo "    and retry!"
   exit 12
fi

source _/installFuncs.sh

function age() {
   local filename=\$1
   local changed=`stat -c \%Y "\$filename"`
   local now=`date +\%s`
   local elapsed

   let elapsed=now-changed
   echo \$elapsed
}
function Error() {
  local Stage=\$1

  echo "\$0 - ERROR: project could not be fully activated at stage '\$Stage'!"
  exit 11
}

MakeFile="extensions.active/makefile"
if [ -e \$MakeFile ]; then
  AgeMakefile=\$(age \$MakeFile)
  AgeActivateProject=\$(age \$0)

  if [ \$AgeActivateProject -lt \$AgeMakefile ]; then
    echo "\$0 - project settings changed: cleaning extensions"
    ./clean.sh
  fi
fi


ExtensionsActive=`ls 2>/dev/null \${MakeFile}.*`
if [ "\$ExtensionsActive" != ""  ]; then
  echo "\$0 - extension setup found: not changing extensions.active/"
  exit 0
fi

echo "\$0 - no extensions active: activating configured extensions"
fi #}
if [ true ]; then #{ 000 - Provide Features to enable Low-Level Drivers
  echo "\$0 - providing features.."
  #
  # Several ttc drivers provide several low-level drivers.
  # During activation, the high level activate script will run all low-level drivers.
  # Features are a construct that help to activate the right low-level driver.
  # A feature file is an empty file named extensions.active/feature.*
  # To create a feature file, simply call the enableFeature function inside this script
  # or call _/enableFeature.pl from a shell inside a project folder. This will also
  # enable all features depending on given one.
  # Run _/enableFeature.pl to learn more about feature dependencies!
  #
  # Which Features should be enabled?
  # First you have to identify which features are required by an extension.
  # When activating a ttc_foo extension, look for NNN_foo_* extensions (NNN being a rank number).
  #
  # E.g. let's say you want to activate ttc_math.
  #   In the list below, you find two matching features. Let's activate 450_math_software_double:
  #   # enableFeature 450_math_software_double
  #   enableFeature 450_math_software_float
  #
  #   We also activate the high-level extension itself:
  #   activate.500_ttc_math.sh
  #
  #   When run, activate.500_ttc_math.sh will automatically run its low-level activate scripts:
  #     activate.450_math_software_double.sh -> will not activate (missing dependency) 
  #     activate.450_math_software_float.sh  -> activates as extensions.active/feature.450_math_software_float exists
  #
  
  # Note: Exactly one 110_board_* line must be active!
  enableFeature 110_board_stm32l100c_discovery

#ProvideFeatures
fi #}000
$Activates
if [ true ]; then #{ 700 - extensions.local/makefile.700_extra_settings

  # this makefile belongs to current project and is added automatically
  cd extensions.active/

  # activate local makefile
  createLink ../extensions.local/makefile.700_extra_settings .

  cd ..
fi #}700

END_OF_SCRIPT
#}

if (-e $ScriptName) { system("mv $ScriptName $ScriptName.old"); }
open(OUT, ">$ScriptName") or die("$0 - ERROR: Cannot create activate-script `pwd`/$ScriptName ($!)");
print OUT $Script;
close(OUT) or die("$0 - ERROR: Cannot write to activate-script `pwd`/$ScriptName ($!)");
system("chmod +x \"$ScriptName\"");

sub readFile {
  my $FileName = shift;
  my @Lines;

  open(IN, "<$FileName") or die("Cannot read file '$FileName' ($!)");
  @Lines = <IN>;  
  close(IN);
  
  return join('', @Lines);
}
sub strip {              # removes invisible characters at begin + end of given string
  my $Line = shift || "";

  my %IsWhiteChar = ( ' ' => 1, "\n" => 1, "\r" => 1, "\t" => 1 ); 

  $Line =~ s/^\s+//;
  $Line =~ s/\s+$//;
  #$Line =~ s/(?:^ +)||(?: +$)//g;
  #$Line =~ s/^\ *([A-Z,a-z,0-9]*)\ *$/\1/g;
  #$Line =~ s/^\s+//;
  #$Line  =~ s/\s+$//;
  return $Line;
  
  my $lIndex = 0;
  while ( $IsWhiteChar{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsWhiteChar{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  #X print " substr(\$Line, $lIndex, $rIndex - $lIndex + 1);\n"; #D
  return substr($Line, $lIndex, $rIndex - $lIndex + 1);

}
