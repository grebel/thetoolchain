#!/bin/bash

# Set of helpfull functions for install-scripts
#
# written 2003 - 2018 by Gregor Rebel (gregor HT thetoolchain DT com)
# Feel free do distribute and change to your own needs!
#

# ensure that $PATH contains no . (brings find into trouble)
export PATH=`perl -e "my @A=split(':', '$PATH');  print join(':', grep { substr(\\\$_,0,1) eq '/' } @A );"`
iF_StartDir=`pwd`

#{ InstallFuncs v1.11 - written by Gregor Rebel---------------------------------------

function add2CleanUp() {             # File, Line                            adds given line to cleanup file (if not already done)
    CleanUpFile="$1"
    Line="$2"
    
    if [ ! -e "$CleanUpFile" ]; then
      echo "#!/bin/bash"  >$CleanUpFile
      echo ""            >>$CleanUpFile
      chmod +x $CleanUpFile
    fi
    addLine "$CleanUpFile" "$Line"
}
function add2Script() {              # Script,Command,Execute                adds Command to Script; executes Command before if Execute==1  
  ScriptFile=$1
  Command=$2
  Execute=$3
  
  grep "$Command" $ScriptFile >tmp.grep
  AlreadyAdded=`cat tmp.grep`
  if [ "$AlreadyAdded" == "" ]; then
    Error=""
    if [ "$Execute" == "1" ]; then
      $Command || Error="1"
    fi
    if [ "$Error" == "" ]; then
      echo "$ScriptFile: $Command"
      echo "$Command" >>$ScriptFile
    fi
  fi
}
function addLine() {                 # File,Line                             adds given line to File if not already done
  al_File="$1"
  al_Line="$2"
  
  if [ ! -e "$al_File" ]; then #{
    touch "$al_File" || Error="addLine($al_File, $al_Lin)"
    if [ "$Error" != "" ]; then
      echo $Error
      printCallerStack "    "
    fi
  fi #}
  if [ ! -w "$al_File" ]; then #{
    chmod +w "$al_File"
  fi #}
  if [ ! -w "$al_File" ]; then #{
    sudoCMD "chmod +w \"$al_File\""
  fi #}\
  if [ "$al_Line" == "" ]; then #{
    echo "addLine($al_File, $al_Line, $al_Keyword) - ERROR: Missing argument Line."
    printCallerStack "    "
    exit 5
  fi #}
  
  AlreadyPresent=`grep "$al_Line" "$al_File"`
  if [ "$AlreadyPresent" == "" ]; then
    echo "$al_Line" >>"$al_File"
#  else
#    echo "file already up2date: $File2Add2"
  fi
}
function addLineBefore() {           # File,Line,Keyword                     adds given line to File if not already done directly before given keyword
  al_File="$1"
  al_Line="$2"
  al_Keyword="$3"
  
  if [ ! -e "$al_File" ]; then #{
    touch "$al_File" || Error="addLine($al_File, $al_Line, $al_Keyword)"
    if [ "$Error" != "" ]; then
      echo $Error
      printCallerStack "    "
    fi
  fi #}
  if [ ! -w "$al_File" ]; then #{
    chmod +w "$al_File"
  fi #}
  if [ ! -w "$al_File" ]; then #{
    echo "chmod +w '$al_File'"
    sudoCMD "chmod +w \"$al_File\""
  fi #}
  if [ "$al_Line" == "" ]; then #{
    echo "addLine($al_File, $al_Line, $al_Keyword) - ERROR: Missing argument Line."
    printCallerStack "    "
    exit 5
  fi #}
  if [ "$al_Keyword" == "" ]; then #{
    echo "addLine($al_File, $al_Line, $al_Keyword) - ERROR: Missing argument Keyword."
    printCallerStack "    "
    exit 5
  fi #}
  
  AlreadyPresent=`grep "$al_Line" "$al_File"`
  if [ "$AlreadyPresent" == "" ]; then
    LineNumbers=`wc -l $al_File | cut -d' ' -f1`
    grep --before-context $LineNumbers "$al_Keyword" $al_File | head -n-1  >addLineBefore.PartA 
    grep --after-context  $LineNumbers "$al_Keyword" $al_File              >addLineBefore.PartB
    mv  $al_File ${al_File}.old
    cp addLineBefore.PartA    $al_File
    echo $al_Line           >>$al_File
    cat addLineBefore.PartB >>$al_File
    #rm addLineBefore.PartB
#  else
#    echo "file already up2date: $File2Add2"
  fi
}
function addLineIf() {               # CheckCMD, File, Line                  if (`$CheckCMD`) then addLine(File, Line)
  ali_CheckCMD="$1"

  if [ "`$ali_CheckCMD`" ]; then
    addLine "$2" "$3"      
  fi  
}
function addLine2Header() {          # File,Line                             adds given line as first line to File if not already done
  al_File="$1"
  al_Line="$2"
  
  if [ ! -e "$al_File" ]; then #{
    touch "$al_File"
  fi #}
  if [ ! -w "$al_File" ]; then #{
    chmod +w "$al_File"
  fi #}
  if [ ! -w "$al_File" ]; then #{
    echo "chmod +w '$al_File'"
    sudoCMD chmod +w "$al_File"
  fi #}

  AlreadyPresent=`grep "$al_Line" "$al_File"`
  if [ "$AlreadyPresent" == "" ]; then
    echo "adding to ${al_File}: '$al_Line'"
    echo "$al_Line" >"${al_File}_new"
    cat "$al_File" >>"${al_File}_new"
    mv "${al_File}_new" "$al_File"
  fi
}
function applyTargetUserAndGroup() { # Directoy[,NewUserAndGroup]       chowns given directory to $TargetUserAndGroup
  aTUAG_DirPath="$1"
  aTUAG_UserAndGroup="$2" # "user:group"
  
  if [ "$aTUAG_UserAndGroup" != "" ]; then
    TargetUserAndGroup=$aTUAG_UserAndGroup 
  fi
  if [ "$TargetUserAndGroup" != "" ]; then #{ chown install directory
    # TargetUserAndGroup is set inside installFuncs.sh
    if [ -d "$aTUAG_DirPath" ]; then
      if [ "$USER" == "root" ]; then
        chown -R $TargetUserAndGroup "$aTUAG_DirPath"
      fi
    fi
  fi #}

}
function cmd() {                     # CMD[,STDOUT]                          display + execute given command; records log + errors
  CMD="$1"
  StdOut="$2"
  Quiet="$3"
  
  echo -n "" >stdout.log
  echo -n "" >stderr.log
  
  if [ "$StdOut" != "" ]; then
    echo "`pwd`> $CMD >>$StdOut (line `caller`)"
    if [ "$Quiet" != "QUIET" ]; then
      #X (Not compatible with scripts/compile.sh as it hinders QtCreator in parsing error messages)  $CMD 2>&1 1>>$StdOut | tee $Stdout 2>stderr.log
      $CMD
    else
      $CMD 2>&1 1>>$StdOut | tee $Stdout 2>stderr.log 1>stdout.log
    fi
  else
    echo "`pwd`> $CMD (line `caller`)"
    if [ "$Quiet" != "QUIET" ]; then
      $CMD 2>stderr.log
    else
      $CMD 1>stdout.log 2>stderr.log
    fi
  fi
  EC="$?"
  CMD_STDOUT=`cat stdout.log`
  if [ "$EC" != "0" ]; then
    CMD_ERROR=`cat stderr.log`
    if [ "$Quiet" != "QUIET" ]; then
      echo "$CMD_ERROR"
      printCallerStack "    "
    fi
  else
    CMD_ERROR=""
    if [ "$Quiet" != "QUIET" ]; then
      cat stderr.log >&2
    fi
  fi
}
function confirmInstalled() {        # CheckCMD,Name,FailCMD   executes given check and creates OK-flag if check returns any string
  cI_CheckCMD="$1"   # command to execute (check is successfull if command returns any string)
  cI_Name="$2"       # name of application to check
  cI_FailCMD="$3"    # optional: command to execute when check has not returned anything
  
  # Example Usage:
  # confirmInstalled "which postfix" postfix "exit 10"
  
  ci_Result="`$cI_CheckCMD`"
  if [ "$ci_Result" != "" ]; then
    echo "Successfully installed: $cI_Name"
    touch OK.${cI_Name}
  else
    echo "$0 - ERROR: Cannot install $cI_Name!"
    if [ "$cI_FailCMD" != "" ]; then
      $cI_FailCMD
    fi
  fi  
}
function countCPUs() {               #                                       $AmountCPUs = amount of installed CPU-cores
  AmountCPUs=`cat /proc/cpuinfo | grep "processor" | wc -l`
}
function createLink() {              # Source,Link,User,QUIET                creates symbolic link (removes old one before)
  cl_Source="$1"
  cl_Link="$2"
  cl_User="$3"
  cl_Quiet="$4"
  
  #X echo "`pwd` > createLink('$1', '$2', '$3', '$4')"
  
  cl_Quiet="1"
  if [ "$cl_Quiet" = "" ]; then
    Verbose="-v"
    echo -n "`pwd`> "
  else
    Verbose=""
  fi
  LastCharacter=`perl -e "print substr('$cl_Source',-1);"`
  if [ "$LastCharacter" == "/" ]; then #{ remove trailing slash to avoid creating strange links containing //)
    cl_Source=`perl -e "print substr('$cl_Source',0,-1);"`
  fi #}
  if [ ! -e "$cl_Source" ]; then
    MissingFiles="$MissingFiles $cl_Source"
  fi
  
  Dir=`dirname $cl_Link`
  if [ ! -d $Dir ]; then
    dir $Dir
  fi
  if [ -f "$cl_Link" ]; then
    rm -f "$cl_Link"
  fi
  cl_ERROR=""
  if [ "$cl_User" != "" ]; then
    sudoCMD -u$cl_User ln $Verbose -sf "$cl_Source" "$cl_Link" || cl_ERROR="($!)"
  else
    ln $Verbose -sf "$cl_Source" "$cl_Link" || cl_ERROR="($!)"
  fi
  if [ "$cl_ERROR" != "" ]; then
    echo "$0 / createLink() - ERROR: Cannot create symbolic link! Will try to copy instead.."
    printCallerStack "    "
    cl_ERROR=""
    if [ "$cl_User" != "" ]; then
      sudoCMD -u$cl_User cp $Verbose -R "$cl_Source" "$cl_Link" || cl_ERROR="($!)"
    else
      cp $Verbose -R "$cl_Source" "$cl_Link" || cl_ERROR="($!)"
    fi
    if [ "$cl_ERROR" != "" ]; then
      echo "$0 / createLink() - ERROR: Cannot copy '$cl_Source' -> '$cl_Link'!"
      printCallerStack "    "
    fi
  fi
}
function dir() {                     # Directory                             creates directory if missing (can automatically create multi-level directories)
  Directory="$1"
  
  Dir_Parts=`perl -e "my @A = split('/','$Directory'); unless (\\$A[0]) { \\$A[1]='/'.\\$A[1]; } print join(' ',@A).\"\\n\";"`
  Dir_Path=""

  d_PWD=`pwd`
  #echo "$d_PWD/ >dir '$Directory'"

  for Dir_Part in $Dir_Parts; do #{ create directory path from top to bottom
    if [ "$Dir_Path" == "" ]; then
      Dir_Path="$Dir_Part"
    else
      Dir_Path="$Dir_Path/$Dir_Part"
    fi
    if [ ! -d $Dir_Path ]; then
      mkdir -v "$Dir_Path"
      applyTargetUserAndGroup $Dir_Path
    fi
  done #}
  if [ ! -d "$Directory" ]; then
    echo "dir($Directory) - Cannot create directory from `pwd`!"
    printCallerStack "    "
  fi

}
function extractNameSuffix() {       # FILENAME.SUFFIX                       extracts SUFFIX (substring after last dot) and exports it as $Suffix
   String="$1"
   
   export Suffix=`perl -e "my \\$P = rindex('$String', '.'); print substr('$String', \\$P+1);"`
}
function extractNamePrefix() {       # PREFIX.SUFFIX                         extracts PREFIX (substring before last dot) and exports it as $Prefix
   String="$1"
   
   export Prefix=`perl -e "my \\$P = rindex('$String', '.'); print substr('$String', 0, \\$P+1);"`
}
function findNewest() {              # URL_Prefix,Wildcard                   $NewestFile = matching file with highest version number on given website; $Error_findNewest=ErrorMessage
  fnURL_Prefix="$1"  # website that lists file downloads on an index page
  fnWildCard="$2"    # regular expression identifying interesting files
  
  # Examples
  # findNewest http://arduino.cc/en/main/software/ ".*32.bit.*arduino-1.*"
  # get $NewestURL= $NewestFile= arduino-linux32.tgz
  
  
  # Note: make sure that findNewestDownload.pl is in search path!
  findNewest="`which findNewestDownload.pl`"
  if [ "$findNewest" == "" ]; then
    findNewest="$HOME/Source/TheToolChain/InstallData/scripts/findNewestDownload.pl"
    if [ ! -e "$findNewest" ]; then
      findFolderUpwards scripts
      if [ "${FoundFolder}" == "" ]; then
        get http://thetoolchain.com/mirror/ findNewestDownload.pl findNewestDownload.pl
        chmod 755 findNewestDownload.pl
        sudoCMD chown root: findNewestDownload.pl
        sudoCMD mv -v findNewestDownload.pl /usr/bin/
        findNewest="`which findNewestDownload.pl`"
      else
        findNewest="${FoundFolder}/findNewestDownload.pl"
      fi
    fi
  fi
  if [ ! -e "$findNewest" ]; then
    echo "getNewestFile($OutFile) - ERROR: Cannot locate findNewestDownload.pl in `pwd`"
    printCallerStack "    "
    exit 10
  fi
  CMD="$findNewest \"$fnURL_Prefix\" \"$fnWildCard\""
  $findNewest >/dev/null "$fnURL_Prefix" "$fnWildCard" tmp.GetNewest_URL tmp.GetNewest_File tmp.GetNewest_Dir
  
  if [ -e tmp.GetNewest_URL ]; then
    export NewestURL=`cat tmp.GetNewest_URL`
  else
    export NewestURL=""
  fi
  if [ -e tmp.GetNewest_File ]; then
    export NewestFile=`cat tmp.GetNewest_File`
  else
    export NewestFile=""
  fi
  if [ -e tmp.GetNewest_Dir ]; then
    export NewestDir=`cat tmp.GetNewest_Dir`
  else
    export NewestDir=""
  fi
  if [ "$NewestFile$NewestDir" == "" ]; then
    cat >&2 <<END_OF_ERROR

findNewest('$fnURL_Prefix', '$fnWildCard') - ERROR: No newest File or Directory found! (Dir='$NewestDir' URL='$NewestURL')
COMMAND> $CMD
Called from line `caller`

END_OF_ERROR
    export Error_findNewest="$Error_findNewest findNewest($fnURL_Prefix / $fnWildCard)"
  else
    echo "findNewest(): \$NewestFile='$NewestFile' \$NewestDir='$NewestDir' \$NewestURL='$NewestURL'"
  fi
}
function findFolderUpwards() {       # FolderName                            goes upwards in filesystem until given foldername is found; returns $FoundFolder
  FoundFolder="$1"
  SingleFolder="$1"
  if [ "$2" == "QUIET" ]; then
    Quiet="1"
  fi
  
  
  Quiet="1" # // ==1: no debug output
  if [ "$Quiet" != "1" ]; then  
    echo "findFolderUpwards($1):"
    printCallerStack "    "
  fi
  
  fFU_StartDir=`pwd`
  for Level in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20; do
    [ "$Quiet" == "1" ] || echo "findFolderUpwards($1): passing `pwd` ..."
    if [ "`pwd`" != "/" ]; then
      if [ -d "$SingleFolder" ]; then
        cd "$fFU_StartDir"
        [ "$Quiet" == "1" ] || echo "findFolderUpwards($1): found $FoundFolder"
        FoundFolder="$FoundFolder/"
        return # folder found
      fi
      FoundFolder="../$FoundFolder"
      cd ..
    fi
  done
  #if [ "$Quiet" != "1" ]; then
    echo "$0 - findFolderUpwards($SingleFolder) ERROR: Cannot find folder anywhere above '`pwd`'!"
    printCallerStack "    "
    exit 10
  #fi
  FoundFolder=""
  cd "$fFU_StartDir"
} #
function findPrefix() {              #                                       loads variable Prefix either with /usr or /usr/local according to system
  if [ -e "/usr/include/math.h" ]; then
    Prefix="/usr"
  else
    Prefix="/usr/local"
  fi
}
function get() {                     # URL_Prefix,File,OutFile[,DestDir]     retrieves file from URL if not already done
  URL_Prefix="$1"
  gFile="$2"
  gOutFile="$3"        # Suffix .html is handled special (will download complete webpage and convert it to work offline)
  gDownloadDir="$4"
  
  gStartDir=`pwd`
  
  if [ "$gOutFile" == "" ]; then
    gOutFile=$gFile
  fi
  gOutFileSuffix=`perl -e "print substr('$gOutFile', -5);"`
  
  if [ "$gDownloadDir" != "" ]; then
    dir "$gDownloadDir"
    gOutFile="$gDownloadDir$gOutFile"
  fi

  if [ ! -e "$gOutFile" ]; then
    CMD_ERROR=""
    rm -f "${gOutFile}.tmp"
    if [ "$WGET_QUIET" != "" ]; then
      WGET_QUIET="--no-verbose"
    fi
    if [ "$gOutFileSuffix" == ".html" ] || [ "$gOutFileSuffix" == ".htm" ]; then # download complete website + convert it to work offline
      echo "downloading complete html page to work offline.."
      gPWD=`pwd`
      dir "${gOutFile}"
      cd "${gOutFile}"
      cmd "wget --progress=bar:force:noscroll $WGET_QUIET --timeout=60 --mirror --convert-links --adjust-extension --page-requisites --no-parent --no-directories $URL_Prefix$gFile"
      if [ -e $gFile ]; then #{ check if it's only single file
        rm stderr.log stdout.log
        MultipleFiles=`ls | grep -v "$gFile"`
        if [ "$MultipleFiles" == "" ]; then #{ only downloaded html file: move it updwards and delete empty directory
          echo "downloaded single html file '$gFile'"
          mv "../$gFile" "../${gFile}.delete"
          mv "$gFile" ../
          cd ../
          rmdir "${gFile}.delete"
        fi #}
      fi #}
      cd "$gPWD"
    else
      cmd "wget -nc --progress=bar:force:noscroll $WGET_QUIET --timeout=60 -O ${gOutFile}.tmp $URL_Prefix$gFile"
    fi
    if [ "$CMD_ERROR" == "" ]; then
      mv "${gOutFile}.tmp" "${gOutFile}"
    else
      echo "get($URL_Prefix$gFile) - ERROR: '$CMD_ERROR'"
      printCallerStack "    "
    fi
#  else
#    echo "`pwd`> get($URL_Prefix$gFile) - file already exists: '$gOutFile"
  fi
  
  getFileSize "$gOutFile"
  if [ "$FileSize" == "0" ]; then
    echo "get($URL_Prefix$gFile) - ERROR: ignoring received empty file"
    rm "$gOutFile"
  fi

  if [ ! -e "$gOutFile" ]; then
    echo "get() - ERROR: failed to download '`pwd`/$gOutFile"
    Get_Fails="$Get_Fails `pwd`/$gFile"
  fi
  
  cd "$gStartDir"
}
function getFileSize() {             # FilePath                              $FileSize = size of local file found under $FilePath
  gFS_FilePath="$1"
  
  if [ ! -e "$gFS_FilePath" ]; then
    FileSize=""
  else
    FileSize=`ls -al "$gFS_FilePath" | awk '{ print $5 }'`
  fi
}
function getNewest() {               # URL_Prefix,Wildcard,OutFile[,DestDir] downloads newest version of a file from given website 
  gnURL_Prefix="$1"     # website that lists file downloads on an index page
  gnWildCard="$2"       # regular expression identifying interesting files
  gnOutFile="$3"        # filename to use for download file
  gnDownloadDir="$4"    # download into this directory
  
  findNewest "$gnURL_Prefix" "$gnWildCard"
  if [ "$Error_findNewest" == "" ]; then
    get "$NewestURL" "$NewestFile" "$gnOutFile" "$gnDownloadDir"
  fi
  return
}
function getScriptPath() {           #                                       ScriptPath = absolute path of directory in which script is located
  MyPWD=`pwd`
  FirstChar=`perl -e "print substr('$0', 0, 1);"`
  if [ "$FirstChar" == "/" ]; then
    MySelf="$0"
  else
    MySelf="$MyPWD/$0"
  fi
  ScriptPath=`perl -e "print substr('$MySelf', 0, rindex('$MySelf', '/'));"`
  cd "$ScriptPath"
  ScriptPath=`pwd`
  cd "$MyPWD"
  ##X echo "MySelf= $MySelf"
  ##X echo "ScriptPath= $ScriptPath"
}
function getScriptName() {           #                                       ScriptName = filename of current script without file-path
  ScriptName=`perl -e "my \\\$P='$0'; my \\\$Pos = rindex(\\\$P, '/') + 1; print substr(\\\$P, \\\$Pos);"`
}
function gitClone() {                # URL, Directory                        clones/ updates git repository from URL into Directory/
  URL="$1"
  Directory="$2"
  
  if [ -d "$Directory" ]; then
    if [ ! -d "$Directory/.git" ]; then
      echo "$0 - removing incomplete git-Folder '$Directory'.." 
      rm -Rf "$Directory"
    fi
  fi

  if [ -d "$Directory" ]; then
    cd "$Directory"
    echo -n "`pwd`> git pull: "
    git pull
    cd ..
  else
    echo "`pwd`> git clone $URL"
    git clone $URL
  fi
}
function insertTextAfter() {         # File,Mark,Text[,QUIET]                insert text into a file after a text marker
  File=$1; shift
  Mark=$1; shift
  Text=$1; shift
  if [ "$1" == "QUIET" ]; then
    QUIET="1"
  else
    QUIET=""
  fi

  if [ -L $File ]; then # file is symlink: write to link target
    Target=`readlink $File`
  else
    Target=$File
  fi
  if [ ! -w "$Target" ]; then                     #{ given file is not writable
    if [ "$QUIET" == "" ]; then
      echo "$0 - ERROR: Cannot write to file '$Target'!"
      printCallerStack
      exit 13
    fi
    exit 10
  fi #}
  if [ "`grep \"$Mark\" \"$File\"`" == "" ]; then #{ given file is missing insert mark
    if [ "$QUIET" == "" ]; then
      echo "$0 - ERROR: Missing insert mark '$Mark' in file '$File'"
      printCallerStack
      exit 14
    fi
    exit 11
  fi #}
  if [ "`grep \"$Text\" \"$File\"`" == "" ]; then #{ given file is missing $Text: insert it
    awk "1;/$Mark/{ print \"$Text\" }" $File >${File}.2
    chmod --reference="$Target" "${File}.2"
    mv ${File}.2 $Target
  fi #}
}
function identifyOS() {              #                                       loads $OS_SHORT, $OS_TYPE, $OS_SUBTYPE, $OS_VERSION, $OS_ARCHITECTURE according to current operating system

  OS_TYPE=""
  OS_SUBTYPE=""
  OS_VERSION=""
  OS_ARCHITECTURE=""
  OS_SHORT=""
  
  if [ -e /etc/SuSE-release ]; then #{ checking repositories on openSUSE
    OS_TYPE="opensuse"
    OS_VERSION=`grep VERSION /etc/SuSE-release | awk '{ print $3 }'`
  fi #}
  if [ -e /etc/lsb-release ]; then #{ checking repositories on Ubuntu
    IsUbuntu=`grep DISTRIB_ID /etc/lsb-release | grep -i ubuntu`
    if [ "$IsUbuntu" != "" ]; then
      OS_TYPE="ubuntu"
    fi
  fi
  if [ "$OS_TYPE" == "" ]; then
    IsUbuntu=`uname -a | grep -i ubuntu`
    if [ "$IsUbuntu" != "" ]; then
      OS_TYPE="ubuntu"
    fi
  fi
  if [ "$OS_TYPE" == "ubuntu" ]; then
    OS_VERSION=`grep RELEASE /etc/lsb-release | awk '{ split($0,a,"="); print a[2] }'`
    
    IsXUbuntu=`grep -i xubuntu /etc/apt/sources.list`
    if [ "$IsXUbuntu" != "" ]; then
      OS_SUBTYPE="xubuntu"
    fi
    IsKUbuntu=`grep -i kubuntu /etc/apt/sources.list`
    if [ "$IsKUbuntu" != "" ]; then
      OS_SUBTYPE="kubuntu"
    fi
  fi #}
  IsX86_64=`uname -a | grep -i x86_64`
  if [ "$IsX86_64" != "" ]; then
    OS_ARCHITECTURE="x86_64"
  fi
  IsX86_32=`uname -a | grep -i i686`
  if [ "$IsX86_32" != "" ]; then
    OS_ARCHITECTURE="x86_32"
  fi

  OS_SHORT="$OS_TYPE ($OS_SUBTYPE) v$OS_VERSION $OS_ARCHITECTURE"
  #echo "identifyOS() - $OS_SHORT"
}
function install() {                 #                                       downloads, compiles + installs software from source tarball
  TestIfInstalled=$1; shift 
  UrlPrefix=$1;       shift
  FileName=$1;        shift
  Suffix=$1;          shift
  UrlSuffix=$1;       shift
  Directory=$1;       shift
  UnpackCommand=$1;   shift
  Configure=$1;       shift
  Make=$1;            shift
  Install=$1;         shift

  # Default-values:  
  # $Suffix          .tar.gz
  # $UrlSuffix       ""
  # $Directory       $FileName
  # $UnpackCommand   "tar xzf", "tar xjf" (for suffix .tar.bz2) or "tar xf" (for suffix .tar) 
  # $Configure       "./configure --prefix='/usr/'"
  # $Make            "make"
  # $Install         "make install"
  
  # Examples:
  # install " " "ftp://ftp.mars.org/pub/mpeg/" "libmad-0.15.1b"
  # install " " "http://downloads.xiph.org/releases/theora/" "libtheora-1.0alpha7" ".tar.bz2"
  # install " " "http://downloads.sourceforge.net/project/inkscape/inkscape/${Version}/" "inkscape-${Version}" ".tar.bz2" "?use_mirror=mesh" ".tar.bz" "inkscape-${Version}"
  # install " " "http://www2.mplayerhq.hu/MPlayer/releases/" "MPlayer-1.0rc1" ".tar.bz2" "MPlayer-1.0rc1" "tar xjf" \
  #             "./configure --cc=gcc410 --host-cc=gcc410 --prefix=/usr --mandir=/usr/share/man --confdir=/etc/mplayer/ --enable-gui --enable-largefiles --enable-menu --enable-mga --enable-xmga --enable-xv --enable-x11 --enable-fbdev --enable-vm  --enable-xvmc  --language=de,en  --enable-mmx  --enable-3dnow  --enable-sse  --enable-sse2  --enable-shm  --with-glib-config=/opt/gnome/bin/glib-config --with-gtk-config=/opt/gnome/bin/gtk-config"

  OriginDirectory=`pwd`
  
  if [ "$ReinstallAll" == "1" ]; then
    TestIfInstalled=""
  fi
  
  if [ $TestIfInstalled ]; then
    echo "skipping already installed '$FileName'"
  else
    if [ "$Suffix" == "" ]; then
      Suffix=".tar.gz"
    fi
    if [ "$Directory" == "" ]; then
      Directory=$FileName
    fi
    if [ "$UnpackCommand" == "" ]; then
      UnpackCommand="tar xzf"
      if [ "$Suffix" == ".tar.bz2" ]; then
        UnpackCommand="tar xjf"
      fi
      if [ "$Suffix" == ".tar.bz" ]; then
        UnpackCommand="tar xjf"
      fi
      if [ "$Suffix" == ".tar" ]; then
        UnpackCommand="tar xf"
      fi
    fi
    if [ "$Configure" == "" ]; then
      Configure="./configure --prefix=/usr"
    fi
    if [ "$Make" == "" ]; then
      Make="make"
    fi
    if [ "$Install" == "" ]; then
      Install="make install"
    fi
    
    echo ""
    echo "installiere $FileName"
    
    if [ ! -e $FileName$Suffix ]; then
      echo "Datei '$FileName$Suffix' nicht gefunden"
      pwd
      echo "hole $UrlPrefix$FileName$Suffix$UrlSuffix"
      wget -N $UrlPrefix$FileName$Suffix$UrlSuffix
      echo ""
    fi
    
    if [ ! "$UnpackCommand" == " " ]; then
      echo "entpacke mit '$UnpackCommand'"
      $UnpackCommand $FileName$Suffix
      echo "rm -Rf $FileName" >>$CleanUpBatch
    fi
    
    if [ ! -d $Directory ]; then
      echo "Verzeichnis '$Directory' nicht gefunden!"
  pwd
      echo "Verzeichnis '$Directory' nicht gefunden!" >>$OriginDirectory/$FileName.err
    else
      cd "$Directory"
      LogFileBase="../$FileName"
      if [ "$Directory" == "." ]; then
        LogFileBase="$FileName"
      fi
      Error=""
      echo -n "kompiliere mit $Configure && $Make && $Install || Error='1'."
      $Configure >$OriginDirectory/$FileName.log  2>$OriginDirectory/$FileName.err && echo -n "." && \
      $Make     >>$OriginDirectory/$FileName.log 2>>$OriginDirectory/$FileName.err && echo -n "." && \
      $Install  >>$OriginDirectory/$FileName.log 2>>$OriginDirectory/$FileName.err || \
      Error='1' 
      
      if [ -n "$Error" ]; then
        echo "FEHLER!"
        echo "$FileName" >>$OriginDirectory/$Failed_Installs
        else
        echo "OK"
      fi
    fi
    cd "$OriginDirectory"
  fi
}
function installPackage() {          # Name1, Name2, ...                     installs all named packages via local package manager (zypper or apt-get)
  if [ -x /usr/bin/zypper ]; then
    sudoCMD "zypper --no-gpg-checks install -n -l -y $@"
  else
    sudoCMD "dpkg --configure -a"    # some installations showed a problem requiring this command
    waitForProcess dpkg
    export DEBIAN_FRONTEND=noninteractive
    # cmd "sudo DEBIAN_FRONTEND=noninteractive apt-get --install-suggests --force-yes --fix-missing -y install $@"
    #sudoCMD "DEBIAN_FRONTEND=noninteractive apt-get --force-yes --fix-missing -y install $@"
    sudoCMD "DEBIAN_FRONTEND=noninteractive apt-get --allow-unauthenticated --fix-missing -y install $@"
    if [ "$CMD_ERROR" != "" ]; then
      Failed_Packages="$Failed_Packages $@"
    fi
  fi
}
function installPackageSafe() {      # CheckIfInstalled, Name,               Alternative1, ...  installs named package, installs alternatives if test fails
#{ Example Usage:
#  if [ ! -e OK.Packages ]; then #{ install some general software packages
#      FailedInstalls="" 
#      installPackage "which git" git gitosis
#      installPackage "which svn" svn subversion
#      if [ "$FailedInstalls" == "" ]; then
#        touch OK.Packages
#      else
#        echo "$0 - ERROR: Cannot install required software packages: $FailedInstalls"
#        exit 10
#      fi
#    fi #}
#}
    
  CheckIfInstalled="$1"      # 

  for Name in $2 $3 $4 $5 $6; do
    if [ "`$CheckIfInstalled`" == "" ]; then #{ test failed: install package
      if [ -x /usr/bin/zypper ]; then
        sudoCMD "zypper --no-gpg-checks install -n -l -y $Name"
      else
        sudoCMD "dpkg --configure -a"    # some installations showed a problem requiring this command
        waitForProcess dpkg
        export DEBIAN_FRONTEND=noninteractive
        # cmd "sudo DEBIAN_FRONTEND=noninteractive apt-get --install-suggests --force-yes --fix-missing -y install $Name"
        sudoCMD "DEBIAN_FRONTEND=noninteractive apt-get --force-yes --fix-missing -y install $Name"
      fi
    fi #}
  done
  if [ "`$CheckIfInstalled`" == "" ]; then #{ test failed: package install error
    echo "$0 installPackageSafe() - ERROR: Could not install any package from of $2 $3 $4 $5 $6!"
    printCallerStack "    "
    FailedInstalls="$Name $FailedInstalls"
  fi #}
}
function makeOrig() {                # File                                  creates backup file XXX_orig if not present
  bFile="$1"
  
  BakFile="${bFile}_orig"
  if [ ! -e "$BakFile" ]; then
    cp "$bFile" "$BakFile"
  fi
}
function printCallerStack() {        # Prefix [,LogFile]                     displays history of function that called current one
   Prefix="$1"   # string that should prefix every displayed line
   LogFile="$2"  # if given then text will be send to this file 
   
   if [ "$LogFile" == "" ]; then
     echo >&2 "${Prefix}line `caller`"
     for Level in 1 2 3 4 5 6 7 8 9 10; do
       if [ "`caller $Level`" != "" ]; then
         echo >&2 "${Prefix}called from line `caller $Level`"
       fi
     done
   else 
     echo >$LogFile 2>&1 "${Prefix}line `caller`"
     for Level in 1 2 3 4 5 6 7 8 9 10; do
       if [ "`caller $Level`" != "" ]; then
         echo >$LogFile 2>&1 "${Prefix}called from line `caller $Level`"
       fi
     done
   fi
}
function printError() {              # Message                               print given message as error message
  Message=$1; shift
  
  echo >&2 "$0 - ERROR: $Message"
  for Message in $@; do # print all other error messages
    echo >&2 "$Message "
  done
  
  echo >&2 "PWD=`pwd`"
  printCallerStack
}
function replaceInFile() {           # File,Search,Replace[,QUIET]           replaces all occurences of "$Search" in file $File by "$Replace"
    riF_File="$1"
    Search="$2"   # some characters have to be escape (e.g. * -> \*)
    Replace="$3"  # some characters have to be escape (e.g. * -> \*)
    if [ "$4" == "QUIET" ]; then 
      Quiet="1" # do not report unchanged files
    else
      Quiet=""
    fi
    
   if [ ! -e "${riF_File}_orig" ]; then
      makeOrig "${riF_File}"
    fi
    
    echo "replace $riF_File: $Search"
    echo "     -> $riF_File: $Replace"
    echo "sed -e \"s|$Search|$Replace|g\" $riF_File"
    sed -e "s|$Search|$Replace|g" $riF_File >${riF_File}_2
    
    if [ "$Quiet" == "" ]; then #{ report when file was NOT changed
      FileChanged=`diff ${riF_File}_2 ${riF_File}` 
      if [ "$FileChanged" == "" ]; then
        echo "replaceInFile($riF_File) file unchanged!"
        printCallerStack "    "
      fi
    fi #}
    
    mv -f ${riF_File}_2 ${riF_File}
}
function replaceLinesInFile() {      # File,StartWith,ReplaceBy              replaces all lines in given file starting with given $StartWith string with given $ReplaceBy line
    rlif_File="$1"
    rlif_StartWith="$2"   # some characters have to be escape (e.g. * -> \*)
    rlif_ReplaceBy="$3"  # some characters have to be escape (e.g. * -> \*)
  
    makeOrig "${rlif_File}"
    
    sed -i "/$rlif_StartWith/c\\$rlif_ReplaceBy" "$rlif_File"

}
function startIfMissing() {          # File,Command,Name                     starts Command if File is missing
  
  TestFile=$1
  Command=$2
  Name=$3
  
  if [ ! -e $TestFile ]; then
    echo "Datei nicht gefunden: '$TestFile'"
    if [ ! "$Name" == "" ]; then
      echo "installiere $Name"
    fi
    $Command
  else
    echo "Datei bereits vorhanden: '$TestFile'"
  fi
}
function sudoCMD() {                 # CMD                                   executes given command as sudo
   echo >&2 "`pwd` > sudo $@"
   /usr/bin/sudo $@
   
   EC="$?"
   if [ "$EC" != "0" ]; then
     CMD_ERROR="RC=$EC from $@"
     echo "$CMD_ERROR"
     printCallerStack "    "
   else
     CMD_ERROR=""
   fi
}
function testZIP() {                 # Archiv                                tests integrity of .zip archive (exports $Error)
  Archiv="$1"
  
  export Error=`unzip -qqt $Archiv 2>&1`
}
function testTARBZ() {               # Archiv                                tests integrity of .tar.bz archive (exports $Error)
  Archiv="$1"
  
  export Error=`bzip2 -t $Archiv 2>&1`
}
function untgz() {                   # Dir,Archiv                            unpacks .tar.gz / .tgz archive if Dir is missing
  Dir="$1"
  Archiv="$2"
  

  if [ ! -d $Dir ]; then
    cmd "tar xzf $Archiv"
  fi
}
function unTBZ() {                   # Dir,Archiv                            unpacks .tar.bz2 / .tbz archive if Dir is missing
  Dir="$1"
  Archiv="$2"
  

  if [ ! -d $Dir ]; then
    cmd "tar xjf $Archiv"
  fi
}
function untgj() {                   # Dir,Archiv                            unpacks .tar.bz2 / .tgj archive if Dir is missing
  Dir="$1"
  Archiv="$2"
  

  if [ ! -d $Dir ]; then
    cmd "tar xjf $Archiv"
  fi
}
function unZIP() {                   # Dir,Archiv                            unpacks .zip archive if Dir is missing
  Dir="$1"
  Archiv="$2"
  
  installPackageSafe "which unzip" unzip
  if [ ! -d $Dir ]; then
    cmd "unzip -o $Archiv" "unzip.log"
  fi
}
function unRAR() {                   # Dir,Archiv                            unpacks .rar archive if Dir is missing
  Dir="$1"
  Archiv="$2"
  
  installPackageSafe "which unrar" unrar
  if [ ! -d "$Dir" ]; then
    cmd "unrar x -u -y $Archiv" "unrar.log"
  fi
}
function updateNewest() {            # URL_Prefix,Wildcard,SourceFile        looks for newest file via findNewest() + updates source-file
  unVariablePrefix="$1"  # Prefix to use for variable names when adding lines to $unSourceFile 
  unURL_Prefix="$2"      # website that lists file downloads on an index page
  unWildCard="$3"        # regular expression identifying interesting files
  unSourceFile="$4"      # shell-script where to store URL of newest file URL
  
  #X findNewest https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2'
  findNewest $unURL_Prefix $unWildCard
  if [ ! -e "$unSourceFile" ]; then #{ create new file
  cat <<END_OF_SCRIPT >$unSourceFile
#!/bin/bash
#
# list of newest versions of file downloads.
# This file has been created/ updated by 
# $0
#

END_OF_SCRIPT
  fi #}    
  NotNew=`grep "$NewestFile" "$unSourceFile"`
  if [ "$NotNew" == "" ]; then #{
    echo "found new: '$NewestURL$NewestFile'"
    cat <<END_OF_SCRIPT >>$unSourceFile #{

# $0 FINDNEWEST (`date`)
${unVariablePrefix}_URL="$NewestURL"
${unVariablePrefix}_File="$NewestFile"
END_OF_SCRIPT

#}
    export return="$NewestURL$NewestFile"
  else
    echo "no update."
    export return=""
  fi #}

}
function updateSoftware() {          #                                       downloads all available software updates and brings OS up to date
    if [ "`which apt-get`" != "" ]; then
      echo "getting required system updates..."
      sudoCMD apt-get update -y
      sudoCMD apt-get dist-upgrade -y
    fi
    if [ "`which zypper`" != "" ]; then
      echo "getting required system updates..."
      zypper update --auto-agree-with-licenses --best-effort -y
    fi
}
function waitForProcess() {          # Processname                           waits until all processes matching given name do exit
  ProcessName="$1"
  echo "$0 - waiting for process '$ProcessName' to exit.."
  while killall -0 $ProcessName 2>/dev/null; do sleep 1; done
}

identifyOS
countCPUs
getScriptName # ScriptName = name of current script without leading path name
getScriptPath # ScriptPath = absolute path of directory in which script is located             

#} InstallFuncs v1.11 - written by Gregor Rebel
#{ Extra functions for The ToolChain -------------------------------------------------

function getDocumentation() {               # URL_Prefix,File,OutFile,DocSubDir     will download given URL into given subdirectory of documentation folder
  URL_Prefix="$1"
  URL_File="$2"
  OutFile="$3"
  DocSubDir="$4"  # sub-folder to be created inside documentation folder (may contain sub-folders like "uC/stm32f10x/CMSIS")
  
  getFile $URL_Prefix $URL_File $OutFile
  Categories=`perl -e "my \\$S='$DocSubDir'; my @A=split('/', \\$S); print '\\"'.join('\\" \\"', @A).'\\"'.\\"\\\n\\";"`
  
  addDocumentationFile "$OutFile" "$DocSubDir"
  if [ "$DocSubDir" == "" ]; then
    echo "ERROR: Missing Argument DocSubDir!"
  fi
  echo "getDocumentation(`pwd`/$OutFile) -> `pwd`/$OutFile" >>$HOME/Source/TheToolChain/Documentation/files.log
  printCallerStack "    " $HOME/Source/TheToolChain/Documentation/files.log
}
function addDocumentationFile() {           # File, Category1/[Category2/ ...]      moves given file into documentation folder + creates symbolic link
  adF_File="$1"  # must be found in current working directory!
  Cat1="$2"
  Cat2="$3"
  Cat3="$4"
  
  findFolderUpwards Documentation; DocFolder="$FoundFolder"
  
  if [ "$Cat1" != "" ]; then #{ create category folder (if required)
    DocFolder=$DocFolder/$Cat1  
    if [ ! -d $DocFolder ]; then
      dir "$DocFolder"
    fi
    DocFolder=$DocFolder/$Cat2  
    if [ ! -d $DocFolder ]; then
      dir "$DocFolder"
    fi
    DocFolder=$DocFolder/$Cat3  
    if [ ! -d $DocFolder ]; then
      dir "$DocFolder"
    fi
  fi #}
  if [ -e "$adF_File" ]; then
    cp -R --dereference "$adF_File" "$DocFolder/"
  fi

  echo "addDocumentationFile(`pwd`/$adF_File) -> `pwd`/$DocFolder" >>$HOME/Source/TheToolChain/Documentation/files.log
  printCallerStack "    "  $HOME/Source/TheToolChain/Documentation/files.log
}
function addDocumentationFolder() {         # Folder, Name, Category1/[Category2/ ...] adds a complete folder as symbolic link into sub-hierarchy of documentation folder
  Folder="$1"  # must be found in current working directory!
  Name="$2"
  Cat1="$3"
  Cat2="$4"
  Cat3="$5"

  findFolderUpwards Documentation; DocFolder="$FoundFolder"
  
  if [ "$Cat1" != "" ]; then #{ create category folder (if required)
    DocFolder=$DocFolder/$Cat1  
    if [ ! -d $DocFolder ]; then
      mkdir "$DocFolder"
    fi
    DocFolder=$DocFolder/$Cat2  
    if [ ! -d $DocFolder ]; then
      mkdir "$DocFolder"
    fi
    DocFolder=$DocFolder/$Cat3  
    if [ ! -d $DocFolder ]; then
      mkdir "$DocFolder"
    fi
  fi #}
  if [ -d "$Folder" ]; then
    #X echo "creating documentation link '$Folder' -> '$DocFolder/$Name'"
    createLink "`pwd`/$Folder" "$DocFolder/$Name"
  fi
}
cASH_Activated="" # stores Name of last createActivateScriptHead() call
function createActivateScriptHead() {       # ScriptName, Directory, CreatorScript, Info  creates head of activate.XXXX script in given file
  cASH_ScriptName="$1"
  cASH_Directory="$2"
  cASH_CreatorScript="$3"
  cASH_InfoText="$4"

  if [ "$cASH_Activated" != "" ]; then #{ ERROR: function called twice
    Error="createActivateScriptHead() was called twice!"
  
    cat <<END_OF_ERROR
$0 - createActivateScriptHead() ERROR: $Error

It is mandatory to call createActivateScriptTail() after every createActivateScriptHead() for same script!

Previous call: createActivateScriptTail('$cASH_Activated')
Current call:  createActivateScriptTail('$cASH_ScriptName', 
                                        '$cASH_Directory', 
                                        '$cASH_CreatorScript'
                                        '$cASH_InfoText'
                                       )
                                       
Called from  ${BASH_SOURCE[*]}:${BASH_LINENO[*]}
END_OF_ERROR
    exit 10
  fi #}
  cASH_Activated="$cASH_ScriptName"
  
  ActivateScriptFile="${cASH_Directory}/activate.${cASH_ScriptName}.sh"

  if [ "$cASH_CreatorScript" == "" ]; then #{
    cASH_CreatorScript="$0"
  fi #}
  if [ "$cASH_InfoText" == "" ]; then #{
    cASH_InfoText="DEFAULT_INFO: $ActivateScriptFile created by $cASH_CreatorScript"
  fi #}

  cASH_InfoText="$cASH_InfoText - architectures: `$iF_StartDir/scripts/findLowLevelArchitectures.pl $cASH_CreatorScript`"
  #echo "createActivateScriptHead($ActivateScriptFile) called from `caller` PWD `pwd`"
  Year=`date +%Y`
  cat <<END_OF_SCRIPT    >$ActivateScriptFile #{
#!/bin/bash
#
#                    The ToolChain
#
#           written 2010-$Year by Gregor Rebel
#
# This script activates an extension for TheToolChain.
# This script has been automatically created by $cASH_CreatorScript.
# Any changes made to this script will be lost!
#
# Usage: Run this script from your project folder where your main.c is.
#

#{ SCRIPT_HEADER ---------------------------------------------------------------

DirName=\`dirname \$0\`
if [ "\$DirName" == ""      ]; then #{
  DirName=\`pwd\`
fi #}
if [       "\$1" == "QUIET" ]; then #{
  Quiet="1"
  shift
fi #}
if [       "\$1" == "INFO"  ]; then #{
  cat <<'END_OF_INFO'
$cASH_InfoText
END_OF_INFO
  exit 0
fi #}
if [       "\$1" == "TEST"  ]; then #{
  TEST="1"
  shift
else
  TEST=""
fi #}   
if [       "\$1" == "HELP" ]; then #{
  cat <<END_OF_HELP
$ActivateScriptFile [INFO|TEST|HELP|PROVIDES] [QUIET] [ONLY <ARCHITECTURE>] [<CALLER>]

INFO            print out short single single info text about this activate and exit
TEST            sets variable TEST=1 (can be used by user part of activate script)
HELP            this text
PROVIDES        print out list of supported low-level drivers (looks for corresponding low_level/install_*.sh files)
QUIET           suppress text output
ONLY            run only single low-level install script corresponding to <ARCHITECTURE> instead of all
<ARCHITECTURE>  one from output of PROVIDES

$ActivateScriptFile has been created by $0 on `date`

END_OF_HELP
  exit 0
fi #}

END_OF_SCRIPT
#}
  cat <<'END_OF_SCRIPT' >>$ActivateScriptFile #{

# ensure that $PATH contains no . (brings find into trouble)
export PATH=`perl -e "my @A=split(':', '$PATH');  print join(':', grep { substr(\\\$_,0,1) eq '/' } @A );"`

function sudoCMD() {          # CMD                     executes given command as sudo
   echo >&2 "`pwd` > sudo $@"
   /usr/bin/sudo $@
   
   EC="$?"
   if [ "$EC" != "0" ]; then
     CMD_ERROR="RC=$EC from $@"
     echo "$CMD_ERROR"
     printCallerStack "    "
   else
     CMD_ERROR=""
   fi
}
function createLink() {       # Source,Link,User,QUIET  creates symbolic link (removes old one before)
  cl_Source="$1"
  cl_Link="$2"
  cl_User="$3"
  cl_Quiet="$4"
  
  cl_Quiet="1"
  if [ "$cl_Quiet" = "" ]; then
    Verbose="-v"
  else
    Verbose=""
  fi
  if [ ! -e "$cl_Source" ]; then
    MissingFiles="$MissingFiles $cl_Source"
  fi
  
#X   if [ "$cl_User" != "" ]; then
#X     sudoCMD -u$cl_User rm -f $Verbose $cl_Link
#X   else
#X     rm -f $cl_Link
#X   fi
   
  if [ "$cl_User" != "" ]; then
    sudoCMD -u$cl_User ln $Verbose -sf "$cl_Source" "$cl_Link"
  else
    ln $Verbose -sf "$cl_Source" "$cl_Link"
  fi
}
function getScriptName() {    #                         ScriptName = returns filename of current script without file-path
  ScriptName=`perl -e "my \\\$P='$0'; my \\\$Pos = rindex(\\\$P, '/') + 1; print substr(\\\$P, \\\$Pos);"`
}
function getScriptPath() {    #                         ScriptPath = absolute path of directory in which script is located
  MyPWD=`pwd`
  FirstChar=`perl -e "print substr('$0', 0, 1);"`
  if [ "$FirstChar" == "/" ]; then
    MySelf="$0"
  else
    MySelf="$MyPWD/$0"
  fi
  ScriptPath=`perl -e "print substr('$MySelf', 0, rindex('$MySelf', '/'));"`
  cd "$ScriptPath"
  ScriptPath=`pwd`
  cd "$MyPWD"
  #X echo "MySelf= $MySelf"
  #X echo "ScriptPath= $ScriptPath"
}
function addLine() {          # File,Line               adds given line to File if not already done
  File2Add2="$1"
  Line="$2"
  
  if [ ! -e $File2Add2 ]; then
    touch $File2Add2
  fi
  AlreadyPresent=`grep "$Line" $File2Add2`
  if [ "$AlreadyPresent" == "" ]; then
    echo "$Line" >>$File2Add2
#  else
#    echo "file already up2date: $File2Add2"
  fi
}
function enableFeature() {    # NAME                    creates file extensions.active/feature.NAME for given NAME 
  pF_Name="$1"
  
  if [ "$Dir_ExtensionsActive" == "" ]; then
    Dir_ExtensionsActive="extensions.active"
  fi

  echo -n "enableFeature($pF_Name) "
  printCallerStack "    "

  #X pF_File="$Dir_ExtensionsActive/feature.${pF_Name}"
  #X echo "$0: providing feature '$pF_File'"
  #X echo "$0" >>$pF_File
  _/enableFeature.pl ${pF_Name}
  if [ "$?" != "0" ]; then
    printCallerStack "    "
  fi
} #
function checkFeature() {     # NAME [,DESCRIPTION]     checks if named feature has been provided before
# Usage Example:
#
# checkFeature "450_cpu_cortexm3" "CPU driver for ARM CortexM3 based microcontrollers"
# if [ "$?" == "1" ]; then # feature available: activate this extension
#   ...
# fi
#

  Dependency="$1"
  Description="$2" # used by createNewProject.pl
  
  if [ "$Dependency" == "" ]; then
    return 1 # empty dependency is always provided
  fi
  if [ "$Dir_ExtensionsActive" == "" ]; then
    Dir_ExtensionsActive="extensions.active"
  fi

  cF_Feature=`ls 2>/dev/null $Dir_ExtensionsActive/*${Dependency}*`
  if [ "$cF_Feature" != "" ]; then
    #echo "found feature '$cF_Feature'"
    return 1
  fi
  
  #echo "$0: missing feature '$Dependency'"
  return 0
} #
function printCallerStack() { # Prefix                  displays history of function that called current one
   Prefix="$1"  # string that should prefix every displayed line
   
   echo "${Prefix}line `caller`"
   for Level in 1 2 3 4 5 6 7 8 9 10; do
     if [ "`caller $Level`" != "" ]; then
       echo "${Prefix}called from line `caller $Level`"
     fi
   done
}

CurrentPath=`pwd`
getScriptName                                 # ScriptName = name of current script without leading path name
getScriptPath                                 # ScriptPath = absolute path of directory in which script is located
Dir_Extensions=$CurrentPath/extensions        # folder where this script is located
Dir_ExtensionsLocal=${Dir_Extensions}.local   # path to extensions.local/ (extension configuration and makefile for local project)
Dir_Configs=$CurrentPath/configs              # path to configs/          (static configuration files for local project) 
Dir_Additionals=$CurrentPath/additionals      # path to additionals/      (additional libraries being required for local project)
Dir_Bin=$CurrentPath/bin                      # path to bin/              (binaries and symlinks to binaries being used in local project)
cd "$CurrentPath"

if [ -d extensions.active ]; then #{
  Dir_ExtensionsActive="extensions.active"
  Dir_Additionals="additionals"
#}
else #{
  if [ -d ../extensions.active ]; then
    Dir_ExtensionsActive="../extensions.active"
    Dir_Additionals="../additionals"
  else
    Dir_ExtensionsActive=${ScriptPath}.active
    Dir_Additionals=""
  fi
fi #}

END_OF_SCRIPT
#}
  cat <<END_OF_SCRIPT   >>$ActivateScriptFile #{

if [ "\$1" == "PROVIDES" ]; then #{
  _/extractProvidedMainObjects.pl \${Dir_Extensions}/makefile.${cASH_ScriptName}
  exit 0
fi #}

if [ -e "\${Dir_ExtensionsActive}/makefile.${cASH_ScriptName}" ]; then
  # echo "\$0 - already activated"
  exit 0
fi

if [ "\$1" == "ONLY" ]; then # 
  Architecture=\$2
  if [ "\$Architecture" != "" ]; then # user wants to run only single low level activate script
    File_LowLevelInstall="\`ls extensions/activate.450_*\${Architecture}.sh\`"
    echo "\$0 - Will activate single architecture '\$Architecture'"
  fi
  shift
  shift
fi

if [ "\$1" != "" ]; then
  Caller="\$1"
  shift
fi

if [ "\$Quiet" == "" ]; then
  echo ""
fi

echo "    running: $ActivateScriptFile (called from \$Caller)"

#}SCRIPT_HEADER
#{ USER_PART -------------------------------------------------------------------

END_OF_SCRIPT
#}
#}
  if [ ! -e "$ActivateScriptFile" ]; then
    echo ""
    echo "$0 - ERROR: Cannot create activate script '$ActivateScriptFile' PWD='`pwd`"
    printCallerStack "    "
    exit 10
  fi

}
function createActivateScriptTail() {       # ScriptName, Directory, Name           creates tail of activate.XXXX script in given file
  cAST_ScriptName="$1"
  cAST_Directory="$2"
  #X cAST_Name="$3"
  
  if [ "$cASH_Activated" == "" ]; then #{ ERROR: 
    Error="createActivateScriptHead() not called before!"
  else
    if [ "$cASH_Activated" != "$cAST_ScriptName" ]; then
      Error="createActivateScriptHead() called for different script!"
    fi
  fi #}
  if [ "$Error" != "" ]; then #{
    cat <<END_OF_ERROR
$0 - createActivateScriptTail() ERROR: $Error

It is mandatory to call createActivateScriptHead() before every createActivateScriptTail() for same script!

Previous call: createActivateScriptHead('$cASH_Activated')
Current call:  createActivateScriptTail('$cAST_ScriptName', 
                                        '$cAST_Directory'
                                       )

Called from  ${BASH_SOURCE[*]}:${BASH_LINENO[*]}
END_OF_ERROR
    exit 10
  fi #}
  cASH_Activated=""

  ActivateScriptFile="${cAST_Directory}/activate.${cAST_ScriptName}.sh"

  cat <<END_OF_SCRIPT >>$ActivateScriptFile #{

  
#}USER_PART
#{ SCRIPT_FOOTER ---------------------------------------------------------------

# back to project folder
cd "\$CurrentPath"
if [ -e "\${Dir_ExtensionsActive}/makefile.$cAST_ScriptName" ]; then
  echo "    Activated: $cAST_ScriptName (called from \$Caller)"
  if [ "\$Quiet" == "" ]; then
    echo ""
    echo "\${CurrentPath}/extensions.active/"
    find "\${Dir_ExtensionsActive}/" -name "makefile.*" -execdir echo '  {}' \; | sort
  fi
#X else
#X   echo "ERROR: Failed to activate extension $cAST_ScriptName in \$CurrentPath (missing '\`pwd\`/\${Dir_ExtensionsActive}/makefile.$cAST_ScriptName')!"
fi

#}SCRIPT_FOOTER 
END_OF_SCRIPT
  chmod +x $ActivateScriptFile
}
function createActivateScript() {           # ScriptName,Directory,CreatorScript,Delete,Info   creates complete activate.XXXX script in given file
    cAS_ScriptName="$1"    # used to build filename 
    cAS_Directory="$2"     # relative/ absolute directory path where to create activate-script
    cAS_CreatorScript="$3" # $0 of script calling createActivateScript() (makes debugging easier) 
    cAS_Delete="$4"        # != "": will add rm $cAS_Delete to header of activate script 
    cAS_InfoText="$5"      # each activate script provides a short information text
    
    createActivateScriptHead "$cAS_ScriptName" "$cAS_Directory" "$cAS_CreatorScript" "$cAS_InfoText"
    if [ "$cAS_Delete" != "" ]; then
      echo "rm 2>/dev/null \${Dir_ExtensionsActive}/$cAS_Delete" >>$ActivateScriptFile
    fi
    echo "createLink \$ScriptPath/makefile.$cAS_ScriptName \${Dir_ExtensionsActive}/makefile.$cAS_ScriptName '' QUIET \"\$0\"" >>$ActivateScriptFile
    createActivateScriptTail "$cAS_ScriptName" "$cAS_Directory"
}
cEMH_Activated="" # stores Name of last createExtensionMakefileHead() call
function createExtensionMakefileHead() {    # Name,[Folder],[ExtensionName]         creates head of makefile.XXXX in given file; return variable MakeFile
  cEMH_Name="$1"           # used as name suffix for created makefile
  cEMH_Folder="$2"         # if given, makefile will be created in this folder
  cEMH_ExtensionName="$3"  # if given, and !="$cEMH_Name" then it will be used as constant definition suffix for EXTENSION_
  if [ "$cEMH_ExtensionName" == "" ]; then
    cEMH_ExtensionName="$cEMH_Name"
  fi
  cEMH_NameShort=`perl -e "print substr('$cEMH_Name', 4);"`;                   # remove rank from name
  cEMH_ExtensionNameShort=`perl -e "print substr('$cEMH_ExtensionName', 4);"`; # remove rank from extension name
  
  if [ "$cEMH_Activated" != "" ]; then #{ ERROR: function called twice
    Error="createExtensionMakefileHead() was called twice!"
  
    cat <<END_OF_ERROR
$0 - createExtensionMakefileHead() ERROR: $Error

It is mandatory to call createExtensionMakefileTail() after every createExtensionMakefileHead() for same script!

Previous call: createExtensionMakefileTail('$cEMH_Activated')
Current call:  createExtensionMakefileTail('$cEMH_Name', '$cEMH_Folder')
                                       
Called from  ${BASH_SOURCE[*]}:${BASH_LINENO[*]}
END_OF_ERROR
    exit 10
  fi #}
  cEMH_Activated="$cEMH_Name"

  if [ "$cEMH_Folder" == "" ]; then #{
    findFolderUpwards "extensions"
    cEMH_Folder="$FoundFolder"
    if [ ! -d "$cEMH_Folder" ]; then # target folder does not exist: maybe one level upwards?
      cEMH_Folder="../$cEMH_Folder"
    fi
  fi #}
  ExtensionMakeFile="$cEMH_Folder/makefile.${cEMH_Name}" #{
  #echo "createExtensionMakefileHead($ExtensionMakeFile) called from `caller`"

  cat <<END_OF_MAKEFILE >$ExtensionMakeFile 
#{ EXTENSION_$cEMH_Name

# Now only defining constants without rank number
EXTENSION_$cEMH_ExtensionNameShort = 1
COMPILE_OPTS += -DEXTENSION_$cEMH_ExtensionNameShort=1

END_OF_MAKEFILE
#}
#}
  if [ ! -e $ExtensionMakeFile ]; then
    echo "$0 - ERROR: Cannot create makefile '$ExtensionMakeFile' PWD='`pwd`"
    printCallerStack "    "
    exit 10
  fi
}
function createExtensionMakefileTail() {    # Name,[Folder]                         creates tail of makefile.XXXX in given file
  cEMT_Name="$1"
  cEMT_Folder="$2"
  
  if [ "$cEMH_Activated" == "" ]; then #{ ERROR: 
    Error="createExtensionMakefileHead() not called before!"
  else
    if [ "$cEMH_Activated" != "$cEMT_Name" ]; then
      Error="createExtensionMakefileHead() called for different script!"
    fi
  fi #}
  if [ "$Error" != "" ]; then #{
    cat <<END_OF_ERROR
$0 - createExtensionMakefileTail() ERROR: $Error

It is mandatory to call createExtensionMakefileHead() before every createExtensionMakefileTail() for same script!

Previous call: createExtensionMakefileHead('$cEMH_Activated')
Current call:  createExtensionMakefileTail('$cEMT_Name', '$cEMT_Folder')

Called from  ${BASH_SOURCE[*]}:${BASH_LINENO[*]}
END_OF_ERROR
    exit 10
  fi #}
  cEMH_Activated=""

  if [ "$cEMT_Folder" == "" ]; then #{
    findFolderUpwards "extensions"
    cEMT_Folder="$FoundFolder"
  fi #}
  ExtensionMakeFile="$cEMT_Folder/makefile.${cEMT_Name}" #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile #{ 
#} EXTENSION_$cEMT_Name
END_OF_MAKEFILE
#}
}
function createExtensionSourcefileHead() {  # Name,[Folder]                         creates head of source.XXXX.c in given file; return variable MakeFile
  cESH_Name="$1"
  cESH_Folder="$2"
  
  if [ "$cESH_Folder" == "" ]; then
    findFolderUpwards "extensions"
    cESH_Folder="$FoundFolder"
  fi
  export ExtensionSourceFile="$cESH_Folder/source.${cESH_Name}.c" #{
  cat <<END_OF_SOURCEFILE >$ExtensionSourceFile 
//{ EXTENSION_$cESH_Name
END_OF_SOURCEFILE
#}
#}

  if [ "$cESH_Folder" == "" ]; then
    echo "$0 - createExtensionSourcefileHead($cESH_Name) ERROR: Missing argument cESH_Folder! PWD='`pwd`"
    printCallerStack "    "
    exit 10
  fi
  if [ ! -e $ExtensionSourceFile ]; then
    echo "$0 - createExtensionSourcefileHead($cESH_Name)  ERROR: Cannot create source file '$ExtensionSourceFile' PWD='`pwd`"
    printCallerStack "    "
    exit 10
  fi
}
function createExtensionSourcefileTail() {  # Name,[Folder]                         creates tail of source.XXXX.c in given file
  cEST_Name="$1"
  cEST_Folder="$2"
  
  if [ "$cEST_Folder" == "" ]; then
    findFolderUpwards "extensions"
    cEST_Folder="$FoundFolder"
  fi
  export ExtensionSourceFile="$cEST_Folder/source.${cEST_Name}.c" #{
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile #{
//} EXTENSION_$cEST_Name
END_OF_SOURCEFILE
#}
}
function getToolChainVersion() {            #                                       $VersionTTC will be loaded with content of Version file

  if [ "$VersionTTC" == "" ]; then
    OldPWD=`pwd`
    for Level in 1 2 3 4 5 6 7; do
      if [ "$VersionTTC" == "" ]; then
        for gTCV_File in scripts/Version _/Version Version; do
          if [ -e $gTCV_File ]; then
            VersionTTC=`cat $gTCV_File`
            echo "found Version '$VersionTTC' in '`pwd`/$gTCV_File'"
          fi
        done
        cd ..
      fi
    done
    cd $OldPWD
    export VersionTTC
  fi
}
function getFile() {                        # URL_Prefix,File,OutFile,DestDir       retrieves file from URL into common download folder + creates local symbolic link
  URL_Prefix="$1"
  URL_File="$2"
  OutFile="$3"
  DestDir="$4"
  
  if [ "$OutFile" == "" ]; then
    OutFile="$URL_File"
  fi
  
  echo "getFile('$URL_Prefix' '$URL_File' '$OutFile')"
  getToolChainVersion
  DownloadDir="$HOME/Source/TheToolChain.Contents"
  VersionDir="$HOME/Source/TheToolChain.Contents.$VersionTTC"
  dir "$DownloadDir"
  dir "$VersionDir"
  
  TempFile="$DownloadDir/$URL_File"
  get "$URL_Prefix" "$URL_File" "$TempFile"
  #X getFileSize "$TempFile"
  #X if [ "$FileSize" == "0" ]; then
  #X   echo "ignoring received empty file"
  #X   rm "$TempFile"
  #X fi
  if [ ! -e "$TempFile" ]; then
    echo "Original URL not found: '$URL_Prefix/$URL_File'"
    getFileFromMirror "$URL_File" "$TempFile"
  fi
  #X getFileSize "$TempFile"
  #X if [ "$FileSize" == "0" ]; then
  #X   echo "ignoring received empty file"
  #X   rm "$TempFile"
  #X fi

  if [ -e "$TempFile" ]; then
    if [ "$DestDir" != "" ]; then
      dir "$DestDir"
    fi
    rm -f  "$DestDir$OutFile"
    createLink "$TempFile" "$DestDir$OutFile"
    #echo -n "content link: "
    #ln -fv "$TempFile" "$VersionDir/"
    GetFile_Error=""
  else
    GetFile_Error="ERROR: Missing file '$TempFile'"
    echo "getFile($URL_Prefix$URL_Prefix) - ERROR: Missing file '$TempFile'"
    printCallerStack "    "
    addLine "$DownloadDir/_FailedDownloads.text" "wget -N $URL_Prefix/$URL_File -O \"$OutFile\""
  fi
}
function getFileFromMirror() {              # FileName,OutFile             tries to download given file from mirror-site (thetoolchain.com/mirror/)
  gFFM_FileName="$1"  # name of file on website
  gFFM_OutFile="$2"   # name of file to create on local disc (relative or absolute file-path) 
  
  getToolChainVersion
  for MirrorURL in "http://thetoolchain.com/mirror/$VersionTTC/" "http://thetoolchain.com/mirror/"; do
    echo "fetching from backup mirror: $MirrorURL$gFFM_FileName"
    get "$MirrorURL" "$gFFM_FileName" "$gFFM_OutFile"
    if [ -e "$gFFM_OutFile" ]; then
      echo "successfully downloaded file from mirror: $MirrorURL/$gFFM_FileName"
      return
    else
      echo "missing file '$gFFM_OutFile' in `pwd`" #D
    fi
  done
  echo "getFileFromMirror($gFFM_FileName) - ERROR: Cannot find file on mirror!"
  printCallerStack
}
function getNewestFile() {                  # URL_Prefix,WildCard,OutFile,DestDir   retrieves newest file from URL into common download folder + creates local symbolic link
  URL_Prefix="$1"
  WildCard="$2"
  OutFile="$3"
  DestDir="$4"

  # Note: make sure that findNewestDownload.pl is in search path!
  findNewest="$HOME/Source/TheToolChain/InstallData/scripts/findNewestDownload.pl"
  if [ ! -e "$findNewest" ]; then
    findFolderUpwards scripts
    findNewest="${FoundFolder}/findNewestDownload.pl"
  fi
  if [ ! -e "$findNewest" ]; then
    echo "getNewestFile($OutFile) - ERROR: Cannot locate findNewestDownload.pl in `pwd`"
    printCallerStack "    "
    exit 10
  fi
  $findNewest $URL_Prefix "$WildCard" tmp.GetNewest_URL tmp.GetNewest_File
  URL_Prefix=`cat tmp.GetNewest_URL`
  NewestFile=`cat tmp.GetNewest_File`
  NewestURL="${URL_Prefix}${NewestFile}"
  echo "found newest download: '$NewestURL'"
  
  if [ "$NewestURL" != "" ]; then
    getFile "$URL_Prefix" "$NewestFile" "$OutFile" "$DestDir"
  fi
}
function checkUserRoot() {                  # checks given user- and group-name if script is run by user root
  if [ "$USER" == "root" ]; then #{ check for required arguments
    TargetUser="$1"
    TargetGroup="$2"
  
    if [ "$TargetUser" == "" ]; then #{ print help + error + exit
      cat <<END_OF_ERROR
$0 [USERNAME [GROUP]]

USERNAME  chown all created files to this user (requires to run as user root)
GROUP     chown all created files to this group

ERROR: This script should not been run as user root without argument USERNAME!
END_OF_ERROR
      exit 10
    fi #}
    if [ ! -e "/home/$TargetUser" ]; then
      echo "$0 - ERROR: Cannot find home directory '/home/$TargetUser' !"
      printCallerStack "    "
      exit 10
    fi
    
    # set environment (other scripts might use them)
    export HOME="/home/$TargetUser"
    export USER="$TargetUser"
    
    if [ "$TargetGroup" != "" ]; then
      TargetUserAndGroup="$TargetUser:$TargetGroup"
    else
      TargetUserAndGroup="$TargetUser"
    fi
    #}
  else #{
    TargetUser="$USER"
    TargetGroup="`groups | awk '{ print \$1 }'`"
  fi #}
}
function setInstallDir() {                  # creates given directory and sets $Start_Path, $Install_Dir, $Install_Path accordingly 
  Install_Dir="$1"
  Option="$2"          # == "INSTALLDIR": will print Install_Path and exit (used by installAll.sh to query script for its install directory)
  
  if [ "$Option" == "INSTALLDIR" ]; then
    echo $Install_Dir
    exit 0
  fi
  Start_Path=`pwd`

  tryReuseOldInstallDir "$Install_Dir" # maybe we can safe lots of time by reusing the installation directory from an earlier installation of same version
  cd "$Start_Path"
  dir "$Install_Dir"
  cd "$Install_Dir"
  
  Install_Path=`pwd` # absolute directory path
} #
function tryReuseOldInstallDir() {          # tries to reuse install directory from older installation of same ToolChain version  
  InstallDir="$1"
  
  if [ ! -d "$InstallDir" ]; then #{ install directory does not exist: check if we can reuse old dir
    echo "install directory yet does not exist: $InstallDir"
    getToolChainVersion
    TTC_DirNew="$HOME/Source/TheToolChain_$VersionTTC"
    TTC_DirOld="${TTC_DirNew}.old"
    if [ -d "$TTC_DirOld" ]; then #{ old toolchain of same version exists: check for its corresponding install directory
      echo "old toolchain of same version exists: $TTC_DirOld"
      OldInstallDir="$TTC_DirOld/InstallData/$InstallDir"
      if [ -d "$OldInstallDir" ]; then #{ corresponding install dir in old installation exists: check if we can reuse it
        echo "corresponding install dir in old installation exists: $OldInstallDir"
        InstallScript=`perl -e "my \\\$P=rindex('$0', '/'); print substr('$0', \\\$P+1);"` # script is everything after last / character
        InstallScriptOld="$TTC_DirOld/InstallData/$InstallScript"
        echo "checking if '$InstallScript' -nt '$InstallScriptOld'"
        if [ ! "$InstallScript" -nt "$InstallScriptOld" ]; then
          echo "installscript was not modified since previous install: $InstallScript"
          echo "reusing old installdir: $OldInstallDir"
          
          # must force recreation of documentation files to fill new Documentation/ folder
          rm -f $OldInstallDir/OK.Documentation
          
          mv -v $OldInstallDir .
        else
          echo "installscript has been modified, cannot reuse old install dir"
        fi
      fi #}
    fi #}
  else
    echo "install directory already exists: $InstallDir"
  fi #}
} #
function enableFeature() {                  # NAME                                  creates file extensions.active/feature.<NAME> for given <NAME> and all depending features
  pF_Name="$1"
  
  if [ "$Dir_ExtensionsActive" == "" ]; then
    Dir_ExtensionsActive="extensions.active"
  fi

  echo -n "enableFeature($pF_Name) "
  printCallerStack "    "

  if [ "1" == "0" ]; then #{ activating directly
    pF_File="$Dir_ExtensionsActive/feature.${pF_Name}"
    echo "$0: providing feature '$pF_File'"
    echo "$0" >>$pF_File
  #}
  else                    #{ using _/enableFeature.pl to activate all depending features too
    _/enableFeature.pl "$pF_Name"
    printCallerStack "    "
    if [ "$?" != "0" ]; then
      printCallerStack "    "
    fi

  fi #}
} #
function provideFeature() {                 # NAME                                  DEPRECATED: Use enableFeature() instead!
  echo "$0 - WARNING: provideFeature() is deprecated. Use enableFeature() instead (simply rename it)!"
  enableFeature $@
}
function checkFeature() {                   # NAME [,DESCRIPTION]                   checks if named feature has been provided before
# Usage Example:
#
# checkFeature "450_cpu_cortexm3" "CPU driver for ARM CortexM3 based microcontrollers"
# if [ "$?" == "1" ]; then # feature available: activate this extension
#   ...
# fi
#

  Dependency="$1"
  Description="$2" # used by createNewProject.pl
  
  if [ "$Dependency" == "" ]; then
    return 1 # empty dependency is always provided
  fi
  
  if [ "$Dir_ExtensionsActive" == "" ]; then
    Dir_ExtensionsActive="extensions.active"
  fi

  cF_Feature=`ls 2>/dev/null $Dir_ExtensionsActive/*${Dependency}*`
  if [ "$cF_Feature" != "" ]; then
    #echo "found feature '$cF_Feature'"
    return 1
  fi
  
  #echo "$0: missing feature '$Dependency'"
  return 0
} #

#}Extra functions
