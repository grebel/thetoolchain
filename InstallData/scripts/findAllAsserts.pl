#!/usr/bin/perl
#
#          The ToolChain
#
# Script written by Gregor Rebel 2014
#

if (! -d "ttc-lib") {
  print <<"END_OF_HELP";
$0 - ERROR: Cannot find ttc-lib/

This script must be run from inside your project folder!

END_OF_HELP
  exit 10
}

print "# $0 - Assert_XXX() macros collected on ".`date`."\n"; 

my $Hits = `find ttc-lib/ -follow -name "*.h" -exec grep -HsR "#ifndef TTC_ASSERT_" "{}" \\;`;
$Hits .= " ".`find ./ -follow -maxdepth 1 -name "*.h" -exec grep -HsR "#ifndef TTC_ASSERT_" "{}" \\;`;
my @Hits = grep { $_; } map { strip($_); } split("\n", $Hits);
my %AllAsserts;
map { # fill %AllAsserts with all assert definitions and their filename
  my $Line = $_;                        # ./ttc-lib/ttc_network_types.h:#ifndef TTC_ASSERT_NETWORK    // any previous definition set (Makefile)?
  
  # ./ttc-lib/ttc_interrupt_types.h:    #ifndef TTC_ASSERT_INTERRUPT    // any previous definition set (Makefile)?
  $Line =~ m/(\S+)\:\s*\#ifndef\s(\S+)\s*.*/;
  my $FileName = $1;
  my $Assert   = $2;
  
  if ( (index($Assert, '<') == -1) &&            # avoid adding template definition TTC_ASSERT_<DEVICE>
       (index($FileName, 'DEPRECATED') == -1) && # avoid adding deprecated files
       (substr($Assert, -2) ne '_H')             # avoid include guards (especially TTC_ASSERT_TYPES_H)
    ) {
    unless ($AllAsserts{$Assert}) { $AllAsserts{$Assert} = [] };
    push @{ $AllAsserts{$Assert} }, $FileName;
  }
} @Hits;

my $MaxLength = 0;
map { # find $MaxLength = maximum length of COMPILE_OPTS line
  my $Statement = "COMPILE_OPTS += -D$_=0";
  if ( $MaxLength < length($Statement) ) { $MaxLength = length($Statement); }
} keys %AllAsserts;

my $Spaces = '';
while (length($Spaces) < $MaxLength) { $Spaces .= ' '; }

map { # print out formatted COMPILE_OPTS lines
  my $Statement = "COMPILE_OPTS += -D$_=0";
  my @Files = @{ $AllAsserts{$_} };
  print substr($Statement.$Spaces, 0, $MaxLength).'    # used in: '.join(" ", @Files)."\n";
} sort keys %AllAsserts;
sub strip {                       # removes invisible characters at begin + end of given string
  my $Line = shift || "";

  my %IsWhiteChar = ( ' ' => 1, "\n" => 1, "\r" => 1, "\t" => 1 ); 

  $Line =~ s/^\s+//;
  $Line =~ s/\s+$//;
  #$Line =~ s/(?:^ +)||(?: +$)//g;
  #$Line =~ s/^\ *([A-Z,a-z,0-9]*)\ *$/\1/g;
  #$Line =~ s/^\s+//;
  #$Line  =~ s/\s+$//;
  return $Line;
  
  my $lIndex = 0;
  while ( $IsWhiteChar{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsWhiteChar{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  return substr($Line, $lIndex, $rIndex - $lIndex + 1);

}
