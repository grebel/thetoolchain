#!/bin/bash

# todos.sh  - find and list all lines containing string "ToDo" in current folder and all subfolders
# 
# written by Gregor Rebel 2012

grep -RHnT ToDo * 2>/dev/null | less
