#!/bin/bash

#{ parse arguments
MAIN_C=""
NOLOG=""
NOFLASH=""
NOTODOS=""

for I in 1 2 3 4; do
if [ "$1" == "NOLOG" ];   then #{
  NOLOG="NOLOG "
  shift >/dev/null
fi #}
if [ "$1" == "NOFLASH" ]; then #{
  NOFLASH="NOFLASH "
  shift >/dev/null
fi #}
if [ "$1" == "NOTODOS" ]; then #{
  NOTODOS="NOTODOS "
  shift >/dev/null
fi #}
done
MAIN_C="$1"

PWD=`pwd`
echo "$PWD> $0 $NOLOG$NOFLASH$NOTODOS$MAIN_C"
#}

source _/SourceMe.sh
source _/installFuncs.sh
if [ -e extensions.active/makefile.060_compiler_parallel_make ]; then
  echo "activating parallel compilation using $AmountCPUs processes"
  AmountOfMakeJobs="$AmountCPUs"
else
  AmountOfMakeJobs="1"
fi

AutomaticHeapName="000_max_heap"
AutomaticHeapMakefile="extensions.active/makefile.$AutomaticHeapName"
function compileSources() {
  if [ "$1" == "LOGFILE" ]; then
    LOGFILE="1"
  else
    LOGFILE=""
  fi
  
  # ensure that debug.sh will reflash next time
  rm -f flash.log 2>/dev/null
  if [ -e kill_gdb.sh ]; then #{ killing debugger
    echo "killing debugger..."
    bash ./kill_gdb.sh
    rm -f kill_gdb.sh 2>/dev/null
  fi #}
  
  #{ compile ttc_extensions_active.c/.h
  
  cd extensions.active/
  cat <<END_OF_HEADER >makefile #{
#
#                               TheToolChain
#
# Central makefile.
# This makefile was created by concatenatenating all makefile.* files in this folder.
# 
# This file has been automatically created by $0 on `date`.
# ALL CHANGES MADE TO THIS FILE WILL GET LOST!
#

END_OF_HEADER
#}
  find makefile\.* -exec cat {} >>makefile \;
  cd ..
  
  cat <<END_OF_SOURCE >ttc_extensions_active.c #{ create ttc_extensions_active.c
/** ttc_extensions_active.c - initialization of all activated extensions
 *
 * This file was created from a concatenate of all extensions.active/source.*.c files by $0
 *
 */

#include "ttc_extensions_active.h"

/** Calls initialize functions of activated extensions
 *
 * This function is called from ttc_extensions.c BEFORE the multitasking scheduler 
 * is started. You may create as many tasks as you like. The created tasks are run 
 * AFTER ttc_extensions_prepare() returns.
 */
void ttc_extensions_prepare() {

END_OF_SOURCE

  cat <<END_OF_SOURCE >ttc_extensions_active.h
/** ttc_extensions_active.h - initialization of all activated extensions
 *
 * This file was created from a concatenate of all source.*.c files by $0
 *
 */

#ifndef ttc_extensions_active_H
#define ttc_extensions_active_H

#include "ttc-lib/ttc_basic.h"

// starts all activated extensions
void ttc_extensions_prepare();

END_OF_SOURCE

  echo "" >source.c
  rm -f extensions.active/source\.\.c # workaround: this file sometimes appear for unknown reason (ToDo: fix)
  find extensions.active/source\.*\.c -exec cat {} >>source.c \;
  grep -v "#include" source.c >>ttc_extensions_active.c
  grep -h "#include" source.c >>ttc_extensions_active.h
  
  cat <<END_OF_SOURCE >>ttc_extensions_active.c

}

END_OF_SOURCE
  cat <<END_OF_SOURCE >>ttc_extensions_active.h

#endif //ttc_extensions_active_H

END_OF_SOURCE

#} create ttc_extensions_active.c
  
  # cd ..
  #}cd extensions.active/
  Projects=`ls QtCreator/*.project`
  if [ "$Projects" != "" ]; then
    for Project in $Projects; do
      ProjectName=`perl -e "print substr('$Project', 0, -8);"`
      echo "found project '$ProjectName'"
      
      # create project files (if missing)
      touch ${ProjectName}.files
      echo `pwd` >${ProjectName}.includes
      for Suffix in config creator includes; do
        if [ ! -e "${ProjectName}.$Suffix" ]; then
          cp -v "$HOME/Source/TheToolChain/Template/QtCreator/TemplateName.$Suffix" "${ProjectName}.$Suffix"
        fi
      done
    done
  fi
  ProjectFiles=`ls QtCreator/*.files`
  if [ "$ProjectFiles" != "" ]; then
    for ProjectFile in $ProjectFiles; do
      echo "`pwd`> _/updateProjectFiles.pl $ProjectFile ../ >${ProjectFile}.log"
      _/updateProjectFiles.pl $ProjectFile ../ >${ProjectFile}.log || Error="1"
      if [ "$Error" == "1" ]; then
        printError "Error occured while updating project '$ProjectFile'" 
        cat >&2 ${ProjectFile}.log 
      else
        #X (Creating this symlink interferes with other projects using the same ToolChain!) ln -svf `pwd`/compile_options.h ttc-lib/
        cd QtCreator # entries in project files are relative to this directory
        echo "formatting source files (log in `pwd`/formatSources.log) ..."
        ../_/formatSources.pl  "../$ProjectFile" >formatSources.log 2>&1
        cd ..
      fi
    done
  else
    printError "Cannot find any QtCreator projects in `pwd`/QtCreator!"
    exit 20
  fi
  if [ "$Error" == "1" ]; then
    printError "Compilation aborted."
    exit 10
  fi
  
  Log="compile.log"
  CompileOK=""
  echo "compiling with `bin/compiler -v 2>&1 | grep 'gcc version'` ..."

  export PATH="./bin/:$PATH"
  echo "pwd=`pwd`'"
  ls -l main.c
  make clean >/dev/null 2>&1
  if [ "$LOGFILE" == "1" ]; then
    #make all -j$AmountOfMakeJobs >$Log 2>&1 && CompileOK="1"
    cmd "make all -j$AmountOfMakeJobs" "$Log"
    if [ "$CMD_ERROR" != "" ]; then
      printError "$CMD_ERROR"
    else
      CompileOK="1"
    fi
    grep ToDo "$Log" >"${Log}.todos"
  else
    if [ "$NOLOG" != "" ]; then
      #make all -j$AmountOfMakeJobs && CompileOK="1"
      cmd "make all -j$AmountOfMakeJobs" "" 
      if [ "$CMD_ERROR" != "" ]; then
        echo "$CMD_ERROR" >compile.err
        echo ""
        echo "> cat compile.err"
        cat compile.err $Grep
        #X printError "$CMD_ERROR"
        printError "Errors occured during compilation!"
      else
        CMD="cat stdout.log stderr.log"
        echo "> $CMD"
        if [  "$NOTODOS" != "" ]; then
          $CMD | grep -v ToDo
        else
          $CMD
        fi
        CompileOK="1"
      fi      
    else
      #make all -j$AmountOfMakeJobs >$Log 2>&1 && CompileOK="1"
      cmd "make all -j$AmountOfMakeJobs" "$Log" "QUIET"
      if [ "$CMD_ERROR" != "" ]; then
        echo "$CMD_ERROR" >compile.err 
        echo ""
        echo "> cat compile.err"
        cat compile.err $Grep
        #X printError "$CMD_ERROR"
        printError "Errors occured during compilation!"
      else
        CMD="cat stdout.log stderr.log"
        echo "> $CMD"
        if [  "$NOTODOS" != "" ]; then
          $CMD | grep -v ToDo
        else
          $CMD
        fi
        CompileOK="1"
      fi
    fi
  fi
}

if [ "$USER" == "root" ]; then #{
  printError "You should never start a compile process as user root. This would cause temporary files to be owned by root that are not accessible by normal user!"
  exit 10
fi #}
if [ "$MAIN_C" != "" ]; then        #{ create link main.c
  if [ ! -e "$MAIN_C" ]; then
    printError "Cannot find main-file '$MAIN_C'!" "Available candidates: `pwd`/`ls -C *.c`"
    exit 6
  fi
  if [ "$MAIN_C" != "main.c" ]; then
    if [ -L main.c ]; then
      rm 2>/dev/null main.c # remove symbolic link
    else
      printError "main.c is not a symbolic link. Move it aside or rename it if you want to compile another main c-file!"
      exit 10
    fi
    createLink $MAIN_C main.c
  fi
fi #}
if [ ! -e main.c ]; then            #{ check if main.c exists
  printError "No main.c selected!" "Available candidates: `pwd`/`ls -C *.c`"
  exit 5
fi #}
if [ ! -e compile_options.h ]; then #{ ensure this dynamic file exists
  touch compile_options.h
fi #}
if [ ! -e configs/memory_current.ld ]; then #{ ensure this dynamic file exists
  touch configs/memory_current.ld
fi #}

# creating link to our extensions-folder into ttc-lib helps IDE to find include files
ln -svf `pwd`/extensions.active ttc-lib/

compileSources

if [ "$CompileOK" == "1" ]; then #{ create .S, .bin, .elf files
    if [ "$NOLOG" == "" ]; then #{ check log file for warnings + display with less
  
      if [ "$NOTODOS" == "" ]; then
        grep -v warning $Log | grep -- "-warning" >compile.warnings
      else
        grep -v warning $Log | grep -v ToDo | grep -- "-warning" >compile.warnings
      fi
      Warnings=`cat compile.warnings`
      if [ "$Warnings" != "" ]; then
        cat <<END_OF_WARNINGS >>compile.warnings

Warnings occured. You should try to avoid these warnings. Sometimes a warning hints to a real problem.
Good code does compile without warnings.

Type Q to quit from less.
END_OF_WARNINGS
        LESS="-RX +/warning:"
        less compile.warnings
        
      fi
    fi #}
    
    for Binary in `ls *.elf`; do
      echo "creating assembly source of $Binary"
      bin/arm-none-eabi-objdump -S --disassemble "$Binary" > "$Binary.S"
      
      echo "creating symboltable of $Binary"
      bin/arm-none-eabi-objdump -t main.elf | sort >"$Binary.symbols"
    done
    
    echo ""
    echo "Compiled successfully *************************************************"
    ls -lh *.bin *.elf *.S *.symbols
    echo ""
fi #}
if [ "$CompileOK" != "1" ]; then #{ ERROR occured during compilation
    echo "ERROR occured during compilation!"
      cat <<END_OF_HINT >>$Log

Rerunning make to show last error **********************************************************************************
      
END_OF_HINT
    
   #make  >>$Log 2>&1
   cmd "make" "$Log"
   if [ "$CMD_ERROR" != "" ]; then
      printError "$CMD_ERROR"
    else
      CompileOK="1"
    fi
    if [ "$NOLOG" == "" ]; then #{
      cat <<END_OF_HINT >>$Log

Hint: You may run an analyze script on this log file to find out more about it!
      _/analyzeLogFile.pl $Log
      
END_OF_HINT
      LESS="-RX +/makefile:|make:|ld:|collect2:|error:|undefined reference|multiple definition|Assembler messages:";
      ${PAGER:-less} $Log
    fi #}
    exit 10
fi #}

echo ""
