#!/bin/bash
#
# Start OpenOCD GDB server + connect to it
#
# written by Fernando Carmona Varo, Gregor Rebel 2012-2016

if [ "$1" == "DDD" ]; then
  UseDebugger="$1"
  shift
fi
if [ "$1" == "KDBG" ]; then
  UseDebugger="$1"
  shift
fi
EXTRA_CFG="$1"; shift
PORT_GDB="$1";  shift

echo "$0 EXTRA_CFG='$EXTRA_CFG' PORT_GDB='$PORT_GDB'"

if [ "$PORT_GDB" == "" ]; then
  PORT_GDB="3333"
fi

if [ "$EXTRA_CFG" == "-h" ]; then #{
  cat <<END_OF_HELP
$0 [DDD/S] [EXTRA.CFG] [GDB_PORT]

DDD         if given: graphic debugger ddd will be used instead of text only gdb
EXTRA.CFG   if given: path to extra openocd configuration file to use
GDB_PORT    if given: GDB port on local machine to connect to (default: 3333)

END_OF_HELP
  exit 0
fi #}

if [ "$EXTRA_CFG" != "" ]; then
  if [ ! -e $EXTRA_CFG ]; then
    echo "$0 - ERROR: Cannot find extra openocd configuration file '$EXTRA_CFG' (PWD=`pwd`)"
    exit 10
  fi
  SuffixGDB=`perl -e "my \\$I=rindex('$EXTRA_CFG', '_'); my \\$B = substr('$EXTRA_CFG', \\$I+1); my \\$J=index(\\$B, '.'); print substr(\\$B, 0, \\$J);"`
 #X  SuffixGDB=`perl -e "my \\$P=rindex('$EXTRA_CFG', '_'); print substr('$EXTRA_CFG', \\$P+1);"`
fi

[ -e required_version ] || { echo "You need to be in the project directory"; exit 1;}

[ $TERM != "dumb" ] && echo -e "\e[33m** Starting OpenOCD as GDB-Server...\e[0m"
_/gdb_server.sh $EXTRA_CFG $SuffixGDB
if [ "$?" != "0" ]; then
  echo "$0: openocd exited, with error code $?"
  exit 10
fi


[ $TERM != "dumb" ] && echo -e "\e[33m** Starting GDB as OpenOCD client...\e[0m"
#
echo "$0: _/gdb.sh \"$SuffixGDB\" \"$PORT_GDB\" \"main.elf\" \"$UseDebugger\" \"$@\" $EXTRA_CFG"
_/gdb.sh "$SuffixGDB" "$PORT_GDB" "main.elf" "$UseDebugger" "$@" $EXTRA_CFG

