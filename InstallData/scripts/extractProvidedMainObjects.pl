#!/usr/bin/perl

my $FilePath = shift(@ARGV);

unless ($FilePath) {
  print <<"END_OF_HELP";
$0 MAKEFILE

Extracts and displays all object files appended to MAIN_OBJS from given makefile.

END_OF_HELP
  exit 5;
}
my $Makefile = readFile($FilePath);
$Makefile =~ s/\\\n//g;
my @Lines = split("\n", $Makefile);
@Lines = grep { $_; }
         map {
               my $Pos = index($_, '+=');
               if ($Pos > -1) { substr($_, $Pos + 2); }
               else           { undef; }
             }
         grep { index($_, 'MAIN_OBJS') > -1; } @Lines;
my $AllObjects = join(" ", @Lines);
$AllObjects =~ s/\n/ /g;
my @AllObjects = split(" ", $AllObjects);
if (@AllObjects) {
  print join(" ", @AllObjects)."\n";
}

sub readFile {          # read in content of given file
  my $FilePath = shift;
  
  my $Content;
  open(IN, "<$FilePath") or die ("$0 - ERROR: Cannot read from file '$FilePath' ($!)");
  while (<IN>) { $Content .= $_; }
  close(IN);
  
  return $Content;
}
