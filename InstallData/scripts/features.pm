#
# Database of all features and their dependencies.
#
# This file is part of TheToolChain written by Gregor Rebel 2010-2018.
#
# _/features.pm last updated: Fr 13. Apr 12:16:35 UTC 2018 by gregor@hlb21
#
# Execute this inside a project folder to update this file:
#   _/enableFeature.pl UPDATE 

package Features;

use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = qw(loadData);
@EXPORT_OK   = qw(loadData);
%EXPORT_TAGS = ( DEFAULT => [qw(&loadData)],
                 Both    => [qw(&loadData)]
               );

sub loadData {
  my %Dependencies =
  ( #{ features that will enable depending features too
    
#                                              Note: [ Unknown features will be removed on next UPDATE! ] 
      '110_board_dso_0138'               => [  ],
      '110_board_olimex_stm32_lcd'       => [  ],
      '110_board_olimex_stm32_p107_revA' => [  ],
      '110_board_sensor_dwm1000'         => [  ],
      '110_board_stm32l053_discovery'    => [  ],
      '110_board_stm32l100c_discovery'   => [  ],
      '110_board_stm32l152_discovery'    => [  ],
      '450_accelerometer_bno055'         => [  ],
      '450_accelerometer_lis3lv02dl'     => [  ],
      '450_accelerometer_mpu6050'        => [  ],
      '450_adc_stm32f1xx'                => [  ],
      '450_adc_stm32l1xx'                => [  ],
      '450_basic_cm3'                    => [  ],
      '450_basic_stm32f1xx'              => [  ],
      '450_basic_stm32f30x'              => [  ],
      '450_basic_stm32l0xx'              => [  ],
      '450_basic_stm32l1xx'              => [  ],
      '450_cpu_cortexm0'                 => [  ],
      '450_cpu_cortexm3'                 => [ #{ ],
                                                       '450_interrupt_cortexm3',
                                                       '450_systick_cortexm3'
                                                     ], #}
      '450_cpu_cortexm4'                 => [  ],
      '450_cpu_stm32f1xx'                => [ #{ ],
                                                       '450_adc_stm32f1xx',
                                                       '450_basic_stm32f1xx',
                                                       '450_gpio_stm32f1xx',
                                                       '450_i2c_stm32f1xx',
                                                       '450_interrupt_stm32f1xx',
                                                       '450_memory_stm32f1xx',
                                                       '450_register_stm32f1xx',
                                                       '450_spi_stm32f1xx',
                                                       '450_sysclock_stm32f1xx',
                                                       '450_timer_stm32f1xx',
                                                       '450_usart_stm32f1xx',
                                                       '450_usb_stm32f1xx'
                                                     ], #}
      '450_cpu_stm32f2xx'                => [  ],
      '450_cpu_stm32f3xx'                => [  ],
      '450_cpu_stm32f4xx'                => [  ],
      '450_cpu_stm32l0xx'                => [ #{ ],
                                                       '450_basic_stm32l0xx',
                                                       '450_gpio_stm32l0xx',
                                                       '450_memory_stm32l0xx',
                                                       '450_register_stm32l0xx',
                                                       '450_spi_stm32l0xx',
                                                       '450_sysclock_stm32l0xx'
                                                     ], #}
      '450_cpu_stm32l1xx'                => [ #{ ],
                                                       '450_adc_stm32l1xx',
                                                       '450_basic_stm32l1xx',
                                                       '450_dac_stm32l1xx',
                                                       '450_dma_stm32l1xx',
                                                       '450_gpio_stm32l1xx',
                                                       '450_i2c_stm32l1xx',
                                                       '450_interrupt_stm32l1xx',
                                                       '450_memory_stm32l1xx',
                                                       '450_pwm_stm32l1xx',
                                                       '450_pwr_stm32l1xx',
                                                       '450_register_stm32l1xx',
                                                       '450_spi_stm32l1xx',
                                                       '450_sysclock_stm32l1xx',
                                                       '450_timer_stm32l1xx',
                                                       '450_usart_stm32l1xx'
                                                     ], #}
      '450_cpu_stm32w1xx'                => [ #{ ],
                                                       '450_gpio_stm32w1xx',
                                                       '450_interrupt_stm32w1xx',
                                                       '450_memory_stm32w1xx',
                                                       '450_radio_stm32w1xx',
                                                       '450_register_stm32w1xx',
                                                       '450_spi_stm32w1xx',
                                                       '450_sysclock_stm32w1xx',
                                                       '450_timer_stm32w1xx',
                                                       '450_usart_stm32w1xx'
                                                     ], #}
      '450_crc_fast'                     => [  ],
      '450_crc_small'                    => [  ],
      '450_dac_stm32l1xx'                => [  ],
      '450_dma_stm32l1xx'                => [  ],
      '450_ethernet_ste101p'             => [  ],
      '450_fake_sbrk_support'            => [  ],
      '450_filesystem_dosfs'             => [  ],
      '450_gfx_ili93xx'                  => [  ],
      '450_gpio_stm32f1xx'               => [  ],
      '450_gpio_stm32f30x'               => [  ],
      '450_gpio_stm32l0xx'               => [  ],
      '450_gpio_stm32l1xx'               => [  ],
      '450_gpio_stm32w1xx'               => [  ],
      '450_gyroscope_mpu6050'            => [  ],
      '450_heap_freertos'                => [  ],
      '450_heap_zdefault'                => [  ],
      '450_i2c_stm32f1xx'                => [  ],
      '450_i2c_stm32l1xx'                => [  ],
      '450_input_touchpad'               => [  ],
      '450_interface_ste101p'            => [  ],
      '450_interrupt_cortexm3'           => [  ],
      '450_interrupt_stm32f1xx'          => [  ],
      '450_interrupt_stm32l1xx'          => [  ],
      '450_interrupt_stm32w1xx'          => [  ],
      '450_math_software_double'         => [  ],
      '450_math_software_float'          => [  ],
      '450_memory_stm32f1xx'             => [  ],
      '450_memory_stm32l0xx'             => [  ],
      '450_memory_stm32l1xx'             => [  ],
      '450_memory_stm32w1xx'             => [  ],
      '450_network_IEEE_802_15_4'        => [  ],
      '450_network_layer_usart'          => [  ],
      '450_packet_802154'                => [  ],
      '450_pwm_stm32l1xx'                => [  ],
      '450_pwr_stm32l1xx'                => [  ],
      '450_radio_cc1101'                 => [  ],
      '450_radio_dw1000'                 => [  ],
      '450_radio_stm32w1xx'              => [  ],
      '450_register_stm32f1xx'           => [  ],
      '450_register_stm32f30x'           => [  ],
      '450_register_stm32l0xx'           => [  ],
      '450_register_stm32l1xx'           => [  ],
      '450_register_stm32w1xx'           => [  ],
      '450_rtls_crtof_simple_2d'         => [  ],
      '450_rtls_square4'                 => [  ],
      '450_sdcard_spi'                   => [  ],
      '450_slam_simple_2d'               => [  ],
      '450_spi_stm32f1xx'                => [  ],
      '450_spi_stm32f30x'                => [  ],
      '450_spi_stm32l0xx'                => [  ],
      '450_spi_stm32l1xx'                => [  ],
      '450_spi_stm32w1xx'                => [  ],
      '450_string_ascii'                 => [  ],
      '450_sysclock_stm32f1xx'           => [  ],
      '450_sysclock_stm32l0xx'           => [  ],
      '450_sysclock_stm32l1xx'           => [  ],
      '450_sysclock_stm32w1xx'           => [  ],
      '450_systick_cortexm3'             => [  ],
      '450_systick_freertos'             => [  ],
      '450_tcpip_uip'                    => [  ],
      '450_timer_stm32f0xx'              => [  ],
      '450_timer_stm32f1xx'              => [  ],
      '450_timer_stm32l1xx'              => [  ],
      '450_timer_stm32w1xx'              => [  ],
      '450_touchpad_analog4'             => [  ],
      '450_usart_stm32f1xx'              => [  ],
      '450_usart_stm32l1xx'              => [  ],
      '450_usart_stm32w1xx'              => [  ],
      '450_usb_stm32f1xx'                => [  ],
   ); #}

  return %Dependencies;
}

1;

