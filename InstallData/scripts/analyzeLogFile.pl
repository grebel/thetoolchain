#!/usr/bin/perl
#
# analyzeLogFile.pl  written by Gregor Rebel 2012-2013
#
# scans given compile log and tries to give hints how to solve warnings and errors
#
# This source is published under terms of LGPL (Lesser GNU Public license)
#

my $CompileLog = shift(@ARGV);

unless ($CompileLog) {
  print <<"END_OF_HELP";
$0 COMPILE_LOG

Analyzes given gcc compilation log-file and tries to help solving warnings and errors.

written by Gregor Rebel 2012

END_OF_HELP
  exit 0;  
}

if (substr($CompileLog,0,1) ne '/') {
  my $PWD = `pwd`;
  chop($PWD);
  $CompileLog = $PWD.'/'.$CompileLog;
}
unless (-e $CompileLog) {
  die "ERROR: Cannot find log file '$CompileLog'";
}

my %Analyzers = ( # { registered analyzers for each compile stage
 assemble => [ ],
     link => [ ],
  compile => [ ],
  ); #}
my %GlobalErrorTypes = ( General => 'error:', # { each analyzer can register error types and search patterns here
                       ); #}

map {
  print "registered analyzer ".$_->('register')."\n";
} (analyze_Compiler_NoSuchFile, analyze_Linker_UndefinedReference); 

my @Hints;
my @CompileLog;
open(IN, "<$CompileLog") or die("ERROR: Cannot read from log-file '$CompileLog' ($!)");
@CompileLog = <IN>;
close(IN);
  
my $AmountOfLines = scalar @CompileLog;
print "$0 - Analyzing log file '$CompileLog' ($AmountOfLines lines)\n";

my $Compiler = "compiler"; # arm-none-eabi-gcc

my %Stages = ( Compile  => [], #{
               Assemble => [],
               Link     => []
             ); #}
my @Stages; # all stages in order of their appearance

my $LineNo = 0;
# extract compiler runs from different compile stages
map { # @CompileLog -> parseCompilerLine() -> %Stages, @Stages
  $LineNo++;
  if (substr($_, 0, length($Compiler)) eq $Compiler) { 
    my $StageDataRef = parseCompilerLine($_);
    $StageDataRef->{FirstLine} = $LineNo + 1;
    push @Stages, $StageDataRef;
  }
} @CompileLog;
for (my $Index = 0; $Index < scalar @Stages; $Index++) {
  my $LastLine = scalar @CompileLog;
  if ($Index + 1 < scalar @Stages) { $LastLine = $Stages[$Index+1]->{FirstLine}-2; }
  $Stages[$Index]->{LastLine} = $LastLine;
  #print "stage ".$Stages[$Index]->{Stage}." ".$Stages[$Index]->{FirstLine}."-".$Stages[$Index]->{LastLine}."\n"; #D
}
map { parseStage($_, \@CompileLog); } @Stages;

if (@Hints) {
  print "\nHints:\n  ".join("\n  ", @Hints)."\n";
}

my %MissingFiles;
sub analyze_Compiler_NoSuchFile {       # analyzes error "No such file or directorye" during compile stage
  if ($_[0] eq 'register') {
    $GlobalErrorTypes{NoSuchFile} = 'No such file or directory';
    push @{ $Analyzers{compile} }, analyze_Compiler_NoSuchFile;
    return "Compiler_NoSuchFile";
  }
  
  my $StageDataRef  = shift; # hash-ref:  all data of one stage
  my $CompileLogRef = shift; # array-ref: all lines from compile log
  my $ErrorsRef     = shift; # hash-ref:  return value from getErrors()  
  my $WarningsRef   = shift; # array-ref: return value from getWarnings()
  
  print "source='".$StageDataRef->{InFile}."'\n";
  
  foreach my $Error (@{ $ErrorsRef->{NoSuchFile} }) {
    my $Pos = index($Error->{Error}, $ErrorTypes{NoSuchFile}) + length($ErrorTypes{NoSuchFile});
    (my $SourceFile, my $LineNo, my $Error, my $MissingFile, my $ErrorMessage) = split(":",$Error->{Error});
    $SourceFile   = strip($SourceFile);
    $LineNo       = strip($LineNo);
    $Error        = strip($Error);
    $MissingFile  = strip($MissingFile);
    $ErrorMessage = strip($ErrorMessage);
    
    unless ($MissingFiles{$MissingFile}) { # this undefined reference is new
      $MissingFiles{$MissingFile} = 1;
      my $Suffix = lc( substr($MissingFile, rindex($MissingFile, '.')) );
      my $Type;
      if ($Suffix eq '.h')   { $Type = 'header';     }
      if ($Suffix eq '.c')   { $Type = 'c-source';   }
      if ($Suffix eq '.s')   { $Type = 'assembler';  }
      if ($Suffix eq '.cpp') { $Type = 'c++-source'; }
      
      print "    compiler misses $Suffix ($Type) file '".$MissingFile."'\n";
      my @FirstCandidateFiles = findFiles(['*'.$MissingFile], $StageDataRef->{IncludePaths});
      if (@FirstCandidateFiles) {
        map {
          print "      => candidate: $_\n";
        } @FirstCandidateFiles;
      }
      else {
        print "    => no first candidates found.\n";
      }
      if (0) {
        my @DefinitionFiles = findDefinitionFile($StageDataRef, $MissingFile);
        map { 
          print "\n        definition $MissingFile found in: '".$_->{File}."': \n".
                "                   '$_->{Match}'";
          if ($_->{Hint}) { push @Hints, split("\n", $_->{Hint}); }
  
        } @DefinitionFiles; 
        unless (@DefinitionFiles) { print ": NO DEFINITION FOUND!"; }
        print "\n";
      }
      
      if (0) { print "'".$Error->{Error}."'\n"; }
    }
  }
  #print "\n";
}
my %UndefinedReferences;
sub analyze_Linker_UndefinedReference { # analyzes error "undefined reference" during link stage
  if ($_[0] eq 'register') {
    $GlobalErrorTypes{UndefinedReference} = 'undefined reference to';
    push @{ $Analyzers{link} }, analyze_Linker_UndefinedReference;
    return "Linker_UndefinedReference";
  }
  
  my $StageDataRef  = shift; # hash-ref:  all data of one stage
  my $CompileLogRef = shift; # array-ref: all lines from compile log
  my $ErrorsRef     = shift; # hash-ref:  return value from getErrors()  
  my $WarningsRef   = shift; # array-ref: return value from getWarnings()
  
  print "outfile='".$StageDataRef->{OutFile}."'\n";
  
  foreach my $Error (@{ $ErrorsRef->{UndefinedReference} }) {
    my $Pos = index($Error->{Error}, $GlobalErrorTypes{UndefinedReference}) + length($GlobalErrorTypes{UndefinedReference});
    my $UndefinedReference = stripBrackets( strip(substr($Error->{Error}, $Pos)) );
    
    unless ($UndefinedReferences{$UndefinedReference}) { # this undefined reference is new
      $UndefinedReferences{$UndefinedReference} = 1;
      print "    linker misses reference '".$UndefinedReference."': searching for definitions...";
      my @DefinitionFiles = findDefinitionFile($StageDataRef, $UndefinedReference);
      map { 
        print "\n        definition $UndefinedReference found in: '".$_->{File}."': \n".
              "                   '$_->{Match}'";
        if ($_->{Hint}) { push @Hints, split("\n", $_->{Hint}); }

      } @DefinitionFiles; 
      unless (@DefinitionFiles) { print ": NO DEFINITION FOUND!"; }
      print "\n";
      
      if (0) { print "'".$Error->{Error}."'\n"; }
    }
  }
}

my %FoundObjectFiles;
sub findDefinitionFile { # scans all files in source directories for a definition of given reference
  my $StageDataRef = shift; # hash-ref: all data of one stage
  my $Reference    = shift; # name of reference to find
  $Reference =~s/\`/\\`/g;
  
  my @DefinitionFiles;
  my @SearchDirs = @{ $StageDataRef->{IncludePaths} },
  my @AllFiles = findFiles(['*.c', '*.h', '*.s'], ["./"]);
  my @InterestingFiles = grep { strip($_); } 
  map { # grep $Reference from @AllFiles
    my $CMD = "grep 2>/dev/null -Hn \"$Reference\" \"$_\"";
    #print "> $CMD\n";
    `$CMD`;
  } @AllFiles;
  my $InterestingFiles = join("\n", @InterestingFiles); # one or more lines per array item
  @InterestingFiles = split("\n", $InterestingFiles);   # now each array entry has exactly one entry  

  # Example output
  # ./additionals/200_cpu_stm32w1xx/STM32W108/hal/micro/cortexm3/micro-common-internal.c:182:  halCommonGetMfgToken(&biasTrim, TOKEN_MFG_ANALOG_TRIM_BOTH);
  my %InterestingFiles = map { (my $File, my $LineNo, my $Match) = split(":", $_);  
                               (strip($File), {LineNo => strip($LineNo), Match => strip($Match)}); 
                             } @InterestingFiles;
  @InterestingFiles = sort keys %InterestingFiles; # now all doubles have been removed
  map { # @InterestingFiles;
    my $File = $_;
    my $LineNo = $InterestingFiles{$File}->{LineNo};
    my $Match  = $InterestingFiles{$File}->{Match};
    
    open(IN, "<$File") or die("ERROR: cannot read file '$File' ($!)");
    my @FileContent = <IN>;
    close(IN);
    my $FileContent = join("\n", @FileContent);
    my $OldPos    = 0;
    
    while ( index($FileContent, $Reference, $OldPos) > -1) {
      my $PosBefore = index($FileContent, $Reference, $OldPos) - 1;
      $OldPos = $PosBefore + 2;
      my $PosAfter = $PosBefore + length($Reference) + 1;
      my $StringAfterMatch = strip(substr($FileContent, $PosAfter));
      
      my $ExcludeFinding;
      if (!$ExcludeFinding &&  isInsideComment($FileContent, $PosBefore + 1) ) {
        $ExcludeFinding = 1;
      }
        
      if ( !$ExcludeFinding && (substr($StringAfterMatch, 0, 1) eq ')'))  # exclude FOO)
      { $ExcludeFinding = 1; }
      if ( !$ExcludeFinding && (substr($StringAfterMatch, 0, 1) eq '(') ) # FOO(
      {
        my $PosClosingBracket = findClosingBracket($StringAfterMatch); 
        my $StringAfterMatch2 = substr($StringAfterMatch, 0, $PosClosingBracket);
        my $StringAfterClosingBracket = strip( substr($StringAfterMatch, $PosClosingBracket) );
        my @Lines = split("\n", $StringAfterMatch2);
        $StringAfterMatch2 = join("", map { strip($_); } @Lines);
        my $NextCharacter = substr($StringAfterClosingBracket, 0, 1);
        
        if ($NextCharacter eq ';') { $ExcludeFinding = 1; }  # exclude foo(); 
        $Match = "$Reference$StringAfterMatch2 $NextCharacter";
      }
      if (!$ExcludeFinding && isNormalCharacter($StringAfterMatch)) { # exclude FOOx
       $ExcludeFinding = 1;
      }
      if ($ExcludeFinding) {
        #print "excluding $Match\n"; #D
      }
      
      unless ($ExcludeFinding) {
        my $LastDotPos = rindex($File, ".");
        my $LastSlashPos = rindex($File, "/");
        my $Suffix     = substr($File, $LastDotPos + 1); 
        my $ObjectFile = substr($File, $LastSlashPos + 1, $LastDotPos - $LastSlashPos).'o';
        my $VPATH      = substr($File, 0, $LastSlashPos + 1);
        my $Hint;
        unless ($FoundObjectFiles{$ObjectFile}) { # create hint for this object
          $FoundObjectFiles{$ObjectFile} = 1;
          $Hint .= "add to makefile: MAIN_OBJS += $ObjectFile\n";
          $Hint .= "add to makefile: VPATH \%.$Suffix $VPATH\n";
        }
        push @DefinitionFiles, {
          File  => $File,
          Match => $Match,
          Hint  => $Hint 
        };
      }
      #print "\n$File => \@$PosBefore: $Match  "; #D
    }
  } grep { $_; } @InterestingFiles;
  
  return @DefinitionFiles;  
}
sub findFiles {          # returns a list of all files matching given wildcards in given directories
  my $WildCardsRef   = shift; # array-ref
  my $DirectoriesRef = shift; # array-ref
  
  unless ($DirectoriesRef || (ref($DirectoriesRef) eq 'ARRAY') ) 
  { $DirectoriesRef = ['./']; }

  my @FilesFound;
  my %FilesFound;
  foreach my $Directory (@$DirectoriesRef) {
    my $CMD = "find -L $Directory ".join( " -or ", map { "-iwholename \"$_\""; } @$WildCardsRef);
    #print " >$CMD\n"; #D
    my $LocalFilesFound = `$CMD`;
    my @LocalFilesFound = split("\n", $LocalFilesFound);
    map { # add all new files -> @FilesFound, %FilesFound 
      unless ($FilesFound{$_}) {
        $FilesFound{$_} = 1;
        push @FilesFound, $_;
      }
    } @LocalFilesFound;
  }
  
  return @FilesFound;
}
sub findClosingBracket { # finds position of closing bracket matching to first one
  my $String = shift;
  my $OpeningBracket = substr($String, 0, 1);
  my $ClosingBracket = { '(' => ')', '[' => ']', '{' => '}' }->{$OpeningBracket};
  my $Pos = 0;
  if ($ClosingBracket) {
    my $Openings = 0;
    for (my $Index = 0; $Index < length($String); $Index++) {
      my $C = substr($String, $Index, 1);
      if ($C eq $OpeningBracket) { $Openings++; }
      if ($C eq $ClosingBracket) { $Openings--; }
      if ($Openings == 0) {
        $Pos = $Index + 1;
        last;
      }
    }
  }
  #print "findClosingBracket($OpeningBracket -> $ClosingBracket): '".substr($String, 0, $Pos + 1)."'\n"; #D
  
  return $Pos;
}
sub getWarnings {        # returns list of warnings in given lines
  my @Lines = grep { (index($_, 'warning:') > -1); } getCompileLines(@_);
  
  return @Lines;
}
sub getErrors {          # returns list of errors in given lines
  my $FirstLine = shift;
  my $LastLine = shift;
  
  my $LineIndex = $FirstLine;
  
  my %ErrorMessages;
  grep { my $IsError;
         foreach my $ErrorType (keys %GlobalErrorTypes) {
           if (index($_, $GlobalErrorTypes{$ErrorType}) > -1) { 
             $IsError = 1;
             unless ($ErrorMessages{$ErrorType}) { $ErrorMessages{$ErrorType} = []; }
             push @{ $ErrorMessages{$ErrorType} }, {
               Line => $LineIndex,
              Error => $_
             };
             last;
           }
         }
         $LineIndex++;
         $IsError;
       } getCompileLines($FirstLine, $LastLine);

  return \%ErrorMessages;
}
sub getCompileLines {    # returns slice from @CompileLog
  my $FirstLine = shift;
  my $LastLine = shift;
  
  my @Lines;
  for (my $Index = $FirstLine; $Index <= $LastLine; $Index++) {  
    push @Lines, strip($CompileLog[$Index]);
  }
  
  return @Lines;
}
sub isInsideComment {    # finds out if given position is inside a c++ comment
  my $String = shift;
  my $Pos    = shift;
  
  if (0) {
      print "isInsideComment(".substr($String, $Pos -30, 30)." | ".substr($String, $Pos, 30).",$Pos)\n"; #D
  }
  my $IsInsideComment = 0;
  my $LineFeedPassed =  0;
  my $AmountClosingComment = 0;
  my $AmountOpeningComment = 0;
  
  while ($Pos > 0) {
    $Pos--;
    my $B = substr($String, $Pos - 1, 1);
    my $C = substr($String, $Pos    , 1);
    if ($B eq "\n") { $LineFeedPassed = 1; }
    unless ($LineFeedPassed) { # still in same line
      if ($B.$C eq '//') {     # inside // comment
        $IsInsideComment = 1;
        last;
      }
    }
    if ($B.$C eq '*/') { $AmountClosingComment++; }
    if ($B.$C eq '/*') { $AmountOpeningComment++; }
    if ($AmountOpeningComment > $AmountClosingComment) { # inside multi line comment
       $IsInsideComment = 1;
       last;
    }
  }
  
  return $IsInsideComment;
}
sub parseCompilerLine {  # reads in all data about single stage from compile line; creates single %Stage
    my $Line = strip(shift);
    # Examples:
    # compile> arm-none-eabi-gcc -std=gnu99 -c -fno-common -fno-builtin -ffreestanding -funsigned-char -DEXTENSION_basic_extensions_basic_setup=1 -DEXTENSION_basic_extensions_basic_setup=1 -DEXTENSION_basic_extensions_optimize_1_debug=1 -DEXTENSION_050_compiler_sourcery_gpp=1 -DSOURCE_COMPILER=050_compiler_sourcery_gpp -DEXTENSION_100_board_wsn3_etrx357=1 -DEXTENSION_100_board_wsn3_etrx357=1 -DTTC_BOARD=100_board_wsn3_etrx357 -DUC_DEVICE=STM32W108 -DUC_PACKAGE_PINS=48 -DuCONTROLLER=STM32W108 -DTTC_MEMORY_REGION_RAM_SIZEK=8 -DTTC_MEMORY_REGION_RAM_START=0x20000000     -DTTC_MEMORY_REGION_RAM_ACCESS=rxw -DTTC_MEMORY_REGION_ROM_SIZEK=128 -DTTC_MEMORY_REGION_ROM_START=0x08000000   -DTTC_MEMORY_REGION_ROM_ACCESS=rx -DTTC_MEMORY_REGION_FIB_SIZEK=2 -DTTC_MEMORY_REGION_FIB_START=0x08040000   -DTTC_MEMORY_REGION_FIB_ACCESS=ra -DTTC_LED1=TTC_GPIO_C6 -DTTC_LED2=TTC_GPIO_C7 -DTTC_SWITCH1=TTC_GPIO_B0    -DTTC_SWITCH1_TYPE=E_ttc_gpio_mode_input_floating  -DTTC_SWITCH2=TTC_GPIO_B6 -DTTC_SWITCH2_TYPE=E_ttc_gpio_mode_input_floating  -DTTC_SWITCH3=TTC_GPIO_A1 -DTTC_SWITCH3_TYPE=E_ttc_gpio_mode_input_floating  -DTTC_SWITCH4=TTC_GPIO_A2 -DTTC_SWITCH4_TYPE=E_ttc_gpio_mode_input_floating  -DTTC_USART1=UART1 -DTTC_USART1_TX=TTC_GPIO_B1 -DTTC_USART1_RX=TTC_GPIO_B2 -DTTC_USART1_RTS=TTC_GPIO_B4  -DTTC_USART1_CTS=TTC_GPIO_B3 -DTTC_SPI1=SPI1            -DTTC_SPI1_MOSI=TTC_GPIO_B1 -DTTC_SPI1_MISO=TTC_GPIO_B2 -DTTC_SPI1_SCK=TTC_GPIO_B3 -DTTC_SPI1_NSS=TTC_GPIO_B4 -DTTC_SPI2=SPI2 -DTTC_SPI2_MOSI=TTC_GPIO_A0 -DTTC_SPI2_MISO=TTC_GPIO_A1 -DTTC_SPI2_SCK=TTC_GPIO_A2 -DTTC_SPI2_NSS=TTC_GPIO_A3 -DTTC_I2C1=I2C1 -DTTC_I2C1_SDA=TTC_GPIO_B1   -DTTC_I2C1_SCL=TTC_GPIO_B2   -DTTC_I2C2=I2C2 -DTTC_I2C2_SDA=TTC_GPIO_A1   -DTTC_I2C2_SCL=TTC_GPIO_A2   -DEXTENSION_cpu_stm32w1xx=1 -DTARGET_ARCHITECTURE_PREFIX=EM357 -DTARGET_ARCHITECTURE_STM32W1xx -DTARGET_ARCHITECTURE_STM32W1xx -DTARGET_ARCHITECTURE_CM3 -DTARGET_DATA_WIDTH=32 -DuCONTROLLER=STM32W108 -DSTM32W108 -Ittc-lib/stm32w/ -DCORTEXM3_STM32W108 -DPHY_STM32W108XX -DDISABLE_WATCHDOG -DCORTEXM3_STM32W108xB -DCORTEXM3 -DEXTENSION_cpu_stm32w1xx_hal=1 -DHAL_STANDALONE    -DEXTENSION_cpu_stm32w1xx_simple_mac=1 -DTARGET_ARCHITECTURE_STM32W1xx -DEXTENSION_ttc_gpio=1 -DEXTENSION_ttc_gpio=1 -DEXTENSION_ttc_task=1 -DEXTENSION_600_example_leds=1 -DEXTENSION_600_example_leds -DEXTENSION_700_extra_settings -Wall -W -Wconversion  -Wshadow -Wwrite-strings -Winline -O0 -g3 -fmessage-length=0 -I ./ -I configs/ -I ttc-lib/ -I examples/ -I additionals/050_compiler_sourcery_gpp/arm-none-eabi/include/ -I additionals/050_compiler_sourcery_gpp/lib/gcc/arm-none-eabi/4.4.1/include/ -I additionals/200_cpu_stm32w1xx_hal_library/simplemac/include                 -I additionals/200_cpu_stm32w1xx_hal_library/micro/                 -I additionals/200_cpu_stm32w1xx_hal_library/                 -I additionals/200_cpu_stm32w1xx/STM32W108/                 -I additionals/200_cpu_stm32w1xx_hal_library/micro/cortexm3/stm32w108/                 -I additionals/200_cpu_stm32w1xx_hal_library/micro/cortexm3/                 -I additionals/200_cpu_stm32w1xx_hal_library/micro/cortexm3/compiler/                 -I additionals/200_cpu_stm32w1xx_hal_library/micro/generic/compiler                 -I additionals/200_cpu_stm32w1xx_hal_library/micro/cortexm3/compiler                 -I additionals/200_cpu_stm32w1xx_hal_library/simplemac/include -I additionals/200_cpu_stm32w1xx_hal_library/ -Iadditionals/200_cpu_stm32w1xx_hal_library/micro/cortexm3/stm32w108/ -Ittc-lib/cm3 -I additionals/200_cpu_stm32w1xx_SimpleMAC_library/ -mcpu=cortex-m3 -mthumb  ttc-lib/extensions.c
    # link>    arm-none-eabi-gcc -Wl,-Map=main.map -nostartfiles -mcpu=cortex-m3 -mthumb -Tttc-lib/_linker/memory_stm32w1xx.ld -Tadditionals/200_cpu_stm32w1xx_hal_library/micro/cortexm3/stm32w108/gnu-stm32w108.ld -mthumb additionals/200_cpu_stm32w1xx_SimpleMAC_library/library/libsimplemac-library-gnu.a extensions.o ttc_basic.o startup_stm32w108.o context-switch.o spmr.o micro-common-internal.o micro-common.o micro.o board.o clocks.o ttc_gpio.o DEPRECATED_stm32w_gpio.o ttc_task.o example_leds.o main.o -omain.elf
  
    my %ValueOptions  = ( #{ options that have a value
                      '-std' => 0,
                      '-W' => 0,
                      '-Wl,' => 0,
                      '-O' => 0,
                      '-O' => 0,
                      '-g' => 0,
                      '-fmessage-length' => 0,
                      '-T' => 0,
                      '-o' => 0,
                      '-x' => 0,
                      '-I' => 0,
                      '-D' => 0,
                    ); #}
    my %SwitchOptions = ( #{ options that act as switches
                          '-c' => 0,
                          '-fno-common' => 0,
                          '-fno-builtin' => 0,
                          '-ffreestanding' => 0,
                          '-funsigned-char' => 0,
                          '-Wall' => 0,
                          '-mcpu=cortex-m3' => 0,
                          '-mthumb' => 0,
                          '-nostartfiles' => 0,
                        ); #}

    # remove superfluos spaces
    do { $Line =~ s/  / /g; }
    while (index($Line, '  ') > -1);
    foreach my $Option ( sort { length($b) <=> length($a); } # longest options first 
                         keys %ValueOptions
                       ) { # remove space between Option and its value
      $Line =~ s/ $Option / $Option/g;
    }
    
    my @Words = split(" ", $Line);
    shift @Words;                       # remove compiler 
    my @Options = grep { substr($_, 0, 1) eq '-'; } @Words;
    my %RecognizedOptions;
    my %Constants = map { my @Parts = split("=", $_); if (1 == scalar @Parts) { push @Parts, 1; } @Parts; }  # create array (Key=>Value)
                    map { substr($_, 2); }                                                                   # remove -D
                    map {$RecognizedOptions{$_} = 1; $_; }                                                   # recognize this option
                    grep { substr($_, 0, 2) eq '-D'; }                                                       # find options starting with -D 
                    @Options;
    my @IncludePaths;                                                                                        # list of unique include paths in same order as given on command-line
    my %IncludePaths = map { ($_, 1); }                                                                      # create array (Key=>1)
                       map { unless ($IncludePaths{$_}) { push @IncludePaths, $_; } $_; }                    # seen first time: add to @IncludePaths
                       map { substr($_, 2) }                                                                 # remove -I
                       map {$RecognizedOptions{$_} = 1; $_; }                                                # recognize this option
                       grep { substr($_, 0, 2) eq '-I'; }                                                    # find options starting with -I
                       @Options;
                       
    @Options = grep { $RecognizedOptions{$_} != 1; } @Options; # remove already recognized options
    map {$SwitchOptions{$_} = 1; }
    map {$RecognizedOptions{$_} = 1; $_; } # recognize this option
    grep { defined($SwitchOptions{$_}); }  # find known SwitchOptions
    @Options;

    @Options = grep { $RecognizedOptions{$_} != 1; } @Options; # remove already recognized options
    map {$RecognizedOptions{$_} = 1; $_; } # recognize this option
    grep { # %ValueOptions
      my $IsValueOption = '';
      foreach my $ValueOption (sort { length($b) <=> length($a); } # longest options first
                               keys %ValueOptions
                              ) {
        if (substr($_, 0, length($ValueOption)) eq $ValueOption) {  
        $IsValueOption = $ValueOption; last; }
      }
      if ($IsValueOption) {
        my $Value = substr($_, length($IsValueOption));
        if (substr($Value, 0, 1) eq '=') { substr($Value, 0, 1) = undef; }
        unless ($ValueOptions{$IsValueOption}) { $ValueOptions{$IsValueOption} = []; }
        push @{ $ValueOptions{$IsValueOption} }, $Value; 
      }
      $IsValueOption;    
    }  # find known ValueOptions
    @Options;
    
    @Options  = grep { $RecognizedOptions{$_} != 1; } @Options; # remove already recognized options
    my @Files = grep { (substr($_, 0, 1) ne '-'); } @Words;
    
    our %Suffixes = ( c => [], h => [], s => [], o => [] );
    map { # count all file-suffixes
      my $LastDotPos = rindex($_, '.');
      if (substr($_, -1) eq '/') { $LastDotPos = -1; } # files do not end with /
      if ($LastDotPos > -1) {
        my $Suffix = lc(substr($_, $LastDotPos + 1));
        unless ($Suffixes{$Suffix}) { $Suffixes{$Suffix} = []; }
        push @{ $Suffixes{$Suffix} }, $_;
      }
      undef;
    } @Files;
    
    sub amount { # amount of files with given suffix
      my $Suffix = shift;
      if (!defined $Suffixes{$Suffix} ) { return 0; }
      
      return scalar @{ $Suffixes{$Suffix} };
    }
    
    my $Debug = "";
    $Debug .= "@".$LineNo." ";
    
    my $Stage; my $InFile; my $OutFile;
    
    if (index($Line, ' -c ') > -1) { # stage detected: compiling a source file
      $Stage = 'compile';
      $Debug .= "compile '".$Files[-1]."' ";
      $InFile = $Files[-1];
    }
    if ( (amount('c') == 0) &&
         (amount('h') == 0) &&
         (amount('s') == 0) &&
         (amount('o')  > 0)
       ) {                           # stage detected: linking objects
      $Stage = 'link';
      $OutFile = $ValueOptions{'-o'}->[-1];
      $Debug .= "link '$OutFile' ";
      $OutFile = $OutFile;
    }
    unless ($Stage) { $Stage = 'unknown'; }
    
    $Debug .= "\n";
    if (0) { $Debug .= "  Constants:\n    ".join("\n    ", map { $_.' = '.$Constants{$_}; } sort keys %Constants)."\n\n"; }
    if (0) { $Debug .= "  IncludePaths:\n    ".join("\n    ", @IncludePaths)."\n\n"; }
    if (1) { $Debug .= "  ValueOptions:\n    ".join("\n    ", map { $_.' = '.join(', ', @{ $ValueOptions{$_} }); } grep { $ValueOptions{$_}; } sort keys %ValueOptions)."\n\n"; }
    if (0) { $Debug .= "  SwitchOptions:\n    ".join("\n    ", grep { $SwitchOptions{$_}; } sort keys %SwitchOptions)."\n\n"; }
    if (0 && @Options) { $Debug .= "  UnrecognizedOptions:\n    ".join("\n    ", @Options)."\n\n"; }
    unless ($Stage) { 
      print "unkown line:\n  $Line\n";
      print "Files = '".join("','", @Files)."'\n";
      foreach my $Suffix (sort keys %Suffixes) {
      print "  amount($Suffix)=".amount($Suffix)."=".(scalar @{ $Suffixes{$Suffix} })."\n";
    }

    }
    
    my %StageData = ( #{
           ValueOptions => \%ValueOptions,
          SwitchOptions => \%SwitchOptions,
    UnrecognizedOptions => \@Options,
              Constants => \%Constants,
           IncludePaths => \@IncludePaths,
                  Stage => $Stage,
                OutFile => $OutFile,
                 InFile => $InFile
    ); #} 

    push @{ $Stages{$Stage} }, \%StageData;
    if (0) { print $Debug; }
    
    return \%StageData;
}
sub parseStage {         # analyzes messages of single %Stage
  my $StageDataRef  = shift; # hash-ref:  all data of one stage
  my $CompileLogRef = shift; # array-ref: all lines from compile log

  #X my %ErrorTypes = ( General => 'error:', UndefinedReference => 'undefined reference to' );
  my @Warnings  = getWarnings($StageDataRef->{FirstLine}-1, $StageDataRef->{LastLine}-1);
  my $ErrorsRef = getErrors($StageDataRef->{FirstLine}-1, $StageDataRef->{LastLine}-1);
  
  my $AnalyzersRef = $Analyzers{ $StageDataRef->{Stage} };
  if ($AnalyzersRef) {
    print "  analyzing stage ".$StageDataRef->{Stage}." ";
    foreach my $Analyzer (@$AnalyzersRef) {
      $Analyzer->($StageDataRef, $CompileLogRef, $ErrorsRef, \@Warnings);
    }
  }
  else {
    print "  no analyzer available for stage '".$StageDataRef->{Stage}."'\n";
  }
    
  if (0) { print "    warnings (lines ".$StageDataRef->{FirstLine}."-".$StageDataRef->{LastLine}."):\n      '".join("'\n      '", @Warnings)."'\n\n"; }
  #Xif (1) { print "    errors (lines ".$StageDataRef->{FirstLine}."-".$StageDataRef->{LastLine}."):\n      '".join("'\n      '", @Errors)."'\n\n"; }
}
sub strip {              # removes invisible characters at begin + end of given string
  my $Line = shift || "";

  my %IsWhiteChar = ( ' ' => 1, "\n" => 1, "\r" => 1, "\t" => 1 ); 

  $Line =~ s/^\s+//;
  $Line =~ s/\s+$//;
  #$Line =~ s/(?:^ +)||(?: +$)//g;
  #$Line =~ s/^\ *([A-Z,a-z,0-9]*)\ *$/\1/g;
  #$Line =~ s/^\s+//;
  #$Line  =~ s/\s+$//;
  return $Line;
  
  my $lIndex = 0;
  while ( $IsWhiteChar{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsWhiteChar{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  #X print " substr(\$Line, $lIndex, $rIndex - $lIndex + 1);\n"; #D
  return substr($Line, $lIndex, $rIndex - $lIndex + 1);

}
sub stripBrackets {      # removes all bracket type characters from begin + end of given string
  my $Line = shift || "";

  my %IsBracket = ( '[' => 1, ']' => 1, '{' => 1, '}' => 1, '(' => 1, ')' => 1, '"' => 1, '"' => 1, "'" => 1, '`' => 1 );
  
  my $lIndex = 0;
  while ( $IsBracket{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsBracket{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  my $Stripped = substr($Line, $lIndex, $rIndex - $lIndex + 1);
  while (substr($Stripped, -1) eq "'") { substr($Stripped, -1) = undef; }
  
  #print " substr(\$Line, $lIndex, $rIndex - $lIndex + 1) = '$Stripped'\n"; #D
  return $Stripped;

}
sub isNormalCharacter {  # checks if first character of given string is one of a-z,A-Z,0-9,_
  my $C = substr(shift(), 0, 1);

  my %NormalCharacter = ( 'a' => 1, 'b' => 1, 'c' => 1, 'd' => 1, 'e' => 1, 'f' => 1, 'g' => 1, 'h' => 1, 'i' => 1, 'j' => 1, 'k' => 1, 
                          'l' => 1, 'm' => 1, 'n' => 1, 'o' => 1, 'p' => 1, 'q' => 1, 'r' => 1, 's' => 1, 't' => 1, 'u' => 1, 'v' => 1, 
                          'w' => 1, 'x' => 1, 'y' => 1, 'z' => 1, 'A' => 1, 'B' => 1, 'C' => 1, 'D' => 1, 'E' => 1, 'F' => 1, 'G' => 1, 
                          'H' => 1, 'I' => 1, 'J' => 1, 'K' => 1, 'L' => 1, 'M' => 1, 'N' => 1, 'O' => 1, 'P' => 1, 'Q' => 1, 'R' => 1, 
                          'S' => 1, 'T' => 1, 'U' => 1, 'V' => 1, 'W' => 1, 'X' => 1, 'Y' => 1, 'Z' => 1, '0' => 1, '1' => 1, '2' => 1, 
                          '3' => 1, '4' => 1, '5' => 1, '6' => 1, '7' => 1, '8' => 1, '9' => 1, '_' => 1
  );
 
  return $NormalCharacter{"$C"};
}
sub removeDoubles {      # removes double entries from given list of strings
  our %Seen;
  
  #print "\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n"; #D
  #print " removeDoubles(".join(',', @_).") = "; #D 
  my @NoDoubles;
  foreach my $Item (@_) {
    if (!$Seen{$Item}) { 
      $Seen{$Item} = 1;
      push @NoDoubles, $Item;
    }
  }
  #print "".join(',', NoDoubles).";\n"; #D
  return @NoDoubles; 
}

exit 0;
