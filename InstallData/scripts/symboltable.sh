#!/bin/bash

ObjectFile="$1"
if [ "$ObjectFile" == "" ]; then
  ObjectFile="main.elf"
fi
CMD="arm-none-eabi-objdump -t $ObjectFile"
echo ">$CMD"
$CMD | sort | less


