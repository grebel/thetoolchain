#!/bin/bash

BankNo="$1"
AmountBytes="$2"
BinFile="$3"

source _/installFuncs.sh

if [ "$BinFile" == "" ]; then
  TS=`date +"%Y%m%d_%H%M%S"`
  BinFile="binaries/dowloaded_bank${BankNo}_${TS}.content"
fi
dir binaries
rm -f $BinFile

cat <<END_OF_CFG >configs/openocd_read.cfg #{

# openocd read from flash script

# finish configuration stage + proceed to run stage
init

# hardware reset processor with halt
reset halt

# check target state
poll

# list all found falsh banks
flash banks

# identify the flash
flash probe 0

END_OF_CFG

if [ "$AmountBytes" != "" ]; then
  cat <<END_OF_CFG >>configs/openocd_read.cfg

# read 128kB from bank 0
flash read_bank 0 $BinFile $BankNo $AmountBytes

END_OF_CFG
fi #}

  cat <<END_OF_CFG >>configs/openocd_read.cfg

# hardware processor reset with run
reset run

#shutdown openocd daemon
shutdown

END_OF_CFG

CMD="openocd --search additionals/999_open_ocd -f configs/openocd_interface.cfg -f configs/openocd_target.cfg -f configs/openocd_read.cfg"
echo "> $CMD"
$CMD

if [ "$AmountBytes" == "" ]; then
  cat <<END_OF_CFG


$0 - ERROR: Missing argument <AMOUNT_BYTES> !


Synopsis:

$0 <BANK_NUMBER> <AMOUNT_BYTES> [<BINARY_FILE>]

Will read content from flash memory of connected microcontroller into local file flash_bank<BANK_NUMBER>.bin.


Arguments:
    <BANK_NUMBER>   0.. amount of available flash memory banks (see output above for available banks!)
    <AMOUNT_BYTES>  Amount of bytes to read (see output above for size of flash memory in connected microcontroller!)
    <BINARY_FILE>   File path where to store downloaded flash content. If ommitted, a new unique filename will be created inside binaries/.  
    
Examples:
  $0 0 131072   # read 128 kB from bank 0


END_OF_CFG
  exit 10
fi #}
if [ -e "$BinFile" ]; then
  echo ""
  echo "Successfully downloaded $AmountBytes from bank #$BankNo to file '$BinFile':"
  echo -n "  "
  ls -l "$BinFile"
fi