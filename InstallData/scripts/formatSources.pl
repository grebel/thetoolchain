#!/usr/bin/perl

my $ListFile = shift(@ARGV);

unless ($ListFile && (-e $ListFile)) {
  print <<"END_OF_HELP";
$0 LIST_FILE

LIST_FILE  file storing a list of source files to format (one relative or absolute file name per line).

Format set of source code files with uniform coding style by use of astyle tool.

written by Gregor Rebel 2015

END_OF_HELP

  unless (-e $ListFile) {
    my $PWD = `pwd`; chomp($PWD);
    if (substr($ListFile, 0, 1) ne '/') {
      $ListFile = $PWD.'/'.$ListFile;
    }
    
    print "ERROR: File not found: $ListFile\n\n";
  }
  
  exit(10);
}

#DISABLED (Caused trouble to install) installPackage("which astyle", "astyle");

my @Styles = #{ astyle configuration (-- prefix omitted)
qw( 
  add-one-line-brackets
  align-pointer=type
  align-reference=middle
  break-closing-brackets
  convert-tabs
  indent-classes
  indent-col1-comments
  indent-labels
  indent-preproc-block
  indent-preproc-define
  indent=spaces=4
  indent-switches
  keep-one-line-statements
  lineend=linux
  max-instatement-indent=100
  pad-oper
  pad-paren-in
  pad-header
  style=java
  suffix=none
  unpad-paren
  formatted
  ); #}
my $FormatCommand = "astyle ".join(" ", map { '--'.$_; } grep { $_ } @Styles);
print "> $FormatCommand\n";

my $SourceFiles = readFile($ListFile);
my @SourceFiles = split("\n", $SourceFiles);

my %ValidSuffix = (
     c => 1,
     h => 1
  );

@SourceFiles = grep {
  my $LastDotPos = rindex($_, '.');
  my $Suffix = substr($_, $LastDotPos+1);
  
  $ValidSuffix{$Suffix};
} @SourceFiles;

print "formatting ".(scalar @SourceFiles)." files..\n";

foreach my $SourceFile (@SourceFiles) {
  my $Result = `$FormatCommand $SourceFile`;
  $Result = strip($Result);
  if ($Result) { print "    formatted $SourceFile\n"; }
}

sub readFile {                   # read in content of given file
  my $FilePath = shift;
  
  my $Content;
  my $Error;
  open(IN, "<$FilePath") or $Error = "$0 - ERROR: Cannot read from file '$FilePath' ($!)";
  dieOnError($Error);
  while (<IN>) { $Content .= $_; }
  close(IN);
  
  $Content =~ s/\\\n//g; # concatenate multiline strings
  
  return $Content;
}
sub installPackage {             # install given software via package manager if given test fails
  #
  # Example Usage:
  #   installPackage("which astyle", "astyle");
  #
  
  my $Test         = shift;  # bash command that will be executed to test existence of file or executable (package will be installed if test returns nothing)
  my @PackageNames = @_;     # list of alternative package names to install. Installation will stop at first successfull installed entry

  my $AmountRetries=5;
  while ( (`$Test` eq '') && ($AmountRetries-- > 0) ) {  
    my $PackageName = shift(@PackageNames);
    
    print "installing missing package $PackageName...\n";
  
    if ( -x '/usr/bin/zypper' ) {
      system("sudo zypper --no-gpg-checks install -n -l -y $PackageName");
    }
    else {
      system("sudo dpkg --configure -a");    # some installations showed a problem requiring this command
      system("waitForProcess dpkg");
      $ENV{DEBIAN_FRONTEND} = 'noninteractive';
      
      # cmd "sudo DEBIAN_FRONTEND=noninteractive apt-get --install-suggests --force-yes --fix-missing -y install $@"
      system("sudo DEBIAN_FRONTEND=noninteractive apt-get --force-yes --fix-missing -y install $@");
    }
  }
}
sub dieOnError {                 # dies if error given + prints caller stack
  my @ErrorMessages = grep { $_; } @_;
  unless (@ErrorMessages) { return; } # no error no die
  
  my $Level = 1;
  my @Stack;
  my ($package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash);

  do {
     ($package, $filename, $line, $subroutine, $hasargs,
      $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
      = caller($Level++);
      if ($subroutine) { push @Stack, "$subroutine() called from ${filename}:$line"; }
  } while ($filename);
  ($package, $filename, $line, $subroutine, $hasargs,
   $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
   = caller(0);
  my $PWD = `pwd`; chomp($PWD);
  push @ErrorMessages, "PWD='$PWD'";
  push @Stack, "dieOnError() called from ${filename}:$line";
  push @ErrorMessages, @Stack;

  print( "$0 - ERROR in $subroutine(): ".join("\n    ", @ErrorMessages)."\n" );
  exit 10;
}
sub strip {                      # removes invisible characters at begin + end of given string
  my $Line = shift || "";

  my %IsWhiteChar = ( ' ' => 1, "\n" => 1, "\r" => 1, "\t" => 1 ); 

  $Line =~ s/^\s+//;
  $Line =~ s/\s+$//;
  #$Line =~ s/(?:^ +)||(?: +$)//g;
  #$Line =~ s/^\ *([A-Z,a-z,0-9]*)\ *$/\1/g;
  #$Line =~ s/^\s+//;
  #$Line  =~ s/\s+$//;
  return $Line;
  
  my $lIndex = 0;
  while ( $IsWhiteChar{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsWhiteChar{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  return substr($Line, $lIndex, $rIndex - $lIndex + 1);

}
