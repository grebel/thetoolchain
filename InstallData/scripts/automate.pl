#!/usr/bin/perl

use strict;

system("killall -q openocd_"); # such a process could interfere with our own openocd instance

my $MakeFile_Dynamic = "extensions.local/makefile.dynamic";

my %DebugInterfaces2Process;
my @Targets2Process; #{ List of commands for each target interface as parsed from argument list
#
# +-------------------+
# | @Targets2Process  |
# +-------------------+   +------------------+
# | $Target2Process1 ---> | %Target2Process  | <-- settings for one programming interface
# |        :          |   +------------------+
#                         | Interface => IFn | <-- >0: index number of programming interface to use   
#                         | Compile => 1|0   | <-- ==1: compile sources   
#                         | Flash   => 1|0   | <-- ==1: flash compiled binary onto uC   
#                         | Debug   => 1|0   | <-- ==1: start debugger    
#                         |                  |   +---------------------+
#                         | Constants ---------> | %Constants          | <-- all constants to be set for current target 
#                         |                  |   +---------------------+
#                         |                  |   | Constant1 => Value1 |
#                         |                  |   |         :           |
#                         +------------------+
#
#
#
#
#
#}
my $Target2Process = {};
               
my %DebugKeys = ( #          debug.sh Option --vvvv            v--- descriptive text for help page 
                  'DEBUG'      => { Option => '',     Text => 'start textual debugger for debug interface #<n>' }, 
                  'DEBUG_DDD'  => { Option => 'DDD',  Text => 'start graphical debugger ddd for debug interface #<n>' }, 
                  'DEBUG_KDBG' => { Option => 'KDBG', Text => 'start graphical debugger kdbg for debug interface #<n> (known to have issues)' } 
                ); 

while (@ARGV) { # parse all arguments into @Targets2Process
  unless ($Target2Process) { $Target2Process = {  }; } # create new hash

  my $Argument = shift(@ARGV);
  my $InterfaceIndex = -1;
  if    ($Argument eq 'COMPILE') { $Target2Process->{Compile} = 1; }
  elsif (substr($Argument, 0, 6) eq 'FLASH@') {
    $InterfaceIndex = substr($Argument, 6) + 0;
    if ($Target2Process->{Interface}) { # interface already set: check if different
      if ($Target2Process->{Interface} != $InterfaceIndex) { # new interface: push target hash + create new one
        push @Targets2Process, $Target2Process;
        $Target2Process = {  };
      }
    }
    $Target2Process->{Interface} = $InterfaceIndex;
    $Target2Process->{Flash}     = 1; 
    push @Targets2Process, $Target2Process;
    $Target2Process = {  };
  }
  elsif (substr($Argument, 0, 5) eq 'DEBUG') {
    my $IsDebugKey = 0;
    foreach my $DebugKey (keys %DebugKeys) {
      if (substr($Argument, 0, length($DebugKey) + 1) eq $DebugKey.'@') {
        $IsDebugKey = $DebugKey;
      }
    }
    if ($IsDebugKey) {
      $InterfaceIndex = substr($Argument, length($IsDebugKey) + 1) + 0;
      if ($Target2Process->{Interface}) { # interface already set: check if different
        if ($Target2Process->{Interface} != $InterfaceIndex) { # new interface: push target hash + create new one
          push @Targets2Process, $Target2Process;
          $Target2Process = {  };
        }
      }
      $Target2Process->{Interface} = $InterfaceIndex;
      $Target2Process->{Debug}     = $IsDebugKey;
    }
  }
  elsif (index($Argument, '=') > -1) {
    my $Pos = index($Argument, '=');
    my $Name  = substr($Argument, 0, $Pos);
    my $Value = substr($Argument, $Pos + 1);
    $Target2Process->{Constants}->{$Name} = $Value;
  }

  if ($InterfaceIndex > -1) {
    $DebugInterfaces2Process{$InterfaceIndex} = $Argument;
  }
}
if (%$Target2Process) {
  push @Targets2Process, $Target2Process;
}

unless (@Targets2Process) { # print help + exit
  my $DebugKeys = join("|", map { '['.$_.'@<IFn>]'; } sort keys %DebugKeys);
  my $DebugLines = "  ".
                   join("\n  ",
                        map { substr($_.'@<IFn>                                    ', 0, 19).$DebugKeys{$_}->{Text}
                            } sort keys %DebugKeys
                       )."\n";
  
  print <<"END_OF_HELP";

Synopsis:
  $0 [<CONSTANT>=<VALUE> [..]] [COMPILE] [FLASH\@<IFn>] $DebugKeys [...] 

  COMPILE            compile source code with current set of dynamic definition
  FLASH\@<n>          flash compiled binary onto uC connected to interface #<n>
$DebugLines
  Note: At the moment only one debug session is supported for each $0 run.
        You may use multiple copies of same project and run one $0 in each
        if you want to simultaneously debug multiple microcontrollers.

  <CONSTANT>=<VALUE> append a constant definition for next compile run
  
Compile, flash and debug one or more microcontrollers with individual settings.
The given arguments are interpreted one after another. 
Accumulated constant definitions are stored in makefile.dynamic. This may be included from
extensions.local/makefile.700_extra_settings to pass them to C source during compile run.

To see a list of supported programming interfaces, simply issue:
   _/openocd_scan_interfaces.pl HELP


Examples:

# Just compile source code without flashing (like ./compile.sh NOFLASH)
$0 COMPILE

# Compile source code + flash using interface #1
$0 COMPILE FLASH\@1

# Compile source code + flash same binary using interfaces #1, #2 and #3
$0 COMPILE FLASH\@1 FLASH\@2 FLASH\@3

# Start debugger using debug interface #2
$0 DEBUG\@2

# Start graphical debugger ddd using debug interface #2
$0 DEBUG_DDD\@2

# Define two constants A and B and compile everything
# Note: Add this line to extensions.local/makefile.700_extra_settings:
#       include $MakeFile_Dynamic
$0 A=123 B=234 COMPILE

# compile + flash with different constant definitions for three targets
#  1) erase all constant definitions
#  2) add definition to $MakeFile_Dynamic:
#     COMPILE_OPTS += -DEXAMPLE_TTC_RADIO_NODE_INDEX=0
#  3) compile source code
#  4) flash compiled binary using programming interface #1
# 
#  5) erase all constant definitions
#  6) add definition to $MakeFile_Dynamic:
#     COMPILE_OPTS += -DEXAMPLE_TTC_RADIO_NODE_INDEX=2
#  7) compile source code
#  8) flash compiled binary using programming interface #2
#
#  9) erase all constant definitions
# 10) add definition to $MakeFile_Dynamic:
#     COMPILE_OPTS += -DEXAMPLE_TTC_RADIO_NODE_INDEX=100
# 11) compile source code
# 12) flash compiled binary using programming interface #3

$0 \\
   EXAMPLE_TTC_RADIO_NODE_INDEX=0   COMPILE FLASH\@1 \\
   EXAMPLE_TTC_RADIO_NODE_INDEX=2   COMPILE FLASH\@2 \\
   EXAMPLE_TTC_RADIO_NODE_INDEX=100 COMPILE FLASH\@3


written by Gregor Rebel 2016

END_OF_HELP
  exit 0;
}

my @InterfaceCfgs = scanInterfaces();
unless (@InterfaceCfgs) {
  print "Could not find any interfaces: recompiling...\n";
  system("./clean.sh >/dev/null 2>&1; ./compile.sh NOFLASH >/dev/null 2>&1");
  @InterfaceCfgs = scanInterfaces();
}
print "found ".(scalar @InterfaceCfgs)." debug interfaces\n";

map {
  my $InterfaceIndex = $_ + 0;
  my $Argument       = $DebugInterfaces2Process{$InterfaceIndex};
  unless ( ($InterfaceIndex > 0) && ($InterfaceIndex <= scalar @InterfaceCfgs) ) {
    die("$Argument: Illegal interface index '$InterfaceIndex' (must be 1..".(scalar @InterfaceCfgs).")");
  }
} sort keys %DebugInterfaces2Process;

print "using ".(scalar @Targets2Process)." interfaces: ".
      join(" ", map { "#".($_->{Interface}+0); } @Targets2Process)."\n";
map {
  my %Target2Process = %$_;
  my $Comments =  "\n    ".join( "\n    ", map { my $Value = $Target2Process{$_};
                                                        if (ref($Value) eq 'HASH') {
                                                            my %Constants = %{ $Value };
                                                            "$_:\n        ".join("\n        ", map { $_.'='.$Constants{$_}; } sort keys %Constants)."\n";
                                                        }
                                                        else { "$_ = ".$Value; }
                                                      } sort keys %Target2Process
                                      )."\n";
  if (0) { # display what to do
    print "Target2Process:".$Comments;
  }
  if ($Target2Process{Compile}) { 
    if (ref($Target2Process{Constants}) eq 'HASH') { writeConstants( $Target2Process{Constants}, $Comments ); }
    compileSource(); 
  }
  if ($Target2Process{Flash})   { flashNode( $Target2Process{Interface} ); }
  if ($Target2Process{Debug})   { debugNode( $Target2Process{Interface}, $Target2Process{Debug} ); }
} @Targets2Process;

sub appendToString {              # appends given test to given string, if not already appended
  my $String = shift; # string to which text should be added to
  my $Append = shift; # text to append to $String
  
  my $AlreadyPresent = index( $String, strip($Append) ) + 1;
  unless ($AlreadyPresent) {
    $String .= $Append;
  }
  
  return $String;
}
sub dieOnError {                  # dies if error given + prints caller stack
  my @ErrorMessages = grep { $_; } @_;
  unless (@ErrorMessages) { return; } # no error no die
  
  my $Level = 1;
  my @Stack;
  my ($package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash);

  do {
     ($package, $filename, $line, $subroutine, $hasargs,
      $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
      = caller($Level++);
      if ($subroutine) { push @Stack, "$subroutine() called from ${filename}:$line"; }
  } while ($filename);
  ($package, $filename, $line, $subroutine, $hasargs,
   $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
   = caller(0);
  my $PWD = `pwd`; chomp($PWD);
  push @ErrorMessages, "PWD='$PWD'";
  push @Stack, "dieOnError() called from ${filename}:$line";
  push @ErrorMessages, @Stack;

  print( "$0 - ERROR in $subroutine(): ".join("\n    ", @ErrorMessages)."\n" );
  exit 10;
}
sub writeConstants {              # write given constants into makefile.dynamic
  my %Constants = %{ shift(@_) };
  my $Comments  = '# '.join("\n# ", split("\n", shift(@_) ) ); 
  
  my $Constants = join("\n    ", map {
                                   "COMPILE_OPTS += -D${_}=$Constants{$_}";
                                 } sort keys %Constants
                      );
  print "  $MakeFile_Dynamic:\n    ".$Constants."\n";
  my $TimeStamp=`date +"%Y%m%d-%H%M%S"`; chomp($TimeStamp);
  writeFile($MakeFile_Dynamic, <<"END_OF_CONTENT");
# Dynamic defined constants created by $0 at $TimeStamp
$Comments

$Constants

END_OF_CONTENT
  my $File = "extensions.local/makefile.700_extra_settings";
  if (-e $File) { # ensure that makefile.dynamic is included
    my $Content = readFile($File);
    my $Include = "include $MakeFile_Dynamic";
    my $Content2 = appendToString($Content, "$Include\n");
    if ($Content ne $Content2) {
      print "  appending to $File: $Include\n";
      writeFile($File, $Content2);
    }
  }
  else { print "missing '$File'\n"; } #DEBUG
}
sub scanInterfaces {              # scan for available programming interfaces
  system("_/openocd_scan_interfaces.pl");

  my $InterfaceCfgs = `ls 2>/dev/null configs/openocd_interface_usb_*`;
  my @InterfaceCfgs = split("\n", $InterfaceCfgs);

  return @InterfaceCfgs;
}
sub flashNode {                   # flash main.bin using given interface
  my $InterfaceIndex = shift;
  #X my $InterfaceCfg = shift;
  my $InterfaceCfg = $InterfaceCfgs[$InterfaceIndex - 1];
  
  print "  flashing main.bin using $InterfaceCfg ...\n";
  my $CMD = "_/flash_openocd.sh main.bin $InterfaceCfg >flash.log 2>&1";
  print "$0> $CMD\n";
  system($CMD);
}
sub debugNode {                   # start debugger for given interface
  my $InterfaceIndex = shift; # 1..amount of found debug interfaces
  my $Debugger       = shift; # one key from %DebugKeys 
  
  my $MyDebugger;
  if ($DebugKeys{$Debugger}) { $MyDebugger = $DebugKeys{$Debugger}->{Option}; }
  
  my $InterfaceCfg = $InterfaceCfgs[$InterfaceIndex - 1];
  
  print "starting debugger $MyDebugger using interface $InterfaceCfg ...\n";
  my $PortGDB = 3332 + $InterfaceIndex;
  sleep(3); # if graphical debugger is started immediately target crashed often
  my $CMD = "./debug.sh $MyDebugger $InterfaceCfg $PortGDB";
  print "$0> $CMD\n";
  system($CMD);
}
sub compileSource {               # compile current source code without flashing it 
  print "  compiling sources ...\n";
  system("./compile.sh NOFLASH >compile.log 2>&1");
  
  unless (-e "main.bin") {
    system("less compile.log");
    print "\n$0 Error occured during compilation!\n";
    exit 10;
  }
  my $CompiledBinary = `ls -l main.bin`;
  print "  compiled binary: $CompiledBinary\n";
}
sub readFile {                    # read in content of given file
  my $FilePath = shift;
  
  my $Content;
  my $Error;
  open(IN, "<$FilePath") or $Error = "$0 - ERROR: Cannot read from file '$FilePath' ($!)";
  dieOnError($Error);
  while (<IN>) { $Content .= $_; }
  close(IN);
  
  $Content =~ s/\\\n//g; # concatenate multiline strings
  
  return $Content;
}
sub strip {                       # removes invisible characters at begin + end of given string
  my $Line = shift || "";

  my %IsWhiteChar = ( ' ' => 1, "\n" => 1, "\r" => 1, "\t" => 1 ); 

  $Line =~ s/^\s+//;
  $Line =~ s/\s+$//;
  #$Line =~ s/(?:^ +)||(?: +$)//g;
  #$Line =~ s/^\ *([A-Z,a-z,0-9]*)\ *$/\1/g;
  #$Line =~ s/^\s+//;
  #$Line  =~ s/\s+$//;
  return $Line;
  
  my $lIndex = 0;
  while ( $IsWhiteChar{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsWhiteChar{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  return substr($Line, $lIndex, $rIndex - $lIndex + 1);

}
sub writeFile {                   # write content to given file
  my $FilePath     = shift;
  my $Content      = shift;
  my $BackUpSuffix = shift; # if given and original file exists, a backup file is being created using this suffix
  
  # ensure that directoy exists
  my $LastSlashPos = rindex($FilePath, '/');
  if ($LastSlashPos > -1) { # file is to be created in different folder: ensure that folder exists
    my $DirPath = substr($FilePath, 0, $LastSlashPos);
    createDirectoryPath($DirPath);
  }
  
  if ($BackUpSuffix) {
    if (-e $FilePath) {
      system("mv \"${FilePath}\" \"${FilePath}$BackUpSuffix\"");
    }
  }
  open(OUT, ">$FilePath") or die ("$0 - ERROR: Cannot write to file '$FilePath' ($!)");
  print OUT $Content;
  close(OUT);
  
  return $Content;
}
sub createDirectoryPath {         # creates a multi-level directory path from top to down
  my $DirectoyPath = shift; # relative or absolute directory path (without filename)
  
  my @DirectoyPath = split("/", $DirectoyPath);
  
  my $CurrentPath;
  map {
    unless ($CurrentPath) { $CurrentPath = $_; }
    else                  { $CurrentPath .= '/'.$_; }
    unless (-d $CurrentPath) { mkdir($CurrentPath); }
  } grep { $_; } @DirectoyPath; 
}
