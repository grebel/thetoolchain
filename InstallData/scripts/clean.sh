#!/bin/bash

if [ ! -e createLinks.sh ]; then
  if [ -e ../createLinks.sh ]; then
    cd ..
  fi
fi

./createLinks.sh >/dev/null
if [ -e kill_gdb.sh ]; then #{ killing debugger
  echo "killing debugger..."
  bash ./kill_gdb.sh
  rm -f kill_gdb.sh 2>/dev/null
fi #}

find ./ -name "*.o"         -exec rm -f "{}" \;
find ./ -name "*.elf"       -exec rm -f "{}" \;
find ./ -name "*.map"       -exec rm -f "{}" \;
find ./ -name "*.list"      -exec rm -f "{}" \;
find ./ -name "*.bin"       -exec rm -f "{}" \;
find ./ -name "compile.log" -exec rm -f "{}" \;
find ./ -name "*~"          -exec rm -f "{}" \;

rm -f openocd*
rm -f main.elf.S
rm -f *.elf.symbols
rm -Rf additionals/*
rm -f extensions.active/*
rm -f bin/*
rm -f compile_options.h
rm -f *.log
rm -f *.err
rm -f activate_project.sh.old
rm -f QtCreator/*.user
rm -f extensions.local/makefile.000_disable_asserts
rm -f ttc_extensions_active.*
rm -f source.c
rm -f compile.options
rm -f configs/memory_current.ld
rm -f QtCreator/*.files

#find ./ -name .updateLinks.sh -exec rm {} \;

