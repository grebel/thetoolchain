#!/usr/bin/perl

my $HighLevelInstall = shift(@ARGV);
unless ($HighLevelInstall) {
  print <<"END_OF_HELP";
$0 HIGH_LEVEL_INSTALL

Will look for all low-level install scripts in low_level/ that correspond 
to given HIGH_LEVEL_INSTALL and print their architecture names.

Note: This script must be run inside InstallData/

E.g.: $0 install_010_TTC_TIMER.sh

END_OF_HELP
  exit 5
}
my $PWD=`pwd`; chomp($PWD);
my $DirInstalls;
foreach my $Dir ('low_level', '../low_level', '../../low_level') {
  if (-d $Dir) { $DirInstalls = $Dir; last; }
}
unless (-d $DirInstalls) {
  print "$0 - ERROR: Missing folder low_level/ in '$PWD'! (Are you inside InstallData/ ?)\n";
  exit 10;
}

my %Architectures;
$HighLevelInstall =~ m/install_([0-9]+)_TTC_(.+)\.sh/;
my $ExtensionName = $2;
my $LowLevelInstalls = `ls $DirInstalls/`;
my @LowLevelInstalls = split("\n", $LowLevelInstalls);
map  {
  
  if ($_ =~ m/install_([0-9]+)_TTC_${ExtensionName}_([a-zA-Z0-9_]+)\.sh/) {
    my $Index        = $1;
    my $Architecture = $2;
    if ($Architecture) {
      $Architectures{$Architecture} = {
        Index => $Index
      };
    }
  }
} @LowLevelInstalls;
print join(" ", sort keys %Architectures)."\n";
