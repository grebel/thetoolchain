#!/bin/bash
#
#  Commandline tool for replacing text in files.
#
# Script written by Gregor Rebel 2012 
#

function replaceInFile() {   # File,Search,Replace     replaces all occurences of "$Search" in file $File by "$Replace"
    File="$1"
    Search="$2"
    Replace="$3"
    
    echo "replace $File: $Search"
    echo "     -> $File: $Replace"
    #echo "sed -e \"s|$Search|$Replace|g\" $File"
    sed -e "s|$Search|$Replace|g" $File >${File}_2
    mv -f ${File}_2 ${File}
}

File="$1"
Search="$2"
Replace="$3"

if [ "$Replace" == "" ]; then #{
  cat <<END_OF_HELP
$0 FILE SEARCH REPLACE
    Will replace every occurance of SEARCH in file FILE by REPLACE.

END_OF_HELP
  exit 10
fi #}

replaceInFile "$File" "$Search" "$Replace"
