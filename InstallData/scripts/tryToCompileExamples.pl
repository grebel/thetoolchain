#!/usr/bin/perl

unless (-e "extensions/") {
  print STDERR "$0 - ERROR: this script must be run inside a project directory (folder extensions/ is missing)!\n";
  exit 10;
}
my $Argument = shift(@ARGV);
my %Options = ( ALL => 0, UNTIL_ERROR => 0 );
unless (defined($Options{$Argument}) ) {
  print "$0 ".join('|', sort keys %Options)."\n";
  exit 0;
}
$Options{$Argument} = 1;

my $ActivateExamples = `ls ./extensions/activate.600_example_*`;
my @ActivateExamples = split("\n", $ActivateExamples);
my $AmountErrors = 0;
foreach my $ActivateExample (@ActivateExamples) {
  my $Pos = index($ActivateExample, '600_example_') + 12;
  if ($Pos > 12) {
    my $Example = substr($ActivateExample, $Pos, -3);
    print "trying to compile example '$Example'..";
    system("rm -f extensions.active/*");
    # activate basic set
    system("./activate_project.sh >/dev/null 2>&1");
    
    # ensure that only one example is activated
    system("rm -f extensions.active/makefile.600_example_*");
    system("activate.600_example_".$Example.".sh TEST >/dev/null 2>&1");
    
    my $LogFile="compile_Example_".$Example.".log";
    system("./compile.sh NOFLASH NOLOG >$LogFile 2>&1");
    my $ErrorOccured = `grep ERROR $LogFile`;
    if ($ErrorOccured) 
    {
      print "FAIL\n";
      $AmountErrors++;
      if ($Options{UNTIL_ERROR}) { exit 10; }
    }
    else { print "OK\n"; }
  }
}
if ($AmountErrors > 0) {
  print "$0 ERROR: $AmountErrors examples failed to compile!}\n";
  exit 11;
}
else {
  print "$0: All examples compiled successfully.}\n";
  exit 0;
}