#!/bin/bash

Dir="$1"
Pattern="$2"
WildCard="$3"

if [ "$Pattern" == "" ]; then ##                                                                                                           
  echo "$0 DIRECTORY STRING_PATTERN [FILE_PATTERN]"
  exit 0
fi #/
if [ "$WildCard" == "" ]; then ##                                                                                                          
  WildCard="*.h"
fi #/

if [ ! -d "$Dir" ]; then
  echo "$0: ERROR cannot find directory '$Dir'!"
  exit 5
fi

echo "find -L $Dir -name \"$WildCard\" -type f -exec grep -iHn \"$Pattern\" {} \; | less"
      find 2>/dev/null -L $Dir -name  "$WildCard" -type f -exec grep -iHn  "$Pattern"  {} \; | less
