#!/usr/bin/perl

if (1) { # check for problematic versions of installed libraries (libusb is known to cause trouble)
  my $InstalledLibUSB = `apt list --installed 2>/dev/null | grep libusb`;
  my @InstalledLibUSB = split("\n", $InstalledLibUSB);
  my %ProblematicLibraries;
  foreach my $BadVersion ('2:0.1.12-31', '2:1.0.21-2') {
    map { $ProblematicLibraries{$_} = 1; } 
    grep { index($_, $BadVersion) > -1; }  
    @InstalledLibUSB; 
  }
  if (%ProblematicLibraries) { # report problematic library versions
    my $Lines = "    ".join("\n    ", sort keys %ProblematicLibraries)."\n";
  
    print <<"END_OF_TEXT";
$0 - ERROR: Found library versions being known as problematic:
$Lines

If you experience issues with flashing or debugging your target, you should 
install different versions of these libraries!

END_OF_TEXT
  }
}

my %AllCommands = ( #{ 
    scan => { Text     => 'scan for known interfaces', # descriptive text
              Synopsis => 'scan',                      # added to synopsis line (describes all additional arguments for this command)
              Examples => [ 'scan' ],                  # list of examples for this command (script name is added automatically)
              Sub      => \&command_scan,              # function to be called
            },
    killall -ewq => { Text     => 'kill all running openocd tasks',
                 Synopsis => 'killall',
                 Examples => [ 'killall' ],
                 Sub      => \&command_killall,
               },
                 
  ); #}
my %KnownPIDVIDs = ( #{ stores all combinations of debug interface PID and VID 
                     '3748:0483' => { Name => 'STLINK v2.0 interface',          # name of interface for this PID-VID combination                                                                                                                                                                                                                                                                                                                   
                                      createConfig => \&create_config_StlinkV2  # function that will create single openocd configuration file
                                    },
#                     '*' => { Name => "all available and supported interfaces (must be escaped like \\* or '*')" }
                     # add more PID-VID combinations here
                   ); #}

my $Command = shift(@ARGV);

unless ($AllCommands{$Command}) {
  
  my $MaxLength = 0;
  map { if ($MaxLength < length($_)) { $MaxLength = length($_); } } keys %KnownPIDVIDs;
    
  my $KnownPIDVIDs = join( "\n    ",
                           map { substr($_.'           ', 0, $MaxLength).'  '.$KnownPIDVIDs{$_}->{Name}; }
                           sort keys %KnownPIDVIDs
                         );     
   
  my $Synopsis = join("|", map {
                                 '['.$AllCommands{$_}->{Synopsis}.']';
                               }
                           sort keys %AllCommands
                     );
  
  my $Examples = "  $0 ".
                 join("  $0 ", grep { $_; }
                               map { 
                                      if ( ref($AllCommands{$_}->{Examples}) eq 'ARRAY' ) {
                                        @{ $AllCommands{$_}->{Examples} };
                                      }
                                      else { undef; }
                                   }
                               sort keys %AllCommands
                     );
  
  print <<"END_OF_HELP";
$0 $Synopsis

Written by Gregor Rebel 2016
This script is part of TheToolChain an open source operating system and development environment for embedded systems (http://thetoolchain.com).

Flash and debug via multiple debug interfaces in parallel.

Known USB PID:VID combinations:
    $KnownPIDVIDs
  
Examples:
$Examples
  
END_OF_HELP

  if ($Command) {
    print stderr "\n$0 - ERROR: unknown command '$Command'!\n";
    exit 10;
  }

  exit 0;
}

$AllCommands{$Command}->{Sub}->();

sub command_scan {                # command: scans for all known debug interfaces
  # print "Looking for interfaces with PID:VID= ".join(", ", sort keys %Interface_PIDVID)."\n";
  print "$0: scanning for debug interfaces..\n";
  my @Interfaces = scan4Interfaces_OpenOCD();
  
  if (-d "configs/") {                                                                                    
    doIt("rm -fv configs/openocd_interface_usb_*");
  }
  
  my $Index = 1;
  foreach my $InterfaceRef (@Interfaces) {
    
    if (0) {
      print "Interface:\n  ".
            join( "\n  ", 
              map {
                "$_ = ".$InterfaceRef->{$_}
              } sort keys (%$InterfaceRef)
              )."\n";
    }
    my $FileName = "openocd_interface_usb_".$InterfaceRef->{Name}.'_'.$Index.'.cfg';
    $Index++;
                                      
    $FileName =~ s/ /_/g;
    my $Serial = $InterfaceRef->{Serial};
    print "$FileName  serial \"$Serial\"\n";
    if (-d "configs/") {
      foreach my $PIDVID (keys %Interface_PIDVID) {
        if ($KnownPIDVIDs{"$PIDVID"}->{createConfig}) {
          $KnownPIDVIDs{"$PIDVID"}->{createConfig}->("configs/".$FileName, $InterfaceRef);
        }
      }
    }
  }
}
sub command_killall -ewq {                 # command: killall -ewq running openocd tasks
  my $Tasks = `ps -Af | grep openocd | grep -v grep | grep -v bin/bash | grep -v gdb`;
  my @Tasks = split("\n", $Tasks);
  my $AmountTasks = scalar @Tasks;
  print "killing $AmountTasks openocd tasks..\n";
  
}
sub create_config_StlinkV2 {      # create configuration file for STLINK v2.0 interface
    my $FileName     = shift; # relative or absolute file name path of configuration file to create
    my $InterfaceRef = shift; # reference to %Interface structure as described at return of scan4Interfaces_FileSystem()

    writeFile($FileName, <<"END_OF_FILE");
# $InterfaceRef->{Name}
# USB: BusID=$InterfaceRef->{BusID} DeviceID=$InterfaceRef->{DeviceID} PID=$InterfaceRef->{PID} VID=$InterfaceRef->{VID}
# DeviceDir=$InterfaceRef->{DeviceDir}
#
hla_serial $InterfaceRef->{Serial}

END_OF_FILE
}
sub createDirectoryPath {         # creates a multi-level directory path from top to down
  my $DirectoyPath = shift; # relative or absolute directory path (without filename)
  
  my @DirectoyPath = split("/", $DirectoyPath);
  
  my $CurrentPath;
  map {
    unless ($CurrentPath) { $CurrentPath = $_; }
    else                  { $CurrentPath .= '/'.$_; }
    unless (-d $CurrentPath) { mkdir($CurrentPath); }
  } grep { $_; } @DirectoyPath; 
}
sub scan4Interfaces_OpenOCD {     # use patched openocd to find all attached interfaces with matching PID:VID combination
  writeFile("openocd_scan.cfg", <<"END_OF_FILE"); #{

hla_serial "just_scanning"

# finish configuration stage + proceed to run stage
init

reset init

# hardware reset processor with halt
reset halt

# check target state
poll

# hardware processor reset with run
reset run

#shutdown openocd daemon
shutdown

END_OF_FILE
#}
  
  my $Return = doIt("openocd -d3 --search additionals/999_open_ocd -f configs/openocd_interface.cfg -f configs/openocd_target.cfg -f openocd_scan.cfg 2>&1");
  my @Return = split("\n", $Return);
  
  my @Interfaces;
  my @Serials = map { 
                  my @Words = split(" ", $_);
                  
                  my $Serial   = $Words[3];
                  my $Name     = 'unknown';
                  my $BusID    = 1;                      # put all devices on same bus
                  my $DeviceID = 1 + scalar @Interfaces; # just enumerate all found devices
                  my $PID      = '';
                  my $VID      = '';
                  my $DeviceDi = '';
                  
                  push @Interfaces, {
                    Name      => $Name,
                    Serial    => $Serial,

                    BusID     => $BusID,
                    DeviceID  => $DeviceID,
                    PID       => $PID,
                    VID       => $VID,
                    DeviceDir => $DeviceDir
                  };
                }    
                grep { index($_, "Found serial number") > -1; }  # find all lines showing a serial number 
                @Return;
  
  my @Errors = grep { index($_, 'Error') > -1; } @Return;
  if (@Errors) { print "Errors:\n    ", join("\n    ", @Errors)."\n\n"; }

  unless (@Serials) {
    print <<"END_OF_INFO";
$0 - ERROR: Cannot find any interface!

Possible reasons:
- no debug interface connected
- openocd unpatched (must use specially patched openocd being installed with TheToolChain!)
- wrong debug interface activated (check your activate_project.sh to enable corresponding line in rank 350!)

END_OF_INFO
  }
  
  return @Interfaces; #{
  #
  # +--------------+
  # | @Interfaces  |
  # +--------------+   +-----------------------+
  # | $Interface1 ---> | %Interface            |      
  # |    :         |   +-----------------------+
  #                    | BusID    => $BusID    | <- USB bus ID where interface is connected
  #                    | DeviceID => $DeviceID | <- USB device ID where interface is connected
  #                    | PID      => $PID      | <- USB Product ID field
  #                    | VID      => $VID      | <- USB Vendor ID field
  #                    | Name     => $Name     | <- name field of USB descriptor                 
  #                    | Serial   => $Serial   | <- Serial number field (e.g. '"\x51\xC3\xBF\x6E\x06\x50\x67\x48\x57\x44\x57\x17\xC2\x87"')                     
  #                    | DeviceDir=> $DeviceDir| <- corresponding directory in local filesystem                     
  #                    +-----------------------+                                            
  #                                                                    
  #}
}
sub scan4Interfaces_FileSystem {  # scans /sys/bus/usb/devices/ for debug interfaces  
  my $Return = doIt("find /sys/bus/usb/devices/ -follow -maxdepth 2 -iname idProduct");
  
  my @Interfaces;
  map {
    my $LastSlashPos = rindex($_, '/');
    my $DeviceDir = substr($_, 0, $LastSlashPos);
    
    my $PID = strip(readFile($DeviceDir.'/idProduct'));
    my $VID = strip(readFile($DeviceDir.'/idVendor'));
    my $PidVid = $PID.':'.$VID;
    if ($Interface_PIDVID{$PidVid}) {
      my $Serial = readFile($DeviceDir.'/serial');
      chomp($Serial);
      $Serial = sprintf "%*v2.2X", ' ', $Serial; # "51 C3 BF 6E 06 50 67 48 57 44 57 17 C2 87"
      my @Serial = split(" ", $Serial);          # ('51', 'C3', 'BF', '6E', '06', '50', '67', '48', '57', '44', '57', '17', 'C2', '87')
      @Serial = map { "\\x".lc($_); } @Serial;       # ('\x51', '\xC3', '\xBF', '\x6E', '\x06', '\x50', '\x67', '\x48', '\x57', '\x44', '\x57', '\x17', '\xC2', '\x87')
      $Serial = join("", @Serial);              # '\x51\xC3\xBF\x6E\x06\x50\x67\x48\x57\x44\x57\x17\xC2\x87'
      
      my $Name = strip( readFile($DeviceDir.'/product') ).
                 ' v'.strip( readFile($DeviceDir.'/version') );
               
      my $BusID    = strip( readFile($DeviceDir.'/busnum') );
      my $DeviceID = strip( readFile($DeviceDir.'/devnum') );
      
      #print "$DeviceDir: PIDVID='$PidVid' Serial: '$Serial'\n"; #D

      push @Interfaces, {
        BusID     => $BusID,
        DeviceID  => $DeviceID,
        PID       => $PID,
        VID       => $VID,
        Name      => $Name,
        Serial    => '"'.$Serial.'"',
        DeviceDir => $DeviceDir
      };
      
    }
  } split("\n", $Return);
  
  return @Interfaces; #{
  #
  # +--------------+
  # | @Interfaces  |
  # +--------------+   +-----------------------+
  # | $Interface1 ---> | %Interface            |      
  # |    :         |   +-----------------------+
  #                    | BusID    => $BusID    | <- USB bus ID where interface is connected
  #                    | DeviceID => $DeviceID | <- USB device ID where interface is connected
  #                    | PID      => $PID      | <- USB Product ID field
  #                    | VID      => $VID      | <- USB Vendor ID field
  #                    | Name     => $Name     | <- name field of USB descriptor                 
  #                    | Serial   => $Serial   | <- Serial number field (e.g. '"\x51\xC3\xBF\x6E\x06\x50\x67\x48\x57\x44\x57\x17\xC2\x87"')                     
  #                    | DeviceDir=> $DeviceDir| <- corresponding directory in local filesystem                     
  #                    +-----------------------+                                            
  #                                                                    
  #}
}
sub readFile {                    # read in content of given file
  my $FilePath = shift;
  
  my $Content;
  my $Error;
  open(IN, "<$FilePath") or $Error = "$0 - ERROR: Cannot read from file '$FilePath' ($!)";
  dieOnError($Error);
  while (<IN>) { $Content .= $_; }
  close(IN);
  
  $Content =~ s/\\\n//g; # concatenate multiline strings
  
  return $Content;
}
sub writeFile {                   # write content to given file
  my $FilePath     = shift;
  my $Content      = shift;
  my $BackUpSuffix = shift; # if given and original file exists, a backup file is being created using this suffix
  
  # ensure that directoy exists
  my $LastSlashPos = rindex($FilePath, '/');
  if ($LastSlashPos > -1) { # file is to be created in different folder: ensure that folder exists
    my $DirPath = substr($FilePath, 0, $LastSlashPos);
    createDirectoryPath($DirPath);
  }
  
  if ($Verbose>1) { print "writing to disc: $FilePath\n"; }
  if ($BackUpSuffix) {
    if (-e $FilePath) {
      system("mv \"${FilePath}\" \"${FilePath}$BackUpSuffix\"");
    }
  }
  open(OUT, ">$FilePath") or die ("$0 - ERROR: Cannot write to file '$FilePath' ($!)");
  print OUT $Content;
  close(OUT);
  
  return $Content;
}
sub dieOnError {                  # dies if error given + prints caller stack
  my @ErrorMessages = grep { $_; } @_;
  unless (@ErrorMessages) { return; } # no error no die
  
  my $Level = 1;
  my @Stack;
  my ($package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash);

  do {
     ($package, $filename, $line, $subroutine, $hasargs,
      $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
      = caller($Level++);
      if ($subroutine) { push @Stack, "$subroutine() called from ${filename}:$line"; }
  } while ($filename);
  ($package, $filename, $line, $subroutine, $hasargs,
   $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
   = caller(0);
  my $PWD = `pwd`; chomp($PWD);
  push @ErrorMessages, "PWD='$PWD'";
  push @Stack, "dieOnError() called from ${filename}:$line";
  push @ErrorMessages, @Stack;

  print( "$0 - ERROR in $subroutine(): ".join("\n    ", @ErrorMessages)."\n" );
  exit 10;
}
sub doIt {                        # execute given command in shell and returns it's output
  my $CMD = shift;
  
  print "> $CMD\n";
  my $Result = `$CMD  2>&1`;
  
  return $Result;
}
sub strip {                       # removes invisible characters at begin + end of given string
  my $Line = shift || "";

  my %IsWhiteChar = ( ' ' => 1, "\n" => 1, "\r" => 1, "\t" => 1 ); 

  $Line =~ s/^\s+//;
  $Line =~ s/\s+$//;
  #$Line =~ s/(?:^ +)||(?: +$)//g;
  #$Line =~ s/^\ *([A-Z,a-z,0-9]*)\ *$/\1/g;
  #$Line =~ s/^\s+//;
  #$Line  =~ s/\s+$//;
  return $Line;
  
  my $lIndex = 0;
  while ( $IsWhiteChar{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsWhiteChar{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  return substr($Line, $lIndex, $rIndex - $lIndex + 1);

}
