#!/bin/bash

LineNo="$1"
Source="$2"
echo "$1 $2 $3 $4" >jedit.log

if [ "$LineNo" != "" ]; then
  LineNo=`perl -e "print substr('$LineNo',1);"`
  LineNo="+line:$LineNo"
fi
CMD="/usr/bin/jedit -reuseview -norestore $Source $LineNo"
echo ">$CMD" >jedit.log
$CMD &
#/usr/bin/jedit -reuseview $Source +Line:$Line

