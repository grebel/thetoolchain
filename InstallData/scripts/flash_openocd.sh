#!/bin/bash
#
# (c) Gregor Rebel 2010-2018

FILE="$1"
EXTRA_CFG="$2"

./createLinks.sh      || exit 11   # recreate symbolic links to TheToolChain
source _/SourceMe.sh               # update path variables to binary tools

if [ "$FILE" == "" ]; then
  
  cat <<END_OF_HELP
$0 FLASHFILE [ADDITIONAL.cfg]

  FLASHFILE       binary file to write into FLASH memory of connected microcontroller
  ADDITIONAL.cfg  additional openocd configuration file to use (e.g. defining serial number of programming interface to use)
END_OF_HELP

  _/openocd_scan_interfaces.pl \* >/dev/null 2>&1
  Files=`ls configs/openocd_interface_usb_* 2>/dev/null`
  for File in $Files; do
    echo "                  $File"
  done

  cat <<END_OF_HELP

Examples:

# flash file main.bin via first detected interface
$0 main.bin

# flash file main.bin via certain interface as described in $File
$0 main.bin $File

# this script will scan for available debug interfaces and create files in configs/
_/openocd_scan_interfaces.pl \*

# flash file main.bin via all detected interfaces:
find configs/openocd_interface_usb_* -exec _/flash_openocd.sh main.bin {} \;

END_OF_HELP

  exit 10
fi

if [ ! -e "$FILE" ]; then
  echo "$0 - ERROR: Cannot read flash-file '$FILE'!"
  exit 11
fi

if [ "$FILE" != "main.bin" ]; then
  # flash script always wants to flash file main.bin!
  cp -v "$FILE" main.bin
fi

TaskNameOpenOCD="openocd"
if [ "$EXTRA_CFG" != "" ]; then #{
  if [ ! -r $EXTRA_CFG ]; then
    echo "$0 - ERROR: Cannot read configuration file '$EXTRA_CFG' (PWD=`pwd`)!"
    exit 11
  fi
  #X Suffix=`perl -e "my \\$I=rindex('$EXTRA_CFG', '/'); print substr('$EXTRA_CFG', \\$I+1);"`
  Suffix=`perl -e "my \\$I=rindex('$EXTRA_CFG', '_'); my \\$B = substr('$EXTRA_CFG', \\$I+1); my \\$J=index(\\$B, '.'); print substr(\\$B, 0, \\$J);"`
  TaskNameOpenOCD="openocd_$Suffix"
  EXTRA_CFG="-f $EXTRA_CFG"
fi #}

Script2="bin/flash.sh"
cat >$Script2 <<END_OF_TEXT
#!/bin/bash

ERROR=""
source _/openocd.sh $TaskNameOpenOCD "--search additionals/999_open_ocd -f configs/openocd_interface.cfg -f configs/openocd_target.cfg $EXTRA_CFG -f configs/openocd_flash.cfg "
if [ "\$Error" != "" ]; then
  FlashWritten=\`grep wrote openocd.log | grep $FILE\`
  echo ""
  if [ "\$FlashWritten" == "" ]; then
    echo ""
    echo "ERROR: Cannot flash $FILE onto uC (check openocd.log)!"
  else
    echo "Successfully flashed $FILE onto uC."  
  fi
fi

END_OF_TEXT

source $Script2 | tee flash.log

if [ "$Error" != "" ]; then 
  echo "$0 - ERROR: $ERROR"
  echo "See flash.err for details!"
  mv flash.log flash.err
  exit 10
else
  rm $Script $Script2
fi


