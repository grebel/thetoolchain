#!/usr/bin/perl

my $Option = shift(@ARGV);
my $OnlyPrintTotal;
if ($Option eq 'TOTAL') { $OnlyPrintTotal = 1; }

my $NM='arm-none-eabi-nm --print-size --size-sort --radix dec --demangle --line-numbers';

my $ObjectFiles = `ls *.o`;
my @ObjectFiles = split("\n", $ObjectFiles);

my %Regions = ( BSS  => 'uninitialized data section (initial value == 0)',
                DATA => 'initialized data           (initial value != 0)'
              );
                
my %SumTotalSize; # sum of all sizes in all regions
my %TypesInRAM = ( B => 'BSS', b => 'BSS', 
                   D => 'DATA', d => 'DATA', 
                   G => 'DATA', g => 'DATA', 
                   S => 'DATA', s => 'DATA' 
                 );

foreach my $ObjectFile (sort @ObjectFiles) {
  my %Objects = collectObjects($ObjectFile);
  
  my @Rows;
  my %Sizes;
  my $TotalSize = 0;
  foreach my $Type (sort keys %TypesInRAM) {
    my $ObjectNamesRef = $Objects{$Type};
    foreach my $Name (sort { $ObjectNamesRef->{$a}->{Size} <=> $ObjectNamesRef->{$b}->{Size}; 
                           } keys %{ $ObjectNamesRef }) {
      my $ObjectRef = $ObjectNamesRef->{$Name};
      my $TypeName = $TypesInRAM{$Type};
      push @Rows, "$TypeName ".$Name."  ".$ObjectRef->{Size}." bytes";
      $SumTotalSize{$TypeName} += $ObjectRef->{Size};
      $Sizes{$TypeName}        += $ObjectRef->{Size};
      $TotalSize           += $ObjectRef->{Size};
    }
  }
  @Rows = formatColumns(\@Rows);
  my $Sizes = $TotalSize.'b';
  if ($TotalSize > 0) { $Sizes .= ': '.join(' + ', map { $_.'='.$Sizes{$_}.'b'; } sort keys %Sizes); }
  
  print "File: $ObjectFile (".$Sizes.")\n";
  if (@Rows) { print "    ".join("\n    ", @Rows)."\n\n"; }
}

my $SumTotal = 0;
map { $SumTotal += $SumTotalSize{$_}; } keys %SumTotalSize;
unless ($OnlyPrintTotal) { 
  print "\nTotal allocations: ".
        join(' + ', map { $SumTotalSize{$_}.' bytes '.strip($_)} sort keys %SumTotalSize).
        " = ".
        $SumTotal." bytes.\n".
        "\n".
        "Memory regions:\n    ".
        join("\n    ", map { substr($_."        ", 0, 6).$Regions{$_}; } sort keys %Regions).
        "\n\n";
  
}
else { print "$SumTotal\n"; }

sub strip() {                  # removes trailing and leading whitechars from given string
  my $String = shift;
  
  my %s_WhiteChar;
  unless (%s_WhiteChar) {
    $s_WhiteChar{"\n"} = 1;
    $s_WhiteChar{"\r"} = 1;
    $s_WhiteChar{"\t"} = 1;
    $s_WhiteChar{" "}  = 1;
  }
  while ( $s_WhiteChar{substr($String, 0, 1)} ) { substr($String, 0, 1) = undef; }
  while ( $s_WhiteChar{substr($String, -1, 1)} )   { substr($String, -1)   = undef; }
  
  return $String;
}
sub collectObjects() {         # collect all objects from single given object file
  my $FileName = shift;
  
  my $Lines = `$NM $FileName`;
  my @Lines = split("\n", $Lines);
  
  my %ObjectTypes;

  map {
    # 00000000 00000001 B ttc_usart_Amount_BlocksAllocated
    # 00000001 00000001 B ttc_usart_Amount_RxBytesSkipped
    my $Line = $_; 
    (my $Address, my $Size, my $Type, my $Name) = split(" ", $Line);

    my @Data = ( Size => $Size + 0, Address => $Address + 0, Type => $Type, Name => $Name, File => $FileName, Line => $Line );
    my %Data = @Data;
    $ObjectTypes{$Type}->{$Name} = \%Data;
    
  } @Lines;
  
  return %ObjectTypes; #{
  #     +------------+   +--------------+
  #     | Type1 =======> | %ObjectNames | <-- Objects stored under their name
  #     | ...        |   +--------------+   +---------------------+
  #                      | Name1 =========> |      %Object        | <-- single object from one object file
  #                      | ...          |   +---------------------+
  #                                         | Size    => $Size    | <-- object size (bytes) 
  #                                         | Address => $Address | <-- object address
  #                                         | Type    => $Type    | <-- type of object
  #                                         | Name    => $Name    | <-- name of object
  #                                         | File    => $File    | <-- filename of corresponding objectfile
  #                                         | Line    => $Line    | <-- original output line from $NM
  #                                         +---------------------+
  #}
}
sub formatColumns() {          # returns given lines with equal formatted columns
  my $LinesRef = shift;
  my @Lines = @$LinesRef;
  
  my @MaxColumnWidth;
  
  map {
        my $Line = $_;
        my @Line = split(" ", $Line);
        for (my $Index = 0; $Index < scalar(@Line); $Index++) {
          if ( $MaxColumnWidth[$Index] < length($Line[$Index]) ) {
            $MaxColumnWidth[$Index] = length($Line[$Index]); 
          }
        }
      } @Lines;
                      
  my @Formatted = map {
                        my $FormattedLine = '';
                        my @Line = split(" ", $_);
                        for (my $Index = 0; $Index < scalar(@Line); $Index++) {
                          my $Item = $Line[$Index];
                          while (length($Item) < $MaxColumnWidth[$Index]) { $Item .= ' '; }
                          $FormattedLine .= $Item.' ';
                        }
                        
                        $FormattedLine;
                      } @Lines;
                      
  return sort @Formatted;
}
