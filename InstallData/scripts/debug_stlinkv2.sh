#!/bin/bash
#
# Start OpenOCD GDB server + connect to it
#
# written by Greg Knoll, Gregor Rebel 2013-2015


[ -e required_version ] || { echo "You need to be in the project directory"; exit 1;}

Script1="configs/debug_stlinkv2.gdb"
if [ ! -e $Script1 ]; then
  cat <<END_OF_SCRIPT >$Script1
# Default GDB Startscript created by $0 at `date`
#

# connect to running openocd GDB-server
target extended-remote :4242

# load symbol tables
load

# set default breakpoints
break ttc_assert_halt_origin

# reset cpu and stop at reset handler
monitor reset halt

END_OF_SCRIPT
  echo "missing gdb start-script '`pwd`/$Script1'"
fi

killall -9 --wait st-util
st-util >st-util.log 2>&1 &
sleep 1
arm-none-eabi-gdb -q main.elf -x $Script1
killall -9 --wait st-util

