#!/usr/bin/perl
use strict;

my %Actions = ( #{ ); available action functions
                 add => \&actionAdd,
                enum => \&actionEnum,
            function => \&actionFunction,
              inject => \&actionInjectCode,
            register => \&actionRegister,
          states_new => \&actionStatesNew,
              ); #}

#{ create some usefull global variables
my $Space_ActionName; # amount of spaces as long as longest action name
map { 
  while (length($Space_ActionName) < length($_)) { $Space_ActionName .= ' '; }
} keys %Actions;
my $Space_ActionNameLength = length($Space_ActionName);

my $LastPos = rindex($0, '/');
my $MySelf = substr($0, $LastPos + 1);
my $S; while (length($S) < length($0)) { $S .= ' '; } # as much spaces as $0 for exact formatting
#}
#{ global variables

my $IsDevelopmentToolChain       = 0;
my $Checked4DevelopmentToolChain = 0;
my $GlobalStop                   = 0;

#}
#{ global settings
my $Verbose = 0;
if (1) { # enable hard shutdown in case of Ctrl+C signal (will always shut down)
  $SIG{'INT'} = $SIG{'__DIE__'} = $SIG{'__WARN__'} = sub { dieOnError("Shutting down ($!) ERROR:".join('|', @_)."\n"); }; 
}
else   { # only set global flag in case of Ctrl+C signal (requires adapted code)
  $SIG{'INT'} = $SIG{'__DIE__'} = $SIG{'__WARN__'} = sub { $GlobalStop = 1; }; 
}

#}global settings
#{ parse arguments

my @Arguments = @ARGV;
my $Action = lc(shift(@ARGV));
$Action = $Actions{$Action};
my @Options = @ARGV;
my $DateString = `date --utc +"%Y%m%d %T UTC"`; chomp($DateString); # datestring used for timestamps
my $Executable_qtcreator = (-x "qtcreator.sh") ? './qtcreator.sh' : 'qtcreator'; # select best available qtcreator executable 

#}
unless ($Action && @Options) { # wrong arguments: print help + exit
  
  my $Infos = join("\n\n\n", map {
                                my $Name = $_;
                                my $Info = $Actions{$Name}->('INFO');
                                $Info = prefixLines($Space_ActionName.'      ', $Info);
                                $Info = substr($Space_ActionName.'= '.$Name, -($Space_ActionNameLength + 4) ).
                                        substr($Info, $Space_ActionNameLength+4);
                                $Info;
                             } sort keys %Actions
                  );
  my $Examples = join("\n\n\n", map {
                                      my $Name = $_;
                                      my $Example = $Actions{$Name}->('EXAMPLE');
                                      $Example = prefixLines("", $Example);
                                      "<COMMAND>=$Name:\n".
                                      $Example;
                                    } sort keys %Actions
                     );
  
  open(my $Less, "| less") or die("Cannot pipe to less! ($!)");
  print $Less <<"END_OF_HELP"; #{


$MySelf <COMMAND> <OPTIONS>

Swiss army knife for automated source code generation for The ToolChain.

Arguments:

    The arguments descriptions below use a grammar to describe which options are expected and allowed.
    This grammar uses dedicated symbols to describe each option. Do not type any of these dedicated symbols!
    
    Symbols   Meaning
    < >       Every word inside angular brackets has to be replaced as described.
    ,         Multiple options are separated by commata.
    [ [] ]    Optional options are surrounded by rectangle brackets. You may, but do not have to provide them.
    ...       The previous option may be given multiple times.
    |         One of multiple options. Only one option may be given.
    
    
    <OPTIONS>  A combination of sub commands and their individual options as described below.
        
    <COMMAND>  Operation to take place:
$Infos

Examples:

$Examples


$MySelf was written by Gregor Rebel 2015-2018


Press q to quit!
END_OF_HELP
  close($Less);
#}
  exit 0;
}
unless (-e "ttc-lib") {        # started in wrong path: error + exit
  die("ERROR: Cannot find ttc-lib/! You must run this script from the base of your project folder.") 
}

my @Errors;
my $ProjectChanged = 0;
$Action->(@ARGV); # execute action
if (@Errors) { # ERROR + exit 12
  my $PWD = `pwd`; chomp($PWD);
  print "\n\n$PWD/$0 ".join(" ", @Arguments)."\n";
  foreach my $Error (@Errors) {
    print "    ERROR: $Error\n";
  }
  print "\n\n";
  exit 12;
}

if ($ProjectChanged) { # clean changed project
  print "Cleaning changed project..\n";
  system("./clean.sh");

  my $PWD = `pwd`; chomp($PWD);
  my @Parts = split("/", $PWD);
  @Parts = grep { $_; } @Parts; 
  my $ProjectName = $Parts[-1];
  print "updating settings of project $ProjectName..\n";
  system("_/updateProjectFiles.pl QtCreator/$ProjectName.files ../ >QtCreator/$ProjectName.files.log 2>&1");
}

sub actionAdd {                   # creates new files from templates and adds them to current project
  if ($_[0] eq 'INFO') {  # return info text
    return <<"END_OF_INFO";
<SOURCE_NAME1> [, <SOURCE_NAME2> [, ...]]

Creates new or adds existing source files to the current project.
If foo.c and one or more .h header files are given, then include
lines will be added to foo.c automatically.
<SOURCE_NAME>s are relative or absolute file-path of .c source- or 
.h header files to create or add.

Creating new source files means to
1) Create given directory path if it does not exist.
2) Copy existing template .c and .h files.
3) Replace placeholders inside new files.


Adding files to a TTC project means to
1) Add include lines to main.c.
2) Add object lines to makefile.

END_OF_INFO
    }
  if ($_[0] eq 'EXAMPLE') {  # return usage example
    return <<"END_OF_EXAMPLE";
$0 add foo.c foo.h                                    # default: add source + corresponding header file
$0 add foo                                            # shortcut for line above (creates foo.c foo.h in same folder)
$0 add foo_types.h                                    # creates + adds single header file
$0 add mylib/cool/stuff/abc.c mylib/cool/stuff/abc.h  # full directory paths will be added
END_OF_EXAMPLE
  }

  my ($FilesH, $FilesA) = checkFileNames(@_);
  
  createFiles($FilesH, $FilesA);
  addFiles($FilesH, $FilesA);
}
sub actionEnum {                  # scans or creates enumerations
    my %SubCommands = ( #{ ); available text parsers for register description files
                   scan => \&enums_scan,
                   add  => \&enums_add,
                 ); #}

    if ($_[0] eq 'INFO') {     # return info text
      my $Prefix = "";
      map { while (length($Prefix) < length($_)) { $Prefix .= ' '; } } keys %SubCommands;
      $Prefix .= '     ';
      
      my $SubCommandList = join("|", sort keys %SubCommands);
      my $SubCommands = join("\n\n", map {
                                     my $Name = $_;
                                     my $Info = $SubCommands{$Name}->('INFO');
                                     $Info = prefixLines($Prefix, $Info);
                                     $Info = substr($Prefix.'= '.$Name.'  ', - length($Prefix) ).
                                             substr($Info, length($Prefix));
                                     $Info;
                                  } sort keys %SubCommands
                            );
        return <<"END_OF_INFO";
$SubCommandList <SUB_<OPTIONS>

Reads, modifies or creates enumeration declarations.

<SUB_<OPTIONS>  Options for sub command.

Sub commands:
$SubCommands
END_OF_INFO
    }
    if ($_[0] eq 'EXAMPLE') {  # return usage example
      my $Examples = join("\n", map {
                                      my $Name = $_;
                                      my $Example = $SubCommands{$Name}->('EXAMPLE');
                                      $Example = prefixLines("    ", $Example);
                                      "<SUB_COMMAND>=$Name:\n".
                                      $Example;
                                   } sort keys %SubCommands
                        );
      return <<"END_OF_EXAMPLE";

$Examples
END_OF_EXAMPLE
  }

    my ($SubCommand, @Switches) = @_;
    unless ($SubCommands{$SubCommand}) { dieOnError("<COMMAND>=enums: Unsupported sub command '$SubCommand'!"); }
    shift(@_);
    $SubCommands{$SubCommand}->(@_);
}
sub actionFunction {              # inserts a new function into existing source code
  my %Types = ( public => 1, private => 1, feature => 1 );

  if ($_[0] eq 'INFO') {     # return info text
    
    my $TypeList = join("|", sort keys %Types);

    return <<"END_OF_INFO";
$TypeList <PROTOTYPE> <SOURCE_NAME>

Adds a new function to a source- and its header-file.
public          Public functions can be called from outside.
private         Private functions are only callable from inside same source file.
feature         Feature functions are special constructs for ttc_* files.
                A feature function consists of
                - high-level public ttc_DEVICE_*() function (prefix is added automatically)
                - public interface function ttc_DEVICE_interface_*()
                - several public low-level functions 
                  (created via TTC-Library/templates/create_DeviceDriver.pl)
<PROTOTYPE>     Function prototype of form 'RETURN FUNCTIONNAME(ARGUMENT1, ...)'.
<SOURCE_NAME>   Relative path to a .c source or .h header file.
                The corresponding header/source file will be found automatically.
                It is even possible to ommit the file name suffix.
END_OF_INFO
    }
  if ($_[0] eq 'EXAMPLE') {  # return usage example
    return <<"END_OF_EXAMPLE";
    $0 function public  'void calc(t_u8 A, t_u8 B)' foo.c  # foo.h will be found automatically
    $0 function public  'void calc(t_u8 A, t_u8 B)' foo    # short form of line above
    $0 function feature 'ttm_number reduce_angle( ttm_number Angle )' ttc-lib/ttc_math
    $S        # feature function: creates 
    $S        # - ttc-lib/ttc_math.c:ttc_math_angle_reduce()
    $S        # - ttc-lib/ttc_math.h:ttc_math_angle_reduce()
    $S        # - ttc-lib/interfaces/ttc_math_interface.c:_driver_math_angle_reduce()
    $S        # - ttc-lib/interfaces/ttc_math_interface.h:_driver_math_angle_reduce()
END_OF_EXAMPLE
  }

  my ($Type, $Prototype, $SourceName) = @_;
  
  $SourceName = strip($SourceName);
  if (substr($SourceName, 0, 1) eq './') { $SourceName = substr($SourceName, 2); } # remove leading ./
  
  unless ($Types{$Type}) { push @Errors, "Invalid function type '$Type'. Use one from [".join(', ', sort keys %Types)."]"; return; }

  my %Function = %{ parseFunctionPrototype($Prototype, $SourceName, $Type) };
  if ($Type eq 'feature') { # check prerequisites for feature function
    if ( substr($Function{SourceName}, 0, 4) ne 'ttc_') {
      push @Errors, "Feature functions can only be added to ttc_* files which are not interface files!"; return;
    }
    if ( index('_interface_', $Function{DirectoryPath}) > -1 ) {
      push @Errors, "Feature functions must not be added to ttc_*_interface_* files. Add it to high-level ttc_* file instead!"; return;
    }
  }
  #print "parsed function:\n".join("\n    ", map { $_.' => "'.$Function{$_}.'"'."\n    " } sort keys %Function); 
  
  unless ($Function{Error} || @Errors) { # create function prototype and definition
    print "creating $Type function $Function{Prototype} in files $Function{Header}, $Function{Source} ..\n";
    
    my $Header = ($Type ne 'private') ? readFile($Function{Header}) : ''; my $HeaderOrig = $Header;
    my $Source = readFile($Function{Source});                             my $SourceOrig = $Source;
    my $Short       = "ADD_SHORT_DESCRIPTION_HERE";
    my $Description = <<"END_OF_DESCRIPTION"; #{
ADD_MORE_DESCRIPTION_HERE
END_OF_DESCRIPTION
#}

    ($Source, $Header) = createFunction( \%Function, #{ # create + insert new function
                                        Source      => $Source,
                                        Header      => $Header,
                                        Short       => $Short,
                                        Description => $Description,
                                       ); #}
    
    $ProjectChanged += writeFileIfChanged($Function{Header}, $Header, $HeaderOrig);
    $ProjectChanged += writeFileIfChanged($Function{Source}, $Source, $SourceOrig);
    
    # Extra spaces might have been inserted after return type to format source code.
    # Create a search pattern without return type.
    my $FirstSpacePos = index($Function{Prototype}, ' ');
    my $FunctionWithoutReturn = ' '. strip( substr($Function{Prototype}, $FirstSpacePos + 1) );
    
    my @Header_LineNumbers = findLineNumbers($Function{Header}, 'ADD_MORE_DESCRIPTION_HERE');
    my @Source_LineNumbers = findLineNumbers($Function{Source}, $FunctionWithoutReturn);
    
    my @Header_EditorCalls = map { "$Executable_qtcreator $Function{Header}+".$_; } grep { $_; } @Header_LineNumbers;
    my @Source_EditorCalls = map { "$Executable_qtcreator $Function{Source}+".$_; } grep { $_; } @Source_LineNumbers;
    my $EditorCalls = join("\n       ", @Header_EditorCalls, @Source_EditorCalls);
    
    if ($Type eq 'feature') { # show user hint
        
      print <<"END_OF_TEXT"; #{
Feature function added: 
    $Prototype

Note: You have to update driver to propagate new feature functions into all low-level drivers:
    1) Write documentation for your new feature function $Function{Name}():
       $EditorCalls
       
    2) Propagate feature function into interface and all low-level drivers:
       TTC_Git="\$HOME/git/TheToolChain"      # path to git may vary on your system
       cd \$TTC_Git/TTC-Library/templates/
       ./create_DeviceDriver.pl $Function{TTC_Device} UPDATE

END_OF_TEXT
#}
  }
    else {
      print <<"END_OF_TEXT"; #{
Function added: 
    $Prototype

Edit documentation and implementation of your new function $Function{Name}():
     $EditorCalls

END_OF_TEXT
#}
    }
  }
}
sub actionInjectCode {            # inserts C-code into existing function
  if ($_[0] eq 'INFO') {     # return info text
    my @ValidIdentifiers = findPlace();
    my $ValidIdentifiers = join( "\n", map { "                ".$_; } @ValidIdentifiers );
    return <<"END_OF_INFO";
<SOURCE_NAME> <CODE> <PLACE1> [<PLACE2> [...]]

Inserts given C code at specified place.

<SOURCE_NAME>   Relative path to a .c source or .h header file.
                The corresponding header/source file will be found automatically.
                It is even possible to ommit the file name suffix.

<CODE>          Single or multiline string to be inserted.

<PLACEn>        One or more place identifiers. Places can be identified in several ways:
$ValidIdentifiers

END_OF_INFO
    }
  if ($_[0] eq 'EXAMPLE') {  # return usage example
    return <<"END_OF_EXAMPLE";
# insert single line declaration into header file
$0 inject test.h "void foo(t_u8 A);" before_first="INSERT_PROTOTYPES_ABOVE"

# insert multi line declaration into header file
$0 inject test.h <<"END_OF_CODE" before_first="INSERT_PROTOTYPES_ABOVE"
/* A test function that does virtually nothing
 *
 * \@param A  (t_u8)  given value is simply ignored
 */
void foo(t_u8 A);
END_OF_CODE

# add an assert line to block of asserts in function ttc_sdcard_prepare()
$0 inject ttc-lib/ttc_sdcard.c "Assert_SDCARD( LogicalIndex > 0, ttc_assert_origin_auto );" in_function="ttc_sdcard_prepare" after_asserts

# add an assert line to block of asserts in function ttc_sdcard_prepare() (there should be only one *prepare() function)
$0 inject ttc-lib/ttc_sdcard.c "Assert_SDCARD( LogicalIndex > 0, ttc_assert_origin_auto );" in_function="prepare" after_asserts

# insert function call test(); after assert block and the first following command
$0 inject ttc-lib/ttc_sdcard.c "test();" in_function="ttc_sdcard_get_configuration" after_asserts skip_command

END_OF_CODE
END_OF_EXAMPLE
  }

  my ($FilesH, $FilesA) = checkFileNames( shift(@_) );
  my $FilePath = shift(@$FilesA)->{Path};
  my $Code             = shift;
  my @PlaceIdentifiers = @_;
  
  if ($Code) {
    my $Source   = readFile($FilePath);
    my $Position = findPlace($FilePath, $Source, @PlaceIdentifiers);

    if ($Position > -1) { # inject code at found position
      if (1 || ($Verbose > 0) ) { print "    injecting ".length($Code)." bytes into '$FilePath' \@$Position\n"; }
      
      my $Indent;
      my $LineStartPos = findLineStart($Source, $Position);
      if (substr($Code, -1) ne "\n") { $Code .= "\n"; }
      if (substr($Code, 0, 1) ne ' ') { # code not indented: indent ourself
        my $TextBeforeInjectPoint = substr($Source, $LineStartPos, $Position - $LineStartPos);
        unless (strip($TextBeforeInjectPoint)) { # ony spaces before injection point: use as indent
          $Indent = $TextBeforeInjectPoint;
        }
      }
      $Source = substr($Source, 0, $Position).
                $Code.
                $Indent.substr($Source, $Position);
      writeFile($FilePath, $Source);
    }
  }
}
sub actionRegister {              # compile register structures from given description file 
    my %SubCommands = ( #{ ); available text parsers for register description files
                   stm1 => \&registers_parse_STM1 
                 ); #}

    if ($_[0] eq 'INFO') {     # return info text
      my $Prefix = "";
      map { while (length($Prefix) < length($_)) { $Prefix .= ' '; } } keys %SubCommands;
      $Prefix .= '      ';
      my $SubCommandList = join("|", sort keys %SubCommands);

      my $Parsers = join("\n\n", map {
                                    my $Name = $_;
                                    my $Info = $SubCommands{$Name}->('INFO');
                                    $Info = prefixLines($Prefix, $Info);
                                    $Info = substr($Prefix.'= '.$Name.'  ', - length($Prefix) ).
                                            substr($Info, length($Prefix));
                                    $Info;
                                 } sort keys %SubCommands
                      );
        return <<"END_OF_INFO";
<TEXT_FILE> <SOURCE_NAME> $SubCommandList NoCaseChange

Scans given text file for textual description of a memory mapped register.
Scanned register description is compiled into a C struct and appended to or replaced in given 
source file.

<TEXT_FILE>    Text file storing a textual description of one or more registers copied from a datasheet PDF.
               My recommendation is to use the table selection tool in okular to copy text from datasheet 
               tables as this tool avoids to mix up lines.
               A very good text editor with goot text macro support is jedit. It should have been installed 
               together with TheToolChain. The macros can save a lot of time when creating unified lines.

<SOURCE_NAME>  Relative of absolute file-path (.c or .h suffix may be ommitted) where to insert compiled
               C structs into. The C struct is inserted into <SOURCE_NAME>.h and one commented variable is 
               inserted into <SOURCE_NAME>.c. Files will be created if missing.
           
NoCaseChange   If given, field names will not be converted to CAMEL_CASE.

<PARSER>       Name of text parser to use:

$Parsers
END_OF_INFO
    }
    if ($_[0] eq 'EXAMPLE') {  # return usage example
      my $Examples = join("\n", map {
                                      my $Name = $_;
                                      my $Example = $SubCommands{$Name}->('EXAMPLE');
                                      $Example = prefixLines("    ", $Example);
                                      "<PARSER>=$Name:\n".
                                      $Example;
                                   } sort keys %SubCommands
                        );
      return <<"END_OF_EXAMPLE";

$Examples
END_OF_EXAMPLE
  }

    my ($TextFile, $SourceName, $SubCommand, @Switches) = @_;
    unless ($SubCommands{$SubCommand}) { dieOnError("<COMMAND>=register: Unsupported register parser '$SubCommand'!"); }
    unless (-r $TextFile)                 { dieOnError("<COMMAND>=register: Cannot read from register description text file '$TextFile'!"); }
    
    my %SourceNames;
    my $Suffix = lc(substr($SourceName, -2));
    if    ($Suffix eq '.c') {
      $SourceNames{source} = $SourceName;
      $SourceNames{header} = substr($SourceName, 0, -2).'.h'; 
    }
    elsif ($Suffix eq '.h') {
      if (substr($SourceName, -8) eq '_types.h') {
        $SourceNames{source} = substr($SourceName, 0, -8).'.c';
        $SourceNames{header} = $SourceName;
      }
      else {
        $SourceNames{source} = substr($SourceName, 0, -2).'.c';
        $SourceNames{header} = $SourceName;
      }
    }
    else {
      $SourceNames{source} = $SourceName.'.c';
      $SourceNames{header} = $SourceName.'.h';
    }

    # domain prefix will prefix struct- and variable-names
    my $DomainPrefix = lc( extractNakedFileName($SourceNames{source}) ).'_'; 
    
    print "parsing $SubCommand type register descriptions from '$TextFile'";
    my %Switches = ( NoCaseChange => 1 );
    if (@Switches) {
      map {
        unless ($Switches{$_}) {
          dieOnError("<COMMAND>=register: Unknown switch '$_' (Valid switches: ".join(" ", sort keys %Switches).")");
        }
      } @Switches;
      print " switches: ".join(" ", @Switches);
    }
    print "\n";
    
    my $RegistersRef = $SubCommands{$SubCommand}->($TextFile);
    registers_FixData($RegistersRef, $TextFile, @Switches); # will check and correct some parse errors
    my $MaxLength_RegisterDefinition = 0;
    my $MaxLength_StructHead         = 0;
    
    foreach my $RegisterName (sort keys %$RegistersRef) { # compile each register into one C struct 
      my $RegisterRef = $RegistersRef->{$RegisterName};
      my $RegisterNote = $RegisterRef->{Note};
      my $RegisterText = $RegisterRef->{Text};
      my $RegisterSize = $RegisterRef->{Size};
      if ($RegisterText) { $RegisterText = prefixLines("    // ", $RegisterText)."\n"; }
      
      my (@Bits, $MaxNameLength, $MaxSizeLength, $MaxIndexLength);
      foreach my $BitFieldRef (@{ $RegisterRef->{Bits} }) { # find max field lengths
        if ($MaxNameLength < length($BitFieldRef->{Name})) {
          $MaxNameLength = length($BitFieldRef->{Name});
        }
        if ($MaxSizeLength < length("$BitFieldRef->{Size}")) {
          $MaxSizeLength = length("$BitFieldRef->{Size}");
        }
        if ($MaxIndexLength < length("$BitFieldRef->{Index}")) {
          $MaxIndexLength = length("$BitFieldRef->{Index}");
        }
      }
      
      my ($MaxNameSpace, $MaxSizeSpace, $MaxIndexSpace);
      while (length($MaxNameSpace)  < $MaxNameLength)  { $MaxNameSpace  .= ' '; }
      while (length($MaxSizeSpace)  < $MaxSizeLength)  { $MaxSizeSpace  .= ' '; }
      while (length($MaxIndexSpace) < $MaxIndexLength) { $MaxIndexSpace .= ' '; }
      
      my @BitFieldLines;
      foreach my $BitFieldRef (@{ $RegisterRef->{Bits} }) { # compile all bit fields of this register
        # format fields
        my $BitFieldName  = substr($BitFieldRef->{Name}.$MaxNameSpace,   0, $MaxNameLength);
        my $BitFieldSize  = substr($MaxSizeSpace.$BitFieldRef->{Size},     -$MaxSizeLength);
        my $BitFieldIndex = substr($BitFieldRef->{Index}.$MaxIndexSpace, 0, $MaxIndexLength);
        my $BitFieldNote  = $BitFieldRef->{Note};
        
        my $FirstLine = "volatile unsigned $BitFieldName : $BitFieldSize; ";
        my $Prefix;
        my $PrefixLength = length($FirstLine);
        while (length($Prefix) < $PrefixLength) { $Prefix .= ' '; }
        $Prefix .= '// ';
         
        $FirstLine .= "// Bit $BitFieldIndex  $BitFieldNote";
        push @BitFieldLines, $FirstLine;
         
        my @Text = split("\n", prefixLines($Prefix, $BitFieldRef->{Text}));
        push @BitFieldLines, @Text;
      }
      my $BitFieldLines = join("\n", @BitFieldLines);
      $BitFieldLines = prefixLines("        ", $BitFieldLines);
      
      my $RegisterType = $DomainPrefix.lc($RegisterName)."_t";
      my $RegisterAllType;
      if ($RegisterSize <=  8) { $RegisterAllType = 't_u8  All';  }
   elsif ($RegisterSize <= 16) { $RegisterAllType = 't_u16 All'; }
   elsif ($RegisterSize <= 32) { $RegisterAllType = 't_u32 All'; }
   else                        {                   # t_u8 Bytes[]
     my $Bytes = int( ($RegisterSize + 7) / 8 ); 
     $RegisterAllType = 't_u8  Bytes['.$Bytes.']'; 
   }
        
      # first line is added later to build cute columns
      $RegisterRef->{StructHead} = "typedef union { // $RegisterType"; 
      if ($MaxLength_StructHead < length($RegisterRef->{StructHead})) { 
      $MaxLength_StructHead = length($RegisterRef->{StructHead}); }
      
      my $RegisterStruct = <<"END_OF_STRUCT";
$RegisterText
    volatile $RegisterAllType;
    struct {
$BitFieldLines
    } Bits;
} __attribute__( ( __packed__ ) ) $RegisterType;
END_OF_STRUCT
      
      $RegisterRef->{Struct} = $RegisterStruct;
      $RegisterRef->{Type}   = $RegisterType;

      #X print "compiled register $RegisterType\n"; 
    }
    foreach my $RegisterName (sort keys %$RegistersRef) { # format head line of register structs
      my $RegisterRef = $RegistersRef->{$RegisterName};
      while (length($RegisterRef->{StructHead}) < $MaxLength_StructHead) { $RegisterRef->{StructHead} .= ' '; }
      $RegisterRef->{StructHead} .= " - ".$RegisterRef->{Note};
      
      # insert formatted head line as first line of C-struct
      $RegisterRef->{Struct} = $RegisterRef->{StructHead}.
                               "\n".$RegisterRef->{Struct};
    }
        
    my $Source; my $Header;
    if (-r $SourceNames{source}) {
      $Source = readFile($SourceNames{source});
    }
    if (-r $SourceNames{header}) {
      $Header = readFile($SourceNames{header});
    }
    unless ($Source) { # create default content
      $Source = <<"END_OF_SOURCE"
/** { $SourceNames{source} ************************************************
 *
 * Automatically created example register variables
}*/

#include "$SourceNames{header}"

//{ Global Variables ***********************************************************

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}Global Variables

END_OF_SOURCE
    }
    unless ($Header) { # create default content
      my $Date = `date`; chomp($Date);
      my $Guard = uc( extractNakedFileName($SourceNames{header}) ).'_H';
      $Header = <<"END_OF_HEADER"
#ifndef $Guard
#define $Guard

/** { $SourceNames{source} ************************************************
 *
 * Automatically compiled register structs.
 * File created by $MySelf at $Date.
}*/


//{ Structure definitions ***************************************************

//InsertStructureDefinitons above (DO NOT DELETE THIS LINE!)

//}Global Variables

#endif //$Guard
END_OF_HEADER
    }
    
    my $MaxLength1 = 0; # length of dynamic column 1
    foreach my $RegisterName (sort keys %$RegistersRef) { # format register definition stage 1
      my $RegisterRef = $RegistersRef->{$RegisterName};
      my $RegisterType = $RegisterRef->{Type};

      $RegisterRef->{Define} = "volatile $RegisterType";
      if ($MaxLength1 < length($RegisterRef->{Define}) ) {
        $MaxLength1 = length($RegisterRef->{Define});
      }
    }
    my $Spaces1;
    while (length($Spaces1) < $MaxLength1) { $Spaces1 .= ' '; }

    my $MaxLength2 = 0; # length of dynamic column 2
    foreach my $RegisterName (sort keys %$RegistersRef) { # format register definition stage 2
      my $RegisterRef = $RegistersRef->{$RegisterName};
      my $RegisterType = $RegisterRef->{Type};

      # create name of global variable (file_VARIABLE)
      $RegisterRef->{Variable}   = $DomainPrefix.uc($RegisterName);
      
      $RegisterRef->{Define} = substr($RegisterRef->{Define}.$Spaces1, 0, $MaxLength1).
                               "  ".$RegisterRef->{Variable}.";";
                            
      if ($MaxLength2 < length($RegisterRef->{Define}) ) {
        $MaxLength2 = length($RegisterRef->{Define});
      }
    }
    my $Spaces2;
    while (length($Spaces2) < $MaxLength2) { $Spaces2 .= ' '; }

    foreach my $RegisterName (sort keys %$RegistersRef) { # format register definition stage 3
      my $RegisterRef = $RegistersRef->{$RegisterName};
      my $RegisterType = $RegisterRef->{Type};

      $RegisterRef->{Define} = substr($RegisterRef->{Define}.$Spaces2, 0, $MaxLength2).
                               "  // $RegisterRef->{Note}";
    }
    foreach my $RegisterName (sort keys %$RegistersRef) { # insert compiled structs into source- and header-file
      my $RegisterRef = $RegistersRef->{$RegisterName};
      my $RegisterStruct     = $RegisterRef->{Struct};
      my $RegisterType       = $RegisterRef->{Type};
      my $RegisterDefinition = $RegisterRef->{Define};
      
      my $Pos = index($Header, $RegisterType.';');
      if ($Pos > -1) { # structure already exists: replace it
        my $PosEOL2 = index($Header, "\n", $Pos); # position of end of line after struct
        #{
        my $PosBracket2 = rindex($Header, '}', $Pos);
        #{
        my $PosBracket1 = findMatchingBracket($Header, $PosBracket2, '}');
        my $PosEOL1 = rindex($Header, "\n", $PosBracket1); # position of end of line before struct
        
        print "update: struct   $RegisterDefinition\n";
        #X substr($Header, $PosEOL1 + 1, $PosEOL2 - $PosEOL1 + 1) = $RegisterStruct;
        $Header = substr($Header, 0, $PosEOL1+1).
                  $RegisterStruct.
                  substr($Header, $PosEOL2+1);
      }
      else {
        print "new:    struct   $RegisterType\n";
        $Header = insertIntoString( $SourceNames{header}, 
                                    $Header, 
                                    'InsertStructureDefinitons', 
                                    $RegisterStruct,
                                    PREVIOUS_LINE => 1
                                  );
      }
 
      $Pos = index($Source, $RegisterRef->{Variable}.';');
      if ($Pos > -1) { # variable already exists: replace it
        my $PosEOL1 = rindex($Source, "\n", $Pos); # position of end of line before struct
        my $PosEOL2 = index($Source, "\n", $Pos);  # position of end of line after struct
        print "update: variable $RegisterRef->{Variable}\n";
        my $Line = substr($Source, $PosEOL1+1, $PosEOL2 - $PosEOL1);
        my $Comment = (substr($Line, 0, 2) eq '//') ? '//' : ''; # check if line was commented out
        
        $Source = substr($Source, 0, $PosEOL1+1).
                  $Comment.$RegisterDefinition.
                  substr($Source, $PosEOL2);
      }
      else {
        print "new:    variable $RegisterRef->{Variable}\n";
        $Source = insertIntoString( $SourceNames{source}, 
                                    $Source, 
                                    'InsertGlobalVariables', 
                                    '// '.$RegisterDefinition."\n",
                                    PREVIOUS_LINE => 1
                                  );
      }
    }
    
    print "writing to file $SourceNames{source}\n";
    writeFile($SourceNames{source}, $Source, '.bak');
    print "writing to file $SourceNames{header}\n";
    writeFile($SourceNames{header}, $Header, '.bak');
}
sub actionStatesNew {             # creates new ttc_states statemachine in given source file 
  if ($_[0] eq 'INFO') {     # return info text
    return <<"END_OF_INFO";
<STATEMACHINE_NAME> <SOURCE_NAME> <STATE_NAME1> [, <STATE_NAME2> [, ...]]

Creates a new ttc_states statemachine in given source and corresponding header file.
Will create:
- States Enumeration
- State Configurations
- State Functions
- Initialization Function

Arguments
  <STATEMACHINE_NAME> Used as infix for all created functions and constants (CamElCase ist translated to cam_el_case).
  <SOURCE_NAME>       Path to a .c file with or without suffix (corresponding .h file must reside next to it).
  <STATE_NAMEn>       Name of an individual state. Given states are enumerated in given order. 
                      + First State
                        First states are always named "reset" and "init" (added if missing) in given list.
                      + Sub States
                        State names starting with sub_ will be created as substates. 
                        A substate must be called via ttc_states_call() and returns to calling state via
                        ttc_states_return(). The state to return to must not be pre-configured.

END_OF_INFO
    }
  if ($_[0] eq 'EXAMPLE') {  # return usage example
    return <<"END_OF_EXAMPLE";
    $0 states_new RunMe statemachine.c calc1 calc2 display

END_OF_EXAMPLE
  }

  #{ parse arguments
  my $StatemachineName = correctCamelCase( shift(@_) );  # name of statemachine to create
  my $SourceName       = shift(@_);                      # name of source file where to create statemachine
  my @StateNames       = correctCamelCase(@_);           # names of individual states

  if (substr($SourceName, -2, 1) eq '.') { $SourceName = substr($SourceName, 0, -2); } # remove file suffix to force checkFileNames() to generate .c/.h pair
  my ($FilesH, $FilesA) = checkFileNames($SourceName);
  checkFileExists( map { $_->{Path} } @$FilesA );
  my @Headers = grep { substr($_, -2) eq '.h' } map { $_->{Path}; } @$FilesA; # extract all paths to header files (shall be only one but we must store in array first)
  my @Sources = grep { substr($_, -2) eq '.c' } map { $_->{Path}; } @$FilesA; # extract all paths to source files (shall be only one but we must store in array first)
  my $PathHeader = shift(@Headers);
  my $PathSource = shift(@Sources);
  
  @StateNames = grep { $_ ne 'reset' } @StateNames; # reset cannot be used as state name!
  unshift(@StateNames, 'init');                              # ensure that second state is named init (doubled entry will be removed later)
  my %UniqueStateNames = map { $_ => 1 } @StateNames;        # create hash of unique state names
  @StateNames = grep { $UniqueStateNames{$_}; } @StateNames; # remove all double entries
  my $AmountStates = scalar @StateNames;
  
  my $FileNameNaked = $FilesA->[0]->{Name};  # naked filename (without path and .c/.h suffix)
  if (substr($FileNameNaked, -2, 1) eq '.') { $FileNameNaked = substr($FileNameNaked, 0, -2); } # remove file suffix to force checkFileNames() to generate .c/.h pair

  my $GlobalPrefix        = $FileNameNaked.'_tsm_'.$StatemachineName.'_'; # tsm is shorter than 'ttc_states_machine_'
  my $StateEnumPrefix     = $GlobalPrefix.'enum_';                        # prefix of all state number enums
  my $StateEnumType       = $StateEnumPrefix.'e';                         # type of state number enums
  my $StateDataType       = $GlobalPrefix.'data_t';                       # type of struct storing state data
  my $StateDataName       = $GlobalPrefix.'StateData';                    # variable name: Pointer to allocated state data
  my $StateFunctionPrefix = '_'.$GlobalPrefix.'state_';                   # prefix for all state function names
  my $StateConfigPrefix   = $GlobalPrefix.'config_';                      # prefix for all state configuration variables
  my $ArrayAllConfigs     = $GlobalPrefix.'AllConfigurations';            # variable name: array of all configurations
  my $Function_Prepare    = $GlobalPrefix.'prepare';                      # function name: preparing statemachine for operation
  my $Function_Run        = '_'.$GlobalPrefix.'task';                     # function name: runs new statemachine  
  my $Function_Monitor    = '_'.$GlobalPrefix.'monitor';                  # function name: function will be called on every state change
  
  my @StateEnums = ('reset', @StateNames, 'unknown'); # need two extra state numbers: reset, unknown
     @StateEnums          = map { $StateEnumPrefix.$_; }     @StateEnums; # compile names of state enums 
  my @StateFunctions      = map { $StateFunctionPrefix.$_; } @StateNames; # compile names of state functions
  my @StateConfigurations = map { $StateConfigPrefix.$_; }   @StateNames; # compile names of state configurations
  #}
  
  unless (@Errors) { # all prerequisites ok: create statemachine
    if (1) { # print info text
      my $Info_StateEnums          = "    ".join("\n    ", @StateEnums);
      my $Info_StateFunctions      = "    ".join("\n    ", @StateFunctions);
      my $Info_StateConfigurations = "    ".join("\n    ", @StateConfigurations);
      my $Info_Sources             = "    ".join("\n    ", map { $_->{Path}; } @$FilesA);
      
      print <<"END_OF_INFO";

Creating statemachine $StatemachineName for $AmountStates states:
  Creating in files:
    $PathSource
    $PathHeader

  State Enums:
$Info_StateEnums

  State Functions:
$Info_StateFunctions

  State Configurations:
$Info_StateConfigurations

END_OF_INFO
    }
    
    my $Source = readFile($PathSource); my $SourceOrig = $Source;
    my $Header = readFile($PathHeader); my $HeaderOrig = $Header;
    
    if (1) { # create state functions
      foreach my $StateFunction (@StateFunctions) {
        my $Prototype = "void $StateFunction( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );";
        my $FunctionRef = parseFunctionPrototype($Prototype, $PathSource, 'private');
        my $IsSubState = index($StateFunction, $StateFunctionPrefix.'sub_') > -1; # substate names must start with sub_
        my $ReturnCode; my $Info;
        if ($IsSubState) { # compile return code from sub state
          $ReturnCode = "    // ttc_states_return( MyStatemachine, ( void* ) NULL ); // returning to calling state (change NULL to any return value)";
          $Info = <<"END_OF_INFO";
    // This is a statemachine sub-state function:
    // - has to be called via ttc_states_call()
    // - returns to its calling state via ttc_states_return()
    // - calling state may obtain return value via ttc_states_get_return()
END_OF_INFO
        }
        else {
          $Info = <<"END_OF_INFO";
    // This is a regular statemachine state function:
    // - has to be called via ttc_states_switch()
    // - can switch to reachable states via ttc_states_switch()
END_OF_INFO
        }
        $FunctionRef->{Body} = <<"END_OF_BODY"; #{
$Info
    // only we know which type of pointer is stored because we set it before
    ${StateDataType}* MyStateData = ( ${StateDataType}* ) StateData;

    // Some usefull statemachine functions:
    // ttc_states_call( MyStatemachine, ${StateEnumPrefix}, NULL ); // call a substate (must be preconfigured as substate!)
    // ttc_states_switch( MyStatemachine,  ${StateEnumPrefix});    // switch to different state (state number must be reachable!)
$ReturnCode

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state
            break;
        }
        case states_transition_Return: { // came back from a sub-state via ttc_state_return()
            // void* ReturnValue = ttc_states_get_return( MyStatemachine );  // obtain return value from sub-state (if set)
            break;
        }
        default:                       { // state did not change since previous call
            break;
        }
    }

END_OF_BODY
#}
        my $Short       = "state function of statemachine $StatemachineName";
        my $Description = <<"END_OF_DESCRIPTION"; #{
DESCRIBE_THIS_STATE!
END_OF_DESCRIPTION
#}

        ($Source, $Header) = createFunction( $FunctionRef,
                                             Source      => $Source,
                                             Header      => $Header,
                                             Short       => $Short,
                                             Description => $Description,
                                           );
      }
    }
    if (1) { # create state enumeration
      my $Name = 'foo_e';
      my $Comment = <<"END_OF_COMMENT";
Each statemachine state is identified by a unique number.
Before initialization, the state is zero (reset). 
The first state for which a state function is called is always the init state. 
END_OF_COMMENT
      my @FunctionNames = @StateFunctions;
      my @Comments = map { "number of state implemented in ${_}()"; } @StateFunctions;
      unshift(@Comments, 'reset state is not handled by any function');
      push(@Comments,    'state numbers below are invalid');
      my $Enumeration = compileEnum(Name => $StateEnumType, 
                                 Comment => $Comment,
                                   Names => \@StateEnums,
                                Comments => \@Comments
                                   );
      $Header = insertIntoString( $PathHeader, 
                                  $Header,
                                  #{
                                  "//}ConstantDefines", 
                                  $Enumeration,
                                  #{
                                  IF_NOT_FOUND => "} $StateEnumType;"                                  
                                 );
    }
    if (1) { # create state data struct
      my @Entries = (
                { Name    => 'MyStatemachine',
                  Type    => 't_ttc_states_config*',
                  Comment => 'pointer to configuration of ttc_states device to use'
                },
                { Name    => 'Delay',
                  Type    => '// t_ttc_systick_delay',
                  Comment => "this type of delay is usable in single- and multitasking setup"
                },
              );
      my $Comment = <<"END_OF_COMMENT"; #{
Each statemachine function has access to a storage of this type.
Add as many fields as you require for your statemachine.
END_OF_COMMENT
#}
      my $StateDataStruct = compileStruct(Name => $StateDataType, Comment => $Comment, Entries => \@Entries);
      $Header = insertIntoString( $PathHeader, 
                                  $Header, 
                                  #{
                                  "//}StructureDeclarations", 
                                  $StateDataStruct,
                                  #{
                                  IF_NOT_FOUND => "} $StateDataType;"
                                );
    }
    if (1) { # create state configurations
      my @Entries;
      my @ArrayEntries;
      
      for (my $Index = 0; $Index < scalar @StateConfigurations; $Index++) {
        my $StateConfiguration = $StateConfigurations[$Index];
        my $StateEnum          = $StateEnums[$Index+1]; # skipping first state 'reset'
        my $StateFunction      = $StateFunctions[$Index];
        
        my $Entry = <<"END_OF_CODE";
const t_ttc_states_state $StateConfiguration = { // configuration of statemachine state $StateEnum

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) ) & $StateFunction,

    // Unique number identifying this state
    .Number = $StateEnum,

    // amount of reachable states listed below (keep in sync with amount of non-zero entries of ReachableStates!)
    .AmountReachable = 0,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A substate return is no switch. States being reached via ttc_states_return() are not listed in here!
    .ReachableStates = {
        //CHANGE_ME,
        0
    }
};
END_OF_CODE

        push @ArrayEntries, $StateConfiguration;
        $Source = insertIntoString( $PathSource, 
                                    $Source, 
                                    "//InsertGlobalVariables",
                                    $Entry,
                                    IF_NOT_FOUND => "const t_ttc_states_state $StateConfiguration"
                                    ); 
      }
      
      my $ArrayEntries = join("", map { "    &".$_.",\n"; } @ArrayEntries);
      my $AllConfigsArray = <<"END_OF_CODE"; #{ array storing configurations of all valid states

/** Store references of all valid state configurations in a zero terminated array.
 *
 *  Entries must be sorted by their Number value without vacancies!
 *  ${ArrayAllConfigs}[i].Number + 1 = ${ArrayAllConfigs}[i + 1].Number
 *
 */
const t_ttc_states_state* ${ArrayAllConfigs}[] = {
$ArrayEntries
    NULL                  // array must be zero terminated!
};  
END_OF_CODE
#}
      $Source = insertIntoString( $PathSource, 
                                  $Source, 
                                  "//InsertGlobalVariables",
                                  $AllConfigsArray,
                                  IF_NOT_FOUND => "const t_ttc_states_state* ${ArrayAllConfigs}[]"
                                ); 

    }
    if (1) { # insert required includes
      $Source = insertInclude( $PathSource, $Source, 'ttc-lib/ttc_systick.h',       'precise delays for single- and multitasking setup');
      $Source = insertInclude( $PathSource, $Source, 'ttc-lib/ttc_states.h',        'universal statemachine');
      $Header = insertInclude( $PathHeader, $Header, 'ttc-lib/ttc_systick_types.h', 'datatypes for precise delays for single- and multitasking setup');
      $Header = insertInclude( $PathHeader, $Header, 'ttc-lib/ttc_states_types.h',  'datatypes for universal statemachine');
    }
    if (1) { # insert prepare function
      my $FunctionBody = <<"END_OF_CODE"; #{
  // allocate memory for statemachine data from heap
  ${StateDataType}* $StateDataName = (${StateDataType}*) ttc_heap_alloc_zeroed( sizeof(${StateDataType}) );
  
  // create a new statemachine instance
  ${StateDataName}->MyStatemachine = ttc_states_create();
  
  // set list of all states
  ${StateDataName}->MyStatemachine->Init.AllStates = $ArrayAllConfigs;
  
  // we simply use our previously allocated task data
  ${StateDataName}->MyStatemachine->Init.StateData = &$StateDataName;
  
  // optional: register a monitor function to allow debugging of state transitions
  // (typecast avoids compiler warning)
  ${StateDataName}->MyStatemachine->Init.MonitorTransition =
      ( void ( * )( volatile void*, void*, volatile TTC_STATES_TYPE, volatile TTC_STATES_TYPE, e_ttc_states_transition ) )
      \&$Function_Monitor;
  
  // every nested ttc_state_push() call increases required stack size by 1 
  ${StateDataName}->MyStatemachine->Init.Stack_Size = 1;
  
  // prepare our new ttc_states device for operation
  ttc_states_init( ${StateDataName}->MyStatemachine );
  
  // register our run function as a new task (will be run automatically later)
  ttc_task_create(
                   $Function_Run,  // function to run as task
                   "tsm_$StatemachineName",  // thread name (just for debugging)
                   256,                   // stack size (adjust to amount of local variables of _example_states_task() AND ALL ITS CALLED FUNTIONS!)
                   $StateDataName,        // passed as argument to _example_states_task()
                   1,                     // task priority (higher values mean more process time)
                   NULL                   // can return a handle to created task
                 );
END_OF_CODE
#}
      my $Short       = "prepare statemachine $StatemachineName";
      my $Description = <<"END_OF_DESCRIPTION"; #{
Prepares a new instance of statemachine $StatemachineName for operation.
Will allocate required memory from heap.
END_OF_DESCRIPTION
#}
      my $Prototype   = "void $Function_Prepare()";
      
      my $FunctionRef = parseFunctionPrototype($Prototype, $PathSource, 'public');
      $FunctionRef->{Body} = $FunctionBody;
      ($Source, $Header) = createFunction( $FunctionRef,
                                           Source      => $Source,
                                           Header      => $Header,
                                           Short       => $Short,
                                           Description => $Description,
                                         );
    }
    if (1) { # insert run function
      my $FunctionBody = <<"END_OF_CODE"; #{
    ${StateDataType}* Data = ( ${StateDataType}* ) Argument; // we're expecting this pointer type

    // get pointer to statemachine configuration
    t_ttc_states_config* MyStatemachine = Data->MyStatemachine;

    do {
        // run our prepared statemachine
        ttc_states_run( MyStatemachine );

        ttc_task_yield(); // give cpu to other processes
    }
    while (TTC_TASK_SCHEDULER_AVAILABLE); // Tasks run endlessly if scheduler is available.

END_OF_CODE
#}
      my $Short       = "example single or multitasking task that will run statemachine $StatemachineName";
      my $Description = <<"END_OF_DESCRIPTION"; #{
This function can be registered to ttc_task_create() in both setups, single- and multitasking.
In single tasking setup, it is called frequently from ttc_task_scheduler().
In multi tasking setup, it runs its own event loop.

END_OF_DESCRIPTION
#}
      my $Prototype   = "void ${Function_Run}(void* Argument)";
      
      my $FunctionRef = parseFunctionPrototype($Prototype, $PathSource, 'private');
      $FunctionRef->{Body} = $FunctionBody;
      ($Source, $Header) = createFunction( $FunctionRef,
                                           Source      => $Source,
                                           Header      => $Header,
                                           Short       => $Short,
                                           Description => $Description,
                                         );
    }
    if (1) { # insert monitor function
      my $FunctionBody = <<"END_OF_CODE"; #{
    // place breakpoint here!

    // avoid compiler warnings about unused variables
    ( void ) Config;
    ( void ) MyStateData;
    ( void ) State_Current;
    ( void ) State_Previous;
    ( void ) Transition;

    volatile static t_u32 CallCounter = 0; // counts function calls
    const           t_u32 BreakAt     = 0; // set to value > 0 to enable breakpoint at certain function call
    CallCounter++;

    if ( BreakAt && ( CallCounter == BreakAt ) ) {
        ttc_assert_break_origin( ttc_assert_origin_auto ); // desired function call reached
    }
END_OF_CODE
#}
      my $Short       = "monitor of statemachine $StatemachineName";
      my $Description = <<"END_OF_DESCRIPTION"; #{
If configured during initialization, this function is called on every state change of statemachine $StatemachineName.
You typically do not implement statemachine functionality here. 
But it is a good place for a breakpoint or to print debug informations.
END_OF_DESCRIPTION
#}
      my $Prototype   = "void $Function_Monitor(t_ttc_states_config* volatile Config, ${StateDataType}* volatile MyStateData, $StateEnumType volatile State_Current, $StateEnumType volatile State_Previous, e_ttc_states_transition volatile Transition)";
      
      my $FunctionRef = parseFunctionPrototype($Prototype, $PathSource, 'private');
      $FunctionRef->{Body} = $FunctionBody;
      ($Source, $Header) = createFunction( $FunctionRef,
                                           Source      => $Source,
                                           Header      => $Header,
                                           Short       => $Short,
                                           Description => $Description,
                                         );
    }
    
    $ProjectChanged += writeFileIfChanged($PathHeader, $Header, $HeaderOrig);
    $ProjectChanged += writeFileIfChanged($PathSource, $Source, $SourceOrig);
  }
}
sub addFiles {                    # adds given files to current project
  my %FilesH = %{ shift() };
  my @FilesA = @{ shift() };

  unless (@FilesA) { return; }
  
  # read in makefile
  my $Makefile = "extensions.local/makefile.700_extra_settings";
  (-e $Makefile) || dieOnError("Cannot find file `pwd`/$Makefile");
  my $MakefileContent = readFile($Makefile);
  my $MakefileContentOrig = $MakefileContent;
  
  # read in main source code
  my $Mainfile = "main.c";
  my $MainfileContent = readFile($Mainfile);
  my $MainfileContentOrig = $MainfileContent;
  
  foreach my $File (@FilesA) {
    if ($File->{Suffix} eq 'c') {
      my $ObjectFile = $File->{Name}.'.o';
      my $SourcePath = $File->{Directory};

      if ($SourcePath) {
        print "adding source path $SourcePath to $Makefile ...\n";
        #"{"
        $MakefileContent = insertIntoString($Makefile, $MakefileContent, "#}EXTENSION_700_extra_settings", "vpath %.c $SourcePath\n");
      }
      
      print "adding object file $ObjectFile to $Makefile ...\n";
      # "{"
      $MakefileContent = insertIntoString($Makefile, $MakefileContent, "#}EXTENSION_700_extra_settings", "MAIN_OBJS += $ObjectFile\n");
    }
    if ($File->{Suffix} eq 'h') {
      my $IncludePath = $File->{Directory};

      if ($IncludePath) {
        print "adding include path $IncludePath to $Makefile ...\n";
        # "{"
        $MakefileContent = insertIntoString($Makefile, $MakefileContent, "#}EXTENSION_700_extra_settings", "INCLUDE_DIRS += -I$IncludePath\n");
      }

      print "adding include path $IncludePath to $Mainfile ...\n";
      #X $MainfileContent = insertIntoString($Mainfile, $MainfileContent, "//InsertIncludes", "#include \"$File->{Path}\"\n");
      $MainfileContent = insertInclude($Mainfile, $MainfileContent, $File->{Path}, 'main file');
    }
  }

  $ProjectChanged += writeFileIfChanged($Makefile, $MakefileContent, $MakefileContentOrig);
  $ProjectChanged += writeFileIfChanged($Mainfile, $MainfileContent, $MainfileContentOrig);
}
sub appendToString {              # appends given test to given string, if not already appended
  my $String = shift; # string to which text should be added to
  my $Append = shift; # text to append to $String
  
  my $AlreadyPresent = index( $String, strip($Append) ) + 1;
  unless ($AlreadyPresent) {
    $String .= $Append;
  }
  
  return $String;
}
sub callerStack {                 # returns stack of function calls
  my $Level = 1;
  my @Stack;
  my ($package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash);

  do {
     ($package, $filename, $line, $subroutine, $hasargs,
      $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
      = caller($Level++);
      if ($subroutine) { push @Stack, "$subroutine() called from ${filename}:$line"; }
  } while ($filename);
  ($package, $filename, $line, $subroutine, $hasargs,
   $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
   = caller(0);
   
   return @Stack;
}
sub calculateMaxLength {          # calculate maximum length of all strings in given list

  my $MaxLength = 0;
  map { if ($MaxLength < length($_)) { $MaxLength = length($_); } } @_;
    
  return $MaxLength;
}
sub checkFileExists {             # checks given list of filenames if a file exists for each one
  my @FileNames = @_;

  foreach my $FileName (@FileNames) {
    unless (-e $FileName) { push @Errors, "Missing file: '$FileName'"; }
    else {
      unless (-r $FileName) { push @Errors, "File not readable: '$FileName'"; }
      else {
        unless (-w $FileName) { push @Errors, "File not writable: '$FileName'"; }
      }
    }
  }
}
sub checkFileNames {              # checks given list of filenames for correctness and generates error messages accordingly
  my @FileNames = @_;

  my %ForbiddenDirectory = ( #{
                             '_/'                => 1,
                             'bin/'              => 1,
                             'dynamic_scales/.'  => 1,
                             'extensions/'       => 1,
                             'extensions.local'  => 1,
                             'mylib/'            => 1,
                             'ttc-lib/'          => 1,
                             'additionals/'      => 1,
                             'configs/'          => 1,
                             'examples/'         => 1,
                             'extensions.active' => 1,
                             'QtCreator/'        => 1
                          ); #}
  
  my %IsValidSuffix = ( h => 1, c => 1 );
  
  my @FilesA; my %FilesH;
  foreach my $FileName (@FileNames) {
    my $LastDotPos = rindex($FileName, '.');
    if ($LastDotPos == -1) { # file has no suffix: create a pair of header + source file
      push @FileNames, $FileName.'.c';
      push @FileNames, $FileName.'.h';
    }
    else {
      my $Suffix = lc( substr($FileName, $LastDotPos + 1) );
    
      unless ($IsValidSuffix{$Suffix}) { push @Errors, "Invalid filename suffix '$Suffix' in '$FileName'"; return({},[]); }
      if ($FilesH{$FileName}) { push @Errors, "Doubled filename '$FileName'"; return({},[]); }
      else { # filename is unique
        my $ShortName = substr($FileName, 0, $LastDotPos);
        my $LastSlashPos = rindex($ShortName, '/');
        $ShortName = substr($ShortName, $LastSlashPos + 1);
        my $Directory = substr($FileName, 0, $LastSlashPos + 1);
        my $FirstSlashPos = index($FileName, '/');
        my $FirstDirectory = substr($FileName, 0, $FirstSlashPos + 1);
        if ($ForbiddenDirectory{$FirstDirectory} && (! checkIsDevelopmentToolChain())) 
        { push @Errors, "Creating or adding files in project subfolder $FirstDirectory is not supported. Use a development TooChain for this!"; return({},[]); }
        $FilesH{$FileName} = { Name => $ShortName, 
                               Path => $FileName, 
                             Suffix => $Suffix,
                          Directory => $Directory 
                             };
        push @FilesA, $FilesH{$FileName};
      }
    }
  }

  return (\%FilesH, \@FilesA); #{
  #
  # +-----------+
  # | @return   |
  # +-----------+   +------------+
  # | FilesH -----> | %Files     | <-- hash storing file entries under their filename
  # |           |   +------------+   +-------------------------+
  # |           |   | FileName1 ---> | %File                   |
  # |           |   | ...        |   +-------------------------+
  # |           |   +------------+   | Name      => $Name      | <-- filename without filepath            (e.g. foo.c)
  # |           |                    | Path      => $Path      | <-- relative filepath including filename (e.g. lib/foo.c)
  # |           |                    | Suffix    => $Suffix    | <-- filename suffix                      (e.g. c)
  # |           |                    | Directory => $Directory | <-- relative path to directory of file   (e.g. lib/)
  # |           |                    +-------------------------+
  # |           |
  # |           |   +------------+
  # | FilesA -----> | @Files     | <-- array storing file entries in same order as defined in input
  # |           |   +------------+   +-------+
  # +-----------+   | FileName1 ---> | %File |
  #                 | ...        |   +-------+
  #                 +------------+
  #}
}
sub checkIsDevelopmentToolChain { # checks if current project directory belongs to a development toolchain
  unless ($Checked4DevelopmentToolChain) { # called first time: check if current project folder belongs to a development toolchain
    if ( -e "required_version") {
      my $Content = strip(readFile("required_version"));
      if ($Content eq 'devel') {
        $IsDevelopmentToolChain = 1;
        print "Found project using development ToolChain.\n";
      }
    }
    $Checked4DevelopmentToolChain = 1;
  }
  
  return $IsDevelopmentToolChain;
}
sub correctCamelCase {            # replaces CamelCase by camel_case in scalar or array context
  my @Strings = @_;
  my @Corrected;
  
  foreach my $String (@Strings) {
  
    my @Chars = ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    my %IsUppercase = map { $_ => 1; } @Chars;
    
    my $Corrected;
    for (my $Index = 0; $Index < length($String); $Index++) {
      my $Char = substr($String, $Index, 1);
      my $char = lc($Char);
      
      if ($IsUppercase{$Char}) {
        if ($Index > 0) { $Corrected .= '_'.$char; }
        else            { $Corrected .= $char; }
      }
      else              { $Corrected .= $char; }
    }
    push @Corrected, $Corrected;
  }
  
  if (wantarray()) { return @Corrected; }
  return join("", @Corrected);  
}
sub createDirectoryPath {         # creates a multi-level directory path from top to down
  my $DirectoyPath = shift; # relative or absolute directory path (without filename)
  
  my @DirectoyPath = split("/", $DirectoyPath);
  
  my $CurrentPath;
  map {
    unless ($CurrentPath) { $CurrentPath = $_; }
    else                  { $CurrentPath .= '/'.$_; }
    unless (-d $CurrentPath) { mkdir($CurrentPath); }
  } grep { $_; } @DirectoyPath; 
}
sub createFiles {                 # creates new files from templates and adds them to current project
  my %FilesH = %{ shift() };
  my @FilesA = @{ shift() };

  foreach my $File (@FilesA) {
    if (-e $File->{Path}) { # file already exists: skip it
      print "$0: skipping to create exisiting file '$File->{Path}'\n";
    }
    else {                  # file does not exist: create it
      my $TemplateFile = "ttc-lib/templates/new_file.".$File->{Suffix};
      my $Content = readFile($TemplateFile);
      unless ($Content) { # error + exit
        die("ERROR: Cannot read template file `pwd`/$TemplateFile !");
      }
      print "creating $File->{Suffix}-file $File->{Name} ...\n";
  
      $Content =~ s/<FILE>/$File->{Name}/g;
      $Content =~ s/Gregor Rebel/$ENV{USER}/g;
      $Content =~ s/<DATE>/$DateString/g;
  
      my @Parts = split("/", $File->{Path});
      my $Depth = -1 + scalar @Parts;
      my $IncludePrefix;
      while ($Depth > 0) { $IncludePrefix .= '../'; $Depth--; }

      if ($File->{Suffix} eq 'c') {
        my $lastDotPos = rindex($File->{Path}, '.');
        my $HeaderFile = substr($File->{Path}, 0, $lastDotPos + 1).'h';
        if ($FilesH{$HeaderFile}) { # corresponding header file given: insert include line
          my $HeaderFileName = extractFileName($HeaderFile);
          #$Content = insertIntoString($File->{Path}, $Content, '//InsertIncludes', "#include \"$HeaderFileName\"\n");
          $Content = insertInclude($File->{Path}, $Content, $HeaderFileName, '');
        }
        $Content = insertInclude($File->{Path}, $Content, "${IncludePrefix}ttc-lib/ttc_memory.h", 'memory checks and safe pointers');
        $Content = insertInclude($File->{Path}, $Content, "${IncludePrefix}ttc-lib/ttc_heap.h",   'dynamic memory and safe arrays');
        $Content = insertInclude($File->{Path}, $Content, "${IncludePrefix}ttc-lib/ttc_basic.h",  'basic functionality');
        #$Content = insertInclude($File->{Path}, $Content, "${IncludePrefix}ttc-lib/ttc_string.h", 'string compare and copy');
      }
      if ($File->{Suffix} eq 'h') {
        
        #X $Content = insertIntoString($File->{Path}, $Content, '//InsertIncludes', "#include \"${IncludePrefix}ttc-lib/ttc_basic.h\"  // basic datatypes\n");
        #X $Content = insertIntoString($File->{Path}, $Content, '//InsertIncludes', "#include \"${IncludePrefix}ttc-lib/ttc_memory.h\" // memory checks and safe pointers\n");
        #X $Content = insertIntoString($File->{Path}, $Content, '//InsertIncludes', "// #include \"${IncludePrefix}ttc-lib/ttc_heap.h\"   // dynamic memory and safe arrays\n");
        #X $Content = insertIntoString($File->{Path}, $Content, '//InsertIncludes', "// #include \"${IncludePrefix}ttc-lib/ttc_string.h\" // string compare and copy\n");
        $Content = insertInclude($File->{Path}, $Content, "${IncludePrefix}ttc-lib/ttc_basic_types.h",  'basic datatypes');
      }
      if ($Content) {
        print "creating new file (".length($Content)."b): $File->{Path}\n";
        writeFile($File->{Path}, $Content);
      }
    }
  }
}
sub compileEnum {                 # creates a new enumeration declaration and returns its code
  if (0) { # example usage (just copy and modify)
    my $Name = 'e_foo';
    my $Comment = <<"END_OF_COMMENT";
This is a cool new enumeration.
It has lots of items.
END_OF_COMMENT
    my @Names = qw(OK RESET FOO BAR);
    my %Values = (OK => 0, RESET => 3); 
    my $Enumeration = compileEnum(Name => $Name, Comment => $Comment, Names => \@Names, Values => \%Values);
  }
  my %Options  = @_; #{ Extra named arguments
                     # +---------------------------+ 
                     # | %Options                  |
                     # +---------------------------+
                     # | Name       => $Name       | <- datatype of enumeration
                     # | Comment    => $Comment    | <- text to use as comment for whole enum
                     # |                           |   +---------+
                     # | Names ======================> | @Names  | <- list of names of individual enum constants
                     # |                           |   +---------+
                     # |                           |   | $Name1  |
                     # |                           |   |    :    |
                     # |                           |   +---------+
                     # |                           |
                     # |                           |   +-------------------+
                     # | Values =====================> | %Values           | <- dedicated values to assign (missing names will be enumerated in classical way) 
                     # |                           |   +-------------------+
                     # |                           |   | $Name1 => $Value1 | <- dedicated value for first enum item
                     # |                           |   |    :              |
                     # |                           |
                     # |                           |   +-----------+
                     # | Comments ===================> | @Comments | <- list of comments for each entry in @Names
                     # |                           |   +-----------+
                     # |                           |   | $Comment1 |
                     # |                           |   |     :     |
                     # +---------------------------+
                     #
                     #}
  
   my $Comment;
   if ($Options{Comment}) { # compile comment
     my @Lines = split("\n", $Options{Comment});
     if (2 > scalar @Lines) {
       $Comment = "\n    // $Lines[0]\n";
     }
     else {
        $Comment = "\n    /* ".join("\n     * ", @Lines)."\n     */\n";
     }
   }

   my $MaxNameLength = calculateMaxLength(@{ $Options{Names} }) + 2;
   my $Spaces = ' ';
   while (length($Spaces) < $MaxNameLength) { $Spaces .= $Spaces; }
   my @Enums;
   for (my $Index = 0; $Index < scalar @{ $Options{Names} }; $Index++) {
     my $ItemName = $Options{Names}->[$Index];
     my $Entry = ( defined($Options{Values}->{$ItemName}) ) ? 
                 $ItemName.'='.$Options{Values}->{$ItemName} : # item has dedicated value
                 $ItemName;                                    # item is enumerated classical
     if ($Index + 1 < scalar @{ $Options{Names} }) {
         $Entry =  substr($Entry.','.$Spaces, 0, $MaxNameLength);
     }
     else {                                          # last name: not adding comma
         $Entry =  substr($Entry.$Spaces, 0, $MaxNameLength);
     }
     push @Enums,  $Entry.'  // '.$Options{Comments}->[$Index];
   }
   my $Enums = join("\n", map { "    $_" } @Enums);
   my $Enumeration = <<"END_OF_ENUM";
typedef enum { // $Options{Name}
$Comment$Enums
} $Options{Name};
END_OF_ENUM
  
   return $Enumeration;
}
sub compileStruct {               # creates a new structure declaration and returns its code
  if (0) { # example usage (just copy and modify)
    my $Name = 'foo_e';
    my $Comment = <<"END_OF_COMMENT";
This is a cool new structure definition.
It has lots of items.
END_OF_COMMENT
    my @Entries = (
                    { Name    => 'A',
                      Type    => 't_u8',
                      Comment => 'just a plain 8 bit unsigned value'
                    },
                    { Name    => 'B',
                      Type    => 't_u32*',
                      Comment => "pointer to a 32 bit unsigned value\nWhich may be used to point somewhere"
                    },
                  );
    my $Structure = compileStruct(Name => $Name, Comment => $Comment, Entries => \@Entries);
  }
  my %Options  = @_; #{ Extra named arguments
                     # +---------------------------+ 
                     # | %Options                  |
                     # +---------------------------+
                     # | Name       => $Name       | <- datatype of structure
                     # | Comment    => $Comment    | <- text to use as comment for whole structure
                     # |                           |   +----------+
                     # | Entries ====================> | @Entries | <- list of individual structure entries
                     # |                           |   +----------+   +---------------------+
                     # |                           |   | $Entry1 ---> | %Entry              |
                     # |                           |   |     :    |   +---------------------+
                     # |                           |                  | Name    => $Name    | <- name of this field
                     # |                           |                  | Type    => $Type    | <- datatype for this field
                     # |                           |                  | Comment => $Comment | <- single- or multiline comment for this field
                     # |                           |                  +---------------------+
                     # +---------------------------+
                     #}
  
   my $Comment;
   if ($Options{Comment}) { # compile comment
     my @Lines = split("\n", $Options{Comment});
     if (2 > scalar @Lines) {
       $Comment = "\n    // $Lines[0]\n";
     }
     else {
        $Comment = "\n    /* ".join("\n     * ", @Lines)."\n     */\n";
     }
   }
   
   my @Entries = grep { $_; } @{ $Options{Entries} };
   my $MaxNameLength = calculateMaxLength( map { $_->{Name}; } @Entries) + 2;
   my $MaxTypeLength = calculateMaxLength( map { $_->{Type}; } @Entries) + 2;
   my $Spaces = ' ';
   while (length($Spaces) < $MaxNameLength + $MaxTypeLength) { $Spaces .= $Spaces; }
   
   my @Formatted;
   for (my $Index = 0; $Index < scalar @{ $Options{Entries} }; $Index++) {
     my $Entry = $Options{Entries}->[$Index];
     my $Formatted;
     my $Comma;
     
     my $Formatted = substr($Entry->{Type}.$Spaces, 0, $MaxTypeLength).' '.
                     substr($Entry->{Name}.';'.$Spaces, 0, $MaxNameLength);
     if ($Entry->{Comment}) {
       my $BaseLength = length($Formatted) + 4;
       my @Comment = split("\n", $Entry->{Comment});
       $Formatted .= '  // '.shift(@Comment)."\n";
       if (@Comment) {
         $Formatted .= join("", map { substr($Spaces, 0, $BaseLength)."  // $_\n";  } @Comment);
       }
     }
     push @Formatted, $Formatted;
   }
   my $Formatted = join("", map { "    $_" } @Formatted);

   my $StructType = $Options{Name};
   my $StructName = substr($StructType, 0, -1).'s';
   my $Structure = <<"END_OF_ENUM";
typedef struct $StructName { // $Options{Name}
$Comment$Formatted
} $StructType;
END_OF_ENUM

   return $Structure;
}
sub createFunction {              # creates a new function declaration and/ or definition and inserts it into given source code strings
  if (0) { # example usage (just copy and modify)
    my $SourceName = '...';
    my $HeaderName = '...';
    my $Source      = readFile($SourceName);
    my $Header      = readFile($HeaderName);
    my $Short       = 'INSERT_SINGLE_LINE_DESCRIPTION';
    my $Description = <<"END_OF_DESCRIPTION"; #{
INSERT_MULTI_LINE_DESCRIPTION
END_OF_DESCRIPTION
#}
    my $Type        = ['public', 'private', 'feature']->[0];
    my $Prototype   = 'volatile void* calculate(t_u8 A, t_u8* B)';
    
    my $FunctionRef = parseFunctionPrototype($Prototype, $SourceName, $Type);
    $FunctionRef->{Body} = '// function body';
    ($Source, $Header) = createFunction( $FunctionRef,
                                           Source      => $Source,
                                           Header      => $Header,
                                           Short       => $Short,
                                           Description => $Description,
                                       );
  }
  
  my %Function = %{ shift(@_) }; # function description has as returned by parseFunctionPrototype()
  my %Options  = @_; #{ Extra named arguments
                     # +-----------------------------+ 
                     # | %Options                    |
                     # +-----------------------------+
                     # | Source      => $Source      | <- Content of header file where to insert 
                     # | Header      => $Header      | <- Content of source file where to insert
                     # | Short       => $Short       | <- Single line descriptive text to insert into first line of declaration
                     # | Description => $Description | <- Multiline descriptive text to insert into declaration 
                     # +-----------------------------+
                     #}

  unless ($Options{Short}) { $Options{Short} = 'ADD_SHORT_DESCRIPTION_HERE'; }
  my $HasLogicalIndex = (index($Function{Prototype}, '(t_u8 LogicalIndex') > -1) ? 1 : 0; # t_u8 LogicalIndex must be first argument to enable extra code to convert it to Config pointer!
  my $ConfigType      = 't_ttc_'.$Function{TTC_Device}.'_config*';
      
  my $NameLength = 0; my $TypeLength = 0;
  my $AddedExtraConfigArgument = 0;
  if ($HasLogicalIndex && ($Function{Type} eq 'feature') ) { # add argument Config for calling low-level driver
    print "adding argument '$ConfigType Config' for calling low-level driver\n";
    my @VariableConfig = parseVariableDeclarations("$ConfigType Config"); # create entry for configuration pointer
    unshift @{ $Function{ArgumentsList} }, @VariableConfig;               # configuration pointer will be first argument
    $AddedExtraConfigArgument = 1;
  }
  map { # calculate field lengths for formatted display
        if ($NameLength < length($_->{NameFixed}))  { $NameLength = length($_->{NameFixed});  }
        if ($TypeLength < length($_->{TypeString})) { $TypeLength = length($_->{TypeString}); }
      } @{ $Function{ArgumentsList} };

  my @Description = map { " * ".$_; } 
                    (
                      split("\n", $Options{Description}),
                      '',
                      map { 
                            my $VariableRef = $_; # format described as %Variable in parseVariableDeclarations() return
                            my $Name = $VariableRef->{Name};
                            my $Type = $VariableRef->{TypeString};
                            
                            my $Description;
                            $Description .= ($HasLogicalIndex && ($Name eq 'Config')) ?
                                            "Configuration of $Function{TTC_Device} device as returned by ttc_$Function{TTC_Device}_get_configuration()" : '';
                            $Description .= ($HasLogicalIndex && ($Name eq 'LogicalIndex')) ?
                                            "Logical index of $Function{TTC_Device} instance. Each logical device <n> is defined via COMPILE_OPTS += -DTTC_".uc($Function{TTC_Device})."<n> lines in extensions.active/makefile" : '';
  
                            while (length($Name) < $NameLength) { $Name .= ' '; }
                            $Type = "($Type)";
                            while (length($Type) - 2 < $TypeLength) { $Type .= ' '; }
  
                            "\@param $Name $Type  $Description";
                          }
                          @{ $Function{ArgumentsList} }
                    );

  my $Spaces; while (length($Spaces) < $NameLength) { $Spaces .= ' '; }
  my $ReturnType = "($Function{Return})";
  while (length($ReturnType) - 2 < $TypeLength) { $ReturnType .= ' '; }
  push @Description, ($Function{Return} eq 'void') ? '' : " * \@return ${Spaces}$ReturnType  ";
  my $Description = join("\n", @Description);
  if ($Description && (substr($Description,-1) ne "\n") ) { $Description .= "\n"; }

  my $Source_InsertMark = ($Function{Type} eq 'private') ? '//InsertPrivateFunctionDefinitions' : '//InsertFunctionDefinitions'; 
  my $FunctionNameIndent = ' ';
  if (1) { # try to find indent of function name directly before insert position to allow adaptive indenting 
    if (substr($Function{SourceName}, -2) eq '.c') {
      my $InsertPos = index( $Options{Source}, $Source_InsertMark );
      if ($InsertPos > -1) {
        #{
        my $LastClosingBracketPos = rindex( substr($Options{Source}, 0, $InsertPos), '}' );
        if ($LastClosingBracketPos > -1) {
          #{
          my $OpeningBracketPos = findMatchingBracket($Options{Source}, $LastClosingBracketPos, '}');
          my $StartOfLinePos = rindex( substr($Options{Source}, 0, $OpeningBracketPos), "\n" ) + 1; # start of line which contains opening curly bracket
          my $FunctionDefinition =  substr($Options{Source}, $StartOfLinePos, $OpeningBracketPos - $StartOfLinePos);
          my $ArgumentsStartPos = index($FunctionDefinition, '(');
          if ($ArgumentsStartPos >-1) {
            $FunctionDefinition = strip( substr($FunctionDefinition, 0, $ArgumentsStartPos) );
            my $LastSpacePos = rindex($FunctionDefinition, " ");
            while ( length($Function{Return}) + length($FunctionNameIndent) <= $LastSpacePos ) {
              $FunctionNameIndent .= ' ';
            }
          }
        }
      }
    }
  }

  my $FeatureDeclaration; my $AdditionalInitializer; my $ReturnCode; my $ReturnVariable;
  if ($Function{Type} eq 'feature') { # create declaration and definition part of high-level feature function
    my $DriverPrototype    = $Function{Prototype}; $DriverPrototype    =~ s/ ttc_/ _driver_/;
    my $DriverFunctionName = $Function{Name};      $DriverFunctionName =~ s/ttc_/_driver_/;

    my $TTC_Device = $Function{TTC_Device};
    if ($HasLogicalIndex)  { # replace 't_u8 LogicalIndex' -> 't_ttc_DEVICE_config Config' for driver function prototype 
      $DriverPrototype =~ s/t_u8 LogicalIndex/t_ttc_${TTC_Device}_config* Config/;
    }
    my $DriverMacro; 
    if (!$HasLogicalIndex) { # compile driver macro
      my $ArgumentNames = '('. # compile list of argument names
                          join(", ", map {
                                           #X my ($Type, $Name) = @$_;
                                           #X $Name;
                                           $_->{NameFixed}
                                         } 
                                     @{ $Function{ArgumentsList} }
                              ).
                          ')';
      $DriverMacro = '#define '.$Function{Name}.'  '.$DriverFunctionName;
      $DriverMacro = "//$DriverMacro  // enable to directly forward function call to low-level driver (remove function definition before!)";
    }
    $FeatureDeclaration = <<"END_OF_TEXT"; #{
${DriverPrototype};
$DriverMacro
END_OF_TEXT
#}
    if ($HasLogicalIndex)  {
      $AdditionalInitializer = <<"END_OF_TEXT"; #{
    $ConfigType Config = ttc_$Function{TTC_Device}_get_configuration(LogicalIndex);
END_OF_TEXT
#}
    }
    my $DriverCall = $DriverFunctionName.'('.
                     join( ', ',
                           grep { $_; }
                           map {
                                 my $Type = $_->{TypeString};
                                 my $Name = $_->{Name};
                                 
                                 if ($HasLogicalIndex) {
                                   if ($Name ne 'LogicalIndex') { $Name; }
                                   else                         { ''; } # remove logical index from low-level driver call
                                 }
                                 else {
                                   $Name; 
                                 }
                               }
                           @{ $Function{ArgumentsList} }
                         ).
                     ')';
    
    if ($Function{Return} ne 'void') { # compile return code for feature function with non-void return value
      my $Null = ( substr($Function{Return}, -1) eq '*' ) ? 'NULL' : '0';
      $ReturnVariable = "\n    $Function{Return} Result = $Null;\n";
      $ReturnCode = <<"END_OF_CODE";

    // pass function call to interface or low-level driver function
    Result = $DriverCall;

    // high-level code may check Result before returning it...

    return Result;
END_OF_CODE
      chomp($ReturnCode);
    }
    else {                             # compile return code for feature function with no return value
      $ReturnCode = <<"END_OF_CODE";

    // pass function call to interface or low-level driver function
    $DriverCall;

    // high-level code being executed after calling low-level driver..
END_OF_CODE
      chomp($ReturnCode);
    }
  }
  elsif  ($Function{Return} ne 'void') { # return code for generic (non feature) function
    $ReturnVariable = "\n    $Function{Return} Result = 0;\n";
    $ReturnCode     = "\n    return Result; // $Function{Return}";
  }
  
  my $IndentedFunctionPrototype = $Function{Return}.$FunctionNameIndent.$Function{Name}.'('.$Function{ArgumentsString}.')';
  my $Declaration = <<"END_OF_TEXT"; #{
/** $Options{Short}
 *
${Description} */
$Function{Prototype};
$FeatureDeclaration
END_OF_TEXT
#}
  
  my $AssertIsReadable; my $AssertIsWritable;
  if ($Function{TTC_Device}) { # select asserts for ttc_*.c sources
    $AssertIsReadable = 'Assert_'.uc($Function{TTC_Device}).'_Readable';
    $AssertIsWritable = 'Assert_'.uc($Function{TTC_Device}).'_Writable';
  }
  else {                       # select asserts for generic sources
    $AssertIsReadable = 'Assert_Readable';
    $AssertIsWritable = 'Assert_Writable';
  }
  
  my $ArgumentChecks = join("\n", grep { $_; }
                                  map {
                                        my $VariableRef = $_; # format described as %Variable in parseVariableDeclarations() return
                                        my $Name = $VariableRef->{Name};
                                        if ($VariableRef->{Pointer}) {
                                          unless ($AddedExtraConfigArgument && ( substr($Function{Name}, 0, 4) eq 'ttc_' ) ) {
                                            if ($VariableRef->{ConstPointer})
                                            { "    $AssertIsReadable($Name, ttc_assert_origin_auto); // always check incoming pointer arguments"; }
                                            else
                                            { "    $AssertIsWritable($Name, ttc_assert_origin_auto); // always check incoming pointer arguments"; }
                                          }
                                          else { undef; }
                                        }
                                        else { undef; }
                                      }
                                      @{ $Function{ArgumentsList} }
                           );
  
  my $HintText = ( ($Function{Type} eq 'feature') && ( substr($Function{Name}, 0, 4) eq 'ttc_' ) ) ?
                 '    // high-level code being executed before calling low-level driver...' : # hint for high-level feature function
                 '    // your_code_goes_here...';                                             # hint for non high-level or non feature function
                 
  my $Definition = <<"END_OF_TEXT"; #{
$IndentedFunctionPrototype {
$ArgumentChecks$AdditionalInitializer$ReturnVariable$Function{Body}
$HintText
$ReturnCode
}
END_OF_TEXT
#}
  my $Source = $Options{Source};
  my $Header = $Options{Header};
  my $IfNotFound = "$Function{Name}($Function{ArgumentsString}) ";
  $Source = insertIntoString( $Function{SourceName},  
                              $Options{Source}, 
                              $Source_InsertMark,
                              $Definition,  
                              IF_NOT_FOUND => $IfNotFound
                            );
  
  if ($Function{Type} eq 'private') { # private: insert declaration into source
    $Source = insertIntoString( $Function{SourceName},  
                                $Source,
                                '//InsertPrivateFunctionDeclarations',  
                                $Declaration,  
                                IF_NOT_FOUND => "$Function{Prototype};"
                              );
  }
  else {                              # public: insert declaration into header 
    $Header = insertIntoString( $Function{HeaderName}, #{ 
                                $Options{Header}, 
                                '//InsertFunctionDeclarations', 
                                $Declaration, 
                                IF_NOT_FOUND => "$Function{Prototype};" 
                              ); #}
  }

  return ($Source, $Header); # returning updated strings
}
sub dieOnError {                  # dies if error given + prints caller stack
  my @ErrorMessages = grep { $_; } @_;
  unless (@ErrorMessages) { return; } # no error no die
  
  my $Level = 0;
  my @Stack;
  my ($package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash);

  do {
     ($package, $filename, $line, $subroutine, $hasargs,
      $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
      = caller($Level++);
      if ($subroutine) { push @Stack, "$subroutine() called from ${filename}:$line"; }
  } while ($filename);
  ($package, $filename, $line, $subroutine, $hasargs,
   $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
   = caller(0);
  my $PWD = `pwd`; chomp($PWD);
  push @ErrorMessages, "PWD='$PWD'";
  push @Stack, "dieOnError() called from ${filename}:$line";
  push @ErrorMessages, @Stack;

  print( "$0 - ERROR in $subroutine(): ".join("\n    ", @ErrorMessages)."\n" );
  exit 10;
}
sub enums_add {                   # adds new enumeration to given source file
  if ($_[0] eq 'INFO') {  # return info text
    return <<"END_OF_INFO";
<SOURCE_FILE>    <ENUM_NAME> [<ITEM0>[=<VALUE0>] [, <ITEM1>[=<VALUE1>] [...]]]
<SOURCE_FILE>     Filepath to source.c or source.h where to insert new enum
<ENUM_NAME>       Base name of enumeration (will be prefixed by e_source_ automatically)
<ITEM*>           Name of item to insert in enumeration (will be prefixed by E_source_<ENUM_NAME>_ automatically). 
<ITEM*>=<VALUE*>  Each item can be given an indidividual value
END_OF_INFO
  }
  if ($_[0] eq 'EXAMPLE') {  # return usage example
    return <<"END_OF_EXAMPLE";
# Create new enumeration e_foo_error with items E_foo_error_none, E_foo_error_severe in foo.h
$0 enum add foo.h error=0 none=3 severe

END_OF_EXAMPLE
  }

  my $SourceFile = shift(@_);
  my $EnumName   = shift(@_);
  my $FileName = lc( extractNakedFileName($SourceFile) );
  if (substr($FileName, -6) eq '_types') { $FileName = substr($FileName, 0, - 6); } # remove trailing _types from prefix
  $FileName =~ s/ /_/g;
  
  my $prefix = lc($FileName.'_');
  
  my $ItemPrefix = 'E_'.$prefix.$EnumName.'_';
  $EnumName = 'e_'.$prefix.$EnumName;
  
  if (index($EnumName, '=') > -1) { dieOnError("Argument <ENUM_NAME>='$EnumName' must not contain '=' character!"); } 
  my @Items; my %Items;
  map { # parse all <ITEM*> arguments
        my $EqualPos = index($_, '=');
        my $Item; my $Value;
        if ($EqualPos > -1) {
          $Item  = substr($_, 0, $EqualPos);
          $Value = substr($_, $EqualPos + 1);
        }
        else { $Item = $_; }
        
        if ($Item) {
          if (substr($Item, 0, length($ItemPrefix)) eq $ItemPrefix) 
          { $Item = substr($Item, length($ItemPrefix)); }  # <ITEM*> already prefixed with E_filename_enumname_: remove prefix
          $Item = $ItemPrefix.$Item;
          unless ($Items{$Item}) { 
            $Items{$Item} = $Value;
            push @Items, $Item;
          }
        }
      } @_;
      
  my @ItemValues = map { 
  if (defined($Items{$_})) { $_.'='.$Items{$_}; } else { $_;} } @Items;
      

  print "Creating enumeration $EnumName in $SourceFile..\n";
  
  my $Comment = <<"END_OF_COMMENT";
Add description here!
END_OF_COMMENT
  my $Enumeration = compileEnum(Name => $EnumName, Comment => $Comment, Names => \@Items, Values => \%Items);

  my $Identifier = "typedef enum { // $EnumName"; #}
  my $Source = readFile($SourceFile); my $SourceOrig = $Source;
  $Source = insertIntoString($SourceFile, $Source, '//InsertEnums', $Enumeration, PREVIOUS_LINE => 1, IF_NOT_FOUND=>$Identifier);
  my $FileChanged = writeFileIfChanged($SourceFile, $Source, $SourceOrig);

  if ($FileChanged) { # show editor call to newly created enum
    my @LineNumbers = findLineNumbers($SourceFile, $Identifier);
    my @EditorCalls = map { "$Executable_qtcreator $SourceFile+".$_; } grep { $_; } @LineNumbers;
    my $EditorCalls = join("\n       ", @EditorCalls);

    print <<"END_OF_TEXT"; #{
Enumeration added: 
$Enumeration

You can now extend and documentat your new enumeration $EnumName:
     $EditorCalls

END_OF_TEXT
#}
  }

}
sub enums_scan {                  # scans given source files for declared enumeration
  if ($_[0] eq 'INFO') {  # return info text
    return <<"END_OF_INFO";
<SOURCE_FILE1> [, <SOURCE_FILE2> [, ...]]

END_OF_INFO
  }
  if ($_[0] eq 'EXAMPLE') {  # return usage example
    return <<"END_OF_EXAMPLE";
# scan files foo1. and foo2.c for declared enumerations and print them to stdout
$0 enum scan foo1.h foo2.c

END_OF_EXAMPLE
  }

  my ($TextFile) = @_;
  my $Text = strip( readFile($TextFile) ) ;
  
}
sub extractFileName {             # extracts filename from given filepath
  my $FilePath = shift;
  
  my $LastSlashPos = rindex($FilePath, "/");
  my $FileName = substr($FilePath, $LastSlashPos+1);
  
  return $FileName;
}
sub extractNakedFileName {        # extracts filename without suffix from given filepath
 
  my $FileName = extractFileName( shift() );
  my $LastDotPos = rindex($FileName, '.');
  if ($LastDotPos == -1) { $LastDotPos = length($FileName); } # filename contains no dot: use complete name
  $FileName = substr($FileName, 0, $LastDotPos);
  
  return $FileName;
}
sub findNextCommand {             # find position of next command in given source code
  my $FileName           = shift; # used to relate error messages to a certain input file
  my $Source             = shift; # C-source code to search
  my $StartPosition      = shift; # search will start at this position and search towards end of $Source
  my $PositionEndOfScope = shift() || findMatchingBracket($Source, $StartPosition, '{'); #} end of scope can be calculated externally to speed up multiple function calls in same scope
  my $Position = $StartPosition;
  if ($PositionEndOfScope == -1) { $PositionEndOfScope = length($Source); }
  
  my $PosNextSemicolon = index($Source, ';', $Position);
  if ($PosNextSemicolon > -1) {
    my $NextPosition = findNextNonComment($FileName, $Source, $PosNextSemicolon + 1);
    if ( ($NextPosition > ($PosNextSemicolon + 1)) && ($NextPosition <= $PositionEndOfScope) ) 
    { $Position = $NextPosition; }
  }
  
  return $Position; # == $StartPosition: no further command in same scope
}
sub findNextNonComment {          # find position of next non-comment text in given source code
  my $FileName      = shift; # used to relate error messages to a certain input file
  my $Source        = shift; # C-source code to search
  my $StartPosition = shift; # search will start at this position and search towards end of $Source
  my $FoundNonComment = 0;
  
  my $CurrentLine;
  my $Position = $StartPosition;
  my $PreviousPosition = $Position - 1;
  while (!$FoundNonComment) {
    my $LineEndPos;
    do {
      $LineEndPos = findLineEnd($Source, $Position);
      $CurrentLine = substr($Source, $Position, $LineEndPos - $Position);
      unless ($CurrentLine) { $Position++; }
    } while (!$CurrentLine && ($Position < length($Source)) );
    
    my $SingleLineCommentPos = index($CurrentLine, '//');
    my $MultiLineCommentPos  = index($CurrentLine, '/*');
    
    my $FirstCommentPos = $SingleLineCommentPos;
    my $CommentIsMultiline = 0;
    if ( ($MultiLineCommentPos > -1) && ($FirstCommentPos > $MultiLineCommentPos) )
    { $FirstCommentPos = $MultiLineCommentPos; $CommentIsMultiline = 1; }
    if ($FirstCommentPos > -1) { # found a comment start before end of line: check if there is a non-comment part before
      $FirstCommentPos += $Position;
      my $CommentStart = substr($Source, $FirstCommentPos, 2);
      my $PartBeforeComment = substr($Source, $Position, $FirstCommentPos - $Position);
      if ( $PartBeforeComment =~ m/\S+/ ) { # found non-whitespace character before comment start
        $Position = $FoundNonComment = $-[0] +  $Position; # position of first non-whitespace character
      }
      else {                                        # found no non-whitespace character before comment start: seek behind comment
        unless ( $CommentIsMultiline ) { # single-line comment: find start of next line
          $Position = findLineEnd($Source, $FirstCommentPos);
        }
        else {                           # multi-line comment: find end of comment lines
          my $CommentEndPos = index($Source, '*/', $Position) + 2;
          print "multi-line: ".substr($Source, $Position, $CommentEndPos - $Position);
          $Position = $CommentEndPos;
        }
      }
    }
    else {
      if ( $CurrentLine =~ m/\S+/ ) { # found non-whitespace character before end of line
        $Position = $FoundNonComment = $-[0] + $Position; # position of first non-whitespace character in current line
      }
      else { $Position = $LineEndPos + 1; } # empty line: continue in next line
    }
    if ( $Position >= length($Source) ) { $FoundNonComment = -1; last; } # no more non-comment text in $Source
    if ($PreviousPosition == $Position) { dieOnError("findNextNonComment($FileName, \@$StartPosition) - Endless loop detected. Check implementation!"); }
    $PreviousPosition = $Position;
  }
  if ( ($FoundNonComment > -1) && ($FoundNonComment < $Position) ) { dieOnError("findNextNonComment($FileName, \@$StartPosition) == $FoundNonComment - ERROR: somehow we ran backwards. Check implementation!"); }
  if ($FoundNonComment == -1) { $FoundNonComment = $StartPosition; }
  
  return $FoundNonComment; # ==$StartPosition: no more non-comment text found in $Source; >=0: position of next non-comment text in $Source
}
sub findFunction {                # find implementation of named function in given source code
  my $FileName     = shift; # used to relate error messages to a certain input file
  my $Source       = shift; # source code content in which to find place 
  my $FunctionName = shift; # name of function to look for
  my @Functions;
  
  my @FunctionDefinitons;
  my @StartPositions;
  if (index($FunctionName, '(') == -1) { # no parenthesis in function name: append ( to search pattern
    @FunctionDefinitons = $Source =~ m/$FunctionName\s*\(.*\)\s*\{/g; #}
    @StartPositions = @-;
  }
  else {                                 # parenthesis in function name: use as search pattern unchanged
    @FunctionDefinitons = $Source =~ m/$FunctionName\s*\{/g; #}
    @StartPositions = @-;
  }
  
  for (my $Index = 0; $Index < scalar @FunctionDefinitons; $Index++) {
    my $StartPosition     = $StartPositions[$Index];
    my $FunctionDefiniton = $FunctionDefinitons[$Index];
    my $CurlyBracketPos = $StartPosition + length($FunctionDefiniton) - 1; # last character is '{'  #}
    if (substr($Source, $CurlyBracketPos, 1) ne '{') #} 
    { dieOnError("findFunction($FileName, $FunctionName) - INTERNAL ERROR: Position of opening bracket miscalculated. Check implementation!"); }
    if (0 || ($Verbose >= 2) ) { print "findFunction('$FileName', '$FunctionName'): '$FunctionDefiniton' \@$StartPosition\n"; }
    
    my $LineStartPos = findLineStart($Source, $StartPosition);
    my $Prototype = substr($Source, $LineStartPos, $StartPosition + length($FunctionDefiniton) - $LineStartPos - 1);
    my $FunctionRef = parseFunctionPrototype($Prototype, $FileName, '');
    my $PositionBody = $StartPosition + length($FunctionDefiniton);
    $FunctionRef->{Pos_Body} = findNextNonComment($FileName, $Source, $PositionBody);
    if ($FunctionRef->{Pos_Body} > $PositionBody) { # found a function body: extract body
      $FunctionRef->{Pos_BodyEnd} = findMatchingBracket($Source, $CurlyBracketPos, '{'); #}
      if ($FunctionRef->{Pos_BodyEnd} > -1) {
        $FunctionRef->{Body} = substr($Source, $FunctionRef->{Pos_Body}, $FunctionRef->{Pos_BodyEnd} - $FunctionRef->{Pos_Body} );
      }
    }
    
    if (0 || ($Verbose >=3) ) { # Debug return hash
      print "findFunction($FileName, $FunctionName) = \n    ".
            join( "\n    ", map {
                                  if (ref($FunctionRef->{$_}) eq 'ARRAY') {
                                    $_.' => ['.
                                    join( ", ", map {
                                                     if (ref($_) eq 'ARRAY') {
                                                       ' ['.join(", ", @$_ ).'] ';
                                                     }
                                                     else {
                                                       $_;
                                                     }
                                                   }
                                          @{ $FunctionRef->{$_} }
                                        ).
                                    ']';
                                  }
                                  else {
                                    $_.' => "'.$FunctionRef->{$_}.'"';
                                  }
                                } sort keys %$FunctionRef
                )."\n";
    }

    push @Functions, $FunctionRef;
  }

  return @Functions; #{
  #
  # +-------------+
  # | @Functions  |
  # +-------------+   +-----------------------------+
  # | $Function1 ---> |   %Function                 |
  # |      :      |   +-----------------------------+
  #                   | ...                         | <- all fields returned by parseFunctionPrototype() 
  #                   | Body        => $Body        | <- function body (implementation between { .. })
  #                   | Pos_Body    => $Pos_Body    | <- start position of function body in $Source
  #                   | Pos_BodyEnd => $Pos_BodyEnd | <- end position of function body in $Source 
  #                   +-----------------------------+
  #
  #}
}
sub findLineNumbers {             # find numbers of all lines containing given string in given file
  my $FilePath = shift; # path to file to search
  my $String   = shift; # string to be found in file
  
  my @Lines = split( "\n", readFile($FilePath) );
  my @LineNumbers;
  my $LineNumber = 0;
  foreach my $Line (@Lines) {
    $LineNumber++;
    if (index($Line, $String) > -1) { push @LineNumbers, $LineNumber; }
  }
  
  return @LineNumbers;
}
sub findLineEnd {                 # finds position after last character in current line in given string
  my $Source   = shift; # source code content in which to search 
  my $Position = shift; # start position (somewhere in a line)
  
  $Position = index($Source, "\n", $Position);
  if ($Position == -1) { $Position = length($Source); }
  
  return $Position;
}
sub findLineStart {               # finds position of first character in current line in given string
  my $Source   = shift; # source code content in which to search 
  my $Position = shift; # start position (somewhere in a line)
  
  while ( ($Position > 0) && (substr($Source, $Position -1, 1) ne "\n") ) {
      $Position--;
  }
  #X while (substr($Source, $Position, 1) =~ m/\s+/) { $Position++; } # move to first non-whitechar 
  
  return $Position;
}
sub findMatchingBracket {         # find bracket matching to given one (will skip additional pairs of same type)
  my $Text     = shift;
  my $StartPos = shift() + 0;  # where to start search in $Text
  my $Bracket  = shift;        # opening bracket: search downwards; closing bracket: search upwards
  my $Level    = shift() + 1;
  
  my %OpeningBracket = ( '(' => ')', '{' => '}', '[' => ']' ); #{
  my %ClosingBracket = ( ')' => '(', '}' => '{', ']' => '[' ); #}
  
  my $MatchingPos = -1;
  
  my $LastMatchingPos = -1;
  if ($ClosingBracket{$Bracket}) { # search upward for matching opening bracket
    #  { ... { ... } { ... { ... } ... } ... }
    #  ^-Match                               ^-Start
    my $MatchingBracket = $ClosingBracket{$Bracket};
    my $NextSamePos;
    my $CurrentStartPos = $StartPos;
    do {
      $MatchingPos = rindex($Text, $MatchingBracket, $CurrentStartPos-1);
      $NextSamePos = rindex($Text, $Bracket,         $CurrentStartPos-1);
      if ($MatchingPos == -1) { last; }
      if ($MatchingPos < $NextSamePos) { # at least one additional pair of same brackets before matching position
        
        # use recursion to skip over other pairs of matching brackets
        $CurrentStartPos = findMatchingBracket($Text, $NextSamePos, $Bracket, $Level);
      }
      if (0) { # debug 
        print "L$Level: $MatchingPos > $NextSamePos\n";
        print "    matching \@$MatchingPos: '".substr($Text, $MatchingPos - 20, 40)."'\n";
        print "    same     \@$NextSamePos: '".substr($Text, $NextSamePos - 20, 40)."'\n";
      }
      if ($LastMatchingPos == $MatchingPos) { dieOnError("findMatchingBracket('".substr($Text, 0, 100)."', \@$StartPos, '$Bracket') - Endless loop detected. Check implementation!"); }
      $LastMatchingPos = $MatchingPos;
    } while ($MatchingPos < $NextSamePos);
  }
  if ($OpeningBracket{$Bracket}) { # search downward for matching closing bracket
    #  { ... { ... } { ... { ... } ... } ... }
    #  ^-Start                               ^-Match
    my $MatchingBracket = $OpeningBracket{$Bracket};
    my $NextSamePos;
    my $CurrentStartPos = $StartPos;
    do {
      $MatchingPos = index($Text, $MatchingBracket, $CurrentStartPos+1);
      $NextSamePos = index($Text, $Bracket,         $CurrentStartPos+1);
      if ($MatchingPos == -1) { last; }
      if ($MatchingPos > $NextSamePos) { # at least one additional pair of same brackets before matching position
        
        # use recursion to skip over other pairs of matching brackets
        $CurrentStartPos = findMatchingBracket($Text, $NextSamePos, $Bracket, $Level);
      }
      if (1) { # debug 
        print "L$Level: $MatchingPos < $NextSamePos\n";
        print "    '$MatchingBracket' matching \@$MatchingPos: '".substr($Text, $MatchingPos - 20, 40)."'\n";
        print "    '$Bracket' same     \@$NextSamePos: '".substr($Text, $NextSamePos - 20, 40)."'\n";
      }
      if ($LastMatchingPos == $MatchingPos) { dieOnError("findMatchingBracket('".substr($Text, 0, 100)."', \@$StartPos, '$Bracket') - Endless loop detected. Check implementation!"); }
      $LastMatchingPos = $MatchingPos;
    } while ($MatchingPos > $NextSamePos);
  }

  return $MatchingPos; # ==-1: no matching bracket found
}
sub findPlace {                   # find a place in given source code according to given place identifier
  my $FileName         = shift; # used to relate error messages to a certain input file
  my $Source           = shift; # source code content in which to find place 
  my @PlaceIdentifiers = @_;    # see hash of valid identifiers below!

  my $TestMode = ( $FileName eq '<TestOnly>') ? 1 : 0; # in testmode, we only check if given identifiers are currently supported
  
  my %ValidIdentifiers = ( #{
    'before_first='    => '<STRING>  before first line containing given string <STRING>',
    'before_last='     => '<STRING>   before last line containing given string <STRING>',
    'after_first='     => '<STRING>   line after first line containing given string <STRING>',
    'after_last='      => '<STRING>    line after last line containing given string <STRING>',
    'in_enum='         => '<NAME>         inside function of given name',
    'in_function='     => '<NAME>     inside function of given name',
    'in_struct='       => '<NAME>       inside struct of given name',
    'in_union='        => '<NAME>        inside struct of given name',
    'after_asserts'    => '          first block in every function is typically reserved for asserts checking arguments',
    'after_variables'  => '        second block in every function is typically reserved for variable declarations',
    'before_commands'  => '        before first line that is no declaration or assert',
    'before_return'    => '          before last return statement in a function',
    'at_start'         => '               before first line', 
    'at_end'           => '                 after last line',
    'skip_spaces'      => '            skip empty lines until next non-empty line',
    'skip_command'     => '           skip single line containing a command',
    'skip_commands='   => '<AMOUNT> skip given amount of lines containing a command'
    
  ); #}
  unless ($Source) { # missing source code: return a compiled list of valid place identifiers 
    my @ValidIdentifiers = map {
      my $Identifier = $_;
      findPlace("<TestOnly>", "<TestOnly>", $Identifier); # ask ourself if this identifier is supported
      my $NotYetImplemented = (@Errors) ? 'ToDo: ' : ''; 
      @Errors = ();

      $NotYetImplemented.$Identifier.$ValidIdentifiers{$Identifier}; 
    } sort keys %ValidIdentifiers;
    return sort { lc($a) cmp lc($b) } @ValidIdentifiers;
  }
  
  my $Position = -1;
  my $Offset = 0;
  my $AmountErrors = 0;
  my @ParsedIdentifiers; #{
  #
  # +---------------------+
  # | @ParsedIdentifiers  |
  # +---------------------+  +-------------------+
  # | $ParsedIdentifier1 --> | %ParsedIdentifier |
  # |         :           |  +-------------------+
  #                          | Name  => $Name    | <- one key from %ValidIdentifiers
  #                          | Value => $Value   | <- undef or value of an '*=' identifier (e.g. 'in_function=foo' => $Value='foo')
  #                          +-------------------+
  #}
  
  foreach my $PlaceIdentifier (@PlaceIdentifiers) {
    my $ParsedIdentifier;
    foreach my $ValidIdentifier ( reverse sort keys %ValidIdentifiers ) { # must run in reverse sorted order to detect skip_commands instead skip_command
      if (substr($PlaceIdentifier, 0, length($ValidIdentifier) ) eq $ValidIdentifier) {
        my $Value = substr($PlaceIdentifier, length($ValidIdentifier) );
        if ( substr($Value, 0, 1) eq '"' ) { $Value = substr($Value, 1); }      # remove leading "
        if ( substr($Value, -1) eq '"' )   { $Value = substr($Value, 0, -1); }  # remove trailing "
        $ParsedIdentifier = { Name => $ValidIdentifier, Value=> $Value };
        last;
      }
    }
    my $IdentifierName  = $ParsedIdentifier->{Name};
    my $IdentifierValue = $ParsedIdentifier->{Value};
    
    if ( (!$TestMode) &&
         (substr($IdentifierName, -1) eq '=') && (!$IdentifierValue) 
       ) { # *= identifier is missing value: error
      push @Errors, "Place identifier $IdentifierName requires a value!";  
    } 
    else { # process individual identifier
      unless ($TestMode) { print "    seeking place  $IdentifierName$IdentifierValue\n"; }
      if ($IdentifierName eq "before_first=") {
        my $Position2 = index($Source, $IdentifierValue);
        if ($Position2 > -1) {
          $Position = findLineStart($Source, $Position2);
        }
      }
   elsif ($IdentifierName eq "before_last=") {
        my $Position2 = rindex($Source, $IdentifierValue);
        if ($Position2 > -1) {
          $Position = findLineStart($Source, $Position2);
        }
      }
   elsif ($IdentifierName eq "after_first=") {
        my $Position2 = index($Source, $IdentifierValue);
        if ($Position2 > -1) {
          $Position = findLineEnd($Source, $Position2) + 1;
        }
      }
   elsif ($IdentifierName eq "after_last=") {
        my $Position2 = rindex($Source, $IdentifierValue);
        if ($Position2 > -1) {
          $Position = findLineEnd($Source, $Position2) + 1;
        }
      }
   elsif ($IdentifierName eq "after_asserts") {
       unless ($TestMode) {
          my $NewPosition = $Position;
          my $PosEndOfFunction = findMatchingBracket($Source, $NewPosition, '{'); #}
          if ($PosEndOfFunction == -1) { push @Errors, "Place identifier $IdentifierName requires to start inside a function body. Provide another place identifier before!"; }
          else {
            while ( ( lc(substr($Source, $NewPosition, 7)) eq 'assert_' ) && 
                    ($NewPosition < $PosEndOfFunction)
                  ) {
              # proceed to next line
              $NewPosition = findLineEnd($Source, $NewPosition);
              $NewPosition = findNextNonComment($FileName, $Source, $NewPosition);
            }
            if ( ($NewPosition > $Position) && ($NewPosition < $PosEndOfFunction) )
            { $Position = $NewPosition; }
          }
       }
     }
   elsif ($IdentifierName eq "after_variables") {
        
        push @Errors, "ToDo: findPlace() shall implement identifier '$IdentifierName'!";
      }
   elsif ($IdentifierName eq "before_commands") {
        
        push @Errors, "ToDo: findPlace() shall implement identifier '$IdentifierName'!";
      }
   elsif ($IdentifierName eq "before_return") {
        
        push @Errors, "ToDo: findPlace() shall implement identifier '$IdentifierName'!";
      }
   elsif ($IdentifierName eq "at_start") {
        
        push @Errors, "ToDo: findPlace() shall implement identifier '$IdentifierName'!";
      }
   elsif ($IdentifierName eq "at_end") {
        
        push @Errors, "ToDo: findPlace() shall implement identifier '$IdentifierName'!";
      }
   elsif ($IdentifierName eq "in_enum=") {
        
        push @Errors, "ToDo: findPlace() shall implement identifier '$IdentifierName'!";
      }
   elsif ($IdentifierName eq "in_function=") {
        my $Source2 = substr($Source, $Position + 1);
        my @FunctionDefinitions = grep { !$_->{IsDeclaration}; } findFunction($FileName, $Source2, $IdentifierValue);
        if (@FunctionDefinitions) {
          $Position += $FunctionDefinitions[0]->{Pos_Body} + 1;
        }
        if (0 || ($Verbose > 1) ) { print "function '$IdentifierValue' \@$Position\n"; }
      }
   elsif ($IdentifierName eq "in_struct=") {
        
        push @Errors, "ToDo: findPlace() shall implement identifier '$IdentifierName'!";
      }
   elsif ($IdentifierName eq "in_union=")  {
        
        push @Errors, "ToDo: findPlace() shall implement identifier '$IdentifierName'!";
      }
   elsif ($IdentifierName eq "skip_spaces")  {
        
       my $CurrentLine;
       do {
         my $PosEndOfLine = findLineEnd($Source, $Position);
         if ($PosEndOfLine > -1) {
           $CurrentLine = strip ( substr($Position, $PosEndOfLine - $Position) );
           unless ($CurrentLine) {
             my $NextPosition = findNextNonComment($FileName, $Source, $PosEndOfLine + 1);
             if ($NextPosition > ($PosEndOfLine + 1) ) { $Position = $NextPosition; }
             else { last; }
           }
         }
         else { last; }
       } while (!$CurrentLine);
     }
   elsif ($IdentifierName eq "skip_command")  {
        print "skipping single command..\n"; #D 
        $Position = findNextCommand($FileName, $Source, $Position);
      }
   elsif ($IdentifierName eq "skip_commands=")  {
        my $Amount = $IdentifierValue + 0;
        my $PreviousPosition = $Position;
        my $PositionEndOfScope = findMatchingBracket($Source, $Position, '{'); #}
        while ($Amount) {
          $Position = findNextCommand($FileName, $Source, $Position, $PositionEndOfScope);
          if ($PreviousPosition < $Position) { $Amount--; }
          else                               { $Amount = 0; } # no progress: abort skipping
          $PreviousPosition = $Position;
        }
      }
   else { # unknown place identifier
        push @Errors, "ToDo: findPlace() unknown place identifier '$PlaceIdentifier'!";
      }
    }
    
    if ($AmountErrors == scalar @Errors) { push @ParsedIdentifiers, $ParsedIdentifier; }
    else                                 { $AmountErrors = scalar @Errors;}
  }
  
  $Position += $Offset;
  
  if (0 || ($Verbose > 1) ) { 
    print "$0: findPlace('$FileName', ".
          join(', ', map { ($_->{Value}) ? $_->{Name}."'$_->{Value}'" : $_->{Name} } @ParsedIdentifiers).
          ") = $Position\n"."    ".
          join("\n    ", callerStack())."\n\n"; 
  }
  return $Position; # ==-1: no position found; >=0: character position inside $Source 
}
sub insertIntoString {            # inserts given text into given string before insert mark
  my $FileName   = shift;  # used as reference in error-message 
  my $String     = shift;  # string to insert into
  my $InsertMark = shift;  # will search for position of $InsertMark in $String and insert $Text before
  my $Text       = shift;  # text to insert
  my %Options = @_;
  my $Quiet           = $Options{QUIET};          # ==1: no error message if InsertMark is not found
  my $InPreviousLine  = $Options{PREVIOUS_LINE};  # ==1:insert $Text in line before $InsertMark; ==0: insert in same line directly before $InserMark
  my $Prefix          = $Options{PREFIX};         # if given, will be added in front of $Text (e.g. '#' to insert a comment)
  my $IfNotFound      = $Options{IF_NOT_FOUND} || $Text; # if given, $Text will only inserted if $IfNotFound was not found in $String 
  
  my $AlreadyPresent = index($String, $IfNotFound) + 1;
  unless ($AlreadyPresent) { # Text not yet included in String: insert it
    my $InsertPos = index($String, $InsertMark);
    if ($InsertPos > -1) {
      if ($InPreviousLine) { # find position of previous line
        my $LastSlashPos = rindex(substr($String, 0, $InsertPos), "\n"); # position of last newline before insert mark (end of previous line)
        if ($LastSlashPos > -1) {
          $InsertPos = $LastSlashPos + 1;
        }
      }
      #X substr($String, $InsertPos, 0) = $Prefix.$Text;
      $String = substr($String, 0, $InsertPos).
                $Prefix.$Text.
                substr($String, $InsertPos);
    }
    elsif (!$Quiet) { # ERROR
      my $PWD = `pwd`;
      chomp($PWD);
      print "$0 - insertIntoString($FileName) - ERROR: Missing insert mark '$InsertMark'\n";
      print "     Insert this line into file '$PWD/$FileName':\n";
      print "       $InsertMark  above (DO NOT DELETE THIS LINE!)\n";
      print "\n";
      dieOnError("Driver Creation Canceld!");
    }
  }
  
  return $String;
}
sub insertInclude {               # inserts include line for given header file into given source code
  my $FileName    = shift;  # used as reference in error-message 
  my $String      = shift;  # string to insert into
  my $IncludePath = shift;  # relative path to file to include
  my $Comment     = shift;  # single line comment 
  
  my $Source = insertIntoString( $FileName, 
                                 $String, 
                                 "//InsertIncludes",
                                 "#include \"$IncludePath\" // $Comment\n",
                                 IF_NOT_FOUND => "#include \"$IncludePath\""
                               ); 

  return $Source;  
}
sub nameConstant {                # corrects given constant name (all uppercased, CamelCase -> CAMEL_CASE)
  my @Strings = @_;
  my @Corrected;
  
  foreach my $String (@Strings) {
                                          #    "  CamelCase  "
    $String =~ s/\s//g;                   # -> "CamelCase"
    $String = correctCamelCase($String);  # -> "camel_case"
    $String = uc($String);                # -> "CAMEL_CASE"
    push @Corrected, $String;
  }
  
  if (wantarray()) { return @Corrected; }
  return join("", @Corrected);  
}
sub nameVariable {                # corrects given variable name (first character uppercased)
  my $String        = shift; # name of variable to correct
  
  $String =~ s/ //g;
  my $PointerPrefix;
  while (substr($String, 0, 1) eq '*') { $PointerPrefix .= '*'; $String = substr($String, 1); }
  $String = uc(substr($String, 0, 1)).substr($String, 1); # uppercase first character
  
  return ($PointerPrefix, $String);
}
sub nameFunction {                # corrects given function name (all lowercased with underscores)
  my $String        = shift; # name of function to correct
  my $NakedFileName = shift; # naked name of file to which this function belongs to
  my $Type          = { private => '_', public => '' }->{ shift() };
  
  $String =~ s/ //g;
  my $PointerPrefix;
  while (substr($String, 0, 1) eq '*') { $PointerPrefix .= '*'; $String = substr($String, 1); }
  
  $String = lc(substr($String, 0, 1)).substr($String, 1); # lowercase first character
  
  $String = correctCamelCase($String);

  while (substr($String, 0, 1) eq '_') { # remove all leading underscores
    $String = substr($String, 1);
  }
  if (substr($String, 0, length($NakedFileName)+1) ne $NakedFileName.'_') { # prefix function name with naked filename
    $String = $NakedFileName.'_'.$String;
  }
  
  return ($PointerPrefix, $Type.$String); 
}
sub nameType {                    # correct given datatype (all lowercased with underscores)
  my $String        = shift; # name of function to correct

  my %Exception = ( #{ type names that will not be corrected
                     BOOL          => 1,
                     t_base        => 1, # ToDo: Replace by appropriate datatype base_t
                     t_base_signed => 1, # ToDo: Replace by appropriate datatype base_signed_t
                   ); #}
  if ($Exception{$String}) { return $String; } 
  
  $String = lc(substr($String, 0, 1)).substr($String, 1); # lowercase first character
  
  $String = correctCamelCase($String);
  
  return $String;
}
sub parseFunctionPrototype {      # reads given string as a function prototype and returns corrected, extracted data 
  my $String   = shift; # single line function prototype as found in source code (e.g. 'volatile void* calculate(t_u8 A, const t_u8* B)')
  my $FileName = shift; # reference name for error messages
  my $Type     = shift; # one from ['public', 'private', 'feature']
  
  my %Function = (String => $String, Error => 0, Type => $Type, Body => '');
  my $NakedFileName = extractNakedFileName($FileName); # filename to which given prototype belongs to
  
  $String = strip($String);
  my $PosOpenBracket  = index($String, '(');
  my $PosCloseBracket = index($String, ')');
  my $PosFirstSpace   = index($String, ' ');
  
  if ($PosOpenBracket == -1)               { $Function{Error} = $Function{Error} || 'Missing opening bracket'; }
  if ($PosOpenBracket >= $PosCloseBracket) { $Function{Error} = $Function{Error} || 'Missing closing bracket'; }
  if ($PosFirstSpace  == -1)               { $Function{Error} = $Function{Error} || 'Missing return type'; }
  if ($PosFirstSpace  >= $PosOpenBracket)  { $Function{Error} = $Function{Error} || 'Missing return type'; }
  
  $String =~ m/(\S+)\s+(\S+)\((.*)\)/;
  my $PointerPrefix;
  ($PointerPrefix, $Function{Name})   = nameFunction($2, $NakedFileName, $Type);
  $Function{Return} = nameType($1).$PointerPrefix;
  my $Arguments = $3;
  my @Arguments = split(',', $Arguments);
  @Arguments = parseVariableDeclarations(@Arguments);
               
  $Function{ArgumentsList}   = \@Arguments;
  $Function{ArgumentsString} = join(', ', map { $_->{String}; } @{ $Function{ArgumentsList} });
  $Function{Prototype}       = "$Function{Return} $Function{Name}($Function{ArgumentsString})";

  my $LastSlashPos = rindex($FileName, '/');
  my $DirectoryPath = $Function{DirectoryPath} = ($LastSlashPos > -1) ? substr($FileName, 0, $LastSlashPos + 1) : '';
  
  my $LastDotPos = rindex($FileName, '.');
  if ($LastDotPos == -1) { $LastDotPos = length($FileName); } # no dot in filename: take whole filename
  my $SourceNamePrototype = $Function{SourceNamePrototype} = substr($FileName, 0, $LastDotPos);

  $LastDotPos = rindex($NakedFileName, '.');
  if ($LastDotPos == -1) { $LastDotPos = length($NakedFileName); } # no dot in filename: take whole filename 
  my $SourceNamePrototypeNaked = $Function{SourceNamePrototypeNaked} = substr($NakedFileName, 0, $LastDotPos);
  
  $Function{Header} = $SourceNamePrototype.'.h';
  $Function{Source} = $SourceNamePrototype.'.c';
  
  $Function{HeaderName} = substr($NakedFileName, 0, $LastDotPos).'.h';
  $Function{SourceName} = substr($NakedFileName, 0, $LastDotPos).'.c';
  
  if (substr($SourceNamePrototypeNaked, 0, 4) eq 'ttc_') { # it's a ttc_ device-driver: extract device name 
    if (substr($SourceNamePrototypeNaked, -10) eq '_interface') { # ttc_DEVICE_interface
      $Function{TTC_Device} = substr($SourceNamePrototypeNaked, 4, -10);
    }
    else {                                                        # ttc_DEVICE
      $Function{TTC_Device} = substr($SourceNamePrototypeNaked, 4);
    }
  }
  else { $Function{TTC_Device} = ''; }
  
  #? unless (-e $Function{Header}) { push @Errors, "Missing header file '$Function{Header}'"; }
  #? unless (-e $Function{Source}) { push @Errors, "Missing source file '$Function{Source}'"; }
  
  if ($Function{Error}) { push @Errors, "Invalid function prototype '$String' (Error=$Function{Error})!"; }
  
  if (0 || ($Verbose >=3) ) { # Debug return hash
    print "parseFunctionPrototype($FileName, $Type) = \n    ".
          join( "\n    ", map {
                                if (ref($Function{$_}) eq 'ARRAY') {
                                  $_.' => ['.
                                  join( ", ", map {
                                                   if (ref($_) eq 'ARRAY') {
                                                     ' ['.join(", ", @$_ ).'] ';
                                                   }
                                                   else {
                                                     $_;
                                                   }
                                                 }
                                        @{ $Function{$_} }
                                      ).
                                  ']';
                                }
                                else {
                                  $_.' => "'.$Function{$_}.'"';
                                }
                              } sort keys %Function
              )."\n";
  }

  return \%Function; #{
                     #  +----------------------------------------+
                     #  | %Function                              |
                     #  +----------------------------------------+
                     #  | String                   => $String    | <- original string from which below fields have been extracted
                     #  | Error                    => $Error     | <- error messages accumulated during parsing
                     #  | Name                     => $Name      | <- name of function
                     #  | Header                   => $Header    | <- filepath to header file
                     #  | Source                   => $Source    | <- filepath to source file
                     #  | HeaderName               => $Header    | <- name of header file without path
                     #  | SourceName               => $Source    | <- name of source file without path
                     #  | DirectoryPath            => $Path      | <- path to directory storing header and source files
                     #  | SourceNamePrototype      => $String    | <- path to source file without suffix (e.g. ttc-lib/ttc_example')
                     #  | SourceNamePrototypeNaked => $String    | <- filename of source file without suffix (e.g. 'ttc_example')
                     #  | TTC_Device               => $Device    | <- !="": name of ttc device (derived from filename)
                     #  | Prototype                => $Prototype | <- compiled function prototype (e.g. 'void* calc(volatile void* PointerA)')
                     #  | Return                   => $Return    | <- type of return value (e.g. 'void*')
                     #  | Type                     => $Type      | <- type of function (one from ['public', 'private', 'feature'])
                     #  | ArgumentsString          => $Arguments | <- compiled string of all arguments
                     #  | ArgumentsList            => @Arguments | <- array of variables as returned by parseVariableDeclarations()
                     #  +----------------------------------------+
                     #}
}
sub parseVariableDeclarations {   # parses given variable declarations and returns list of extracted data
  my @Strings = @_; # one variable declaration per item (e.g. ( 't_u8 A;', 'void* B' ))
  my @Variables;
  
  foreach my $String (@Strings) {
    $String = strip($String);
    while (substr($String, -1) eq ';') { $String = substr($String, 0, -1); }
    $String = strip($String);
    
    my $PosEqual = rindex($String, '=');
    my $Value;
    if ($PosEqual > -1) { $Value = strip( substr($String, $PosEqual + 1) ); } # extract initial value
    
    my @Parts = split(" ", $String);
     
    my $Name     = pop(@Parts);
    my $LastType = pop(@Parts);
    while (substr($Name, 0, 1) eq '*')  # move all asterisks from variable to last type
    { $Parts[-1] .= '*'; $Name = substr($Name, 2); } 
     
    push(@Parts, $LastType);
    my @Types = @Parts;
    push(@Parts, $Name);
    
    my $Pointer;
    my $PointerPos = index($String, '*');
    if ($PointerPos > -1) {
      my $Length = 1;
      while (substr($String, $PointerPos + $Length, 1) eq '*')
      { $Length++; }
      $Pointer = substr($String, $PointerPos, $Length);
    }
    my ($Ignore, $NameFixed) = nameVariable($Name);
    
    my $String = join(" ", @Parts);
    if ($Value ne undef) { $String .= ' = '.$Value; }
    
    my $ConstPointer = 0;
    my $Constant     = 0;
    #? my $PointerPos = index($String, '*');
    my @Part2 = split(" ", substr($String, $PointerPos));    # words after first asterisk
    if ($Pointer) {
      my @Part1 = split(" ", substr($String, 0, $PointerPos)); # words before first asterisk
     
      if (grep { $_ eq 'const' } @Part1) { $ConstPointer = 1; } # found const keyword before first asterisk
    }
    if (grep { $_ eq 'const' } @Part2) { $Constant     = 1; } # found const keyword after first asterisk
    
    push @Variables, { # add variable data
                       Name         => $Name,
                       NameFixed    => $NameFixed,
                       String       => $String,
                       Pointer      => $Pointer,
                       Constant     => $Constant,
                       ConstPointer => $ConstPointer,
                       Value        => $Value,
                       Types        => \@Types,
                       TypeString   => join(" ", @Types),
                     };

    if (0) { # debug data
      print "parseVariableDeclarations($String)\n    ".
            join("\n    ", map {
                                 my $Name = $_;
                                 my $Value = $Variables[-1]->{$Name};
                                 if (ref($Value) eq 'ARRAY') { $Name.' => '.join("|", @$Value); }
                                 else                        { $Name.' => '.$Value; }
                               } sort keys %{ $Variables[-1] }
                )."\n";
    }
  }
  
  if (wantarray()) { # return list of all parsed variables
    return @Variables; #{
                       #  +-------------+ 
                       #  | @Variables  |
                       #  +-------------+   +-------------------------------+
                       #  | $Variable1 ---> | %Variable                     | <- data extracted for one variable
                       #  |     :       |   +-------------------------------+
                       #                    | Name         => $Name         | <- variable name without any datatype or asterisk
                       #                    | NameFixed    => $NameFixed    | <- variable name being passed via nameVariable()
                       #                    | String       => $String       | <- recompiled variable declaration string (trailing semicolon removed)
                       #                    | Pointer      => $Pointer      | <- amount of asterisks being used in declaration (=='': not a pointer)
                       #                    | Constant     => $Constant     | <- ==1: variable is constant
                       #                    | ConstPointer => $ConstPointer | <- ==1: variable points to a readonly location
                       #                    | Value        => $Value        | <- !=undef: initial value
                       #                    | TypeString   => $TypeString   | <- recompiled string of all datatypes of this variable
                       #                    |                               |   +--------+
                       #                    | Types --------------------------> | @Types | <-- list of all types applied to this variable
                       #                    |                               |   +--------+
                       #                    |                               |   | $Type1 | <-- single datatype
                       #                    |                               |   |   :    |
                       #                    +-------------------------------+
                       #}
  }
  else { return $Variables[0]; } # return only pointer to first %Variable
}
sub prefixLines {                 # prefixes every line in given string
  my $Prefix = shift;  # string to insert at begin of every line in $String
  my $String = shift;  # single- or multiline string
  
  my @String = split("\n", $String);
  @String = map {
                  $Prefix.$_;
                } @String;
                
  return join("\n", @String);
}
sub readFile {                    # read in content of given file
  my $FilePath = shift;
  
  my $Content;
  my $Error;
  open(IN, "<$FilePath") or $Error = "$0 - ERROR: Cannot read from file '$FilePath' ($!)";
  dieOnError($Error);
  while (<IN>) { $Content .= $_; }
  close(IN);
  
  $Content =~ s/\\\n//g; # concatenate multiline strings
  
  return $Content;
}
sub registers_FixData {           # check and correct some errors in register structure
    my $RegistersRef = shift; # struct-ref as returned by ()
    my $TextFile     = shift; # used for debugging messages
    my %Switches = ( NoCaseChange => 1 );       # supported switches
    my %Enabled  = map  { $_ => 1 }        # create a hash
                   grep { $Switches{$_}; } # pass only supported switches
                   @_;                     # all remaining arguments are switches
    
    foreach my $RegisterName (sort keys %$RegistersRef) {
      my $RegisterRef = $RegistersRef->{$RegisterName};
      if (ref($RegisterRef) ne 'HASH') { dieOnError("Invalid struct-ref '".ref($RegisterRef)."' ne 'HASH' for register '$RegisterName'");  }
      my $BitFieldsRef = $RegisterRef->{Bits};
      if (ref($BitFieldsRef) ne 'ARRAY') { dieOnError("Invalid struct-ref '".ref($BitFieldsRef)."' ne 'ARRAY' for register '$RegisterName'");  }
      
      # remove empty bit indices
      @$BitFieldsRef = grep { $_; } @$BitFieldsRef;
      
      # insert fields for undefined bits
      my $LastIndex = 0;
      my $LastSize  = 0;
      my @BitFields_OK;
      my %FieldNames;
      foreach my $BitFieldRef (@$BitFieldsRef) {
        if ($BitFieldRef) {
          my $BitIndex = $BitFieldRef->{Index};
          my $Index_Expected = $LastIndex + $LastSize;
          if ($BitIndex > $Index_Expected) { # bits not defined
            my %BitField_Undefined = (
                   Index => $Index_Expected,
                   Name  => 'undefined',
                   Note  => "ERROR: these bits were not defined in '$TextFile'!",
                   Size  => $BitIndex - $Index_Expected
                 );
            push @BitFields_OK, \%BitField_Undefined;

            my $FieldName = $BitField_Undefined{Name};
            $FieldNames{$FieldName}++;
          }
       elsif ($BitIndex < $Index_Expected) { # bitfields overlap: include error message
            $BitFieldRef->{Note} = "ERROR: Bitfield overlaps with previous one!    ".$BitFieldRef->{Note};
          }
          push @BitFields_OK, $BitFieldRef;
          $LastIndex = $BitIndex;
          $LastSize  = $BitFieldRef->{Size};
          
          unless ($Enabled{NoCaseChange}) { # unify field name
            $BitFieldRef->{Name} = nameConstant($BitFieldRef->{Name});
            if (substr($BitFieldRef->{Name}, 0, 8) eq 'RESERVED') { # reserved field are not to be used -> lowercase them
              $BitFieldRef->{Name} = lc($BitFieldRef->{Name});
            }
          }
          my $FieldName = $BitFieldRef->{Name};
          $FieldNames{$FieldName}++;
        }
      }
      
      # store corrected bitfields in structure
      $RegisterRef->{Bits} = $BitFieldsRef = \@BitFields_OK;
     
      # rename doubled field names
      foreach my $FieldName (keys %FieldNames) { # prepare enumeration
        if ($FieldNames{$FieldName} > 1) 
        { $FieldNames{$FieldName} = 1; } # must enumerate this name
        else 
        { $FieldNames{$FieldName} = 0; } # must not enumerate this name
      }
      
      foreach my $BitFieldRef (@$BitFieldsRef) { # enumerate all names that occured multiple times
        if ($BitFieldRef) {
          my $FieldName = $BitFieldRef->{Name};
          if ($FieldNames{$FieldName}) {
            my $UniqueName;
            do { # generate unique name by increasing counter
              $UniqueName = $BitFieldRef->{Name}.$FieldNames{$FieldName};
              $FieldNames{$FieldName}++;
            } while ($FieldNames{$UniqueName});
            $FieldNames{$UniqueName} = 1; # register new name to avoid generating unique names twice (may happen if names ends with digits)
            
            $BitFieldRef->{Name} = $UniqueName; # assign new, unique name
          }
        }
      }
    
      if (0) { # debug register content
        print "Register($RegisterName\n".
              prefixLines("         ",
                join( "\n", 
                      map { 
                            if ($_ ne 'Bits') { "$_ => $RegisterRef->{$_}"; }
                            elsif(1) { # compile bit field
                              my $Text = "$_ => \n";
                              my @Bits = map {
                                               if (ref($_) eq 'HASH') {
                                                 my %BitField = %$_;
                                                 join("\n", map { "$_ => '$BitField{$_}'" } sort keys %BitField)."\n";
                                               }
                                               else { "UNKNOWN '$_'"; }
                                             } @{ $RegisterRef->{$_} };
                              $Text.prefixLines("        ", join("\n", @Bits) );
                            }
                          } sort keys %$RegisterRef
                    )."\n"
               )."\n        ); #Register ------------------------\n";
      }
    }
}
sub registers_parse_STM1 {        # parse given string as ST Microelectronics Datasheet format type #1
  if ($_[0] eq 'INFO') {  # return info text
    return <<"END_OF_INFO";
ST Microelectronics datasheet type 1. This is a common format used in several 
microcontroller datasheets like RM0038 (STM32L15x type architecture).
The description should be copied using the table selector tool of okular or similar
tools in other PDF viewers. Each register description is started with a register line.

Each register line starts with capital keyword "REGISTER" and has this form (<FOO> are field names):
  REGISTER <RegisterName> <First Comment Line>
 
The register line is followed by one or more bit lines for a single bit or multiple bits.
A single bit line always starts with keyword "Bit" followed by a whitespace (case insensitive):
  Bit <No> <Name> <First Comment Line>
A multi bit line always starts with keyword "Bits" followed by a whitespace (case insensitive)
  Bits <No1>:<No2> <Name> <First Comment Line>
 
The single or multi bit line can be followed by any amount of text lines. These text lines
add to the description of this single or multi bit. 

The description of a register ends at the next register line or at the file end. 
See examples below for a stm1 example description.
END_OF_INFO
  }
  if ($_[0] eq 'EXAMPLE') {  # return usage example
    return <<"END_OF_EXAMPLE";

# Create stm1 type register description (copied from RM0038 p.154)
cat <<END_OF_TEXT >register_stm1.text

REGISTER RI_ASCR2 RI analog switch control register 2 (RI_ASCR2)  (->RM0038 p.156)
The RI_ASCR2 register is used to configure the analog switches of groups of I/Os not linked 
to the ADC. In this way, predefined groups of I/Os can be connected together. 
Bits31:29	Reserved, must be kept at reset value 
Bits28:16	GRx-x: GRx-x analog switch control 
These bits are set and cleared by software to control the analog switches independently. 
from the ADC interface. Refer to Table 24: I/O groups and selection on page 148. 
0: Analog switch open 
1: Analog switch closed 
Note: These bits are available in high density devices only. 
Bits 15:12	Reserved, must be kept at reset value 
Bit 11:0	GRx-x: GRx-x analog switch control 
These bits are set and cleared by software to control the analog switches independently. 
Refer to Table 24: I/O groups and selection on page 148 from the ADC interface. 
0: Analog switch open 
1: Analog switch closed 

REGISTER RI_HYSCR1 RI hysteresis control register (RI_HYSCR1)  (->RM0038 p.157)
The RI_HYSCR1 register is used to enable/disable the hysteresis of the input Schmitt 
trigger of ports A and B. 
Bits 31:16 PB[15:0]: Port B hysteresis control on/off 
These bits are set and cleared by software to control the Schmitt trigger hysteresis of the 
Port B[15:0]. 
0: Hysteresis on 
1: Hysteresis off 
Bits 15:0 PA[15:0]: Port A hysteresis control on/off 
These bits are set and cleared by software to control the Schmitt trigger hysteresis of the 
Port A[15:0]. 
0: Hysteresis on 
1: Hysteresis off 
END_OF_TEXT

# create C structs from stm1 type register description and insert it into foo.c / foo.h 
$0 register register_stm1.text foo stm1

END_OF_EXAMPLE
  }

  my ($TextFile) = @_;
  my $Text = strip( readFile($TextFile) ) ;
  my %Registers;
  
  # remove all text before first REGISTER line
  my $FirstRegisterPos = (substr($Text, 0, 9) eq 'REGISTER ') ? 0 # text starts with register description
                       : index($Text, "\nREGISTER ") + 1;         # find first line starting with register description 
  $Text = "\n".substr($Text, $FirstRegisterPos); 
                       
  my @Texts = split("\nREGISTER ", $Text); # now we have a list of register descriptions
  map {

    my @Lines = split("\n", $_);
    my $RegisterLine = strip( shift(@Lines) );
    $RegisterLine =~ m/([A-Za-z0-9_]+)\s*(.*)/; # "REGISTER RI_ASCR1 RI analog switches control register (RI_ASCR1)  (->RM0038 p.154)"
    my $RegisterName = $1;
    my $RegisterNote = $2;
    my @Text;
    while (
            (@Lines) &&
            (! ( $Lines[0] =~ m/bit\s*([0-9]+)\s*([A-Za-z0-9_]+)(.*)/i ) ) &&
            (! ( $Lines[0] =~ m/bits*\s*([0-9]+)\:([0-9]+)\s*([A-Za-z0-9_]+)(.*)/i ) )
          ) { # @Text = all lines between register line and first bit line
    
      push @Text, shift(@Lines);
    }
    
    # remaining lines can only store bit lines + their description
    my @Bitfields;
    my $CurrentBitIndex = -1;
    my $MaxBitIndex     = -1;
    while (@Lines) { # collect all bit lines + their text
      my $Line = shift(@Lines);

      if ( $Line =~ m/bit[s]*\s*([0-9]+)\:([0-9]+)\s*([A-Za-z_0-9\-]+)(.*)/i ) {      # multi bit line "Bits 16:12 COMP_A cuohfurhf"
        my $Index1 = $1;
        my $Index2 = $2;
        my $Name   = $3; 
        my $Note   = $4;
        my $Size;
        $Name =~ s/\-/_/g;
        
        # using smaller index as bit index
        if ($Index1 < $Index2) {
          $CurrentBitIndex = $Index1;
          $Size = $Index2 - $Index1 + 1;
        }
        else {
          $CurrentBitIndex = $Index2;          
          $Size = $Index1 - $Index2 + 1;
        }
        
        my %BitField = (
                         Index => $CurrentBitIndex,
                         Name  => strip($Name),
                         Note  => $Note,
                         Size  => $Size
                       );
        $Bitfields[$CurrentBitIndex] = \%BitField;
      }
   elsif ( $Line =~ m/bit\s*([0-9]+)\s*([A-Za-z_0-9\-]+)(.*)/i ) {               # single bit line "Bit 12 PA_ENABLE cuohfurhf"
        my $Index = $1;
        my $Name  = $2; 
        my $Note  = $3;
        $Name =~ s/\-/_/g;

        $CurrentBitIndex = $Index;
        my %BitField = (
                         Index => $Index,
                         Name  => strip($Name),
                         Note  => $Note,
                         Size  => 1
                       );
        $Bitfields[$CurrentBitIndex] = \%BitField;
      }
      else {                                       # multiline text: add to Text of current bit field
        if ($CurrentBitIndex >= 0) {
          $Bitfields[$CurrentBitIndex]->{Text} .= $Line;
        }
      }
      
      if ($MaxBitIndex < $CurrentBitIndex)
      { $MaxBitIndex = $CurrentBitIndex; }
    }
    
    
    my $LastBitfieldRef = $Bitfields[-1];
    my $Size = $LastBitfieldRef->{Index} + $LastBitfieldRef->{Size};
    
    my %Register = ( Name => $RegisterName,
                     Note => $RegisterNote,
                     Text => join("\n", @Text),
                     Bits => \@Bitfields,
                     Size => $Size
                   );

    $Registers{$RegisterName} = \%Register;
  } grep { $_ } @Texts;
  
  return \%Registers; #{ created structure as parsed from given description
  #
  # +----------------+
  # | %Registers     |
  # +----------------+   +---------------+
  # | RegisterName1 ---> | %Register     | <-- data of single register
  # |       :        |   +---------------+
  #                      | Name => $Name | <-- name of this register 
  #                      | Size => $Size | <-- total size of this register (bits)
  #                      | Note => $Note | <-- descriptive, singleline text (added to first line of declaration)
  #                      | Text => $Text | <-- descriptive, multiline text
  #                      |               |
  #                      |               |   +----------+
  #                      | Bits -----------> | @Bits    | <-- ordered list of bitfields
  #                      |               |   +----------+   +-----------------+
  #                                          | $Bit1 -----> | %BitField       | <-- data of one bitfield
  #                                          |    :     |   +-----------------+
  #                                                         | Name  => $Name  | <-- name of this bitfield
  #                                                         | Index => $Index | <-- lowest bit index of this field
  #                                                         | Size  => $Size  | <-- size of this bitfield (bits)
  #                                                         | Note  => $Note  | <-- descriptive, single line text (added as comment in bit declaration line) 
  #                                                         | Text  => $Text  | <-- descriptive, multi line text 
  #                                                         +-----------------+
  #}
}
sub strip {                       # removes invisible characters at begin + end of given string
  my $Line = shift || "";

  my %IsWhiteChar = ( ' ' => 1, "\n" => 1, "\r" => 1, "\t" => 1 ); 

  $Line =~ s/^\s+//;
  $Line =~ s/\s+$//;
  #$Line =~ s/(?:^ +)||(?: +$)//g;
  #$Line =~ s/^\ *([A-Z,a-z,0-9]*)\ *$/\1/g;
  #$Line =~ s/^\s+//;
  #$Line  =~ s/\s+$//;
  return $Line;
  
  my $lIndex = 0;
  while ( $IsWhiteChar{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsWhiteChar{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  return substr($Line, $lIndex, $rIndex - $lIndex + 1);

}
sub writeFile {                   # write content to given file
  my $FilePath     = shift;
  my $Content      = shift;
  my $BackUpSuffix = shift; # if given and original file exists, a backup file is being created using this suffix
  
  # ensure that directoy exists
  my $LastSlashPos = rindex($FilePath, '/');
  if ($LastSlashPos > -1) { # file is to be created in different folder: ensure that folder exists
    my $DirPath = substr($FilePath, 0, $LastSlashPos);
    createDirectoryPath($DirPath);
  }
  
  if (1 || ($Verbose>1) ) { print "writing to disc: $FilePath\n"; }
  if ($BackUpSuffix) {
    if (-e $FilePath) {
      system("mv \"${FilePath}\" \"${FilePath}$BackUpSuffix\"");
    }
  }
  open(OUT, ">$FilePath") or die ("$0 - ERROR: Cannot write to file '$FilePath' ($!)");
  print OUT $Content;
  close(OUT);
  
  return $Content;
}
sub writeFileIfChanged {          # writes content to given file if changed
  my $FilePath     = shift;
  my $Content      = shift;
  my $ContentOrig  = shift;
  
  if ($Content ne $ContentOrig) {
    print "writing changed file $FilePath\n";
    writeFile($FilePath, $Content);
    return 1;
  }
  else { print "file unchanged: '$FilePath'\n"; }
  return 0;
}
