#!/bin/bash

NotFound=`which cscope | grep "which: no"`
if [ "$NotFound" != "" ]; then
  echo "ERROR: cscope executable not found in \$PATH !"
  echo "       Solution: Install cscope"
  echo "       openSuSE:       sudo zypper  install cscope"
  echo "       Debian/ Ubuntu: sudo apt-get install cscope"
  exit 10
fi

echo "collecting all *.c|*.h files.."
AllFiles=`find 2>find.err -L ./ -name "*.h" -or -name "*.c"`
echo "$AllFiles" >cscope.files
cscope -c -icscope.files

