#!/usr/bin/perl
#
#
# written by Gregor Rebel 2013
#

unless(@ARGV) {
  print <<"END_OF_HELP";
$0 FILENAME[, STARTDIR]

    Finds directory relative to given startdir where given file is located.

END_OF_HELP
  exit 5;
}

my $FileName = shift(@ARGV);
my $StartDir = shift(@ARGV) || "./";

unless (substr($StartDir, -1) eq '/') { $StartDir .= '/'; }

my $CMD = "find $StartDir -follow -name \"$FileName\"";
my $Found = `$CMD`;
my @Found = split("\n", $Found);
print substr($Found[0], length($StartDir), - length($FileName) )."\n";
