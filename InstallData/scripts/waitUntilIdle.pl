#!/usr/bin/perl

my $PID = shift(@ARGV);
unless ($PID) {
  print <<"END_OF_HELP";
$0 PROCESS_ID

Wait until process with given ID is idle (0% CPU usage) or terminated.

written by Gregor Rebel 2015

END_OF_HELP

  exit 10;
}

my $ColumnCPU     = 0;
my $ColumnPID     = 0;
my $ColumnCOMMAND = 0;

my $Wait = 0;
while (1) {
  my $Top = `top -p$PID -n1 -o%CPU -b -w80`;
  my @Top = split("\n", $Top);
  
  unless ($ColumnCPU) { # determine which columns shows PID, %CPU, COMMAND
    my @Grep = grep { (index($_, '%CPU') > -1) && (index($_, '%CPU(s)') == -1); } @Top;
    my @Cols = split(" ", $Grep[0]);
    my $ColumnNo = 0;
    while (@Cols) { # check all column names
      my $Column = shift(@Cols);
      $ColumnNo++;
      if ($Column eq '%CPU') {
        $ColumnCPU = $ColumnNo;
      }
      if ($Column eq 'PID') {
        $ColumnPID = $ColumnNo;
      }
      if ($Column eq 'COMMAND') {
        $ColumnCOMMAND = $ColumnNo;
      }
    }
    unless ($ColumnCPU) {
      print STDERR "$0 - ERROR: Cannot find \%CPU-column in top output '".$Grep[0]."' Check my implementation!\n";
      exit 6;
    }
  }
  my @Grep = grep { #  extract all lines showing $PID in PID-column
    my @Cols = split(" ", $_);
    $Cols[$ColumnPID-1] eq $PID;
  } @Top;
  my $GreppedLine = shift(@Grep);        # first line showing $PID in PID-column
  unless ($GreppedLine) {
    print "process $PID not running!\n";
    exit 5;
  }
  my @Cols = split(" ", $GreppedLine);   # make line an array
  my $CPU = $Cols[$ColumnCPU-1];

  if ($CPU =~ m/0?0/) { 
    if ($Wait++ >= 5) { # wait for stable readings 
      print "process $PID (".$Cols[$ColumnCOMMAND-1].") is idle ($CPU)\n";
      exit 0;
    }
  }
  else { $Wait = 0; }
  
  
  #print "$Top\n";
  sleep(1);
}