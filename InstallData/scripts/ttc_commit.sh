#!/bin/bash

OldPWD="`pwd`"

if [ "$1" == "--help" ]; then
  cat <<END_OF_HELP
$0 [PUSH]

This scripts helps you in committing your changes to the GIT repository.

Technically, it
- changes directory into your git repository
- recalls all pending commit comments from .pending_commits 
- issues "git pull" to download and merge latest changes from central server 
- issues "git commit -a" to commit your changes and let you comment them
- may issue "git push" to upload your changes to the central server

Arguments:
  PUSH     issue git push after successfull commit


END_OF_HELP
  exit 0
fi
if [ "$1" == "PUSH" ]; then
  PUSH="1"  
fi

while [ ! -d ".git" ]; do
  pwd #D
  cd ..
  if [ "/" == "`pwd`" ]; then
    if [ "$TTC_DIR_REPOSITORY" != "" ]; then # not started inside git repository: try finding it via environment variable
      cd "$TTC_DIR_REPOSITORY"
    fi
    if [ ! -d ".git" ]; then
      echo "`pwd`/$0 - ERROR: Must be run inside git-reposotiory!"
      exit 10
    fi
  fi
done

echo "`pwd`> commit"

if [ ! -e .pending_commits ]; then
  echo "" >.pending_commits
fi

if [ "$PUSH" == "1" ]; then #{ append source statistics
  AlreadyAdded=`grep 'Statistics:' .pending_commits`
  if [ "$AlreadyAdded" == "" ]; then #{ gather statistics
    echo "gathering statistics..."
    NoDeprecated='-not -name "*DEPRECATED*"'
    Stats_SourceFiles=`         find ./ $NoDeprecated -and -name "*.c"  -or $NoDeprecated -and -name "*.h"  -exec echo "{}"  \; | wc -l`
    Stats_ScriptFiles=`         find ./ $NoDeprecated -and -name "*.pl" -or $NoDeprecated -and -name "*.sh" -exec echo "{}"  \; | wc -l`
    Stats_LinesOfSource=`       find ./ $NoDeprecated -and -name "*.c"  -or $NoDeprecated -and -name "*.h"  -exec cat "{}"   \; | wc -l`
    Stats_LinesOfScripts=`      find ./ $NoDeprecated -and -name "*.pl" -or $NoDeprecated -and -name "*.sh" -exec cat "{}"   \; | wc -l`
    Stats_LinesOfDocumentation=`find Documentation/ $NoDeprecated -and -name "*.tml"  -exec cat "{}"  \; | wc -l`
    
    cat <<END_OF_STATS >>.pending_commits

Statistics:
- Source Code Files:   $Stats_SourceFiles
- Lines C Source Code: $Stats_LinesOfSource
- Script Files:        $Stats_ScriptFiles
- Lines Helper Script: $Stats_LinesOfScripts
- Lines Documentation: $Stats_LinesOfDocumentation

END_OF_STATS
  #}
  else
    echo "reusing old statistics in .pending_commits"
  fi
fi #}

COMMIT_OK=""
git pull && git commit -a -F .pending_commits -e && COMMIT_OK="1"

if [ "$COMMIT_OK" == "1" ]; then
  echo "" >.pending_commits
  if [ "$PUSH" == "1" ]; then
    git push
  fi
else
  echo "$0 - commit aborted."
fi