#!/bin/bash

if [ "$USER" == "root" ]; then
  echo "$0 - Must not run this script as user root! - TERMINATING"
  exit 15  
fi

Version="$1"
if [ "$2" == "b" ]; then
  Beta="b"
fi
if [ "$2" == "a" ]; then
  Beta="a"
fi
FullVersion=${Version}${Beta}
if [ "$Version" == "" ]; then
  echo "$0 VERSION [a|b]"
  echo "    VERSION version number to append (x.x.xx e.g.: 1.0.35)"
  echo "    a  an alpha release automatically overwrites existing alpha release of same version"
  echo "    b  a beta release automatically overwrites existing beta release of same version"
  exit 5
fi
Archive="TheToolChain_${FullVersion}.tar.bz"
if  [ "$Beta" == "" ]; then
  if [ -e ../archive/$Archive ]; then
    echo "$0 ERROR: ../archive/$Archive already exists!"
    exit 6
  fi
fi

function createLink() {      # Source,Link,User,QUIET  creates symbolic link (removes old one before)
  cl_Source="$1"
  cl_Link="$2"
  cl_User="$3"
  cl_Quiet="$4"
  
  cl_Quiet="" #D
  if [ "$cl_Quiet" = "" ]; then
    Verbose="-v"
    echo -n "`pwd`> "
  else
    Verbose=""
  fi
  if [ ! -e "$cl_Source" ]; then
    MissingFiles="$MissingFiles $cl_Source"
  fi
  
  rm -f "$cl_Link"
  if [ "$cl_User" != "" ]; then
    sudo -u$cl_User ln $Verbose -sf "$cl_Source" "$cl_Link"
  else
    ln $Verbose -sf "$cl_Source" "$cl_Link"
  fi
}

chmod 0700 $HOME/.ssh/id_rsa
if [ ! -d TheToolChain ]; then
  echo ">git clone"
  git clone git@hlb-labor.de:TheToolChain
fi
cd TheToolChain

if [ "1" == "1" ]; then #{ compile readme.History
  mv readme.History readme.History_old
  echo "$FullVersion @`date`" >readme.History
  echo ">git fetch"
  git fetch
  echo ">git log"
  git log HEAD..origin >>readme.History
  echo "---------------------------------------------------------"  >>readme.History
  echo "" >>readme.History
  cat readme.History_old >>readme.History
  rm readme.History_old
fi #}

echo ">git pull"
git pull
echo $FullVersion >Version
echo $FullVersion >InstallData/scripts/Version

InstallData/scripts/compile_ReadMe.TheToolChain.pl .readme.TheToolChain
cp readme.* ../../

echo ">git commmit"
git commit -a -m "released Package v${FullVersion}"
echo ">git tag"
git tag -f "Package_v${FullVersion}"
echo ">git push --tags"
git push --tags -f
echo ">git push"
git push

cd ..

if [ "$Beta" == "" ]; then
   Archive="../TheToolChain_current.tar.bz"
else
  if [ "$Beta" == "b" ]; then
    Archive="../TheToolChain_current_beta.tar.bz"
  else
    Archive="../TheToolChain_current_alpha.tar.bz"
  fi
fi
rm -f $Archive

mv TheToolChain/.git .
mv TheToolChain TheToolChain_${FullVersion}
createLink TheToolChain_${FullVersion} TheToolChain
if [ "$Beta" != "" ]; then
  rm ../archive/*${Beta}.tar*
fi
tar cjf ../archive/TheToolChain_${FullVersion}.tar.bz TheToolChain_${FullVersion} TheToolChain
rm TheToolChain
mv TheToolChain_${FullVersion} TheToolChain
mv .git TheToolChain/
cp -v TheToolChain/InstallData/deployment/install_TheToolChain* ../

createLink archive/TheToolChain_${FullVersion}.tar.bz $Archive
