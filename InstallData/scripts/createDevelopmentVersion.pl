#!/usr/bin/perl

my $DevelDir = "$ENV{HOME}/Source/TheToolChain_devel";
my $SourceDir = `pwd`; chop($SourceDir);

if (! -e "InstallData/scripts/createDevelopmentVersion.pl") {
  print "\n$0 - creates development version of this toolchain in $DevelDir\n";
  print "Error: Must be run from base of ToolChain directory!\n\n";
  exit 10
}
if (-d $DevelDir) {
  system("sudo rm -Rf ".$DevelDir.".old");
  system("mv $DevelDir ".$DevelDir.".old");
}

my $Dirs = `find "$SourceDir/" -type d`;
my @Dirs = sort { $a cmp $b; } 
           map { substr($_, length("$SourceDir/")); } 
           grep { index($_, "/.git") == -1; }  
           split("\n", $Dirs);

my $Files=`find "$SourceDir/" -type f`;
my @Files = sort { $a cmp $b; } 
            map { substr($_, length("$SourceDir/")); } 
            grep { index($_, "/.git") == -1; }  
            split("\n", $Files);


foreach my $Dir (@Dirs) { # create all directories
  mkd($DevelDir.'/'.$Dir);
  createUpdateScript($SourceDir, $DevelDir, $Dir);
}
foreach my $File (@Files) { # create symbolic links 
  system("ln -sv \"$SourceDir/$File\" \"$DevelDir/$File\"");
}
foreach my $File2Delete ( #{ delete some symbolic links
                          "Template/QtCreator/TemplateName.files",
                          "Version"
                        )  #}
{
  system("rm -f \"$DevelDir/$File2Delete\"");
  system("touch \"$DevelDir/$File2Delete\"");
}
foreach my $File2Copy ( #{ create duplicates of some files
                          "Template/QtCreator/TemplateName.creator.user",
                          "Template/QtCreator/TemplateName.creator"
                        )  #}
{
  system("rm -f \"$DevelDir/$File2Copy\"");
  system("cp \"$SourceDir/$File2Copy\" \"$DevelDir/$File2Copy\"");
}
print2File($DevelDir.'/Version', "devel\n");
print2File($DevelDir.'/InstallData/scripts/SourceMe.sh', <<"END_OF_TEXT");
export TTC_DIR_DEVELOPMENT='$DevelDir' # location of development toolchain ($0)
export TTC_DIR_REPOSITORY='$DevelDir'  # location of git repository ($0)
END_OF_TEXT

my $ReadMeText = <<"END_OF_TEXT"; #{

When using development ToolChain, keep in mind that

a) Changes in source code directly change files inside git folder
b) Adding files to ToolChain_devel/ will not change git folder
c) Adding files to a project folder will not change git folder
d) Procedure of adding new files to The ToolChain
   (1) create files inside git folder
   (2) git add new files to repository
   (3) run ./.updateLinks.sh in corresponding devel folder
   
Have fun!

END_OF_TEXT
#}

print <<"END_OF_TEXT"; #{

$ReadMeText

$0 finished
A new development ToolChain has been created in $DevelDir.

Next steps:
  cd $DevelDir/InstallData
  ./installAll.sh BASIC

END_OF_TEXT
my $Readme = "$DevelDir/readme.DevelopmentToolChain";
open(OUT, ">$Readme") or die("Cannot create file '$Readme' ($!)");
print OUT $ReadMeText;
close(OUT);

#}

sub print2File {
  my $File = shift;
  
  open(OUT, ">$File") or die("$0 - ERROR: Cannot write to file '$File' ($!)");
  print OUT join("\n", @_);
  close(OUT);
  
}
sub mkd {  # create directory path
  my $Dir = shift;
  
  $Dir =~ s-//-/-g;
  my @Parts = split ('/', $Dir);
  my $Path = shift(@Parts);
  foreach my $Part (@Parts) {
    $Path .= '/'.$Part;
    if (! -d $Path) {
      system("mkdir \"$Path\"");
    }
  }
}
sub createUpdateScript {
  my $SourceDir = shift;
  my $DevelDir  = shift;
  my $SubDir    = shift;
  
  my $ScriptFile    = joinPath($DevelDir, $SubDir, '.updateLinks.sh');
  my $LinkSourceDir = joinPath($SourceDir, $SubDir, '*');
  open(OUT, ">$ScriptFile") or die("Cannot create file '$ScriptFile' ($!)");
  print OUT <<"END_OF_SCRIPT";
#!/bin/bash

Dir=`dirname $0`
cd \$Dir

rm -f *
ln -sv $LinkSourceDir .
find ./ -maxdepth 1 -name "*.orig" -exec rm -v "{}" \\;

END_OF_SCRIPT
  close(OUT);
  system("chmod +x \"$ScriptFile\"");
}
sub joinPath { # returns joined file-path of given parts
  my @Parts;
  my $AbsolutePrefix;
  if (substr($_[0], 0, 1) eq '/') 
  { $AbsolutePrefix = '/'; }
  
  foreach my $Part (@_) {
    if ($Part) {
      while (substr($Part, 0, 1) eq '/')
      { substr($Part, 0, 1) = undef; }
      while (substr($Part, -11) eq '/')
      { substr($Part, -1) = undef; }
      
      push @Parts, $Part;
    }
  }

  return $AbsolutePrefix.join('/', @Parts);
}
