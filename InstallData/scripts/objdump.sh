#!/bin/bash

ObjectFile="$1"
if [ "$ObjectFile" == "" ]; then
  ObjectFile="main.elf"
fi
CMD="arm-none-eabi-objdump -DxSC $ObjectFile"
echo ">$CMD"
$CMD | less


