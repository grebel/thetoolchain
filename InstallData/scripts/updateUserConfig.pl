#!/usr/bin/perl

my $UserConfig = shift(@ARGV);

unless ($UserConfig) {
  print <<"END_OF_HELP";
$0 USER_CONFIG

USER_CONFIG  path to a PROJECTNAME.creator.user file being created by QtCreator

Will replace section valuemap ProjectExplorer.BuildStepList.Step.0 in given USER_CONFIG file with predefined one.

END_OF_HELP
  exit 10;
}

my $Lines = readFile($UserConfig);
my @UserConfig = split("\n", $Lines);

my $BuildConfig0_FirstLine = findLineContaining( 'ProjectExplorer.Target.BuildConfiguration.0' );
my $BuildConfig0_LastLine  = findEndTag($BuildConfig0_FirstLine);

my $RunConfig_FirstLine    = findLineContaining( "ProjectExplorer.Target.RunConfiguration.0", $BuildConfig_LastLine );
my $RunConfig_LastLine     = findEndTag($RunConfig_FirstLine);

if (0) { # print found configuration sections
  print "\$BuildConfig0_FirstLine: ".($BuildConfig0_FirstLine+1)."\n";
  print "\$BuildConfig0_LastLine:  ".($BuildConfig0_LastLine+1)."\n";
  print "\$RunConfig_FirstLine:    ".($RunConfig_FirstLine+1)."\n";
  print "\$RunConfig_LastLine:     ".($RunConfig_LastLine+1)."\n";
}

my @UserConfig2;
push @UserConfig2, arraySlice(0, $BuildConfig0_FirstLine - 1, @UserConfig);

my $PWD= `pwd`; chomp($PWD);
push @UserConfig2, split("\n", <<"END_OF_TEXT"); #{ BuildConfiguration.0
   <valuemap type="QVariantMap" key="ProjectExplorer.Target.BuildConfiguration.0">
    <value type="QString" key="ProjectExplorer.BuildConfiguration.BuildDirectory">$PWD/QtCreator</value>
    <valuemap type="QVariantMap" key="ProjectExplorer.BuildConfiguration.BuildStepList.0">
     <valuemap type="QVariantMap" key="ProjectExplorer.BuildStepList.Step.0">
      <value type="bool" key="ProjectExplorer.BuildStep.Enabled">true</value>
      <value type="QString" key="ProjectExplorer.ProcessStep.Arguments">NOLOG NOFLASH</value>
      <value type="QString" key="ProjectExplorer.ProcessStep.Command">./compile.sh</value>
      <value type="QString" key="ProjectExplorer.ProcessStep.WorkingDirectory">\%{buildDir}/..</value>
      <value type="QString" key="ProjectExplorer.ProjectConfiguration.DefaultDisplayName">Compile current project</value>
      <value type="QString" key="ProjectExplorer.ProjectConfiguration.DisplayName"></value>
      <value type="QString" key="ProjectExplorer.ProjectConfiguration.Id">ProjectExplorer.ProcessStep</value>
     </valuemap>
     <value type="int" key="ProjectExplorer.BuildStepList.StepsCount">1</value>
     <value type="QString" key="ProjectExplorer.ProjectConfiguration.DefaultDisplayName">Build</value>
     <value type="QString" key="ProjectExplorer.ProjectConfiguration.DisplayName"></value>
     <value type="QString" key="ProjectExplorer.ProjectConfiguration.Id">ProjectExplorer.BuildSteps.Build</value>
    </valuemap>
    <valuemap type="QVariantMap" key="ProjectExplorer.BuildConfiguration.BuildStepList.1">
     <valuemap type="QVariantMap" key="ProjectExplorer.BuildStepList.Step.0">
      <value type="bool" key="ProjectExplorer.BuildStep.Enabled">true</value>
      <value type="QString" key="ProjectExplorer.ProcessStep.Arguments"></value>
      <value type="QString" key="ProjectExplorer.ProcessStep.Command">./clean.sh</value>
      <value type="QString" key="ProjectExplorer.ProcessStep.WorkingDirectory">\%{buildDir}/..</value>
      <value type="QString" key="ProjectExplorer.ProjectConfiguration.DefaultDisplayName">Remove all temporary files in project</value>
      <value type="QString" key="ProjectExplorer.ProjectConfiguration.DisplayName"></value>
      <value type="QString" key="ProjectExplorer.ProjectConfiguration.Id">ProjectExplorer.ProcessStep</value>
     </valuemap>
     <value type="int" key="ProjectExplorer.BuildStepList.StepsCount">1</value>
     <value type="QString" key="ProjectExplorer.ProjectConfiguration.DefaultDisplayName">Bereinigen</value>
     <value type="QString" key="ProjectExplorer.ProjectConfiguration.DisplayName"></value>
     <value type="QString" key="ProjectExplorer.ProjectConfiguration.Id">ProjectExplorer.BuildSteps.Clean</value>
    </valuemap>
    <value type="int" key="ProjectExplorer.BuildConfiguration.BuildStepListCount">2</value>
    <value type="bool" key="ProjectExplorer.BuildConfiguration.ClearSystemEnvironment">false</value>
    <valuelist type="QVariantList" key="ProjectExplorer.BuildConfiguration.UserEnvironmentChanges"/>
    <value type="QString" key="ProjectExplorer.ProjectConfiguration.DefaultDisplayName">TheToolChain</value>
    <value type="QString" key="ProjectExplorer.ProjectConfiguration.DisplayName">TheToolChain</value>
    <value type="QString" key="ProjectExplorer.ProjectConfiguration.Id">GenericProjectManager.GenericBuildConfiguration</value>
   </valuemap>
END_OF_TEXT
#}
push @UserConfig2, arraySlice($BuildConfig0_LastLine + 1, $RunConfig_FirstLine - 1, @UserConfig);
push @UserConfig2, split("\n", <<"END_OF_TEXT"); #{ RunConfiguration.0
   <valuemap type="QVariantMap" key="ProjectExplorer.Target.RunConfiguration.0">
    <valuelist type="QVariantList" key="Analyzer.Valgrind.AddedSuppressionFiles"/>
    <value type="bool" key="Analyzer.Valgrind.Callgrind.CollectBusEvents">false</value>
    <value type="bool" key="Analyzer.Valgrind.Callgrind.CollectSystime">false</value>
    <value type="bool" key="Analyzer.Valgrind.Callgrind.EnableBranchSim">false</value>
    <value type="bool" key="Analyzer.Valgrind.Callgrind.EnableCacheSim">false</value>
    <value type="bool" key="Analyzer.Valgrind.Callgrind.EnableEventToolTips">true</value>
    <value type="double" key="Analyzer.Valgrind.Callgrind.MinimumCostRatio">0.01</value>
    <value type="double" key="Analyzer.Valgrind.Callgrind.VisualisationMinimumCostRatio">10</value>
    <value type="bool" key="Analyzer.Valgrind.FilterExternalIssues">true</value>
    <value type="int" key="Analyzer.Valgrind.LeakCheckOnFinish">1</value>
    <value type="int" key="Analyzer.Valgrind.NumCallers">25</value>
    <valuelist type="QVariantList" key="Analyzer.Valgrind.RemovedSuppressionFiles"/>
    <value type="int" key="Analyzer.Valgrind.SelfModifyingCodeDetection">1</value>
    <value type="bool" key="Analyzer.Valgrind.Settings.UseGlobalSettings">true</value>
    <value type="bool" key="Analyzer.Valgrind.ShowReachable">false</value>
    <value type="bool" key="Analyzer.Valgrind.TrackOrigins">true</value>
    <value type="QString" key="Analyzer.Valgrind.ValgrindExecutable">valgrind</value>
    <valuelist type="QVariantList" key="Analyzer.Valgrind.VisibleErrorKinds">
     <value type="int">0</value>
     <value type="int">1</value>
     <value type="int">2</value>
     <value type="int">3</value>
     <value type="int">4</value>
     <value type="int">5</value>
     <value type="int">6</value>
     <value type="int">7</value>
     <value type="int">8</value>
     <value type="int">9</value>
     <value type="int">10</value>
     <value type="int">11</value>
     <value type="int">12</value>
     <value type="int">13</value>
     <value type="int">14</value>
    </valuelist>
    <value type="int" key="PE.EnvironmentAspect.Base">2</value>
    <valuelist type="QVariantList" key="PE.EnvironmentAspect.Changes"/>
    <value type="QString" key="ProjectExplorer.CustomExecutableRunConfiguration.Arguments">DDD</value>
    <value type="QString" key="ProjectExplorer.CustomExecutableRunConfiguration.Executable">./debug.sh</value>
    <value type="bool" key="ProjectExplorer.CustomExecutableRunConfiguration.UseTerminal">false</value>
    <value type="QString" key="ProjectExplorer.CustomExecutableRunConfiguration.WorkingDirectory">\%{buildDir}/..</value>
    <value type="QString" key="ProjectExplorer.ProjectConfiguration.DefaultDisplayName">Flash binary onto microcontroller</value>
    <value type="QString" key="ProjectExplorer.ProjectConfiguration.DisplayName">Flash binary onto microcontroller</value>
    <value type="QString" key="ProjectExplorer.ProjectConfiguration.Id">ProjectExplorer.CustomExecutableRunConfiguration</value>
    <value type="uint" key="RunConfiguration.QmlDebugServerPort">3768</value>
    <value type="bool" key="RunConfiguration.UseCppDebugger">false</value>
    <value type="bool" key="RunConfiguration.UseCppDebuggerAuto">true</value>
    <value type="bool" key="RunConfiguration.UseMultiProcess">false</value>
    <value type="bool" key="RunConfiguration.UseQmlDebugger">false</value>
    <value type="bool" key="RunConfiguration.UseQmlDebuggerAuto">true</value>
   </valuemap>
END_OF_TEXT
#}
push @UserConfig2, arraySlice($RunConfig_LastLine + 1, scalar @UserConfig, @UserConfig);

#X for (my $I = $BuildConfig0_LastLine; $I < scalar @UserConfig; $I++) { push  @UserConfig2, $UserConfig[$I]; }

writeFile("${UserConfig}", join("\n", @UserConfig2));

sub readFile {
  my $FileName = shift;
  my @Lines;

  open(IN, "<$FileName") or die("Cannot read file '$FileName' ($!)");
  @Lines = <IN>;  
  close(IN);
  
  return join('', @Lines);
}
sub writeFile {
  my $FileName = shift;
  my $Content  = shift;
  
  open(OUT, ">$FileName") or die("Cannot write to file '$FileName' ($!)");
  print OUT $Content;  
  close(OUT);
}
sub exitOnError {
  my $Error = $_[0];
  
  if ($Error) 
  { 
    while (substr($Error, -1) eq "\n") { substr($Error, -1) = undef; } 
    print STDERR "$0 - ERROR: $Error\n";
    print STDERR "$0 - Project creation has been canceled.\n";
    exit 10; 
  }
}
sub strip {              # removes invisible characters at begin + end of given string
  my $Line = shift || "";

  my %IsWhiteChar = ( ' ' => 1, "\n" => 1, "\r" => 1, "\t" => 1 ); 

  $Line =~ s/^\s+//;
  $Line =~ s/\s+$//;
  #$Line =~ s/(?:^ +)||(?: +$)//g;
  #$Line =~ s/^\ *([A-Z,a-z,0-9]*)\ *$/\1/g;
  #$Line =~ s/^\s+//;
  #$Line  =~ s/\s+$//;
  return $Line;
  
  my $lIndex = 0;
  while ( $IsWhiteChar{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsWhiteChar{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  #X print " substr(\$Line, $lIndex, $rIndex - $lIndex + 1);\n"; #D
  return substr($Line, $lIndex, $rIndex - $lIndex + 1);

}
sub findLineContaining {     # finds line number of next line containing given string
  my $Search      = shift;                               # search string
  my $StartLineNo = shift() + 0;                         # starting search at this line number
  my $EndLineNo   = shift() + 0 || scalar @UserConfig;   # ending search before this line number
  
  my $LineNo = $StartLineNo; 
  while ($LineNo < $EndLineNo) {
    if (index($UserConfig[$LineNo], $Search) > -1) 
    { return $LineNo; }
    $LineNo++;
  }
  
  return -1; # search string not found
}
sub extractTag {            # extract tag at begin of given string
  my $String   = shift;       # string begining with a start or end tag
  my $StartPos = shift() + 0; # start position in String 
  
  my $Line = strip($String);
  my @Line = split(" ", $Line);
  
  my $TagStartPos = index($String, '<', $StartPos);
  my $TagEndPos   = index($String, '>', $TagStartPos);
  if (
       ($TagStartPos == -1)  || # no tag start
       ($TagEndPos   == -1)     # no tag end
     )
  { return ( '', length($String) ); }
  
  my $Tag = substr($String, $TagStartPos, $TagEndPos - $TagStartPos + 1);
  my @Words = split(" ", $Tag);
  my $TagName = $Words[0];          # first word
  $TagName = substr($TagName, 1);   # remove leading '<'
  if (substr($TagName, -1) eq '>')  # remove trailing '>'
  { $TagName = substr($TagName, 0, -1); }
  
  return ( 
           $TagName,       # name of found tag
           $TagStartPos,   # position of tag in $String 
           $TagEndPos      # position of last character of tag in $String
         )
}
sub findEndTag {            # find line containg ending tag for starting tag in given line
  my $StartTagLineNo = shift() + 0; # line number of line containing a starting tag (e.g.: <valuemap ...>)
  
  (my $StartTag, my $TagStartPos, my $TagEndPos) = extractTag($UserConfig[$StartTagLineNo]);
  if ( (! $StartTag)                    || # no tag at all
       (substr($StartTag, 1, 1) eq '/')    # ending tag
     )
  { die("ERROR: findEndTag($StartTagLineNo) - No start tag in given line: '$UserConfig[$StartTagLineNo]'"); }

  my @Stack;
  my $LineNo = $StartTagLineNo - 1;
  
  do {
    $LineNo++;
    my $Line = strip($UserConfig[$LineNo]);
    my $PosInLine = 0;
    
    while ($PosInLine < length($Line) ) {
        (my $CurrentTag, my $TagStartPos, my $TagEndPos) =  extractTag($Line, $PosInLine);
        
        unless ($CurrentTag) { last; }
        if (substr($CurrentTag, 0, 1) eq '/') { # end-tag found
          my $TagName = substr($CurrentTag, 1);
          while ( @Stack &&
                  ($TagName ne $Stack[ -1 + scalar @Stack ]->[0])
                ) { # close all open tags until matching one
            pop @Stack;
          }
          pop @Stack;
          if (0) {
            my $StartTag = $Stack[ -1 + scalar @Stack ];
            my $StartTagName = $StartTag->[0];
            my $StartTagLine = $StartTag->[1];
            print "ERROR: Closing tag in line ".($LineNo+1)." '/$TagName' !='$StartTagName' in line ".($StartTagLine+1)."!\nline: $Line\n"; 
          }
        }
        else {                                  # start-tag found
          push @Stack, [ $CurrentTag, $LineNo ];
        }
        $PosInLine = $TagEndPos;
    }
    
  } while (@Stack && ($LineNo < scalar @UserConfig) );
  
  if ($LineNo < scalar @UserConfig)
  { return $LineNo; }
  
  return -1; # end-tag not found!
}
sub arraySlice {            # return sclice of given array
  my $FirstIndex = shift() + 0; # index of first array entry to return
  my $LastIndex  = shift() + 0; # index of last array entry to return
  
  my @Slice;
  for (my $Index = $FirstIndex; $Index <= $LastIndex; $Index++)
  { push @Slice, $_[$Index]; }
  
  return @Slice;
}

exit 0;
