#!/usr/bin/perl
#
# updateProjectFiles.pl  written by Gregor Rebel 2011-2016
#
# Automatically generates list of all source-code and header files being used 
# by current makefile-project.
#
# + How it works
#   (1) run "make all -p" to retrieve all path variables from make
#   (2) process main.c + all included files (depth-search)
#   (3) write out all processed files
#

print "$0 ".join(" ", @ARGV)."\n\n";

my $ReturnCode = 0;
my $ProjectFile = shift(@ARGV);
my $FilePrefix  = shift(@ARGV);
my @Files2Process = @ARGV;


unless ($ProjectFile) {
  print "$0 PROJECT_FILE FILE_PREFIX [A.C] [B.C]..\n";
  print "  PROJECT_FILE  filename of QtCreator .files file (will be filled with all found files)\n";
  print "  FILE_PREFIX   used as prefix for each line in PROJECT_FILE\n";
  print "  A.C,B.C,..    start points of recursive search\n";
  print "\n";
  print "  $0 will run make all -p to retrieve all include- and source-paths being known to the compiler.\n";
  print "  It will then recursively scan all given .c-files for included files. All found files will be \n";
  print "  added to PROJECT_FILE.\n";
  print "\n";
  print "  Example: $0 Test123.files\n";
  print "  Example: $0 v/Test123.files ../\n";
  exit 5;
}

my $Makefile = `make all -p 2>&1`;
$Makefile =~ s/\n\r/\n/g;
$Makefile =~ s/\r\n/\n/g;
my @Makefile = split("\n", $Makefile);
$Makefile =~ s/\\\n//g;
my @ErrorMessages;
my $PWD = `pwd`; chop($PWD);

#{ extract list of c-source search paths (vpath %.c)       -> @VPathC, %VPathC
my @VPathC; my %VPathC;
my $Pos; 
my $LastPos= 0;
while ( ($Pos = index($Makefile, 'vpath %.c ', $LastPos)) > -1 ) {
  $Pos += 10;
  my $EolPos = findEndOfLine($Makefile, $Pos);
  my @NewVPathC = split(":", substr($Makefile, $Pos, $EolPos - $Pos + 1));
  @NewVPathC = map { $_ =~ s/\r//g; $_ =~ s/\n//g; $_; }  @NewVPathC;
  addEntries(\@VPathC, \%VPathC, @NewVPathC);
  $LastPos = $Pos + 1;
}
print "VPathC: ".join(',', @VPathC)."\n";
#}
#{ extract list of assembler code search paths (vpath %.s) -> @VPathS, %VPathS
my @VPathS; my %VPathS;

$LastPos = 0;
while ( ($Pos = index($Makefile, 'vpath %.s ', $LastPos)) > -1 ) {
  $Pos += 10;
  my $EolPos = findEndOfLine($Makefile, $Pos);
  my @NewVPathS = split(":", substr($Makefile, $Pos, $EolPos - $Pos + 1));
  @NewVPathS = map { $_ =~ s/\r//g; $_ =~ s/\n//g; $_; }  @NewVPathS;
  addEntries(\@VPathS, \%VPathS, @NewVPathS);
  $LastPos = $Pos + 1;
}
print "VPathS: ".join(',',@VPathS)."\n";
#}
#{ extract list of common search paths (VPATH)             -> @VPath,  %VPath
my $Pos = index($Makefile, 'Variable »VPATH«)');
my $EolPos = findEndOfLine($Makefile, $Pos);
$Pos = index($Makefile, '# ', $EolPos) + 2; # pos @ after # in next line
$EolPos = findEndOfLine($Makefile, $Pos);
my @NewVPath = split(":", substr($Makefile, $Pos, $EolPos - $Pos + 1));
@NewVPath = map { $_ =~ s/\r//g; $_ =~ s/\n//g; strip($_); }  @NewVPath;

my @VPath; my %VPath;
addEntries(\@VPath, \%VPath, '.');
print "VPath: ".join(',', addEntries(\@VPath, \%VPath, @NewVPath))."\n";
#}
#{ extract list of include paths                           -> @INCLUDE_DIRS, %INCLUDE_DIRS
my @INCLUDE_DIRS; my %INCLUDE_DIRS;
my $Pos = index($Makefile, 'INCLUDE_DIRS =');
if ($Pos > -1) {
  $Pos += 14;
  my $EolPos = findEndOfLine($Makefile, $Pos);
  my @IncludeDirs = split( " -I", substr($Makefile, $Pos, $EolPos - $Pos) );
  @IncludeDirs = map { $_ =~ s/\r//g; $_ =~ s/\n//g; $_; }  @IncludeDirs;
  unless ($IncludeDirs[0]) { shift(@IncludeDirs); }
  foreach my $IncludeDir (@IncludeDirs) {
    my %RemoveChar = (' ' => 1, '/' => 1);
    while ( $RemoveChar{substr($IncludeDir, -1)}) { substr($IncludeDir, -1) = undef; }
  }
  addEntries(\@INCLUDE_DIRS, \%INCLUDE_DIRS,  @IncludeDirs);
  print "INCLUDE_DIRS: ".join(',', @INCLUDE_DIRS)."\n";
}
#}
#{ extract list of main objects                            -> @MainObjects, %MainObjects

my @MainObjects; my %MainObjects;
my %ObjectFiles =  map { split(": ", $_); }  # "a.o: path/a.c" -> ("a.o", "path/a.c") 
                   grep { my $Pos = index($_, '.o: '); ($Pos > -1) && (substr($_, $Pos-1,1) ne '%'); } # find all entries containing '.o ' but not '%.o '
                   @Makefile;          
foreach my $Key (keys %ObjectFiles) {
  my $File = $ObjectFiles{$Key};
  my @Files = split(" ", $File), $File; # sometimes a file is printed twice in a line, separated by a space
  foreach my $SingleFile (@Files) {
    if (-e $SingleFile) {
      $ObjectFiles{$Key} = $SingleFile;
    }
    else { delete $ObjectFiles{$Key}; }
  }
} 
foreach my $Key (sort keys %ObjectFiles) {
     print "'$Key' -> '$ObjectFiles{$Key}'\n";
}

my $PATTERN='MAIN_OBJS =';
my $Pos = index($Makefile, $PATTERN);
if ($Pos > -1) {
  $Pos += length($PATTERN);
  my $EolPos = findEndOfLine($Makefile, $Pos);
  my @Objects = split( " ", substr($Makefile, $Pos, $EolPos - $Pos) );
  print "MainObjects: ".join(',', addEntries(\@MainObjects, \%MainObjects, @Objects))."\n";
}

#{ check @MainObjects for doubles

my @Doubles;
map { if ($MainObjects{$_} > 1) { push @Doubles, $_; } } @MainObjects;
if (@Doubles) {
  push @ErrorMessages, map { "Object file $_ given ".$MainObjects{$_}." times!",
                       map { "    $_"; } split("\n", `grep -Hn $_ extensions.active/makefile`);
  } @Doubles;
  push @ErrorMessages, "Check your makefiles for MAIN_OBJS+= lines!"
}
#}
unless (@Files2Process)  # no source-files given: use main-objects  
{ @Files2Process = map { if ($ObjectFiles{$_}) { $ObjectFiles{$_} }        # object-file is already known: use it  
                         else                  { substr($_, 0, -1).'c'; }  # object-file is unknown: assume it will be compiled from a .c-file
                       } @MainObjects; }

#}
#{ extract list of variables                               -> @Variables, %Variables

my @VariableLines = grep { index($_, ' = ') > -1; } @Makefile;
my @Variables;
my %Variables;

map { 
  (my $Name, my $Value) = split(" = ", $_);
  $Variables{$Name} = $Value;
} @VariableLines;
#print "Variables:\n    ".join("\n    ", map { $_.' = "'.$Variables{$_}.'"';} sort keys %Variables)."\n"; #D

# print "COMPILE_OPTS=".resolveVariable("COMPILE_OPTS")."\n";

sub resolveVariable { # recursivley determines value of given variable
  my $Name = shift;

  my $Value = $Variables{$Name};
  my $ResolvedValue;  
  my $LastPos = 0;
  my $Pos = index($Value, '$(', $LastPos);
  while ($Pos > -1) { # scan through $Value and recursively resolve all occurences of $(foo)
      $ResolvedValue .= substr($Value, $LastPos, $Pos - $LastPos);
      my $ClosePos = index($Value, ')', $Pos + 2);
      $ResolvedValue .= resolveVariable(substr($Value, $Pos + 2, $ClosePos - $Pos - 2));

      $LastPos = $ClosePos + 1;
      $Pos = index($Value, '$(', $LastPos);
  }
  $ResolvedValue .= substr($Value, $LastPos);
  #X print "resolveVariable($Name)='$ResolvedValue'\n"; #D
  
  return $ResolvedValue;
}

#}
#{ extract list of compile options                         -> @CompileOpts, %CompileOpts
my @CompileOpts; my %CompileOpts;

my $Pos = index($Makefile, 'COMPILE_OPTS =');
my $CompileOpts = resolveVariable("COMPILE_OPTS");
$CompileOpts = substr($CompileOpts, index($CompileOpts, "-D"));

my @List = split(" ", $CompileOpts);
foreach my $CompileOpt (@List) {
  my ($Key,$Value) = split("=", $CompileOpt);
  if (substr($Key, 0, 2) eq '-D') { # only interested in entries starting with '-D'
    $Key   = strip(substr($Key, 2)); # remove leading -D
    $Value = strip($Value);
    #X while (substr($Key, -1)   eq ' ') { substr($Key, -1)   = ''; }
    #X while (substr($Value, -1) eq ' ') { substr($Value, -1) = ''; }
    $CompileOpts{$Key} = $Value;
  }
}
@CompileOpts = sort keys %CompileOpts;

#}compile options
#{ extract list of linker scripts                          -> @LinkerScripts, %LinkerScripts
  my @LinkerScripts; %LinkerScripts;
  
  my @LDFlags = findLineStartingWith($Makefile, 'LDFLAGS');
  foreach my $LDFlags (@LDFlags) {
    my @Flags = grep { $_; } map { strip($_); } split(" ", $LDFlags);
    my @LinkerFlags = grep { substr($_, 0, 2) eq '-T' }  @Flags;
    map { 
          my $Script = substr($_, 2); # remove -T
          unless ($LinkerScripts{$Script}) {
            print "linker script: $Script\n";
            $LinkerScripts{$Script} = 1;
            push @LinkerScripts, $Script;
          }
    } @LinkerFlags;
  }
  %LinkerScripts = (); # will be refilled by findAllIncludedFiles() 
  @LinkerScripts = findAllIncludedFiles(\@LinkerScripts, 'INCLUDE ', \%LinkerScripts);
  print "linker scripts:\n    ".join("\n    ", @LinkerScripts)."\n"; #D
#}

my @FilesProcessed;
my %FilesProcessed;
my @FilesFound;
my %FilesFound;
my %FilesNotFound;

my %ProjectFiles;
if (1) { # read in additional entries from existing .files2 file
  my $ProjectFile2 = $ProjectFile.'2';
  my $OldStyleFile;
  if (-e $ProjectFile2) { # 2-file exists: check if it is deprecated 
    $OldStyleFile = `grep 'extensions.active/ttc_extensions_active.c' $ProjectFile2`;
    if ($OldStyleFile) { # extensions.active/ttc_extensions_active.* were move to ttc_extensions_active.* after git commit 8610eb00922043db862a1d8ec745961e16df9c5f
      system("mv -v $ProjectFile2 ${ProjectFile2}.deprecated");
    }
  }
  if (! -e $ProjectFile2) { # 2-file missing: create default one
    my $Error;
    open(OUT, ">$ProjectFile2") or die("$0 ERROR: Cannot create additionals file '$ProjectFile2' ($!)\n");
    print OUT <<"END_OF_FILE";
// additional files for $ProjectFile
// All files listed here are being added to $ProjectFile on every run of $0.
//
// Lines starting with // will be ignored

makefile
activate_project.sh
compile_options.h
ttc_extensions_active.c
ttc_extensions_active.h
extensions.active/makefile

END_OF_FILE
    close(OUT);
  }
  if (-e $ProjectFile2) {
    open(IN, "<$ProjectFile2") or die("$0 ERROR: Cannot read from project file '$ProjectFile2' ($!)\n");
    my @Lines = <IN>;
    close(IN);
    foreach my $File (@Lines) {
      my $Entry = strip($File);
      if ( (substr($Entry,0,2) ne '//') && ($Entry ne '') ) 
        { $ProjectFiles{$Entry} = 1; }
    }
  }
}
if (@ErrorMessages) {
  print STDERR map { "$0 - ERROR: $_\n"; } @ErrorMessages;
  @ErrorMessages = ();
  $ReturnCode = 10;
}

print "\nstart processing c-files for MainObjects..\n";
processFiles(0, '', @Files2Process);
my @SourceFiles = grep { ( (lc(substr($_, -2)) eq '.c') || (lc(substr($_, -2)) eq '.s') ); } # list of all C and Assembly Source files with absolute or relative path 
                  @FilesProcessed;
if (1) { # write out list of all active compile options
  open(OUT, ">compile.options") or die("$0 ERROR: Cannot write to project file '`pwd`/compile.options' ($!)\n");
  print OUT join("\n", map { $_.'='.$CompileOpts{$_}; } sort @CompileOpts)."\n";
  close(OUT);
}

# append all linker scripts to project files
map { $ProjectFiles{$_} = 1; } @LinkerScripts;

#{ write out list of all active compile options as includable header file
open(OUT, ">compile_options.h") or die("$0 ERROR: Cannot write to project file '`pwd`/compile.options' ($!)\n");
print OUT <<"END_OF_HEAD"; #{
#ifndef TTC_COMPILE_OPTS_H
#define TTC_COMPILE_OPTS_H

/* This file contains all compile options gathered from makefiles during current compile run.
 * This file was created by $0 in $PWD.
 *
 * You may include this file from every source to provide constant definitions to your IDE (e.g. QtCreator/ Eclipse).
 *
 * DO NOT CHANGE ANYTHING HERE, IT WILL GET LOST!
 */


// undefining everything to avoid tons of redefinition warnings 
END_OF_HEAD
#}
my @CompileOpts1 = grep { substr($_, 0, 10) eq 'EXTENSION_'  } sort @CompileOpts; # defines to be placed before includes 
my @CompileOpts2 = grep { substr($_, 0, 10) ne 'EXTENSION_'  } sort @CompileOpts; # defines to be placed before includes 

if (1) { # add @CompileOpts1
  print OUT "\n\n// undefining everything to avoid tons of redefinition warnings\n\n"; 

  print OUT join("\n", map { 

"  #ifdef $_\n".    
"  #undef $_\n".
"  #endif"; 

} sort @CompileOpts1)."\n";
  print OUT <<"END_OF_HEAD";

// definitions collected from makefiles 
END_OF_HEAD
  print OUT join("\n", map { 
    if ($CompileOpts{$_} ne undef) { '  #define '.$_.' '.$CompileOpts{$_}.''; }
    else                           { '  #define '.$_.' 1'; }
  } sort @CompileOpts1)."\n";
}

print OUT <<"END_OF_HEAD"; #{

// including some type definitions to improve visibility 

#ifndef TTC_BASIC_TYPES_H
#  ifndef TTC_GPIO_TYPES_H
#    include "ttc-lib/ttc_gpio_types.h"   // provides TTC_GPIO_xy definitions
#  endif
#endif

END_OF_HEAD
#}

if (1) { # add @CompileOpts2
  print OUT "\n\n// undefining everything to avoid tons of redefinition warnings\n\n"; 
  print OUT join("\n", map { 

"  #ifdef $_\n".    
"  #undef $_\n".
"  #endif"; 

} sort @CompileOpts2)."\n";
  print OUT <<"END_OF_HEAD";

// definitions collected from makefiles 
END_OF_HEAD
  print OUT join("\n", map { 
    if ($CompileOpts{$_} ne undef) { '  #define '.$_.' '.$CompileOpts{$_}.''; }
    else                           { '  #define '.$_.' 1'; }
  } sort @CompileOpts2)."\n";
}




print OUT <<"END_OF_HEAD";

#endif // TTC_COMPILE_OPTS_H
END_OF_HEAD
close(OUT);
#}
#{ write out configured memory layout

my $MemoryLD="configs/memory_current.ld";
my $Error;
open(OUT, ">$MemoryLD") or $Error = "$!"; 
if ($Error) { my $PWD=`pwd`; chomp($PWD); die("$0 ERROR: Cannot write to linker script '$PWD/$MemoryLD' ($Error)\n"); }
my $Prefix = "TTC_MEMORY_REGION_";

my @Names = grep { $_; }
            map {
              /$Prefix([A-Z,a-z,0-9,_]+)_.+$/;
              $1; }
            grep { (index($_, $Prefix) > -1) }
            keys %CompileOpts;
my @Regions;
my %Regions;
foreach my $Name (@Names) {
  unless ($Regions{$Name}) {
    $Regions{$Name} = {};
    push @Regions, lc($Name);
  }
}
print "\nMemory Regions found in Makefile: ".
      join(', ', @Regions)."\n";
    
foreach my $Region (@Regions) {
  $Regions{$Region} = {
    Size   => ($CompileOpts{$Prefix.uc($Region).'_SIZEK'} + 0).'K',
    Start  => ($CompileOpts{$Prefix.uc($Region).'_START'}),
    Access => ($CompileOpts{$Prefix.uc($Region).'_ACCESS'})
  };
  
  unless ($Regions{$Region}->{Start})  { $Regions{$Region}->{Start} = "0x00000000"; }
  unless ($Regions{$Region}->{Access}) { $Regions{$Region}->{Start} = "rxw"; }
}

my $MaxWidth1 = 0;
my $MaxWidth2 = 0;
map { # find MaxWidths
  if ($MaxWidth1 < length($_)) 
  { $MaxWidth1 = length($_); }
  
  if ($MaxWidth2 < length($Regions{$_}->{Access})) 
  { $MaxWidth2 = length($Regions{$_}->{Access}); }
  
} @Regions;
my $Spaces;
while (length($Spaces) < $MaxWidth1) { $Spaces .= ' '; }

my $Memories;
@Regions = sort { # sort regions by their origin address
  my $A10 = hex($Regions{$a}->{Start});
  my $B10 = hex($Regions{$b}->{Start});
  
  $A10 <=> $B10; # compare decimal start addresses
} @Regions;
foreach my $Region (@Regions) {
  $Memories .= "  ".lc(substr($Spaces.$Region, - $MaxWidth1))." (".
               lc( substr($Regions{$Region}->{Access}.$Spaces, 0, $MaxWidth2) ).") : ORIGIN = ".
               $Regions{$Region}->{Start}.",  LENGTH = ".
               $Regions{$Region}->{Size}."\n";  
}

my $MemoryGenericLD="configs/memory_project.ld";
if (! -e "$MemoryGenericLD") {
  open(OUT2, ">$MemoryGenericLD") or $Error = "$!"; 
  if ($Error) { my $PWD=`pwd`; chomp($PWD); die("$0 ERROR: Cannot write to linker script '$PWD/$MemoryGenericLD' ($Error)\n"); }
  print OUT2 <<"END_OF_MEMORY";
/** { memory_project.ld **********************************************
 *
 *                          The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Project specific memory section definitions.
 *  This file has to define all memory sections being required by The ToolChain library.
 *  If your project requires additional settings you may define them here.
 *  This script is automatically included by one of the architecture specific linker
 *  scripts in ttc-lib/_linker/.
 *
 */

/* _ttc_size_stack - The main stack size
 *
 * The main stack is used at system startup and for single tasking applications (if FreeRTOS is disabled).
 * If a multitasking scheduler is activated then this defines the stack size being used for the task scheduler.
 * In many cases this allows to reduce the stack size as the scheduler may not use that much memory.
 * Stack sizes must be choosen for each application individually for optimal RAM usage!
 */
_ttc_size_stack = 0x400;

/* Include basic memory layout.
 * You may disable this line and define your own, project specific memory layout here.
 * Please read and understand the basic memory layout to define all symbols required by TTC library!
 */ 
INCLUDE ttc-lib/_linker/memory_basic.ld

END_OF_MEMORY
  close(OUT2);
}

print OUT <<"END_OF_MEMORY"; #{ memory layout derived from defines 
/* Define memory spaces.
 *
 * Memory layout automatically created by $0
 *
 * The memory regions listed here have been collected from extensions.active/makefile.
 * Memory regions can be defined in makefile by adding a constant definition "-D" to variable COMPILE_OPTS.
 * Each constant must be prefixed by "TTC_MEMORY_REGION_".
 * Format: TTC_MEMORY_REGION_<region>_<field>
 *         - <region> being the name of a new memory region (will be lowercased) 
 *         + <field>  being one from
 *           - START  the start address (hexadecimal)
 *           - SIZEK  size of region in kB
 *           - ACCESS a set of access bits from (rwx)
 *
 * Example:
 * COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_EXTERNAL_START=0x60000000
 * COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_EXTERNAL_SIZEK=1048576
 * COMPILE_OPTS += -DTTC_MEMORY_REGION_RAM_EXTERNAL_ACCESS=rwx
 *
 * Output:
 * ram_external (rwx) : ORIGIN = 0x60000000,  LENGTH = 1048576K
 */

MEMORY
{
$Memories
}

END_OF_MEMORY
close(OUT); #}

#}memory layout
#}
#{ append some extra files to list of project files
my @ConfigFiles = split("\n", `ls configs/`);
foreach my $ConfigFile (@ConfigFiles) { $ProjectFiles{"configs/$ConfigFile"} = 1; }
$ProjectFiles{makefile} = 1;

if (-e "main.elf.S") { $ProjectFiles{"main.elf.S"} = 1; }
#}

if (1) { # read in files from extensions.local/
  my $Files = `ls extensions.local/`;
  my @Files = split("\n", $Files);
  map { $ProjectFiles{'extensions.local/'.$_} = 1; 
  } @Files;
}

# write out list of all files belonging to this project 
open(OUT, ">$ProjectFile") or die("$0 ERROR: Cannot write to project file '$ProjectFile' ($!)\n");
print OUT join("\n", map { $FilePrefix.$_; } sort keys %ProjectFiles)."\n";
close(OUT);

#X $ProjectFiles{$PWD.'/ttc-lib/' => 1}; # -> additionals/current/ttc-lib/ 
createSymlinks('additionals/current', \%ProjectFiles);

exit $ReturnCode;

sub processFiles() {           # scan given files for includes
  my $RecursionDepth = shift;   # increased by every recursion
  my $IncludedBy     = shift;   # name of file which included @Files2Process
  my @Files2Process = @_;
  my $Spaces;
  for (my $I = 0; $I < $RecursionDepth; $I++) { $Spaces .= '  '; }
  
  foreach my $File2Process (@Files2Process) {        # scan for all included files
    @ErrorMessages = ();
    my $FilePath = findFile($File2Process);
    unless ($FilePath) {
      if ( lc(substr($File2Process, -2) ) eq '.c' ) { # maybe we can find a corresponding assembly source?
        $FilePath = findFile(substr($File2Process, 0, -2).'.s');
      }
    }
    unless ($FilePath) {
      if ($RecursionDepth > 0) 
        { print $Spaces."2+ $IncludedBy #include $File2Process - ERROR: ".shift(@ErrorMessages)."!\n"; }
      else
        { print "$File2Process - ERROR: ".shift(@ErrorMessages)."!\n"; }
    }
    else {
      if ( addEntries(\@FilesProcessed, \%FilesProcessed, $FilePath) ) {
        if (open(IN, "<$FilePath")) {
          $ProjectFiles{ $FilePath } = 1;
          if ($RecursionDepth > 0)
            { print $Spaces."3+ $IncludedBy: #include $File2Process (-> $FilePath)\n"; }
          else
            { print "1+ $File2Process ($FilePath)\n"; }
          my @Lines = <IN>;
          close(IN);
          my $Lines = join("", @Lines);
          if (0) { # concatenate multi-lines (corrupts line-number calculation)
            $Lines =~ s/\n\r/\n/g;
            $Lines =~ s/\r\n/\n/g;
            $Lines =~ s/\\\n//g;
          }
          
          my $Pos = index($Lines, '#include ');
          while ($Pos > 0) {
            $Pos += 9;
            my $StartPos = findNext($Lines, $Pos, '<', '"') + 1;
            my $EndPos;
            my $StartChar = substr($Lines, $Pos, 1);
            if ($StartChar eq '<') {
              $EndPos = findNext($Lines, $Pos + 1, '>') - 1;
            }
            if ($StartChar eq '"') {
              $EndPos = findNext($Lines, $Pos + 1, '"') - 1;
            }
            if ($EndPos > $StartPos) { # found include filename
              my $IncludeName = substr($Lines, $StartPos, $EndPos - $StartPos + 1);
              my $LineNo = getLineNo($Lines, $StartPos);

              if ( addEntries(\@FilesFound, \%FilesFound, $IncludeName) ) {
                # print $Spaces."| new include '$IncludeName'\n";
                processFiles($RecursionDepth+1, $File2Process.':'.$LineNo, $IncludeName);
              }
            }
          
            $Pos = index($Lines, '#include ', $Pos);
          }
        }
        else { print "$0 - ERROR: Cannot read from file '$FilePath'"; }
      }
    }
  }
  if (0) { print "DEBUG: \n'".join("'\n'", keys %ProjectFiles)."\n"; }
}

sub findAllIncludedFiles {     # returns list of files being included by given files or any included file
  my $FilesRef      = shift;                 # array-ref: files to start with
  my $IncludeKey    = shift() || '#include'; # string that identifies an include (e.g. '#include', 'INCLUDE', ...)
  my $FoundFilesRef = shift() || {};         # names of files to not process anymore
  my $Includers1Ref = shift() || { '"' => 1, "'" => 1, '<' => 1 }; # hash-ref: characters that may enclose included filename (prefix) 
  my $Includers2Ref = shift() || { '"' => 1, "'" => 1, '>' => 1 }; # hash-ref: characters that may enclose included filename (suffix)
  
  my $IncludeKeyLength = length($IncludeKey);
  my @Files2Do = grep { $_; } @$FilesRef; # list of files to process
  my @FilesFound;
  while (@Files2Do) {
    my $File = shift(@Files2Do);
    unless ($FoundFilesRef->{$File}) { # file has not been seen so far

      $FoundFilesRef->{$File} = 1;
      push @FilesFound, $File;
      my $Lines = readFile($File);
      my @Includes = grep { (substr($_, 0, $IncludeKeyLength) eq $IncludeKey) } 
                     map { strip($_); } 
                     split("\n", $Lines);
                     
      @Includes = map {
                         my $IncludedFile = strip( substr($_,  $IncludeKeyLength) );
                         if ( $Includers1Ref->{substr($IncludedFile, 0, 1)} )
                         { $IncludedFile = substr($IncludedFile, 1); }         # remove prefixing include character
                         
                         if ( $Includers1Ref->{substr($IncludedFile, -1)} )
                         { $IncludedFile = substr($IncludedFile, 0, -1); } # remove suffixing include character

                         $IncludedFile;
                       } @Includes;
      push @Files2Do, @Includes;
    }
  }
  
  return @FilesFound;
}
my %fF_Folders;
my %fF_FilePaths;
sub findFile() {               # finds relative or absolute file-path for given filenames
  my @FileNames = @_;
  
  my @FilePaths;
  
  foreach my $FileName (@FileNames) {
    unless (defined $fF_FilePaths{$FileName}) { # file-name new: search in all corresponding directories
      my @CorrespondingDirectories = @VPath;
      my $Suffix = getSuffix($FileName);
      if ($Suffix eq 's') { push @CorrespondingDirectories, @VPathS; }
      if ($Suffix eq 'c') { push @CorrespondingDirectories, @VPathC; }
      if ($Suffix eq 'h') { push @CorrespondingDirectories, @INCLUDE_DIRS; }
      
      foreach $CorrespondingDirectory (@CorrespondingDirectories) {
        my $FilePath = $CorrespondingDirectory.'/'.$FileName;
        
        if ( -e  $FilePath) { $fF_FilePaths{$FileName} = $FilePath; break; }
      }
      unless ($fF_FilePaths{$FileName}) { # ERROR: filepath not found
        unless ($FilesNotFound{$FileName}) {
          push @ErrorMessages, "Cannot find $FileName (.$Suffix) in any of ".
                               join(' ', @CorrespondingDirectories)."\n";
        }
        $FilesNotFound{$FileName} = 1;
      }
      #X else { $ProjectFiles{ $fF_FilePaths{$FileName} } = 1; }
    }
    push @FilePaths, $fF_FilePaths{$FileName};
  }
  
  if (wantarray) { return @FilePaths; }
  else           { return $FilePaths[0]; }
}
sub findNext() {               # finds next occurence of one of given list of characters
  my $Text       = shift;
  my $Position   = shift;
  my @Chars2Find = @_;
  
  my $Length = length($Text);
  my %Chars2Find;
  foreach my $Char2Find (@Chars2Find)
  { $Chars2Find{$Char2Find} = 1; }
  
  while ( ( ! $Chars2Find{ substr($Text, $Position , 1) } ) &&
          ($Position < $Length)
        )
        { $Position++; }
  
  return $Position;
}
sub findLineStartingWith {     # find all lines that start with given string
  my $Text  = shift;  # text to search in
  my $Search = shift;  # string to search for (must be located at first non-whitespace character
  
  my @Found = grep { (substr($_, 0, length($Search)) eq $Search) }
              map { strip($_); } 
              split("\n", $Text);
  return @Found; 
}
sub findStartOfLine() {        # returns position of first  non-white character in current line
  my $Text     = shift;
  my $Position = shift;
  my $C;
  
  do {
    $C = substr($Text,  $Position, 1);
    if ( ($C ne "\n") && ($C ne "\r") ) { $Position--; }
  } while ( ($Position > 0) && ($C ne "\n") && ($C ne "\r") );
  my %WhiteChar = ( " " => 1, "\t" => 1 );
  
  while ( $WhiteChar{substr($Text,  $Position, 1)} && ($Position > 1) ) 
  { $Position++; }             # remove leading spaces
  
  return $Position;
}
sub findEndOfLine() {          # returns position of last non-white character before end of line
  my $Text     = shift;
  my $Position = shift;
  my $C;
  
  my $Length = length($Text);
  do {
    $C = substr($Text,  $Position, 1);
    if ( ($C ne "\n") && ($C ne "\r") ) { $Position++; }
  } while ( ($Position < $Length) && ($C ne "\n") && ($C ne "\r") );
  my %WhiteChar = ( " " => 1, "\t" => 1 );
  
  while ( $WhiteChar{substr($Text,  $Position, 1)} && ($Position > 1) ) 
  { $Position--; }             # remove trailing spaces
  
  return $Position;
}
sub addEntries() {             # adds given strings to list-ref + hash-ref if new
  my $ListRef = shift;
  my $HashRef = shift;
  
  my @AddedEntries;
  while (@_) {
    my $Entry   = strip( shift(@_) );
    
    if ($Entry) {
      unless ($HashRef->{$Entry}) {
        $HashRef->{$Entry} = 1;
        push @$ListRef, $Entry;
        push @AddedEntries, $Entry;
      }
      else {
         # print "DOUBLE: $Entry\n"; #D
         $HashRef->{$Entry}++;
      }
    }
  }
  
  if (wantarray) { return @AddedEntries; }
  else           { return $AddedEntries[0]; }
}
sub createSymlinks() {         # creates symbolic links for all files outside ttc-lib/
  my $TargetDir    = shift;   # directory path where to create symlinks (all files in this folder will be deleted first)
  my $ProjectFiles = shift;   # hash-ref
  
  unless (-d "$TargetDir") { mkdir($TargetDir); }
  system("rm -f $TargetDir/*");
  my $PWD= `pwd`; chomp($PWD);
  
  my @ProjectFiles = grep { 
    if ( index($_, 'ttc-lib/') == -1 ) { 1; } else {
      print "removing '$_'\n"; #D 
    0; }
  } keys %$ProjectFiles;
  print "creating ".(scalar @ProjectFiles)." symlinks to foreign files into $TargetDir/ ...\n";
  foreach my $ProjectFile (@ProjectFiles) {
    system("ln -sf '$PWD/$ProjectFile' '$TargetDir/'");
  }
  system("ln -sf '$PWD/ttc-lib' '$TargetDir/'"); # ttc-lib -> additional/current/ttc-lib 
  my $ReadMeTxt = "$TargetDir/readme.txt";
  open(OUT, ">$ReadMeTxt") or die("ERROR: Cannot create file '$ReadMeTxt' in `pwd` ($!)");
  print OUT <<"END_OF_README";

                    The ToolChain

The symlinks in this folder have been dynamically created by $0.
These are all files required for last build which are not located inside ttc-lib/.

END_OF_README
  close(OUT);
}
sub dieOnError {               # dies if error given + prints caller stack
  my @ErrorMessages = grep { $_; } @_;
  unless (@ErrorMessages) { return; } # no error no die
  
  my $Level = 1;
  my @Stack;
  my ($package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash);

  do {
     ($package, $filename, $line, $subroutine, $hasargs,
      $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
      = caller($Level++);
      if ($subroutine) { push @Stack, "$subroutine() called from ${filename}:$line"; }
  } while ($filename);
  ($package, $filename, $line, $subroutine, $hasargs,
   $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
   = caller(0);
  my $PWD = `pwd`; chomp($PWD);
  push @ErrorMessages, "PWD='$PWD'";
  push @Stack, "dieOnError() called from ${filename}:$line";
  push @ErrorMessages, @Stack;

  print( "$0 - ERROR in $subroutine(): ".join("\n    ", @ErrorMessages)."\n" );
  exit 10;
}
sub getSuffix() {              # returns filename suffix
  my $FileName = shift();
  
  my $LastDotPos = rindex($FileName, '.');
  if ($LastDotPos > -1) { return lc( substr($FileName, $LastDotPos + 1) ); }
  return undef;
}
sub getLineNo() {              # returns line number of given byte position in given string
  my $Text     = shift;
  my $Position = shift;
  
  $Text = substr($Text, 0, $Position + 1);
  
  my $LineNo = 0;
  my $LfPos = index($Text, "\n");
  while ($LfPos > -1) {
    $LineNo++;
    $LfPos = index($Text, "\n", $LfPos + 1);
  }
  
  return $LineNo;
}
sub readFile {                 # read in content of given file
  my $FilePath = shift;
  
  my $Content;
  my $Error;
  open(IN, "<$FilePath") or $Error = "$0 - ERROR: Cannot read from file '$FilePath' ($!)";
  dieOnError($Error);
  while (<IN>) { $Content .= $_; }
  close(IN);
  
  $Content =~ s/\\\n//g; # concatenate multiline strings
  
  return $Content;
}
my %s_WhiteChar;
sub strip() {                  # removes trailing and leading whitechars from given string
  my $String = shift;
  
  unless (%s_WhiteChar) {
    $s_WhiteChar{"\n"} = 1;
    $s_WhiteChar{"\r"} = 1;
    $s_WhiteChar{"\t"} = 1;
    $s_WhiteChar{" "}  = 1;
  }
  while ( $s_WhiteChar{substr($String, 0, 1)} ) { substr($String, 0, 1) = undef; }
  while ( $s_WhiteChar{substr($String, -1, 1)} )   { substr($String, -1)   = undef; }
  
  return $String;
}
sub writeFile {                # write content to given file
  my $FilePath     = shift;
  my $Content      = shift;
  my $BackUpSuffix = shift; # if given and original file exists, a backup file is being created using this suffix
  
  # ensure that directoy exists
  my $LastSlashPos = rindex($FilePath, '/');
  if ($LastSlashPos > -1) { # file is to be created in different folder: ensure that folder exists
    my $DirPath = substr($FilePath, 0, $LastSlashPos);
    createDirectoryPath($DirPath);
  }
  
  if ($Verbose>1) { print "writing to disc: $FilePath\n"; }
  if ($BackUpSuffix) {
    if (-e $FilePath) {
      system("mv \"${FilePath}\" \"${FilePath}$BackUpSuffix\"");
    }
  }
  open(OUT, ">$FilePath") or die ("$0 - ERROR: Cannot write to file '$FilePath' ($!)");
  print OUT $Content;
  close(OUT);
  
  return $Content;
}

exit 0;
