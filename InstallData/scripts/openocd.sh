#!/bin/bash

TaskNameOpenOCD="$1"
shift

function killProcesses() {
  ProcessName="$1"
  
  # remove leading path (causes trouble) 
  ProcessName=`perl -e "my \\$I=rindex('$ProcessName', '/'); print substr('$ProcessName', \\$I+1);"`
  
  echo "$0> killall -9 -ewq $ProcessName"
  killall -ewq $ProcessName
}
function printCallerStack() {        # Prefix                                displays history of function that called current one
   Prefix="$1"  # string that should prefix every displayed line
   
   echo >&2 "${Prefix}line `caller`"
   for Level in 1 2 3 4 5 6 7 8 9 10; do
     if [ "`caller $Level`" != "" ]; then
       echo >&2 "${Prefix}called from line `caller $Level`"
     fi
   done
}

killProcesses $TaskNameOpenOCD # make sure that no other one is running

rm -f $TaskNameOpenOCD # must be deleted first (otherwise which openocd may return this file)
WhichOpenOCD=`which openocd`
if [ "$WhichOpenOCD" == "" ]; then
  echo "$0 - ERROR: Cannot find openocd binary in PATH (which openocd). Check TTC installation!"
  exit 12
fi
ln -sf $WhichOpenOCD $TaskNameOpenOCD
CMD="$TaskNameOpenOCD $@"
PWD=`pwd`
echo "$PWD> $CMD" | tee openocd.log
echo ""           | tee --append openocd.log
./$TaskNameOpenOCD $@ >>openocd.log 2>&1 &

BaseNameOpenOCD=`basename $TaskNameOpenOCD`
#? while ! pgrep $BaseNameOpenOCD >/dev/null 2>&1
#? do sleep 0.1; done
PID=`pgrep $BaseNameOpenOCD`
if [ "$PID" != "" ]; then
  _/waitUntilIdle.pl $PID
fi

Error=`grep -i "Error: " openocd.log | grep -v "does not support soft_reset_halt"`
Error2=`grep 'Unable to open FTDI Device' openocd.log`
Error3=`grep 'address + size wrapped' openocd.log`
IsError="${Error}${Error2}${Error3}"
if [ "$IsError" != "" ]; then #{
  #echo "> $CMD"
  if [ "$Error" != "" ]; then
    echo "ERROR: $Error"
  fi
  if [ "$Error2" != "" ]; then
    echo "ERROR: $Error2"
  fi
  echo ""
  echo "OpenOCD has reported some errors:"
  echo ""
  cat openocd.log
 
  cat <<END_OF_MSG #{
$IsError

       Possible Solutions:
       - Check cabling PC <-> Programmer <-> Board <-> uC
       - Make sure that programmer has been recognized as USB device:
         lsusb
       - activate interface according to your JTAG-programmer
       - check these openocd config files:
         $@
END_OF_MSG
#}
  if [ "`groups | grep dialout`" == "" ]; then
    cat <<END_OF_MSG #{
        - NOTE: Current user does not belong to group dialout.
                Try this line:
                sudo usermod --append -G dialout $USER
                Then restart computer and retry!
END_OF_MSG
  #}
  fi  

#X  if [ "`cat openocd.log | grep 'address + size wrapped`" != "" ]; then
    echo "killing openocd..."
    killProcesses $TaskNameOpenOCD
    
    printCallerStack "    "
    exit 5
#X  fi
fi #}

exit 0
