#!/bin/bash
#
# Start Openocd as GDB-Server
#
# written by Gregor Rebel 2010-2018

function killProcesses() {
  ProcessName="$1"
  
  echo "$0> killall -ewq $ProcessName"
  killall -ewq $ProcessName
}

EXTRA_CFG="$1";       shift
TASKNAME_SUFFIX="$1"; shift

echo "$0 EXTRA_CFG='$EXTRA_CFG' TASKNAME_SUFFIX='$TASKNAME_SUFFIX'"

if [ "$EXTRA_CFG" == "-h" ]; then #{
  cat <<END_OF_HELP
$0 [EXTRA.CFG]

EXTRA.CFG   path to extra openocd configuration file to use

END_OF_HELP
  exit 0
fi #}

if [ "$EXTRA_CFG" != "" ]; then
  if [ ! -e $EXTRA_CFG ]; then
    echo "$0 - ERROR: Cannot find extra openocd configuration file '$EXTRA_CFG' (PWD=`pwd`)"
    exit 10
  fi
  EXTRA_CFG="-f $EXTRA_CFG"
fi

#X Suffix=`perl -e "my \\$I=rindex('$EXTRA_CFG', '_'); my \\$B = substr('$EXTRA_CFG', \\$I+1); my \\$J=index(\\$B, '.'); print substr(\\$B, 0, \\$J);"`
TaskName_OpenOCD="openocd_$TASKNAME_SUFFIX"
TaskNameGDB="arm-none-eabi-gdb_$TASKNAME_SUFFIX"

killProcesses $TaskName_OpenOCD  
killProcesses $TaskNameGDB
echo "killall 2>/dev/null -ewq $TaskName_OpenOCD" >>kill_gdb.sh
echo "killall 2>/dev/null -ewq $TaskNameGDB"      >>kill_gdb.sh
chmod +x kill_gdb.sh

if [ -e configs/openocd_debug.cfg ]; then
  Config_Debug="-f configs/openocd_debug.cfg"
fi

if [ "`ls extensions.active/*060_arm_semihosting 2>/dev/null`" != "" ]; then
  ConfigFile="configs/openocd_semihosting.cfg"
  cat >$ConfigFile <<END_OF_FILE

# enable semihosting debug feature
arm semihosting enable

END_OF_FILE

  SemiHosting="-f $ConfigFile"
else
  SemiHosting=""
fi

Options=" --search additionals/999_open_ocd -f configs/openocd_interface.cfg -f configs/openocd_target.cfg $EXTRA_CFG $Config_Debug  -c init $SemiHosting -c reset init"
echo ">_/openocd.sh bin/$TaskName_OpenOCD '$Options'"
IsRunning=""
typeset -i Retries
Retries=0
while [ "$IsRunning" == "" ]; do
  Retries=$Retries+1
  if [ "$Retries" == "10" ]; then
    echo "Aborting gdb server!"
    exit 10
  fi
  echo "starting openocd as gdb server [$Retries/10]"
  
  _/openocd.sh bin/$TaskName_OpenOCD "$Options"
  IsRunning=`pgrep $TaskName_OpenOCD`
  if  [ "$IsRunning" == "" ]; then
    echo "Failed to start openocd as pgrep $TaskName_OpenOCD == "" !)"
  fi
done
echo "Successfully started openocd as gdb-server ($IsRunning)."
sleep 1

if [ "$IsRunning" == "" ]; then
  echo ""
  echo "$0 - ERROR: Cannot start openocd as GDB-server!"
  exit 10
#else
  #echo "$0 - GDB-Server successfully started. Connect your arm-none-eabi-gdb to 127.0.0.1:3333"
fi

