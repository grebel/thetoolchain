#!/bin/bash
#
# Flash script for STM DFU-Bootloader
#
# Authors: Sascha Poggemann (2012), Gregor Rebel (2013)
#

Binary="$1"
if [ "$Binary" == "" ]; then
  Binary="main.bin"
fi
if [ ! -e "$Binary" ]; then
  echo "$0 BINARY"
  echo "  ERROR: Cannot find binary $Binary!"
  exit 5
fi

# ensure that no other openocd is currently connected
pkill -9 openocd && {
    echo "$0 - openocd was running: killing it"
    sleep 0.2 # short delay, for SIGKILL signal to be processed
}

CMD="dfu-util -a 0 -s 0x08000000:leave -D $Binary -v -v -v"
echo "> $CMD"
$CMD || Error="1"

if [ "$Error" == "" ]; then
  echo "Successfully flashed"
else
  exit 10
fi

