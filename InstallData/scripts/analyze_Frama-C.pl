#!/usr/bin/perl

my $CompileLog = readFile("compile.log");

my %SourceFiles; 

my $Key = "Stage: Compiling source file";
my $LastPos = 0;
my $Pos = index($CompileLog, $Key);
while ($Pos > -1) {
  # { Stage: Compiling source file ttc-lib/ttc_extensions.c ******************************************************************************************************************
  #
  # arm-none-eabi-gcc -std=gnu99 -c -fno-common -fno-builtin -ffreestanding -funsigned-char -DEXTENSION_050_compiler_gcc_arm_binary=1 -DSOURCE_COMPILER=050_compiler_gcc_arm_binary -msoft-float -fshort-enums  -fpack-struct -DEXTENSION_basic_extensions_basic_setup=1 -DEXTENSION_basic_extensions_basic_setup=1   -fno-strict-aliasing -DEXTENSION_basic_extensions_optimize_1_debug=1 -DEXTENSION_100_board_uwb_sensor10=1 -DTTC_BOARD -DTTC_SYSCLOCK_EXTERNAL_HIGHSPEED_CRYSTAL=8000000 -DTTC_RADIO1_DW1000_PIN_EXT_ON=E_ttc_gpio_pin_c4                  -DTTC_RADIO1_DW1000_PIN_WAKEUP=E_ttc_gpio_pin_c5                  -DTTC_RADIO1_DW1000_PIN_RX_OK=E_ttc_gpio_pin_c7                   -DTTC_RADIO1_DW1000_PIN_START_FRAME_DETECTED=E_ttc_gpio_pin_c8    -DTTC_RADIO1_DW1000_PIN_EXTPA=E_ttc_gpio_pin_b10                  -DTTC_RADIO1_DW1000_PIN_SPIPOL=E_ttc_gpio_pin_c6        -DTTC_RADIO1_DW1000_PIN_IRQ=E_ttc_gpio_pin_a2           -DTTC_RADIO1_DW1000_TX_POWER=0x15355575      -DTTC_RADIO1_INDEX_SPI=1                           -DTTC_RADIO1_INDEX_SPI_NSS=1                       -DTTC_RADIO1_DW1000_PIN_RESET=E_ttc_gpio_pin_a3         -DTTC_RADIO1_DW1000_PIN_SPIPHA=E_ttc_gpio_pin_c9        -DTTC_SPI1=ttc_device_1                      -DTTC_SPI1_MOSI=E_ttc_gpio_pin_a7                       -DTTC_SPI1_MISO=E_ttc_gpio_pin_a6                       -DTTC_SPI1_SCK=E_ttc_gpio_pin_a5                        -DTTC_SPI1_NSS1=E_ttc_gpio_pin_b1                       -DTTC_USB_ENABLE=E_ttc_gpio_pin_a8 -DTTC_USB_DP=E_ttc_gpio_pin_a12 -DTTC_USB_DM=E_ttc_gpio_pin_a11 -DTTC_RTC_CLOCK_SOURCE_LSE -DTTC_USART1=ttc_device_1  -DTTC_USART1_TX=E_ttc_gpio_pin_a9      -DTTC_USART1_RX=E_ttc_gpio_pin_a10     -DEXTENSION_250_CPAL_STM32L1xx_StdPeriph_Driver=1 -DEXTENSION_250_stm_std_peripherals=1 -DEXTENSION_350_programmer_openocd_stlink_v2=1 -DEXTENSION_basic_cm3=1 -DEXTENSION_basic_stm32l1xx=1 -DEXTENSION_cpu_cortexm3=1 -DTARGET_ARCHITECTURE_CM3 -DTARGET_DATA_WIDTH=32 -DTTC_MEMORY_REGION_CODE_START=0x60000000 -DTTC_MEMORY_REGION_CODE_SIZEK=1048576 -DTTC_MEMORY_REGION_CODE_ACCESS=rwx -DTTC_MEMORY_REGION_SRAM_START=0x60000000 -DTTC_MEMORY_REGION_SRAM_SIZEK=1048576 -DTTC_MEMORY_REGION_SRAM_ACCESS=rwx -DTTC_MEMORY_REGION_SRAM_BITBAND_START=0x22000000 -DTTC_MEMORY_REGION_SRAM_BITBAND_SIZEK=32768 -DTTC_MEMORY_REGION_SRAM_BITBAND_ACCESS=rw -DTTC_MEMORY_REGION_PERIPHERAL_START=0x40000000 -DTTC_MEMORY_REGION_PERIPHERAL_SIZEK=1024 -DTTC_MEMORY_REGION_PERIPHERAL_ACCESS=rw -DTTC_MEMORY_REGION_PERIPHERAL_BITBAND_START=0x42000000 -DTTC_MEMORY_REGION_PERIPHERAL_BITBAND_SIZEK=32768 -DTTC_MEMORY_REGION_PERIPHERAL_BITBAND_ACCESS=rw -DTTC_MEMORY_REGION_RAM_EXTERNAL_START=0x60000000 -DTTC_MEMORY_REGION_RAM_EXTERNAL_SIZEK=1048576 -DTTC_MEMORY_REGION_RAM_EXTERNAL_ACCESS=rwx -DTTC_MEMORY_REGION_DEVICE_EXTERNAL_START=0xa0000000 -DTTC_MEMORY_REGION_DEVICE_EXTERNAL_SIZEK=1048576 -DTTC_MEMORY_REGION_DEVICE_EXTERNAL_ACCESS=rw -DTTC_MEMORY_REGION_PRIVATE_PERIPHERAL_START=0xe0000000 -DTTC_MEMORY_REGION_PRIVATE_PERIPHERAL_SIZEK=1024 -DTTC_MEMORY_REGION_PRIVATE_PERIPHERAL_ACCESS=rw -DTTC_MEMORY_REGION_VENDOR_MEMORY_START=0xe0100000 -DTTC_MEMORY_REGION_VENDOR_MEMORY_SIZEK=523264 -DTTC_MEMORY_REGION_VENDOR_MEMORY_ACCESS=rw -DEXTENSION_cpu_stm32l1xx=1 -DTARGET_ARCHITECTURE_PREFIX=STM32 -DTARGET_ARCHITECTURE_STM32L1xx -DTARGET_DATA_WIDTH=32 -DUSE_STDPERIPH_DRIVER -DUSE_FULL_ASSERT=1 -DTTC_REAL_TIME_CLOCK_AMOUNT=1 -DTTC_REAL_TIME_CLOCK1 -DTTC_INTERRUPT_GPIO_AMOUNT=16 -DTTC_MEMORY_REGION_RAM_START=0x20000000 -DTTC_MEMORY_REGION_RAM_ACCESS=rxw -DTTC_MEMORY_REGION_ROM_START=0x08000000 -DTTC_MEMORY_REGION_ROM_ACCESS=rx -DTTC_MEMORY_REGION_EEPROM_START=0x08080000 -DTTC_MEMORY_REGION_EEPROM_ACCESS=rx -DTTC_MEMORY_REGION_MB1_SIZEK=0 -DTTC_MEMORY_REGION_MB1_START=0x60000000 -DTTC_MEMORY_REGION_MB1_ACCESS=rx -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32l100rbt6 -DTTC_MEMORY_REGION_RAM_SIZEK=10 -DTTC_MEMORY_REGION_ROM_SIZEK=128 -DTTC_MEMORY_REGION_EEPROM_SIZEK=2 -DTTC_TIMER1=ttc_device_2        -DTTC_TIMER1_CHANNELS=4          -DTTC_TIMER1_WIDTH=16            -DTTC_TIMER1_UP=1                -DTTC_TIMER1_DOWN=1              -DTTC_TIMER1_UP_DOWN=1           -DTTC_TIMER1_PRESCALE_MIN=1      -DTTC_TIMER1_PRESCALE_MAX=65535  -DTTC_TIMER1_PWM=4               -DTTC_TIMER1_PWM1_PIN1=E_ttc_gpio_pin_a15  -DTTC_TIMER1_PWM2_PIN1=E_ttc_gpio_pin_b3   -DTTC_TIMER1_PWM3_PIN1=E_ttc_gpio_pin_b10  -DTTC_TIMER1_PWM4_PIN1=E_ttc_gpio_pin_b11  -DTTC_TIMER1_PWM1_PIN2=E_ttc_gpio_pin_a0   -DTTC_TIMER1_PWM2_PIN2=E_ttc_gpio_pin_a1   -DTTC_TIMER1_PWM3_PIN2=E_ttc_gpio_pin_a2   -DTTC_TIMER1_PWM4_PIN2=E_ttc_gpio_pin_a3   -DTTC_TIMER1_CAPTURES=4          -DTTC_TIMER1_CAPTURE1_PIN1=E_ttc_gpio_pin_a15  -DTTC_TIMER1_CAPTURE2_PIN1=E_ttc_gpio_pin_b3   -DTTC_TIMER1_CAPTURE3_PIN1=E_ttc_gpio_pin_b10  -DTTC_TIMER1_CAPTURE4_PIN1=E_ttc_gpio_pin_b11  -DTTC_TIMER1_CAPTURE1_PIN2=E_ttc_gpio_pin_a0   -DTTC_TIMER1_CAPTURE2_PIN2=E_ttc_gpio_pin_a1   -DTTC_TIMER1_CAPTURE3_PIN2=E_ttc_gpio_pin_a2   -DTTC_TIMER1_CAPTURE4_PIN2=E_ttc_gpio_pin_a3   -DTTC_TIMER2=ttc_device_3 -DTTC_TIMER3=ttc_device_4 -DTTC_TIMER4=ttc_device_6 -DTTC_TIMER5=ttc_device_7 -DTTC_TIMER6=ttc_device_9 -DTTC_TIMER7=ttc_device_10 -DTTC_TIMER7_PWM=1               -DTTC_TIMER7_PWM1_PIN1=E_ttc_gpio_pin_b12  -DTTC_TIMER7_PWM1_PIN2=E_ttc_gpio_pin_b98   -DTTC_TIMER7_CAPTURES=1          -DTTC_TIMER7_CAPTURE1_PIN1=E_ttc_gpio_pin_b12  -DTTC_TIMER7_CAPTURE1_PIN2=E_ttc_gpio_pin_b8   -DTTC_TIMER8=ttc_device_11 -DTTC_TIMER8_PWM=1               -DTTC_TIMER8_PWM1_PIN1=E_ttc_gpio_pin_b15  -DTTC_TIMER8_PWM1_PIN2=E_ttc_gpio_pin_b9   -DTTC_TIMER8_CAPTURES=1          -DTTC_TIMER8_CAPTURE1_PIN1=E_ttc_gpio_pin_b15  -DTTC_TIMER8_CAPTURE1_PIN2=E_ttc_gpio_pin_b9   -DTTC_CPU_VARIANT=TTC_CPU_VARIANT_stm32l100rbt6 -DuCONTROLLER=STM32L1XX_MD -DSTM32L1XX_MD -DEXTENSION_fake_sbrk_support=1 -DEXTENSION_gpio_stm32l1xx=1 -DEXTENSION_heap_zdefault=1 -DEXTENSION_interrupt_cortexm3=1 -DEXTENSION_interrupt_stm32l1xx=1 -DEXTENSION_math_software_float=1 -DEXTENSION_memory_stm32l1xx=1 -DEXTENSION_packet_802154=1 -DEXTENSION_radio_dw1000=1 -DTTC_RADIO1=0 -DTTC_RADIO1_DRIVER=ta_radio_dw1000 -DTTC_RADIO1_BOOST_MODE=0 -DTTC_RADIO1_ALTERNATE_TX_PATH=0 -DTTC_RADIO1_PIN_TX_ACTIVE=E_ttc_gpio_pin_c5 -DTTC_RADIO1_PIN_TX_LOWACTIVE=0 -DTTC_PACKET_RANGING=1 -DEXTENSION_register_stm32l1xx=1 -DEXTENSION_spi_stm32l1xx=1 -DEXTENSION_string_ascii=1 -DEXTENSION_sysclock_stm32l1xx=1 -DEXTENSION_systick_cortexm3=1 -DEXTENSION_usart_stm32l1xx=1 -DEXTENSION_ttc_assert=1 -DEXTENSION_ttc_basic=1 -DEXTENSION_ttc_cpu=1 -DEXTENSION_ttc_gpio=1 -DEXTENSION_ttc_heap=1 -DEXTENSION_ttc_interrupt=1 -DEXTENSION_ttc_list=1 -DEXTENSION_ttc_math=1 -DEXTENSION_ttc_memory=1 -DEXTENSION_ttc_mutex=1 -DEXTENSION_ttc_mutex_cortexm3=1 -DEXTENSION_ttc_packet=1 -DEXTENSION_ttc_queue=1 -DEXTENSION_ttc_radio=1 -DEXTENSION_ttc_register=1 -DEXTENSION_ttc_semaphore=1 -DEXTENSION_ttc_semaphore_cortexm3=1 -DEXTENSION_ttc_spi=1 -DEXTENSION_ttc_string=1 -DEXTENSION_ttc_sysclock=1 -DEXTENSION_ttc_systick=1 -DEXTENSION_ttc_task=1 -DEXTENSION_ttc_usart=1 -DEXTENSION_600_example_ttc_radio_ranging=1 -DEXTENSION_700_extra_settings -DNODE_INDEX=100 -Wall -Wshadow -Wwrite-strings -Winline -g3  -fmessage-length=0 -I ./ -I configs/ -I additionals/050_compiler_gcc_arm_binary/arm-none-eabi/include/ -I additionals/050_compiler_gcc_arm_binary/lib/gcc/arm-none-eabi/4.4.1/include/ -I ttc-lib/ -I ttc-lib/scheduler/ -I examples/ -I regressions/ -I extensions.active/ -I additionals/250_stm_std_peripherals/inc/ -I additionals/270_CPAL_CMSIS/CM3/CoreSupport/ -I additionals/270_CPAL_CMSIS/CM3/DeviceSupport/ST/STM32L1xx/ -Iadditionals/270_CPAL_CMSIS/CM3/CoreSupport/ -Ittc-lib/assert/ -Ittc-lib/basic/ -Ittc-lib/cpu/ -Ittc-lib/gpio/ -Ittc-lib/heap/ -Ittc-lib/interrupt/ -Ittc-lib/math/ -Ittc-lib/memory/ -Ittc-lib/packet/ -Ittc-lib/radio/ -Ittc-lib/register/ -Ittc-lib/spi/ -Ittc-lib/string/ -Ittc-lib/sysclock/ -Ittc-lib/systick/ -Ittc-lib/usart/ -mcpu=cortex-m3 -mthumb  ttc-lib/ttc_extensions.c
  #
  # }

  
  my $NextLinePos = index($CompileLog, "\n", $Pos);
  if ($NextLinePos > -1) {
    my $CloseBracketPos = index($CompileLog, "}", $Pos);
    my $CompileLine = strip( substr($CompileLog, $NextLinePos, $CloseBracketPos - $NextLinePos) );
    
    my $PosFilePath = $Pos + length($Key); 
    my @FilePathLine = split(" ",  substr($CompileLog, $PosFilePath, $NextLinePos - $PosFilePath) );
    my $FilePath = $FilePathLine[0];
    
    unless ($SourceFiles{$FilePath}) {
      $SourceFiles{$FilePath} = { Compile => $CompileLine };
    }
  }
  $LastPos = $Pos;
  $Pos = index($CompileLog, $Key, $LastPos + 1);
}
#{ write out start script for static code analyzer Frama-C
my $Script="analyze_Frama-C.sh";
my $Lines = <<"END_OF_HEAD"; #{
#!/bin/bash

END_OF_HEAD
#}

my %IgnoreFile = ( #{
                 #  'additionals/250_stm_std_peripherals/src/misc.c' => 1,
                 #  'additionals/270_CPAL_CMSIS/CM3/CoreSupport/core_cm3.c' => 1,
                 ); #}

# extract first compile line as template to compile all sources
my @SourceFiles = grep { !$IgnoreFile{$_}; } sort keys %SourceFiles;
my $FilePath  = $SourceFiles[0];
my $FirstCompileLine = $SourceFiles{$FilePath}->{Compile};
$FirstCompileLine =~ s/$FilePath//;
$FirstCompileLine =~ s/\s/ /g;
my @FirstCompileLine = split(" ", $FirstCompileLine);
my $Compiler = shift(@FirstCompileLine);
$FirstCompileLine = join(" ", @FirstCompileLine);

my $CMD_Compile   = "frama-c -no-pp-annot -cpp-gnu-like -cpp-command \"$Compiler\" -cpp-extra-args=\"$FirstCompileLine\"";
my $CMD_Typecheck = "frama-c -no-pp-annot -cpp-gnu-like -cpp-command \"$Compiler\" -cpp-extra-args=\"$FirstCompileLine\" -main main -typecheck";

if (1) { # compile one big frama-c run for all files
    $Lines .= $CMD_Compile.
              " \\\n        ".join(" \\\n        ", @SourceFiles)."\n\n\n";
    $Lines .= $CMD_Typecheck." \\\n        ".
              join(" \\\n        ", @SourceFiles)."\n";
}
else {  # compile one frama-c run for each file
    $Lines .= join("\n", map {
                               $CMD_Compile.' '.$_;
                             } @SourceFiles)."\n\n\n";

    $Lines .= join("\n", map {
                               $CMD_Typecheck.$_;
                             } @SourceFiles)."\n";

}
writeFile($Script, $Lines);
chmod(0700, $Script);
#}

sub readFile {                 # read in content of given file
  my $FilePath = shift;
  
  my $Content;
  my $Error;
  open(IN, "<$FilePath") or $Error = "$0 - ERROR: Cannot read from file '$FilePath' ($!)";
  dieOnError($Error);
  while (<IN>) { $Content .= $_; }
  close(IN);
  
  $Content =~ s/\\\n//g; # concatenate multiline strings
  
  return $Content;
}
sub strip() {                  # removes trailing and leading whitechars from given string
  my $String = shift;
  
  unless (%s_WhiteChar) {
    $s_WhiteChar{"\n"} = 1;
    $s_WhiteChar{"\r"} = 1;
    $s_WhiteChar{"\t"} = 1;
    $s_WhiteChar{" "}  = 1;
  }
  while ( $s_WhiteChar{substr($String, 0, 1)} ) { substr($String, 0, 1) = undef; }
  while ( $s_WhiteChar{substr($String, -1, 1)} )   { substr($String, -1)   = undef; }
  
  return $String;
}
sub writeFile {                # write content to given file
  my $FilePath     = shift;
  my $Content      = shift;
  my $BackUpSuffix = shift; # if given and original file exists, a backup file is being created using this suffix
  
  # ensure that directoy exists
  my $LastSlashPos = rindex($FilePath, '/');
  if ($LastSlashPos > -1) { # file is to be created in different folder: ensure that folder exists
    my $DirPath = substr($FilePath, 0, $LastSlashPos);
    createDirectoryPath($DirPath);
  }
  
  if ($Verbose>1) { print "writing to disc: $FilePath\n"; }
  if ($BackUpSuffix) {
    if (-e $FilePath) {
      system("mv \"${FilePath}\" \"${FilePath}$BackUpSuffix\"");
    }
  }
  open(OUT, ">$FilePath") or die ("$0 - ERROR: Cannot write to file '$FilePath' ($!)");
  print OUT $Content;
  close(OUT);
  
  return $Content;
}
sub dieOnError {               # dies if error given + prints caller stack
  my @ErrorMessages = grep { $_; } @_;
  unless (@ErrorMessages) { return; } # no error no die
  
  my $Level = 1;
  my @Stack;
  my ($package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash);

  do {
     ($package, $filename, $line, $subroutine, $hasargs,
      $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
      = caller($Level++);
      if ($subroutine) { push @Stack, "$subroutine() called from ${filename}:$line"; }
  } while ($filename);
  ($package, $filename, $line, $subroutine, $hasargs,
   $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash)
   = caller(0);
  my $PWD = `pwd`; chomp($PWD);
  push @ErrorMessages, "PWD='$PWD'";
  push @Stack, "dieOnError() called from ${filename}:$line";
  push @ErrorMessages, @Stack;

  print( "$0 - ERROR in $subroutine(): ".join("\n    ", @ErrorMessages)."\n" );
  exit 10;
}
