Extensions are grouped into ranks. Each rank has a three digit rank number that creates an order between these groups.
The rank number is part of the extension name. 
Higher ranks depend on lower ranks. Normally, an activate script of a certain rank will only call other activate scripts of lower ranks.

E.g.: 200_std_peripherals_library
      300_scheduler_freertos
      500_ttc_debug_registers
      600_example_gfx
      999_libftd2xx

Currently these rank-numbers are defined:
  000  Basic settings
  050  System libraries (e.g. provided by a compiler)
  100  Basic definitions that do not depend on other features (e.g. board-makefiles)
  150  Board extension definitions
  200  Basic libraries that may depend on basic definitions (e.g. std_peripherals_library)
  300  Multitasking scheduler depending on other libraries in rank 200 (e.g. freertos)
  350  Device programmer/ debugger
  400  Libraries provided by external sources
  420  Low level chip drivers for special chips placed on active board/ board extension (these get activated from rank 100)
  450  Low level architecture dependent libraries provided by TheToolChain
  500  High level libraries provided by The ToolChain
  600  Example frameworks that may provide functionality for higher ranked frameworks (e.g. example_leds)
  700  Regression tests and self tests
  800  Reserved for future use
  999  Features that are not usable from source-code (e.g. open_ocd)
