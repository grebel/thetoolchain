#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Initial Script written by Gregor Rebel 2010-2018.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "000_Content" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation www.eecs.umich.edu/eecs/courses/eecs373/readings/ Linker.pdf The_GNU-Linker.pdf Compiler
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ download + install software
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe 'which autoreconf' autoreconf

    installPackageSafe 'which astyle' astyle
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $Install_Dir ..."
    TarballDir="$HOME/Source/TheToolChain.tarballs"
    Install_Path=`pwd`

    Version=`cat ../../Version`
    dir $TarballDir
    cd $TarballDir
    cat <<END_OF_INFO >readme.TarBalls #{

                               The ToolChain
                               
                           TarBall Download Folder
                            
Each file in this folder is a compressed bundle of all download content required by the
corresponding ToolChain installation. The tarballs are stored here to reduce network traffic
when a toolchain is removed and installed a second time. You may delete these tarballs after 
successfull installation.

END_OF_INFO
#}
    Archive=TheToolChain.Contents_${Version}.tar.bz2
    OldTimeStamp=`stat -c%y $Archive`
    if [ -e $Archive ]; then #{ archive exists: download only if newer one exists
      echo "Old content tarball exists: will download only if server has newer one.." 
      wget -N -c --progress=bar:force --timeout=60 http://thetoolchain.com/mirror/$Archive
    #}
    else #{
      echo "Content tarball not found - downloading new one: `pwd`/$Archive"
      get http://thetoolchain.com/mirror/ $Archive $Archive
    fi #}
    NewTimeStamp=`stat -c%y $Archive`
    if [ "$OldTimeStamp" != "" ] && [ "$OldTimeStamp" == "$NewTimeStamp" ]; then
      TarBallUnchanged="1"
    else
      TarBallUnchanged=""
    fi
    cd "$Install_Path"
    createLink $TarballDir/$Archive . 
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    if [ -e "$Archive" ]; then #{
      if [ ! -e OK.$Archive ]; then
        TarBallUnchanged="" 
        echo "archive not yet tested: force integrity test"
      fi
      if [ "$TarBallUnchanged" == "" ]; then
        echo "`pwd`> testing archive integrity: $Archive .."
        ArchiveCorrupt=`bzip2 -t $Archive 2>&1`
      fi
      if [ "$ArchiveCorrupt" == "" ]; then #{
        touch OK.$Archive
        ArchDir=`pwd`
        ContentDir="$HOME/Source/TheToolChain.Contents/"
        dir $ContentDir
        cd $ContentDir
        echo "extracting contents archive $ArchDir/$Archive -> $ContentDir ..."
        if [ "$TarBallUnchanged" == "" ] || [ ! -d "$ArchDir" ]; then
          tar xjvf $ArchDir/$Archive
        fi
        cd $ArchDir
        if [ "$TargetUser" != "" ]; then #{
          chown -R $TargetUser:$TargetGroup .
        else
          sudo chown -R $USER:$GROUPS . 
        fi #}
        echo "" >OK.Install
      else
        echo "$ScriptName - ERROR: Corrupt archive `pwd`/${Archive}. Aborting installation. Rerun install script to retry!"
        mv $TarballDir/$Archive $TarballDir/${Archive}.defect
      fi #}
    fi #}
  else
    echo "$ScriptName - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts
  Name="${Install_Dir}"
  
  cd "$OldPWD"
  echo "Installed successfully: $Install_Dir"
#}
else                            #{ Install failed
  echo "failed to install $Install_Dir"
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
