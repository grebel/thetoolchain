#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Initial Script written by Gregor Rebel 2010-2018.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="500"
EXTENSION_SHORT="gui_mt"
EXTENSION_PREFIX="ttc"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

      echo "" >OK.Install

if [   -e OK.Install ]; then    #{ create makefiles and activate scripts
  Name="${Install_Dir}"
  
  createExtensionMakefileHead ${Name}      #{ create makefile
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 
  
# additional object files to be compiled
MAIN_OBJS += ttc_gui_mt.o

# we have to define at least one font to make ttc_font.h happy
COMPILE_OPTS += -DTTC_FONT1=font_type1_16x24

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} "$ScriptName 2" "architecture independent support for gui devices (ToDo)" #{
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# remove activated variants of same type
# rm 2>/dev/null \$Dir_Additionals/$Install_Dir_*

activate.510_ttc_font__font_type1_16x24.sh QUIET "\$ScriptName"
activate.500_ttc_memory.sh QUIET "\$ScriptName"
activate.500_ttc_gfx_mt.sh  QUIET \"\$ScriptName\"
activate.500_ttc_input.sh QUIET \"\$ScriptName\"

#? activate.510_ttc_font__font_type1_16x24.sh QUIET "\$ScriptName"
if [ -e extensions.active/makefile.500_ttc_gfx ]; then # ttc_gfx is available: activate ttc_gui_mt
  # create links into extensions.active/
  createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET
fi

END_OF_ACTIVATE


  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  echo "Installed successfully: $Name"
  
#}
else                            #{ Install failed
  echo "failed to install $Install_Dir"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
