#!/bin/bash
#
#  Install script for FreeRTOS Multitasking Schedulder for Toolchain_STM32
#  Script written by Gregor Rebel 2010-2018.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="300"
EXTENSION_SHORT="freertos"
EXTENSION_PREFIX="scheduler"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #  getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.freertos.org/ RTOS-Cortex-M3-M4.html  RTOS-Cortex-M3-M4.html uC/CortexM3/
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$ScriptName - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then #{

  ERROR=""
  
  if [ ! -e OK.Packages ]; then #{
    installPackage libgmp3-dev 
    installPackage libmpfr-dev
    touch OK.Packages
  fi #}
  #Version="6.1.1"
  #Version="7.0.2"
  #Version="7.1.0"
  #Version="7.1.1"
  #Version="7.2.0"
  #Version="7.3.0"
  #Version="7.5.2"
  Version="8.2.3"
  
  
  Folder="FreeRTOSV${Version}"
  Archive="FreeRTOSV${Version}.zip"
  if [ ! -d $Folder ]; then
    ERROR=""
    unzip -t $Folder.zip || ERROR="1"
    if [ $ERROR == "1" ]; then
      rm 2>/dev/null $Folder.zip
      echo "Downloading FreeRTOS (server sometimes is very slow).."
      #getFile http://dfn.dl.sourceforge.net/project/freertos/FreeRTOS/V${Version}/ $Archive
      getFile http://heanet.dl.sourceforge.net/project/freertos/FreeRTOS/V${Version}/ $Archive
    fi
    ERROR=""
    unzip -t $Archive || ERROR="1"
    if [ "$ERROR" == "" ]; then
      unZIP FreeRTOS $Archive
      mv FreeRTOS $Folder
      createLink $Folder/FreeRTOS current
      #add2CleanUp ../cleanup.sh "rm -Rf $Install_Dir/$Folder"
    else
      echo "ERROR: Zip Archive is corrupted ($Folder.zip)!"
      exit 11
    fi
  fi

  MajorVersion=`perl -e "print substr('$Version', 0, 1);"`
  if [ "$MajorVersion" -lt "8" ]; then #{ FreeRTOS 7.x
    echo "patching sources of FreeRTOS 7.x ..."
  fi #}
  if [ "$MajorVersion" -ge "8" ]; then #{ FreeRTOS 8.x
    echo "patching sources of FreeRTOS 8.x ..."
  fi #}
  
  Comment="removing static from global variable to make it available from outside" #{
    
  if [ "$MajorVersion" -lt "8" ]; then #{ FreeRTOS 7.x
  replaceInFile "current/Source/include/task.h" \
                "unsigned short usStackDepth;" \
                "size_t usStackDepth; // TheToolChain: $Comment"

  replaceInFile "current/Source/tasks.c" \
                "unsigned short usStackDepth;" \
                "size_t usStackDepth; // TheToolChain: $Comment"
  replaceInFile "current/Source/tasks.c" \
                "PRIVILEGED_DATA static volatile unsigned portBASE_TYPE uxTopReadyPriority 		= tskIDLE_PRIORITY;" \
                "PRIVILEGED_DATA  volatile unsigned portBASE_TYPE uxTopReadyPriority 		= tskIDLE_PRIORITY;  // TheToolChain: $Comment"
  fi #}FreeRTOS 7.x
  if [ "$MajorVersion" -ge "8" ]; then #{ FreeRTOS 8.x
  replaceInFile "current/Source/tasks.c" \
                "PRIVILEGED_DATA static volatile UBaseType_t uxTopReadyPriority 		= tskIDLE_PRIORITY;" \
                "PRIVILEGED_DATA  volatile UBaseType_t uxTopReadyPriority 		= tskIDLE_PRIORITY;  // TheToolChain: $Comment"
  fi #}FreeRTOS 8.x                

  #}
  Comment="Variable is read by ttc_task_is_inside_critical_section()" #{
  if [ "$MajorVersion" -lt "8" ]; then #{ FreeRTOS 7.x
  replaceInFile "current/Source/portable/GCC/ARM_CM3/port.c" \
                "static unsigned portBASE_TYPE uxCriticalNesting = 0xaaaaaaaa;" \
                "unsigned portBASE_TYPE uxCriticalNesting = 0xaaaaaaaa; // TheToolChain: $Comment"
  fi #}FreeRTOS 7.x
  if [ "$MajorVersion" -ge "8" ]; then #{ FreeRTOS 8.x
  replaceInFile "current/Source/portable/GCC/ARM_CM3/port.c" \
                "static UBaseType_t uxCriticalNesting = 0xaaaaaaaa;" \
                "UBaseType_t uxCriticalNesting = 0xaaaaaaaa; // TheToolChain: $Comment"
  fi #}FreeRTOS 8.x
  #}
  Comment="variable is read directly on 32-bit architectures" #{
  if [ "$MajorVersion" -lt "8" ]; then #{ FreeRTOS 7.x
  replaceInFile "current/Source/tasks.c" \
                "static volatile portTickType xTickCount" \
                "volatile portTickType xTickCount /* TheToolChain: $Comment */"
  fi #}FreeRTOS 7.x
  if [ "$MajorVersion" -ge "8" ]; then #{ FreeRTOS 8.x
  replaceInFile "current/Source/tasks.c" \
                "static volatile TickType_t xTickCount" \
                "volatile TickType_t xTickCount /* TheToolChain: $Comment */"
  fi #}FreeRTOS 8.x
  
  replaceInFile "current/Demo/Common/drivers/ST/STM32F10xFWLib/inc/stm32f10x_systick.h" \
                '#include "stm32f10x_map.h"' \
                '// TTC ERROR: Do not include this file! #include "stm32f10x_map.h" '
  
  replaceInFile "current/Demo/Common/drivers/ST/STM32F10xFWLib/inc/stm32f10x_rcc.h" \
                '#include "stm32f10x_map.h"' \
                '// TTC ERROR: Do not include this file! #include "stm32f10x_map.h" '
  
  #}
  Comment="Fix for: #queue.c:1274: Warnung:cast discards qualifiers from pointer target type" #{
  if [ "$MajorVersion" -lt "8" ]; then #{ FreeRTOS 7.x
  replaceInFile "current/Source/queue.c" \
                "memcpy( ( void \* ) pvBuffer, ( void \* ) pxQueue->pcReadFrom, ( unsigned ) pxQueue->uxItemSize );" \
                "memcpy( ( void \* ) pvBuffer, ( const void \* ) pxQueue->pcReadFrom, ( unsigned ) pxQueue->uxItemSize ); //TheToolChain: $Comment"
  
  replaceInFile "current/Source/queue.c" \
                "static void prvCopyDataFromQueue( xQUEUE \* const pxQueue, const void \*pvBuffer )" \
                "static void prvCopyDataFromQueue( xQUEUE \* const pxQueue, void \*pvBuffer )"
  fi #}FreeRTOS 7.x

  #}
  Comment="Fix for: list.c:81,89,90: Warnung:cast discards qualifiers from pointer target type" #{
  if [ "$MajorVersion" -lt "8" ]; then #{ FreeRTOS 7.x
  replaceInFile "current/Source/list.c" \
                         "( xListItem \* ) \&( pxList->xListEnd );" \
                "( volatile xListItem \* ) \&( pxList->xListEnd ); /* TheToolChain: $Comment */ "
  fi #}FreeRTOS 7.x
                
  #}
  Comment="Fix for: tasks.c:1071: Warnung:cast discards qualifiers from pointer target type" #{
  if [ "$MajorVersion" -lt "8" ]; then #{ FreeRTOS 7.x
  replaceInFile "current/Source/tasks.c" \
                "( signed char \* ) \"IDLE\"" \
                "( const signed char \* const ) \"IDLE\""
  fi #}FreeRTOS 7.x
  #}
  Comment="additionals/300_scheduler_freertos/Source/list.c:79:18: warning: assignment discards 'volatile' qualifier from pointer target type [enabled by default]" #{
  if [ "$MajorVersion" -lt "8" ]; then #{ FreeRTOS 7.x
  replaceInFile "current/Source/include/list.h" \
                "pxList->pxIndex = ( volatile xListItem \* )" \
                "pxList->pxIndex = ( configLIST_VOLATILE xListItem \* )"

  replaceInFile "current/Source/list.c" \
                "xListEnd.pxNext = ( volatile xListItem \* )" \
                "xListEnd.pxNext = ( configLIST_VOLATILE xListItem \* )"

  replaceInFile "current/Source/list.c" \
                "( volatile xListItem \* )" \
                "( configLIST_VOLATILE xListItem \* )"
  fi #}FreeRTOS 7.x
                
  #}
  Comment="Fix for: tasks.c:1629: warning: cast discards qualifiers from pointer target type" #{
  if [ "$MajorVersion" -lt "8" ]; then #{ FreeRTOS 7.x
  replaceInFile "current/Source/include/StackMacros.h" \
                "if( memcmp( ( void \* ) pxCurrentTCB->pxStack, ( void \* ) ucExpectedStackBytes, sizeof( ucExpectedStackBytes ) ) != 0 )" \
                "if( memcmp( ( void \* ) pxCurrentTCB->pxStack, ( const void \* ) ucExpectedStackBytes, sizeof( ucExpectedStackBytes ) ) != 0 ) /* TheToolChain: $Comment */"
                
  fi #}FreeRTOS 7.x
  #}
  Comment="Fix for: tasks.c:1659: Warnung:cast discards qualifiers from pointer target type" #{
  if [ "$MajorVersion" -lt "8" ]; then #{ FreeRTOS 7.x
  replaceInFile "current/Source/include/list.h" \
                "if( ( pxConstList )->pxIndex == ( xListItem \* ) \&( ( pxConstList )->xListEnd ) )" \
                "if( ( pxConstList )->pxIndex == ( volatile xListItem \* ) \&( ( pxConstList )->xListEnd ) ) /* TheToolChain: $Comment */"  
  fi #}FreeRTOS 7.x
  #}
  Comment="Fix for: tasks.c:1659: Warnung:cast discards qualifiers from pointer target type" #{
  if [ "$MajorVersion" -lt "8" ]; then #{ FreeRTOS 7.x
  replaceInFile "current/Source/tasks.c" \
                "void vTaskPlaceOnEventList( const xList \* const pxEventList, portTickType xTicksToWait )" \
                "void vTaskPlaceOnEventList( xList \* pxEventList, portTickType xTicksToWait ) /* TheToolChain: $Comment */"
  replaceInFile "current/Source/include/task.h" \
                "void vTaskPlaceOnEventList( const xList \* const pxEventList, portTickType xTicksToWait )" \
                "void vTaskPlaceOnEventList( xList \* pxEventList, portTickType xTicksToWait ) /* TheToolChain: $Comment */"
  fi #}FreeRTOS 7.x
  #}
  
  #{ required for freertos_task_get_all()
                
  if [ "$MajorVersion" -lt "8" ]; then #{ FreeRTOS 7.x
  replaceInFile "current/Source/tasks.c" \
                "static void prvListTaskWithinSingleList" \
                "void prvListTaskWithinSingleList"

  replaceInFile "current/Source/tasks.c" \
                "PRIVILEGED_DATA static unsigned portBASE_TYPE uxTopUsedPriority" \
                "PRIVILEGED_DATA unsigned portBASE_TYPE uxTopUsedPriority"

  replaceInFile "current/Source/tasks.c" \
                "PRIVILEGED_DATA static xList pxReadyTasksLists" \
                "PRIVILEGED_DATA xList pxReadyTasksLists"

  replaceInFile "current/Source/tasks.c" \
                "PRIVILEGED_DATA static xList xDelayedTaskList1" \
                "PRIVILEGED_DATA xList xDelayedTaskList1"

  replaceInFile "current/Source/tasks.c" \
                "PRIVILEGED_DATA static xList xDelayedTaskList2" \
                "PRIVILEGED_DATA xList xDelayedTaskList2"

  replaceInFile "current/Source/tasks.c" \
                "PRIVILEGED_DATA static xList \* volatile pxDelayedTaskList" \
                "PRIVILEGED_DATA xList \* volatile pxDelayedTaskList"

  replaceInFile "current/Source/tasks.c" \
                "PRIVILEGED_DATA static xList \* volatile pxOverflowDelayedTaskList" \
                "PRIVILEGED_DATA xList \* volatile pxOverflowDelayedTaskList"

  replaceInFile "current/Source/tasks.c" \
                "PRIVILEGED_DATA static xList xPendingReadyList" \
                "PRIVILEGED_DATA xList xPendingReadyList"

  replaceInFile "current/Source/tasks.c" \
                "PRIVILEGED_DATA static xList xTasksWaitingTermination" \
                "PRIVILEGED_DATA xList xTasksWaitingTermination"

  replaceInFile "current/Source/tasks.c" \
                "PRIVILEGED_DATA static xList xSuspendedTaskList" \
                "PRIVILEGED_DATA xList xSuspendedTaskList"

  replaceInFile "current/Source/tasks.c" \
                "PRIVILEGED_DATA static unsigned portBASE_TYPE uxTopUsedPriority" \
                "PRIVILEGED_DATA unsigned portBASE_TYPE uxTopUsedPriority"
  fi #}FreeRTOS 7.x
  if [ "$MajorVersion" -ge "8" ]; then #{ FreeRTOS 8.x
  replaceInFile "current/Source/tasks.c" \
                "static UBaseType_t prvListTaskWithinSingleList" \
                "UBaseType_t prvListTaskWithinSingleList"
                
  replaceInFile "current/Source/tasks.c" \
                "PRIVILEGED_DATA static List_t" \
                "PRIVILEGED_DATA List_t"

  fi #}FreeRTOS 8.x

  #}              

  cat >replace.sh <<END_OF_REPLACE #{ general optimization replacements
#!/bin/bash

MustBePatched=\`grep -e 'stdio.h' -e 'stdlib.h' -e 'string.h' \$1\`

if [ "\$MustBePatched" != "" ]; then
  source \$HOME/Source/TheToolChain/InstallData/scripts/installFuncs.sh
  
  echo "\$ScriptName patching file \$1 ..."

  replaceInFile "\$1" "#include <stdio.h>"  "// #include <stdio.h> // TTC provides optimized version of this library" QUIET
  replaceInFile "\$1" "#include <stdlib.h>" "// #include <stdlib.h> // TTC provides optimized version of this library" QUIET
  replaceInFile "\$1" "#include <string.h>"  '#define FREERTOS_INCLUDE\n#include "ttc-lib/ttc_memory.h" // #include <string.h>  // TTC provides optimized version of this library' QUIET
fi

END_OF_REPLACE
#}
  
  find ./ -name "*.c" -exec bash ./replace.sh {} \; 

  if [ 1 == 0 ]; then #{ DISABLED PATCHES
  
      
    File="current/Source/include/mpu_wrappers.h"
    if [ ! -e "${File}_orig" ]; then #{
        mv ${File} ${File}_orig
        echo "modifing ${File}.."
        sed -e "s/\#define PRIVILEGED_FUNCTION __attribute__((section(\"privileged_functions\")))/\#define PRIVILEGED_FUNCTION \/\/__attribute__((section(\"privileged_functions\")))/g" ${File}_orig >${File}
    fi #}
  
    File="current/Source/include/task.h"
    if [ ! -e "${File}_orig" ]; then #{ fixing warning: tasks.c:1659: Warnung:cast discards qualifiers from pointer target type
        mv ${File} ${File}_orig
        echo "modifing ${File}.."
        sed -e "s/void vTaskPlaceOnEventList( const xList \* const pxEventList, portTickType xTicksToWait )/void vTaskPlaceOnEventList( xList \* pxEventList, portTickType xTicksToWait )/g" ${File}_orig >${File}
    fi #}
  
    File="current/Source/tasks.c"
    if [ ! -e "${File}_orig" ]; then #{ fixing warning: tasks.c:1625: Warnung:cast discards qualifiers from pointer target type
        mv ${File} ${File}_orig
        echo "modifing ${File}.."
        sed -e "s/PRIVILEGED_DATA static xList pxReadyTasksLists\[ configMAX_PRIORITIES \];/PRIVILEGED_DATA xList pxReadyTasksLists\[ configMAX_PRIORITIES \];/g" ${File}_orig >${File}
    fi #}
  
  fi #}

  Error=""
  if [ ! -e current ]; then
    Error="Missing symbolic link current !"
  fi
  if [ ! -d $Folder ]; then
    Error="Missing folder $Folder !"
  fi
  if [ "$Error" == "" ]; then
    echo "" >OK.Install
  fi
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then #{
  Name="${Install_Dir}"
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "FreeRTOS.h"
#include "task.h"

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() #{

FREE_RTOS=1
TTC_MULTITASKING_SCHEDULER=FREE_RTOS
COMPILE_OPTS += -DTTC_MULTITASKING_SCHEDULER=\$(TTC_MULTITASKING_SCHEDULER)

# activate FreeRTOS parts of source code
COMPILE_OPTS += -DFREE_RTOS -DEXTENSION_$Name

# set some defines required to include sources
COMPILE_OPTS += -DRAM_SIZEK=TTC_MEMORY_REGION_RAM_SIZEK

# required to keep variable even if -gc-sections is passed to linker (variable is used by OpenOCD during debug session)
# -> http://sourceforge.net/p/openocd/mailman/message/33852210/
LDFLAGS += -Wl,--undefined=uxTopUsedPriority

# add include directories
INCLUDE_DIRS += -I additionals/$Name/Source/include/ \\
                -I additionals/$Name/Source/portable/GCC/ARM_CM3/

#                -I additionals/$Name/Demo/Common/drivers/ST/STM32F10xFWLib/inc/

# add source directories
VPATH +=  additionals/$Name/Source/ \\
          additionals/$Name/Source/portable/MemMang/ \\
          additionals/$Name/Source/portable/GCC/ARM_CM3/ \\

# disable warnings 
# (W_CONVERSION=-Wconversion produces lots of warnings from FreeRTOS code)
W_CONVERSION=

# redefine FreeRTOS exception handlers --------------------------------------
# in additionals/$Name/Source/portable/GCC/ARM_CM3/port.c to match with CMSIS vector handlers 
# in std_peripherals_library/CMSIS/CM3/DeviceSupport/ST/STM32F10x/startup/gcc_ride7/startup_stm32f10x_md.s
# (see http://www.mikrocontroller.net/topic/148988)
#X          -DUSART1_IRQHandler=vUARTInterruptHandler
#X          -DADC1_2_IRQHandler=ADC_IRQHandler
ASFLAGS += -DSysTick_Handler=xPortSysTickHandler \\
           -DSVC_Handler=vPortSVCHandler \\
           -DPendSV_Handler=xPortPendSVHandler

# add FreeRTOS object-files
MAIN_OBJS += port.o queue.o list.o tasks.o freertos_task.o
#? syscalls.o 

END_OF_MAKEFILE
#}
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Multitasking scheduler FreeRTOS (http://freertos.org)"  #{
  
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.300_scheduler_*

# ACTIVATE_SECTION_B create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
#addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/${Install_Dir}/ $Install_Dir"

# ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_E call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$ScriptName"

# required for Hardfault-debugging
#X activate.500_ttc_interrupt.sh QUIET "\$ScriptName"

enableFeature 450_systick_freertos

END_OF_ACTIVATE


  # create links into extensions.active/
  echo "createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ${Dir_Extensions} 
  #}

  if [ "1" == "0" ]; then #{ GRE: what's this???
  Name="${Install_Dir}_stm32l1xx"
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "FreeRTOS.h"
#include "task.h"

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() #{

FREE_RTOS=1
TTC_MULTITASKING_SCHEDULER=FREE_RTOS
COMPILE_OPTS += -DTTC_MULTITASKING_SCHEDULER=\$(TTC_MULTITASKING_SCHEDULER)

# activate FreeRTOS parts of source code
COMPILE_OPTS += -DFREE_RTOS -DEXTENSION_$Name

# set some defines required to include sources
COMPILE_OPTS += -DRAM_SIZEK=TTC_MEMORY_REGION_RAM_SIZEK

# add include directories
INCLUDE_DIRS += -I additionals/$Install_Dir/Source/include/ \\
                -I additionals/$Install_Dir/Source/portable/GCC/ARM_CM3/

#                -I additionals/$Install_Dir/Demo/Common/drivers/ST/STM32F10xFWLib/inc/

# add source directories
VPATH +=  additionals/$Install_Dir/Source/ \\
          additionals/$Install_Dir/Source/portable/MemMang/ \\
          additionals/$Install_Dir/Source/portable/GCC/ARM_CM3/ \\

# disable warnings 
# (W_CONVERSION=-Wconversion produces lots of warnings from FreeRTOS code)
W_CONVERSION=

# redefine FreeRTOS exception handlers --------------------------------------
# in additionals/$Install_Dir/Source/portable/GCC/ARM_CM3/port.c to match with CMSIS vector handlers 
# in std_peripherals_library/CMSIS/CM3/DeviceSupport/ST/STM32F10x/startup/gcc_ride7/startup_stm32f10x_md.s
# (see http://www.mikrocontroller.net/topic/148988)
#X          -DUSART1_IRQHandler=vUARTInterruptHandler
#X          -DADC1_2_IRQHandler=ADC_IRQHandler
ASFLAGS += -DSysTick_Handler=xPortSysTickHandler \\
           -DSVC_Handler=vPortSVCHandler \\
           -DPendSV_Handler=xPortPendSVHandler

# add FreeRTOS object-files
MAIN_OBJS += stm32l1_freertos_port.o queue.o list.o tasks.o freertos_task.o
#? syscalls.o 

END_OF_MAKEFILE
#}
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Multitasking scheduler FreeRTOS (http://freertos.org)"  #{
  
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.300_scheduler_*

# ACTIVATE_SECTION_B create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
#addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/${Install_Dir}/ $Install_Dir"

# ACTIVATE_SECTION_C create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_E call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$ScriptName"

# required for Hardfault-debugging
#X activate.500_ttc_interrupt.sh QUIET "\$ScriptName"

END_OF_ACTIVATE


  # create links into extensions.active/
  echo "createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ${Dir_Extensions} 
  #}
fi #}

  Name="${Install_Dir}_heap1"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

EXTENSION_heap=1

COMPILE_OPTS += -DSCHEDULER_HEAP=$Name

# can only allocate    (deterministic, fast, minimal memory usage)
# MAIN_OBJS += heap_1.o
MAIN_OBJS += freertos_heap_1.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "memory allocation scheme 1: can only allocate; smallest memory + processing overhead" #{
  echo 'rm 2>/dev/null ${Dir_ExtensionsActive}/makefile.300_scheduler_*_heap*' >>$ActivateScriptFile

  # create links into extensions.active/
  echo "createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  Name="${Install_Dir}_heap2"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

EXTENSION_heap=2

COMPILE_OPTS += -DSCHEDULER_HEAP=$Name

# allocate/ deallocate (deterministic, high memory usage)
#X MAIN_OBJS += heap_2.o
MAIN_OBJS += freertos_heap_2.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "memory allocation scheme 2: allocate/ deallocate (deterministic, high memory usage)" #{
  echo 'rm 2>/dev/null ${Dir_ExtensionsActive}/makefile.300_scheduler_*_heap*' >>$ActivateScriptFile

  # create links into extensions.active/
  echo "createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  Name="${Install_Dir}_heap3"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

EXTENSION_heap=3

COMPILE_OPTS += -DSCHEDULER_HEAP=$Name

# allocate/ deallocate (full implementation uses malloc()/ free())
MAIN_OBJS += freertos_heap_3.o 

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "memory allocation scheme 3: allocate/ deallocate (uses malloc()/ free() provided by compiler)" #{
  echo 'rm 2>/dev/null ${Dir_ExtensionsActive}/makefile.300_scheduler_*_heap*' >>$ActivateScriptFile

  # create links into extensions.active/
  echo "createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

    Name="${Install_Dir}_heap4"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

EXTENSION_heap=3

COMPILE_OPTS += -DSCHEDULER_HEAP=$Name

# allocate/ deallocate (full implementation uses malloc()/ free())
#X MAIN_OBJS += heap_4.o
MAIN_OBJS += freertos_heap_4.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "memory allocation scheme 4: full implementation combining adjactend memory blocks to reduce fragmentation" #{
  echo 'rm 2>/dev/null ${Dir_ExtensionsActive}/makefile.300_scheduler_*_heap*' >>$ActivateScriptFile

  # create links into extensions.active/
  echo "createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  Name="${Install_Dir}_stack_check_simple"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

COMPILE_OPTS += -DconfigCHECK_FOR_STACK_OVERFLOW=1
COMPILE_OPTS += -DINCLUDE_uxTaskGetStackHighWaterMark

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "enables high watermarks and quick stack size checking (->http://www.freertos.org/Stacks-and-stack-overflow-checking.html)" #{

  # create links into extensions.active/
  echo "createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  Name="${Install_Dir}_stack_check_intense"
  createExtensionMakefileHead ${Name} #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead()

COMPILE_OPTS += -DconfigCHECK_FOR_STACK_OVERFLOW=2
COMPILE_OPTS += -DINCLUDE_uxTaskGetStackHighWaterMark

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "enables high watermarks and stack size checking method 2 (->http://www.freertos.org/Stacks-and-stack-overflow-checking.html)" #{

  # create links into extensions.active/
  echo "createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET" >>$ActivateScriptFile
  
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  
  addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir;  createLink \$Source/TheToolChain/InstallData/$Install_Dir/current  $Install_Dir"
  addLine ../scripts/createLinks.sh "rm 2>/dev/null ${Install_Dir}_network_lwip;  createLink \$Source/TheToolChain/InstallData/$Install_Dir/current/Demo/Common/ethernet/lwip-1.4.0/  ${Install_Dir}_network_lwip"
  echo "$Install_Dir installed successfully"
#}
else
  echo "failed to install $Install_Dir"
  exit 10
fi

#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
