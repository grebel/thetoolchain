#!/bin/bash

#
#  Install script for Microcontroller STM32 F1xx 
#  with ARM Cortex M3 core + Std Peripherals Library for STM32F1xx.
#
#  Script written by Gregor Rebel 2012
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

setInstallDir "190_cpu_cortexm0" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"


#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

if [ ! -e OK.Documentation ]; then #{ (download documentation)

  Get_Fails=""
  
  # download documentation files + link them into documentation folder
  # 
  # getDocumentation(URL_Prefix,File,OutFile,DocSubDir)     will download given URL into given subdirectory of documentation folder
  
  #getDocumentation http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library STM32F10x_Standard_Peripherals_Library.html uC/STM/StdPeripheralLibrary/
  getDocumentation http://www.eecs.umich.edu/~prabal/teaching/eecs373-f11/readings/                            ARMv7-M_ARM.pdf ARMv7-M_Architecture_Reference_Manual.pdf           uC/ARM7
  getDocumentation http://www.st.com/web/en/resource/technical/document/programming_manual/                    DM00051352.pdf  Cortex-M0_programming_manual.pdf                    uC/CortexM0
  getDocumentation http://www.peter-cockerell.net/aalp/resources/pdf/                                          all.pdf         ARM-Assembly_Language_Programming.pdf               uC/CortexM0
  getDocumentation http://www.st.com/internet/com/TECHNICAL_RESOURCES/TECHNICAL_LITERATURE/REFERENCE_MANUAL/   CD00240193.pdf  STM32_RM0038_Advanced_ARM-based_32bit_MCUs.pdf      uC/STM32F0xx
  getDocumentation http://www.st.com/web/en/resource/technical/document/reference_manual/                      DM00031936.pdf  Advanced_developers_guide_for-STM32F05xxx/06xxx.pdf uC/STM32F0xx
  getDocumentation http://www.st.com/st-web-ui/static/active/en/resource/technical/document/application_note/  DM00052530.pdf  Migrating_from_STM32F1_to_STM32F0.pdf               uC/STM32F0xx
  
  if [ "$Get_Fails" == "" ]; then #{ all files downloaded successfully 
    touch OK.Documentation
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}

  if [ ! -e OK.Install ];       then #{
    if [ ! -e OK.Packages ]; then #{
      touch OK.Packages
    fi #}
    if [ -e OK.Packages ]; then #{
      Error=""
      
      Archive="CMSIS-SP-00300-r3p1-00rel0.zip"
      #getFile https://silver.arm.com/download/Development_Tools/Keil/Keil:_generic/CMSIS-SP-00300-r3p1-00rel0/ $Archive $Archive
      getFile http://thetoolchain.com/mirror/ $Archive $Archive
      if [ "$Error" == "" ]; then
        testZIP $Archive
      fi
      if [ "$Error" == "" ]; then
        unZIP "foo" $Archive
      else
        Error="Corrupt archive $Archive!"
        mv -v $Archive ${Archive}_broken
      fi
      if [ ! -d CMSIS/ ]; then
        Error=" Cannot download CMSIS library! (missing $Archive)"
      fi
    
      if [ "$Error" == "" ]; then
        touch OK.Install
      else
        echo "$0 - ERROR: $Error!"
        exit 10
      fi
    fi #}
  fi #}
  if [   -e OK.Install ];       then #{
    Name="${Install_Dir}"
    
    createExtensionMakefileHead ${Name} #{         create makefile for cpu
    File="${Dir_Extensions}makefile.${Name}" 
    cat <<END_OF_MAKEFILE >>$File 

COMPILE_OPTS += -DEXTENSION_${Name}=1

ARM_CPU     =arm6
TARGET      =-mcpu=cortex-m0 -mthumb
TARGET_ARCHITECTURE_CM0=1
COMPILE_OPTS += -DTARGET_ARCHITECTURE_CM0
COMPILE_OPTS += -DTARGET_DATA_WIDTH=32

# some compiled libraries (e.g. ST SimpleMAC for STM32W) use 2 byte wchars
#? COMPILE_OPTS += -fshort-wchar

# architecture specific support code
INCLUDE_DIRS += -Ittc-lib/cm0
vpath %.c ttc-lib/cm0

# Basic Cortex M0 support library
INCLUDE_DIRS += -I additionals/250_STM32L0xx_CMSIS/Include/

# suppress warnings about invalid wchar sizes during linker stage
# (not supported by gcc-arm v4.7) LDFLAGS += --no-wchar-size-warning

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} $0 "CPU STM32L0xx" #{
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{
#? rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.200_cpu_*

# create links into extensions.active/
createLink ${Dir_Extensions}makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

#X activate.500_ttc_basic.sh QUIET \"\$0\"

END_OF_ACTIVATE
#}
    createActivateScriptTail $Name ${Dir_Extensions}
    #}

  if [ "1" == "0" ]; then # { CMSIS (currently not usable due to missing C-sources in downloaded archive)

      Name="${Install_Dir}_cmsis"  
    # (addLine) 
    # create symbolic link from extension folder into every PROJECT_FOLDER/additionals/ to provide extra source code and headers
    #
    # Activate line below and change SUBFOLDER to your needs
    addLine ../scripts/createLinks.sh "rm 2>/dev/null $Install_Dir; createLink \$Source/TheToolChain/InstallData/${Install_Dir}/CMSIS $Name"

    createExtensionSourcefileHead ${Name}    #{ (create initializer code)
    
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

//  #include "YOUR_SOURCE_HERE.h"
//  YOUR_START_HERE();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ (create makefile)
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
INCLUDE_DIRS += -I additionals/$Name/Include/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
#MAIN_OBJS += 

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ${Dir_Extensions} "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ (create activate script)
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \${Dir_Extensions}        -> extensions/
#   \${Dir_ExtensionsLocal}   -> extensions.local/
#   \${Dir_ExtensionsActive}  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.${Install_Dir}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
#createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
#X activate.500_ttc_memory.sh QUIET "\$0"


END_OF_ACTIVATE
  createActivateScriptTail $Name ${Dir_Extensions}
  #}
  fi #}
    
    echo "Installed successfully: $Install_Dir"
  fi #}
  cd ..
  rmdir 2>/dev/null $Install_Dir  # removes only empty install dir
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
