#!/bin/bash

#
#  Install script for a set of High- and Low-Level Drivers
#  for interrupt devices.
#
#  Supported Architectures: stm32w
#
#  Initial Script written by Gregor Rebel 2010-2013.
# 
#  Published under GNU Lesser General Public License
#  Check LEGAL.txt for details.
#
# Tested successfully on:
# openSuSE 12.1 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

StartPath="`pwd`"
RANK="500"
EXTENSION_NAME="ttc_interrupt"
INSTALLPATH="${RANK}_${EXTENSION_NAME}"
dir "$INSTALLPATH"
cd "$INSTALLPATH"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

#{ look for newest versions on download pages
NewestDownloads="../${0}_downloads"
if [ "$1" == "FINDNEWEST" ] || [ ! -e "$NewestDownloads" ]; then #{ find newest downloads
  # echo -n "$0: looking for new GCC... "
  # updateNewest GCC https://launchpad.net/gcc-arm-embedded/+download 'gcc-arm-none-eabi-.+-linux\.tar\.bz2' $NewestDownloads
  if [ "$return" != "" ]; then
    # prepare folder for new version
    rm -Rf *
  fi
fi #}
if [ -e "$NewestDownloads" ]; then
  source $NewestDownloads
fi
# if [ "$GCC_URL" == "" ] || [ "$GCC_File" == "" ]; then #{ check if at least one version has been found 
#   echo "$0 - ERROR: Cannot find newest GCC version!"
#   exit 10
# fi #}
#}look for newest versions

if [ ! -e OK.Docs ];    then    #{ (download documentation)

  Get_Fails=""
  
  # download doc-file + move to Documentation/ + create symbolic link into Documentation/ right here 
  # File="STM32F10x_Standard_Peripherals_Library.html"
  # getFile http://www.mikrocontroller.net/articles/ STM32F10x_Standard_Peripherals_Library $File
  # addDocumentationFile $File STM
  
  if [ "$Get_Fails" == "" ]; then #{ check get() inside scripts/installFuncs.sh 
    touch OK.Docs
  else
    echo "$0 - ERROR: missing files: $Get_Fails"
  fi #}
fi #}
if [ ! -e OK.Install ]; then    #{ (download + install third party software)
  if [ ! -e OK.Packages ]; then #{ install packages
    # installPackageSafe 'which autoreconf' autoreconf
    
    touch OK.Packages
  fi #}
  if [ -e OK.Packages ];   then #{ install software
    echo "installing in $INSTALLPATH ..."
    InstallDir=`pwd`
    
    # getFile http://www.st.com/internet/com/SOFTWARE_RESOURCES/SW_COMPONENT/FIRMWARE/ stm32_i2c_cpal.zip stm32_i2c_cpal.zip
    # unZIP "UNKNOWN" stm32_i2c_cpal.zip
    
    #if [ -e "SOME_FILE" ]; then
      echo "" >OK.Install
    #fi
  else
    echo "$0 - ERROR: Packages were not installed correctly!"
  fi #}OK.Packages
fi #} [ ! -e OK.Install ]; then
if [   -e OK.Install ]; then    #{ create makefiles and activate scripts

  Name="${INSTALLPATH}"  
  Driver_stm32f1="450_interrupt_stm32f1"
  Driver_stm32w1xx="450_interrupt_stm32w"
  Driver_stm32l1xx="450_interrupt_stm32l1"

  createExtensionSourcefileHead ${Name}    #{ (create initializer code)
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

    #include "../ttc_interrupt.h"
    ttc_interrupt_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead ${Name}      #{ (create makefile)
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional object files to be compiled
MAIN_OBJS += ttc_interrupt.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Name} #}
  createActivateScriptHead $Name ../extensions/ "$0 2" "high-level interrupt Driver" #{ (create activate script)
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${INSTALLPATH}_*

DriverFound=""
# call low-level activates until one activates successfully.. 
for DriverName in ${Driver_stm32f1} ${Driver_stm32w1xx} ${Driver_stm32l1xx}; do
  if [ "\$DriverFound" == "" ]; then
    activate.\${DriverName}.sh QUIET "\$0"
    if [ -e \$Dir_ExtensionsActive/makefile.\${DriverName} ]; then
      DriverFound="\${DriverName}"
    fi
  fi
done

if [ "\$DriverFound" != "" ]; then
  # ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
  createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

  # ACTIVATE_SECTION_C activate initialization source-code for this extension 
  createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

  # ACTIVATE_SECTION_D call other activate-scripts
  # activate.500_ttc_memory.sh QUIET "\$0"
else
  echo "\$0 - ERROR: No low-level interrupt Driver found: Skipping activation!"
fi

END_OF_ACTIVATE
  createActivateScriptTail $Name ../extensions/
  #}
  
  createExtensionMakefileHead ${Driver_stm32f1}      #{ (create makefile)
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

  # additional directories to search for header files
INCLUDE_DIRS += -Ittc-lib/stm32f1/

# additional directories to search for C-Sources
vpath %.c ttc-lib/stm32f1/

MAIN_OBJS += stm32_interrupt.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Driver_stm32f1} #}
  createActivateScriptHead $Driver_stm32f1 ../extensions/ "$0 2" "low-level interrupt Driver for stm32f1xx Architecture" #{ (create activate script)
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

ArchitectureSupported=\`ls \$Dir_ExtensionsActive/makefile.*stm32f1* 2>/dev/null\`
if [ "\$ArchitectureSupported" != "" ]; then

  # ACTIVATE_SECTION_A remove activated variants of same type
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${INSTALLPATH}_*
  
  # ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
  createLink \$Dir_Extensions/makefile.$Driver_stm32f1 \$Dir_ExtensionsActive/makefile.$Driver_stm32f1 '' QUIET
  
  # ACTIVATE_SECTION_D call other activate-scripts
  activate.250_stm_std_peripherals__exti.sh QUIET "\$0"
  activate.250_stm_std_peripherals__rcc.sh  QUIET "\$0"
  # activate.500_ttc_memory.sh QUIET "\$0"

else
  echo "$0 - WARNING: No supported architecture found, skipping activation."
  exit 0
fi

END_OF_ACTIVATE
  createActivateScriptTail $Driver_stm32f1 ../extensions/
  #}

  createExtensionMakefileHead ${Driver_stm32w1xx}      #{ (create makefile)
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

  # additional directories to search for header files
INCLUDE_DIRS += -Ittc-lib/stm32w/

# additional directories to search for C-Sources
vpath %.c ttc-lib/stm32w/

MAIN_OBJS += stm32w_interrupt.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Driver_stm32w1xx} #}
  createActivateScriptHead $Driver_stm32w1xx ../extensions/ "$0 2" "low-level interrupt Driver for stm32w Architecture" #{ (create activate script)
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

ArchitectureSupported=\`ls \$Dir_ExtensionsActive/makefile.*stm32w* 2>/dev/null\`
if [ "\$ArchitectureSupported" != "" ]; then
  
  # ACTIVATE_SECTION_A remove activated variants of same type
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${INSTALLPATH}_*
  
  # ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
  if [ "1" == "1" ]; then
    createLink \$Dir_Extensions/makefile.$Driver_stm32w1xx \$Dir_ExtensionsActive/makefile.$Driver_stm32w1xx '' QUIET
  else
    echo "$0 - WARNING: Driver has been disabled until low-level driver gets stable!" 
  fi
else
  echo "$0 - WARNING: No supported architecture found, skipping activation."
  exit 0
fi

# ACTIVATE_SECTION_D call other activate-scripts
# activate.500_ttc_memory.sh QUIET "\$0"

END_OF_ACTIVATE
  createActivateScriptTail $Driver_stm32w1xx ../extensions/
  #}

  
  createExtensionMakefileHead ${Driver_stm32l1xx}      #{ (create makefile)
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

  # additional directories to search for header files
INCLUDE_DIRS += -Ittc-lib/stm32l1/

# additional directories to search for C-Sources
vpath %.c ttc-lib/stm32l1/

MAIN_OBJS += stm32l1_interrupt.o

END_OF_MAKEFILE
  createExtensionMakefileTail ${Driver_stm32l1xx} #}
  createActivateScriptHead $Driver_stm32l1xx ../extensions/ "$0 2" "low-level interrupt Driver for stm32f1xx Architecture" #{ (create activate script)
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/
#   \$Dir_Configs           -> configs/

ArchitectureSupported=\`ls \$Dir_ExtensionsActive/makefile.*stm32l1* 2>/dev/null\`
if [ "\$ArchitectureSupported" != "" ]; then

  # ACTIVATE_SECTION_A remove activated variants of same type
  # rm 2>/dev/null \$Dir_ExtensionsActive/*${INSTALLPATH}_*
  
  # ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
  createLink \$Dir_Extensions/makefile.$Driver_stm32l1xx \$Dir_ExtensionsActive/makefile.$Driver_stm32l1xx '' QUIET
  
  # ACTIVATE_SECTION_D call other activate-scripts
  
  # activate.500_ttc_memory.sh QUIET "\$0"
  activate.250_stm_std_peripherals__gpio.sh QUIET "\$0"
  activate.250_stm_std_peripherals__rcc.sh QUIET "\$0"
  activate.250_stm_std_peripherals__exti.sh QUIET "\$0"
  activate.250_stm_std_peripherals__syscfg.sh QUIET "\$0"

else
  echo "$0 - WARNING: No supported architecture found, skipping activation."
  exit 0
fi

END_OF_ACTIVATE
  createActivateScriptTail $Driver_stm32l1xx ../extensions/
  #}

  if [ "1" == "0" ]; then #{ create regression test for your extension
  
    Name="650_${EXTENSION_NAME}_regression"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "${Name}.h"
${Name}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# Every regression test should set this to enable extra safety checks
COMPILE_OPTS += -DTTC_REGRESSION

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/
#INCLUDE_DIRS += -I

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += regression_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ../extensions/ "$0 2" "PLACE YOUR INFO HERE FOR $0" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${INSTALLPATH}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${INSTALLPATH}.sh QUIET "\$0"


END_OF_ACTIVATE
    createActivateScriptTail $Name ../extensions/
    #}

  fi #}
  if [ "1" == "1" ]; then #{ create usage example for your extension
  
    Name="600_example_${EXTENSION_NAME}"  
    createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
    # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
    # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
    #
    cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "../examples/example_${EXTENSION_NAME}.h"
example_${EXTENSION_NAME}_prepare();

END_OF_SOURCEFILE
    createExtensionSourcefileTail ${Name} #}    
    createExtensionMakefileHead ${Name}      #{ create makefile
    cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() 

# additional constant defines
#COMPILE_OPTS += -D

# additional directories to search for header files
#INCLUDE_DIRS += -I additionals/$Name/

# additional directories to search for C-Sources
#vpath %.c 

# additional object files to be compiled
MAIN_OBJS += example_${EXTENSION_NAME}.o

END_OF_MAKEFILE
    createExtensionMakefileTail ${Name} #}
    createActivateScriptHead $Name ../extensions/ "$0 2" "Usage example for extension ${EXTENSION_NAME}" #{ create activate script
    cat <<END_OF_ACTIVATE >>$ActivateScriptFile
# available variables (-> scripts/installFuncs.sh:createActivateScriptHead())
#
# Paths to certain directories in current project folder
#   \$Dir_Extensions        -> extensions/
#   \$Dir_ExtensionsLocal   -> extensions.local/
#   \$Dir_ExtensionsActive  -> extensions.active/
#   \$Dir_Additionals       -> additionals/

# ACTIVATE_SECTION_A remove activated variants of same type
# rm 2>/dev/null \$Dir_ExtensionsActive/*${INSTALLPATH}_*

# ACTIVATE_SECTION_B create link to your makefile into extensions.active/ to add makefile to your project
createLink \$Dir_Extensions/makefile.$Name \$Dir_ExtensionsActive/makefile.$Name '' QUIET

# ACTIVATE_SECTION_C activate initialization source-code for this extension 
createLink \$Dir_Extensions/$ExtensionSourceFile \$Dir_ExtensionsActive/ '' QUIET

# ACTIVATE_SECTION_D call other activate-scripts
activate.${INSTALLPATH}.sh QUIET "\$0"
activate.500_ttc_mutex.sh  QUIET "\$0"
activate.500_ttc_memory.sh QUIET "\$0"
activate.500_ttc_gpio.sh   QUIET "\$0"
END_OF_ACTIVATE
    createActivateScriptTail $Name ../extensions/
    #}

  fi #}
  
  cd "$OldPWD"
  echo "Installed successfully: $INSTALLPATH"
#}
else                            #{ Install failed
  echo "failed to install $INSTALLPATH"
  exit 10
fi #}

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$StartPath"
applyTargetUserAndGroup "$INSTALLPATH"

exit 0
