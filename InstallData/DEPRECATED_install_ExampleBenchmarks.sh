#!/bin/bash

#
#  Install script for Fixpoint Mathematical Library 
#  URL: http://code.google.com/p/fixpointlib/
#
#  Script written by Gregor Rebel 2010-2017.
# 
#  Feel free do distribute and change to your own needs!
#
# Tested successfully on:
# Ubuntu 10.04 Netbook Edition x86
# KUbuntu 11.04 x86
# openSuSE 11.4 x86
# XUbuntu 11.10 x64

source scripts/installFuncs.sh
checkUserRoot "$1" "$2"

RANK="600"
EXTENSION_SHORT="benchmarks"
EXTENSION_PREFIX="example"
EXTENSION_NAME="${EXTENSION_PREFIX}_$EXTENSION_SHORT"

setInstallDir "${RANK}_${EXTENSION_NAME}" "$1" # sets $Start_Path, $Install_Dir, $Install_Path
findFolderUpwards extensions; Dir_Extensions="$FoundFolder"

#{ IMPLEMENT YOUR INSTALL SCRIPT BELOW -----------------------------------------------

  Name="${Install_Dir}"
  createExtensionSourcefileHead ${Name}    #{ create initializer code for your extension
  
  # Below you may write C-code that will automatically being placed inside extensions.active/ttc_extensions_active.c during _/compile.sh
  # Every #include directive will automatically being added to extensions.active/ttc_extensions_active.h
  #
  cat <<END_OF_SOURCEFILE >>$ExtensionSourceFile

#include "example_benchmarks.h"
  example_benchmarks_prepare();

END_OF_SOURCEFILE
  createExtensionSourcefileTail ${Name} #}    
  createExtensionMakefileHead $Name #{
  cat <<END_OF_MAKEFILE >>$ExtensionMakeFile # variable set by createExtensionMakefileHead() #{
# Adds Libraries/stm32_Registers.* to the project
# Provides global variables with detailed structures for various CortexM3 registers.
# See Libraries/stm32_Registers.* for details.

MAIN_OBJS += DEPRECATED_example_benchmarks.o

END_OF_MAKEFILE
#}
  createExtensionMakefileTail ${Install_Dir} #}
  createActivateScriptHead $Name ${Dir_Extensions} $ScriptName "Example code for a Benchmark Suite - CRC32 + Dijkstra + AEX + Mathematical Suite" #{ 
  cat <<END_OF_ACTIVATE >>$ActivateScriptFile #{

# rm 2>/dev/null \${Dir_ExtensionsActive}/makefile.600_example_*
createLink \${Dir_Extensions}/makefile.$Name \${Dir_ExtensionsActive}/makefile.$Name '' QUIET

activate.500_ttc_gpio.sh      QUIET \"\$ScriptName\"
activate.500_ttc_memory.sh    QUIET \"\$ScriptName\"
activate.500_ttc_interrupt.sh QUIET \"\$ScriptName\"

# ACTIVATE_SECTION_D activate initialization source-code for this extension 
createLink \${Dir_Extensions}/$ExtensionSourceFile \${Dir_ExtensionsActive}/ '' QUIET

END_OF_ACTIVATE
#}
  createActivateScriptTail $Name ${Dir_Extensions}
  #}

  echo "Installed successfully: $Install_Dir"

cd ..
#} IMPLEMENT YOUR INSTALL SCRIPT ABOVE -----------------------------------------------

cd "$Start_Path"
applyTargetUserAndGroup "$Install_Dir"

exit 0
