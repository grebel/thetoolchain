#!/bin/bash
#
# The ToolChain vdeve
#
# Update script for changes introduced in vdeve of The ToolChain.
#
# This script shall help you in porting your project to compile with a newer release of The ToolChain.
# Updatescripts must be run in order starting at first version released after your current project.
# In addtion to run this script, you should compile and check your project for every version update.
#
ttc_manage.pl replace "ttc_MutexHandle" "ttc_Mutex*"
ttc_manage.pl replace "strnlen__8" "ttc_string_length8"
ttc_manage.pl replace "strnlen__16" "ttc_string_length16"
ttc_manage.pl replace "strnlen__32" "ttc_string_length32"
ttc_manage.pl replace "ttc_memory_queuable" "ttc_list_item"
ttc_manage.pl replace "ttc_Mutex" "ttc_mutex_t"
ttc_manage.pl replace "tme_HigherPriorityTaskWoken" "tme_WasLockedByTask"
ttc_manage.pl replace "-DTTC_MEMORY_" "-DTTC_MEMORY_REGION"
ttc_manage.pl replace "-DTTC_MEMORY_REGION" "-DTTC_MEMORY_REGION_"
ttc_manage.pl replace "TTC_MEMORY_RAM_SIZE_K" "TTC_MEMORY_REGION_RAM_SIZE_K"
ttc_manage.pl replace "TTC_MEMORY_REGION_DEBUG_POOL_RELEASE_PIN" "TTC_MEMORY_DEBUG_POOL_RELEASE_PIN"
ttc_manage.pl replace "TTC_MEMORY_REGION_DEBUG_POOL_ALLOC_PIN" "TTC_MEMORY_DEBUG_POOL_ALLOC_PIN"
ttc_manage.pl replace "ttc_memory_CountAlloc" "ttc_memory_Count_Alloc"
ttc_manage.pl replace "ttc_mutex_init_smart" "ttc_mutex_smart_init"
ttc_manage.pl replace "ttc_mutex_create_smart" "ttc_mutex_smart_create"
ttc_manage.pl replace "ttc_mutex_lock_smart" "ttc_mutex_smart_lock"
ttc_manage.pl replace "_ttc_mutex_unlock_smart" "_ttc_mutex_smart_unlock"
ttc_manage.pl replace "ttc_mutex_unlock_isr_smart" "ttc_mutex_smart_unlock_isr"
ttc_manage.pl replace "ttc_mutex_unlock_smart" "ttc_mutex_smart_unlock"
ttc_manage.pl replace "programCalibrateUSleep" "ttc_task_calibrate_usleep"
ttc_manage.pl replace "automatic_heap_calculation" "max_heap"
ttc_manage.pl replace "activate.500_ttc_math.sh" "activate.500_ttc_math_trigonometry.sh"
ttc_manage.pl replace "regression_start_memory_pools" "regression_memory_pools_start"
ttc_manage.pl replace "ttc_SimpleQueueBytes_t" "ttc_queue_bytes_t"
ttc_manage.pl replace "ttc_SimpleQueuePointers_t" "ttc_queue_pointers_t"
ttc_manage.pl replace "ttc_QueueHandle_Pointers_t" "ttc_queue_pointers_t*"
ttc_manage.pl replace "ttc_QueueHandle_Bytes_t" "ttc_queue_bytes_t*"
ttc_manage.pl replace "aaa_ttc_memory_init_heap" "ttc_memory_init_heap"
ttc_manage.pl replace "lis3lv02dl.h" "DEPRECATED_lis3lv02dl.h"
ttc_manage.pl replace "lis3lv02dl.c" "DEPRECATED_lis3lv02dl.c"
ttc_manage.pl replace "lis3lv02dl.o" "DEPRECATED_lis3lv02dl.o"
ttc_manage.pl warn_deleted InstallData/install_078_TTC_RTC.sh
ttc_manage.pl replace "accelerometer_DEPRECATED_lis3lv02dl.config_t" "accelerometer_lis3lv02dl_config_t"
ttc_manage.pl replace "__Unknown" "_Unknown"
ttc_manage.pl replace "OK\.Docs" "OK\.Documentation"
ttc_manage.pl replace "OK\.Documentation" "OK.Documentation"
ttc_manage.pl replace "example_ttc_i2c.c" "example_ttc_i2c_master.c"
ttc_manage.pl replace "functionReceivedPacket" "function_received_packet"
