#!/bin/bash
#
# The ToolChain v1.0.50
#
# Update script for changes introduced in v1.0.50 of The ToolChain.
#
# This script shall help you in porting your project to compile with a newer release of The ToolChain.
# Updatescripts must be run in order starting at first version released after your current project.
# In addtion to run this script, you should compile and check your project for every version update.
#
ttc_manage.pl replace "gfx_font.c" "font_type1_16x24.c"
ttc_manage.pl replace "gfx_font.h" "gfx_font_type1_16x24.h"
ttc_manage.pl replace "gfx_font.o" "gfx_font_type1_16x24.o"
ttc_manage.pl replace "font_type1_16x24.c" "font_type1_16x24.c"
ttc_manage.pl replace "gfx_font_type1_16x24.c" "font_type1_16x24.c"
ttc_manage.pl replace "gfx_font_type1_16x24.h" "font_type1_16x24.h"
ttc_manage.pl replace "ttc_gfx_font_" "ttc_font_"
ttc_manage.pl replace "fill_rect" "rect_fill"
ttc_manage.pl replace "gfx_get_pixel_rgb24" "gfx_image_get_pixel_24bit"
ttc_manage.pl replace "example_lcd_boxes.c" "example_gfx_boxes.c"
ttc_manage.pl replace "example_lcd_boxes.h" "example_gfx_boxes.h"
ttc_manage.pl replace "example_lcd_boxes.o" "example_gfx_boxes.o"
ttc_manage.pl replace "example_lcd_printf.c" "example_gfx_printf.c"
ttc_manage.pl replace "example_lcd_printf.h" "example_gfx_printf.h"
ttc_manage.pl replace "example_lcd_printf.o" "example_gfx_printf.o"
