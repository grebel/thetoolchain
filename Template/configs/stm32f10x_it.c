/*{ stm32f10x_it.c ***********************************************
 *
 * TheToolChain.com
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Interrupt handler definitions
 * Place a call to your interrupt-handling right here. 
 * 
}*/
#include "stm32f10x_it.h"

// Note: all interrupt-handlers have been moved to ttc-lib/stm32/stm32_interrupt.c/.h !
