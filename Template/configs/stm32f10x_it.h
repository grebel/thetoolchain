/*{ stm32f10x_it.h ***********************************************
 *
 * TheToolChain.com
 *
 * Written by Gregor Rebel 2010-2018
 *
 * Interrupt handler definitions
 * Place a call to your interrupt-handling right here. 
 * 
}*/
#ifndef __STM32F10x_IT_H
#define __STM32F10x_IT_H

#ifdef __cplusplus
 extern "C" {
#endif 

#include "stm32f10x.h"
#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)

// Note: all interrupt-handlers have been moved to ttc-lib/stm32/stm32_interrupt.c/.h !

#ifdef __cplusplus
}
#endif

#endif /* __STM32F10x_IT_H */

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
