#!/bin/bash

#{ SYNOPSIS: compile.sh  [FLASH] [NOCOMPILE] [NOLOG] [<MAIN.C>]
# FLASH          transfer compiled binary onto uC
# NOCOMPILE      do not compile
# NOLOG          do not spool compiler output into logfile (directly print to STDOUT)
# DEBUG_SERVER   start gdb-server after successfull flashing
# <MAIN.C>       c-file to use as  main.c (creates symbolic link to main.c)

if [ "$1" == "-h" ]; then
  echo "$0 [FLASH] [NOCOMPILE] [NOLOG] [DEBUG_SERVER] [<MAIN.C>]"
  exit 0;
fi

FLASH=""
NOCOMPILE=""
NOLOG=""
DEBUG_SERVER=""


for Arg in $1 $2 $3 $4; do
  echo "Arg='$Arg'" #D
  if [ "$Arg" == "FLASH" ]; then
    FLASH="FLASH "
  else 
    if [ "$Arg" == "NOCOMPILE" ]; then
      NOCOMPILE="NOCOMPILE "
    else
      if [ "$Arg" == "NOLOG" ]; then
        NOLOG="NOLOG "
      else
        if [ "$Arg" == "DEBUG_SERVER" ]; then
          DEBUG_SERVER="DEBUG_SERVER "
          shift
        else
          MAIN_C="$Arg"
        fi
      fi
    fi
  fi
done

echo  ">$0 $FLASH$NOCOMPILE$NOLOG$DEBUG_SERVER$MAIN_C"
#}SYNOPSIS

if [ "$USER" == "root" ]; then #{
  echo ""
  echo "$0 - ERROR: You should not start a compile process as user root!"
  echo "This would cause temporary files to be owned by root that are not accessible by normal user."
  echo "Compilation aborted."
  echo ""
  exit 10
fi #}

if [ -e ../required_version ]; then
  cd ..
fi
SourceDir=`pwd`          # Where is our source code?
if [ ! -e required_version ]; then #{
  echo "$0 - ERROR: Missing Version-file! Compiling in incorrect folder '$SourceDir'?";
  echo "Create a copy of template project via createNewProject.pl and try again!"
  exit 10
fi #}
RequiredVersion=`cat required_version`

if [ ! -e _ ]; then #{ missing symlinks
  echo "Missing symbolic links: trying to recreate.."
  ./createLinks.sh && ./clean.sh 
fi #}

#{ obtain TheToolChain (if not already done)
cd
if [ ! -d Source ]; then
  mkdir Source
fi
cd Source
if [ -d TheToolChain ]; then
  mv TheToolChain TheToolChain_DELETE
fi
rm -f TheToolChain
if [ -e TheToolChain ]; then
  echo "$0 - ERROR: Cannot move away `pwd`/TheToolChain !"
  exit 11
fi

Folder="TheToolChain_${RequiredVersion}"
if [ ! -d $Folder ]; then #{ download missing toolchain version
  echo "downloading $Folder.."
  wget -c http://thetoolchain.com/archive/${Folder}.tar.bz
  tar xjf ${Folder}.tar.bz
  if [ -d TheToolChain ]; then
    mv TheToolChain ${Folder}
  fi
fi
#}
ln -sv ${Folder} TheToolChain

cd TheToolChain/InstallData/
if [ ! -e OK.AllInstalls ]; then
  echo "Installation incomplete: Starting toolchain installation.."
  ./installAll.sh BASIC
  if [ ! -e OK.AllInstalls ]; then
    echo "Toolchain was installed successfully."
    $0
    exit 0
  else
    echo "ERROR: Toolchain installation failed!"
    exit 10
  fi
fi
#}

cd "$SourceDir"                    # cd back to this source code directory
if [ -e main.bin ]; then
  if [ activate_project.sh -nt main.bin ]; then
    echo "project settings changed: cleaning first.."
    ./clean.sh                         # only clean if activate_project.sh is newer than main.bin!
  fi
fi
./createLinks.sh      || exit 11   # recreate symbolic links to TheToolChain
source _/SourceMe.sh               # update path variables to binary tools
./activate_project.sh || exit 12   # activate all extensions for current project
./createLinks.sh      || exit 13   # recreate symbolic links to TheToolChain (setup may have changed during activate_project.sh)

if [ "$NOCOMPILE" == "" ]; then
  _/compile.sh $MAIN_C $NOLOG      # compile source
fi

#{ flash onto uC
if [ -e main.bin ]; then
  echo "Compiled successfully."
  if [ "$FLASH" != "" ]; then
    echo "Trying to flash binary onto microcontroller.."
    _/flash.sh main.bin 
  fi
  if [ "$DEBUG_SERVER" != "" ]; then
    echo "Starting gdb-server.."
    _/gdb_server.sh
  fi
  
fi
#}
