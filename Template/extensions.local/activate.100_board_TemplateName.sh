#!/bin/bash
#
#                    The ToolChain
#
#           written 2010-2018 by Gregor Rebel
#
# This script activates an individual board found in the extensions.local/ folder of current project folder.
#
# Usage: Run this script from your project folder where your main.c is like this:
#   cd MY_PROJECT
#   extensions.local/activate.100_board_Unnamed.sh
#

BoardName="TemplateName"  # change to match name of makefile to activate
Board_Makefile="`pwd`/extensions.local/makefile.100_board_$BoardName"
Board_Sourcefile="`pwd`/extensions.local/source.100_board_$BoardName"
source _/installFuncs.sh

if [ ! -e "$Board_Makefile" ]; then
  echo "$0 - ERROR: Cannot find board makefile '$Board_Makefile'!"
  exit 10
fi

#{ SCRIPT_HEADER ---------------------------------------------------------------

DirName=`dirname $0`
if [ "$DirName" == ""      ]; then #{
  DirName=`pwd`
fi #}
if [       "$1" == "QUIET" ]; then #{
  Quiet="1"
  shift
fi #}
if [       "$1" == "INFO"  ]; then #{
  cat <<'END_OF_INFO'
Standard Peripheral Library for CPU STM32W1xx - architectures: 
END_OF_INFO
  exit 0
fi #}
if [       "$1" == "TEST"  ]; then #{
  TEST="1"
  shift
else
  TEST=""
fi #}   
if [       "$1" == "HELP" ]; then #{
  cat <<END_OF_HELP
$0 [INFO|TEST|HELP|PROVIDES] [QUIET] [ONLY <ARCHITECTURE>] [<CALLER>]

INFO            print out short single single info text about this activate and exit
TEST            sets variable TEST=1 (can be used by user part of activate script)
HELP            this text
PROVIDES        print out list of supported low-level drivers (looks for corresponding low_level/install_*.sh files)
QUIET           suppress text output
ONLY            run only single low-level install script corresponding to <ARCHITECTURE> instead of all
<ARCHITECTURE>  one from output of PROVIDES

$0 has been created by ./install_032_CPU_STM32L1xx.sh on Mi 21. Jan 11:19:47 CET 2015

END_OF_HELP
  exit 0
fi #}


# ensure that $PATH contains no . (brings find into trouble)
export PATH=`perl -e "my @A=split(':', '$PATH');  print join(':', grep { substr(\\\$_,0,1) eq '/' } @A );"`

function createLink() {      # Source,Link,User,QUIET  creates symbolic link (removes old one before)
  cl_Source="$1"
  cl_Link="$2"
  cl_User="$3"
  cl_Quiet="$4"
  
  if [ "$cl_Quiet" = "" ]; then
    Verbose="-v"
  else
    Verbose=""
  fi
  if [ ! -e "$cl_Source" ]; then
    MissingFiles="$MissingFiles $cl_Source"
  fi
  
#X   if [ "$cl_User" != "" ]; then
#X     sudo -u$cl_User rm -f $Verbose $cl_Link
#X   else
#X     rm -f $cl_Link
#X   fi
   
  if [ "$cl_User" != "" ]; then
    sudo -u$cl_User ln $Verbose -sf "$cl_Source" "$cl_Link"
  else
    ln $Verbose -sfv "$cl_Source" "$cl_Link"
  fi
}
function getScriptPath() {   #                         ScriptPath = absolute path of directory in which script is located
  MyPWD=`pwd`
  FirstChar=`perl -e "print substr('$0', 0, 1);"`
  if [ "$FirstChar" == "/" ]; then
    MySelf="$0"
  else
    MySelf="$MyPWD/$0"
  fi
  ScriptPath=`perl -e "print substr('$MySelf', 0, rindex('$MySelf', '/'));"`
  cd "$ScriptPath"
  ScriptPath=`pwd`
  cd "$MyPWD"
  #X echo "MySelf= $MySelf"
  #X echo "ScriptPath= $ScriptPath"
}
function addLine() {         # File,Line               adds given line to File if not already done
  File2Add2="$1"
  Line="$2"
  
  if [ ! -e $File2Add2 ]; then
    touch $File2Add2
  fi
  AlreadyPresent=`grep "$Line" $File2Add2`
  if [ "$AlreadyPresent" == "" ]; then
    echo "$Line" >>$File2Add2
#  else
#    echo "file already up2date: $File2Add2"
  fi
}

CurrentPath=`pwd`
getScriptPath
Dir_Extensions=$CurrentPath/extensions        # folder where this script is located
Dir_ExtensionsLocal=${Dir_Extensions}.local   # path to extensions.local/ (extension configuration and makefile for local project)
Dir_Configs=$CurrentPath/configs              # path to configs/          (static configuration files for local project) 
Dir_Additionals=$CurrentPath/additionals      # path to additionals/      (additional libraries being required for local project)
Dir_Bin=$CurrentPath/bin                      # path to bin/              (binaries and symlinks to binaries being used in local project)
cd "$CurrentPath"

if [ -d extensions.active ]; then #{
  Dir_ExtensionsActive="extensions.active"
  Dir_Additionals="additionals"
#}
else #{
  if [ -d ../extensions.active ]; then
    Dir_ExtensionsActive="../extensions.active"
    Dir_Additionals="../additionals"
  else
    Dir_ExtensionsActive=${ScriptPath}.active
    Dir_Additionals=""
  fi
fi #}


if [ "$1" == "PROVIDES" ]; then #{
  _/extractProvidedMainObjects.pl ${Dir_Extensions}/makefile.200_cpu_stm32l1xx
  exit 0
fi #}

if [ -e "${Dir_ExtensionsActive}/makefile.200_cpu_stm32l1xx" ]; then
  # echo "$0 - already activated"
  exit 0
fi

if [ "$1" == "ONLY" ]; then # 
  Architecture=$2
  if [ "$Architecture" != "" ]; then # user wants to run only single low level activate script
    File_LowLevelInstall="`ls extensions/activate.450_*${Architecture}.sh`"
    echo "$0 - Will activate single architecture '$Architecture'"
  fi
  shift
  shift
fi

if [ "$1" != "" ]; then
  Caller="$1"
  shift
fi

if [ "$Quiet" == "" ]; then
  echo ""
fi


#}SCRIPT_HEADER
#{ USER_PART -------------------------------------------------------------------

  # deactivate all other boards (only one board may be active)
  rm 2>/dev/null ${Dir_ExtensionsActive}/makefile.100_board_*
  
  # activate makefile of local board

  # create links into extensions.active/
  createLink $Board_Makefile ${Dir_ExtensionsActive}/makefile.100_board_$BoardName '' QUIET

  # activate source file for local board if it exists
  if [ -e "$Board_Sourcefile" ]; then
    createLink $Board_Sourcefile ${Dir_ExtensionsActive}/source.100_board_$BoardName '' QUIET
  fi
  
  #{ provide features that allow to activate certain extensions  (at least provide one 450_cpu feature!)
  #
#InsertFeatures above
#}provide features

#}USER_PART
#{ SCRIPT_FOOTER ---------------------------------------------------------------

# back to project folder
cd "$CurrentPath"
if [ -e "${Dir_ExtensionsActive}/makefile.200_cpu_stm32l1xx" ]; then
  echo "Extension activated: 200_cpu_stm32l1xx (called from $Caller)"
  if [ "$Quiet" == "" ]; then
    echo ""
    echo "${CurrentPath}/extensions.active/"
    find "${Dir_ExtensionsActive}/" -name "makefile.*" -execdir echo '  {}' \; | sort
  fi
fi

#}SCRIPT_FOOTER
