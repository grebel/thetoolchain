#ifndef EXAMPLE_RTLS_CRTOF_SIMPLE_2D_TYPES_H
#define EXAMPLE_RTLS_CRTOF_SIMPLE_2D_TYPES_H

/** example_ttc_RTLS_CRTOF_SIMPLE_2D.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_rtls_crtof_simple_2d.
 *  The basic flow is as follows:
 *  (1) example_ttc_rtls_crtof_simple_2d_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_rtls_crtof_simple_2d_prepare() gets executed
 *
 *  Created from template example_ttc_device_architecture.h revision 15 at 20161208 10:46:38 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_RTLS_CRTOF_SIMPLE_2D.c only do not belong here!
#include "../ttc-lib/ttc_basic.h"              // basic datatypes and definitions
#include "../ttc-lib/ttc_systick_types.h"      // precise delays
#include "../ttc-lib/ttc_rtls_types.h"    // rtls datatypes and definitions
#include "example_ttc_radio_ranging_common.h"  // using datatypes from ranging example
//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_rtls_crtof_simple_2d_t
    t_ttc_rtls_config* Config_RTLS;   // !=NULL: pointer to configuration of rtls device to use (==NULL: do not use ttc_rtls)
    t_ttc_systick_delay     Delay;              // this type of delay is usable in single- and multitasking setup

    etr_task_info_t         Common;             // configuration required to use example_ttc_radio_ranging_common
    t_u8                    IndexSLAM;          // logical index of ttc_slam device to use
} t_example_ttc_rtls_crtof_simple_2d_data;

//}Structures/ Enums

#endif //EXAMPLE_RTLS_CRTOF_SIMPLE_2D_TYPES_H
