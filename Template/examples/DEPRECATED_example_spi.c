/*{ TemplateName::.c ************************************************
 
 Empty template for new c-files.
 Copy and adapt to your needs.
 
}*/

#include "example_spi.h"

//{ Global variables *************************************************

ttc_spi_generic_t*       SPI_Generic;  // architecture independent SPI configuration
#define SPI_INDEX 1                   // using first SPI available on current board

//}Global Variables
//{ Function definitions *************************************************

void example_spi_init() {
#ifdef TTC_LED1
    ttc_gpio_init(TTC_LED1, tgm_output_push_pull, tgs_Max);
    ttc_gpio_set(TTC_LED1);
#endif

    u8_t MaxSPI = ttc_spi_get_max_index();
    Assert(MaxSPI > 0, ec_UNKNOWN);

    ttc_spi_errorcode_e Error;

    SPI_Generic = ttc_spi_get_configuration(SPI_INDEX);
    Assert(SPI_Generic, ec_NULL);

#ifdef EXTENSION_600_example_spi_master
    SPI_Generic->Flags.Bits.Master = 1;
#else
    SPI_Generic->Flags.Bits.Master = 0;
#endif
    SPI_Generic->Flags.Bits.CRC8 = 0;
    SPI_Generic->Flags.Bits.HardwareNSS = 0;

    //SPI_Generic->Flags.Bits.
    Error = ttc_spi_init(SPI_INDEX);
    Assert(Error == tse_OK, ec_UNKNOWN);
}
void example_spi_start() {
    example_spi_init();

#ifdef EXTENSION_600_example_spi_master
    ttc_task_create(task_SPI_sender, // function to start as thread
                    "SPItx",         // thread name (just for debugging)
                    512,             // stack size
                    NULL,            // passed as argument to taskLEDs()
                    1,               // task priority (higher values mean more process time)
                    NULL             // can return a handle to created task
                    );
#else
    ttc_task_create(task_SPI_listener, // function to start as thread
                    "SPIrx",           // thread name (just for debugging)
                    512,               // stack size
                    NULL,              // passed as argument to taskLEDs()
                    1,                 // task priority (higher values mean more process time)
                    NULL               // can return a handle to created task
                    );
#endif
}
void task_SPI_sender(void *TaskArgument) {
    TaskArgument = TaskArgument; // avoids warning: unused parameter 'TaskArgument'

#ifdef TTC_LED1
    ttc_gpio_clr(TTC_LED1);
#endif
    const char* Hello = "Hello world!\n\r";
    char Buffer[100];
    (void) Buffer;

    while (1) {
#ifdef TTC_LED1
        ttc_gpio_set(TTC_LED1);
#endif
        ttc_spi_send_string(SPI_INDEX, Hello, -1);
#ifdef TTC_LED1
        ttc_gpio_clr(TTC_LED1);
#endif
        ttc_task_msleep(300);
    }
}
void task_SPI_listener(void *TaskArgument) {
    TaskArgument = TaskArgument; // avoids warning: unused parameter 'TaskArgument'

#define Size_ReceiveBuffer 20
    u8_t ReceiveBuffer[Size_ReceiveBuffer];
    ReceiveBuffer[0] = 0;
    u8_t* Writer = ReceiveBuffer;
    u8_t* WriterEnd = Writer + Size_ReceiveBuffer - 1;

#ifdef TTC_LED1
    ttc_gpio_clr(TTC_LED1);
#endif

    while (1) {
        ttc_spi_errorcode_e Status = ttc_spi_read_byte(SPI_INDEX, Writer, 30000);

        while (Status == tse_OK) { // store all incoming bytes
#ifdef TTC_LED1
          ttc_gpio_set(TTC_LED1);
#endif
          char Byte = *Writer;
          Writer++;

          if ( (Byte == 13) ||       // carriage return found
               (Writer == WriterEnd) // buffer full
               )
          {
              *Writer = 0;            // terminate string
              if (0) processData(ReceiveBuffer, Size_ReceiveBuffer);
              Writer = ReceiveBuffer; // back to start of buffer
          }

          // check for next byte
          Status = ttc_spi_read_byte(SPI_INDEX, Writer, 30);
#ifdef TTC_LED1
          ttc_gpio_clr(TTC_LED1);
#endif
        }
        uSleep(1);
    }
}
void processData(u8_t* Data, u8_t MaxLength) {
    ttc_spi_send_string(SPI_INDEX, "You said '\0", -1);
    ttc_spi_send_string(SPI_INDEX, (char*) Data, MaxLength);
    ttc_spi_send_string(SPI_INDEX, "'\n\0", -1);
}

//}FunctionDefinitions
