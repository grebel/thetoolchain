/** example_ttc_gfx.c **********************************************{
 *
 *                             The ToolChain
 *
 *
 * example_ttc_gfx_start()
 *     fills et_gfx_TaskArguments with data
 *     spawns thread task_GFX()
 *     returns immediately
 *
 * task_GFX()
 *     while (1) {
 *       do something
 *       ttc_task_msleep(100);
 *     }
 *
}*/

#include "example_ttc_gfx.h"

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
ttc_gfx_config_t* et_gfx_DisplayConfig = NULL;

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_gfx_start() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */
   static example_gfx_t et_gfx_TaskArguments;
   et_gfx_TaskArguments.Foo = 1; // set some arguments to be passed to task
   
   et_gfx_DisplayConfig = ttc_gfx_get_configuration(1);
   Assert(et_gfx_DisplayConfig, ec_DeviceNotFound);

   // set color to use for first screen clear
   ttc_gfx_set_color_bg24(TTC_GFX_COLOR24_BLUE);
   ttc_gfx_set_color_fg24(TTC_GFX_COLOR24_BLACK);

   ttc_gfx_init(1);

   A_define(char, String, 10);

   ttc_gfx_text_at(0,0, "example_ttc_gfx", -1);
   ttc_gfx_text_at(0,2, "Seconds: ", -1);

    
   ttc_task_create( task_GFX,               // function to start as thread
                    "tGFX",                 // thread name (just for debugging)
                    128,                         // stack size
                    &et_gfx_TaskArguments,  // passed as argument to task_GFX()
                    1,                           // task priority (higher values mean more process time)
                    NULL                         // can return a handle to created task
                  );
}
void task_GFX(void *TaskArgument) {
    Assert(TaskArgument, ec_NULL);                  // always check pointer arguments with Assert()!
    example_gfx_t* TaskArguments = (example_gfx_t*) TaskArgument; // we're expecting this pointer type
    (void) TaskArguments; // not used yet

    Base_t Seconds = 0;

    while (TTC_TASK_SCHEDULER_AVAILABLE) {
        ttc_string_snprintf( (u8_t*) String.Data, String.Size, "%3i", Seconds++);
        ttc_gfx_text_solid_at(9,2, String.Data, String.Size);

      ttc_task_yield();    // always sleep for a while to give cpu time to other tasks (does nothing if multitasking is disabled)
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
