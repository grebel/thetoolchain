#ifndef example_ttc_rtls_crtof_simple_2d_anchor_h
#define example_ttc_rtls_crtof_simple_2d_anchor_h

/** { example_ttc_rtls_crtof_simple_2d_anchor.h *********************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.h revision 11 at 20161130 06:10:02 UTC
 *
 *  Authors: gregor
}*/
/** { Includes *********************************************************
 *
 * Put all includes here that are required to include this header file.
 * Do NOT add includes required to compile example_ttc_rtls_crtof_simple_2d_anchor.c here!
 */

#include "../ttc-lib/ttc_basic.h"                         // basic datatypes
#include "../ttc-lib/ttc_memory.h"                        // memory checks and safe pointers
#include "example_ttc_radio_ranging_common.h"             // functions and structures being common for all node types
#include "example_ttc_rtls_crtof_simple_2d_types.h"  // datatypes used in this example only

// #include "../ttc-lib/ttc_heap.h"   // dynamic memory and safe arrays
// #include "../ttc-lib/ttc_string.h" // string compare and copy
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Constant Defines / Macros ****************************************
 *
 * As a convention, constant definitions are all UPPERCASED to distinguish
 * them from variables and functions.
 *
 * Examples:

 #define ABC 1
 #define calculate(A,B) (A+B)
 */

//}ConstantDefines
/** { Structure Declarations *******************************************
 *
 * This can be typedef struct or typedef enum declarations.
 * Always prefix structures and enums with filename to avoid double declarations!
 *
 * Examples:

   typedef struct s_example_ttc_rtls_crtof_simple_2d_anchor_list {
      struct s_example_ttc_rtls_crtof_simple_2d_anchor_list* Next;
      t_base Value;
   } example_ttc_rtls_crtof_simple_2d_anchor_list_t;

   typedef enum {
     example_ttc_rtls_crtof_simple_2d_anchor_None,
     example_ttc_rtls_crtof_simple_2d_anchor_Type1
   } example_ttc_rtls_crtof_simple_2d_anchor_types_e;

 */

//}StructureDeclarations
/** { Function Declarations ********************************************
 *
 * Declare all functions here that may be called from outside.
 * Rules of Good Code:
 * - document every function declaration
 * - prefix public function names with their filename to avoid double definitions
 */
/** do some extra initialization for this anchor node
 *
 * @param TaskInfo  pointer to same structure being handed to task function later
 */
void example_ttc_rtls_crtof_simple_2d_prepare_anchor( t_example_ttc_rtls_crtof_simple_2d_data* TaskInfo );

/** Task implementing an anchor node with a ranging enabled radio transceiver
 *
 * @param TaskArgument (void*)  pointer to a t_example_ttc_rtls_crtof_simple_2d_data instance
 */
void example_ttc_rtls_crtof_simple_2d_task_anchor( void* TaskArgument );

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //example_ttc_rtls_crtof_simple_2d_anchor_h
