/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#include "example_ttc_i2c_slave_receiver.h"
#include "../ttc-lib/ttc_i2c.h"



example_i2c_slave_t et_i2c_TaskArguments;
ttc_i2c_errorcode_e Error;
ttc_i2c.config_t * I2C_Generic;
s16_t Acceleration[3];

#ifdef EXTENSION_500_ttc_gfx
ttc_gfx_config_t* DisplayConfig = NULL;
#endif

#define I2C_INDEX 2

void printfAt(u16_t X, u16_t Y, const char* String, s32_t Value) {

#ifdef EXTENSION_500_ttc_gfx
     char Buffer[20];
    ttc_string_snprintf( (u8_t*) Buffer, 20, String, Value);
    ttc_gfx_print_solid_at(X, Y, Buffer, 20);
#endif

}

void example_ttc_i2c_slave_receiver_start() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */
   et_i2c_TaskArguments.Foo = 1; // set some arguments to be passed to task

#ifdef EXTENSION_500_ttc_gfx
    //u8_t MaxDisplay = ttc_gfx_get_max_display_index();
  //  Assert(MaxDisplay > 0, ec_DeviceNotFound); // no displays defined by board makefile!
    DisplayConfig = ttc_gfx_get_configuration(1);
    Assert(DisplayConfig, ec_DeviceNotFound);

    // set color to use for first screen clear
    ttc_gfx_set_color_bg24(TTC_GFX_COLOR24_AQUA);
    ttc_gfx_init(1);

#endif

    Error = tie_OK;

    I2C_Generic = ttc_i2c_get_configuration(I2C_INDEX);

    I2C_Generic->OwnAddress = 0x30;

    /* I2C Configurated as Slave */
    I2C_Generic->Flags.Bits.Slave = 1;


#ifdef EXTENSION_100_board_stm32l100C_discovery

    /* Number of bytes to use in DMA */
    I2C_Generic->LowLevelConfig->NumBytes=6;

#endif

    Error = ttc_i2c_init(I2C_INDEX);
    Assert(Error==tie_OK, ec_UNKNOWN);


#ifdef TTC_LED1
   ttc_gpio_init(TTC_LED1, tgm_output_push_pull, tgs_Min);
#endif


   ttc_task_create( task_I2C_Slave,        // function to start as thread
                    "tI2C_Slave",          // thread name (just for debugging)
                    128,                    // stack size
                    &et_i2c_TaskArguments,  // passed as argument to task_SPI()
                    1,                      // task priority (higher values mean more process time)
                    NULL                    // can return a handle to created task
                  );

}



void task_I2C_Slave(void *TaskArgument){

    Assert(TaskArgument, ec_NULL);                  // always check pointer arguments with Assert()!
    example_i2c_slave_t* TaskArguments = (example_i2c_slave_t*) TaskArgument; // we're expecting this pointer type
    (void) TaskArguments;

    u8_t receive_data_h;
    u8_t receive_data_l;

    u8_t i;


#ifdef EXTENSION_500_ttc_gfx

    ttc_gfx_print_solid_at(0, 0, "Receiving ...", -1);
    ttc_gfx_print_solid_at( 0, 2, "X:       mG", -1);
    ttc_gfx_print_solid_at( 0, 3, "Y:       mG", -1);
    ttc_gfx_print_solid_at( 0, 4, "Z:       mG", -1);

#endif

    while(1){

        #ifdef TTC_LED1
                ttc_gpio_clr(TTC_LED1);
        #endif

#ifdef EXTENSION_100_board_stm32l100C_discovery

            /* Read acceleration from the master board */
            for(i=0;i<3;i++){
                receive_data_h = _i2c_stm32l1xx_read_byte(I2C_Generic);
                receive_data_l = _i2c_stm32l1xx_read_byte(I2C_Generic);
                Acceleration[i]= ((u16_t)((u16_t)receive_data_h <<8) + receive_data_l);
            }
#endif

        #  ifdef EXTENSION_500_ttc_gfx
            printfAt(3, 2, "%6i mg  ", Acceleration[0]);
            printfAt(3, 3, "%6i mg  ", Acceleration[1]);
            printfAt(3, 4, "%6i mg  ", Acceleration[2]);
        #  endif

        #ifdef TTC_LED1
                ttc_gpio_clr(TTC_LED1);
        #endif
    }

}
