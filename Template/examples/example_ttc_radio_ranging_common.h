#ifndef example_ttc_radio_ranging_common_h
#define example_ttc_radio_ranging_common_h

/** { example_ttc_radio_ranging_common.h *********************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.h revision 11 at 20161201 12:44:19 UTC
 *
 *  Authors: gregor
}*/
/** { Includes *********************************************************
 *
 * Put all includes here that are required to include this header file.
 * Do NOT add includes required to compile example_ttc_radio_ranging_common.c here!
 */

#include "../compile_options.h"
#include "../ttc-lib/ttc_basic_types.h"  // basic datatypes
#include "../ttc-lib/ttc_memory_types.h" // memory checks and safe pointers
#include "../ttc-lib/ttc_packet.h"       // IEEE 802.15.4-2011 type packet format
#include "../ttc-lib/ttc_radio_types.h"  // Architecture independent radio transceiver
#include "../ttc-lib/ttc_usart_types.h"  // debug output via serial port
#include "../ttc-lib/ttc_math_types.h"   // 2D and 3D vectors

// #include "../ttc-lib/ttc_string.h" // string compare and copy
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Constant Defines / Macros ****************************************
 *
 * As a convention, constant definitions are all UPPERCASED to distinguish
 * them from variables and functions.
 *
 * Examples:

 #define ABC 1
 #define calculate(A,B) (A+B)
 */

/** Unique 16-bit Identifiere of our Personal Area Network
 *
 * Only nodes with same PanID can communicate with each other.
 * Inter PAN communication is
 */
#ifndef EXAMPLE_TTC_RADIO_LOCAL_PAN_ID
    #  warning Missing definition for EXAMPLE_TTC_RADIO_LOCAL_PAN_ID. Using default value.
    #define EXAMPLE_TTC_RADIO_LOCAL_PAN_ID 0xb9a  // bga = bio gas anlage
#endif

/** Define node index for which to compile source code
 * 0..99:   this node is a sensor node
 * 100.199: this node is an anchor node
 *
 * The 16-bit logical and 64-bit hardware address is defined below or via commandline defines.
 */
#ifndef EXAMPLE_TTC_RADIO_NODE_INDEX
    #  warning Missing definition for EXAMPLE_TTC_RADIO_NODE_INDEX. Define in your makefile as 0=Observer, 1..99=Anchor, 100..199=Mobile node!
    #define EXAMPLE_TTC_RADIO_NODE_INDEX  0  // 0: observer (receives only); 1..99: sensor node; 100..199: anchor node
#endif

#if EXAMPLE_TTC_RADIO_NODE_INDEX < 100
    #if EXAMPLE_TTC_RADIO_NODE_INDEX < 0
        #    error Invalid EXAMPLE_TTC_RADIO_NODE_INDEX (must be from 0..255)
    #endif
    #if EXAMPLE_TTC_RADIO_NODE_INDEX == 0
        #define EXAMPLE_TTC_RADIO_NODE_IS_OBSERVER   // 0: observer node will just listen on radio frequency and debug received packets to TTC_UART1
    #else
        #define EXAMPLE_TTC_RADIO_NODE_IS_ANCHOR       // 1..99: anchor nodes build up coordinate system and provide localization service
    #endif
#else
    #undef EXAMPLE_TTC_RADIO_NODE_IS_ANCHOR
    #define EXAMPLE_TTC_RADIO_NODE_IS_MOBILE     // 100..: mobile node will use localization service from anchor nodes
#endif

// amount of registered anchor nodes
#ifndef EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS
    #  warning Missing definition for EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS. Using default value.
    #define EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS 2
#endif
#ifdef EXAMPLE_TTC_RADIO_NODE_IS_ANCHOR

    // calculate array index (arrays start at 0)
    #define ANCHOR_INDEX EXAMPLE_TTC_RADIO_NODE_INDEX-1

    #if ANCHOR_INDEX >= EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS
        #    error Invalid ANCHOR_INDEX definition! (must be within 0..EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS-1)
    #endif
    #if EXAMPLE_TTC_RADIO_NODE_INDEX > EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS
        #    error Invalid EXAMPLE_TTC_RADIO_NODE_INDEX (0 = Observer, 1..99 = Anchor , 100..255 = Mobile Sensor)
    #endif

#endif


//}ConstantDefines
/** { Structure Declarations *******************************************
 *
 * This can be typedef struct or typedef enum declarations.
 * Always prefix structures and enums with filename to avoid double declarations!
 *
 * Examples:

   typedef struct s_example_ttc_radio_ranging_common_list {
      struct s_example_ttc_radio_ranging_common_list* Next;
      t_base Value;
   } example_ttc_radio_ranging_common_list_t;

   typedef enum {
     example_ttc_radio_ranging_common_None,
     example_ttc_radio_ranging_common_Type1
   } example_ttc_radio_ranging_common_types_e;

 */

#define SIZE_ETR_STRINGBUFFER 400

typedef struct { // etr_task_info_t - local variables of node task (reference to an instance is passed to ttc_task_create()
    //? t_ttc_packet_address*       AnchorIDs;          // pointer to first entry of Array of anchor addresses
    t_ttc_packet_address*       LocalNodeID;        // address of local node (may point into AnchorIDs)
    t_u8*                       PrepareBuffer;      // Buffer for assembling temporary strings. This buffer is bigger than maximum payload size (MaxPacketSize also counts MAC-header)
    t_u16                       PrepareBuffer_Size; // size of PrepareBuffer[]
    t_ttc_radio_config*         ConfigRadio;        // pointer to configuration of pre-initialized ttc_radio device to use
#ifdef TTC_USART1
    t_ttc_usart_config*         ConfigUSART;        // pointer to configuration of pre-initialized ttc_usart device
#endif
    t_ttc_packet_debug          PacketDebug;        // select which packet details should be debugged
    t_u8       StringBuffer[SIZE_ETR_STRINGBUFFER]; // universal string buffer used to compile strings
    t_u8                        RadioUserID;        // key to exclusive ttc_radio access
    t_ttc_radio_job_socket    SocketJob;        // statemachine data required to receive only packets of matching protocol type

    struct {
#ifdef EXAMPLE_TTC_RADIO_NODE_IS_ANCHOR
#endif
        unsigned reserved : 1;
    } Flags;
} etr_task_info_t;

//}StructureDeclarations
/** { Function Declarations ********************************************
 *
 * Declare all functions here that may be called from outside.
 * Rules of Good Code:
 * - document every function declaration
 * - prefix public function names with their filename to avoid double definitions
 */

/* DEPRECATED
** recursively initialize all memory blocks in given pool to same packet type
 *
 * @param Pool   memory pool storing memory buffers to be used as network packets (return value from ttc_heap_pool_create() )
 * @param Type   all memory blocks in Pool will be initialized to this packet type
 *
void example_ttc_radio_ranging_initialize_pool( t_ttc_heap_pool* Pool, e_ttc_packet_type Type );
*/
/** send given format string and its value via usart (if available)
 *
 * Note: Maximum length of string is limited by internal buffer!
 */
void example_ttc_radio_ranging_usart_send_stringf( const char* Format, t_base Value );

/** send given string via usart (if available)
 *
 * Note: String length unlimited
 * Note: This function is faster than example_ttc_radio_ranging_usart_send_stringf()
 */
void example_ttc_radio_ranging_usart_send_string( const char* String, t_u16 BufferSize );

/**  transmits given string via radio and copies it to TTC_USART1
 *
 * @param TaskInfo          pointer to struct containing all task data
 * @param Data              Buffer storing binary data to transmit
 * @param Amount            Amount of bytes to transmit from Data[]
 * @param TargetID          Node index where to send packet to
 * @param PacketType        type of data packet to use for transmission
 * @param StringBuffer      !=NULL: buffer used to prepare + send out debug message
 * @param StringBufferSize  size of StringBuffer[]
 */
void example_ttc_radio_ranging_transmit( etr_task_info_t* TaskInfo, t_u8* Data, t_u16 Amount, t_ttc_packet_address* TargetID, e_ttc_packet_type Type, t_u8* StringBuffer, t_base StringBufferSize );

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //example_ttc_radio_ranging_common_h
