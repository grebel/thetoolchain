#ifndef EXAMPLE_DAC_H
#define EXAMPLE_DAC_H

/** example_ttc_DAC.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_dac.
 *  The basic flow is as follows:
 *  (1) example_ttc_dac_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_dac_prepare() gets executed
 *
 *  Created from template example_ttc_device.h revision 11 at 20141215 10:19:15 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_dac.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_dac_t
    t_u8 Foo;  // example data
    // more arguments...
} example_dac_t;

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *
 * - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_dac_prepare();

/** task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void task_DAC(void *TaskArgument);


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_DAC_H
