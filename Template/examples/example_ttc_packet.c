/** example_ttc_packet.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple example code to demonstrate
 *  how to use ttc_packet.
 *
 *  The basic flow is as follows:
 *  (1) example_ttc_packet_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_packet_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 13 at 20150928 09:30:04 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_packet.h"
#include "../ttc-lib/ttc_gpio.h"      // general purpose IO
#include "../ttc-lib/ttc_string.h"    // string compare/ copy/ snprintf
#include "../ttc-lib/ttc_memory.h"    // memory copying, pointer testing
#include "../ttc-lib/ttc_random.h"    // generation of random numbers (for more realistic traffic generation)

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

// let's define a maximum packet size (must be greater than sizeof( t_ttc_packet ))
const t_u8 MaxTransferUnit = 100;

// Identifier for our virtual network
const t_u16 LocalPanID = 0xc001;

// amount of network nodes to start
#define AMOUNT_NODES 2

// Usage Statistic of Memory Pool
// Note: Activate by adding a line like this to your extensions.local/makefile.700_extra_settings file:
//       COMPILE_OPTS += -DTTC_HEAP_POOL_STATISTICS

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task simulating a network node.
 *
 * The node task will exchanges packets with other instances of _task_Node(). A ttc_list
 * structure serves as a virtual communication channel.
 * Each task pushes packets to and pops them from the list.
 *
 * This task is created automatically from example_ttc_packet_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _task_Node( void* Argument );

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function Definitions *************************************************

void _example_t_ttc_packetx( t_etp_node_config* NodeCfg, t_ttc_packet* Packet ) {

    // we know, that this packet came from a memory pool
    t_ttc_heap_block_from_pool* MemoryBlock = ( t_ttc_heap_block_from_pool* ) Packet;

    // every block from pool is also a list-item (conversion is a constant pointer arithmetic)
    t_ttc_list_item* Item2Send = ttc_heap_pool_to_list_item( MemoryBlock );

    #ifdef TTC_LED1
// toggle LED1 to indicate packet transmission
    ttc_task_critical_begin(); // block task switching while toggling status led
    ttc_gpio_put( TTC_LED1, 1 - ttc_gpio_get( TTC_LED1 ) );
    ttc_task_critical_end();
    #endif

    // append packet to list <=> send packet to virtual channel
    ttc_list_push_back_single( NodeCfg->List, Item2Send );

    // update statistics
    NodeCfg->AmountTX_Packets++;

}
t_ttc_packet* _example_ttc_packet_rx( t_etp_node_config* NodeCfg ) {

    // check if we can pop an item from the list (will return immediately)
    t_ttc_list_item* ReceivedItem = ttc_list_pop_front_single_try( NodeCfg->List );
    if ( ReceivedItem ) { // received a packet: process + free its memory block

        #ifdef TTC_LED2
        // toggle LED2 to indicate packet receival
        ttc_task_critical_begin(); // block task switching while toggling status led
        ttc_gpio_put( TTC_LED2, 1 - ttc_gpio_get( TTC_LED2 ) );
        ttc_task_critical_end();
        #endif

        // retrieve back memory block from list item (constant pointer arithmetic)
        t_ttc_heap_block_from_pool* ReceivedBlock = ttc_heap_pool_from_list_item( ReceivedItem );

        // memory blocks from pool can safely be casted to any other type
        t_ttc_packet* ReceivedPacket = ( t_ttc_packet* ) ReceivedBlock;

        // update statistics
        NodeCfg->AmountRX_Packets++;

        return ReceivedPacket;
    }

    return NULL; // nothing received
}

void _task_Node( void* Argument ) {
    t_etp_node_config* NodeCfg = ( t_etp_node_config* ) Argument;

    // variable used to assemble target IDs
    t_ttc_packet_address TargetID;

    // use our own address to fill most bytes automatically
    ttc_memory_copy( & TargetID, &NodeCfg->LocalAddress, sizeof( TargetID ) );

    do {
        // This loop runs endlessly if scheduler is available.
        // Otherwise it will run only onve and be called periodically from ttc_task_start_scheduler().
        t_ttc_heap_block_from_pool* EmptyBlock = ttc_heap_pool_block_get_try( NodeCfg->Pool );

        if ( EmptyBlock ) { // got a new memory block: create a packt from it and send it to virtual channel

            // initialize data packet with 16-bit Source + Target address (->packet_802154_types.h)
            ttc_packet_initialize( EmptyBlock, E_ttc_packet_type_802154_Data_001010 );

            // memory blocks from pool can safely be casted to any other type
            t_ttc_packet* NewPacket = ( t_ttc_packet* ) EmptyBlock;

            // set packet source address (will automatically copy Address16 according to packet type
            E_ttc_packet_address_source_set( NewPacket, &( NodeCfg->LocalAddress ) );

            // we know that our packet stores only 16-bit target address
            do { // generate random 16-bit target address
                TargetID.Address16 = 0x1000 + ttc_random_generate() % AMOUNT_NODES;
            }
            while ( TargetID.Address16 == NodeCfg->LocalAddress.Address16 );
            ttc_packet_address_target_set( NewPacket, &TargetID );

            // prepare packet payload
            t_u8* Payload;
            t_u16  PayloadSize;

            // using assert to ensure that we get valid pointer to payload area
            Assert( ttc_packet_payload_get_pointer( NewPacket, &Payload, &PayloadSize ) == ec_packet_OK, ttc_assert_origin_auto );

            // copy some text into packet payload area
            ttc_string_snprintf( Payload, PayloadSize, "Sensor #%04x -> Anchor #%04x", NodeCfg->LocalAddress.Address16, TargetID.Address16 );

            // send packet to virtual channel
            _example_t_ttc_packetx( NodeCfg, NewPacket );

        }

        // check if we can receive an item from virtual channel

        t_ttc_packet* ReceivedPacket = _example_ttc_packet_rx( NodeCfg );
        if ( ReceivedPacket ) { // process received packet

            if ( t_ttc_packet_addressarget_compare( ReceivedPacket, &( NodeCfg->LocalAddress ) ) ) { // this packet is for us
                e_ttc_packet_category PacketCategory = ttc_packet_identify( ReceivedPacket );

                switch ( PacketCategory ) {
                    case E_ttc_packet_category_Beacon: {          // broadcast transfer 1:n
                        // process received beacon...

                        break;
                    }
                    case E_ttc_packet_category_Data: {            // data transfer 1:1 or 1:n
                        // process received data...

                        // reusing memory buffer of packet to create acknowledgement packet

                        if ( ReceivedPacket->Meta.AddressFormat & E_ttc_packet_address_Source16 ) { // we expect such an addresstype
                            t_u16* SenderID;    // we have just checked that packet stores a 16-bit SourceID
                            t_u16* SenderPanID;

                            // reading source id from packet
                            E_ttc_packet_address_source_get_pointer( ReceivedPacket, ( void** ) &SenderID, &SenderPanID );
                            t_u16 SenderID16 = *SenderID;

                            // packet came from a memory pool so it's safe to cast it
                            ttc_packet_initialize( ( t_ttc_heap_block_from_pool* ) ReceivedPacket, E_ttc_packet_type_802154_ack );
                            ReceivedPacket->MAC.packet_802154_ack_000000.SequenceNo = SenderID16 & 0xff; // dummy sequence number

                            // send acknowledge packet
                            _example_t_ttc_packetx( NodeCfg, ReceivedPacket );
                            ReceivedPacket = NULL; // packet now belongs to someone else: delete pointer to avoid using it anymore!

                            // update statistics
                            NodeCfg->AmountRX_Acknowledge++;
                        }

                        break;
                    }
                    case E_ttc_packet_category_Command: {         // network layer command (e.g. acquire new logical id, associate to coordinator, ...)
                        // handle incoming command...

                        break;
                    }
                    case E_ttc_packet_category_Acknowledgement: { // acknowledgement for received packet
                        // process received acknowledge (e.g. free prior transmitted packet)

                        break;
                    }
                    default:
                        ttc_assert_halt_origin( ec_basic_InvalidImplementation ); // unknown packet category (memory corruption?)
                        break;
                }
            }
            else { // packet is targeted at foreign node

                // we may resend it in the hope it will then be read by its intended recipient node

                // update statistics
                NodeCfg->Amount_Dumped++;
            }
            if ( ReceivedPacket ) { // return memory block to its pool for reuse

                // we know that the packet is stored in a pool memory block
                t_ttc_heap_block_from_pool* ReceivedBlock = ( t_ttc_heap_block_from_pool* ) ReceivedPacket;

                // return memory block for reuse
                ttc_heap_pool_block_free( ReceivedBlock );
            }
        }

        ttc_task_yield();  // always sleep for a while to give cpu time to other tasks
    }
    while ( TTC_TASK_SCHEDULER_AVAILABLE );
}
//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_packet_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    // using leds to indicate packet receival and transmission if available
    #ifdef TTC_LED1
    ttc_gpio_init( TTC_LED1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    #endif
    #ifdef TTC_LED2
    ttc_gpio_init( TTC_LED2, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    #endif

    // Note: Adding a unique prefix to all global and static variable makes it
    //       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

    // Storing Configuration of all nodes in a static array has several advantages
    // - compiler can check ram usage at compile time
    // - no stack space used
    // - Array is still private and cannot be read from outside
    static t_etp_node_config et_packet_NodeConfigs[AMOUNT_NODES];

    // Create a memory pool for 10 memory blocks of equal size to use for packets.
    // The pool will provide
    // - fast, constant time allocation of new packets
    // - fast, constant time release of processed packets for later reuse
    // - synchronization: block allocation on empty pool will put task to sleep given CPU time to other tasks
    Assert( sizeof( t_ttc_packet ) < MaxTransferUnit, ttc_assert_origin_auto ); // packets will not fit into buffers. Increase MTU!
    t_ttc_heap_pool* Pool = ttc_heap_pool_create( MaxTransferUnit, 3 );

    // Create a dynamic. single linked list to use as communication channel
    // This virtual channel differs from real channels in different ways:
    // - no collisions
    // - each packet is seen by one node only (packet is skipped if node is not the receiver)
    //   => packets get lost often
    t_ttc_list* List = ttc_list_create( "Channel" );

    t_u8 TaskName[10];

    // configure and start all nodes
    for ( t_u8 I = 0; I < AMOUNT_NODES; I++ ) {
        t_etp_node_config* NodeConfig = & ( et_packet_NodeConfigs[I] );

        // clear node config
        ttc_memory_set( NodeConfig, 0, sizeof( *NodeConfig ) );

        // all nodes use same memory pool and virtual channel
        NodeConfig->Pool = Pool;
        NodeConfig->List = List;

        // 64 bit address of node
        NodeConfig->LocalAddress.Address64.Bytes[0] = 0xaf;
        NodeConfig->LocalAddress.Address64.Bytes[1] = 0xfe;
        NodeConfig->LocalAddress.Address64.Bytes[2] = 0x00;
        NodeConfig->LocalAddress.Address64.Bytes[3] = 0x00;
        NodeConfig->LocalAddress.Address64.Bytes[4] = 0x00;
        NodeConfig->LocalAddress.Address64.Bytes[5] = 0x00;
        NodeConfig->LocalAddress.Address64.Bytes[6] = 0x10;
        NodeConfig->LocalAddress.Address64.Bytes[7] = I;

        // 16 bit address of node
        NodeConfig->LocalAddress.Address16 = 0x1000 + I;

        ttc_string_snprintf( TaskName, 10, "Node%04x", NodeConfig->LocalAddress.Address16 );

        // starting task for this network node
        ttc_task_create( _task_Node,         // function to start as thread
                         ( char* ) TaskName, // thread name (just for debugging)
                         128,                // stack size (adjust to amount of local variables of _task_PACKET() AND ALL ITS CALLED FUNTIONS!)
                         NodeConfig,         // passed as argument to _task_PACKET()
                         1,                  // task priority (higher values mean more process time)
                         NULL                // can return a handle to created task
                       );
    }
}


//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}Function Definitions
