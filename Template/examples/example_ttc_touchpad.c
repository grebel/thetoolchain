/** example_ttc_touchpad.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_touchpad.
 *  The basic flow is as follows:
 *  (1) example_ttc_touchpad_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_touchpad_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 11 at 20150316 07:48:52 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_touchpad.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_string.h"

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!

// Note: every global or static variable needs a unique prefix to identify it during debugging!
//}

//{ Private Function Declarations ****************************************

// declaration of private function which can only be called from within this file
void _ett_AdjustTouchpad(void* Argument, t_u16 Position_X, t_u16 Position_Y);

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_touchpad_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not available.
      */

    static example_touchpad_t ett_TouchPadData;

    // configure graphic display
    ett_TouchPadData.ConfigGfx = ttc_gfx_get_configuration(1);
    Assert(ett_TouchPadData.ConfigGfx, ttc_assert_origin_auto);

    // set color to use for first screen clear
    ttc_gfx_color_bg24(TTC_GFX_COLOR24_WHITE);
    ttc_gfx_color_fg24(TTC_GFX_COLOR24_RED);
    ttc_gfx_init(1);

    // configure touchpad driver
    ett_TouchPadData.ConfigTouchpad = ttc_touchpad_get_configuration(1);
    Assert(ett_TouchPadData.ConfigTouchpad, ttc_assert_origin_auto);
    ett_TouchPadData.ConfigTouchpad->Max_X = ett_TouchPadData.ConfigGfx->Width;
    ett_TouchPadData.ConfigTouchpad->Max_Y = ett_TouchPadData.ConfigGfx->Height;
    ttc_touchpad_init(1);

    ttc_task_create( task_TOUCHPAD,               // function to start as thread
                     "tTOUCHPAD",                 // thread name (just for debugging)
                     128,                         // stack size
                     &ett_TouchPadData,           // passed as argument to task_TOUCHPAD()
                     1,                           // task priority (higher values mean more process time)
                     NULL                         // can return a handle to created task
                   );
}
void task_TOUCHPAD(void *Argument) {
    example_touchpad_t* TouchPadData = (example_touchpad_t*) Argument;
    static const char* StringTouchPad = "== Touchpad ==";

    t_ttc_touchpad_config* ConfigTouchpad = TouchPadData->ConfigTouchpad;
    t_ttc_gfx_config*      ConfigGfx      = TouchPadData->ConfigGfx;
    // first we adjust or touchpad
    menuAdjustTouchpad(1);

    ttc_gfx_text_at(0,0, StringTouchPad, -1);

#define tt_Buffer_SIZE 20
    static t_u8 tt_Buffer[tt_Buffer_SIZE]; // declaring as static to reduce stack usage (forbids to run two instances of task_TOUCHPAD() )

    t_u16 PreviousX = -1;
    t_u16 PreviousY = -1;
    const t_u16 MinY = 60;

    do { 
        // This loop runs endlessly if scheduler is available.
        // Otherwise it will run only onve and be called periodically from ttc_task_start_scheduler().
  
        if ( ttc_systick_delay_expired( & Data->Delay ) ) { // delay passed: send hello world (zeroed delay always expires)
            ttc_systick_delay_init( & Data->Delay, 20000 ); // set new delay to expire in 20 milliseconds
            
          e_ttc_touchpad_update Update = ttc_touchpad_check(1);
  
          if (Update) { // touchpad has new position update
              if (0) { // strange effects happen when text is being displayed (ToDo: DEBUG!)
                  ttc_task_msleep(50);
                  ttc_gfx_color_fg24(TTC_GFX_COLOR24_BLUE);
                  ttc_string_snprintf(tt_Buffer, tt_Buffer_SIZE, "X:%04i", ConfigTouchpad->Position_X);
                  ttc_gfx_text_solid_at(0,1, (char*) tt_Buffer, -1);
                  ttc_string_snprintf(tt_Buffer, tt_Buffer_SIZE, "Y:%04i", ConfigTouchpad->Position_Y);
                  ttc_gfx_text_solid_at(0,2, (char*) tt_Buffer, -1);
                  ttc_task_msleep(50);
              }
  #error  ToDo: Complete Implementation!
              switch (Update) {
              case ttu_None: break;
              }
          }
        }
              default: {
                  if (PreviousX != ConfigTouchpad->Position_X) {
                      if (PreviousX < ConfigGfx->Width) { // clear previous X line
                          ttc_gfx_color_fg24(TTC_GFX_COLOR24_WHITE);
                          ttc_gfx_line_vertical(PreviousX, MinY, ConfigGfx->Height - MinY);
                      }
                      if (Update == ttu_TouchMove) {
                          ttc_gfx_color_fg24(TTC_GFX_COLOR24_BLACK);
                          ttc_gfx_line_vertical(ConfigTouchpad->Position_X, MinY, ConfigGfx->Height - MinY);
                      }
                      PreviousX = ConfigTouchpad->Position_X;
                  }
                  if (PreviousY != ConfigTouchpad->Position_Y) {
                      if (PreviousY < ConfigGfx->Height) { // clear previous Y line
                          ttc_gfx_color_fg24(TTC_GFX_COLOR24_WHITE);
                          ttc_gfx_line_horizontal(0, PreviousY, ConfigGfx->Width);
                      }
                      if (Update == ttu_TouchMove) {
                          ttc_gfx_color_fg24(TTC_GFX_COLOR24_BLACK);
                          ttc_gfx_line_horizontal(0, ConfigTouchpad->Position_Y, ConfigGfx->Width);
                      }
                      PreviousY = ConfigTouchpad->Position_Y;
                  }
                  break;
              }
              }
          }
        ttc_task_yield(); // if scheduler available, give cpu to other processes
    } while (TTC_TASK_SCHEDULER_AVAILABLE);
}
void menuAdjustTouchpad(t_u8 LogicalIndex) {
    ttc_gfx_color_bg24(TTC_GFX_COLOR24_WHITE);
    ttc_gfx_clear();

    static const char* StringPressCenter = "Press on center";
    t_ttc_touchpad_config* ConfigTouchpad = ttc_touchpad_get_configuration(LogicalIndex);
    t_ttc_gfx_config*      Config_Gfx     = ttc_gfx_get_configuration(LogicalIndex);

    t_u16 MidX = Config_Gfx->Width  / 2;
    t_u16 MidY = Config_Gfx->Height / 2;
    ttc_gfx_line_horizontal(0, MidY, Config_Gfx->Width);
    ttc_gfx_line_vertical(MidX, 0, Config_Gfx->Height);

    ttc_gfx_text_at(0,0, StringPressCenter, -1);

    // reset adjustmend data
    ConfigTouchpad->AdjustX = 0;
    ConfigTouchpad->AdjustY = 0;

    while (!ConfigTouchpad->AdjustX) { // wait for user input
        e_ttc_touchpad_update Update = ttc_touchpad_read(ConfigTouchpad->LogicalIndex);
        if (Update) {
            ConfigTouchpad->AdjustX = ( Config_Gfx->Width  / 2) -  ConfigTouchpad->Position_X;
            ConfigTouchpad->AdjustY = ( Config_Gfx->Height / 2) -  ConfigTouchpad->Position_Y;
        }
        ttc_task_msleep(100);
    }

    ttc_gfx_clear();
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
