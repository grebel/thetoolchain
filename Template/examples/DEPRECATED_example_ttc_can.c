/** example_ttc_can.c **********************************************{
 *
 *                             The ToolChain
 *
 *
 * example_ttc_can_start()
 *     fills et_can_TaskArguments with data
 *     spawns thread task_CAN()
 *     returns immediately
 *
 * task_CAN()
 *     while (1) {
 *       do something
 *       ttc_task_msleep(100);
 *     }
 *
}*/

#include "example_ttc_can.h"
#include "../ttc-lib/ttc_can.h"
#include "../ttc-lib/ttc_task.h"     // allows to use application in single- and multitasking setup

//{ Global variables
#define TTC_CAN1_INDEX 1

const char* Text1 = "Hallo!\n";
const char* Text2 = "Welt\n";
//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_can_start() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */
    #ifdef TTC_LED1
    ttc_gpio_init( TTC_LED1, tgm_output_push_pull, tgs_Max );
    #endif
    #ifdef TTC_LED2
    ttc_gpio_init( TTC_LED2, tgm_output_push_pull, tgs_Max );
    #endif

    ttc_can_config_t* Config = ttc_can_get_configuration( TTC_CAN1_INDEX );
    Assert( Config, ec_UNKNOWN );

    // change some settings - CAN BitRate = 32MHz
    Config->Flags.Bits.Mode = 1; // =1; enable LoopBack Mode
    Config->Flags.Bits.SJW = 2;
    Config->BS1 = bs1_can_11tq;
    Config->BS2 = bs2_can_4tq;
    Config->Prescaler = 500; // (32MHz/PSC)/(BS1+BS2+SJW) = 250uS
    Config->activity_rx_isr = exu_activity_rx;

    ttc_can_errorcode_e Error = ttc_can_init( TTC_CAN1_INDEX );
    if ( Error == ec_can_OK ) {
        ttc_can_set_filter( 290, TTC_CAN1_INDEX );

        ttc_task_create( task_CAN,                // function to start as thread
                         "tCAN",                      // thread name (just for debugging)
                         128,                         // stack size
                         NULL,                        // passed as argument to task_CAN()
                         1,                           // task priority (higher values mean more process time)
                         NULL                         // can return a handle to created task
                       );
    }
    else
    { Assert( TTC_CAN1_INDEX, ec_InvalidConfiguration ); }
}
void task_CAN( void* TaskArgument ) {
    TaskArgument = TaskArgument;      // avoids warning: unused parameter 'TaskArgument'

    while ( 1 ) {
        ttc_can_send( TTC_CAN1_INDEX, 290, ( void* ) Text1, 7 /*sizeof(Text)*/ );
        ttc_task_msleep( 500 );  // always sleep for a while to give cpu time to other tasks
        ttc_can_send( TTC_CAN1_INDEX, 290, ( void* ) Text2, 5 /*sizeof(Text)*/ );
        ttc_task_msleep( 500 );  // always sleep for a while to give cpu time to other tasks
    }
}
void exu_activity_rx( ttc_can_config_t* CAN_Generic ) {
    #ifdef TTC_LED1
    if ( CAN_Generic->CAN_Rx_Message.Flags.Bits.DLC == 7 )
    { ttc_gpio_set( TTC_LED1 ); }
    else if ( CAN_Generic->CAN_Rx_Message.Flags.Bits.DLC == 5 )
    { ttc_gpio_clr( TTC_LED1 ); }
    #endif
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
