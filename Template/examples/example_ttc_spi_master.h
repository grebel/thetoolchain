#ifndef EXAMPLE_SPI_MASTER_H
#define EXAMPLE_SPI_MASTER_H

/** example_ttc_SPI.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_spi.
 *  The basic flow is as follows:
 *  (1) example_ttc_spi_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_spi_prepare() gets executed
 *
 *  Created from template example_ttc_device.h revision 11 at 20140423 11:04:14 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_systick.h"  // exact delays and timeouts
#include "../ttc-lib/ttc_task_types.h" // required for task delays

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_spi_master_t
    t_ttc_systick_delay  Delay;          // this type of delay is usable in single- and multitasking setup
    // more arguments...
} example_spi_master_t;

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_spi_master_prepare();

/** task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void task_SPI_Master( void* TaskArgument );


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_SPI_MASTER_H
