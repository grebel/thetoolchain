/*{ example_gfx_printf.c ************************************************

  example_gfx_printf_prepare() spawns thread taskBrightness()

  taskBrightness()
      Defines brightness of led as Ton / (Ton + Toff)
      spawns thread taskLEDs()
      while (1) {
        while (Ton < 100) { Ton++; Toff--; }
        while (Ton >   0) { Ton--; Toff++; }
      }

  taskLEDs()
      while (1) {
        switch on LED
        wait for Ton
        switch off LED
        wait for Toff
      }

}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_gfx_printf.h"
#include "../ttc-lib/ttc_heap.h"

// read-only configuration of current display (width, height, ...)
t_ttc_gfx_config* DisplayConfig = NULL;

t_u16 Counter = 0;
t_u8  LightLevel = 255;
t_u8  Buffer[10];
t_u16 SecsX = 0;
t_u16 SecsY = 0;

//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_gfx_printf_init() {

    // t_u8 MaxDisplay = ttc_gfx_get_max_display_index();
    //Assert(MaxDisplay > 0, ttc_assert_origin_auto); // no displays defined by board makefile!
    DisplayConfig = ttc_gfx_get_configuration( 1 );
    Assert_BASIC_Writable( DisplayConfig, ttc_assert_origin_auto );

    // set color to use for first screen clear
    ttc_gfx_color_bg24( TTC_GFX_COLOR24_AQUA );
    ttc_gfx_init( 1 );

    // initialize gfx for use with printf()
    ttc_gfx_stdout_set( 1 );
    ttc_string_register_stdout( ttc_gfx_stdout_print_block, ttc_gfx_stdout_print_string );

}
void example_gfx_printf_prepare() {
    example_gfx_printf_init();

    ttc_task_create( task_LCD,      // function to start as thread
                     "LCD",              // thread name (just for debugging)
                     512,                // stack size
                     ( void* ) NULL,     // passed as argument to taskLEDs()
                     1,                  // task priority (higher values mean more process time)
                     NULL                // can return a handle to created task
                   );
}
void task_LCD( void* TaskArgument ) {
    TaskArgument = TaskArgument; // avoids warning: unused parameter 'TaskArgument'

    const char* Hallo = "Hallo";
    const char* Welt  = "Welt";

    ttc_gfx_color_fg24( TTC_GFX_COLOR24_NAVY );
    ttc_gfx_text_cursor_set( 1, 1 );
    ttc_gfx_text_boxed( Hallo, -1, 1 );
    ttc_gfx_text_boxed( Welt,  -1, 1 );
    ttc_gfx_text_cursor_set( 1, 2 );
    ttc_gfx_text( "Seconds: ", -1 );
    SecsX = DisplayConfig->TextX;
    SecsY = DisplayConfig->TextY;
    ttc_gfx_rect( 1, 1, DisplayConfig->Width - 3, DisplayConfig->Height - 3 );
    t_base BytesFree = ttc_heap_get_free_size();
    ttc_gfx_text_cursor_set( 0, DisplayConfig->TextRows - 1 );
    printf( "free: %iB", BytesFree );


    while ( 1 ) {
        ttc_string_snprintf( Buffer, 10, "%4i", Counter );
        ttc_gfx_text_solid_at( SecsX, SecsY, ( char* ) Buffer, 10 );

        ttc_string_snprintf( Buffer, 10, "%%%08b", Counter );
        ttc_gfx_text_solid_at( SecsX - 5, SecsY + 1, ( char* ) Buffer, 10 );

        ttc_gfx_backlight( LightLevel );
        //LightLevel = 255 - LightLevel;

        ttc_task_msleep( 1000 );
        Counter++;
    }
}
void printLCD() {
    ttc_string_snprintf( Buffer, 10, "%i", Counter++ );
    ttc_gfx_text_solid_at( SecsX, SecsY, ( char* ) Buffer, 10 );
    //ttc_gfx_backlight(LightLevel);
}
void changeColorLCD() {
    ttc_gfx_text_cursor_set( 0, DisplayConfig->TextRows - 3 );

    if ( ( Counter % 2 ) == 0 )
    { printf( "Lorem" ); }
    else { printf( "Ipsum" ); }
}
void moreLCD() {
    ttc_gfx_text_cursor_set( 0, DisplayConfig->TextRows - 5 );

    if ( ( Counter % 2 ) == 0 )
    { printf( "WTF?!" ); }
    else { printf( "It works!" ); }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
