/*{ Template_FreeRTOS::.c ************************************************

  example_gfx_printf_start() spawns thread taskBrightness()

  taskBrightness()
      Defines brightness of led as Ton / (Ton + Toff)
      spawns thread taskLEDs()
      while (1) {
        while (Ton < 100) { Ton++; Toff--; }
        while (Ton >   0) { Ton--; Toff++; }
      }

  taskLEDs()
      while (1) {
        switch on LED
        wait for Ton
        switch off LED
        wait for Toff
      }

}*/

#include "DEPRECATED_example_input.h"

ttc_gfx_generic_t* DisplayConfig = NULL;

void example_input_start(){
    example_input_init();

    ttc_task_create(task_input,       // function to start as thread
                    "INPUT",              // thread name (just for debugging)
                    128,                // stack size
                    (void *) NULL,      // passed as argument to taskINPUTs()
                    1,                  // task priority (higher values mean more process time)
                    NULL                // can return a handle to created task
                    );




}

void example_input_init(){

    u8_t MaxDisplay = ttc_gfx_get_max_display_index();
    Assert(MaxDisplay > 0, ec_DeviceNotFound); // no displays defined by board makefile!
    DisplayConfig = ttc_gfx_get_configuration(1);
    Assert(DisplayConfig, ec_DeviceNotFound);

    // set color to use for first screen clear
    ttc_gfx_set_color_bg24(GFX_COLOR24_AQUA);
    ttc_gfx_init(1); //initialisation of the lcd
    ttc_input_start(1); //starting input-area-check,
    ttc_gfx_stdout_set(1);




}

void task_input(void *TaskArgument){
    TaskArgument = TaskArgument; // avoids warning: unused parameter 'TaskArgument'
    u16_t X=0,Y=0;
    ttc_input_register_input_area(button_event,NULL,0,0,200,80,NULL,gss_Rounded,me_MouseDown); //register a area which execute the butten event if the area is touched

    while(1){

        function_read_position(&X,&Y); //get present position
        ttc_gfx_set_pixel(X,Y); //set pixel

        ttc_task_msleep(1);
    }
    return;
}

void button_event(void *TaskArgument){
    ttc_gfx_set_color_bg24(GFX_COLOR24_LIME); //in this case the button event clear the display
    function_clear();
}
