#ifndef EXAMPLE_LEDS
#define EXAMPLE_LEDS

/*{ ExampleLeds::.h ************************************************

 Empty template for new header files.
 Copy and adapt to your needs.

}*/
//{ Includes *************************************************************
// Header files should typically only include *_types.h files
// to avoid nasty include loops.

#include "../ttc-lib/ttc_basic_types.h"    // basic datatypes for current microcontroller architecture
#include "../ttc-lib/ttc_gpio_types.h"     // allows to store gpio pins in structures
#include "../ttc-lib/ttc_task_types.h"     // allows to store task delay data in structures

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // t_configled
    t_u8                Brightness;       // 0 = off, 99 = 100% on
    e_ttc_gpio_pin      PortPin;          // LED to use
    BOOL                Initialized_TaskBrightness; // FALSE: taskBrightness is called first time
} t_configled;

//}Structures/ Enums
//{ Function declarations **************************************************

// main entry: initializes hardware + spawns all threads
// returns immediately
void example_leds_prepare();

// task: changes brightness continuosly
void taskBrightness( void* TaskArgument );

// task: applies brightness to single LED
void taskControlLed( void* TaskArgument );

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_LEDS
