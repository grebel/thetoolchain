/** example_ttc_board_olimex_stm32_lcd.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for prototype board olimex stm32 lcd with 320x240 pixel color touch display and 144 pin stm32f103
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_board_olimex_stm32_lcd_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_board_olimex_stm32_lcd_prepare() get executed
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_board_olimex_stm32_lcd_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template example_ttc_device_architecture.c revision 20 at 20180119 01:41:26 UTC
 *
 *  Authors: <AUTHOR>
 *  
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "example_ttc_board_olimex_stm32_lcd.h"
#include "../ttc-lib/ttc_board.h" // high-level driver of device to use
#include "../ttc-lib/board/board_olimex_stm32_lcd.h" // direct access to low-level driver
#include "../ttc-lib/ttc_basic.h"     // basic datatypes and definitions
#include "../ttc-lib/ttc_heap.h"      // dynamic memory allocations and safe arrays
#include "../ttc-lib/ttc_task.h"      // allows to use application in single- and multitasking setup
#include "../ttc-lib/ttc_systick.h"   // exact delays and timeouts

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_board_olimex_stm32_lcd_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _example_board_olimex_stm32_lcd_task(void *Argument);

/** Statemachine which may be used in single- and multitasking configuration.
 *
 * If no multitasking is active, all registered tasks are called one after another in an endless loop from 
 * ttc_task_start_scheduler(). Each task should then act like a statemachine and return immediately.
 * Implementing your example as a statemachine allows to use it in multi- and singletasking configuration.
 *
 * This function is not required if your example will only run with an enabled multitasking scheduler. 
 */
void _example_board_olimex_stm32_lcd_statemachine(void *Argument);

//}Private Function Declarations
//{ Function Definitions *************************************************

void example_ttc_board_olimex_stm32_lcd_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    if (1) { // create one instance of your example (copy and adjust for multiple instances) 
      
      // LogicalIndex = 1: use device defined as TTC_BOARD1 in makefile
      t_u8 LogicalIndex = 1;

      // allocate data of instance as local static (allows compiler to detect if it fits into RAM)
      static example_board_olimex_stm32_lcd_data_t DaE_ttc_board_architecture_olimex_stm32_lcd_1;
      
      // reset data
      ttc_memory_set(&DaE_ttc_board_architecture_olimex_stm32_lcd_1, 0, sizeof(DaE_ttc_board_architecture_olimex_stm32_lcd_1));
      
      // loading configuration of first board device
      DaE_ttc_board_architecture_olimex_stm32_lcd_1.Config_BOARD = ttc_board_get_configuration(LogicalIndex);
      
      // change configuration before initializing device
      // DaE_ttc_board_architecture_olimex_stm32_lcd_1.Config_BOARD->  = ;
      //...
      
      // initializing our board device here makes debugging easier (still single-tasking)
      // Logical index is stored inside each device configuration
      ttc_board_init(LogicalIndex);
      
      // Register at least one instance of a function to be run after all system has finished booting.
      // ttc_task_create() works in single- and multitasking environments.
      ttc_task_create(
#if (TTC_TASK_SCHEDULER_AVAILABLE)
                      _example_board_olimex_stm32_lcd_task,         // function to start as thread
#else
                      _example_board_olimex_stm32_lcd_statemachine, // function to run periodically (single tasking)
#endif
                       "tBOARD_OLIMEX_STM32_LCD",          // thread name (just for debugging)
                       256,                                 // stack size (adjust to amount of local variables of _example_board_olimex_stm32_lcd_task() AND ALL ITS CALLED FUNTIONS!) 
                       &DaE_ttc_board_architecture_olimex_stm32_lcd_1,     // passed as argument to _example_board_olimex_stm32_lcd_task()
                       1,                                   // task priority (higher values mean more process time)
                       NULL                                 // can return a handle to created task
                     );
    }
}

// Note: If you implement both of the example functions below, your example will run in single- and multitasking configuration.

void _example_board_olimex_stm32_lcd_statemachine(void *Argument) {
    Assert_BOARD_Writable(Argument, ttc_assert_origin_auto);                  // always check pointer arguments with Assert() before dereferencing them!
    example_board_olimex_stm32_lcd_data_t* Data = ( example_board_olimex_stm32_lcd_data_t* ) Argument; // we're expecting this pointer type

    // get pointer to device configuration
    t_ttc_board_config* Config_BOARD = Data->Config_BOARD;
  
    if ( ttc_systick_delay_expired( & Data->Delay ) ) { // example periodic delay
        ttc_systick_delay_init( & Data->Delay, 
                                ttc_systick_delay_get_minimum() << 4 // minimum delay * 16 is a safe way to always use a small and valid delay   
                              ); // set new delay time

      // implement statemachine here ...
    }
    
    // implement statemachine here ...
}
void _example_board_olimex_stm32_lcd_task(void *Argument) {         // example task will be run when multitasking scheduler (e.g. FreeRTOS) has been started
    Assert_BOARD_Writable(Argument, ttc_assert_origin_auto);                  // always check pointer arguments with Assert() before dereferencing them!
    example_board_olimex_stm32_lcd_data_t* Data = ( example_board_olimex_stm32_lcd_data_t* ) Argument; // we're expecting this pointer type

    // get pointer to device configuration (remove if not required)
    t_ttc_board_config* Config_BOARD = Data->Config_BOARD;

    do { 

        // call statemachine or implement task here...
        _example_board_olimex_stm32_lcd_statemachine(Argument);
        
        ttc_task_yield(); // give cpu to other processes
    } while (1); // Tasks run endlessly.
}

//}Function Definitions
