/*{ Template_FreeRTOS::.h ************************************************

  Example code for FreeRTOS + LCD display on Olimex prototype board STM32-LCD.
  written by Gregor Rebel 2011

}*/
#ifndef EXTENSION_ttc_gfx
#error EXTENSION_ttc_gfx must be defined!
#endif

//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_task.h"
#include "../ttc-lib/ttc_string.h"
#include "../ttc-lib/ttc_gfx.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

#ifndef EXTENSION_ttc_gfx
#error Missing EXTENSION_ttc_gfx !
#endif

//}Defines
//{ Structures/ Enums ****************************************************


//}Structures/ Enums
//{ Global Variables *****************************************************



//}Global Variables
//{ Function declarations **************************************************

// basic hardware intialization
void example_gfx_printf_init();

// main entry: initializes hardware + spawns all threads
// returns immediately
void example_gfx_printf_prepare();

// main task controlling LCD-panel
void task_LCD( void* TaskArgument );

void printLCD();
void changeColorLCD();
void moreLCD();

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
