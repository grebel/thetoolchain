/** example_ttc_slam.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2016.
 *
 *  This source should provide a small and simple example code to demonstrate 
 *  how to use ttc_slam.
 *
 *  The basic flow is as follows:
 *  (1) example_ttc_slam_start() gets called in single tasking mode from 
 *      ttc_extensions.c:startExtensions() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_slam_start() get executed
 *
 *  Created from template example_ttc_device.c revision 14 at 20160607 10:19:25 UTC
 *
 *  Authors: <AUTHOR>
 *  
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "example_ttc_slam.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_slam.h"

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_slam_start() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _task_SLAM(void *Argument);

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function Definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_slam_start() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    // Note: Adding a unique prefix to all global and static variable makes it
    //       easier to get an overview of memory usage via _/calculateMemoryUsage.pl
    static example_slam_data_t et_slam_Data;
    
    et_slam_Data.LogicalIndex = 1; // set logical index of device to use
    
    // get configuration of slam device
    ttc_slam_config_t* Config_SLAM = ttc_slam_get_configuration(et_slam_Data.LogicalIndex);
    
    // change configuration before initializing device
    Config_SLAM->Flags.Bits.Reserved1 = 1; // delete me!
    //...
    
    // initializing our slam device here makes debugging easier (still single-tasking)  
    ttc_slam_init(et_slam_Data.LogicalIndex);

    ttc_task_create( _task_SLAM,       // function to start as thread
                     "tSLAM",          // thread name (just for debugging)
                     128,                  // stack size (adjust to amount of local variables of _task_SLAM() AND ALL ITS CALLED FUNTIONS!) 
                     &et_slam_Data,    // passed as argument to _task_SLAM()
                     1,                    // task priority (higher values mean more process time)
                     NULL                  // can return a handle to created task
                   );
}
void _task_SLAM(void *Argument) {
    Assert(ttc_memory_is_writable(Argument), ec_NULL);                  // always check pointer arguments with Assert() before dereferencing them!
    example_slam_data_t* Data = ( example_slam_data_t* ) Argument; // we're expecting this pointer type

    // get configuration of already initialized slam device
    ttc_slam_config_t* Config_SLAM = ttc_slam_get_configuration(Data->LogicalIndex);
    (void) Config_SLAM;   // yet unused

    while (1) {
      // do something...
      
      ttc_task_yield();    // always sleep for a while to give cpu time to other tasks
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}Function Definitions
