/*{ example_ttc_list.c ************************************************

  etl_task_A() allocates memory blocks from pool and sends them to a list.
  etl_task_B() receives memory blocks from list and returns them to their pool.

}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_list.h"

//{ Global variables

// Note: Global and static variables require a unique prefix (here "el_") to avoid
//       conflicting symbol names when combined with other sources!

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

// amount of list items to create
#define ETL_AMOUNT_ITEMS 100

// all list items are created at compile time (easy to check for compiler)
etl_Item_t ListItems[ETL_AMOUNT_ITEMS];

// transports list items from etl_task_A() to etl_task_B()
t_ttc_list List_A2B;

// transports list items from etl_task_B() to etl_task_A()
t_ttc_list List_B2A;

// Counters for blocks being transmitted/ received
t_u32 CountTx = 0;
t_u32 CountRx = 0;


//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_list_prepare() {
#ifdef TTC_LED1
    ttc_gpio_init(TTC_LED1, E_ttc_gpio_mode_output_push_pull);
#endif
#ifdef TTC_LED2
    ttc_gpio_init(TTC_LED2, E_ttc_gpio_mode_output_push_pull);
#endif

    // lists must be initialized BEFORE any task is using them!
    ttc_list_init(&List_A2B, "List_A2B");
    ttc_list_init(&List_B2A, "List_B2A");

    ttc_task_create( etl_task_A,  // function to start as thread
                     "A",         // thread name (just for debugging)
                     128,              // stack size
                     NULL,             // passed as argument to taskBrightness()
                     1,                // task priority (higher values mean more process time)
                     NULL              // can return a handle to created task
                     );
    ttc_task_create( etl_task_B,  // function to start as thread
                     "B",         // thread name (just for debugging)
                     128,              // stack size
                     NULL,             // passed as argument to taskBrightness()
                     1,                // task priority (higher values mean more process time)
                     NULL              // can return a handle to created task
                     );

}
void etl_task_A(void *TaskArgument) {
    (void) TaskArgument; // not used
    volatile t_u8 Value = 0;

    // add all statically created items to first list
    // Note: list operations are only allowed from a task!
    for (t_u8 Count = 0; Count < ETL_AMOUNT_ITEMS; Count++) {

        // each item gets different data
        ListItems[Count].Data = Count;

        // add item to first list
        ttc_list_push_back_single(&List_A2B, &(ListItems[Count].ListItem));
    }

    while (1) {
#ifdef TTC_LED1
        ttc_gpio_setn(TTC_LED1, Value++ & 1);
#endif
        // receive one single item from B
        t_ttc_list_item* ListItem = ttc_list_pop_front_single_wait(&List_B2A);

        // extract data from list item
        etl_Item_t* Item = etl_list_item_2_item(ListItem);
        Value = Item->Data;

        // send item back to B
        ttc_list_push_back_single(&List_A2B, &(Item->ListItem));

        ttc_task_msleep(30);
    }
}
void etl_task_B(void *TaskArgument) {
    (void) TaskArgument; // not used
    volatile t_u8 Value;

    while (1) {
#ifdef TTC_LED2
        ttc_gpio_setn(TTC_LED2, Value & 1);
#endif
        // receive three single items from A
        t_ttc_list_item* ListItem1 = ttc_list_pop_front_single_wait(&List_A2B);
        t_ttc_list_item* ListItem2 = ttc_list_pop_front_single_wait(&List_A2B);
        t_ttc_list_item* ListItem3 = ttc_list_pop_front_single_wait(&List_A2B);

        // extract data from list items
        etl_Item_t* Item1 = etl_list_item_2_item(ListItem1);
        etl_Item_t* Item2 = etl_list_item_2_item(ListItem2);
        etl_Item_t* Item3 = etl_list_item_2_item(ListItem3);

        Value = Item3->Data;

        // change order of items 1->2->3 => 1->3->2
        Item1->ListItem.Next = &(Item3->ListItem);
        Item3->ListItem.Next = &(Item2->ListItem);

        // send all items back to A
        ttc_list_push_back_multiple(&List_B2A, &(Item1->ListItem), &(Item2->ListItem), 3);

        CountRx+=3;
    }
    (void) Value; // avoid warning unused..
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
