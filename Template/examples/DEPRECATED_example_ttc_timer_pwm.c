#include "example_ttc_timer_pwm.h"

//{ Global variables
#define INDEX_TIMER1 2
#define INDEX_TIMER2 3

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************
ttc_timer_config_t * Config_tim2;
ttc_timer_config_t * Config_tim3;

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_timer_init() {
#ifdef TTC_LED1
    ttc_gpio_init(TTC_LED1, tgm_output_push_pull, tgs_Max);
    ttc_gpio_set(TTC_LED1);
#endif

#ifdef TTC_LED2
    ttc_gpio_init(TTC_LED2, tgm_output_push_pull, tgs_Max);
    ttc_gpio_set(TTC_LED2);

#endif
}
void example_ttc_timer_pwm_start() {

    example_ttc_timer_init();

    /*--------------------------------- GPIO Configuration -------------------------*/

    /* If clocks are not enable you have to enabled them */

    /*Pin Configuration for Timer 2 */
    ttc_gpio_init(tgp_a0,tgm_alternate_function_push_pull, tgs_Max);
    ttc_gpio_alternate_function(tgp_a0,GPIO_AF_TIM2);

    /*Pin Configuration for Timer 3 */
    ttc_gpio_init(tgp_c6,tgm_alternate_function_push_pull, tgs_Max);
    ttc_gpio_alternate_function(tgp_c6,GPIO_AF_TIM3);

//    sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
//    ttc_gpio_init(tgp_a8,tgm_alternate_function_push_pull,tgs_40MHz);
//    sysclock_stm32l1xx_RCC_MCOConfig(ttc_sysclock_RCC_MCOSource_SYSCLK,ttc_sysclock_RCC_MCODiv_1); // Put on MCO pin the: System clock selected

    u32_t CoreClock = ttc_sysclock_get_frequency();

    u16_t PrescalerValue = (u16_t) (CoreClock / 8000000) - 1;

    /* Configuring Timer 2 */
    Config_tim2 = ttc_timer_get_configuration(INDEX_TIMER1);
    Config_tim2->Flags.Bits.OutputPWM = 0b1;
    Config_tim2->LowLevelConfig->Period = (80-1);
    Config_tim2->LowLevelConfig->Prescaler = PrescalerValue;
    Config_tim2->LowLevelConfig->ClockDivision = 0;
    Config_tim2->LowLevelConfig->CounterMode = 0x0000;

    /* PWM1 Mode configuration: Channel1  100KHz */
    Config_tim2->Flags.Bits.PWM_Channel= 0; // We choose pwm channel number one
    Config_tim2->LowLevelConfig->oc_Mode =0x0060;
    Config_tim2->LowLevelConfig->oc_OutputState =0x0001;
    Config_tim2->LowLevelConfig->oc_Polarity =0x0000;
    Config_tim2->LowLevelConfig->oc_Pulse = 20;
    ttc_timer_init(INDEX_TIMER1);



    /* Configuring Timer 3 */
    Config_tim3 = ttc_timer_get_configuration(INDEX_TIMER2);
    Config_tim3->Flags.Bits.OutputPWM = 0b1;
    Config_tim3->LowLevelConfig->Period = (665);
    Config_tim3->LowLevelConfig->Prescaler = PrescalerValue;
    Config_tim3->LowLevelConfig->ClockDivision = 0;
    Config_tim3->LowLevelConfig->CounterMode = 0x0000;

    /* PWM1 Mode configuration: Channel1 --> 12KHz */
    Config_tim3->Flags.Bits.PWM_Channel= 0; // We choose pwm channel number one
    Config_tim3->LowLevelConfig->oc_Mode =0x0060;
    Config_tim3->LowLevelConfig->oc_OutputState =0x0001;
    Config_tim3->LowLevelConfig->oc_Polarity =0x0000;
    Config_tim3->LowLevelConfig->oc_Pulse = 333;
    ttc_timer_init(INDEX_TIMER2);


    while(1){

    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
