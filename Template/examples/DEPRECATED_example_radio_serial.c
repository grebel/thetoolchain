/*{ example_radio.c ******************************************************
 
  Basic test of radio transceivers without any protocol.

  Initializes first radio and periodically transmits data.
  Received data is echoed via TTC_USART1.

}*/

#include "example_radio_serial.h"

//{ Function definitions *************************************************

// configuration data of single radio
ttc_radio_generic_t* MyRadio1;
ttc_radio_generic_t* MyRadio2;

const char* Text_HelloWorld = "Hello World this is #%i.%i TX#%i\n";

// randomly chosen ID of this node
u8_t MyID = 0;
u8_t TxLedState = 0;
u8_t RxLedState = 0;
BOOL RadioInitialized = FALSE;

#define LED_TX TTC_LED1
#define LED_RX TTC_LED2

// disable individual radios
// #undef TTC_RADIO1
// #undef TTC_RADIO2
void ers_Task_Beacon(void *TaskArgument) ;//D
void example_radio_serial_start() {
    example_radio_serial_init();

}
void example_radio_serial_init() {

    ttc_gpio_init(LED_TX, tgm_output_push_pull);
    ttc_gpio_init(LED_RX, tgm_output_push_pull);

#if TTC_DEBUG_RADIO==1
    ttc_gpio_init(TTC_DEBUG_RADIO_PIN1, tgm_output_push_pull);
    ttc_gpio_init(TTC_DEBUG_RADIO_PIN2, tgm_output_push_pull);
    ttc_gpio_init(TTC_DEBUG_RADIO_PIN3, tgm_output_push_pull);
#endif

    // retrieve default settings of first USART
    ttc_usart_config_t * usart_generic = ttc_usart_get_configuration(1);

    usart_generic->Flags.Bits.DelayedTransmits = 0;

    // change some settings
    usart_generic->receiveBlock = receiveData;
    usart_generic->Flags.Bits.IrqOnRxNE = 1; // enable IRQ for incoming data
    usart_generic->Char_EndOfLine = 10;      // packets end at linefeed
    Assert_USART(ttc_usart_init(1) == tce_OK, ec_UNKNOWN);

    // set usart #1 as stdout interface
    ttc_usart_stdout_set(1);
    ttc_string_register_stdout(ttc_usart_stdout_send_block, ttc_usart_stdout_send_string);
    printf(Text_HelloWorld, MyID, 1, 0);

#ifdef TTC_RADIO1
    ttc_radio_errorcode_e Error_Radio = tre_OK;
    if (1) Error_Radio = ttc_radio_init(1,0);
    Assert_Radio(Error_Radio==tre_OK,ec_InvalidConfiguration);
    if (1) ttc_radio_register_tx_function(1, txFunction_R1);
    if (1) ttc_radio_register_rx_function(1, rxFunction_R1);
    RadioInitialized = TRUE;
#endif

    // initialize random number generator
    ttc_srand(ttc_task_get_elapsed_usecs());

    // create random id
    MyID = (u8_t) ttc_rand() & 0xff;
    printf(Text_HelloWorld, MyID, 1, 0);

#ifdef TTC_RADIO1 //D
    if (1)
        ttc_task_create( ers_Task_Beacon,                    // function to start as thread
                         "taskRadio1",                 // thread name (just for debugging)
                         TTC_TASK_MINIMAL_STACK_SIZE,  // stack size
                         (void *) 1,                   // passed as argument to taskRadio()
                         1,                            // task priority (higher values mean more process time)
                         (void *) NULL                 // can return a handle to created task
                         );
#endif
}

void ers_Task_Beacon(void *TaskArgument) { //D

    u8_t RadioIndex = 0xff & (u32_t) TaskArgument;
#define TEMP_SIZE 100
    u8_t Temp[TEMP_SIZE];
    u16_t TxCount = 0;
    ttc_radio_set_destination(RadioIndex, 255);
    BOOL StopSending = FALSE;

    while (1) {
        snprintf(Temp, TEMP_SIZE, Text_HelloWorld, MyID, RadioIndex, TxCount++);
        ttc_radio_errorcode_e Error;
        if ( (Error = ttc_radio_send_string(RadioIndex, (char*) Temp, TEMP_SIZE)) != tre_OK)
            ttc_string_printf("ERROR - Cannot send text #%i via Radio #%i (Status=%i)\n", TxCount-1, RadioIndex, Error);
        uSleep((ttc_rand() % 10000) + 500);
        while (StopSending)
            ttc_task_yield();
    }
}
void txFunction_R1(ttc_radio_generic_t* RadioCfg, u8_t Size, const u8_t* Buffer) {
    Assert(RadioCfg!=NULL, ec_NULL);
    (void) RadioCfg;
    TxLedState = 1 - TxLedState;
    ttc_gpio_setn(LED_TX, TxLedState);
//    static u8_t* Temp = NULL;
//    static u8_t* Additional = NULL;
//    if(Size>TTC_NETWORK_MAX_PAYLOAD_SIZE){
//        Assert(NULL,ec_ArrayIndexOutOfBound);
//    }
//    if (Temp == NULL) {
//        Temp = ttc_heap_alloc(TTC_NETWORK_MAX_PAYLOAD_SIZE+10);
//        Assert(Temp!=NULL, ec_Malloc);
//        Additional = snprintf(Temp, TTC_NETWORK_MAX_PAYLOAD_SIZE, "TX->%i: ", RadioCfg->TargetAddress);
//    }

//    ttc_memory_copy(Additional, Buffer, Size);
//    Additional[Size] = 0;
//    ttc_string_print(Temp, TTC_NETWORK_MAX_PAYLOAD_SIZE+10);
}
ttc_heap_block_t* rxFunction_R1(ttc_radio_generic_t* RadioCfg, ttc_heap_block_t* Block, u8_t RSSI, u8_t TargetAddress) {
    (void) RadioCfg;
    (void) Block;

    ttc_gpio_setn(LED_RX, RxLedState);
#if TTC_DEBUG_RADIO==1
    RxLedState = 1 - RxLedState;
    if (1) { ttc_gpio_setn(TTC_DEBUG_RADIO_PIN3, RxLedState); }
#endif
    if (1) {
        ttc_usart_send_block(1, Block);
        return NULL;
    }
    else
        return Block;// let radio reuse this block immediately
}
ttc_heap_block_t* receiveData(ttc_usart_config_t* USART_Generic, ttc_heap_block_t* Data) {
    Assert(USART_Generic != NULL, ec_InvalidArgument);
    Assert(Data          != NULL, ec_InvalidArgument);

    static u8_t TxCount;

    if (RadioInitialized)
        ttc_radio_send_block(1, Data);

    TxCount++;
    return NULL; // ttc_radio_send_block() will release this block!
}

void esr_reloadWatchdog(u8_t Index) {
#ifdef EXTENSION_500_ttc_watchdog
    ttc_watchdog_reload1();
#endif
}

void toggleDebug();

void toggleDebug() {

    static u8_t On = 0;
    ttc_gpio_setn(PIN_PD15, On);
    On = 1-On;
}

//}FunctionDefinitions

