#ifndef EXAMPLE_TTC_TIMER_PWM
#define EXAMPLE_TTC_TIMER_PWM

//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_timer.h"

#ifdef EXTENSION_500_ttc_task
#  include "../ttc-lib/ttc_task.h"
#endif

#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_watchdog.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

#ifndef TTC_LED1
  #error TTC_LED1 not defined!

  // Example header-definition:
  // #define TTC_LED2   PIN_PC6    // status led 1 on Olimex proto board P107
  //
  // Example makefile-definition:
  // COMPILE_OPTS += -DTTC_LED2=PIN_PC6
#endif

//}Defines
//{ Structures/ Enums

//}
//{ Function declarations **************************************************

// basic hardware intialization
void example_ttc_timer_init();

// main entry: initializes hardware + spawns all timers
// returns immediately
void example_ttc_timer_start();

// task: change the state of a pin
void ChangeHighLED1(void *TaskArgument);
void ChangeLowLED1(void *TaskArgument);
void ChangeHighLED2(void *TaskArgument);
void ChangeLowLED2(void *TaskArgument);

void taskBrightness(); 

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_TTC_TIMER
