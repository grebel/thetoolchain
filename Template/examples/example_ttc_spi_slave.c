/** example_ttc_spi.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_spi.
 *  The basic flow is as follows:
 *  (1) example_ttc_spi_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_spi_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 11 at 20140423 11:04:14 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_spi_slave.h"
#include "../ttc-lib/ttc_heap.h"     // dynamic memory allocations during runtime

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!

// Logical index of SPI device to use

#define SPI_INDEX 1

// The corresponding physical spi device has been defined by board makefile.
// Its current settings can be found in compile_options.h and in extensions.actice/makefile:
//
// LogicalIndex 1 is defined by TTC_SPI1* constants.
// LogicalIndex 2 is defined by TTC_SPI2* constants.
// ...
// Example excerpt from compile options for logical index 1:
// #define TTC_SPI1 ttc_device_1
// #define TTC_SPI1_MISO E_ttc_gpio_pin_a11
// #define TTC_SPI1_MOSI E_ttc_gpio_pin_a12
// #define TTC_SPI1_NSS E_ttc_gpio_pin_a15
// #define TTC_SPI1_SCK E_ttc_gpio_pin_b3

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_spi_slave_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    example_spi_slave_t* Arguments = ttc_heap_alloc_zeroed( sizeof( example_spi_slave_t ) );

    #ifdef TTC_LED1
    ttc_gpio_init( TTC_LED1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min );
    #endif

    t_ttc_spi_config* ConfigSPI = ttc_spi_get_configuration( SPI_INDEX ); // getting default configuration for SPI #1
    ConfigSPI->Flags.Bits.Master = 0;  // only required to make sure that use of master mode is forbidden to application
    ConfigSPI->Flags.Bits.Slave  = 1;
    e_ttc_spi_errorcode Result = ttc_spi_init( SPI_INDEX );
    Assert( Result == ec_spi_OK, ec_spi_DeviceNotFound ); // SPI now operational in Master Mode

    ttc_task_create( task_SPI_Slave,         // function to start as thread
                     "tSPI_Slave",           // thread name (just for debugging)
                     128,                    // stack size
                     Arguments,  // passed as argument to task_SPI()
                     1,                      // task priority (higher values mean more process time)
                     NULL                    // can return a handle to created task
                   );
}
void task_SPI_Slave( void* TaskArgument ) {
    Assert_Writable( TaskArgument, ttc_assert_origin_auto );                // always check pointer arguments with Assert()!
    example_spi_slave_t* Data = ( example_spi_slave_t* ) TaskArgument; // we're expecting this pointer type
    t_u8 ByteReceived = 1;


    do {
        // This loop runs endlessly if scheduler is available.
        // Otherwise it will run only onve and be called periodically from ttc_task_start_scheduler().

        if ( ttc_systick_delay_expired( & Data->Delay ) ) { // delay passed: send hello world (zeroed delay always expires)
            ttc_systick_delay_init( & Data->Delay, 500000 ); // set new delay to expire in 500 milliseconds

            // do something...
            #ifdef TTC_LED1
            ttc_gpio_clr( TTC_LED1 );
            #endif

            ttc_spi_slave_read( SPI_INDEX, &ByteReceived, 1 );
            if ( ByteReceived ) {
                ttc_spi_slave_send( SPI_INDEX, &ByteReceived, 1 ); // send received byte back to master

                ttc_task_msleep( 200 );  // always sleep for a while to give cpu time to other tasks
                #  ifdef TTC_LED1
                ttc_gpio_set( TTC_LED1 );
                #  endif
            }
        }
        ttc_task_yield(); // if scheduler available, give cpu to other processes
    }
    while ( TTC_TASK_SCHEDULER_AVAILABLE );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
