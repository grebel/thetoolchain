/*{ DEPRECATED_example_radio.c ******************************************************
 
  Basic test of radio transceivers without any protocol.

  Initializes first radio and periodically transmits data.
  Received data is echoed via TTC_USART1.

}*/

#include "example_usb_vcp.h"

//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

// configuration data of single radio
//ttc_radio_generic_t* MyRadio1;
//ttc_radio_generic_t* MyRadio2;

// randomly chosen ID of this node
//u8_t MyID = 0;
//u8_t TxLedState = 0;
//u8_t RxLedState = 0;

//#define LED_TX TTC_LED1
//#define LED_RX TTC_LED2

// disable individual radios
// #undef TTC_RADIO1
// #undef TTC_RADIO2


#ifdef Virtual_Com_PortDevice_Descriptor_external
/* USB Standard Device Descriptor */
const uint8_t Virtual_Com_Port_DeviceDescriptor[] =
  {
    0x12,   /* bLength */
    USB_DEVICE_DESCRIPTOR_TYPE,     /* bDescriptorType */
    0x00,
    0x02,   /* bcdUSB = 2.00 */
    0x02,   /* bDeviceClass: CDC */
    0x00,   /* bDeviceSubClass */
    0x00,   /* bDeviceProtocol */
    0x40,   /* bMaxPacketSize0 */
    0x83,
    0x04,   /* idVendor = 0x0483 */
    0x40,
    0x57,   /* idProduct = 0x7540 */
    0x00,
    0x02,   /* bcdDevice = 2.00 */
    1,              /* Index of string descriptor describing manufacturer */
    2,              /* Index of string descriptor describing product */
    3,              /* Index of string descriptor describing the device's serial number */
    0x01    /* bNumConfigurations */
  };

const uint8_t Virtual_Com_Port_ConfigDescriptor[] =
  {
    /*Configuration Descriptor*/
    0x09,   /* bLength: Configuration Descriptor size */
    USB_CONFIGURATION_DESCRIPTOR_TYPE,      /* bDescriptorType: Configuration */
    VIRTUAL_COM_PORT_SIZ_CONFIG_DESC,       /* wTotalLength:no of returned bytes */
    0x00,
    0x02,   /* bNumInterfaces: 2 interface */
    0x01,   /* bConfigurationValue: Configuration value */
    0x00,   /* iConfiguration: Index of string descriptor describing the configuration */
    0xC0,   /* bmAttributes: self powered */
    0x32,   /* MaxPower 0 mA */
    /*Interface Descriptor*/
    0x09,   /* bLength: Interface Descriptor size */
    USB_INTERFACE_DESCRIPTOR_TYPE,  /* bDescriptorType: Interface */
    /* Interface descriptor type */
    0x00,   /* bInterfaceNumber: Number of Interface */
    0x00,   /* bAlternateSetting: Alternate setting */
    0x01,   /* bNumEndpoints: One endpoints used */
    0x02,   /* bInterfaceClass: Communication Interface Class */
    0x02,   /* bInterfaceSubClass: Abstract Control Model */
    0x01,   /* bInterfaceProtocol: Common AT commands */
    0x00,   /* iInterface: */
    /*Header Functional Descriptor*/
    0x05,   /* bLength: Endpoint Descriptor size */
    0x24,   /* bDescriptorType: CS_INTERFACE */
    0x00,   /* bDescriptorSubtype: Header Func Desc */
    0x10,   /* bcdCDC: spec release number */
    0x01,
    /*Call Management Functional Descriptor*/
    0x05,   /* bFunctionLength */
    0x24,   /* bDescriptorType: CS_INTERFACE */
    0x01,   /* bDescriptorSubtype: Call Management Func Desc */
    0x00,   /* bmCapabilities: D0+D1 */
    0x01,   /* bDataInterface: 1 */
    /*ACM Functional Descriptor*/
    0x04,   /* bFunctionLength */
    0x24,   /* bDescriptorType: CS_INTERFACE */
    0x02,   /* bDescriptorSubtype: Abstract Control Management desc */
    0x02,   /* bmCapabilities */
    /*Union Functional Descriptor*/
    0x05,   /* bFunctionLength */
    0x24,   /* bDescriptorType: CS_INTERFACE */
    0x06,   /* bDescriptorSubtype: Union func desc */
    0x00,   /* bMasterInterface: Communication class interface */
    0x01,   /* bSlaveInterface0: Data Class Interface */
    /*Endpoint 2 Descriptor*/
    0x07,   /* bLength: Endpoint Descriptor size */
    USB_ENDPOINT_DESCRIPTOR_TYPE,   /* bDescriptorType: Endpoint */
    0x82,   /* bEndpointAddress: (IN2) */
    0x03,   /* bmAttributes: Interrupt */
    VIRTUAL_COM_PORT_INT_SIZE,      /* wMaxPacketSize: */
    0x00,
    0xFF,   /* bInterval: */
    /*Data class interface descriptor*/
    0x09,   /* bLength: Endpoint Descriptor size */
    USB_INTERFACE_DESCRIPTOR_TYPE,  /* bDescriptorType: */
    0x01,   /* bInterfaceNumber: Number of Interface */
    0x00,   /* bAlternateSetting: Alternate setting */
    0x02,   /* bNumEndpoints: Two endpoints used */
    0x0A,   /* bInterfaceClass: CDC */
    0x00,   /* bInterfaceSubClass: */
    0x00,   /* bInterfaceProtocol: */
    0x00,   /* iInterface: */
    /*Endpoint 3 Descriptor*/
    0x07,   /* bLength: Endpoint Descriptor size */
    USB_ENDPOINT_DESCRIPTOR_TYPE,   /* bDescriptorType: Endpoint */
    0x03,   /* bEndpointAddress: (OUT3) */
    0x02,   /* bmAttributes: Bulk */
    VIRTUAL_COM_PORT_DATA_SIZE,             /* wMaxPacketSize: */
    0x00,
    0x00,   /* bInterval: ignore for Bulk transfer */
    /*Endpoint 1 Descriptor*/
    0x07,   /* bLength: Endpoint Descriptor size */
    USB_ENDPOINT_DESCRIPTOR_TYPE,   /* bDescriptorType: Endpoint */
    0x81,   /* bEndpointAddress: (IN1) */
    0x02,   /* bmAttributes: Bulk */
    VIRTUAL_COM_PORT_DATA_SIZE,             /* wMaxPacketSize: */
    0x00,
    0x00    /* bInterval */
  };

/* USB String Descriptors */
const uint8_t Virtual_Com_Port_StringLangID[VIRTUAL_COM_PORT_SIZ_STRING_LANGID] =
  {
    VIRTUAL_COM_PORT_SIZ_STRING_LANGID,
    USB_STRING_DESCRIPTOR_TYPE,
    0x09,
    0x04 /* LangID = 0x0409: U.S. English */
  };


const uint8_t Virtual_Com_Port_StringVendor[VIRTUAL_COM_PORT_SIZ_STRING_VENDOR] =
  {
    VIRTUAL_COM_PORT_SIZ_STRING_VENDOR,     /* Size of Vendor string */
    USB_STRING_DESCRIPTOR_TYPE,             /* bDescriptorType*/
    /* Manufacturer: "STMicroelectronics" */
    'T', 0, 'E', 0, 'S', 0, 'T', 0, '-', 0, 'V', 0, 'E', 0, 'N', 0,
    'D', 0, 'O', 0, 'R', 0, ' ', 0, ' ', 0, ' ', 0, ' ', 0, ' ', 0,
    ' ', 0, ' ', 0

  };

const uint8_t Virtual_Com_Port_StringProduct[VIRTUAL_COM_PORT_SIZ_STRING_PRODUCT] =
  {
    VIRTUAL_COM_PORT_SIZ_STRING_PRODUCT,          /* bLength */
    USB_STRING_DESCRIPTOR_TYPE,        /* bDescriptorType */
    /* Product name: "STM32 Virtual COM Port" */
    'T', 0, 'E', 0, 'S', 0, 'T', 0, '-', 0, 'P', 0, 'R', 0, 'O', 0,
    'D', 0, 'U', 0, 'C', 0, 'T', 0, '-', 0, 'I', 0, 'D', 0, '-', 0,
    'S', 0, 'T', 0, 'M', 0, '3', 0, '2', 0, ' ', 0, ' ', 0, ' ', 0
  };

uint8_t Virtual_Com_Port_StringSerial[VIRTUAL_COM_PORT_SIZ_STRING_SERIAL] =
  {
    VIRTUAL_COM_PORT_SIZ_STRING_SERIAL,           /* bLength */
    USB_STRING_DESCRIPTOR_TYPE,                   /* bDescriptorType */
    'S', 0, 'T', 0, 'M', 0, '3', 0, '2', 0, '1', 0, '0', 0
  };
#endif


//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)


void example_usb_vcp_start() {
    example_usb_vcp_init();

    //start your Tasks here

}
void example_usb_vcp_init() {
    ttc_gpio_init(TTC_LED2,tgm_output_push_pull,tgs_Max);
    ttc_usb_errorcode_e Error = ttc_usb_init(1);
    Assert_USB(Error == tusbe_OK, ec_InvalidConfiguration);
    ttc_usb_register_rx_function(1, USB_Handler_Data_Receive);

}

/*******************************************************************************
* Function Name  : USB_Handler_Data_Send.
* Description    : send data out to USB Bus.
* Input          : None.
* Return         : none.
*******************************************************************************/
u8_t debugLED_TTC_TX_value;
void USB_Handler_Data_Send(u8_t* data_buffer, u32_t Nb_bytes)
{

#ifdef USB_DEBUG_LED
    //ToDo: replace by TTC_UART
    debugLED_TTC_TX_value = 1- debugLED_TTC_TX_value;
    ttc_gpio_set(TTC_TX);
#endif

    ttc_usb_send_raw(1, data_buffer, Nb_bytes);
}

/*******************************************************************************
* Function Name  : USB_Handler_Data_Receive.
* Description    : receive data from USB Bus.
* Input          : data_buffer: data address.
                   Nb_bytes: number of bytes to receive.
* Return         : none.
*******************************************************************************/
u8_t debugLED_TTC_RX_value;
void USB_Handler_Data_Receive(u8_t* data_buffer, u8_t Nb_bytes)
{

#ifdef USB_DEBUG_LED
    //ToDo: replace by TTC_UART
    debugLED_TTC_RX_value = 1- debugLED_TTC_RX_value;
    ttc_gpio_set(TTC_RX);
#endif

#ifdef USB_Handler_Data_echo
    USB_Handler_Data_Send(data_buffer, Nb_bytes);
#endif


}


//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions

