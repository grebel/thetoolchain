#ifndef EXAMPLE_CAN_H
#define EXAMPLE_CAN_H

/** example_ttc_CAN.h **********************************************{
 *
 *                              The ToolChain
 *
 * Empty template for new header files.
 * Copy and adapt to your needs.
 *
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_can.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *  - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_can_start();

/** task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void task_CAN( void* TaskArgument );

/** interrupt function running this example
 *
 * @param CAN_Generic    structure containing configuration data of CAN which has received these bytes
 */
void exu_activity_rx( ttc_can_config_t* CAN_Generic );

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_CAN_H
