/*{ example_ttc_interrupt.c ************************************************
 
 Running Example for Interrupt handling.

 Demonstrates how to register an external input interrupt for a pin being
 connected to a switch. When the switch is pressed, the registered interrupt handler
 is called. Every call to interrupt handler will toggle state of corresponding led.

}*/

#include "example_ttc_interrupt.h"
//{ Global variables


// Note: Global and static variables require a unique prefix (here "eti_") to avoid
//       conflicting symbol names when combined with other sources!
ConfigLED_t eti_LED1;

#ifdef TTC_SWITCH2
ConfigLED_t eti_LED2;
#endif

//}
//{ Function definitions *************************************************

void example_ttc_interrupt_start() {
    //{ initialize + enable port + interrupt for external switch TTC_SWITCH1
    ttc_gpio_init(TTC_SWITCH1, tgm_input_floating);            // init port with default speed

    if (tine_OK == ttc_interrupt_init(tit_GPIO_Rising,
                                          ttc_gpio_create_index8(TTC_SWITCH1), // convert GPIO,PIN -> PhysicalIndex
                                          isr_Switch,
                                          &eti_LED1,  // will be passed as argument to isr_Switch()

                                         // will be loaded with data of previously registered ISR (if any)
                                          &(eti_LED1.NextISR),
                                          &(eti_LED1.NextISR_Argument)
                                      )
       )
    {
      ttc_interrupt_enable(tit_GPIO_Rising, ttc_gpio_create_index8(TTC_SWITCH1), TRUE);
    }
    ttc_gpio_init_variable(&eti_LED1.Port, TTC_LED1, tgm_output_push_pull);

#ifdef TTC_SWITCH2
    ttc_gpio_init(TTC_SWITCH2, tgm_input_floating);            // init port with default speed

    if (tine_OK == ttc_interrupt_init(tit_GPIO_Rising,
                                          ttc_gpio_create_index8(TTC_SWITCH2), // convert GPIO,PIN -> PhysicalIndex
                                          isr_Switch,
                                          &eti_LED2,  // will be passed as argument to isr_Switch()
                                          0, 0)
       )
    {
      ttc_interrupt_enable(tit_GPIO_Rising, ttc_gpio_create_index8(TTC_SWITCH2), TRUE);
    }
    ttc_gpio_init_variable(&eti_LED2.Port, TTC_LED2, tgm_output_push_pull);
#endif
}
void toggleLED(ConfigLED_t* LED) {
    u32_t TickCount = ttc_task_get_elapsed_usecs();
    if ( abs(TickCount - LED->LastToggle) > 1000) { // avoid flicker

        // invert LED state
        if (LED->IsOn) {
            LED->IsOn = FALSE;
            ttc_gpio_clr_variable(&(LED->Port));
        }
        else {
            LED->IsOn = TRUE;
            ttc_gpio_set_variable(&(LED->Port));
        }
        
        LED->LastToggle  = TickCount;
    }
}
void isr_Switch(physical_index_t PhysicalIndex, void* Argument) {
    ConfigLED_t* LED = (ConfigLED_t*) Argument;

    toggleLED(LED);
    if (LED->NextISR) // pass interrupt to next service routine (interrupt chaining)
        LED->NextISR(PhysicalIndex, LED->NextISR_Argument);
}

//}FunctionDefinitions
