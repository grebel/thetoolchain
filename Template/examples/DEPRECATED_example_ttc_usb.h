#ifndef EXAMPLE_USB_H
#define EXAMPLE_USB_H

/** example_ttc_USB.h **********************************************{
 *
 *                              The ToolChain
 *
 * Empty template for new header files.
 * Copy and adapt to your needs.
 *
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_usb.h"
#include"usb_desc.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_usb_t
    u8_t Foo;  // example data
    // more arguments...
} example_usb_t;

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *  - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_usb_start();

/** task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void task_USB(void *TaskArgument);


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_USB_H
