/** example_ttc_math.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple example code to demonstrate
 *  how to use ttc_math.
 *
 * Trignonometric functions always implement an approximation of the real result.
 * In low-power embedded applications, precision can be traded for faster computation.
 * The required precision is set in example_ttc_math_prepare(). The approximating
 * implementations of ttc_math will adapt to different precisions. The approximated
 * result of each function is checked to provide the correct precision.
 *
 * One may attach an oscilloscope probe to LED1 to measure runtime of all activated
 * benchmarks. Output pin TTC_LED1 is set while functions are benchmarked and cleared
 * for 100 ms afterwards.
 *
 *  The basic flow is as follows:
 *  (1) example_ttc_math_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_math_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 13 at 20150724 07:49:04 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_math.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_math.h"
#include "../ttc-lib/ttc_gpio.h"

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function Definitions *************************************************

#ifdef TTC_GFX1
#include "../ttc-lib/ttc_gfx.h"
#include "../ttc-lib/ttc_string.h"


A_define( t_u8, etm_String, 30 ); // buffer for temporary strings

// enable gfx macros
#define _etm_gfx_print_at(X, Y, String, MaxSize ) ttc_gfx_text_solid_at(X, Y, (const char*) String, MaxSize )

const t_u32 ets_ColorBg = TTC_GFX_COLOR24_WHITE;
const t_u32 ets_ColorFg = TTC_GFX_COLOR24_BLUE;

void _etm_gfx_init( ) {

    t_ttc_gfx_config* GFX = ttc_gfx_get_configuration( 1 );
    ttc_assert_address_writable_origin( GFX , ttc_assert_origin_auto );

    // set color to use for first screen clear
    ttc_gfx_color_bg24( ets_ColorBg );
    ttc_gfx_color_fg24( ets_ColorFg );

    ttc_gfx_init( 1 );
}
void _etm_gfx_printf1_at( t_s16 X, t_s16 Y, const char* Format, t_base Value ) {
    if ( X < 0 )
    { X = ttc_gfx_get_configuration( 1 )->TextColumns + X; }
    if ( Y < 0 )
    { Y = ttc_gfx_get_configuration( 1 )->TextRows + Y; }

    ttc_string_snprintf( etm_String.Data, etm_String.Size, Format, Value );
    _etm_gfx_print_at( X, Y, etm_String.Data, etm_String.Size );
}
void _etm_gfx_printf2_at( t_s16 X, t_s16 Y, const char* Format, t_base Value1, t_base Value2 ) {
    if ( X < 0 )
    { X = ttc_gfx_get_configuration( 1 )->TextColumns + X; }
    if ( Y < 0 )
    { Y = ttc_gfx_get_configuration( 1 )->TextRows + Y; }

    ttc_string_snprintf( etm_String.Data, etm_String.Size, Format, Value1, Value2 );
    _etm_gfx_print_at( X, Y, etm_String.Data, etm_String.Size );
}

#else

// disable gfx macros
#define _etm_gfx_init( )
#define _etm_gfx_printf1_at(X, Y, String, MaxSize )
#define _etm_gfx_printf2_at(X, Y, String, MaxSize )

#endif

void _example_ttc_math_task( void* Argument ) {
    example_math_t* ConfigMath = ( example_math_t* ) Argument;
    ( void ) ConfigMath; // not yet used

    while ( TTC_TASK_SCHEDULER_AVAILABLE ) {

#warning ToDo: implement some nice mathematical example code
        // Note: Previous example grew too big and has moved into 650_regression_ttc_math!

        ttc_task_yield(); // give cpu to other tasks
    }
}

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_math_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    // initialize gfx display (if available)
    _etm_gfx_init( );

    // Set precision of all approximation algorithms.
    // Results will be approximated +- precision.
    // Check description of low-level driver for benchmark results!

    static example_math_t ConfigMath;
    ConfigMath.Precision = ttc_math_set_precision( tmp_1m );
    _etm_gfx_printf1_at( 0, 0, "TTC_Math P=%dm", ConfigMath.Precision * 1000 );


    ttc_task_create( _example_ttc_math_task,
                     "math",
                     8192,
                     &ConfigMath,
                     1,
                     NULL
                   );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}Function Definitions
