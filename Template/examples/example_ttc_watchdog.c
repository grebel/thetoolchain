/*{ example_ttc_watchdog.c ************************************************

  This example demonstrates usage of hardware watchdog.
  After initialisation, the watchdog must be reset periodically.
  If ttc_watchdog_reload1() is not called within the set time window, the hardware watchdog
  will cause a hardware reset.

  taskWatchdogTest() first sends out a burst of 50ms LED flashes to indicate its startup.
  Then it initializes the watchdog and reloads it every 300ms.
  The reloads are pause while TTC_SWITCH1 is pressed. If the switch is pressed too long, the
  watchdog will cause a reset and taskWatchdogTest() is created again.

}*/

#include "example_ttc_watchdog.h"
#include "../compile_options.h"

//{ Global variables

// Note: Global and static variables require a unique prefix (here etw_) to avoid
//       conflicting symbol names when combined with other sources!

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_watchdog_prepare() {

#ifndef TTC_SWITCH1
#  error Missing definition for TTC_SWITCH1 !
#endif
    ttc_gpio_init(TTC_SWITCH1, E_ttc_gpio_mode_input_floating);            // init port with default speed
    ttc_gpio_init(TTC_LED1,    E_ttc_gpio_mode_output_push_pull);
    ttc_gpio_clr(TTC_LED1);

    for (t_u8 I = 20; I > 0; I--) { // inidicate task start
      ttc_gpio_clr(TTC_LED1);
      ttc_task_msleep(50);
      ttc_gpio_set(TTC_LED1);
      ttc_task_msleep(50);
    }

    ttc_task_create( taskWatchdogTest, // function to start as thread
                     "WatchdogTest",   // thread name (just for debugging)
                     128,              // stack size
                     NULL,             // passed as argument to taskBrightness()
                     1,                // task priority (higher values mean more process time)
                     NULL              // can return a handle to created task
                     );
}
void taskWatchdogTest(void* Arg) {
    (void) Arg; // avoid warning unused argument

    // assume start-value as switch-off value
    bool SwitchOff = ttc_gpio_get(TTC_SWITCH1);

    ttc_watchdog_init1(0,0);

    while (TTC_TASK_SCHEDULER_AVAILABLE) {
        ttc_gpio_clr(TTC_LED1);
        ttc_watchdog_reload1();
        ttc_task_msleep(300);
        ttc_gpio_set(TTC_LED1);
        ttc_watchdog_reload1();
        ttc_task_msleep(300);

        while (SwitchOff != ttc_gpio_get(TTC_SWITCH1))
            ttc_task_msleep(100); // sleep while switch is pressed (will cause watchdog reset after a while!)
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
