/*{ TemplateName::.c ************************************************
 
 Empty template for new c-files.
 Copy and adapt to your needs.
 
}*/

#include "example_channel.h"

//{ Global variables *************************************************

const u8_t* TextHello = (const u8_t*) "Hello world!\n";

#define EXC_USART_INDEX 1                    // USART unit to use on current board
#define EXC_SPI_INDEX 1                      // SPI unit to use on current board
#define EXC_I2C_INDEX 1                      // I2C unit to use on current board
#define EXC_RADIO_INDEX 1                    // RADIO unit to use on current board

u8_t Channel1 = 0;

//}Global Variables
//{ Function definitions *************************************************

void example_channel_init() {
    ttc_usart_config_t* Config = ttc_usart_get_configuration(EXC_USART_INDEX);
    Config->BaudRate = 115200;
    Config->Flags.Bits.DelayedTransmits = 0;
    ttc_usart_init(EXC_USART_INDEX);

    // create first channel
    Channel1 = ttc_channel_create();

    // add ourself as a node to Channel1
    ttc_channel_add_application(Channel1, channel_receive_data);

    // add USART #1 as a node to Channel1
    ttc_channel_add_device(Channel1, tcnt_USART, EXC_USART_INDEX);

    // use Channel1 for stdout functions
    ttc_channel_stdout_set(Channel1);

    // set ttc_channel as stdout interface
    ttc_string_register_stdout(ttc_channel_stdout_send_block, ttc_channel_stdout_send_string);
}
void example_channel_start() {
    example_channel_init();

    ttc_task_create(task_channel_echoes,                    // function to start as thread
                "CHANNEL",                           // thread name (just for debugging)
                (size_t) 512,                      // stack size
                (void *) NULL,                     // passed as argument to taskLEDs()
                (unsigned portBASE_TYPE) 1,        // task priority (higher values mean more process time)
                NULL                               // can return a handle to created task
                );
}
void task_channel_echoes(void *TaskArgument) {
    TaskArgument = TaskArgument;      // avoids warning: unused parameter 'TaskArgument'

    u8_t Count = 0;

    while (1) {

        // make use of stdout setting via ttc_string_printf()
        printf("example_channel: %i=0x%x\n", Count, Count);
        Count++;

        // zero copying (string is declared as constant)
        // function queues string for transmit and returns immediately
        ttc_channel_send_string_const(Channel1, TextHello, -1);

        /*
        // waits until all bytes have been transmitted (optional)
        ttc_channel_flush_tx(CHANNEL1_INDEX);

        // function will wait for all queued strings and return after sending this string
        ttc_channel_send_string_const(CHANNEL1_INDEX, " ...", -1);

        ttc_task_check_stack(); // place a breakpoint inside to see real stack-usage; will block forever if stack has overflown
*/
        ttc_task_msleep(1000);
    }
}
ttc_heap_block_t* channel_receive_data(ttc_channel_node_handle_t SourceNode, ttc_heap_block_t* Data) {
    //Assert(SourceNode != NULL, ec_InvalidArgument);
    Assert(Data       != NULL, ec_InvalidArgument);


    return Data; // memory block can now be reused
}

//}FunctionDefinitions
