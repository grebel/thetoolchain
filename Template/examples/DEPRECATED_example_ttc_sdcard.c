/** example_ttc_sdcard.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2016.
 *
 *  This source should provide a small and simple example code to demonstrate 
 *  how to use ttc_sdcard.
 *
 *  The basic flow is as follows:
 *  (1) example_ttc_sdcard_start() gets called in single tasking mode from 
 *      ttc_extensions.c:startExtensions() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_sdcard_start() gets executed
 *
 *  Created from template example_ttc_device.c revision 13 at 20150521 11:03:30 UTC
 *
 *  Authors: <AUTHOR>
 *  
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "example_ttc_sdcard.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_sdcard.h"

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_sdcard_start() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _task_SDCARD(void *Argument);

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function Definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void _task_SDCARD(void *Argument) {
    Assert(ttc_memory_is_writable(Argument), ec_NULL);                  // always check pointer arguments with Assert()!
    example_sdcard_t* TaskArguments = (example_sdcard_t*) Argument; // we're expecting this pointer type

    while (1) {
      // do something...
      ttc_task_msleep(100);    // always sleep for a while to give cpu time to other tasks
    }
}
//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_sdcard_start() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    // Note: Adding a unique prefix to all global and static variable makes it
    //       easier to get an overview of memory usage via _/calculateMemoryUsage.pl
    static example_sdcard_t et_sdcard_Config;
    
    et_sdcard_Config.Foo = 1; // set some arguments to be passed to task

   ttc_task_create( _task_SDCARD,        // function to start as thread
                    "tSDCARD",          // thread name (just for debugging)
                    128,                  // stack size (adjust to amount of local variables of _task_SDCARD() AND ALL ITS CALLED FUNTIONS!) 
                    &et_sdcard_Config,  // passed as argument to _task_SDCARD()
                    1,                    // task priority (higher values mean more process time)
                    NULL                  // can return a handle to created task
                  );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}Function Definitions
