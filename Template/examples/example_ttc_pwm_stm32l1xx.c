/** example_ttc_pwm_stm32l1xx.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for pulse width modulation output
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_pwm_stm32l1xx_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_pwm_stm32l1xx_prepare() get executed
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_pwm_stm32l1xx_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template example_ttc_device_architecture.c revision 18 at 20180502 11:57:47 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_pwm_stm32l1xx.h"
#include "../ttc-lib/ttc_pwm.h" // high-level driver of device to use
#include "../ttc-lib/pwm/pwm_stm32l1xx.h" // direct access to low-level driver
#include "../ttc-lib/ttc_heap.h"     // dynamic memory allocations during runtime
#include "../ttc-lib/ttc_task.h"     // allows to use application in single- and multitasking setup

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_pwm_stm32l1xx_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _example_pwm_stm32l1xx_task(void *Argument);

/** Statemachine which may be used in single- and multitasking configuration.
 *
 * If no multitasking is active, all registered tasks are called one after another in an endless loop from 
 * ttc_task_start_scheduler(). Each task should then act like a statemachine and return immediately.
 * Implementing your example as a statemachine allows to use it in multi- and singletasking configuration.
 *
 * This function is not required if your example will only run with an enabled multitasking scheduler. 
 */
void _example_pwm_stm32l1xx_statemachine(void *Argument);

//}Private Function Declarations
//{ Function Definitions *************************************************

void example_ttc_pwm_stm32l1xx_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    if (1) { // create one instance of your example (copy and adjust for multiple instances) 
      
      // LogicalIndex = 1: use device defined as TTC_PWM1 in makefile
      t_u8 LogicalIndex = 1;

      // allocate data of instance as local static (allows compiler to detect if it fits into RAM)
      static example_pwm_stm32l1xx_data_t Data_pwm_stm32l1xx_1;
      
      // reset data
      ttc_memory_set(&Data_pwm_stm32l1xx_1, 0, sizeof(Data_pwm_stm32l1xx_1));
      
      // loading configuration of first pwm device
      Data_pwm_stm32l1xx_1.Config_PWM = ttc_pwm_get_configuration(LogicalIndex);
      
      // change configuration before initializing device
      // Data_pwm_stm32l1xx_1.Config_PWM->  = ;
      //...
      
      // initializing our pwm device here makes debugging easier (still single-tasking)
      // Logical index is stored inside each device configuration
      ttc_pwm_init(LogicalIndex);
      
      // Register at least one instance of a function to be run after all system has finished booting.
      // ttc_task_create() works in single- and multitasking environments.
      ttc_task_create(
#if (TTC_TASK_SCHEDULER_AVAILABLE)
                      _example_pwm_stm32l1xx_task,         // function to start as thread
#else
                      _example_pwm_stm32l1xx_statemachine, // function to run periodically (single tasking)
#endif
                       "tPWM_STM32L1XX",          // thread name (just for debugging)
                       256,                                 // stack size (adjust to amount of local variables of _example_pwm_stm32l1xx_task() AND ALL ITS CALLED FUNTIONS!) 
                       &Data_pwm_stm32l1xx_1,     // passed as argument to _example_pwm_stm32l1xx_task()
                       1,                                   // task priority (higher values mean more process time)
                       NULL                                 // can return a handle to created task
                     );
    }
}

// Note: If you implement both of the example functions below, your example will run in single- and multitasking configuration.

void _example_pwm_stm32l1xx_statemachine(void *Argument) {
    Assert_PWM_Writable(Argument, ttc_assert_origin_auto);                  // always check pointer arguments with Assert() before dereferencing them!
    example_pwm_stm32l1xx_data_t* Data = ( example_pwm_stm32l1xx_data_t* ) Argument; // we're expecting this pointer type

    // get pointer to device configuration
    t_ttc_pwm_config* Config_PWM = Data->Config_PWM;
  
    if ( ttc_systick_delay_expired( & Data->Delay ) ) { // example periodic delay
        ttc_systick_delay_init( & Data->Delay, 
                                ttc_systick_delay_get_minimum() << 4 // minimum delay * 16 is a safe way to always use a small and valid delay   
                              ); // set new delay time

      // implement statemachine here ...
    }
    
    // implement statemachine here ...
}
void _example_pwm_stm32l1xx_task(void *Argument) {         // example task will be run when multitasking scheduler (e.g. FreeRTOS) has been started
    Assert_PWM_Writable(Argument, ttc_assert_origin_auto);                  // always check pointer arguments with Assert() before dereferencing them!
    example_pwm_stm32l1xx_data_t* Data = ( example_pwm_stm32l1xx_data_t* ) Argument; // we're expecting this pointer type

    // get pointer to device configuration (remove if not required)
    t_ttc_pwm_config* Config_PWM = Data->Config_PWM;

    do { 

        // call statemachine or implement task here...
        _example_pwm_stm32l1xx_statemachine(Argument);
        
        ttc_task_yield(); // give cpu to other processes
    } while (1); // Tasks run endlessly.
}

//}Function Definitions
