#ifndef EXAMPLE_TTC_RADIO_RANGING_H
#define EXAMPLE_TTC_RADIO_RANGING_H

/** example_ttc_RADIO.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_radio.
 *  The basic flow is as follows:
 *  (1) example_ttc_radio_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_radio_prepare() gets executed
 *
 *  Created from template example_ttc_device.h revision 11 at 20140514 03:13:59 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"         // basic functions and definitions for all supported architectures
#include "../ttc-lib/ttc_radio_types.h"   // datatypes for all supported radio transceivers

//} Includes
//{ Defines/ TypeDefs ****************************************************

//} Defines
//{ Structures/ Enums ****************************************************

//} Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_radio_ranging_prepare();

/** task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void example_ttc_radio_ranging_mainTask( void* TaskArgument );

#ifndef TTC_RADIO1_PIN_SPI_NSS
#  ifdef TTC_SPI1_NSS
// use nSS pin of first spi device as default
#    warning Missing gpio pin definition TTC_RADIO1_PIN_SPI_NSS. Using pin from TTC_SPI1
#    define TTC_RADIO1_PIN_SPI_NSS TTC_SPI1_NSS
#  endif
#endif

//} Function prototypes

#endif //EXAMPLE_TTC_RADIO_RANGING_H
