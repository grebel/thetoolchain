/** { example_ttc_radio_ranging_mobile.c ************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.c revision 11 at 20161130 06:10:10 UTC
 *
 *  Authors: gregor
}*/

/** { Includes ********************************************************
 *
 * Put all includes here that are required to compile this source code.
 */

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_radio_ranging_mobile.h"
#include "example_ttc_radio_ranging_common.h"
#include "../ttc-lib/ttc_radio.h"
#include "../ttc-lib/ttc_string.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Global Variables ************************************************
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** { Private Function Declarations ***********************************
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** { Function Definitions ********************************************
 */

void example_ttc_radio_ranging_prepare_mobile( etr_task_info_t* TaskInfo ) {

    // receive ACKs + beacons + broadcasts + packets targeted at us
    ttc_radio_configure_frame_filter( 1, ttc_radio_frame_filter_sensor );

    ttc_task_create(
        example_ttc_radio_ranging_task_mobile, // function to start as thread
        "tMobile",                  // thread name (just for debugging)
        512,                        // stack size
        TaskInfo,                   // passed as argument to example_ttc_radio_ranging_mainTask()
        1,                          // task priority (higher values mean more process time)
        NULL                        // can return a handle to created task
    );
}
void example_ttc_radio_ranging_task_mobile( void* TaskArgument ) { // implementation of sensor node task
    etr_task_info_t* TaskInfo = ( etr_task_info_t* ) TaskArgument;

    example_ttc_radio_ranging_usart_send_stringf( "starting sensor node #%d ", TaskInfo->ConfigRadio->LocalID.Address16 );
    example_ttc_radio_ranging_usart_send_stringf( "on channel %d\n", TaskInfo->ConfigRadio->ChannelRX );

    // calculate size of buffer that can store biggest packet + META header
    t_u16 BufferSize = sizeof( t_ttc_packet_meta ) + ttc_radio_get_configuration( 1 )->Init.MaxPacketSize;

    // ttc_packet only accepts memory buffers from a memory pool
    // create a pool for some data-packets
    t_ttc_heap_pool* Pool_Data = ttc_heap_pool_create( BufferSize, 2 );

    // Initialize all buffers in this pool to same type
    // ttc_radio will return all blocks to their original pool. So we don't have to
    // reinitialize the blocks later.
    example_ttc_radio_ranging_initialize_pool( Pool_Data, E_ttc_packet_type_802154_Data_011010 );


    const t_u16 Interleave = 100; // amount of while loops to pass until a new packet is send
    t_u16 Count = 0;
    t_u16 MessageCounter = 0;
    t_ttc_packet_address TargetID;

    // declaring array static to reduce stack usage
    // (compiler can check if static variables will fit into RAM)
    static etrr_measure_statistics_t MeasureStatistics[EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS];
    for ( t_u8 Index = 0; Index < EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS; Index++ ) {
        MeasureStatistics[Index].LowerDistanceCM  = -1;
        MeasureStatistics[Index].UpperDistanceCM  = 0;
        MeasureStatistics[Index].MedianDistanceCM = 0;
        MeasureStatistics[Index].AvgDistanceCM    = 0;
    }
    static t_ttc_radio_distance Distances[EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS];


    while ( 1 ) {
        if ( 1 && ( Count-- == 0 ) ) { // become active
            Count = Interleave;

            if ( 0 ) { // send broadcast message to everybody who is just listening
                ttc_string_snprintf( TaskInfo->PrepareBuffer, TaskInfo->PrepareBuffer_Size, "Hello World #%i", MessageCounter++ );
                TargetID.Address16 = 0xffff; // broadcast address

                example_ttc_radio_ranging_transmit( TaskInfo,
                                                    TaskInfo->PrepareBuffer,
                                                    ttc_string_length8( ( char* ) TaskInfo->PrepareBuffer, TaskInfo->PrepareBuffer_Size ) + 1, // transmit string + string terminator
                                                    &TargetID,
                                                    E_ttc_packet_type_802154_Data_001010,
                                                    &( TaskInfo->StringBuffer[0] ),
                                                    SIZE_ETR_STRINGBUFFER
                                                  );
            }
            if ( 1 ) { // measure range to one of our neighbours
                TargetID.Address16 = 0xffff;        // broadcast address
                TargetID.PanID     = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID;  // looking for remote nodes in same PAN

                for ( t_u8 AnchorIndex = 0; AnchorIndex < EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS; AnchorIndex++ ) { // reset statistics
                    MeasureStatistics[AnchorIndex].AvgDistanceCM = 0;
                }
                for ( t_u16 Retries = 100; Retries > 0; Retries -- ) {
                    // Single Sided Time Of Flight (SSTOF) is the most simple ranging method
                    // We send a broadcast ranging request and expect every anchor to answer it
                    volatile static t_u8 AmountRepliesRequired = EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS;

                    t_u8 AmountReplies = ttc_radio_ranging_request( 1,
                                                                    rcrt_Request_Ranging_SSTOFCR,
                                                                    AmountRepliesRequired,
                                                                    &TargetID,
                                                                    1000,
                                                                    0,
                                                                    Distances
                                                                  );

                    // obtain pointers to first entry in both arrays
                    etrr_measure_statistics_t* MeasureStatistic = MeasureStatistics;
                    t_ttc_radio_distance*      Distance         = Distances;

                    for ( t_u8 RemoteIndex = 0; RemoteIndex < AmountReplies; RemoteIndex++ ) {

                        if ( !MeasureStatistic->AvgDistanceCM ) {
                            MeasureStatistic->AvgDistanceCM   = Distance->Distance_cm;
                            MeasureStatistic->LowerDistanceCM = Distance->Distance_cm;
                            MeasureStatistic->UpperDistanceCM = Distance->Distance_cm;
                        }
                        else {
                            if ( MeasureStatistic->LowerDistanceCM > Distance->Distance_cm )
                            { MeasureStatistic->LowerDistanceCM = Distance->Distance_cm; }
                            if ( MeasureStatistic->UpperDistanceCM < Distance->Distance_cm )
                            { MeasureStatistic->UpperDistanceCM = Distance->Distance_cm; }

                            MeasureStatistic->MedianDistanceCM = MeasureStatistic->LowerDistanceCM / 2 + MeasureStatistic->UpperDistanceCM / 2;
                            MeasureStatistic->AvgDistanceCM    = MeasureStatistic->AvgDistanceCM   / 2 + Distance->Distance_cm / 2;
                        }
                    }
                }
                for ( t_u8 AnchorIndex = 0; AnchorIndex < EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS; AnchorIndex++ ) { // reset statistics
                    if ( MeasureStatistics[AnchorIndex].AvgDistanceCM ) { // got a range measure to this anchor: display it
                        example_ttc_radio_ranging_usart_send_stringf( "Range(%i):",     AnchorIndex );
                        example_ttc_radio_ranging_usart_send_stringf( "AVG=%i cm ",     MeasureStatistics[AnchorIndex].AvgDistanceCM );
                        example_ttc_radio_ranging_usart_send_stringf( "(Max-Min=%i)\n", MeasureStatistics[AnchorIndex].UpperDistanceCM - MeasureStatistics[AnchorIndex].LowerDistanceCM );
                    }
                }
            }
            if ( 1 )
            { ttc_radio_receiver( 1, 1, 0, 0 ); }  // enable receiver to scan for incoming packets (will wait until all queued packets have been transmitted)
        }
        t_ttc_packet* ReceivedPacket = ttc_radio_packet_received_tryget( 1 );
        if ( ReceivedPacket ) { // somebody wants to talk to us
            if ( 1 )
            { ttc_radio_receiver( 1, 1, 0, 0 ); }  // re-enable receiver (is automatically disabled after receiving frame)

            if ( 1 ) { // debug received packet via usart
                t_u8* Append = &( TaskInfo->StringBuffer[0] );
                t_base StringBufferFree = SIZE_ETR_STRINGBUFFER;
                Append += ttc_string_appendf( Append, &StringBufferFree, "(0x%04x) RX: ", TaskInfo->LocalNodeID->Address16 );
                Append += ttc_packet_debug( ReceivedPacket, Append, &StringBufferFree, TaskInfo->PacketDebug );
                Append += ttc_string_appendf( Append, &StringBufferFree, "\n" );
                example_ttc_radio_ranging_usart_send_string( ( char* ) & ( TaskInfo->StringBuffer[0] ), SIZE_ETR_STRINGBUFFER );
            }

            // return packet memory buffer to radio for reuse
            ttc_radio_packet_release( ReceivedPacket );
        }

        ttc_radio_maintenance( 1 ); // let radio cleanup buffers and check transceiver state

        if ( 1 ) { ttc_task_msleep( 1 ); }
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
