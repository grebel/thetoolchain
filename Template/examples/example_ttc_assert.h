#ifndef EXAMPLE_ASSERT_H
#define EXAMPLE_ASSERT_H

/** example_ttc_ASSERT.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for central self testing and error reporting
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_assert_none_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_assert_none_prepare() get executed
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_assert_none_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template example_ttc_device.h revision 16 at 20161107 21:10:10 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_ASSERT.c only do not belong here!
#include "../ttc-lib/ttc_basic.h"      // basic datatypes and definitions
#include "../ttc-lib/ttc_systick.h"    // exact delays and timeouts
#include "../ttc-lib/ttc_task_types.h" // required for task delays
#include "../ttc-lib/ttc_gpio.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_assert_t
    t_u8           Toggle; // 1 or 0
    e_ttc_gpio_pin LED;    // !=0: gpio pin connected to Status LED

    // more arguments...
} example_assert_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_assert_prepare();


//}Function prototypes

#endif //EXAMPLE_ASSERT_H
