/** example_ttc_usart.c **********************************************{
 *
 *                             The ToolChain
 *
 *
 * example_ttc_usart_prepare()
 *     fills et_usart_TaskArguments with data
 *     spawns thread task_USART()
 *     returns immediately
 *
 * task_USART()
 *     while (1) {
 *       do something
 *       ttc_task_msleep(100);
 *     }
 *
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_usart.h"
#include "../ttc-lib/ttc_string.h"
//{ Global variables


const char* TextHello = "Hello, talk to me!\n";
#define SIZE_YouSaid 20
char YouSaid1[SIZE_YouSaid];

BOOL UsartAvailable[2] = {FALSE, FALSE};

#define USART1_INDEX 1                    // using first  USART available on current board
#define USART2_INDEX 2

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_usart_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */
    t_u8 MaxUSART = ttc_usart_get_max_logicalindex();
    Assert( MaxUSART > 0, ttc_assert_origin_auto ); // no USARTs defined by board makefile!
    e_ttc_usart_errorcode Error;

    #ifdef TTC_LED1
    ttc_gpio_init( TTC_LED1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    #endif
    #ifdef TTC_LED2
    ttc_gpio_init( TTC_LED2, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    ttc_gpio_clr( TTC_LED2 );
    #endif

    if ( USART1_INDEX <= MaxUSART ) { // USART defined by board: initialize it
        t_ttc_usart_config* Config = ttc_usart_get_configuration( USART1_INDEX );
        Assert_Writable( Config, ttc_assert_origin_auto );

        // change some settings
        Config->Init.Flags.IrqOnRxNE = 1; // enable IRQ on Rx-buffer not empty
        Config->Init.Char_EndOfLine = 13;      // packets end at carriage return
        Config->Init.Flags.Receive = 1;

        /* IF we want tu use parity we will have to put WordLenght = 9 */
        Config->Init.WordLength = 9;
        Config->Init.BaudRate = 115200;
        Config->Init.HalfStopBits = 2;

        /* Configure Parity */
        Config->Init.Flags.ParityEven = 1;
        Config->Init.Flags.ControlParity = 1;
        Config->Init.Flags.ParityOdd = 0;
        Config->Init.Flags.TransmitParity = 1;
        Config->Init.Flags.SendBreaks = 0;

        if ( 0 )
        { Config->Init.Flags.DelayedTransmits = 1; } // all transmits are queued and sent in interrupt service routin parallel to this task
        else
        { Config->Init.Flags.DelayedTransmits = 0; } // all transmits are blocking until data is send (can loose rx-data while waiting for transmit!)

        // register activity indicators (will be called from interrupt service routine!)
        Config->Init.activity_rx_isr = exu_activity_rx;
        Config->Init.activity_tx_isr = exu_activity_tx;

        Error = ttc_usart_init( USART1_INDEX ); // note: first bytes might get lost until ttc_usart_register_receive()
        Assert( !Error, ttc_assert_origin_auto ); // error occured during init
        if ( Config->Init.Flags.Receive ) // usart implementation can receive data: register our receice function
        { ttc_usart_register_receive( USART1_INDEX, eu_receiveData, ( void* ) USART1_INDEX ); }
        UsartAvailable[0] = TRUE;
    }

    // set first logical USART as standard output (otherwise last initialized one would be used)
    ttc_usart_stdout_set( USART1_INDEX );

    // set usart as stdout interface
    // ttc_string_register_stdout(ttc_channel_usart_send_block, ttc_usart_stdout_send_string);
    ttc_string_register_stdout( ttc_usart_stdout_send_block, ttc_usart_stdout_send_string );


    ttc_task_create( task_USART,    // function to start as thread
                     "tUSART",       // thread name (just for debugging)
                     256,            // stack size
                     NULL,           // passed as argument to taskLEDs()
                     1,              // task priority (higher values mean more process time)
                     NULL            // can return a handle to created task
                   );
}
void task_USART( void* TaskArgument ) {
    TaskArgument = TaskArgument;      // avoids warning: unused parameter 'TaskArgument'

    t_u8 Count = 0;
    t_ttc_heap_block ReceiveBuffer;
    ReceiveBuffer.Buffer  = ( t_u8* ) YouSaid1;
    ReceiveBuffer.MaxSize = SIZE_YouSaid;
    t_ttc_usart_config* Config = ttc_usart_get_configuration( USART1_INDEX );

    // variables allow delay until feature in single- and multitasking setup
    t_base NextTime = ttc_systick_get_elapsed_usecs() + 1;
    t_base PreviousTime = NextTime;

    while ( TTC_TASK_SCHEDULER_AVAILABLE ) {
        if ( PreviousTime > NextTime ) { // time stamp experienced overrun: test for counter overrun
            if ( ttc_systick_get_elapsed_usecs() < NextTime ) // system clock overrun occured
            { PreviousTime = NextTime; }
        }
        else if ( NextTime < ttc_systick_get_elapsed_usecs() ) { // delay passed: send hello world
            PreviousTime = NextTime;
            NextTime += 500000; // next action in 500ms (variable will overrun somewhen)

            // make use of stdout setting via ttc_string_printf()
            printf( "example_usart: %i=0x%x ", Count, Count );
            Count++;

            // zero copying (string is declared as constant)
            // function queues string for transmit and returns immediately
            ttc_usart_send_string_const( USART1_INDEX, TextHello, -1 );


            // waits until all bytes have been transmitted (optional)
            ttc_usart_flush_tx( USART1_INDEX );

            // function will wait for all queued strings and return after sending this string
            ttc_usart_send_string_const( USART1_INDEX, " ...", -1 );

            if ( !Config->Init.Flags.IrqOnRxNE ) { // no receive interrupts available: poll for incoming bytes
                ReceiveBuffer.Size = 0;
                t_u8* Writer = ReceiveBuffer.Buffer;
                while ( ( ttc_usart_read_byte( USART1_INDEX, Writer++ ) == ec_usart_OK ) && // Word received
                        ( ++ReceiveBuffer.Size < ReceiveBuffer.MaxSize )                // buffer not full
                      ) {
                    if ( *( Writer - 1 ) == 0xa ) // LF found
                    { break; }
                }
                if ( ReceiveBuffer.Size ) // received some bytes: process them
                { eu_receiveData( ( void* ) USART1_INDEX, NULL, &ReceiveBuffer ); }
            }
            ttc_task_check_stack(); // place a breakpoint inside to see real stack-usage; will block forever if stack has overflown
        }
        ttc_task_yield(); // if scheduler available, give cpu to other processes
    }
}
t_ttc_heap_block* eu_receiveData( void* Argument, t_ttc_usart_config* USART_Generic, t_ttc_heap_block* Data ) {
    ( void ) USART_Generic;
    ( void ) Argument;
    Assert( Data          != NULL, ttc_assert_origin_auto );

    if ( Data->Size < SIZE_YouSaid - 1 ) {
        ttc_memory_copy( YouSaid1, Data->Buffer, Data->Size );
        Data->Buffer[Data->Size] = 0; // terminate string
    }
    else {
        ttc_memory_copy( YouSaid1, Data->Buffer, SIZE_YouSaid - 1 );
        Data->Buffer[SIZE_YouSaid - 1] = 0; // terminate string
    }

    if ( 1 ) { // example implementation: just echoing back what we received
        t_u8* StrippedBuffer = Data->Buffer;
        t_base StrippedSize  = Data->Size;
        ttc_string_strip( &StrippedBuffer, &StrippedSize );
        ttc_usart_send_string_const( USART1_INDEX, "relaying: '", -1 );
        ttc_usart_send_raw( USART1_INDEX, StrippedBuffer, StrippedSize ); // not constant: data will be copied
        ttc_usart_send_string_const( USART1_INDEX, "'\n", -1 );
    }

    return Data; // memory block must be returned for reuse


}
void exu_activity_tx( t_ttc_usart_config* USART_Generic, t_u8 Byte ) {
    ( void ) USART_Generic;
    ( void ) Byte;

    #ifdef TTC_LED1
    static t_u8 StateTX = 0;
    if ( StateTX++ & 1 )
    { ttc_gpio_set( TTC_LED1 ); }
    else
    { ttc_gpio_clr( TTC_LED1 ); }
    #endif
}
void exu_activity_rx( t_ttc_usart_config* USART_Generic, t_u8 Byte ) {
    ( void ) USART_Generic;
    ( void ) Byte;

    #ifdef TTC_LED2
    static t_u8 StateRX = 0;
    if ( StateRX++ & 1 )
    { ttc_gpio_set( TTC_LED2 ); }
    else
    { ttc_gpio_clr( TTC_LED2 ); }
    #endif
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
