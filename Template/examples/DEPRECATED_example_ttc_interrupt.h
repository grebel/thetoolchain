#ifndef EXAMPLE_INTERRUPT
#define EXAMPLE_INTERRUPT

/*{ example_interrupt::.h ************************************************
 
 Running Example for Interrupt handling.

 Demonstrates how to register an external input interrupt for a pin being
 connected to a switch. When the switch is pressed, the registered interrupt handler
 is called. Every call to interrupt handler will toggle state of led connected to TTC_LED1.
 
}*/
//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gpio.h"
#ifdef EXTENSION_500_ttc_task
#  include "../ttc-lib/ttc_task.h"
#endif
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_interrupt.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

#ifndef TTC_LED1
  #error TTC_LED1 not defined!

  // Example header-definition:
  // #define TTC_LED2   PIN_PC6    // status led 1 on Olimex proto board P107
  //
  // Example makefile-definition:
  // COMPILE_OPTS += -DTTC_LED2=PIN_PC6
#endif

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // ConfigLED_t
  ttc_Port_t Port;         // port where LED is connected
  BOOL       IsOn;         // == TRUE: LED is enlighted
  u32_t      LastToggle;   // time when LED has been toggled last time

  // Interrupt Chaining
  void (*NextISR)(physical_index_t, void*);
  void* NextISR_Argument;
} ConfigLED_t;

//}Structures/ Enums
//{ Function declarations **************************************************

// main entry: initializes hardware
// returns immediately
void example_ttc_interrupt_start();

// toggles current state of pin TTC_LED1
void toggleLED(ConfigLED_t* LED);

// interrupt service routine: automatically called when switch is pressed
void isr_Switch(physical_index_t PhysicalIndex, void* Argument);

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_INTERRUPT
