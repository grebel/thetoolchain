#ifndef EXAMPLE_FILESYSTEM_DOSFS_H
#define EXAMPLE_FILESYSTEM_DOSFS_H

/** example_ttc_FILESYSTEM_DOSFS.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_filesystem_dosfs.
 *  The basic flow is as follows:
 *  (1) example_ttc_filesystem_dosfs_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_filesystem_dosfs_prepare() gets executed
 *
 *  Created from template example_ttc_device_architecture.h revision 16 at 20180413 11:21:12 UTC
 *
 *  Authors: <AUTHOR>
 *  
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_FILESYSTEM.c only do not belong here! 
// This is why header files typically include *_types.h files only (avoids nasty include loops).

#include "../ttc-lib/ttc_basic_types.h"    // basic datatypes for current microcontroller architecture
#include "../ttc-lib/ttc_gpio_types.h"     // allows to store gpio pins in structures 
#include "../ttc-lib/ttc_task_types.h"     // allows to store task delay data in structures
#include "../ttc-lib/ttc_filesystem_types.h" // datatypes of filesystem devices
#include "../ttc-lib/filesystem/filesystem_dosfs_types.h" // datatypes of dosfs filesystem devices

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_filesystem_dosfs_t
    t_ttc_filesystem_config* Config_FILESYSTEM; // pointer to configuration of filesystem device to use
    t_ttc_systick_delay   Delay;            // this type of delay is usable in single- and multitasking setup

    // more arguments...
} example_filesystem_dosfs_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_filesystem_dosfs_prepare();


//}Function prototypes

#endif //EXAMPLE_FILESYSTEM_DOSFS_H
