/** example_ttc_network_layer.c **********************************************{
 *
 *                             The ToolChain
 *
 *
 * example_ttc_network_layer_prepare()
 *     fills et_network_layer_TaskArguments with data
 *     spawns thread task_NETWORK_LAYER()
 *     returns immediately
 *
 * task_NETWORK_LAYER()
 *     while (1) {
 *       do something
 *       ttc_task_msleep(100);
 *     }
 *
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_network_layer.h"

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
example_network_layer_t et_network_layer_TaskArguments;

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_network_layer_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */
   et_network_layer_TaskArguments.Foo = 1; // set some arguments to be passed to task

   ttc_task_create( task_NETWORK_LAYER,               // function to start as thread
                    "tNETWORK_LAYER",                 // thread name (just for debugging)
                    128,                         // stack size
                    &et_network_layer_TaskArguments,  // passed as argument to task_NETWORK_LAYER()
                    1,                           // task priority (higher values mean more process time)
                    NULL                         // can return a handle to created task
                  );
}
void task_NETWORK_LAYER(void *TaskArgument) {
    Assert(TaskArgument, ttc_assert_origin_auto);                  // always check pointer arguments with Assert()!
    example_network_layer_t* TaskArguments = (example_network_layer_t*) TaskArgument; // we're expecting this pointer type

    while (1) {
      // do something...
      ttc_task_msleep(100);    // always sleep for a while to give cpu time to other tasks
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
