/** example_ttc_assert.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for central self testing and error reporting
 *
 *  Basic flow of booting examples:
 *  (1) ttc_extensions.c:ttc_extensions_start() calls example_ttc_assert_prepare() to
 *      - initialize all required devices
 *      - initializes its global and local static variables
 *      - create all required task or statemachine instances
 *      - return immediately
 *
 *  Multitasking is available:
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_assert_none_prepare() get executed.
 *      Each task uses its private stack as given to ttc_task_create() before.
 *
 *  Multitasking is NOT available:
 *  (2) Minimalistic main loop is started
 *  (3) Main loop calls all registered statemachine functions in an endless loop.
 *      All statemachine functions use the same global stack that is maximum of all
 *      stack sizes given to ttc_task_create() before.
 *
 *  Created from template example_ttc_device.c revision 18 at 20161107 21:10:10 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_assert.h"
#include "../ttc-lib/ttc_assert.h" // high-level driver of device to use
#include "../ttc-lib/ttc_heap.h"     // dynamic memory allocations during runtime
#include "../ttc-lib/ttc_task.h"     // allows to use application in single- and multitasking setup

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_assert_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _example_assert_task( void* Argument );

//}Private Function Declarations
//{ Function Definitions *************************************************

void example_ttc_assert_prepare() {                   // prepare this example for being started
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */


    static example_assert_data_t AssertData1;
    ttc_memory_set( &AssertData1, 0, sizeof( AssertData1 ) );
    #ifdef TTC_LED1
    AssertData1.LED = TTC_LED1;
    ttc_gpio_init( AssertData1.LED, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    #endif

    // Register a function to be run after all system has finished booting.
    // ttc_task_create() works in single- and multitasking environments.
    ttc_task_create(
        #if (TTC_TASK_SCHEDULER_AVAILABLE)
        _example_assert_task,          // function to start as thread
        #else
        _example_assert_statemachine,  // function to run periodically (single tasking)
        #endif
        "tASSERT",                     // thread name (just for debugging)
        256,                           // stack size (adjust to amount of local variables of _example_assert_task() AND ALL ITS CALLED FUNTIONS!)
        &AssertData1,                  // passed as argument to _example_assert_task()
        1,                             // task priority (higher values mean more process time)
        NULL                           // can return a handle to created task
    );

    // You may initialize and create multiple assert instances..
}

// Note: If you implement both of the example functions below, your example will run in single- and multitasking configuration.
void _example_assert_statemachine( void* Argument );

void _example_assert_task( void* Argument ) {       // example task will be run when multitasking scheduler (e.g. FreeRTOS) has been started

    do {
        // all is implemented in our statemachine
        _example_assert_statemachine( Argument );

        ttc_task_yield(); // give cpu to other processes
    }
    while ( 1 );
}
void _example_assert_statemachine( void* Argument ) { // example statemachine will be run if no multitasking scheduler is available

    Assert_NULL( Argument , ttc_assert_origin_auto );   // cheap check to see if we got a NULL pointer
    Assert_Writable( Argument, ttc_assert_origin_auto ); // check if pointer targets to RAM or peripheral
    Assert_Readable( Argument, ttc_assert_origin_auto ); // if it's writable, it also must be readable

    // now we trust in this pointer (though it is not absolutely safe)
    example_assert_data_t* Data = ( example_assert_data_t* ) Argument;
    if ( Data->LED ) { // we have a LED: let's make some party light
        ttc_gpio_put( Data->LED, Data->Toggle );
        Data->Toggle = 1 - Data->Toggle;
    }

    Assert_Writable( &Argument, ttc_assert_origin_auto ); // check if variable Argument is located in RAM (it should be on the stack)
    Assert_Readable( &Argument, ttc_assert_origin_auto ); // if it's writable, it also must be readable

    const t_u8 SomeConstant = 42;     // a scalar data being placed in ROM (or FLASH)
    Assert_Readable( &SomeConstant, ttc_assert_origin_auto ); // a constant surely must be readable
    if ( 0 )
    { Assert_Writable( ( t_u8* ) &SomeConstant, ttc_assert_origin_auto ); } // this test will probably fail

    Assert( SomeConstant == 42, ttc_assert_origin_auto ); // check if it stores the right value
    Assert( SomeConstant == 42, ttc_assert_origin_auto ); // same check with a predefined error code (one from e_ttc_assert_origin)
}

//}Function Definitions
