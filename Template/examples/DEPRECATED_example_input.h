/*{ Template_FreeRTOS::.h ************************************************
 
  Example code for FreeRTOS + LCD display on Olimex prototype board STM32-LCD.
  written by Gregor Rebel 2011
 
}*/
#ifndef EXTENSION_500_ttc_gfx
#error EXTENSION_500_ttc_gfx must be defined!
#endif

//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_task.h"
#include "../ttc-lib/ttc_string.h"
#include "../ttc-lib/ttc_gfx.h"
#include "../ttc-lib/DEPRECATED_DEPRECATED_ttc_input.h"
//}Includes
//{ Defines/ TypeDefs ****************************************************

#ifndef EXTENSION_500_ttc_gfx
  #error Missing EXTENSION_500_ttc_gfx !
#endif

//}Defines
//{ Structures/ Enums ****************************************************


//}Structures/ Enums
//{ Global Variables *****************************************************



//}Global Variables
//{ Function declarations **************************************************

// basic hardware intialization
void example_input_start();
//start of the task input
void example_input_init();
//acting routine of the example input
void task_input(void *TaskArgument);
//function which is called when the defined area is touched
void button_event(void *TaskArgument);

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
