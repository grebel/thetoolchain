/** { example_ttc_radio_ranging_common.c ************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.c revision 11 at 20161201 12:44:19 UTC
 *
 *  Authors: gregor
}*/

/** { Includes ********************************************************
 *
 * Put all includes here that are required to compile this source code.
 */

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_radio_ranging_common.h"
#include "../ttc-lib/ttc_task.h"
#include "../ttc-lib/ttc_radio.h"     // radio transceiver
#include "../ttc-lib/ttc_packet.h"    // network packet handling
#include "../ttc-lib/ttc_gpio.h"      // general purpose input output
#include "../ttc-lib/ttc_string.h"    // string compare/ copy/ snprintf
#include "../ttc-lib/ttc_memory.h"    // memory copying, pointer testing

#ifdef TTC_USART1
    #include "../ttc-lib/ttc_usart.h"   // serial debug output
#endif
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Global Variables ************************************************
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** { Private Function Declarations ***********************************
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** { Function Definitions ********************************************
 */

/* DEPRECATED
void example_ttc_radio_ranging_initialize_pool( t_ttc_heap_pool* Pool, e_ttc_packet_type Type ) {

    t_ttc_heap_block_from_pool* MemoryBlock = ttc_heap_pool_block_get_try( Pool );
    if ( MemoryBlock ) { // pool stores at least one more block: initialize it
        ttc_packet_initialize( MemoryBlock, Type, 0 );

        // initialize remaining blocks in this pool
        example_ttc_radio_ranging_initialize_pool( Pool, Type );

        // give initialized block back to its pool
        ttc_heap_pool_block_free( MemoryBlock );
    }
}
*/
void example_ttc_radio_ranging_usart_send_stringf( const char* Format, t_base Value ) {
    Assert_Readable( Format, ttc_assert_origin_auto );
#ifdef TTC_USART1
#define USS_BUFFER_SIZE 100
    static t_u8 uss_Buffer[USS_BUFFER_SIZE];
    ttc_string_snprintf( uss_Buffer, USS_BUFFER_SIZE, Format, Value );
    ttc_usart_send_string( 1, uss_Buffer, USS_BUFFER_SIZE );
#else
    // no usart available: ignore all arguments
    ( void ) Format;
    ( void ) Value;
#endif
}
void example_ttc_radio_ranging_usart_send_string( const char* String, t_u16 BufferSize ) {
    Assert_Readable( String, ttc_assert_origin_auto );
#ifdef TTC_USART1
    ttc_usart_send_string( 1, String, BufferSize );
#else
    // no usart available: ignore all arguments
    ( void ) String;
    ( void ) BufferSize;
#endif
}
void example_ttc_radio_ranging_transmit( etr_task_info_t* TaskInfo, t_u8* Data, t_u16 Amount, t_ttc_packet_address* TargetID, e_ttc_packet_type Type, t_u8* StringBuffer, t_base StringBufferSize ) {
    Assert_Writable( TaskInfo, ttc_assert_origin_auto );
    Assert_Writable( Data,     ttc_assert_origin_auto );
    Assert_Writable( TargetID, ttc_assert_origin_auto );
    Assert( Amount > 0, ttc_assert_origin_auto );

    // Packets can be created in several ways.
    // The simpler ways are usefull to code quick examples. If higher datarates are required,
    // the application can manage several memory pools of pre-initialized packets on its own.
    // See different example implementations below.

    if ( !Type )
    { Type = E_ttc_packet_type_802154_Data_001010; }

    if ( 1 ) { // use individual pre-initialized packets from memory pool managed by ttc_radio
        while ( Amount ) {
            t_ttc_packet* PacketTX = ttc_radio_packet_get_empty( 1, Type, TaskInfo->SocketJob.Init.Pattern );

            if ( 1 ) { // address packet
                E_ttc_packet_address_source_set( PacketTX, TaskInfo->LocalNodeID );
                ttc_packet_address_target_set( PacketTX, TargetID );
            }
            t_u16  PayloadSize = 0; // start of payload depends on packet type
            t_u8* Payload;          // will be loaded with pointer to payload area
            Assert( ttc_packet_payload_get_pointer( PacketTX, &Payload, &PayloadSize, NULL ) == ec_packet_OK, ttc_assert_origin_auto ); // could not obtain pointer to payload area of new packet
            if ( PayloadSize > Amount )
            { PayloadSize = Amount; } // this will be last packet for this data buffer
            ttc_memory_copy( Payload, Data, PayloadSize );
            Data   += PayloadSize;
            Amount -= PayloadSize;
            s_ttc_packet_payloadet_size( PacketTX, PayloadSize );

            if ( StringBuffer ) { // assemble + print debug message
                t_u8* Append = StringBuffer;
                Append += ttc_string_appendf( Append, &StringBufferSize, "(0x%04x) TX: ", TaskInfo->LocalNodeID->Address16 );
                Append += ttc_packet_debug( PacketTX, Append, &StringBufferSize, TaskInfo->PacketDebug );
                Append += ttc_string_appendf( Append, &StringBufferSize, "\n" );
                example_ttc_radio_ranging_usart_send_string( ( char* ) StringBuffer, -1 );
            }
            ttc_radio_transmit_packet_now( 1, TaskInfo->RadioUserID, PacketTX, PayloadSize, FALSE );
        }
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
