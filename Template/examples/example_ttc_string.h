#ifndef EXAMPLE_STRING_H
#define EXAMPLE_STRING_H

/** example_ttc_STRING.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_string.
 *  The basic flow is as follows:
 *  (1) example_ttc_string_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_string_prepare() gets executed
 *
 *  Created from template example_ttc_device.h revision 12 at 20151120 13:30:25 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_STRING.c only do not belong here!
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_systick.h"  // exact delays and timeouts
#include "../ttc-lib/ttc_task_types.h" // required for task delays

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_string_t
    const char* Source;     // points to readable memory (might be located in RAM or ROM)
    char*       Dest;       // points to writable memory (must be located in RAM)
    t_u16       Dest_Size;  // size of Dest[]
    t_ttc_systick_delay  Delay;          // this type of delay is usable in single- and multitasking setup
    // more arguments...
} example_string_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_string_prepare();


//}Function prototypes

#endif //EXAMPLE_STRING_H
