#ifndef EXAMPLE_USB_HOST_HID
#define EXAMPLE_USB_HOST_HID

/*{ example_usb_host_hid.h *******************************************************
 
  Initializes first radio and periodically transmits data.
  Received data is echoes via TTC_USART1.

}*/
//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_task.h"
#include "../ttc-lib/ttc_random.h"
#include "../ttc-lib/ttc_watchdog.h"

//#include "../ttc-lib/ttc_string.h"

#include "../ttc-lib/ttc_usb.h"

#ifdef EXTENSION_400_stm32_usb_fs_host_lib
#include "usb_bsp.h"
#include "usbh_core.h"
#include "usbh_usr.h"
#include "usbh_hid_core.h"
#endif
//}Includes
//{ Defines/ TypeDefs ****************************************************


#ifndef TTC_TX
  #ifdef TTC_LED1
    #define TTC_TX TTC_LED1
  #else
    #warning TTC_TX not defined!
  #endif
#endif
#ifndef TTC_RX
  #ifdef TTC_LED2
    #define TTC_RX TTC_LED2
  #else
    #warning TTC_RX not defined!
  #endif
#endif

#define USB_DEBUG_LED 1
//#define USB_Handler_Data_echo

//}Defines
//{ Structures/ Enums ****************************************************

//}Structures/ Enums
//{ Global Variables *****************************************************



//}Global Variables
//{ Function declarations **************************************************

// basic hardware intialization
void example_usb_host_hid_init();

// main entry: initializes hardware + spawns all threads
// returns immediately
void example_usb_host_hid_start();


///*******************************************************************************
//* Function Name  : USB_Handler_Data_Send.
//* Description    : send data out to USB Bus.
//* Input          : None.
//* Return         : none.
//*******************************************************************************/
//void USB_Handler_Data_Send(u8_t* data_buffer, u32_t Nb_bytes);

///*******************************************************************************
//* Function Name  : USB_Handler_Data_Receive.
//* Description    : receive data from USB Bus.
//* Input          : data_buffer: data address.
//                   Nb_bytes: number of bytes to receive.
//* Return         : none.
//*******************************************************************************/
//void USB_Handler_Data_Receive(u8_t* data_buffer, u8_t Nb_bytes);

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_USB_VCP
