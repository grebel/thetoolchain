#ifndef example_ttc_list_H
#define example_ttc_list_H

/*{ example_ttc_list ************************************************
 
  etl_task_A() allocates memory blocks from pool and sends them to a list.
  etl_task_B() receives memory blocks from list and returns them to their pool.

}*/
//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_list.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

// user defined lit item (something that can be used with ttc_list)
typedef struct {
    t_ttc_list_item ListItem; // <- must be first element in struct!

    t_u8 Data; // example payload
    // .. more payload to add
} etl_Item_t;

//}Structures/ Enums
//{ Function declarations **************************************************

/** returns etl_Item_t that contains givcen list item
 *
 * Note: This function assumes that ListItem is first entry in etl_Item_t !!!
 *
 * @param ListItem   item retrieved from a ttc_list_XXX()
 * @return           etl_Item_t pointer
 */
etl_Item_t* etl_list_item_2_item(t_ttc_list_item* ListItem);
#define etl_list_item_2_item(ListItem) ( (etl_Item_t*) ListItem )

// main entry: initializes hardware + spawns all threads
// returns immediately
void example_ttc_list_prepare();

// sends memory blocks to List
void etl_task_A(void *TaskArgument);

// receives memory blocks from List
void etl_task_B(void *TaskArgument);

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //example_ttc_list_H
