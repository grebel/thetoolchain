#ifndef EXAMPLE_PWM_STM32L1XX_H
#define EXAMPLE_PWM_STM32L1XX_H

/** example_ttc_PWM_STM32L1XX.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_pwm_stm32l1xx.
 *  The basic flow is as follows:
 *  (1) example_ttc_pwm_stm32l1xx_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_pwm_stm32l1xx_prepare() gets executed
 *
 *  Created from template example_ttc_device_architecture.h revision 15 at 20180502 11:57:47 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_PWM_STM32L1XX.c only do not belong here! 
#include "../ttc-lib/ttc_basic.h"      // basic datatypes and definitions
#include "../ttc-lib/ttc_systick.h"    // exact delays and timeouts
#include "../ttc-lib/ttc_task_types.h" // required for task delays
#include "../ttc-lib/ttc_pwm_types.h" // datatypes of pwm devices
#include "../ttc-lib/pwm/pwm_stm32l1xx_types.h" // datatypes of stm32l1xx pwm devices

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_pwm_stm32l1xx_t
    t_ttc_pwm_config* Config_PWM; // pointer to configuration of pwm device to use
    t_ttc_systick_delay   Delay;            // this type of delay is usable in single- and multitasking setup

    // more arguments...
} example_pwm_stm32l1xx_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_pwm_stm32l1xx_prepare();


//}Function prototypes

#endif //EXAMPLE_PWM_STM32L1XX_H
