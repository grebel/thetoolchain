/** example_ttc_interrupt.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_interrupt.
 *  The basic flow is as follows:
 *  (1) example_ttc_interrupt_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_interrupt_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 11 at 20140415 23:54:43 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_interrupt.h"
#include "../ttc-lib/ttc_gpio.h"      // Configuration of individual General Purpose Input Output (GPIO) pins
#include "../ttc-lib/ttc_interrupt.h" // architecture independent external interrupts

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
example_interrupt_t et_interrupt_TaskArguments;

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void isr_switch1( t_physical_index Index, void* Reference ) {
    ( void ) Index;   // not used
    ( void ) Reference; // not used

    // Note: Interrupt Service Routines must be implemented as fast as possible!

    et_interrupt_TaskArguments.ActivateEffect = 1;
}
void isr_switch2( t_physical_index Index, void* Reference ) {
    ( void ) Index;   // not used
    ( void ) Reference; // not used

    // Note: Interrupt Service Routines must be implemented as fast as possible!

    et_interrupt_TaskArguments.ActivateEffect = 0;
}
void example_ttc_interrupt_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    // disabling all interrupts is a very fast, single assembly instruction that will not change interrupt settings
    // Note: This is just for demonstration. Disabling interrupts is a global effect that might interfere with multitasking scheduler!
    //       In most cases, you may want to call ttc_task_critical_begin() instead.
    if ( 0 ) { ttc_interrupt_all_disable(); }

    // using lowest switching speed to reduce EMI on lines (LEDs often use >2mA which is more than on most signal lines)
    #ifdef TTC_LED1
    ttc_gpio_init( TTC_LED1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min );
    #endif

    if ( 1 ) {
        #ifdef TTC_SWITCH1 // Initialize interrupt for first switch
        // initialize GPIO pin connected to switch as input to read its value
        ttc_gpio_init( TTC_SWITCH1, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );

        // registering pin connected to switch #1 as gpio (external) interrupt and register isr_switch1() as interrupt service routine
        et_interrupt_TaskArguments.Handle_GPIO1 = ttc_interrupt_init( tit_GPIO_Rising_Falling, TTC_SWITCH1, isr_switch1, 0, 0, 0 );
        Assert( ( et_interrupt_TaskArguments.Handle_GPIO1 != NULL ), ttc_assert_origin_auto ); // could not initialize interrupt (switch #1 on current board does not support external interrupt?)
        ttc_interrupt_enable_gpio( et_interrupt_TaskArguments.Handle_GPIO1 );
        #endif
        #ifdef TTC_SWITCH2 // Initialize interrupt for second switch
        ttc_gpio_init( TTC_SWITCH2, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
        et_interrupt_TaskArguments.Handle_GPIO2 = ttc_interrupt_init( tit_GPIO_Rising_Falling, TTC_SWITCH2, isr_switch2, 0, 0, 0 );
        // not checking Handle_GPIO2 (is optional in this example)

        if ( et_interrupt_TaskArguments.Handle_GPIO2 )
        { ttc_interrupt_enable_gpio( et_interrupt_TaskArguments.Handle_GPIO2 ); }
        #endif
    }

    // back in business
    if ( 1 ) { ttc_interrupt_all_enable(); }

    ttc_task_create( example_ttc_interrupt_task, "Interrupt", 128, NULL, 0, 0 );
}
void example_ttc_interrupt_task( void* TaskArgument ) {
    ( void ) TaskArgument; // not used

    while ( 1 ) {
        #ifdef TTC_LED1
        if ( et_interrupt_TaskArguments.ActivateEffect ) {
            ttc_gpio_set( TTC_LED1 );
            ttc_task_msleep( 100 );
            ttc_gpio_clr( TTC_LED1 );
            ttc_task_msleep( 100 );
            ttc_gpio_set( TTC_LED1 );
            ttc_task_msleep( 100 );
            ttc_gpio_clr( TTC_LED1 );
        }
        else {
            ttc_gpio_set( TTC_LED1 );
        }
        #endif
        ttc_task_msleep( 500 );
        #ifdef TTC_LED1
        ttc_gpio_clr( TTC_LED1 );
        ttc_task_msleep( 500 );
        ttc_gpio_set( TTC_LED1 );
        #endif
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
