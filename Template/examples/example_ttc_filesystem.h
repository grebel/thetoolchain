#ifndef EXAMPLE_FILESYSTEM_H
#define EXAMPLE_FILESYSTEM_H

/** example_ttc_FILESYSTEM.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for dosfs free fat12/fat16/fat32 filesystem for low-end embedded applications (1kb ram, 4kb rom)
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_filesystem_dosfs_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started.
 *  (3) Tasks being created by example_ttc_filesystem_dosfs_prepare() get executed.
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_filesystem_dosfs_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) Every registered task function is called one after another in an endless loop
 *      from ttc_task_start_scheduler().
 *
 *  Created from template example_ttc_device.h revision 18 at 20180413 11:21:12 UTC
 *
 *  Authors: <AUTHOR>
 *
 *
 *  Description of example_ttc_FILESYSTEM
 *
 *  ToDo: Document this example!
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_FILESYSTEM.c only do not belong here!
// This is why header files typically include *_types.h files only (avoids nasty include loops).

#include "../ttc-lib/ttc_basic_types.h"      // basic datatypes for current microcontroller architecture
#include "../ttc-lib/ttc_gpio_types.h"       // allows to store gpio pins in structures
#include "../ttc-lib/ttc_systick_types.h"    // allows to store task delay data in structures
#include "../ttc-lib/ttc_filesystem_types.h" // datatypes of filesystem devices
#include "../ttc-lib/ttc_sdcard_types.h"     // datatypes of sdcard devices

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_filesystem_t
    t_ttc_systick_delay      Delay;             // this type of delay is usable in single- and multitasking setup
    t_ttc_systick_delay      Delay2;            // this type of delay is usable in single- and multitasking setup

    // logical indices of devices being used
    t_u8                     Idx_filesystem;
    t_u8                     Idx_sdcard;

    // configuration pointers below must not be stored, but they make debugging easier
    t_ttc_filesystem_config* Config_filesystem; // pointer to configuration of filesystem device to use
    t_ttc_sdcard_config*     Config_sdcard;     // pointer to configuration of sdcard device

    // more arguments...
} t_example_filesystem_data;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_filesystem_prepare();


//}Function prototypes

#endif //EXAMPLE_FILESYSTEM_H
