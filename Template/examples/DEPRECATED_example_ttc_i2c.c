/*{ TestRMA::.c ************************************************

 Empty template for new c-files.
 Copy and adapt to your needs.

}*/

#include "example_i2c.h"
#ifndef EXTENSION_400_sensor_lis3lv02dl
# include "../sensor_accelerometer_mpu6050.h"
#endif
//{ Global variables *************************************************

// read-only configuration of current display (width, height, ...)
#ifdef EXTENSION_500_ttc_gfx
ttc_gfx_config_t* DisplayConfig = NULL;
#endif
ttc_i2c.config_t       I2C_Generic;  // architecture independent I2C configuration
ttc_i2c_architecture_t  I2C_Arch;     // architecture specific I2C configuration
#define I2C_INDEX 2                   // using first I2C available on current board
u8_t Address_InertialSensor = 29;
u16_t AmountTransfers = 0;

//}Global Variables
//{ Function definitions *********************************************

void example_i2c_init() {
#ifdef EXTENSION_500_ttc_gfx
    //u8_t MaxDisplay = ttc_gfx_get_max_display_index();
  //  Assert(MaxDisplay > 0, ec_DeviceNotFound); // no displays defined by board makefile!
    DisplayConfig = ttc_gfx_get_configuration(1);
    Assert(DisplayConfig, ec_DeviceNotFound);

    // set color to use for first screen clear
    ttc_gfx_set_color_bg24(TTC_GFX_COLOR24_AQUA);
    ttc_gfx_init(1);
#endif

 //   u8_t MaxI2C = ttc_i2c_get_max_index();
 //  Assert(MaxI2C > 0, ec_UNKNOWN);

    ttc_i2c_errorcode_e Error= tie_OK;

    Assert( Error == tie_OK, ec_UNKNOWN);

    I2C_Generic.Flags.Bits.Master = 1;
    I2C_Generic.ClockSpeed = 100000;
    I2C_Generic.Flags.Bits.DutyCycle_16_9 = 0;
    I2C_Generic.Flags.Bits.ModeFast = 0;
    I2C_Generic.Flags.Bits.Acknowledgement = 1;

    //I2C_Generic.Flags.Bits.
    Error = ttc_i2c_init(I2C_INDEX);
   // Assert( Error == tie_OK, ec_UNKNOWN);

}
void example_ttc_i2c_master_start() {
    example_i2c_init();


    ttc_task_create(task_I2C_master,                 // function to start as thread
                    "I2Ctx",                         // thread name (just for debugging)
                    512,                             // stack size
                    (void *) NULL,                   // passed as argument to taskLEDs()
                    1,                               // task priority (higher values mean more process time)
                    (void*) NULL                     // can return a handle to created task
                    );

}
void printfAt(u16_t X, u16_t Y, const char* String, s32_t Value) {


#ifdef EXTENSION_500_ttc_gfx
     char Buffer[20];
    ttc_string_snprintf( (u8_t*) Buffer, 20, String, Value);
    ttc_gfx_print_solid_at(X, Y, Buffer, 20);
#endif

}
void task_I2C_master(void *TaskArgument) {
    TaskArgument = TaskArgument; // avoids warning: unused parameter 'TaskArgument'


#ifdef EXTENSION_400_sensor_lis3lv02dl
#  ifdef EXTENSION_500_ttc_gfx
    ttc_gfx_text_cursor_set(0,0);
    ttc_gfx_print("LIS3LV02DL ??", -1);
#  endif

    lis3lv02dl_errorcode_e SensorError;
    SensorError = i2c_lis3lv02dl_init(I2C_INDEX);
    Assert(SensorError == lie_OK, ec_UNKNOWN);

    u8_t StatusRegister = 0;
//    ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_REG_WHO_AM_I, tiras_MSB_First_8Bit, &StatusRegister);
    //Assert(StatusRegister == 0x3A, ec_DeviceNotFound);

#  ifdef EXTENSION_500_ttc_gfx
    ttc_gfx_print_solid_at(11, 0, "OK", -1);
    ttc_gfx_print_solid_at( 0, 2, "X:       mG", -1);
    ttc_gfx_print_solid_at( 0, 3, "Y:       mG", -1);
    ttc_gfx_print_solid_at( 0, 4, "Z:       mG", -1);
#  endif

    s16_t AccelerationX;
    s16_t AccelerationY;
    s16_t AccelerationZ;

    while (1) {
        ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_OUTX_L, tiras_MSB_First_8Bit, (u8_t*) &AccelerationX );
        ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_OUTX_H, tiras_MSB_First_8Bit, ( (u8_t*) &AccelerationX ) + 1);

        ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_OUTY_L, tiras_MSB_First_8Bit, (u8_t*) &AccelerationY );
        ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_OUTY_H, tiras_MSB_First_8Bit, ( (u8_t*) &AccelerationY ) + 1);

        ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_OUTZ_L, tiras_MSB_First_8Bit, (u8_t*) &AccelerationZ );
        ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_OUTZ_H, tiras_MSB_First_8Bit, ( (u8_t*) &AccelerationZ ) + 1);

#  ifdef EXTENSION_500_ttc_gfx
        printfAt(3, 2, "%5i   ", AccelerationX);
        printfAt(3, 3, "%5i   ", AccelerationY);
        printfAt(3, 4, "%5i   ", AccelerationZ);
#  endif

        ttc_task_msleep(100);
    }
#else
#  ifdef EXTENSION_500_ttc_gfx
    ttc_gfx_text_cursor_set(0,0);
    ttc_gfx_print("MPU6050 ??", -1);
#  endif

    mpu6050_init(I2C_INDEX, 0x0, sleep_0, cycle_0, temp_dis_0, clksel_3, dlpf_4, ext_sync_set_0,  fifo_en_0,  mst_en_1);

    mpu6050_AFS_e mpu6050_afs=aii_2g;
    acc_i2c_init(I2C_INDEX,mpu6050_afs);

#  ifdef EXTENSION_500_ttc_gfx
    ttc_gfx_print_solid_at(11, 0, "OK", -1);
    ttc_gfx_print_solid_at( 0, 2, "X:       mG", -1);
    ttc_gfx_print_solid_at( 0, 3, "Y:       mG", -1);
    ttc_gfx_print_solid_at( 0, 4, "Z:       mG", -1);
#  endif

    //acc_data_t *data_a;
    acc_data_m Data_m;

    while (1) {
        ttc_i2c_read_register(I2C_INDEX, MPU6050_ADRESS, ACCEL_XOUT_L, tiras_LSB_First_8Bit, (u8_t*) &Data_m.x);
        ttc_i2c_read_register(I2C_INDEX, MPU6050_ADRESS, ACCEL_XOUT_H, tiras_LSB_First_8Bit, ((u8_t*) &Data_m.x)+1);

        ttc_i2c_read_register(I2C_INDEX, MPU6050_ADRESS, ACCEL_YOUT_L, tiras_LSB_First_8Bit, (u8_t*) &Data_m.y);
        ttc_i2c_read_register(I2C_INDEX, MPU6050_ADRESS, ACCEL_YOUT_H, tiras_LSB_First_8Bit,((u8_t*) &Data_m.y)+1);

        ttc_i2c_read_register(I2C_INDEX, MPU6050_ADRESS, ACCEL_ZOUT_L, tiras_LSB_First_8Bit, (u8_t*) &Data_m.z);
        ttc_i2c_read_register(I2C_INDEX, MPU6050_ADRESS, ACCEL_ZOUT_H, tiras_LSB_First_8Bit,((u8_t*) &Data_m.z)+1);

        /*s16_t AccelerationX = data_a->x;
        s16_t AccelerationY = data_a->y;
        s16_t AccelerationZ = data_a->z; */

#  ifdef EXTENSION_500_ttc_gfx
        printfAt(3, 2, "%5i   ", Data_m.x);
        printfAt(3, 3, "%5i   ", Data_m.y);
        printfAt(3, 4, "%5i   ", Data_m.z);
#  endif

        // ToDo: Read in sensor data
        ttc_task_msleep(10);
    }
#endif
}

//}FunctionDefinitions
