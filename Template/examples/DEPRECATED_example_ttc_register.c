/** example_ttc_register.c **********************************************{
 *
 *                             The ToolChain
 *
 *
 * example_ttc_register_start()
 *     fills et_register_TaskArguments with data
 *     spawns thread task_REGISTER()
 *     returns immediately
 *
 * task_REGISTER()
 *     while (1) {
 *       do something
 *       ttc_task_msleep(100);
 *     }
 *
}*/

#include "example_ttc_register.h"

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
example_register_t et_register_TaskArguments;

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_register_start() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */
    et_register_TaskArguments.Foo = 1; // set some arguments to be passed to task

#ifdef TTC_MULTITASKING_SCHEDULER // multitasking scheduler activated: spawn task + return 
    ttc_task_create( task_REGISTER,               // function to start as thread
                     "tREGISTER",                 // thread name (just for debugging)
                     128,                         // stack size
                     &et_register_TaskArguments,  // passed as argument to task_REGISTER()
                     1,                           // task priority (higher values mean more process time)
                     NULL                         // can return a handle to created task
                   );
#else               // no multitasking: simply call function (it will not terminate)
    task_REGISTER(TaskArgument);
#endif
}
void task_REGISTER(void *TaskArgument) {
    Assert(TaskArgument, ec_NULL);                  // always check pointer arguments with Assert()!
    example_register_t* TaskArguments = (example_register_t*) TaskArgument; // we're expecting this pointer type

    while (1) {
      // do something...
      ttc_task_msleep(100);    // always sleep for a while to give cpu time to other tasks
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
