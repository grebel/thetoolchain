/*{ example_leds.c ************************************************

  example_leds_prepare()
      spawns thread taskBrightness()

  taskBrightness()
      Defines brightness of led as Ton / (Ton + Toff)
      spawns thread taskControlLed()
      while (1) {
        while (Ton < 100) { Ton++; Toff--; }
        while (Ton >   0) { Ton--; Toff++; }
      }

  taskControlLed()
      while (1) {
        switch on LED
        wait for Ton
        switch off LED
        wait for Toff
      }

}*/

#include "example_leds.h"
#include "../ttc-lib/ttc_basic.h"     // Basic set of helper functions, datatypes and definitions
#include "../ttc-lib/ttc_systick.h"   // exact timing and delays
#include "../ttc-lib/ttc_gpio.h"      // general purpose input output pins
#include "../ttc-lib/ttc_task.h"      // register functions as tasks or statemachines
#include "../ttc-lib/ttc_memory.h"    // erase/ fill buffers and test pointers

//{ Global variables

// Note: Global and static variables require a unique prefix (here "el_") to avoid
//       conflicting symbol names when combined with other sources!


//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

#ifndef TTC_LED1
#  error TTC_LED1 not defined!
#endif

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_leds_prepare() {

    static t_configled  MyLED;                // must be static or global to survive end of this function!
    ttc_memory_set( &MyLED, 0, sizeof( MyLED ) ); // set all fields to zero
    MyLED.Brightness = 60;                    // starting value for brightness
    MyLED.PortPin    = TTC_LED1;              // gpio pin to use

    // TTC_LED1 is first LED configured by current board
    ttc_gpio_init( TTC_LED1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    ttc_gpio_clr( TTC_LED1 );

    ttc_task_create( taskBrightness,   // function to start as thread
                     "taskBrightness", // thread name (just for debugging)
                     128,              // stack size
                     &MyLED,           // passed as argument to taskBrightness()
                     1,                // task priority (higher values mean more process time)
                     NULL              // can return a handle to created task
                   );
}
void taskBrightness( void* TaskArgument ) {
    Assert_Writable( TaskArgument, ttc_assert_origin_auto );                // always check pointer arguments with Assert()!
    t_configled* LED = ( t_configled* ) TaskArgument; // we're expecting a t_configled* pointer

    if ( ! LED->Initialized_TaskBrightness ) { // first called for this configuration
        LED->Initialized_TaskBrightness = TRUE;

        ttc_task_create( taskControlLed,     // function to start as thread
                         "taskControl_LED1",  // thread name (just for debugging)
                         128,                 // stack size
                         ( void* ) LED,       // passed as argument to taskControlLed()
                         1,                   // task priority (higher values mean more process time)
                         NULL                 // can return a handle to created task
                       );
    }

    do  {
        while ( LED->Brightness > 0 ) { // ramp LED brightness down to 0%
            LED->Brightness--;
            #if TTC_TASK_SCHEDULER_AVAILABLE
            ttc_task_msleep( 10 );
            #else
            // no multitasking available: call other task like a function for 10ms
            t_ttc_systick_delay Delay;
            ttc_systick_delay_init( &Delay, 10000 ); // 10ms delay
            while ( !ttc_systick_delay_expired( &Delay ) ) { // calling taskControlLed() until 10ms have passed
                taskControlLed( TaskArgument );
            }
            #endif
        }

        while ( LED->Brightness < 100 ) { // ramp LED brightness up to 100%
            LED->Brightness++;
            #if TTC_TASK_SCHEDULER_AVAILABLE
            ttc_task_msleep( 10 );
            #else
            // no multitasking available: call other task like a function for 10ms
            t_ttc_systick_delay Delay;
            ttc_systick_delay_init( &Delay, 10000 ); // 10ms delay
            while ( !ttc_systick_delay_expired( &Delay ) ) { // calling taskControlLed() until 10ms have passed
                taskControlLed( TaskArgument );
            }
            #endif
        }
    }
    while ( TTC_TASK_SCHEDULER_AVAILABLE );
}
void taskControlLed( void* TaskArgument ) {
    Assert_Writable( TaskArgument, ttc_assert_origin_auto );                // always check pointer arguments with Assert()!
    t_configled* LED = ( t_configled* ) TaskArgument; // we're expecting a t_configled* pointer
    Assert_Writable( LED, ttc_assert_origin_auto );

    do {
        if ( LED->Brightness >  0 ) { // switch  on LED
            ttc_gpio_set( TTC_LED1 );
            ttc_task_usleep( LED->Brightness * MinimumDelay );
        }
        if ( LED->Brightness < 99 ) { // switch off LED
            ttc_gpio_clr( TTC_LED1 );
            ttc_task_usleep( ( 99 - LED->Brightness ) * MinimumDelay );
        }
    }
    while ( TTC_TASK_SCHEDULER_AVAILABLE );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
