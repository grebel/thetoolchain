#ifndef example_ttc_heap_pool_H
#define example_ttc_heap_pool_H

/*{ example_ttc_heap_pool ************************************************
 
  emp_task_Sender() allocates memory blocks from pool and sends them to a list.
  emp_task_Receiver() receives memory blocks from list and returns them to their pool.

}*/
//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_memory.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************


//}Structures/ Enums
//{ Function declarations **************************************************

// main entry: initializes hardware + spawns all threads
// returns immediately
void example_ttc_heap_pool_prepare();

// sends memory blocks to List
void emp_task_Sender(void *TaskArgument);

// receives memory blocks from List
void emp_task_Receiver(void *TaskArgument);

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //example_ttc_heap_pool_H
