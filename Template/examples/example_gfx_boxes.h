/*{ Template_FreeRTOS::.h ************************************************

  Example code for FreeRTOS + LCD display on Olimex prototype board STM32-LCD.

  written 2012 by Ioanna Tsekoura, Gregor Rebel

}*/

//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_task.h"
#include "../ttc-lib/ttc_random.h"
#include "../ttc-lib/ttc_gfx.h"
#include "../ttc-lib/ttc_gfx_types.h"
#include "../ttc-lib/ttc_mutex.h"
#include "../ttc-lib/ttc_queue_types.h"
#include "../ttc-lib/ttc_math.h"
#include "../ttc-lib/ttc_systick.h"  // exact delays and timeouts
#include "../ttc-lib/ttc_task_types.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

typedef struct {
    t_u32 Color;
    t_s32 PosX;
    t_s32 PosY;
    t_s16 SpeedX;
    t_s16 SpeedY;
    t_u16  Width;
    t_u16  Height;
} __attribute__( ( __packed__ ) ) elb_MovingBox_t;

typedef struct {
    t_u32 Color;      //             <- Width ->
    t_s32 ExternalX;  // External--> X##########       A
    t_s32 ExternalY;  //             ###########       |
    t_s32 InternalX;  //             ###X <-- Internal |Height
    t_s32 InternalY;  //             ####              |
    t_u16  Width;     //             ####              |
    t_u16  Height;    //             ####              V
} __attribute__( ( __packed__ ) ) elb_L_Polygon_t;

typedef union {
    elb_MovingBox_t MovingBox;
    elb_L_Polygon_t L_Polygon;
} elb_Polygon_t;

typedef enum  {
    ddt_None,
    ddt_MovingBox,
    ddt_L_Polygon,
    ddt_unknown
} elb_DrawDataType_e;

typedef struct {
    elb_DrawDataType_e Type;
    elb_Polygon_t Polygon;
} __attribute__( ( __packed__ ) ) DrawData_t;

typedef struct {
    t_ttc_systick_delay  Delay;          // this type of delay is usable in single- and multitasking setup
    t_ttc_queue_generic DrawQueue;
    DrawData_t          Box2Draw;
} __attribute__( ( __packed__ ) ) drawTaskArgs_t;


//}Defines
//{ Structures/ Enums ****************************************************


//}Structures/ Enums
//{ Structures/ Enums ****************************************************

//}Structures/ Enums
//{ Global Variables *****************************************************



//}Global Variables
//{ Function declarations **************************************************

// main entry: initializes hardware + spawns all threads
// returns immediately
void example_gfx_boxes_prepare();

// spawns a new box with randomly chosen parameters
// return: != NULL: settings struct of moving box
DrawData_t* newRandomBox( t_ttc_queue_generic DrawingQueue );

// spawns a new box with given parameters
// return: != NULL: settings struct of moving box
DrawData_t* newBox( t_ttc_queue_generic DrawingQueue, t_u16 Color, t_s32 X, t_s32 Y, t_u16 Width, t_u16 Height, t_s8 SpeedX, t_s8 SpeedY );

void tMoveBox( void* TaskArgument );
void queueInsert( t_ttc_queue_generic DrawQueue, DrawData_t* Polygon2Draw );
void tDrawIt( void* TaskArgument );
void LCD_DrawLpolygon( t_s32 ExternalX, t_s32 ExternalY, t_s32 Width, t_s32 Height, t_s32 InternalX, t_s32 InternalY );

// main task controlling LCD-panel
//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
