/** example_ttc_radio.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_radio.
 *  The basic flow is as follows:
 *  (1) example_ttc_radio_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_radio_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 11 at 20140514 03:13:59 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#define USE_USART 1

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_radio.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_heap.h"
#include "../ttc-lib/ttc_packet.h"
#include "../ttc-lib/ttc_random.h"
#include "../ttc-lib/ttc_string.h"
#include "../ttc-lib/ttc_memory.h"
#if USE_USART == 1
#include "../ttc-lib/ttc_usart.h"
#endif
#include "../ttc-lib/ttc_list.h"

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
const char* String_Hello   = "Hello World";
const char* String_Button1 = "Button #1";
const char* String_Button2 = "Button #2";
const char* String_Text    = "This is a somewhat longer text that should not fit into a single radio packet. Instead ttc_radio_transmit_buffer() has to split it into parts and send it in multiple packets.\n";

// configuration data used by this example
t_etr_config etr_Config;
//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_radio_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    #ifdef TTC_LED1
    ttc_gpio_init( TTC_LED1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min );
    #endif
    #ifdef TTC_LED2
    ttc_gpio_init( TTC_LED2, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min );
    #endif
    #ifdef TTC_SWITCH1
    ttc_gpio_init( TTC_SWITCH1, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
    #endif
    #ifdef TTC_SWITCH2
    ttc_gpio_init( TTC_SWITCH2, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min );
    #endif

    // set logical index of radio device to use
    etr_Config.RadioIndex = 1;

    etr_Config.RadioConfig = ttc_radio_get_configuration( etr_Config.RadioIndex );

    // setting receive function (optional)
    etr_Config.RadioConfig->Init.function_end_rx_isr = example_ttc_radio_isr;

    // setting maximum transmission level
    etr_Config.RadioConfig->Init.LevelTX             = etr_Config.RadioConfig->Features->MaxTxLevel;

    // add more settings in etr_Config.RadioConfig->Init...

    ttc_radio_init( etr_Config.RadioIndex );

    // obtain our own protocol type
    etr_Config.SocketID = ttc_radio_socket_new( etr_Config.RadioIndex, E_ttc_packet_pattern_socket_AnyOf_Application );

    // obtain our own user identifier
    etr_Config.UserID   = ttc_radio_user_new( etr_Config.RadioIndex, 0 );

    // set our local address
    const t_ttc_packet_address LocalAddress = {
        //.Address64 = { ... },    // not using 64 bit addressing in this example
        .Address16 = 0x1000,     // 16-bit logical address
        .PanID     = 0xaffe      // Personal Area Network Identification
    };

    ttc_radio_change_local_address( etr_Config.RadioIndex, etr_Config.UserID, &LocalAddress );

    #if USE_USART == 1
    t_ttc_usart_config* USART = ttc_usart_get_configuration( 1 );
    ( void ) USART; // using default configuration
    USART->Init.Flags.DelayedTransmits = 0;
    USART->Init.Flags.RtsCts = 0;
    USART->Init.Flags.Cts    = 0;
    USART->Init.Flags.Rts    = 0;
    ttc_usart_init( 1 );
    #endif

    ttc_task_create( example_ttc_radio_task,  // function to start as thread
                     "tRADIO",                 // thread name (just for debugging)
                     128,                      // stack size
                     &etr_Config,   // passed as argument to example_ttc_radio_task()
                     1,                        // task priority (higher values mean more process time)
                     NULL                      // can return a handle to created task
                   );
}
void example_ttc_radio_task( void* TaskArgument ) {
    t_etr_config* Config = ( t_etr_config* ) TaskArgument;
    ttc_assert_address_writable( Config ); // does not point to writable memory. Check implementation of caller!

    #ifdef TTC_SWITCH1
#define USING_SWITCHES
    BOOL SwitchState1 = ttc_gpio_get( TTC_SWITCH1 );
    #endif
    #ifdef TTC_SWITCH2
#define USING_SWITCHES
    BOOL SwitchState2 = ttc_gpio_get( TTC_SWITCH2 );
    #endif

    #if USE_USART == 1
    ttc_usart_send_string_const( 1, "Example_TTC_Radio\n", -1 );
    #endif
    #ifdef TTC_LED1
    for ( t_u8 Count = 10; Count > 0; Count-- ) {
        ttc_gpio_set( TTC_LED1 );
        #  ifdef TTC_LED2
        ttc_gpio_clr( TTC_LED2 );
        #  endif
        ttc_task_msleep( 50 );
        ttc_gpio_clr( TTC_LED1 );
        #  ifdef TTC_LED2
        ttc_gpio_set( TTC_LED2 );
        #  endif
        ttc_task_msleep( 50 );
    }
    ttc_gpio_set( TTC_LED1 );
    #  ifdef TTC_LED2
    ttc_gpio_set( TTC_LED2 );
    #  endif
    #endif
    for ( t_u8 Count = 10; Count > 0; Count-- ) { // test radio GPIOs
        ttc_radio_gpio_out( 1, E_ttc_radio_gpio_status_rx, 1 );
        ttc_radio_gpio_out( 1, E_ttc_radio_gpio_status_tx, 0 );
        ttc_task_msleep( 50 );
        ttc_radio_gpio_out( 1, E_ttc_radio_gpio_status_rx, 0 );
        ttc_radio_gpio_out( 1, E_ttc_radio_gpio_status_tx, 1 );
        ttc_task_msleep( 50 );
    }
    ttc_radio_gpio_out( 1, E_ttc_radio_gpio_status_rx, 0 );
    ttc_radio_gpio_out( 1, E_ttc_radio_gpio_status_tx, 0 );

    t_ttc_radio_job_socket SocketJob;         // search job finds packets in List_PacketsRx matching certain socket identifier
    t_ttc_packet*          PacketRX  = NULL;  // points to latest received network packet
    t_u16                  WaitCount = 0;     // not sending hello world in every loop iteration

    ttc_memory_set( &SocketJob, 0, sizeof( SocketJob ) ); // ensure that all unused fields are zero

    while ( 1 ) {
        if ( !WaitCount-- ) { // send hello after random delay
            WaitCount = ( ttc_random_generate16() % 100 ) + 10 ; // choosing next transmission time randomly
            #ifdef TTC_LED2
            ttc_gpio_set( TTC_LED2 );
            #endif
            example_ttc_radio_transmit( Config, ( t_u8* ) String_Hello, ttc_string_length8( String_Hello, -1 ) );
            #ifdef TTC_LED2
            ttc_gpio_clr( TTC_LED2 );
            #endif
        }
        if ( 1 ) { // handle switches (if available)
            #ifdef USING_SWITCHES
            BOOL ButtonSwitched = FALSE;
            #ifdef TTC_SWITCH1
            if ( SwitchState1 != ttc_gpio_get( TTC_SWITCH1 ) ) {
                SwitchState1 = ttc_gpio_get( TTC_SWITCH1 );
                if ( SwitchState1 == 0 ) { // switch pressed: send string
                    example_ttc_radio_transmit( Config, ( t_u8* ) String_Button1, ttc_string_length8( String_Button1, -1 ) );
                    ButtonSwitched = TRUE;
                }
            }
            #endif
            #ifdef TTC_SWITCH2
            if ( SwitchState2 != ttc_gpio_get( TTC_SWITCH2 ) ) {
                SwitchState2 = ttc_gpio_get( TTC_SWITCH2 );
                if ( SwitchState2 == 0 ) { // switch pressed: send string
                    example_ttc_radio_transmit( Config, ( t_u8* ) String_Button2, ttc_string_length8( String_Button2, -1 ) );
                    ButtonSwitched = TRUE;
                }
            }
            #endif
            if ( ButtonSwitched ) {
                if ( SwitchState1 + SwitchState2 == 0 ) { // both buttons pressed: send long text
                    example_ttc_radio_transmit( Config, ( t_u8* ) String_Text, ttc_string_length8( String_Text, -1 ) );
                }
                ttc_task_msleep( 500 );  // always sleep for a while to give cpu time to other tasks
            }
            #endif
        }
        if ( 1 ) { // run search job that looks for packets matching our socket
            SocketJob.Init.Initialized = 0;
            SocketJob.Init.Pattern     = Config->SocketID; // looking for packet directed at our socket

            PacketRX = ttc_radio_packet_received_tryget( Config->RadioIndex, // logical index of radio device
                                                         Config->UserID,     // required to check if we have access
                                                         &SocketJob          // defines which kind of packets to look for
                                                       );
        }
        if ( PacketRX ) { // packet received: process + release it
            t_u8* PacketPayload;
            t_u16 PayloadSize;
            ttc_packet_payload_get_pointer( PacketRX, &PacketPayload, &PayloadSize, 0 );
            #if USE_USART == 1
            if ( PacketPayload ) {
                ttc_usart_send_string_const( 1, "RX: ", -1 );
                ttc_usart_send_string( 1, ( char* )  PacketPayload, PayloadSize );
                ttc_usart_send_string_const( 1, "\n", -1 );
            }
            #endif

            // return memory block to its pool for reuse
            PacketRX = ttc_radio_packet_release( PacketRX );
        }
        else {            // dropping all packets of unsupported socket is important to avoid piling them up
            SocketJob.Init.Initialized = 0;
            SocketJob.Init.Pattern     = E_ttc_packet_pattern_socket_AnyOf_NonApplication;
            ttc_radio_packets_received_ignore( Config->RadioIndex, // logical index of radio device
                                               Config->UserID,     // required to check if we have access
                                               E_ttc_packet_pattern_socket_AnyOf_NonApplication // dropping all packets that are not targeted
                                             );
        }
        ttc_task_msleep( 10 );
    }
}
BOOL example_ttc_radio_isr( t_ttc_radio_config* Config, t_ttc_packet* Packet ) {
    // Note: This function is called from interrupt service routine and must return ASAP!
    ( void ) Config;
    ( void ) Packet; // we do not process the packet here


    #ifdef TTC_LED2
    static t_u8 etr_LedRxOn = 0;
    etr_LedRxOn = 1 - etr_LedRxOn;
    if ( etr_LedRxOn ) { ttc_gpio_set( TTC_LED2 ); }
    else             { ttc_gpio_clr( TTC_LED2 ); }
    #endif

    return FALSE; // ttc_radio will append Packet to its receive list
}
void example_ttc_radio_transmit( t_etr_config* Config, const t_u8* Data, t_u8 Amount ) {

    #ifdef TTC_LED1
    static t_u8 example_ttc_radio_transmit_LedOn = 0;
    example_ttc_radio_transmit_LedOn = 1 - example_ttc_radio_transmit_LedOn;
    if ( example_ttc_radio_transmit_LedOn ) { ttc_gpio_set( TTC_LED1 ); }
    else                          { ttc_gpio_clr( TTC_LED1 ); }
    #endif
    #ifdef TTC_LED2
    static t_u8 example_ttc_radio_transmit_LedOn2 = 0;
    example_ttc_radio_transmit_LedOn2 = 1 - example_ttc_radio_transmit_LedOn2;
    if ( !example_ttc_radio_transmit_LedOn2 ) { ttc_gpio_set( TTC_LED2 ); }
    else                          { ttc_gpio_clr( TTC_LED2 ); }
    #endif

    // broadcast transmission of a binary string using one or more packets
    ttc_radio_transmit_buffer( Config->RadioIndex,                   // index of radio device to use
                               Config->UserID,                       // have no user id (generic user)
                               Config->SocketID,                     // stored as first payload byte to deliver packet to correct endpoint
                               E_ttc_packet_type_802154_Data_001010, // IEEE 802.15.4 packet type (data packet with DstAddr16+DstPan16+SrcAddr16+SrcPan16)
                               Data,                                 // data to be transmitted using one or more packets
                               Amount,                               // maximum amount of bytes to send
                               NULL,                                 // DestinationID==NULL: send as broadcast
                               0,                                    // DelayNS==0:          send as soon as possible
                               FALSE                                 // Acknowledged==FALSE: do not request acknowledge handshake
                             );

    #if USE_USART == 1
    ttc_usart_send_string_const( 1, "TX: ", -1 );
    ttc_usart_send_string( 1, ( const char* ) Data, Amount );
    ttc_usart_send_string_const( 1, "\n", -1 );
    #endif
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
