#ifndef EXAMPLE_I2C_H
#define EXAMPLE_I2C_H

/** example_ttc_I2C.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_i2c.
 *  The basic flow is as follows:
 *  (1) example_ttc_i2c_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_i2c_prepare() gets executed
 *
 *  Created from template example_ttc_device.h revision 12 at 20150531 19:40:52 UTC
 *
 *  Authors: Gregor Rebel
 *
 *  How It Works
 *
 *  On I2C bus each device can have two roles: master and slave
 *  A slave is listening on the bus for its slave address to appear. Once it
 *  receives an address matching to its own address, the slave may read data from or
 *  send data to the bus as requested.
 *
 *  Slave Registers
 *  Slaves provide several bytes of configuration and sensor data.
 *  These are often enumerated by a single byte address counter. This allows to read or 
 *  write individual bytes. Addressed data in I2C slaves is called a register.
 *  A special register-read /-write sequence allows to read or write individual registers
 *  from/ to an I2C slave. 
 *
 *  Basic slave sequence:
 *  1) Wait until address is received that matches slaves own address
 *  2) Check read bit of address byte
 *     ==1: read data from bus
 *     ==0: send data to bus
 *
 *  A master initiates bus transactions with slaves.
 * 
 *  Basic master sequence:
 *  1) Wait until bus is free
 *  2) Generate Start Condition
 *  3) Send out Slave Address
 *  4) Send data to/ read data from slave
 *  5) Generate Stop Condition
 *
 *  Most commercially available I2C sensors implement only the slave role.
 *  Microcontrollers can act as both master and slave.   
 *  example_ttc_i2c.h implements both roles using two mechanisms of data transfer:
 *  + master role
 *    - send single bytes to slave
 *    - read single bytes from slave
 *    - write a register in slave device
 *    - read a register from slave device
 *  + slave role
 *    - send single bytes to master
 *    - read single bytes from master
 *    - receive data and write it into a certain memory space (called register)
 *    - send data to master from a certain memory space (called register)
 *  
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_I2C.c only do not belong here! 
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_i2c_types.h"
#include "../ttc-lib/ttc_adc_types.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // t_example_i2c
    t_ttc_i2c_config* ConfigI2C;    // points to configuration of used I2C device
    t_u16             AnalogValue1; // latest analog value being read from TTC_ANALOG1
    // more arguments...
} t_example_i2c;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_i2c_prepare();


//}Function prototypes

#endif //EXAMPLE_I2C_H
