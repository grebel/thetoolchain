#ifndef EXAMPLE_REGISTER_H
#define EXAMPLE_REGISTER_H

/** example_ttc_REGISTER.h **********************************************{
 *
 *                              The ToolChain
 *
 * Empty template for new header files.
 * Copy and adapt to your needs.
 *
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc_register.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_register_t
    u8_t Foo;  // example data
    // more arguments...
} example_register_t;

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *  - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_register_start();

/** task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void task_REGISTER(void *TaskArgument);


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_REGISTER_H
