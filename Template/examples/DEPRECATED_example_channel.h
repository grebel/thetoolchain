#ifndef EXAMPLE_CHANNEL_H
#define EXAMPLE_CHANNEL_H

/*{ example_channel.h ******************************************************
 
                       The ToolChain
                       
 Example code demonstrating how to use 
 Universal bidirectional communication channels between different types
 of communication devices.
 
 written by Gregor Rebel 2012
 
}*/
//{ Defines/ TypeDefs ****************************************************


//}Defines
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_channel.h"
#include "../ttc-lib/ttc_string.h"

// include supported devices
#include "../ttc-lib/ttc_usart.h"
#include "../ttc-lib/ttc_i2c.h"
#include "../ttc-lib/ttc_spi.h"
#include "../ttc-lib/DEPRECATED_ttc_radio.h"

//}Includes
//{ Structures/ Enums ****************************************************

//}Structures/ Enums
//{ Function declarations **************************************************

/* Call before example_channel_start() to initialize hardware.
 */
void example_channel_init();

/* initializes + starts CHANNEL example
 */
void example_channel_start();

/* runs echo server on initialized CHANNEL
 */
void task_channel_echoes(void *TaskArgument);

/* takes bytes received from CHANNEL
 */
ttc_heap_block_t* channel_receive_data(ttc_channel_node_handle_t SourceNode, ttc_heap_block_t* Data);

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_CHANNEL_H
