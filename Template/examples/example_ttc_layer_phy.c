/** example_ttc_layer_phy.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_layer_phy.
 *  The basic flow is as follows:
 *  (1) example_ttc_layer_phy_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_layer_phy_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 11 at 20150304 08:22:38 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_layer_phy.h"

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
example_layer_phy_t et_layer_phy_TaskArguments;

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_layer_phy_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */
   et_layer_phy_TaskArguments.Foo = 1; // set some arguments to be passed to task

   ttc_task_create( task_LAYER_PHY,               // function to start as thread
                    "tLAYER_PHY",                 // thread name (just for debugging)
                    128,                         // stack size
                    &et_layer_phy_TaskArguments,  // passed as argument to task_LAYER_PHY()
                    1,                           // task priority (higher values mean more process time)
                    NULL                         // can return a handle to created task
                  );
}
void task_LAYER_PHY(void *TaskArgument) {
    Assert(TaskArgument, ttc_assert_origin_auto);                  // always check pointer arguments with Assert()!
    example_layer_phy_t* TaskArguments = (example_layer_phy_t*) TaskArgument; // we're expecting this pointer type

    while (1) {
      // do something...
      ttc_task_msleep(100);    // always sleep for a while to give cpu time to other tasks
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
