#ifndef EXAMPLE_TOUCHPAD_H
#define EXAMPLE_TOUCHPAD_H

/** example_ttc_TOUCHPAD.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_touchpad.
 *  The basic flow is as follows:
 *  (1) example_ttc_touchpad_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_touchpad_prepare() gets executed
 *
 *  Created from template example_ttc_device.h revision 11 at 20150316 07:48:52 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_touchpad.h"
#include "../ttc-lib/ttc_gfx.h"
#include "../ttc-lib/ttc_systick.h"  // exact delays and timeouts

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************



typedef struct { // example_touchpad_t
    t_ttc_gfx_config*      ConfigGfx;
    t_ttc_touchpad_config* ConfigTouchpad;
} example_touchpad_t;

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *
 * - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_touchpad_prepare();

/** task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void task_TOUCHPAD(void *TaskArgument);

/** Displays simple menu to adjust touch display
 *
 * @param LogicalIndex  logical index of touchpad device to adjust
 */
void menuAdjustTouchpad(t_u8 LogicalIndex);

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_TOUCHPAD_H
