#ifndef EXAMPLE_SDCARD_H
#define EXAMPLE_SDCARD_H

/** example_ttc_SDCARD.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for secure digital card storage connected via spi bus
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_sdcard_spi_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started.
 *  (3) Tasks being created by example_ttc_sdcard_spi_prepare() get executed.
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_sdcard_spi_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) Every registered task function is called one after another in an endless loop
 *      from ttc_task_start_scheduler().
 *
 *  Created from template example_ttc_device.h revision 18 at 20180130 09:42:26 UTC
 *
 *  Authors: Gregor Rebel
 *
 *
 *  Description of example_ttc_SDCARD
 *
 *  ToDo: Document this example!
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_SDCARD.c only do not belong here!
// This is why header files typically include *_types.h files only (avoids nasty include loops).

#include "../ttc-lib/ttc_basic_types.h"    // basic datatypes for current microcontroller architecture
#include "../ttc-lib/ttc_gpio_types.h"     // allows to store gpio pins in structures 
#include "../ttc-lib/ttc_task_types.h"     // allows to store task delay data in structures
#include "../ttc-lib/ttc_sdcard_types.h"   // datatypes of sdcard devices
#include "../ttc-lib/ttc_systick_types.h"  // precise delays from single timer

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

#define ESD_BLOCK_SIZE 512
typedef struct { // example_sdcard_t
    t_ttc_sdcard_config* Config_SDCARD; // pointer to configuration of sdcard device to use
    t_ttc_systick_delay  Delay;         // this type of delay is usable in single- and multitasking setup
    t_u8                 Block[ESD_BLOCK_SIZE];    // memory buffer for a single block
} example_sdcard_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_sdcard_prepare();


//}Function prototypes

#endif //EXAMPLE_SDCARD_H
