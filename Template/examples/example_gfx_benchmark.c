/*{ example_gfx_benchmark.c ************************************************
 *
 * Benchmark code for ttc_gfx.c/.h for current architecture and display.
 *
 * written by Gregor Rebel 2012-2016
}*/

#include "example_gfx_benchmark.h"
#include "../compile_options.h"

// read-only configuration of current display (width, height, ...)
t_ttc_gfx_config* egb_DisplayConfig = NULL;
const char* HelloWorld = "Hello World";
#define Random_Amount 1000
const t_u16 RandomData[Random_Amount] = {
    47389, 54889, 47569, 55777, 59697, 200, 12931, 64410, 38406, 50236,
    13005, 55073, 7289, 49246, 59771, 14604, 28153, 25676, 4801, 28166,
    64301, 14959, 15248, 13154, 8244, 54982, 30157, 48796, 54581, 25027,
    48511, 31758, 34806, 34539, 53600, 27726, 18184, 54695, 34129, 46939,
    23212, 2325, 2834, 678, 11124, 55461, 30730, 54645, 57411, 54300,
    1349, 17631, 30321, 53565, 29820, 50901, 39861, 52588, 26893, 23053,
    13446, 4398, 53601, 47103, 65261, 23440, 7231, 32212, 951, 57370,
    32809, 47793, 45727, 46877, 8582, 7170, 23554, 52910, 10534, 42206,
    53153, 32308, 38910, 892, 59086, 10101, 49408, 55328, 42227, 8970,
    11955, 43852, 62946, 60183, 27294, 38530, 19073, 43553, 42977, 23243,
    58299, 9662, 62434, 33724, 20021, 20790, 63237, 25643, 51308, 37801,
    57081, 62540, 15887, 9220, 7162, 34537, 30531, 31583, 46345, 46603,
    44848, 14421, 7342, 64984, 10996, 23535, 21129, 4126, 42793, 18197,
    26188, 45797, 40697, 36399, 63632, 53859, 63975, 40025, 28106, 37037,
    38511, 16376, 12769, 28367, 17440, 58503, 40760, 61584, 63901, 16564,
    1403, 16694, 42764, 12563, 35410, 45547, 50402, 61147, 33789, 23696,
    20437, 43911, 49696, 40311, 64720, 13859, 30763, 28345, 8717, 53702,
    5299, 22059, 48280, 63098, 58958, 36383, 37373, 49845, 38717, 16128,
    15640, 23591, 4433, 3835, 55487, 5836, 33311, 27505, 25651, 43402,
    61379, 53235, 12645, 64552, 65446, 59058, 1340, 45935, 22080, 51286,
    48017, 53813, 57233, 23519, 31895, 55521, 40892, 1088, 62810, 19701,
    39062, 28576, 47603, 36856, 18383, 42319, 31576, 15479, 8328, 59923,
    7582, 47366, 48638, 61662, 58144, 28977, 60117, 9710, 50920, 51747,
    46228, 61127, 4528, 55822, 52335, 6178, 4771, 36101, 20624, 40482,
    25717, 58762, 39782, 43396, 4623, 39306, 60329, 20392, 24816, 25396,
    8129, 2618, 46056, 61318, 35986, 18311, 25652, 40785, 21085, 7397,
    12880, 4762, 54484, 25314, 63664, 13970, 62939, 5608, 6601, 54064,
    59142, 8009, 3591, 11280, 19695, 61729, 59015, 32754, 45871, 3290,
    19433, 8183, 4231, 28422, 50882, 37696, 10896, 26082, 7723, 8613,
    53170, 57337, 35285, 4231, 5948, 16689, 58781, 31764, 37428, 9029,
    9552, 44407, 46880, 38502, 61001, 2018, 60954, 58646, 33354, 37767,
    50545, 5627, 26917, 574, 49130, 21854, 30366, 65082, 20640, 43208,
    44135, 9382, 23578, 18294, 59098, 15115, 38461, 17743, 27392, 48426,
    24559, 59630, 3853, 38919, 8583, 43776, 26855, 54160, 16136, 1004,
    55732, 40570, 38002, 32326, 28483, 64447, 18970, 4073, 64905, 36639,
    54890, 12824, 59549, 21022, 12975, 10705, 21469, 30152, 57137, 9808,
    39578, 17116, 65253, 27325, 31421, 21117, 63356, 25620, 26619, 10587,
    63377, 64212, 24363, 50787, 5193, 27644, 58071, 35107, 23083, 44049,
    48394, 27405, 58627, 39890, 48204, 40470, 32619, 57068, 64399, 31474,
    18429, 52250, 52255, 17879, 46747, 61743, 3684, 47662, 7105, 59317,
    32777, 53396, 19673, 32115, 11446, 40660, 53072, 27206, 65443, 7083,
    5860, 16394, 16630, 36971, 42578, 58437, 14139, 29900, 23355, 35931,
    60660, 46687, 25018, 56785, 18673, 10402, 10637, 56299, 57248, 13700,
    57470, 46000, 39159, 19488, 976, 9881, 664, 60651, 22352, 21528,
    50900, 17389, 15495, 25035, 46923, 47798, 61821, 4905, 27374, 51618,
    38139, 47774, 62582, 4508, 2152, 10547, 23764, 45773, 30068, 56640,
    38410, 12361, 41522, 53039, 39945, 35292, 37060, 57865, 39657, 39912,
    37798, 22545, 45625, 7551, 58268, 18203, 23826, 38747, 29782, 6079,
    35621, 34579, 51082, 45932, 2564, 12538, 36748, 25335, 19377, 52514,
    16538, 7848, 49801, 10491, 8089, 30041, 42713, 13082, 50153, 24382,
    53811, 25272, 27759, 38237, 35968, 31662, 29884, 3722, 24619, 52094,
    55740, 24571, 54100, 57249, 15571, 10479, 54310, 41384, 35815, 49207,
    44105, 16093, 13336, 52809, 60339, 53021, 3734, 47347, 17234, 7692,
    33504, 3363, 9483, 39562, 21171, 53659, 61035, 22317, 18773, 45430,
    46642, 19159, 27920, 43167, 45340, 65482, 7632, 42503, 49186, 30910,
    47635, 59599, 42992, 60372, 25383, 38269, 53025, 45710, 28189, 41222,
    9385, 24633, 3906, 7900, 679, 50789, 9615, 51131, 57617, 64710,
    31101, 7954, 26712, 32636, 47767, 34401, 46021, 41212, 28573, 18320,
    12041, 58194, 33335, 49852, 40780, 5045, 8041, 30795, 22478, 980,
    5407, 15998, 59736, 33261, 23086, 6539, 29820, 19381, 62554, 36832,
    9491, 32019, 22197, 92, 54325, 49847, 44263, 10252, 49338, 64895,
    23932, 16029, 14297, 10123, 39083, 38528, 46088, 4651, 47666, 40431,
    51562, 58733, 65139, 15052, 25115, 21167, 58573, 27402, 39640, 56283,
    7643, 41028, 40662, 56612, 5047, 42285, 25006, 42712, 42833, 53663,
    3331, 42354, 20450, 39873, 32407, 38574, 15271, 3206, 46996, 8221,
    45779, 33372, 41151, 52783, 53695, 24456, 56802, 57006, 44513, 35250,
    32379, 3786, 19555, 59162, 26562, 34876, 6460, 39473, 11715, 37380,
    7500, 36982, 23482, 46258, 20607, 58188, 41060, 61495, 36327, 33664,
    65241, 38357, 53730, 4281, 43264, 31879, 10596, 38677, 64211, 1334,
    44108, 7453, 46896, 44681, 52995, 32110, 1139, 33406, 34762, 26605,
    10865, 10764, 9946, 43108, 398, 59861, 15177, 60048, 46236, 46565,
    36442, 16332, 39674, 11835, 45448, 9961, 50728, 64216, 5474, 58946,
    31808, 12320, 22172, 45027, 26475, 13269, 53172, 29448, 16380, 56479,
    26239, 64193, 13194, 4408, 37370, 14809, 11570, 7704, 15496, 32291,
    25326, 61649, 11466, 62421, 30471, 51967, 12999, 521, 62549, 13513,
    23373, 57066, 42401, 44102, 46994, 27238, 3292, 62373, 59003, 13135,
    11293, 59546, 65235, 13291, 39053, 29727, 61706, 41221, 62689, 40525,
    7131, 20764, 3702, 59260, 64389, 8568, 55996, 14802, 24515, 5109,
    64546, 40342, 62253, 2970, 39567, 8952, 36783, 22153, 42510, 32201,
    35228, 810, 50812, 44895, 58732, 9089, 39812, 59512, 9786, 3577,
    43220, 58583, 54342, 46428, 38357, 17635, 65034, 54365, 53819, 31377,
    15124, 49605, 23547, 26467, 16750, 54317, 17345, 46179, 958, 44152,
    44291, 44143, 55994, 6113, 49891, 10121, 12258, 27521, 25817, 7403,
    60393, 54725, 60090, 13461, 63805, 40141, 10321, 12430, 5298, 13670,
    64193, 1664, 64515, 35671, 59007, 52110, 36292, 23414, 14590, 13073,
    22619, 43114, 26313, 58562, 20746, 50945, 64115, 44299, 22408, 19400,
    60468, 55776, 55837, 64676, 2613, 38097, 26793, 25507, 45031, 55209,
    34133, 30468, 13415, 39024, 14309, 7114, 38358, 58370, 14109, 54745,
    12318, 80, 14976, 26979, 2069, 2160, 49272, 20286, 65511, 14824,
    18750, 55061, 22101, 54105, 26853, 30994, 9807, 44603, 51701, 58270,
    20589, 59150, 8445, 39587, 47314, 62973, 30803, 49921, 5132, 38281,
    9347, 55542, 28931, 2474, 5390, 22768, 12001, 38980, 15826, 47699,
    22727, 57311, 8751, 53523, 18408, 52154, 28612, 45046, 37886, 2261,
    23068, 20559, 31653, 30301, 34439, 36616, 5884, 14780, 48767, 55338,
    30824, 2842, 62220, 9285, 52879, 26453, 62295, 39923, 12544, 34742,
    24562, 22502, 56226, 802, 6567, 46417, 44392, 34723, 41499, 43456,
    2453, 10980, 33961, 23638, 6748, 58377, 597, 10161, 37854, 7781,
    29567, 34105, 47564, 16942, 24770, 25271, 42149, 7077, 28840, 3166,
    49084, 16764, 24041, 20839, 45903, 54786, 6100, 26443, 30707, 4798,
    34965, 32913, 3904, 6376, 51660, 10151, 19071, 8815, 35569, 27963
};
t_u16 RandomIndex = 0;

#define SIZE_egb_TempBuffer 100
t_u8 egb_TempBuffer[SIZE_egb_TempBuffer]; // buffer for use with printf()
t_base Times[10];

#define USART1_INDEX 1
const t_u32 Colors[16] = {
    TTC_GFX_COLOR24_BLACK,
    TTC_GFX_COLOR24_SILVER,
    TTC_GFX_COLOR24_GRAY,
    TTC_GFX_COLOR24_WHITE,
    TTC_GFX_COLOR24_MAROON,
    TTC_GFX_COLOR24_RED,
    TTC_GFX_COLOR24_PURPLE,
    TTC_GFX_COLOR24_FUCHSIA,
    TTC_GFX_COLOR24_GREEN,
    TTC_GFX_COLOR24_LIME,
    TTC_GFX_COLOR24_OLIVE,
    TTC_GFX_COLOR24_YELLOW,
    TTC_GFX_COLOR24_NAVY,
    TTC_GFX_COLOR24_BLUE,
    TTC_GFX_COLOR24_TEAL,
    TTC_GFX_COLOR24_AQUA
};

//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_gfx_benchmark_prepare() {

    egb_DisplayConfig = ttc_gfx_get_configuration( 1 );
    Assert( egb_DisplayConfig != NULL, ttc_assert_origin_auto );

    // set color to use for first screen clear
    ttc_gfx_color_bg24( TTC_GFX_COLOR24_AQUA );

    ttc_gfx_init( 1 );

    ttc_task_create( task_gfx_benchmark, // function to start as thread
                     "GFX_Benchmark",    // thread name (just for debugging)
                     512,                // stack size
                     ( void* ) NULL,     // passed as argument to taskLEDs()
                     1,                  // task priority (higher values mean more process time)
                     NULL                // can return a handle to created task
                   );
}
void task_gfx_benchmark( void* TaskArgument ) {
    ( void ) TaskArgument; // avoids warning: unused parameter 'TaskArgument'

    do {
        // This loop runs endlessly if scheduler is available.
        // Otherwise it will run only onve and be called periodically from ttc_task_start_scheduler().

        t_u8 Run = 0;

        if ( 1 )  { Times[Run++] = benchmark_Clear( 5 ); }      // Run0
        if ( 1 )  { Times[Run++] = benchmark_Circle( 5 ); }     // Run1
        if ( 1 )  { Times[Run++] = benchmark_CircleFill( 5 ); } // Run2
        if ( 1 )  { Times[Run++] = benchmark_Line_ortho( 5 ); } // Run3
        if ( 1 )  { Times[Run++] = benchmark_Line( 5 ); }       // Run4
        if ( 1 )  { Times[Run++] = benchmark_Rect( 5 ); }       // Run5
        if ( 1 )  { Times[Run++] = benchmark_Text( 5 ); }       // Run6
        if ( 1 )  { Times[Run++] = benchmark_SetPixel( 5 ); }   // Run7
        if ( 1 )  { Times[Run++] = benchmark_RectFill( 5 ); }   // Run8

        ttc_gfx_color_fg24( TTC_GFX_COLOR24_BLACK );
        ttc_gfx_color_bg24( TTC_GFX_COLOR24_WHITE );
        ttc_gfx_clear();

        const char* Title = "Gfx Benchmark";
        ttc_gfx_text_cursor_set( ( egb_DisplayConfig->TextColumns - ttc_string_length8( Title, -1 ) ) / 2, 0 );
        ttc_gfx_text( Title, -1 );

        t_u8 Index;
        t_base TotalTime = 0;
        for ( Index = 0; Index < Run; Index++ ) {
            ttc_gfx_text_cursor_set( 0, 1 + Index );
            ttc_string_snprintf( egb_TempBuffer, 30, "Run%i:%7i ms", Index, Times[Index] / 1000 );
            ttc_gfx_text( ( const char* ) egb_TempBuffer, -1 );
            TotalTime += Times[Index] / 1000;
        }

        ttc_gfx_text_cursor_set( 0, 1 + Index );
        ttc_string_snprintf( egb_TempBuffer, 30, "sum: %7i ms", TotalTime );
        ttc_gfx_text( ( const char* ) egb_TempBuffer, -1 );
        ttc_gfx_text_cursor_set( 0, 1 + Index );
        ttc_gfx_line_horizontal( egb_DisplayConfig->X, egb_DisplayConfig->Y, egb_DisplayConfig->Width );

        ttc_basic_test_ok();          // all tests passed successfully
        ttc_assert_halt_origin( ttc_assert_origin_auto ); // stop at end of benchmark

        ttc_task_yield(); // if scheduler available, give cpu to other processes
    }
    while ( TTC_TASK_SCHEDULER_AVAILABLE );
}

t_u16 getNextRandom() {
    if ( RandomIndex >= Random_Amount )
    { RandomIndex = 0; }

    return RandomData[RandomIndex++];
}
void resetRandom() {
    RandomIndex = 0;
}

t_base benchmark_Clear( t_u8 Count ) {
    t_base StartTime = ttc_systick_get_elapsed_usecs();

    for ( ; Count > 0; Count-- ) {
        for ( t_u8 Amount = 10; Amount > 0; Amount-- ) {
            ttc_gfx_color_bg24( Colors[getNextRandom() & 0xf] );
            ttc_gfx_clear();
        }
    }

    return ttc_systick_get_elapsed_usecs() - StartTime;
}
t_base benchmark_Line_ortho( t_u8 Count ) {
    ttc_gfx_color_fg24( TTC_GFX_COLOR24_WHITE );
    ttc_gfx_clear();
    t_base StartTime = ttc_systick_get_elapsed_usecs();

    t_s16 Width  = egb_DisplayConfig->Width;
    t_s16 Height = egb_DisplayConfig->Height;

    for ( ; Count > 0; Count-- ) {
        for ( t_u8 Amount = 10; Amount > 0; Amount-- ) {
            ttc_gfx_color_fg24( Colors[getNextRandom() & 0xf] );
            for ( t_s16 Y = Height - 1; Y > 0; Y -= 2 ) {
                ttc_gfx_line( 0, Y, Width, Y );
            }
            for ( t_s16 X = Width  - 1; X > 0; X -= 2 ) {
                ttc_gfx_line( X, 0, X, Height );
            }
        }
    }

    return ttc_systick_get_elapsed_usecs() - StartTime;
}
t_base benchmark_Line( t_u8 Count ) {
    ttc_gfx_color_bg24( TTC_GFX_COLOR24_WHITE );
    ttc_gfx_clear();
    t_base StartTime = ttc_systick_get_elapsed_usecs();

    t_s16 Width  = egb_DisplayConfig->Width;
    t_s16 Height = egb_DisplayConfig->Height;

    for ( ; Count > 0; Count-- ) {

        ttc_gfx_color_fg24( TTC_GFX_COLOR24_BLACK );
        for ( t_s16 Y = Height - 1; Y > 0; Y -= 2 ) {
            ttc_gfx_line( 0, Y, Width, Height - Y - 1 );
        }
        for ( t_s16 X = Width  - 1; X > 0; X -= 2 ) {
            ttc_gfx_line( X, 0, Width - X - 1, Height - 1 );
        }

        ttc_gfx_color_fg24( TTC_GFX_COLOR24_WHITE );
        for ( t_s16 Y = Height - 1; Y > 0; Y -= 2 ) {
            ttc_gfx_line( 0, Y, Width, Height - Y - 1 );
        }
        for ( t_s16 X = Width  - 1; X > 0; X -= 2 ) {
            ttc_gfx_line( X, 0, Width - X - 1, Height - 1 );
        }
    }

    return ttc_systick_get_elapsed_usecs() - StartTime;
}
t_base benchmark_Rect( t_u8 Count ) {
    ttc_gfx_color_bg24( TTC_GFX_COLOR24_WHITE );
    ttc_gfx_clear();
    t_base StartTime = ttc_systick_get_elapsed_usecs();

    for ( ; Count > 0; Count-- ) {
        for ( t_u8 Amount = 10; Amount > 0; Amount-- ) {
            ttc_gfx_color_fg24( Colors[getNextRandom() & 0xf] );
            if ( egb_DisplayConfig->Width < egb_DisplayConfig->Height ) {
                for ( t_s16 Offset = egb_DisplayConfig->Width / 2; Offset > 0; Offset-- ) {
                    ttc_gfx_rect( Offset, Offset, egb_DisplayConfig->Width - 2 * Offset, egb_DisplayConfig->Height - 2 * Offset );
                }
            }
            else {
                for ( t_s16 Offset = egb_DisplayConfig->Height / 2; Offset > 0; Offset-- ) {
                    ttc_gfx_rect( Offset, Offset, egb_DisplayConfig->Width - 2 * Offset, egb_DisplayConfig->Height - 2 * Offset );
                }
            }
        }
    }

    return ttc_systick_get_elapsed_usecs() - StartTime;
}
t_base benchmark_RectFill( t_u8 Count ) {
    ttc_gfx_color_bg24( TTC_GFX_COLOR24_WHITE );
    ttc_gfx_clear();
    t_base StartTime = ttc_systick_get_elapsed_usecs();
    t_u16 MaxSize = egb_DisplayConfig->Width * 20 / 100;

    for ( ; Count > 0; Count-- ) {
        resetRandom();

        for ( t_u16 I = 1500; I > 0; I-- ) {
            ttc_gfx_color_fg24( Colors[getNextRandom() & 0xf] );
            t_u8 Width  = getNextRandom() % MaxSize;
            t_u8 Height = getNextRandom() % MaxSize;
            t_s16 X = getNextRandom() % ( egb_DisplayConfig->Width - Width );
            t_s16 Y = getNextRandom() % ( egb_DisplayConfig->Height - Height );

            ttc_gfx_rect_fill( X, Y, Width, Height );
        }
    }

    return ttc_systick_get_elapsed_usecs() - StartTime;
}
t_base benchmark_SetPixel( t_u8 Count ) {
    ttc_gfx_color_bg24( TTC_GFX_COLOR24_WHITE );
    ttc_gfx_clear();
    t_base StartTime = ttc_systick_get_elapsed_usecs();
    t_u16 MaxX = egb_DisplayConfig->Width;
    t_u16 MaxY = egb_DisplayConfig->Height;

    for ( ; Count > 0; Count-- ) {
        resetRandom();
        ttc_gfx_color_fg24( Colors[Count & 0xf] );
        t_u16 Shift = 0;
        for ( t_u16 I = 4000; I > 0; I-- ) {

            t_s16 X = getNextRandom() % MaxX;
            t_s16 Y = getNextRandom() % MaxY;
            if ( ( I & 0x3ff ) == 0 ) { Shift++; }
            X += Shift;

            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
            ttc_gfx_pixel_set_at( X, Y );
        }
    }

    return ttc_systick_get_elapsed_usecs() - StartTime;
}
t_base benchmark_Circle( t_u8 Count ) {
    ttc_gfx_color_bg24( TTC_GFX_COLOR24_WHITE );
    ttc_gfx_clear();
    t_base StartTime = ttc_systick_get_elapsed_usecs();

    t_s16 Radius = ( egb_DisplayConfig->Width > egb_DisplayConfig->Height ) ? egb_DisplayConfig->Width / 2 : egb_DisplayConfig->Height / 2;

    t_u16 CenterX = egb_DisplayConfig->Width / 2;
    t_u16 CenterY = egb_DisplayConfig->Height / 2;

    for ( ; Count > 0; Count-- ) {

        ttc_gfx_color_fg24( TTC_GFX_COLOR24_BLACK );
        for ( t_s16 R = Radius - 1; R > 0; R-- ) {
            ttc_gfx_circle( CenterX, CenterY, R );
        }

        ttc_gfx_color_fg24( TTC_GFX_COLOR24_BLUE );
        for ( t_s16 R = Radius - 1; R > 0; R-- ) {
            ttc_gfx_circle( CenterX, CenterY, R );
        }

    }

    return ttc_systick_get_elapsed_usecs() - StartTime;
}
t_base benchmark_CircleFill( t_u8 Count ) {
    ttc_gfx_color_bg24( TTC_GFX_COLOR24_WHITE );
    ttc_gfx_clear();
    t_base StartTime = ttc_systick_get_elapsed_usecs();
    t_u16 MaxSize = egb_DisplayConfig->Width * 20 / 100;
    t_u8 Radius;
    t_s16 X, Y;

    for ( ; Count > 0; Count-- ) {
        resetRandom();

        for ( t_u16 I = 250; I > 0; I-- ) {
            ttc_gfx_color_fg24( Colors[getNextRandom() & 0xf] );
            Radius = getNextRandom() % MaxSize;
            X = getNextRandom() % ( egb_DisplayConfig->Width - Radius );
            Y = getNextRandom() % ( egb_DisplayConfig->Height - Radius );

            ttc_gfx_circle_fill( X, Y, Radius );
        }
    }

    return ttc_systick_get_elapsed_usecs() - StartTime;
}
t_base benchmark_Text( t_u8 Count ) {
    ttc_gfx_color_fg24( TTC_GFX_COLOR24_BLACK );
    ttc_gfx_color_bg24( TTC_GFX_COLOR24_WHITE );
    ttc_gfx_clear();

    t_base StartTime = ttc_systick_get_elapsed_usecs();

    for ( ; Count > 0; Count-- ) {

        ttc_gfx_color_fg24( TTC_GFX_COLOR24_BLACK );
        for ( t_s16 Y = egb_DisplayConfig->TextRows - 1; Y >= 0; Y-- ) {
            ttc_gfx_text_cursor_set( 0, Y );
            ttc_gfx_text( HelloWorld, -1 );
        }
        ttc_gfx_color_fg24( TTC_GFX_COLOR24_BLUE );
        for ( t_s16 Y = egb_DisplayConfig->TextRows - 1; Y >= 0; Y-- ) {
            ttc_gfx_text_cursor_set( 0, Y );
            ttc_gfx_text_solid( HelloWorld, -1 );
        }
        ttc_gfx_color_fg24( TTC_GFX_COLOR24_GREEN );
        for ( t_s16 Y = egb_DisplayConfig->TextRows - 1; Y >= 0; Y-- ) {
            ttc_gfx_text_cursor_set( 0, Y );
            ttc_gfx_text_boxed( HelloWorld, -1, 1 );
        }

        ttc_gfx_color_fg24( TTC_GFX_COLOR24_RED );
        ttc_gfx_text_cursor_set( egb_DisplayConfig->TextColumns / 2 - 2, egb_DisplayConfig->TextRows / 2 - 2 );
        for ( t_u16 Amount = 0; Amount < 200; Amount++ ) {
            t_u8 Direction = ( ( Amount & 0xf ) >> 2 ) & 3;
            char Character = ( Amount % 26 ) + 'a';
            switch ( Direction ) {
                case 0:  ttc_gfx_text_cursor_right( 1 ); break;
                case 1:  ttc_gfx_text_cursor_down( 1 );  break;
                case 2:  ttc_gfx_text_cursor_left( 1 );  break;
                default: ttc_gfx_text_cursor_up( 1 );    break;
            }
            ttc_gfx_char_solid( Character );
        }
    }

    return ttc_systick_get_elapsed_usecs() - StartTime;
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
