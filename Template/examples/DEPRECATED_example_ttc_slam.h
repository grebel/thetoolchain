#ifndef EXAMPLE_SLAM_H
#define EXAMPLE_SLAM_H

/** example_ttc_SLAM.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2016.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_slam.
 *  The basic flow is as follows:
 *  (1) example_ttc_slam_start() gets called in single tasking mode from ttc_extensions.c:startExtensions() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_slam_start() gets executed
 *
 *  Created from template example_ttc_device.h revision 13 at 20160607 10:19:25 UTC
 *
 *  Authors: <AUTHOR>
 *  
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_SLAM.c only do not belong here! 
#include "../ttc-lib/ttc_basic.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_slam_t
    u8_t LogicalIndex;  // logical index of device to use
    // more arguments...
} example_slam_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_slam_start();


//}Function prototypes

#endif //EXAMPLE_SLAM_H
