#ifndef EXAMPLE_PACKET_H
#define EXAMPLE_PACKET_H

/** example_ttc_PACKET.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_packet.
 *  The basic flow is as follows:
 *  (1) example_ttc_packet_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_packet_prepare() gets executed
 *
 *  Created from template example_ttc_device.h revision 12 at 20150928 09:30:04 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_PACKET.c only do not belong here!
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_packet.h"    // network packet handling
#include "../ttc-lib/ttc_heap.h"      // dynamic memory allocation + memory pools
#include "../ttc-lib/ttc_list.h"      // single linked list

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // t_etp_node_config - Configuration data of single network node
    t_ttc_packet_address LocalAddress;         // identification number + PanID of one node
    t_ttc_heap_pool*     Pool;                 // memory pool providing memory blocks for new packets
    t_ttc_list*          List;                 // single linked list serves as a virtual communication channel
    t_base               AmountTX_Packets;     // total amount of packets being sent (any type)
    t_base               AmountRX_Packets;     // total amount of packets being received (any type)
    t_base               AmountTX_Acknowledge; // total amount of acknowledgements sent
    t_base               AmountRX_Acknowledge; // total amount of acknowledgements received
    t_base               Amount_Dumped;        // total amount of packets being silently ignored
} t_etp_node_config; // etp = Example_ttc_Packett

typedef struct { // etp_data_t        - Structure of packet payload
    t_u8 Time2Live; // Set at packet creation. Decreased on every packet receiption. Packet is dumped when it reaches 0.

} etp_data_t; // etp = Example_ttc_Packett


//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_packet_prepare();


//}Function prototypes

#endif //EXAMPLE_PACKET_H
