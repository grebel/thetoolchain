#ifndef EXAMPLE_CRC_H
#define EXAMPLE_CRC_H

/** example_ttc_CRC.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for cyclic redundancy checksum implementations optimized for smallest ram and flash usage
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_crc_small_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started) 
 *  (2) Scheduler is started.
 *  (3) Tasks being created by example_ttc_crc_small_prepare() get executed.
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_crc_small_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) Every registered task function is called one after another in an endless loop
 *      from ttc_task_start_scheduler().
 *
 *  Created from template example_ttc_device.h revision 18 at 20180323 10:28:15 UTC
 *
 *  Authors: <AUTHOR>
 *
 *  
 *  Description of example_ttc_CRC
 *
 *  ToDo: Document this example!
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_CRC.c only do not belong here! 
// This is why header files typically include *_types.h files only (avoids nasty include loops).

#include "../ttc-lib/ttc_basic_types.h"    // basic datatypes for current microcontroller architecture
#include "../ttc-lib/ttc_gpio_types.h"     // allows to store gpio pins in structures 
#include "../ttc-lib/ttc_task_types.h"     // allows to store task delay data in structures
#include "../ttc-lib/ttc_crc_types.h" // datatypes of crc devices

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_crc_t
    t_ttc_crc_config* Config_CRC; // pointer to configuration of crc device to use
    t_ttc_systick_delay    Delay;           // this type of delay is usable in single- and multitasking setup

    // more arguments...
} example_crc_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_crc_prepare();


//}Function prototypes

#endif //EXAMPLE_CRC_H
