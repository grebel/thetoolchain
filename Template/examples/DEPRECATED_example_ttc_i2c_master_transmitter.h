/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx.h"
#include "stm32l1xx_dma.h"
#include "stm32l1xx_i2c.h"
#include "stm32l1xx_gpio.h"
#include "stm32l1xx_rcc.h"
#include "stm32l1xx_syscfg.h"
#include "misc.h"

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_task.h"


#define MPU6050_RA_WHO_AM_I         0x75

#define MPU6050_DEFAULT_ADDRESS     0x68
#define MPU6050_RA_PWR_MGMT_1       0x6B
#define MPU6050_RA_ACCEL_XOUT_H     0x3B
#define MPU6050_RA_ACCEL_XOUT_L     0x3C
#define MPU6050_RA_ACCEL_YOUT_H     0x3D
#define MPU6050_RA_ACCEL_YOUT_L     0x3E
#define MPU6050_RA_ACCEL_ZOUT_H     0x3F
#define MPU6050_RA_ACCEL_ZOUT_L     0x40


#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_spi_master_t
    u8_t Foo;  // example data
    // more arguments...
} example_i2c_master_t;

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_i2c_master_transmitter_start();

/** task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void task_I2C_Master(void *TaskArgument);
