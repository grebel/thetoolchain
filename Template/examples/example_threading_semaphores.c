/*{ ExampleThreading::.c ************************************************
 
 Example code demonstrating how to use inter thread communication.
 
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_threading_semaphores.h"

#define AmountProducers 1
#define AmountConsumers 6


#ifdef EXTENSION_ttc_gfx
  const char* Example    = "Example";
  const char* Threading  = "Threading";
  const char* Semaphores = "Semaphores";

  // configuration data of connected display
  t_ttc_gfx_generic* DisplayConfig = NULL;
#endif

//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

  void example_threading_semaphores_prepare() {

      // Note: This function is called with disabled task scheduler!

#ifdef TTC_LED1
          ttc_gpio_init(TTC_LED1, E_ttc_gpio_mode_output_push_pull);
#endif
#ifdef TTC_LED2
          ttc_gpio_init(TTC_LED2, E_ttc_gpio_mode_output_push_pull);
#endif

  #ifdef EXTENSION_ttc_gfx

      t_u8 MaxDisplay = ttc_gfx_get_max_display_index();
      Assert(MaxDisplay > 0, ttc_assert_origin_auto); // no displays defined by board makefile!
      DisplayConfig = ttc_gfx_get_configuration(1);
      Assert(DisplayConfig, ttc_assert_origin_auto);

      // set color to use for first screen clear
      ttc_gfx_color_bg24(GFX_COLOR24_BLUE);
      ttc_gfx_color_fg24(GFX_COLOR24_YELLOW);
      ttc_gfx_init(1);

      // initialize gfx for use with printf()
      ttc_gfx_stdout_set(1);
      ttc_string_register_stdout(ttc_gfx_stdout_print_block, ttc_gfx_stdout_print_string);

      ttc_gfx_text_solid_at(ttc_gfx_text_center(Example, -1),   0, Example,   -1);
      ttc_gfx_text_solid_at(ttc_gfx_text_center(Threading, -1), 1, Threading, -1);
      ttc_gfx_text_solid_at(ttc_gfx_text_center(Semaphores, -1), 2, Semaphores, -1);

  #endif

      static ets_ThreadInfo_t ThreadInfo; // must be static to survive end of this function!

      // create counting semaphore Producer -> Consumer
      ThreadInfo.SemaphoreProducer2Consumer = ttc_semaphore_create();
      Assert(NULL != ThreadInfo.SemaphoreProducer2Consumer, ttc_assert_origin_auto);

      // create queue Producer/Consumer -> Print
      ThreadInfo.QueuePrint = ttc_queue_generic_create(5, sizeof(t_et_printjob));
      Assert(NULL != ThreadInfo.QueuePrint, ttc_assert_origin_auto);

      ThreadInfo.DisplayRow = 4;

  #ifdef EXTENSION_ttc_gfx
      Assert(0 != ttc_task_create( taskPrint,                    //{ function to start as thread
                                   "Print",                      // thread name (just for debugging)
                                   TTC_TASK_MINIMAL_STACK_SIZE,  // stack size
                                   (void *) &ThreadInfo,         // passed as argument to taskValue()
                                   1,                            // task priority (higher values mean more process time)
                                   NULL                          // can return a handle to created task
                                   ),
             ttc_assert_origin_auto); //}
  #endif

      for (int Amount = 0; Amount < AmountProducers; Amount++) {
          Assert(0 != ttc_task_create( taskProducer,                          //{ function to start as thread
                                       "Producer",       // thread name (just for debugging)
                                       TTC_TASK_MINIMAL_STACK_SIZE, // stack size
                                       (void *) &ThreadInfo,                  // passed as argument to taskValue()
                                       1,            // task priority (higher values mean more process time)
                                       NULL                                   // can return a handle to created task
                                       ),
                 ttc_assert_origin_auto); //}

      }

      for (int Amount = 0; Amount < AmountConsumers; Amount++) {
          Assert(0 != ttc_task_create( taskConsumer,                          //{ function to start as thread
                                       "Consumer",       // thread name (just for debugging)
                                       TTC_TASK_MINIMAL_STACK_SIZE, // stack size
                                       (void *) &ThreadInfo,                  // passed as argument to taskValue()
                                       1,            // task priority (higher values mean more process time)
                                       NULL                                   // can return a handle to created task
                                       ),
                 ttc_assert_origin_auto); //}
      }
  }
void taskProducer(void *TaskArgument) {
    ets_ThreadInfo_t* ThreadInfoPtr = (ets_ThreadInfo_t*) TaskArgument;
    Assert(NULL != ThreadInfoPtr, ttc_assert_origin_auto);
    Assert(NULL != ThreadInfoPtr->SemaphoreProducer2Consumer, ttc_assert_origin_auto);
    Assert(NULL != ThreadInfoPtr->QueuePrint,                 ttc_assert_origin_auto);

#ifdef EXTENSION_ttc_gfx
    ttc_task_critical_begin(taskProducer);
    t_u8 DisplayRow = ThreadInfoPtr->DisplayRow++;
    ttc_task_critical_end();

    queuePrintJob(ThreadInfoPtr->QueuePrint, 0, 3, "Q=");
    t_base BytesFree = ttc_heap_get_free_size();
    queuePrintJobV(ThreadInfoPtr->QueuePrint, 2, DisplayConfig->TextRows - 1, "free: %iB", BytesFree);
    queuePrintJob(ThreadInfoPtr->QueuePrint, 2, DisplayRow, "P>");
#endif

    t_u8 Value = 0; //(t_u16) rand() & 0xfff;
    while (1) {
        Value++;
        if (Value == 100) Value = 0;
        for (int I = 0; I < Value; I+=10) {
            while ( ttc_semaphore_give(ThreadInfoPtr->SemaphoreProducer2Consumer, 1) );
        }
        ttc_task_msleep(800);

#ifdef EXTENSION_ttc_gfx
        queuePrintJobV(ThreadInfoPtr->QueuePrint, 6, DisplayRow, "+%i ", Value/10 + 1);
        displayQueue(ThreadInfoPtr->SemaphoreProducer2Consumer, ThreadInfoPtr->QueuePrint);
#endif
    }
}
void taskConsumer(void *TaskArgument) {
    ets_ThreadInfo_t* ThreadInfoPtr = (ets_ThreadInfo_t*) TaskArgument;
    Assert(NULL != ThreadInfoPtr, ttc_assert_origin_auto);
    Assert(NULL != ThreadInfoPtr->SemaphoreProducer2Consumer, ttc_assert_origin_auto);
    Assert(NULL != ThreadInfoPtr->QueuePrint,                 ttc_assert_origin_auto);

    t_u8 DisplayRow = ThreadInfoPtr->DisplayRow++;
    (void) DisplayRow;
#ifdef EXTENSION_ttc_gfx
    ttc_task_critical_begin(taskProducer);
    ttc_task_critical_end();

    queuePrintJob(ThreadInfoPtr->QueuePrint, 2, DisplayRow, "C>");
#endif
    t_u32 Counter = 0;
    while (1) {
        if (! ttc_semaphore_take(ThreadInfoPtr->SemaphoreProducer2Consumer, 1, -1, taskConsumer) ) { // ==0: take was successfull
            Counter++;
#ifdef TTC_LED1
            if (DisplayRow == 4) {
              for (int I = Counter; I >= 0; I--) {
                  ttc_gpio_set(TTC_LED1);
                  ttc_task_msleep(400);
                  ttc_gpio_clr(TTC_LED1);
                  ttc_task_msleep(400);
              }
            }
#endif
#ifdef TTC_LED2
            if (DisplayRow == 5) {
              for (int I = Counter; I >= 0; I--) {
                  ttc_gpio_set(TTC_LED2);
                  ttc_task_msleep(400);
                  ttc_gpio_clr(TTC_LED2);
                  ttc_task_msleep(400);
              }
            }
#endif
#ifdef EXTENSION_ttc_gfx
            queuePrintJob(ThreadInfoPtr->QueuePrint, 5, DisplayRow, "|");
            ttc_task_msleep(500);
            queuePrintJob(ThreadInfoPtr->QueuePrint, 5, DisplayRow, "-");
            ttc_task_msleep(500);
            queuePrintJobV(ThreadInfoPtr->QueuePrint, 7, DisplayRow, "%d", Counter);
            displayQueue(ThreadInfoPtr->SemaphoreProducer2Consumer, ThreadInfoPtr->QueuePrint);
#endif
        }
    }
}

#ifdef EXTENSION_ttc_gfx

void displayQueue(t_ttc_semaphore_smart* Semaphore, t_ttc_queue_generic QueuePrint) {
    char QueueText[11];
    char* Writer = QueueText;
    t_base QueueSize = ttc_queue_amount_waiting(Semaphore);
    t_u8 I = 0;
    for (; (I < QueueSize/3) && (I < 10); I++)
        *Writer++ = '*';
    for (; I < 10; I++)
        *Writer++ = ' ';
    *Writer++ = 0;
    queuePrintJob(QueuePrint, 2, 3, QueueText);
}
void queuePrintJobV(t_ttc_queue_generic PrintQueue, t_s8 Column, t_s8 Row, const char* Text, t_s32 Value) {
    t_et_printjob PrintJob;
    PrintJob.Column = Column;
    PrintJob.Row    = Row;
    PrintJob.Value  = Value;
    strncpy(PrintJob.Text, Text, 10);

    while (ttc_queue_generic_push_back(PrintQueue, &PrintJob, -1) != tqe_OK);
}
void queuePrintJob(t_ttc_queue_generic PrintQueue, t_s8 Column, t_s8 Row, const char* Text) {
    queuePrintJobV(PrintQueue, Column, Row, Text, 0);
}
void taskPrint(void *TaskArgument) {
    ets_ThreadInfo_t* ThreadInfoPtr = (ets_ThreadInfo_t*) TaskArgument;
    Assert(NULL != ThreadInfoPtr, ttc_assert_origin_auto);
    Assert(NULL != ThreadInfoPtr->SemaphoreProducer2Consumer, ttc_assert_origin_auto);
    Assert(NULL != ThreadInfoPtr->QueuePrint,                 ttc_assert_origin_auto);

    const t_u8 tP_BufferSize = 100;
    t_u8 Buffer[tP_BufferSize];

    while (1) {
        t_et_printjob PrintJob;
        if ( ttc_queue_generic_pull_front(ThreadInfoPtr->QueuePrint, &PrintJob,  -1) == tqe_OK ) {
            if ( (PrintJob.Column != -1) && (PrintJob.Row != -1) )
                ttc_gfx_text_cursor_set(PrintJob.Column, PrintJob.Row);

            // create fixed string
            ttc_string_snprintf(Buffer, tP_BufferSize, PrintJob.Text, PrintJob.Value);
            ttc_gfx_text_solid( (char*) Buffer, tP_BufferSize);
        }
    }
}

#endif

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
