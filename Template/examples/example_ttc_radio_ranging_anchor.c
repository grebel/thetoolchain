/** { example_ttc_radio_ranging_anchor.c ************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.c revision 11 at 20161130 06:10:02 UTC
 *
 *  Authors: gregor
}*/

/** { Includes ********************************************************
 *
 * Put all includes here that are required to compile this source code.
 */

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_radio_ranging_anchor.h"
#include "example_ttc_radio_ranging_common.h"
#include "../ttc-lib/ttc_radio.h"
#include "../ttc-lib/ttc_string.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Global Variables ************************************************
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** { Private Function Declarations ***********************************
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** { Function Definitions ********************************************
 */

void example_ttc_radio_ranging_prepare_anchor( etr_task_info_t* TaskInfo ) {
    // whoami?
    ttc_radio_change_local_address( 1, TaskInfo->LocalNodeID );

    // enable frame filter (only certain types of frames can pass)
    ttc_radio_configure_frame_filter( 1, ttc_radio_frame_filter_coordinator );  // receive ACKs + beacons + broadcasts + all packets in same PAN

    /** Set individual ranging reply delay for each anchor node.
     *  When a sensor node sends a ranging request as broadcast, every anchor node will
     *  answer after a different delay. This avoids reply collisions.
     *
     *  DELAY_MINIMUM = The minimum reply delay
     *  DELAY_SPACING = Minimum delay spacing (time between two incoming consecutive reply frames at requesting node.
     *                  DELAY_SPACING increase with greater distance variations.
     */

// Delay constants as measured experimentally for STM32L100 @32MHz
#define DELAY_SPACING 900
#define DELAY_MINIMUM 1400
    ttc_radio_set_ranging_delay( 1, DELAY_MINIMUM + ( TaskInfo->LocalNodeID->Address16 - 1 ) * DELAY_SPACING );

    example_ttc_radio_ranging_usart_send_stringf( "starting anchor node #%d ", TaskInfo->ConfigRadio->LocalID.Address16 );
    example_ttc_radio_ranging_usart_send_stringf( "on channel %d\n", TaskInfo->ConfigRadio->ChannelRX );

    t_ttc_packet_address TargetID;

    if ( 1 ) { // must send at least single packet to enable autoacknowledge feature of DW1000 (seen on DecaWave mailing list)
        ttc_string_snprintf( TaskInfo->PrepareBuffer, TaskInfo->PrepareBuffer_Size, "New Anchor 0x%04x", TaskInfo->LocalNodeID->Address16 );
        TargetID.Address16 = 0xffff; // broadcast address

        example_ttc_radio_ranging_transmit( TaskInfo,
                                            TaskInfo->PrepareBuffer,
                                            ttc_string_length8( ( char* ) TaskInfo->PrepareBuffer, TaskInfo->PrepareBuffer_Size ) + 1, // transmit string + string terminator
                                            &TargetID,
                                            E_ttc_packet_type_802154_Data_001010,
                                            &( TaskInfo->StringBuffer[0] ),
                                            SIZE_ETR_STRINGBUFFER
                                          );
    }

    /** An anchor node will wait for directed packets from sensor nodes and reply to them individually.
     *
     */
    example_ttc_radio_ranging_usart_send_string( "waiting for sensor nodes\n", -1 );
    ttc_radio_receiver( 1, 1, 0, 0 );  // enable receiver

    ttc_task_create(
        example_ttc_radio_ranging_task_anchor, // function to start as thread
        "tAnchor",                  // thread name (just for debugging)
        512,                        // stack size
        &TaskInfo,                  // passed as argument to example_ttc_radio_ranging_mainTask()
        1,                          // task priority (higher values mean more process time)
        NULL                        // can return a handle to created task
    );
}
void example_ttc_radio_ranging_task_anchor( void* TaskArgument ) { // implementation of anchor node task
    etr_task_info_t* TaskInfo = ( etr_task_info_t* ) TaskArgument;

    do {
        t_ttc_packet* PacketRX = ttc_radio_packet_received_tryget( 1 );
        if ( PacketRX ) { // handle received packet
            ttc_radio_receiver( 1, 1, 0, 0 );  // re-enable receiver

            if ( 1 ) { // assemble + print debug message
                t_u8* Append = &( TaskInfo->StringBuffer[0] );
                t_base StringBufferSize = SIZE_ETR_STRINGBUFFER;
                Append += ttc_string_appendf( Append, &StringBufferSize, "(0x%04x) RX: ", TaskInfo->LocalNodeID->Address16 );
                Append += ttc_packet_debug( PacketRX, Append, &StringBufferSize, TaskInfo->PacketDebug );
                Append += ttc_string_appendf( Append, &StringBufferSize, "\n" );
                example_ttc_radio_ranging_usart_send_string( ( char* ) & ( TaskInfo->StringBuffer[0] ), SIZE_ETR_STRINGBUFFER );
            }
            ttc_radio_packet_release( PacketRX );
        }
        else {
            ttc_radio_maintenance( 1 ); // let radio cleanup buffers and check transceiver state
            ttc_task_msleep( 100 );
        }
        ttc_task_yield();
    }
    while ( TTC_TASK_SCHEDULER_AVAILABLE );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
