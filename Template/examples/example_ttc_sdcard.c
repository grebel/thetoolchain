/** example_ttc_sdcard.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for secure digital card storage connected via spi bus
 *
 *  Basic flow of booting examples:
 *  (1) ttc_extensions.c:ttc_extensions_start() calls example_ttc_sdcard_prepare() to
 *      - initialize all required devices
 *      - initializes its global and local static variables
 *      - create all required task or statemachine instances
 *      - return immediately
 *
 *  Multitasking is available:
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_sdcard_prepare() get executed.
 *      Each task uses its private stack as given to ttc_task_create() before.
 *
 *  Multitasking is NOT available:
 *  (2) Minimalistic main loop is started
 *  (3) Main loop calls all registered statemachine functions in an endless loop.
 *      All statemachine functions use the same global stack that is maximum of all
 *      stack sizes given to ttc_task_create() before.
 *
 *  Created from template example_ttc_device.c revision 23 at 20180130 09:42:26 UTC
 *
 *  Authors: Gregor Rebel
 *
 */
/** Description of sdcard_spi (Do not delete this line!)
 *
 * This code shows how to
 * - detect presence of sdcard in selected sdcard slot
 * - wakeup and initialize (mount) sdcard
 * - read a single block
 * - write a single block (disabled by default)
 *
 * ttc_sdcard does only provide basic sdcard access and no filesystems.
 * Check example_ttc_disk for a complete filesystem on sdcard example!
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "example_ttc_sdcard.h"
#include "../compile_options.h"       // dynamically created file defining all "COMPILE_OPTS += -D..." constants from makefiles
#include "../ttc-lib/ttc_sdcard.h"  // high-level driver of device to use
#include "../ttc-lib/ttc_basic.h"     // basic datatypes and definitions
#include "../ttc-lib/ttc_heap.h"      // dynamic memory allocations and safe arrays
#include "../ttc-lib/ttc_task.h"      // allows to use application in single- and multitasking setup
#include "../ttc-lib/ttc_systick.h"   // exact delays and timeouts

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_sdcard_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _example_sdcard_task( void* Argument );

/** Statemachine running this example
 *
 * If no multitasking is available but multiple delays must be used in parallel,
 * a statemachine can be implemented. This is basically a function that memorizes a current
 * state number and uses a switch(state) {} structure to decide which code has to be executed
 * at the moment. Statemachine functions typically return quickly and have to be called regularly.
 * This behaviour allows to run multiple statemachines intermittently. Multiple statemachine
 * functions can be created via ttc_task_create() and will be called at maximum frequency one
 * after another in an endless loop.
 * See also example_ttc_states for a safer way to implement statemachines!
 */
void _example_sdcard_statemachine( void* Argument );

//}Private Function Declarations
//{ Function Definitions *************************************************

void example_ttc_sdcard_prepare() {                   // prepare this example for being started
    /** Note: The _prepare() function is called in single task mode before the scheduler is started.
      *       It has to return to allow all extensions to prepare.
      *       After all extensions finished their preparation, the created tasks are run.
      *       If no multitasking scheduler has been activated, all created tasks are run one after
      *       another in an endless loop. See ttc_task_start_scheduler() for details.
      *       This scheme allows to enable multiple statemachine functions in a flexible way.
      */

    if ( 1 ) { // create one instance of your example (copy and adjust for multiple instances)

        // LogicalIndex = 1: use device defined as TTC_SDCARD1 in makefile
        t_u8 LogicalIndex = 1;

        // allocate data of instance as local static (allows compiler to detect if it fits into RAM)
        static example_sdcard_data_t DaE_ttc_sdcard_architecture_1;

        // reset data
        ttc_memory_set( &DaE_ttc_sdcard_architecture_1, 0, sizeof( DaE_ttc_sdcard_architecture_1 ) );

        // loading configuration of first sdcard device
        DaE_ttc_sdcard_architecture_1.Config_SDCARD = ttc_sdcard_get_configuration( LogicalIndex );


        // change configuration before initializing device
        //
        // DaE_ttc_sdcard_architecture_1.Config_SDCARD->  = ;
        //...

        // initializing our sdcard device here makes debugging easier (still single-tasking)
        // Logical index is stored inside each device configuration
        ttc_sdcard_init( LogicalIndex );

#ifdef TTC_LED1
        ttc_gpio_init( TTC_LED1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min );
        ttc_gpio_clr( TTC_LED1 );
#endif
        // Register a function to be run after all system has finished booting.
        // ttc_task_create() works in single- and multitasking environments.
        ttc_task_create(
#if (TTC_TASK_SCHEDULER_AVAILABLE)
            _example_sdcard_task,           // function to start as thread
#else
            _example_sdcard_statemachine,   // function to run periodically (single tasking)
#endif
            "tSDCARD",                     // thread name (just for debugging)
            256,                             // stack size (adjust to amount of local variables of _example_sdcard_task() AND ALL ITS CALLED FUNTIONS!)
            &DaE_ttc_sdcard_architecture_1, // passed as argument to _example_sdcard_task()
            1,                               // task priority (higher values mean more process time)
            NULL                             // can return a handle to created task
        );
    }
}

// Note: If you implement both of the example functions below, your example will run in single- and multitasking configuration.

void _example_sdcard_statemachine( void* Argument ) { // example statemachine will be run if no multitasking scheduler is available
    Assert_SDCARD_Writable( Argument, ttc_assert_origin_auto );                // always check pointer arguments with Assert() before dereferencing them!
    example_sdcard_data_t* Data = ( example_sdcard_data_t* ) Argument; // we're expecting this pointer type

    // get pointer to device configuration
    t_ttc_sdcard_config* Config_SDCARD = Data->Config_SDCARD;
    ( void ) Config_SDCARD;

    if ( ttc_systick_delay_expired( & Data->Delay ) ) { // example periodic delay
        ttc_systick_delay_init( & Data->Delay,
                                ttc_systick_delay_get_minimum() << 4 // minimum delay * 16 is a safe way to always use a small and valid delay
                              ); // set new delay time

        e_ttc_storage_event SDCardEvent = ttc_sdcard_medium_detect( 1 );
        if ( SDCardEvent == E_ttc_storage_event_media_inserted ) { // new card has been inserted
            ttc_gpio_set( TTC_LED1 );
            if ( 1 ) { // access sdcard
                const t_u32 BlockAddress = 200; // address of block to read/write

                volatile e_ttc_sdcard_errorcode Error_SDCARD;
                Error_SDCARD = ttc_sdcard_medium_mount( 1 );
                if ( !Error_SDCARD ) { // mounted sdcard without error
                    ttc_systick_delay_simple( 100000 ); // simulate sdcard operation
                    Error_SDCARD = ttc_sdcard_block_read( 1, BlockAddress, Data->Block, ESD_BLOCK_SIZE );
                    if ( 0 ) { // test ttc_sdcard_block_write() - CAUTION: ENABLING THIS CLAUSE WILL CHANGE DATA ON CARD!
                        if ( !Error_SDCARD ) {
                            static t_u8 BlockChangeCounter = 5; // safety: limit amount of write operations to same block to avoid destroying flash cells.
                            if ( BlockChangeCounter ) {
                                // using first two bytes in block as a counter
                                t_u16* Counter = ( t_u16* ) Data->Block;

                                ( *Counter )++;

                                Error_SDCARD = ttc_sdcard_block_write( 1, BlockAddress, Data->Block, ESD_BLOCK_SIZE );
                                BlockChangeCounter--;
                            }
                        }
                    }
                    ttc_sdcard_medium_unmount( 1 );
                }
            }
            ttc_gpio_clr( TTC_LED1 );
        }
    }
    ttc_systick_delay_simple( 100000 );

    // implement statemachine here ...
}
void _example_sdcard_task( void* Argument ) {         // example task will be run when multitasking scheduler (e.g. FreeRTOS) has been started
    Assert_SDCARD_Writable( Argument, ttc_assert_origin_auto );                // always check pointer arguments with Assert() before dereferencing them!
    example_sdcard_data_t* Data = ( example_sdcard_data_t* ) Argument; // we're expecting this pointer type

    // get pointer to device configuration (remove if not required)
    t_ttc_sdcard_config* Config_SDCARD = Data->Config_SDCARD;
    ( void ) Config_SDCARD;

    do {
        // This loop runs endlessly.

        // call statemachine or implement task here...
        _example_sdcard_statemachine( Argument );

        ttc_task_yield(); // give cpu to other processes
    }
    while ( 1 ); // Tasks run endlessly.
}

//}Function Definitions
