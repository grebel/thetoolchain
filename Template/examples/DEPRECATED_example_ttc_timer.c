#include "example_ttc_timer.h"

//{ Global variables
#define INDEX_TIMER1 2
#define INDEX_TIMER2 3

ttc_timer_config_t * Config_tim2;
ttc_timer_config_t * Config_tim3;

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_timer_init() {
    ttc_gpio_init(TTC_LED1, tgm_output_push_pull, tgs_Max);

#ifdef TTC_LED2
    ttc_gpio_init(TTC_LED2, tgm_output_push_pull, tgs_Max);
#endif

#ifdef EXTENSION_450_cpu_stm32l1xx

    SystemCoreClock = ttc_sysclock_get_frequency();

    u16_t PrescalerValue = (u16_t) (SystemCoreClock / 8000000) - 1;

    Config_tim2 = ttc_timer_get_configuration(INDEX_TIMER1);
    Config_tim2->Flags.Bits.OutputPWM = 0b1;
    Config_tim2->LowLevelConfig->Period = (80-1);
    Config_tim2->LowLevelConfig->Prescaler = PrescalerValue;
    Config_tim2->LowLevelConfig->ClockDivision = 0;
    Config_tim2->LowLevelConfig->CounterMode = 0x0000;

    /* PWM1 Mode configuration: Channel1 */
    Config_tim2->Flags.Bits.PWM_Channel= 0; // We choose pwm channel number one
    Config_tim2->LowLevelConfig->oc_Mode =0x0060;
    Config_tim2->LowLevelConfig->oc_OutputState =0x0001;
    Config_tim2->LowLevelConfig->oc_Polarity =0x0000;
    Config_tim2->LowLevelConfig->oc_Pulse = 20;
    ttc_timer_init(INDEX_TIMER1);



    Config_tim3 = ttc_timer_get_configuration(INDEX_TIMER2);
    Config_tim3->Flags.Bits.OutputPWM = 0b1;
    Config_tim3->LowLevelConfig->Period = (665);
    Config_tim3->LowLevelConfig->Prescaler = PrescalerValue;
    Config_tim3->LowLevelConfig->ClockDivision = 0;
    Config_tim3->LowLevelConfig->CounterMode = 0x0000;

    /* PWM1 Mode configuration: Channel1 */
    Config_tim3->Flags.Bits.PWM_Channel= 0; // We choose pwm channel number one
    Config_tim3->LowLevelConfig->oc_Mode =0x0060;
    Config_tim3->LowLevelConfig->oc_OutputState =0x0001;
    Config_tim3->LowLevelConfig->oc_Polarity =0x0000;
    Config_tim3->LowLevelConfig->oc_Pulse = 333;
    ttc_timer_init(INDEX_TIMER2);

#endif

}
void example_ttc_timer_start() {

    example_ttc_timer_init();

    //Check that you have TTC_TIMERx=x defined on your makefile
    ttc_timer_set(INDEX_TIMER1, ChangeHighLED1, (void *) NULL, 2000000);
    ttc_timer_set(INDEX_TIMER2, ChangeLowLED2,  (void *) NULL, 5001);

    while(1);
}
void ChangeHighLED1(void *TaskArgument){
    (void) TaskArgument;   //To Avoid Warnings
    ttc_gpio_set(TTC_LED1);
    ttc_timer_deinit(INDEX_TIMER1);
//    ttc_timer_change_period(INDEX_TIMER1, 1000000);
//    ttc_timer_reload(INDEX_TIMER1);
    ttc_timer_set(INDEX_TIMER1, ChangeLowLED1, (void *) NULL, 2000000);
}
void ChangeLowLED1(void *TaskArgument){
    (void) TaskArgument;    //To Avoid Warnings
    ttc_gpio_clr(TTC_LED1);
    ttc_timer_deinit(INDEX_TIMER1);
//    ttc_timer_change_period(INDEX_TIMER2, 1000000);
//    ttc_timer_reload(INDEX_TIMER1);
    ttc_timer_set(INDEX_TIMER1, ChangeHighLED1, (void *) NULL, 2000000);
}
void ChangeHighLED2(void *TaskArgument){
   (void) TaskArgument;     //To Avoid Warnings
   ttc_gpio_set(TTC_LED2);
   ttc_timer_deinit(INDEX_TIMER2);
//   ttc_timer_change_period(INDEX_TIMER2, 1000000);
//   ttc_timer_reload(INDEX_TIMER2);
   ttc_timer_set(INDEX_TIMER2, ChangeLowLED2,  (void *) NULL, 5001);
}
void ChangeLowLED2(void *TaskArgument){
    (void) TaskArgument;    //To Avoid Warnings
    ttc_gpio_clr(TTC_LED2);
    ttc_timer_deinit(INDEX_TIMER2);
//    ttc_timer_change_period(INDEX_TIMER2, 1000000);
//    ttc_timer_reload(INDEX_TIMER2);
    ttc_timer_set(INDEX_TIMER2, ChangeHighLED2,  (void *) NULL, 5001);
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
