/*{ Template_FreeRTOS::.c ************************************************

     Several different boxes run over the screen.
     Each box is controlled by one task.

     written 2012 by Ioanna Tsekoura, Gregor Rebel

}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_gfx_boxes.h"
#include "../ttc-lib/ttc_heap.h"
#include "../ttc-lib/ttc_queue.h"
#include "../ttc-lib/ttc_math.h"
#include "../ttc-lib/ttc_systick.h"
#include "../ttc-lib/ttc_string.h"

//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************
const char* TaskNames[10] = { "tMoveBox1", "tMoveBox2", "tMoveBox3", "tMoveBox4", "tMoveBox5", "tMoveBox6", "tMoveBox7", "tMoveBox8", "tMoveBox9", "tMoveBox10" };
const char** LastTaskName = NULL;
t_ttc_gfx_config* DisplayConfig = NULL;
t_u32 ColorBg = TTC_GFX_COLOR24_WHITE;

// size of area in which to draw boxes (start at (0,0))
t_u32 DrawAreaWidth  = 0;
t_u32 DrawAreaHeight = 0;
t_u32 TimeBase_usecs = 20000; // box speed is pixel/TimeBase_usecs
const t_u16 MathMultiplier = 1; // coordinates, speed are multiplied by this factor for increased mathematical accuracy

#define NumberofBoxes 14
DrawData_t* AllPolygons[NumberofBoxes];
DrawData_t** LastPolygon = AllPolygons;

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_gfx_boxes_prepare() {

    LastTaskName = TaskNames;

    DisplayConfig = ttc_gfx_get_configuration( 1 );
    Assert_Writable( DisplayConfig, ttc_assert_origin_auto );

    // set color to use for first screen clear
    ttc_gfx_color_bg24( ColorBg );

    ttc_gfx_init( 1 );

    // allow text display only in last line of display
    DisplayConfig->TextBorderTop = DisplayConfig->Height - DisplayConfig->Font->CharHeight;

    DrawAreaWidth  = DisplayConfig->Width         * MathMultiplier;
    DrawAreaHeight = DisplayConfig->TextBorderTop * MathMultiplier;

    // print text in last line
    ttc_gfx_text_cursor_set_precise( 0, DisplayConfig->Height - DisplayConfig->Font->CharHeight );
    ttc_gfx_text( "Boxes/s:", -1 );

    t_ttc_queue_generic DrawQueue = ttc_queue_generic_create( 10, sizeof( DrawData_t ) );
    Assert( NULL != DrawQueue, ttc_assert_origin_auto );

    ttc_random_seed( 32 );

    for ( t_s16 I = 1; I <= NumberofBoxes; I++ ) {
        DrawData_t* Box = newRandomBox( DrawQueue );
        Assert( Box != NULL, ttc_assert_origin_auto );
    }

    ttc_task_create( tDrawIt,                      // function to start as thread
                     "tDrawIt",                             // thread name (just for debugging)
                     1 * TTC_TASK_MINIMAL_STACK_SIZE,          // stack size
                     DrawQueue,                   // passed as argument to tDrawIt()
                     1,                                     // task priority (higher values mean more process time)
                     NULL                         // can return a handle to created task
                   );

}
DrawData_t* newRandomBox( t_ttc_queue_generic DrawingQueue ) {
    ttc_gfx_color_fg24( ( ( ttc_random_generate() & 0xff ) << 16 ) | ( ( ttc_random_generate() & 0xff ) << 8 ) | ( ttc_random_generate() & 0xff ) );

    return newBox( DrawingQueue,
                   DisplayConfig->ColorFg,
                   0 + ttc_random_generate() % ( DrawAreaWidth  / MathMultiplier ),
                   0 + ttc_random_generate() % ( DrawAreaHeight / MathMultiplier ),
                   3 + ttc_random_generate() % 20,
                   3 + ttc_random_generate() % 20,
                   ( ttc_random_generate() % 20 ) - 10,
                   ( ttc_random_generate() % 20 ) - 10
                 );
}
DrawData_t* newBox( t_ttc_queue_generic DrawingQueue, t_u16 Color, t_s32 X, t_s32 Y, t_u16 Width, t_u16 Height, t_s8 SpeedX, t_s8 SpeedY ) {
    drawTaskArgs_t* NewBox = ( drawTaskArgs_t* ) ttc_heap_alloc_zeroed( sizeof( drawTaskArgs_t ) );

    if ( NewBox != NULL ) {
        NewBox->DrawQueue = DrawingQueue;
        NewBox->Box2Draw.Type = ddt_MovingBox;
        NewBox->Box2Draw.Polygon.MovingBox.Color  = Color;
        NewBox->Box2Draw.Polygon.MovingBox.PosX   = X      * MathMultiplier;
        NewBox->Box2Draw.Polygon.MovingBox.PosY   = Y      * MathMultiplier;
        NewBox->Box2Draw.Polygon.MovingBox.Width  = Width  * MathMultiplier;
        NewBox->Box2Draw.Polygon.MovingBox.Height = Height * MathMultiplier;
        NewBox->Box2Draw.Polygon.MovingBox.SpeedX = SpeedX * MathMultiplier;
        NewBox->Box2Draw.Polygon.MovingBox.SpeedY = SpeedY * MathMultiplier;

        if ( ttc_task_create( tMoveBox,                     // function to start as thread
                              ( const char* ) *LastTaskName++,    // thread name (just for debugging)
                              2 * TTC_TASK_MINIMAL_STACK_SIZE, // stack size
                              NewBox,                      // passed as argument to task-function
                              1,      // task priority (higher values mean more process time)
                              NULL                               // can return a handle to created task
                            )
           ) {
            *LastPolygon++ = &( NewBox->Box2Draw );
            return &( NewBox->Box2Draw );
        }
    }

    // Error: could not create task (perhaps out of memory)
    return NULL;
}

/** calculate effect of collision of two moving boxes on their SpeedX,SpeedY
 *
 * @param Box1  first box colliding
 * @param Box2  second box colliding
 */
void collide( elb_MovingBox_t* Box1, elb_MovingBox_t* Box2 ) {
    // reference: http://de.wikipedia.org/wiki/Sto%C3%9F_(Physik)


    if ( 0 ) {

        // calculate line slope Moving-Target (using top-left corner instead of center points for simplicity)
        t_s16 CentralLineX = Box2->PosX - Box1->PosX;
        t_s16 CentralLineY = Box2->PosY - Box1->PosY;

        // calculate line being tangent to CentralLine
        //X t_s16 CentralTangentX = - CentralLineY;
        //X t_s16 CentralTangentY = CentralLineX;

        ttm_number dX = 1.0 * CentralLineX;
        ttm_number dY = 1.0 * CentralLineY;
        ttm_number CentralLineAngle, CentralLineLength;
        ttc_math_cartesian2polar( dX, dY, &CentralLineAngle, &CentralLineLength );

        dX = 1.0 * Box1->SpeedX;
        dY = 1.0 * Box1->SpeedY;
        ttm_number SpeedAngle, SpeedLength;
        ttc_math_cartesian2polar( dX, dY, &SpeedAngle, &SpeedLength );

        // rotate speedangle
        SpeedAngle -= CentralLineAngle;

        ttm_number Speed2X, Speed2Y;
        ttc_math_polar2cartesian( SpeedAngle, SpeedLength, &Speed2X, &Speed2Y );

        ttm_number ImpulseOnTarget  = Speed2X / Speed2Y; // ==0.0: no impact on Target; ==1.0: full impact on target
        ttm_number ImpulseRemaining = 1.0 - ImpulseOnTarget;
        ( void ) ImpulseRemaining;
    }

    t_s32 Mass1 = Box1->Width * Box1->Height;
    t_s32 Mass2 = Box2->Width * Box2->Height;
    t_s32 CommonX = ( 2 * ( Mass1 * Box1->SpeedX + Mass2 * Box2->SpeedX ) ) / ( Mass1 + Mass2 );
    t_s32 CommonY = ( 2 * ( Mass1 * Box1->SpeedY + Mass2 * Box2->SpeedY ) ) / ( Mass1 + Mass2 );

    if ( 0 ) {
        Assert( CommonX != Box1->SpeedX, ttc_assert_origin_auto ); // resulting speed == 0
        Assert( CommonY != Box1->SpeedY, ttc_assert_origin_auto ); // resulting speed == 0
        Assert( CommonX != Box2->SpeedX, ttc_assert_origin_auto ); // resulting speed == 0
        Assert( CommonY != Box2->SpeedY, ttc_assert_origin_auto ); // resulting speed == 0
    }

    Box1->SpeedX = CommonX - Box1->SpeedX;
    Box1->SpeedY = CommonY - Box1->SpeedY;
    Box2->SpeedX = CommonX - Box2->SpeedX;
    Box2->SpeedY = CommonY - Box2->SpeedY;
}
#define pointIsInBox(X, Y, Left, Right, Top, Bottom) ( (X >= Left) && (X <= Right) && (Y >= Top) && (Y <= Bottom) )

void tMoveBox( void* TaskArgument ) {

    drawTaskArgs_t* Data = ( drawTaskArgs_t* ) TaskArgument;
    t_ttc_queue_generic DrawQueue = Data->DrawQueue;

    Assert( NULL != DrawQueue, ttc_assert_origin_auto );

    DrawData_t* ThisPolygon = &( Data->Box2Draw );
    elb_MovingBox_t* ThisBox = &( ThisPolygon->Polygon.MovingBox );

    DrawData_t DrawClearBox;
    DrawClearBox.Type = ddt_MovingBox;
    elb_MovingBox_t* ClearBox = &( DrawClearBox.Polygon.MovingBox );
    ClearBox->Color  = ColorBg;
    ClearBox->SpeedX = 0;
    ClearBox->SpeedY = 0;

    DrawData_t DrawClearLPolygon;
    DrawClearLPolygon.Type = ddt_L_Polygon;
    elb_L_Polygon_t* ClearLPolygon = &( DrawClearLPolygon.Polygon.L_Polygon );
    ClearLPolygon->Color  = ColorBg;

    DrawData_t DrawLPolygon;
    DrawLPolygon.Type = ddt_L_Polygon;
    elb_L_Polygon_t* LPolygon = &( DrawLPolygon.Polygon.L_Polygon );

    queueInsert( DrawQueue, ThisPolygon ); // Drawing of the Box(es) in the initial position(s).
    BOOL DrawFullBoxes = FALSE; // select update implementation to use

    // shrink screen borders according to box size
    t_s32 Border_Right = ( DrawAreaWidth  - ThisBox->Width );
    t_s32 Border_Lower = ( DrawAreaHeight - ThisBox->Height );

    t_s32 PosX = ThisBox->PosX;
    t_s32 PosY = ThisBox->PosY;
    t_s32 NewPosX = PosX;
    t_s32 NewPosY = PosY;

    do {
        // This loop runs endlessly if scheduler is available.
        // Otherwise it will run only onve and be called periodically from ttc_task_start_scheduler().

        if ( ttc_systick_delay_expired( & Data->Delay ) ) { // delay passed: calculate new position + collisions
            ttc_systick_delay_init( & Data->Delay, TimeBase_usecs );

            if ( 1 ) { ttc_task_critical_begin(); }
            PosX = ThisBox->PosX;
            PosY = ThisBox->PosY;
            NewPosX = PosX + ThisBox->SpeedX;
            NewPosY = PosY + ThisBox->SpeedY;

            if ( 1 ) { // collision with other boxes
                t_s16 MovementLeft   = MIN( PosX, NewPosX ) ;
                t_s16 MovementRight  = MAX( NewPosX + ThisBox->Width, PosX + ThisBox->Width );
                t_s16 MovementTop    = MIN( PosY, NewPosY );
                t_s16 MovementBottom = MAX( NewPosY + ThisBox->Height, PosY + ThisBox->Height );

                DrawData_t** Reader = AllPolygons;
                while ( Reader != LastPolygon ) {
                    DrawData_t* OtherPolygon = *Reader++;
                    if ( ThisPolygon != OtherPolygon ) {
                        //X Assert(ttc_memory_is_writable(OtherPolygon), ttc_assert_origin_auto);
                        elb_MovingBox_t* OtherBox = &( OtherPolygon->Polygon.MovingBox );

                        t_s16 OtherBoxLeft   = OtherBox->PosX;
                        t_s16 OtherBoxRight  = OtherBox->PosX + OtherBox->Width;
                        t_s16 OtherBoxTop    = OtherBox->PosY;
                        t_s16 OtherBoxBottom = OtherBox->PosY + OtherBox->Height;

                        BOOL OtherBoxSeen = FALSE;

                        OtherBoxSeen = pointIsInBox( OtherBoxLeft,  OtherBoxTop,    MovementLeft, MovementRight, MovementTop, MovementBottom ) ||
                                       pointIsInBox( OtherBoxLeft,  OtherBoxBottom, MovementLeft, MovementRight, MovementTop, MovementBottom ) ||
                                       pointIsInBox( OtherBoxRight, OtherBoxTop,    MovementLeft, MovementRight, MovementTop, MovementBottom ) ||
                                       pointIsInBox( OtherBoxRight, OtherBoxBottom, MovementLeft, MovementRight, MovementTop, MovementBottom );

                        if ( OtherBoxSeen ) { // found other box within movement box
                            if (
                                (
                                    ( ( OtherBox->PosX < ThisBox->PosX )  && ( ThisBox->SpeedX < 0 ) ) ||
                                    ( ( OtherBox->PosX > ThisBox->PosX )  && ( ThisBox->SpeedX > 0 ) )
                                ) ||
                                (
                                    ( ( OtherBox->PosY < ThisBox->PosY )  && ( ThisBox->SpeedY < 0 ) ) ||
                                    ( ( OtherBox->PosY > ThisBox->PosY )  && ( ThisBox->SpeedY > 0 ) )
                                )
                            ) {
                                // this box is moving towards other box: collide
                                if ( 1 ) { collide( ThisBox, OtherBox ); }
                                NewPosX += ThisBox->SpeedX;
                                NewPosY += ThisBox->SpeedY;
                            }
                        }
                    }
                }
            }
            if ( 1 ) { // collision with borders

                if ( NewPosX > Border_Right ) {
                    ThisBox->SpeedX = -ThisBox->SpeedX;
                    t_s32 Excess = NewPosX - Border_Right;
                    NewPosX -= Excess;
                }
                else if ( NewPosX < 0 ) {
                    ThisBox->SpeedX = -ThisBox->SpeedX;
                    NewPosX = -NewPosX;
                }
                if ( NewPosY > Border_Lower ) {
                    ThisBox->SpeedY = -ThisBox->SpeedY;
                    t_s32 Excess = NewPosY - Border_Lower;
                    NewPosY -= Excess;
                }
                else if ( NewPosY < 0 ) {
                    ThisBox->SpeedY = -ThisBox->SpeedY;
                    NewPosY = -NewPosY;
                }
            }
            if ( 1 ) { ttc_task_critical_end(); }
        }

        // create draw instructions to move box
        if ( DrawFullBoxes ) { // 1st implementation, clear the previous box and redraw all of it
            ClearBox->PosX = PosX;
            ClearBox->PosY = PosY;
            ClearBox->Width  = ThisBox->Width;
            ClearBox->Height = ThisBox->Height;
            queueInsert( DrawQueue, &DrawClearBox );

            ThisBox->PosX = NewPosX;
            ThisBox->PosY = NewPosY;
            queueInsert( DrawQueue, ThisPolygon ); // draw box as indicator of implementation switch
        }
        else {                 // 2nd implementation: clear as few pixels as possible

            if ( ( ThisBox->PosX == NewPosX ) && ( ThisBox->PosY == NewPosY ) ) {
                // clear nothing
                ThisBox->PosX = NewPosX;
                ThisBox->PosY = NewPosY;
                queueInsert( DrawQueue, ThisPolygon );
            }
            else if ( ( ThisBox->PosX == NewPosX ) && ( ThisBox->PosY < NewPosY ) ) {
                if ( ( ThisBox->PosY + ThisBox->Height ) < NewPosY ) {
                    ClearBox->PosX   = PosX;
                    ClearBox->PosY   = PosY;
                    ClearBox->Width  = ThisBox->Width;
                    ClearBox->Height = ThisBox->Height;
                    queueInsert( DrawQueue, &DrawClearBox );

                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                    queueInsert( DrawQueue, ThisPolygon );
                }
                else {
                    ClearBox->PosX   = PosX;
                    ClearBox->PosY   = PosY;
                    ClearBox->Width  = ThisBox->Width;
                    ClearBox->Height = NewPosY - PosY;
                    queueInsert( DrawQueue, &DrawClearBox );

                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                    queueInsert( DrawQueue, ThisPolygon );
                }
            }
            else if ( ( ThisBox->PosX == NewPosX ) && ( ThisBox->PosY > NewPosY ) ) {
                if ( ( NewPosY + ThisBox->Height ) < ThisBox->PosY ) {
                    ClearBox->PosX   = PosX;
                    ClearBox->PosY   = PosY;
                    ClearBox->Width  = ThisBox->Width;
                    ClearBox->Height = ThisBox->Height;
                    queueInsert( DrawQueue, &DrawClearBox );

                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                    queueInsert( DrawQueue, ThisPolygon );
                }
                else {
                    ClearBox->PosX   = PosX;
                    ClearBox->PosY   = NewPosY + ThisBox->Height;
                    ClearBox->Width  = ThisBox->Width;
                    ClearBox->Height = PosY - NewPosY;
                    queueInsert( DrawQueue, &DrawClearBox );

                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                    queueInsert( DrawQueue, ThisPolygon );
                }
            }
            else if ( ( ThisBox->PosX < NewPosX ) && ( ThisBox->PosY == NewPosY ) ) {
                if ( ( ThisBox->PosX + ThisBox->Width ) < NewPosX ) {
                    ClearBox->PosX   = PosX;
                    ClearBox->PosY   = PosY;
                    ClearBox->Width  = ThisBox->Width;
                    ClearBox->Height = ThisBox->Height;
                    queueInsert( DrawQueue, &DrawClearBox );

                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                    queueInsert( DrawQueue, ThisPolygon );
                }
                else {
                    ClearBox->PosX   = PosX;
                    ClearBox->PosY   = PosY;
                    ClearBox->Width  = NewPosX - PosX;
                    ClearBox->Height = ThisBox->Height;
                    queueInsert( DrawQueue, &DrawClearBox );

                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                    queueInsert( DrawQueue, ThisPolygon );
                }
            }
            else if ( ( ThisBox->PosX > NewPosX ) && ( ThisBox->PosY == NewPosY ) ) {
                if ( ( NewPosX + ThisBox->Width ) < ThisBox->PosX ) {
                    ClearBox->PosX   = PosX;
                    ClearBox->PosY   = PosY;
                    ClearBox->Width  = ThisBox->Width;
                    ClearBox->Height = ThisBox->Height;
                    queueInsert( DrawQueue, &DrawClearBox );

                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                    queueInsert( DrawQueue, ThisPolygon );
                }
                else {
                    ClearBox->PosX   = NewPosX + ThisBox->Width;
                    ClearBox->PosY   = PosY;
                    ClearBox->Width  = PosX - NewPosX;
                    ClearBox->Height = ThisBox->Height;
                    queueInsert( DrawQueue, &DrawClearBox );

                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                    queueInsert( DrawQueue, ThisPolygon );
                }
            }

            else if ( ( ThisBox->PosX < NewPosX ) && ( ThisBox->PosY < NewPosY ) ) { /*Left to Right. Top to Bottom.*/
                if ( ( NewPosX > ThisBox->PosX + ThisBox->Width ) || ( NewPosY > ThisBox->PosY + ThisBox->Height ) ) {
                    ClearBox->PosX   = PosX;
                    ClearBox->PosY   = PosY;
                    ClearBox->Width  = ThisBox->Width;
                    ClearBox->Height = ThisBox->Height;
                    queueInsert( DrawQueue, &DrawClearBox );

                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                    queueInsert( DrawQueue, ThisPolygon );
                }
                else {
                    ClearLPolygon->ExternalX = PosX;
                    ClearLPolygon->ExternalY = PosY;
                    ClearLPolygon->Width     = ThisBox->Width;
                    ClearLPolygon->Height    = ThisBox->Height;
                    ClearLPolygon->InternalX = NewPosX;
                    ClearLPolygon->InternalY = NewPosY;
                    queueInsert( DrawQueue, &DrawClearLPolygon );

                    LPolygon->Color = ThisBox->Color;
                    LPolygon->ExternalX = NewPosX + ThisBox->Width;
                    LPolygon->ExternalY = NewPosY + ThisBox->Height;
                    LPolygon->Width     = ThisBox->Width;
                    LPolygon->Height    = ThisBox->Height;
                    LPolygon->InternalX = PosX + ThisBox->Width;
                    LPolygon->InternalY = PosY + ThisBox->Height;
                    queueInsert( DrawQueue, &DrawLPolygon );
                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                }
            }
            else if ( ( ThisBox->PosX < NewPosX ) && ( ThisBox->PosY > NewPosY ) ) { /*Left to Right. Bottom to Top*/
                if ( ( NewPosX > ThisBox->PosX + ThisBox->Width ) || ( ( NewPosY + ThisBox->Height ) < ThisBox->PosY ) ) {

                    ClearBox->PosX   = PosX;
                    ClearBox->PosY   = PosY;
                    ClearBox->Width  = ThisBox->Width;
                    ClearBox->Height = ThisBox->Height;
                    queueInsert( DrawQueue, &DrawClearBox );

                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                    queueInsert( DrawQueue, ThisPolygon );
                }
                else {
                    ClearLPolygon->ExternalX = PosX;
                    ClearLPolygon->ExternalY = PosY + ThisBox->Height;
                    ClearLPolygon->Width     = ThisBox->Width;
                    ClearLPolygon->Height    = ThisBox->Height;
                    ClearLPolygon->InternalX = NewPosX;
                    ClearLPolygon->InternalY = NewPosY + ThisBox->Height;
                    queueInsert( DrawQueue, &DrawClearLPolygon );

                    LPolygon->Color = ThisBox->Color;
                    LPolygon->ExternalX = NewPosX + ThisBox->Width;
                    LPolygon->ExternalY = NewPosY;
                    LPolygon->Width     = ThisBox->Width;
                    LPolygon->Height    = ThisBox->Height;
                    LPolygon->InternalX = PosX + ThisBox->Width;
                    LPolygon->InternalY = PosY;
                    queueInsert( DrawQueue, &DrawLPolygon );
                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                }
            }
            else if ( ( ThisBox->PosX > NewPosX ) && ( ThisBox->PosY < NewPosY ) ) { /*Right to Left. Top to Bottom*/
                if ( ( ( NewPosX + ThisBox->Width ) < ThisBox->PosX ) || ( NewPosY > ThisBox->PosY + ThisBox->Height ) ) {
                    ClearBox->PosX   = PosX;
                    ClearBox->PosY   = PosY;
                    ClearBox->Width  = ThisBox->Width;
                    ClearBox->Height = ThisBox->Height;
                    queueInsert( DrawQueue, &DrawClearBox );

                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                    queueInsert( DrawQueue, ThisPolygon );
                }
                else {
                    ClearLPolygon->ExternalX = PosX + ThisBox->Width;
                    ClearLPolygon->ExternalY = PosY;
                    ClearLPolygon->Width     = ThisBox->Width;
                    ClearLPolygon->Height    = ThisBox->Height;
                    ClearLPolygon->InternalX = NewPosX + ThisBox->Width;
                    ClearLPolygon->InternalY = NewPosY;
                    queueInsert( DrawQueue, &DrawClearLPolygon );

                    LPolygon->Color     = ThisBox->Color;
                    LPolygon->ExternalX = NewPosX;
                    LPolygon->ExternalY = NewPosY + ThisBox->Height;
                    LPolygon->Width     = ThisBox->Width;
                    LPolygon->Height    = ThisBox->Height;
                    LPolygon->InternalX = PosX;
                    LPolygon->InternalY = PosY + ThisBox->Height;
                    queueInsert( DrawQueue, &DrawLPolygon );
                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                }
            }
            else if ( ( ThisBox->PosX > NewPosX ) && ( ThisBox->PosY > NewPosY ) ) {
                if ( ( ( NewPosX + ThisBox->Width ) < ThisBox->PosX ) || ( ( NewPosY + ThisBox->Height ) < ThisBox->PosY ) ) {
                    ClearBox->PosX   = PosX;
                    ClearBox->PosY   = PosY;
                    ClearBox->Width  = ThisBox->Width;
                    ClearBox->Height = ThisBox->Height;
                    queueInsert( DrawQueue, &DrawClearBox );

                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                    queueInsert( DrawQueue, ThisPolygon );
                }
                else {
                    ClearLPolygon->ExternalX = PosX + ThisBox->Width;
                    ClearLPolygon->ExternalY = PosY + ThisBox->Height;
                    ClearLPolygon->Width     = ThisBox->Width;
                    ClearLPolygon->Height    = ThisBox->Height;
                    ClearLPolygon->InternalX = NewPosX + ThisBox->Width;
                    ClearLPolygon->InternalY = NewPosY + ThisBox->Height;
                    queueInsert( DrawQueue, &DrawClearLPolygon );

                    LPolygon->Color     = ThisBox->Color;
                    LPolygon->ExternalX = NewPosX;
                    LPolygon->ExternalY = NewPosY;
                    LPolygon->Width     = ThisBox->Width;
                    LPolygon->Height    = ThisBox->Height;
                    LPolygon->InternalX = PosX;
                    LPolygon->InternalY = PosY;
                    queueInsert( DrawQueue, &DrawLPolygon );
                    ThisBox->PosX = NewPosX;
                    ThisBox->PosY = NewPosY;
                }
            }
        }


        ttc_task_yield(); // if scheduler available, give cpu to other processes
    }
    while ( TTC_TASK_SCHEDULER_AVAILABLE );
}
void queueInsert( t_ttc_queue_generic DrawQueue, DrawData_t* Box ) {

    while ( ttc_queue_generic_push_back( DrawQueue, Box, -1 ) != tqe_OK )
    { ttc_task_yield(); }

}
void tDrawIt( void* TaskArgument ) {
    t_ttc_queue_generic DrawQueue = ( t_ttc_queue_generic ) TaskArgument;
    Assert( NULL != DrawQueue, ttc_assert_origin_auto );
    t_u16 TimeSecs1 = ttc_systick_get_elapsed_usecs() / 1000000;
    t_u16 TimeSecs2 = TimeSecs1;
    t_u16 ElapsedSecs = -1;
    t_base PolygonCount = 0;

#define SIZE_STRING 20
    static t_u8 String[SIZE_STRING];

    do {
        DrawData_t Box2Draw;
        while ( ttc_queue_generic_pull_front( DrawQueue, &Box2Draw,  - 1 ) == tqe_OK ) {
            PolygonCount++;
            if ( Box2Draw.Type == ddt_MovingBox ) {
                ttc_gfx_color_fg16( Box2Draw.Polygon.MovingBox.Color );

                ttc_gfx_rect_fill( Box2Draw.Polygon.MovingBox.PosX   / MathMultiplier,
                                   Box2Draw.Polygon.MovingBox.PosY   / MathMultiplier,
                                   Box2Draw.Polygon.MovingBox.Width  / MathMultiplier,
                                   Box2Draw.Polygon.MovingBox.Height / MathMultiplier
                                 );
            }
            else {
                ttc_gfx_color_fg16( Box2Draw.Polygon.L_Polygon.Color );

                ttc_gfx_polygon_l( Box2Draw.Polygon.L_Polygon.ExternalX / MathMultiplier,
                                   Box2Draw.Polygon.L_Polygon.ExternalY / MathMultiplier,
                                   Box2Draw.Polygon.L_Polygon.Width     / MathMultiplier,
                                   Box2Draw.Polygon.L_Polygon.Height    / MathMultiplier,
                                   Box2Draw.Polygon.L_Polygon.InternalX / MathMultiplier,
                                   Box2Draw.Polygon.L_Polygon.InternalY / MathMultiplier
                                 );
            }
            TimeSecs2 = ttc_systick_get_elapsed_usecs() / 1000000;
            if ( TimeSecs1 != TimeSecs2 ) {
                if ( ElapsedSecs == -1 ) { // first second passed: reset statistic
                    PolygonCount = 0;
                    ElapsedSecs  = 0;
                }
                else {
                    if ( TimeSecs2 > TimeSecs1 )
                    { ElapsedSecs += TimeSecs2 - TimeSecs1; }
                    else
                    { ElapsedSecs += TimeSecs2; } // ToDo: use correct calculation for tick-counter overflow

                    TimeSecs1 = TimeSecs2;
                    ttc_gfx_color_fg16( 0 );
                    if ( ElapsedSecs ) {
                        ttc_string_snprintf( String, SIZE_STRING, "%4i", PolygonCount / ElapsedSecs );
                        ttc_gfx_text_cursor_set_precise( DisplayConfig->Width -  DisplayConfig->Font->CharWidth * 4,
                                                         DisplayConfig->Height - DisplayConfig->Font->CharHeight
                                                       );
                        ttc_gfx_text_solid( ( char* ) String, SIZE_STRING );
                    }
                }
            }
        }
        ttc_task_yield(); // if scheduler available, give cpu to other processes
    }
    while ( TTC_TASK_SCHEDULER_AVAILABLE );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
