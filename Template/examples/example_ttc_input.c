/** example_ttc_input.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_input.
 *  The basic flow is as follows:
 *  (1) example_ttc_input_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_input_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 11 at 20150317 17:32:16 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_input.h"
#include "../ttc-lib/ttc_touchpad.h"

//{ Global variables

// list of valid menu functions
void _menuPageA( example_input_t* Configs );
void _menuPageB( example_input_t* Configs );

// typedefinition required to declare an array of function pointers
typedef void ( *eti_menu_function )( example_input_t* );

// array of pointers to valid menu functions
const eti_menu_function eti_MenuFunctions[] = { _menuPageA,
                                                _menuPageB,
                                                NULL       // last entry must be NULL!
                                              };

// currently selected menu
void ( *eti_CurrentMenu )( example_input_t* Configs ) = _menuPageA;

// Color palettes allow to change the look of a complete application by just changing its palette.
const t_u8 eti_PaletteSize = 4;

const t_u32 eti_Palette1[] = { TTC_GFX_COLOR24_WHITE,   // used as default background color
                               TTC_GFX_COLOR24_BLACK,   // used as default foreground color
                               TTC_GFX_COLOR24_BLUE,
                               TTC_GFX_COLOR24_GREEN
                             };
const t_u32 eti_Palette2[] = { TTC_GFX_COLOR24_YELLOW,  // used as default background color
                               TTC_GFX_COLOR24_BLUE,    // used as default foreground color
                               TTC_GFX_COLOR24_AQUA,
                               TTC_GFX_COLOR24_NAVY
                             };

BOOL ScreenCleared = TRUE;

//}
//{ Private Function declarations *************************************************

/** Task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void _task_INPUT( void* TaskArgument );

/** Checks if ett_CurrentMenu points to a valid, registered menu function
 *
 * @return ==TRUE: value of *ett_CurrentMenu is valid
 */
BOOL _check_menu_is_valid();

/** Displays simple menu to adjust touch display
 *
 * @param LogicalIndex  logical index of touchpad device to adjust
 */
void _menuAdjustTouchpad( t_u8 LogicalIndex );

/** Causes _task_INPUT() to switch to another menu
 *
 * Note: This function is called from ttc_input_check() when a button is pressed
 */
void _changeMenu( t_ttc_input_config* ConfigInput, t_ttc_input_area* AffectedArea, t_u16 RelativeX, t_u16 RelativeY );

/** Creates input area for given function + draws graphical button
 *
 * ttc_input itself does not know anything about buttons.
 * Input areas are invisible rectangles that can be registered for a handler function.
 * When an input event occurs inside the rectangle (e.g. user clicks inside) then the handler function is called.
 * The handler function then can investigate the input event by its arguments:
 * - ConfigInput   configuration of input device that caused the event (e.g. ConfigInput->Event stores type of event)
 * - AffectedArea  registered input area (e.g. AffectedArea->Argument stores the value given as Argument to ttc_input_area_new() )
 * - RelativeX     x-coordinate  of event relative to top-left corner of input area
 * - RelativeY     y-coordinate  of event relative to top-left corner of input area
 *
 * @param TextX     text x-coordinate where to place button text
 * @param TextY     text y-coordinate where to place button text
 * @param Label     text to print inside button
 * @param Handler   function to be called when button is pressed
 * @param Argument  copied into input area (Handler function may access it as AffectedArea->Argument)
 */
void _createButton( t_u8 TextX, t_u8 TextY, const char* Label, void ( *Handler )( struct s_ttc_input_config* ConfigInput, struct s_ttc_input_area* AffectedArea, t_u16 RelativeX, t_u16 RelativeY ), void* Argument );

/** Produces a waiting time to avoid several pulsations on the screen
 * @param LogicalIndex   Index of touchpad device
 */
void _wait_pulsation_to_finish(t_u8 LogicalIndex) {
    Assert(LogicalIndex, ttc_assert_origin_auto ); // logical index starts at 1

    bool loopEnd = FALSE;
    t_ttc_touchpad_config* Config_TouchPad = ttc_touchpad_get_configuration(LogicalIndex);
    while(!loopEnd) {
        ttc_touchpad_check(LogicalIndex);
        if(Config_TouchPad->Flags.Bits.ContactLost)
            loopEnd = TRUE;
        ttc_task_msleep(20);
    }
}

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions **********************************************************

// Functions below are private and may not be called from outside
BOOL _check_menu_is_valid() {

    const eti_menu_function* Compare = eti_MenuFunctions;

    while ( *Compare ) { // compare current menu pointer with all registered menu pointers
        if ( *Compare == eti_CurrentMenu )
        { return TRUE; } // current menu pointer is valid
        Compare++;
    }

    return FALSE; // current menu pointer was not found in list of valid menu functions
}
void _changeMenu( t_ttc_input_config* ConfigInput, t_ttc_input_area* AffectedArea, t_u16 RelativeX, t_u16 RelativeY ) {
    (void) ConfigInput;
    (void) RelativeX;
    (void) RelativeY;

    eti_CurrentMenu = ( void ( * )( example_input_t* Configs ) ) AffectedArea->Argument; // given as Argument to _createButton() before
    Assert( _check_menu_is_valid(), ttc_assert_origin_auto ); // menu function is invalid: did you forget to add your function to eti_Menus[]?
    ScreenCleared = TRUE; // _task_INPUT() will call new menu when this function has returned
}
void _createButton( t_u8 TextX, t_u8 TextY, const char* Label, void ( *Handler )( struct s_ttc_input_config* ConfigInput, struct s_ttc_input_area* AffectedArea, t_u16 RelativeX, t_u16 RelativeY ), void* Argument ) {

    // Get font metrics
    t_u8 CharWidth  = ttc_gfx_get_configuration( 1 )->Font->CharWidth;
    t_u8 CharHeight = ttc_gfx_get_configuration( 1 )->Font->CharHeight;

    const t_u8 BorderSize = 2;
    t_u16 Left = ( TextX ) * CharWidth - BorderSize;
    t_u16 Top  = ( TextY ) * CharHeight - BorderSize;
    t_u8 TextLength = ttc_string_length8( Label, -1 );
    t_u16 Right = Left + ( 2 * BorderSize ) + ( TextLength * CharWidth );
    t_u16 Bottom = Top  + ( 2 * BorderSize ) + ( 1 *  CharHeight );

    t_ttc_input_area* Area = ttc_input_area_new( 1,         // logical index of input device
                                                 Left,      // Left   coordinate of input area
                                                 Top,       // Top    coordinate of input area
                                                 Right,     // Right  coordinate of input area
                                                 Bottom,    // Bottom coordinate of input area
                                                 Handler,   // handler function to call
                                                 Argument   // extra argument
                                               );
    ttc_gfx_rect( Area->Left, Area->Top, Area->Right - Area->Left, Area->Bottom - Area->Top );
    ttc_gfx_text_at( TextX, TextY, Label, TextLength );
}
void _menuPageA( example_input_t* Configs ) {
    Assert(ttc_memory_is_writable(Configs),ttc_assert_origin_auto);        //Always check pointer arguments with Assert()!

    // A menu function always does
    // 1) Clear screen
    // 2) Clear all input areas (registered via _createButton() )
    // 3) Draw new menu layout
    // 4) Register new input areas

    ttc_gfx_color_bg_palette(0);
    ttc_gfx_color_fg_palette(1);
    ttc_gfx_clear();
    ttc_input_clear(Configs->Config_Input->LogicalIndex);
    ttc_gfx_text_at(5,5,"MENU A",-1);
    ttc_gfx_color_fg_palette(3);
    _createButton( 5, 10, "Menu B", _changeMenu, _menuPageB );
    ScreenCleared = FALSE;

}
void _menuPageB( example_input_t* Configs ) {

    // A menu function always does
    // 1) Clear screen
    // 2) Clear all input areas (registered via _createButton() )
    // 3) Draw new menu layout
    // 4) Register new input areas

    ttc_gfx_color_bg_palette(3);
    ttc_gfx_color_fg_palette(0);
    ttc_gfx_clear();
    ttc_input_clear(Configs->Config_Input->LogicalIndex);
    ttc_gfx_text_at(5,5,"MENU B",-1);
    ttc_gfx_color_fg_palette(0);
    _createButton(5,10,"Menu A",_changeMenu, _menuPageA);
    ScreenCleared = FALSE;
}
void _menuAdjustTouchpad( t_u8 LogicalIndex ) {
    ttc_gfx_color_bg_palette(0);
    ttc_gfx_color_fg_palette(1);
    ttc_gfx_clear();

    static const char* StringPressCenter = "Press on center";
    t_ttc_input_config*    Config_Input = ttc_input_get_configuration( LogicalIndex );
    t_ttc_gfx_config*      Config_Gfx   = ttc_gfx_get_configuration( LogicalIndex );
    if ( Config_Input->Architecture == ta_input_touchpad ) { // Have a touchpad input: present adjust menu

        t_ttc_touchpad_config* Config_TouchPad = Config_Input->LowLevelConfig->TouchPad;
        t_u16 MidX = Config_Gfx->Width  / 2;
        t_u16 MidY = Config_Gfx->Height / 2;
        ttc_gfx_line_horizontal( 0, MidY, Config_Gfx->Width );
        ttc_gfx_line_vertical( MidX, 0, Config_Gfx->Height );

        ttc_gfx_text_at( 0, 0, StringPressCenter, -1 );

        // Reset adjustment data
        Config_TouchPad->AdjustX = 0;
        Config_TouchPad->AdjustY = 0;
        //Necessary adjustment for touchpad to work
        Config_TouchPad->LowLevelConfig->analog4.ContactThresholdX = 250;
        Config_TouchPad->LowLevelConfig->analog4.ContactThresholdY = 250;

        while ( !Config_TouchPad->AdjustX ) { // Wait for user input
            e_ttc_touchpad_update Update = ttc_touchpad_check( Config_TouchPad->LogicalIndex );
            if ( Update ) {
                Config_TouchPad->AdjustX = ( Config_Gfx->Width  / 2 ) -  Config_TouchPad->Position_X;
                Config_TouchPad->AdjustY = ( Config_Gfx->Height / 2 ) -  Config_TouchPad->Position_Y;
            }
            ttc_task_msleep( 100 );
        }

        Config_TouchPad->AdjustY -= 15;
        Config_TouchPad->LowLevelConfig->analog4.Calibrate_Xmax = 5950;
        Config_TouchPad->LowLevelConfig->analog4.Calibrate_Ymax = 6500;
        Config_TouchPad->AdjustX = -109+53+12;
        Config_TouchPad->AdjustY = 157+13-125;

        ttc_gfx_clear();
    }
}
void _task_INPUT( void* TaskArgument ) {
    Assert( ttc_memory_is_writable( TaskArgument ), ttc_assert_origin_auto );              // Always check pointer arguments with Assert()!

    example_input_t* Configs = ( example_input_t* ) TaskArgument; // we're expecting this pointer type
    t_ttc_input_config* Config_Input = Configs->Config_Input;
    t_ttc_gfx_config*   Config_Gfx   = Configs->Config_Gfx;

    Assert( ttc_memory_is_writable( Config_Input ), ttc_assert_origin_auto );
    Assert( ttc_memory_is_writable( Config_Gfx ),   ttc_assert_origin_auto );

    // First we adjust our touchpad
    _menuAdjustTouchpad( Config_Input->LogicalIndex );

    while ( 1 ) { // Main loop switches between menus
        if ( ScreenCleared ) {
            Assert( _check_menu_is_valid(), ttc_assert_origin_auto ); // menu function is invalid: did you forget to add your function to eti_Menus[]?
            eti_CurrentMenu( Configs ); // Call current menu dialog
            ScreenCleared = FALSE;
            _wait_pulsation_to_finish(Configs->Config_Input->LowLevelConfig->TouchPad->LogicalIndex);
        }

        // Check for user input (will call functions registered to _createButton())
        ttc_input_check( Configs->Config_Input->LogicalIndex );

        // Always sleep for a while to give cpu time to other tasks
        ttc_task_msleep( 100 );
    }
}

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_input_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    // Note: Global and static variables require a unique prefix to avoid
    //       conflicting symbol names when combined with other sources!
    static example_input_t et_input_Data;

    // Configure input device
    et_input_Data.Config_Input = ttc_input_get_configuration(1);
    Assert( et_input_Data.Config_Input, ttc_assert_origin_auto );

    // Configure graphic display
    et_input_Data.Config_Gfx = ttc_gfx_get_configuration(1);
    Assert( et_input_Data.Config_Gfx, ttc_assert_origin_auto );

    // Set color to use for first screen clear
    ttc_gfx_init(1);
    ttc_gfx_color_bg_palette(0);
    ttc_gfx_color_fg_palette(1);
    ttc_gfx_palette_set( eti_Palette1, eti_PaletteSize);

    // Initialize input driver
    ttc_input_init( 1 );

    ttc_task_create( _task_INPUT,      // function to start as thread
                     "tINPUT",        // thread name (just for debugging)
                     256,             // stack size
                     &et_input_Data,  // passed as argument to _task_INPUT()
                     1,               // task priority (higher values mean more process time)
                     NULL             // can return a handle to created task
                   );
}


//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
