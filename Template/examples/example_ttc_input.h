#ifndef EXAMPLE_INPUT_H
#define EXAMPLE_INPUT_H

/** example_ttc_INPUT.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_input.
 *  The basic flow is as follows:
 *  (1) example_ttc_input_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_input_prepare() gets executed
 *
 *  Created from template example_ttc_device.h revision 11 at 20150317 17:32:16 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_input.h"
#include "../ttc-lib/ttc_gfx.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_input_t
    t_ttc_input_config* Config_Input; // configuration of input driver
    t_ttc_gfx_config*   Config_Gfx;   // configuration of graphic driver
} example_input_t;

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_input_prepare();

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_INPUT_H
