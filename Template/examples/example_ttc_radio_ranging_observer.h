#ifndef example_ttc_radio_ranging_observer_h
#define example_ttc_radio_ranging_observer_h

/** { example_ttc_radio_ranging_observer.h *********************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.h revision 11 at 20161130 06:10:15 UTC
 *
 *  Authors: gregor
}*/
/** { Includes *********************************************************
 *
 * Put all includes here that are required to include this header file.
 * Do NOT add includes required to compile example_ttc_radio_ranging_observer.c here!
 */

#include "../ttc-lib/ttc_basic.h"             // basic datatypes
#include "../ttc-lib/ttc_memory.h"            // memory checks and safe pointers
#include "example_ttc_radio_ranging_common.h" // functions and structures being common for all node types
// #include "../ttc-lib/ttc_heap.h"   // dynamic memory and safe arrays
// #include "../ttc-lib/ttc_string.h" // string compare and copy
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Constant Defines / Macros ****************************************
 *
 * As a convention, constant definitions are all UPPERCASED to distinguish
 * them from variables and functions.
 *
 * Examples:

 #define ABC 1
 #define calculate(A,B) (A+B)
 */

//}ConstantDefines
/** { Structure Declarations *******************************************
 *
 * This can be typedef struct or typedef enum declarations.
 * Always prefix structures and enums with filename to avoid double declarations!
 *
 * Examples:

   typedef struct s_example_ttc_radio_ranging_observer_list {
      struct s_example_ttc_radio_ranging_observer_list* Next;
      t_base Value;
   } example_ttc_radio_ranging_observer_list_t;

   typedef enum {
     example_ttc_radio_ranging_observer_None,
     example_ttc_radio_ranging_observer_Type1
   } example_ttc_radio_ranging_observer_types_e;

 */

//}StructureDeclarations
/** { Function Declarations ********************************************
 *
 * Declare all functions here that may be called from outside.
 * Rules of Good Code:
 * - document every function declaration
 * - prefix public function names with their filename to avoid double definitions
 */
// { Example declaration

/** prepare observer node before its task is started
 */
void example_ttc_radio_ranging_prepare_observer( etr_task_info_t* TaskInfo );

/** Task implementing an observer node that monitors all received traffic to its UART.
 *
 * @param TaskArgument (void*)  pointer to a etr_task_info_t instance
 */
void example_ttc_radio_ranging_task_node_observer( void* TaskArgument );


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //example_ttc_radio_ranging_observer_h
