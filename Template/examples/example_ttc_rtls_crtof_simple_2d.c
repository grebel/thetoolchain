/** example_ttc_rtls_crtof_simple_2d.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for synchronous location and mapping radio protocol for a simple two dimensional anchor setup using the cascaded replies time of flight ranging protocol
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_rtls_crtof_simple_2d_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_rtls_crtof_simple_2d_prepare() get executed
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_rtls_crtof_simple_2d_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template example_ttc_device_architecture.c revision 18 at 20161208 10:46:38 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_rtls_crtof_simple_2d.h"
#include "../ttc-lib/ttc_slam.h"      // ttc_rtls requires a pre-initialized ttc_slam instance
#include "../ttc-lib/ttc_rtls.h" // high-level driver of device to use
#include "../ttc-lib/rtls/rtls_crtof_simple_2d.h" // direct access to low-level driver
#include "../ttc-lib/ttc_heap.h"     // dynamic memory allocations during runtime
#include "../ttc-lib/ttc_task.h"     // allows to use application in single- and multitasking setup
#include "../ttc-lib/ttc_radio.h"    // radio transceiver with ranging capability
#include "../ttc-lib/ttc_usart.h"    // radio transceiver with ranging capability

// Each node type has its own implementation
#ifdef EXAMPLE_TTC_RADIO_NODE_IS_ANCHOR
#  include "example_ttc_rtls_crtof_simple_2d_anchor.h"
#endif
#ifdef EXAMPLE_TTC_RADIO_NODE_IS_MOBILE
#  include "example_ttc_rtls_crtof_simple_2d_mobile.h"
#endif
#ifdef EXAMPLE_TTC_RADIO_NODE_IS_OBSERVER
#  include "example_ttc_radio_ranging_observer.h" // using observer from ranging example
#endif
#include "example_ttc_radio_ranging_common.h" // using common functions from ranging example
//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

//}Private Function Declarations
//{ Function Definitions *************************************************

// Note: If you implement both of the example functions below, your example will run in single- and multitasking configuration.

//}Function Definitions
/** { Global Variables ************************************************
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

// list of available anchors
const t_ttc_packet_address AnchorIDs[EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS] = {
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 0
    {
        .Address64.Bytes = { 0xc0, 0x01, 0xba, 0xbe, 0x00, 0x00, 0x00, 1 },
        .Address16       = 1,            // south pole anchor in slam_simple_2d
        .PanID           = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID
    }
    #endif
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 1
    , {
        .Address64.Bytes = { 0xc0, 0x01, 0xba, 0xbe, 0x00, 0x00, 0x00, 2 },
        .Address16       = 2,           // north pole anchor in slam_simple_2d
        .PanID           = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID
    }
    #endif
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 2
    , {
        .Address64.Bytes = { 0xc0, 0x01, 0xba, 0xbe, 0x00, 0x00, 0x00, 3 },
        .Address16       = 3,          // east anchor in slam_simple_2d
        .PanID           = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID
    }
    #endif
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 3
    , {
        .Address64.Bytes = { 0xc0, 0x01, 0xba, 0xbe, 0x00, 0x00, 0x00, 4 },
        .Address16       = 4,          // west anchor in slam_simple_2d
        .PanID           = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID
    }
    #endif
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 4
    , {
        .Address64.Bytes = { 0xc0, 0x01, 0xba, 0xbe, 0x00, 0x00, 0x00, 5 },
        .Address16       = 5,          // east anchor in slam_simple_2d
        .PanID           = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID
    }
    #endif
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 5
    , {
        .Address64.Bytes = { 0xc0, 0x01, 0xba, 0xbe, 0x00, 0x00, 0x00, 6 },
        .Address16       = 6,          // west anchor in slam_simple_2d
        .PanID           = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID
    }
    #endif
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 6
#  error  Not egnough data!
    #endif
};

#if EXAMPLE_TTC_RADIO_NODE_FIXED_ANCHORS == 1
/* Fixed coordinates for anchor nodes
 *
 * This example uses slam_simple2d, which applies some constrains as described in ttc-lib/slam/slam_simple2d.h!
 *
 * Graphical Representation of Stored Distance Measures during Initial Mapping
 *                           o #2 Northpole = (0,Ny)
 *                        _//!\\_
 *                      _/ / ! \ \_
 *                    _/  /  !  \  \_
 *              #4   o   /   !   \   o  #5
 *                   \  /    !    \  /
 *    West Nodes      \/     !     \/       East Noes
 *                    /\     !     /\.
 *                   /  \    !    /  \.
 *              #6   o   \   !   /   o  #3
 *                    \_  \  !  /  _/
 *                      \_ \ ! / _/
 *                        \_\!/_/
 *                          \!/
 *                           o #1 Southpole = (0,0)
 *
 * Node #n is stored in array index n-1.
 *
 * Make sure to place anchor nodes exactly as defined here!
 *
 */

const t_ttc_math_vector3d_xyz example_tscs2_AnchorNodePositions[] = { // all units in centimeter (cm)
    {    0,    0, 0 },  // southpole
    {    0, 411, 0 },  // northpole
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 2
    {  357,  357, 0 },  // east node
    #endif
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 3
    { -357,  357, 0 },  // west node
    #endif
    // more anchor node coordinates here...

    {    0,    0, 0 }   // dummy entry
};
#endif

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** { Private Function Declarations ***********************************
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** { Function Definitions ********************************************
 */

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_rtls_crtof_simple_2d_prepare() {

    // allocate as static allows compiler to check if it fits into ram (could also allocate via ttc_heap)
    static t_example_ttc_rtls_crtof_simple_2d_data TaskInfo;

    if ( 1 ) { // select which packet details should be debugged
        TaskInfo.Common.PacketDebug.SourceID = 1;
        TaskInfo.Common.PacketDebug.TargetID = 1;
        TaskInfo.Common.PacketDebug.Payload  = 1;
        TaskInfo.Common.PacketDebug.Size     = 1;
    }
    if ( 1 ) { // configure ttc_usart
        #ifdef TTC_USART1
        const char* StringBanner = "\n\nexample_rtls_crtof_simple_2d v%d\n";
        t_ttc_usart_config* ConfigUSART = TaskInfo.Common.ConfigUSART = ttc_usart_get_configuration( 1 );
        ConfigUSART->Init.BaudRate = 2000000; // reduce if your serial port does not support 2Mb/s
        ConfigUSART->Init.HalfStopBits = 2;
        ConfigUSART->Init.Flags.ControlParity = 0;
        ConfigUSART->Init.Flags.Rts = 0;
        ConfigUSART->Init.Flags.Cts = 0;
        ttc_usart_init( 1 );
        example_ttc_radio_ranging_usart_send_stringf( StringBanner, 10 );
        #endif
    }
    if ( 1 ) { // configure ttc_radio
        t_ttc_radio_config* ConfigRadio = TaskInfo.Common.ConfigRadio = ttc_radio_get_configuration( 1 );

        // logical index of SPI device being connected to DW1000 transceiver
        // Note: If not already initialized, low-level driver will initialize ttc_spi
        ConfigRadio->LowLevelConfig.LogicalIndex_SPI = 1;
        //ConfigRadio->Init.Flags.AutoAcknowledge      = 0;
        ConfigRadio->Init.Flags.DelayedTransmits     = 1;
        ConfigRadio->Init.Flags.RxAutoReenable       = 1; // re-enable receiver whenever it has switched off automatically
        #ifndef EXAMPLE_TTC_RADIO_NODE_IS_ANCHOR
        ConfigRadio->Init.Flags.EnableRangingReplies = 0; // only anchor nodes should answer ranging requests in our scenario
        #endif
        //? ConfigRadio->LowLevelConfig.TimeOut          = 0;//1.012;

        ConfigRadio->Init.LevelTX = ConfigRadio->Features->MaxTxLevel;
        //ConfigRadio->Init.MaxPacketSize = ConfigRadio->Features->Init.MaxPacketSize; // ToDo: implement frame length > 127 bytes

        example_ttc_radio_ranging_usart_send_string( "initializing radio..\n", -1 );

        ttc_radio_init( 1 );

        //X ttc_gpio_init( E_ttc_gpio_pin_a10, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max ); //DEBUG  using RxD-pin as debug output

        // allocate buffer used to prepare packet payload
        TaskInfo.Common.PrepareBuffer_Size = ttc_radio_get_configuration( 1 )->Init.MaxPacketSize;
        TaskInfo.Common.PrepareBuffer      = ttc_heap_alloc_zeroed( TaskInfo.Common.PrepareBuffer_Size );
        TaskInfo.Common.RadioUserID        = ttc_radio_user_new( 1, 2 ); // request new user id + additional packet buffers for our application

        if ( 0 ) { // test DecaWave DW1000 RX- and TX-LEDs (ToDo: Remove block!)
            radio_dw1000_gpio_out( ConfigRadio, 2, 1 ); // RXLED
            radio_dw1000_gpio_out( ConfigRadio, 3, 1 ); // TXLED
            while ( 1 );
        }

        // store pointer to radio configuration for fast access
        TaskInfo.Common.ConfigRadio = ConfigRadio;
    }
    if ( 1 ) { // set local node identifier
        static t_ttc_packet_address                         LocalNodeID;

        #ifdef EXAMPLE_TTC_RADIO_NODE_IS_ANCHOR  // load local address of an anchor node
        #if (EXAMPLE_TTC_RADIO_NODE_INDEX < 1) || (EXAMPLE_TTC_RADIO_NODE_INDEX >99)
#error Invalid value for EXAMPLE_TTC_RADIO_NODE_INDEX (must be 1..99)
        #endif
        ttc_memory_copy( &LocalNodeID, &( AnchorIDs[EXAMPLE_TTC_RADIO_NODE_INDEX - 1] ), sizeof( LocalNodeID ) );
        #else                  // load local address of an observer or mobile node
        LocalNodeID.PanID     = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID;
        LocalNodeID.Address16 = EXAMPLE_TTC_RADIO_NODE_INDEX;

        const t_u8 Address64[8] = { 0xaf, 0xfe, 0x00, 0x00, 0x00, 0x00, 0x00, EXAMPLE_TTC_RADIO_NODE_INDEX };
        ttc_memory_copy( &LocalNodeID, Address64, 8 );

        #endif

        // pass pointer to local node identifier to task and statemachine
        TaskInfo.Common.LocalNodeID = &LocalNodeID;

        // tell radio our local address
        ttc_radio_change_local_address( 1, TaskInfo.Common.LocalNodeID );
    }
    if ( 1 ) { // configure first ttc_slam instance to be used by ttc_rtls
        /* ttc_slam instance is used different in anchor and mobile nodes:
         *
         * Anchor nodes use it to built up a coordinate system.
         * Mobile nodes use it to calculate their current position.
         */
        t_ttc_slam_config* ConfigSlam = ttc_slam_get_configuration( 1 );

        // configuration of ttc_slam only requires to set maximum amount of anchor nodes to support
        ConfigSlam->Init.Amount_Nodes = EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS;

        TaskInfo.IndexSLAM = ConfigSlam->LogicalIndex ;
        ttc_slam_init( TaskInfo.IndexSLAM );

        #if EXAMPLE_TTC_RADIO_NODE_FIXED_ANCHORS == 1
        if ( 1 ) { // assign fixed coordinates to all anchor nodes
            for ( t_u8 NodeIndex = 1; NodeIndex <= ConfigSlam->Init.Amount_Nodes; NodeIndex++ ) {
                ttc_slam_update_coordinates( TaskInfo.IndexSLAM,
                                             NodeIndex,
                                             example_tscs2_AnchorNodePositions[NodeIndex - 1].X,
                                             example_tscs2_AnchorNodePositions[NodeIndex - 1].Y,
                                             example_tscs2_AnchorNodePositions[NodeIndex - 1].Z
                                           );
            }
        }
        #endif
    }
    if ( EXAMPLE_TTC_RADIO_NODE_FIXED_ANCHORS == 0 ) { // create + configure ttc_rtls
        t_ttc_rtls_config* Config_RTLS = ttc_rtls_create();

        Config_RTLS->Init.Amount_AnchorIDs = EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS;
        Config_RTLS->Init.AnchorIDs   = &( AnchorIDs[0] );
        Config_RTLS->Init.LocalID     = TaskInfo.Common.LocalNodeID;
        Config_RTLS->Init.Index_Radio = TaskInfo.Common.ConfigRadio->LogicalIndex;

        #ifdef EXAMPLE_TTC_RADIO_NODE_IS_ANCHOR
        Config_RTLS->Flags.IsAnchorNode = 1; // enable anchor node implementation in ttc_rtls
        #else
        Config_RTLS->Flags.IsAnchorNode = 0; // disable anchor node implementation in ttc_rtls
        #endif

        // inform ttc_rtls of ttc_slam instance to use
        Config_RTLS->Init.Index_SLAM = TaskInfo.IndexSLAM;

        // initialize rtls instance for operation
        ttc_rtls_init( Config_RTLS->LogicalIndex );

        // store pointer to rtls configuration for fast access
        TaskInfo.Config_RTLS = Config_RTLS;
    }
    if ( 1 ) { // let node prepare itself and start its task
        #ifdef EXAMPLE_TTC_RADIO_NODE_IS_MOBILE
        example_ttc_rtls_crtof_simple_2d_prepare_mobile( &TaskInfo );
        #endif
        #ifdef EXAMPLE_TTC_RADIO_NODE_IS_ANCHOR
        example_ttc_rtls_crtof_simple_2d_prepare_anchor( &TaskInfo );

        #if EXAMPLE_TTC_RADIO_NODE_FIXED_ANCHORS == 1
        // check if our own fixed coordinates are visible to ttc_radio for automatic replies to localization requests (->radio_common.c:radio_common_ranging_reply_isr() )
        ttc_assert_test_origin( TaskInfo.Common.ConfigRadio->Init.MyLocation->X == example_tscs2_AnchorNodePositions[EXAMPLE_TTC_RADIO_NODE_INDEX - 1].X, ttc_assert_origin_auto ); // fixed coordinate was not set. Check implementation!
        ttc_assert_test_origin( TaskInfo.Common.ConfigRadio->Init.MyLocation->Y == example_tscs2_AnchorNodePositions[EXAMPLE_TTC_RADIO_NODE_INDEX - 1].Y, ttc_assert_origin_auto ); // fixed coordinate was not set. Check implementation!
        ttc_assert_test_origin( TaskInfo.Common.ConfigRadio->Init.MyLocation->Z == example_tscs2_AnchorNodePositions[EXAMPLE_TTC_RADIO_NODE_INDEX - 1].Z, ttc_assert_origin_auto ); // fixed coordinate was not set. Check implementation!
        #endif

        #endif
        #ifdef EXAMPLE_TTC_RADIO_NODE_IS_OBSERVER
        example_ttc_radio_ranging_prepare_observer( &( TaskInfo.Common ) );
        #endif
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
