#ifndef EXAMPLE_SLAM_SIMPLE_2D_H
#define EXAMPLE_SLAM_SIMPLE_2D_H

/** example_ttc_SLAM_SIMPLE_2D.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_slam_simple_2d.
 *  The basic flow is as follows:
 *  (1) example_ttc_slam_simple_2d_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_slam_simple_2d_prepare() gets executed
 *
 *  Created from template example_ttc_device_architecture.h revision 13 at 20160607 12:30:10 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_SLAM_SIMPLE_2D.c only do not belong here!
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_math_types.h"
#ifdef TTC_GFX1
#include "../ttc-lib/ttc_gfx.h"
#endif

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_slam_simple_2d_t
    t_u8  IndexSLAM;      // logical index of ttc_slam device to use
    t_u32 MaxDifference;  // maximum allowed difference between localized and real coordinates
    // more arguments...

    #ifdef TTC_GFX1
    t_ttc_gfx_config* GFX;  // configuration of graphic display to use

    // center point of graphical display
    t_u16 Gfx_CenterX;
    t_u16 Gfx_CenterY;

    // scale to fit bounding box into square rectangle
    ttm_number Gfx_ScaleX;
    ttm_number Gfx_ScaleY;
    #endif

} example_slam_simple_2d_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_slam_simple_2d_prepare();


//}Function prototypes

#endif //EXAMPLE_SLAM_SIMPLE_2D_H
