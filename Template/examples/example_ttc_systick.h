#ifndef EXAMPLE_SYSTICK_H
#define EXAMPLE_SYSTICK_H

/** example_ttc_SYSTICK.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for system timer for precise time measures and periodice interrupt generation
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_systick_cortexm3_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_systick_cortexm3_prepare() get executed
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_systick_cortexm3_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template example_ttc_device.h revision 15 at 20160926 17:50:02 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_SYSTICK.c only do not belong here!
#include "../ttc-lib/ttc_basic.h"      // basic datatypes and definitions
#include "../ttc-lib/ttc_systick.h"  // exact delays and timeouts
#include "../ttc-lib/ttc_task_types.h" // required for task delays

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_systick_t
    t_u8              Index_SYSTICK;  // logical index of device to use
    t_ttc_systick_delay  Delay;          // this type of delay is usable in single- and multitasking setup

    // more arguments...
} example_systick_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_systick_prepare();


//}Function prototypes

#endif //EXAMPLE_SYSTICK_H
