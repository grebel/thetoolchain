#ifndef example_ttc_watchdog_H
#define example_ttc_watchdog_H

/*{ ExampleLeds::.h ************************************************
 
 Empty template for new header files.
 Copy and adapt to your needs.
 
}*/
//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gpio.h"
#ifdef EXTENSION_ttc_task
#  include "../ttc-lib/ttc_task.h"
#endif
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_watchdog.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

//}Structures/ Enums
//{ Function declarations **************************************************

// main entry: initializes hardware + spawns all threads
// returns immediately
void example_ttc_watchdog_prepare();

// task that will init + reset watchdog periodically
void taskWatchdogTest(void* Arg);

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //example_ttc_watchdog
