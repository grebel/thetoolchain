/** { example_ttc_rtls_crtof_simple_2d_mobile.c ************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.c revision 11 at 20161130 06:10:10 UTC
 *
 *  Authors: gregor
}*/

/** { Includes ********************************************************
 *
 * Put all includes here that are required to compile this source code.
 */

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_rtls_crtof_simple_2d_mobile.h"
#include "example_ttc_radio_ranging_common.h"
#include "../ttc-lib/ttc_radio.h"                          // radio transmission with ranging capability
#include "../ttc-lib/ttc_math.h"                           // mathematic datatypes and vectors
#include "../ttc-lib/ttc_slam.h"                           // synchronous location and mapping calculates 3D coordinates from distance measures
#include "../ttc-lib/ttc_rtls.h"                      // slam radio protocol for indoor localization
#include "../ttc-lib/ttc_string.h"                         // string composition
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Global Variables ************************************************
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** { Private Function Declarations ***********************************
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** { Function Definitions ********************************************
 */

void example_ttc_rtls_crtof_simple_2d_prepare_mobile( t_example_ttc_rtls_crtof_simple_2d_data* TaskInfo ) {

    // receive ACKs + beacons + broadcasts + packets targeted at us
    ttc_radio_configure_frame_filter( 1,
                                      TaskInfo->Common.RadioUserID,
                                      ttc_radio_frame_filter_sensor
                                    );

    ttc_task_create(
        example_ttc_rtls_crtof_simple_2d_task_mobile, // function to start as thread
        "tMobile",                  // thread name (just for debugging)
        512,                        // stack size
        TaskInfo,                   // passed as argument to example_ttc_rtls_crtof_simple_2d_mainTask()
        1,                          // task priority (higher values mean more process time)
        NULL                        // can return a handle to created task
    );
}
void _example_ttc_rtls_crtof_simple_2d_debug() {
    volatile static t_u32 Calls = 0;
    Calls++;
}
void example_ttc_rtls_crtof_simple_2d_task_mobile( void* TaskArgument ) { // implementation of sensor node task
    t_example_ttc_rtls_crtof_simple_2d_data* TaskInfo = ( t_example_ttc_rtls_crtof_simple_2d_data* ) TaskArgument;

    example_ttc_radio_ranging_usart_send_stringf( "starting sensor node #%d ", TaskInfo->Common.ConfigRadio->LocalID.Address16 );
    example_ttc_radio_ranging_usart_send_stringf( "on channel %d\n", TaskInfo->Common.ConfigRadio->ChannelRX );

    const t_u16 Interleave = 1000; // amount of while loops to pass until a new packet is send
    t_u16 Count = 0;
    t_u16 MessageCounter = 0;
    t_ttc_packet_address BroadcastID;
    BroadcastID.PanID = TaskInfo->Common.LocalNodeID->PanID;
    BroadcastID.Address16 = 0xffff; // broadcast address

    t_u8 RadioUserID = TaskInfo->Common.RadioUserID;
    while ( 1 ) {
        if ( 1 && ( Count-- == 0 ) ) { // become active
            Count = Interleave;

            if ( 0 ) { // send broadcast message to everybody who is just listening
                ttc_string_snprintf( TaskInfo->Common.PrepareBuffer, TaskInfo->Common.PrepareBuffer_Size, "Hello World #%i", MessageCounter++ );

                example_ttc_radio_ranging_transmit( &( TaskInfo->Common ),
                                                    TaskInfo->Common.PrepareBuffer,
                                                    ttc_string_length8( ( char* ) TaskInfo->Common.PrepareBuffer, TaskInfo->Common.PrepareBuffer_Size ) + 1, // transmit string + string terminator
                                                    &BroadcastID,
                                                    E_ttc_packet_type_802154_Data_001010,
                                                    &( TaskInfo->Common.StringBuffer[0] ),
                                                    SIZE_ETR_STRINGBUFFER
                                                  );
            }
            if ( 1 ) { // try to localize ourself
                t_ttc_rtls_localization* Localization = ttc_rtls_mobile_localize( TaskInfo->Config_RTLS->LogicalIndex,
                                                                                  rcrt_Request_Localization_SSTOFCR,
                                                                                  &( TaskInfo->Location[0] ),
                                                                                  &( TaskInfo->Location[1] )
                                                                                );

                if ( Localization ) { // localization successfull
                    if ( 1 ) { // send position update as broadcast message to everybody who is just listening
                        for ( t_u8 Index = 0; Index < 2; Index++ ) { // send both mathematical solutions
                            ttc_string_snprintf( TaskInfo->Common.PrepareBuffer, TaskInfo->Common.PrepareBuffer_Size,
                                                 "#%i03 %i@(%f03.3,%f03.3,%f03.3)",
                                                 //"#III @(FFF.fff, FFF.fff, FFF.fff)" // 33 chars
                                                 TaskInfo->Config_RTLS->ConfigRadio->LocalID.Address16,
                                                 Index + 1,
                                                 TaskInfo->Location[Index].X,
                                                 TaskInfo->Location[Index].Y,
                                                 TaskInfo->Location[Index].Z
                                               );

                            // transmit position report as readable text (debug)
                            example_ttc_radio_ranging_transmit( &( TaskInfo->Common ),
                                                                TaskInfo->Common.PrepareBuffer,
                                                                ttc_string_length8( ( char* ) TaskInfo->Common.PrepareBuffer, TaskInfo->Common.PrepareBuffer_Size ) + 1, // transmit string + string terminator
                                                                &BroadcastID,
                                                                E_ttc_packet_type_802154_Data_001010,
                                                                &( TaskInfo->Common.StringBuffer[0] ),
                                                                SIZE_ETR_STRINGBUFFER
                                                              );

                            // copy message to USART
                            example_ttc_radio_ranging_usart_send_stringf( ( char* ) & ( TaskInfo->Common.StringBuffer[0] ), 0 );
                        }
                    }
                    ttc_assert_break_origin( ttc_assert_origin_auto ); //DEBUG

                    if ( 1 ) { // print statistics
                        for ( t_u8 AnchorIndex = 0; AnchorIndex < Localization->AmountValid; AnchorIndex++ ) {
                            t_ttc_rtls_statistics* Statistic = &( Localization->Rangings[AnchorIndex] );
                            if ( Statistic->Average ) { // got a range measure to this anchor: display it
                                example_ttc_radio_ranging_usart_send_stringf( "Range(%i):",     AnchorIndex );
                                example_ttc_radio_ranging_usart_send_stringf( "AVG=%i cm ",     Statistic->Average );
                                example_ttc_radio_ranging_usart_send_stringf( "Median=%i cm ",  Statistic->Median );
                                example_ttc_radio_ranging_usart_send_stringf( "(Max-Min=%i)\n", Statistic->Upper - Statistic->Lower );
                            }
                        }
                        _example_ttc_rtls_crtof_simple_2d_debug(); // successfully localized ourself
                    }
                }
            }
        }
        if ( 1 ) { ttc_radio_receiver( 1, RadioUserID, 1, 0, 0 ); }  // enable receiver to scan for incoming packets (will wait until all queued packets have been transmitted)

        t_ttc_packet* ReceivedPacket = ttc_radio_packet_received_tryget( 1, RadioUserID, &( TaskInfo->Common.SocketJob ) );
        if ( ReceivedPacket ) { // somebody wants to talk to us
            if ( 1 )
            { ttc_radio_receiver( 1, RadioUserID, 1, 0, 0 ); }  // re-enable receiver (is automatically disabled after receiving frame)

            if ( 1 ) { // debug received packet via usart
                t_u8* Append = &( TaskInfo->Common.StringBuffer[0] );
                t_base StringBufferFree = SIZE_ETR_STRINGBUFFER;
                Append += ttc_string_appendf( Append, &StringBufferFree, "(0x%04x) RX: ", TaskInfo->Common.LocalNodeID->Address16 );
                Append += ttc_packet_debug( ReceivedPacket, Append, &StringBufferFree, TaskInfo->Common.PacketDebug );
                Append += ttc_string_appendf( Append, &StringBufferFree, "\n" );
                example_ttc_radio_ranging_usart_send_string( ( char* ) & ( TaskInfo->Common.StringBuffer[0] ), SIZE_ETR_STRINGBUFFER );
            }

            // return packet memory buffer to radio for reuse
            ttc_radio_packet_release( ReceivedPacket );
        }

        ttc_radio_maintenance( 1, RadioUserID ); // let radio cleanup buffers and check transceiver state

        if ( 1 ) { ttc_task_msleep( 1 ); }
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
