/** example_ttc_sysclock.c **********************************************{
 *
 *                             The ToolChain
 *
 * This example shows how to use the high-level system clock configuration API.
 * The main API function is ttc_sysclock_profile_switch(). It implements a concept of
 * system clock profiles. Different profiles can be implemented like maximum speed,
 * highest energy efficiency and others.
 * See declaration of e_ttc_sysclock_profile and implementation of current architecture
 * driver for details.
 *
 * written by Gregor Rebel 2014
 *
 *
 * example_ttc_sysclock_prepare()
 *     fills et_sysclock_TaskArguments with data
 *     spawns thread task_SYSCLOCK()
 *     returns immediately
 *
 * task_SYSCLOCK()
 *     while (1) {
 *
 *       switch to maximum system clock
 *       switch on LED1
 *       blink LED2 with maximum speed
 *
 *       switch to minimum system clock
 *       switch off LED1
 *       blink LED2 with maximum speed
 *
 *       sleep for 100 ms
 *     }
 *
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_sysclock.h"

// include foreign headers
#include "../ttc-lib/ttc_sysclock.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_task.h"

typedef struct {
    t_ttc_systick_delay Delay;
} ets_data_t;
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_sysclock_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    ets_data_t* SYSCLOCK_Data = ttc_heap_alloc_zeroed( sizeof( ets_data_t ) );

    ttc_task_create( task_SYSCLOCK,               // function to start as thread
                     "tSYSCLOCK",                 // thread name (just for debugging)
                     128,                         // stack size
                     SYSCLOCK_Data,                        // passed as argument to task_SYSCLOCK()
                     1,                           // task priority (higher values mean more process time)
                     NULL                         // can return a handle to created task
                   );

    ets_data_t* SYSCLOCK_Delay_Data = ttc_heap_alloc_zeroed( sizeof( ets_data_t ) );
    ttc_task_create( task_SYSCLOCK_Delay,         // function to start as thread
                     "tDelay",                    // thread name (just for debugging)
                     128,                         // stack size
                     SYSCLOCK_Delay_Data,                        // passed as argument to task_SYSCLOCK()
                     1,                           // task priority (higher values mean more process time)
                     NULL                         // can return a handle to created task
                   );
}
void task_SYSCLOCK( void* TaskArgument ) {
    ets_data_t* Data = ( ets_data_t* ) TaskArgument;

    // initialize gpio pins connected to leds as output
    ttc_gpio_init( TTC_LED1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    ttc_gpio_init( TTC_LED2, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );

    t_u8 Toggle = 1;

    ttc_sysclock_profile_switch( E_ttc_sysclock_profile_MaxSpeed );
    t_base MaxFrequency = ttc_sysclock_get_frequency();

    do {
        // This loop runs endlessly if scheduler is available.
        // Otherwise it will run only onve and be called periodically from ttc_task_start_scheduler().

        if ( ttc_systick_delay_expired( & Data->Delay ) ) { // delay passed: send hello world (zeroed delay always expires)
            ttc_systick_delay_init( & Data->Delay, 100000 ); // set new delay to expire in 100 milliseconds

            if ( Toggle ) { // switch to maximum speed
                ttc_sysclock_profile_switch( E_ttc_sysclock_profile_MaxSpeed );
            }
            else {        // switch to minimum speed
                ttc_sysclock_profile_switch( E_ttc_sysclock_profile_MinSpeed );
            }
            Toggle = 1 - Toggle;

            // adjust length of for-loop to make it equal length at every system clock frequency
            t_base CurrentFrequency = ttc_sysclock_get_frequency();
            t_base Iterations = 1000 * MaxFrequency / CurrentFrequency;

            for ( t_base I = Iterations; I > 0; I-- ) {
                if ( I & 1 ) {
                    ttc_gpio_set( TTC_LED1 );
                }
                else {
                    ttc_gpio_clr( TTC_LED1 );
                }
            }
        }
        ttc_task_yield(); // if scheduler available, give cpu to other processes
    }
    while ( TTC_TASK_SCHEDULER_AVAILABLE );
}
void task_SYSCLOCK_Delay( void* TaskArgument ) {
    ets_data_t* Data = ( ets_data_t* ) TaskArgument;

    do {
        // This loop runs endlessly if scheduler is available.
        // Otherwise it will run only onve and be called periodically from ttc_task_start_scheduler().

        if ( ttc_systick_delay_expired( & Data->Delay ) ) { // delay passed: send hello world (zeroed delay always expires)
            ttc_systick_delay_init( & Data->Delay, 100000 ); // set new delay to expire in 100 milliseconds

            // if ttc_sysclock is configured correctly, then msleeps will stay constant at all system clock frequencies

            ttc_gpio_set( TTC_LED2 );
            ttc_task_msleep( 100 );
            ttc_gpio_clr( TTC_LED2 );
        }
        ttc_task_yield(); // if scheduler available, give cpu to other processes
    }
    while ( TTC_TASK_SCHEDULER_AVAILABLE );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
