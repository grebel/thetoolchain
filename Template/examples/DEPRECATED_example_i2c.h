#ifndef EXAMPLE_I2C_H
#define EXAMPLE_I2C_H

/*{ example_i2c.h ******************************************************
 
                       The ToolChain
                       
 Example code demonstrating how to use 
 Universal Synchronous Asynchronous Receiver Transmitter (I2C)
 
 written by Gregor Rebel 2012
 
}*/
//{ Defines/ TypeDefs ****************************************************


//}Defines
//{ Includes *************************************************************

 // Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/DEPRECATED_ttc_i2c.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_task.h"

#ifdef EXTENSION_500_ttc_gfx
#include "../ttc-lib/ttc_gfx.h"
#include "../ttc-lib/ttc_string.h"
#endif

#ifdef EXTENSION_400_sensor_lis3lv02dl
#include "ttc-lib/sensors/accelerometer/DEPRECATED_lis3lv02dl.h" // accelerometer embedded into STM32-LCD prototype board
#endif

//}Includes
//{ Structures/ Enums ****************************************************

//}Structures/ Enums
//{ Global Variables *****************************************************



//}Global Variables
//{ Function declarations **************************************************

/** Call before example_i2c_start() to initialize hardware.
 */
void example_i2c_init();

/** initializes + starts I2C example
 */
void example_i2c_start();

/** Sends "Hello World" to initialized I2C
 */
void task_I2C_master(void *TaskArgument);

/** Reads from initialized I2C and sends answer
 */
void task_I2C_slave(void *TaskArgument);

/** simple printf() at given coordinates
  */
void printfAt(u16_t X, u16_t Y, const char* String, s32_t Value);

/** processes received data */
void processData(char* Data, u8_t MaxLength);

/** send given buffer */
void sendData(const char* Buffer, u16_t MaxLength);

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_I2C_H
