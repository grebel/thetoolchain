/*{ Template_FreeRTOS::.c ************************************************

  leds_start() spawns thread taskBrightness()

  taskBrightness()
      Defines brightness of led as Ton / (Ton + Toff)
      spawns thread taskControlLed()
      while (1) {
        while (Ton < 100) { Ton++; Toff--; }
        while (Ton >   0) { Ton--; Toff++; }
      }

  taskControlLed()
      while (1) {
        switch on LED
        wait for Ton
        switch off LED
        wait for Toff
      }

}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_leds.h"

//{ Global variables

t_configled  el_LED1;
t_configled* el_LED2 = NULL;
bool SwitchInterrupt = FALSE; // ==TRUE: switch is controlled via interrupt; polling otherwise
t_u32 NoToggleBefore = 0;     // time when LED is allowed to be toggled again

#ifndef MinimumDelay
#define MinimumDelay 1
#endif

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_leds_init() {

#ifdef TTC_SWITCH1 //{ initialize + enable port + interrupt for external switch TTC_SWITCH1
    if (1) ttc_gpio_init(TTC_SWITCH1, E_ttc_gpio_mode_input_floating);            // init port with default speed
    else   ttc_gpio_init2(TTC_SWITCH1, E_ttc_gpio_mode_input_floating, E_ttc_gpio_speed_min);

#ifdef EXAMPLE_LEDS_USE_INTERRUPTS // try to initialize interrupt to check switch
        if (tine_OK == ttc_interrupt_init(tit_GPIO_Rising,
                                          ttc_gpio_create_index8(TTC_SWITCH1), // convert GPIO,PIN -> PhysicalIndex
                                          isr_Switch1,
                                          0, 0, 0)
                )
        {
            SwitchInterrupt = TRUE;
            ttc_interrupt_enable(tit_GPIO_Rising, ttc_gpio_create_index8(TTC_SWITCH1), TRUE);
        }
#endif

#endif //}
    // initializing ports as variables consumes more memory than a simple ttc_gpio_init(),
    // but it allows to pass a port configuration like a variable (hence the name)
    el_LED1.Brightness = 99;
    ttc_gpio_init_variable(&(el_LED1.Port), TTC_LED1, E_ttc_gpio_mode_output_push_pull);

#ifdef TTC_LED2
    el_LED2 = ttc_heap_alloc_zeroed(sizeof(t_configled));
    el_LED2->Brightness = 99;
    ttc_gpio_init_variable(&(el_LED2->Port), TTC_LED2, E_ttc_gpio_mode_output_push_pull);
#else
    // no TTC_LED2 defined: use same LED
    el_LED2 = &el_LED1;
#endif

#ifdef EXTENSION_ttc_watchdog
    ttc_watchdog_init1(0,0);
#endif
}
void example_leds_prepare() {

#ifdef TARGET_ARCHITECTURE_STM32W1xx
    // stm32w currently has no FreeRTOS support
    if (1) {
        t_ttc_Port PortLED1;
        ttc_gpio_init_variable(&PortLED1, TTC_LED1, E_ttc_gpio_mode_output_push_pull);
        while (1) {
            ttc_gpio_clr_variable(&PortLED1);
            for(int i = 0 ; i<50000;i++){}
            ttc_gpio_set_variable(&PortLED1);
            for(int i = 0 ; i<50000;i++){}
        }

    }
    else {
        ttc_gpio_init(TTC_LED1, E_ttc_gpio_mode_output_push_pull);
        while (1) {
            ttc_gpio_clr(TTC_LED1);
            for(int i = 0 ; i<50000;i++){}
            ttc_gpio_set(TTC_LED1);
            for(int i = 0 ; i<50000;i++){}
        }
    }
#endif

//#ifdef TARGET_ARCHITECTURE_STM32L1xx
//    return;
//#endif

    example_leds_init();

    ttc_task_create( taskBrightness,   // function to start as thread
                     "taskBrightness", // thread name (just for debugging)
                     128,              // stack size
                     NULL,             // passed as argument to taskBrightness()
                     1,                // task priority (higher values mean more process time)
                     NULL              // can return a handle to created task
                     );
}
void reloadWatchdog() {
#ifdef EXTENSION_ttc_watchdog    
    if (0) ttc_watchdog_reload1();
#endif
}
void taskBrightness(void *TaskArgument) {
    TaskArgument = TaskArgument; // avoids warning: unused parameter 'TaskArgument'
    
    if (1) { ttc_task_create(taskControlLed,      // function to start as thread
                             "taskControl_LED1",  // thread name (just for debugging)
                             128,                 // stack size
                             (void *) &el_LED1,   // passed as argument to taskControlLed()
                             1,                   // task priority (higher values mean more process time)
                             NULL                 // can return a handle to created task
                             );
    }
#ifdef TTC_LED2
    if (1) { ttc_task_create(taskControlLed,      // function to start as thread
                             "taskControl_LED2",  // thread name (just for debugging)
                             128,                 // stack size
                             (void *) el_LED2,    // passed as argument to taskControlLed()
                             1,                   // task priority (higher values mean more process time)
                             NULL                 // can return a handle to created task
                             );
    }
#endif

    while (1) {
        while (el_LED1.Brightness > 0) {
            if (!SwitchInterrupt)
                pollSwitch1();
            reloadWatchdog();
            el_LED1.Brightness--;
            ttc_task_msleep(10);
        }
        while (el_LED1.Brightness < 99) {
            if (!SwitchInterrupt)
                pollSwitch1();
            reloadWatchdog();
            el_LED1.Brightness++;
            ttc_task_msleep(10);
        }
    }
}
void taskControlLed(void *TaskArgument) {
    t_configled *LED = (t_configled*) TaskArgument;
    if (LED != NULL) {

        t_ttc_Port* Port = &(LED->Port);
        t_u8 OldBrightness = LED->Brightness;

        while (1) {
            if (OldBrightness != LED->Brightness)
                OldBrightness = LED->Brightness;

            if (LED->Brightness >  0) { // switch  on LED
                if (LED->Invert)
                    ttc_gpio_clr_variable(Port);
                else
                    ttc_gpio_set_variable(Port);
                uSleep(LED->Brightness * MinimumDelay);
            }
            if (LED->Brightness < 99) { // switch off LED
                if (LED->Invert)
                    ttc_gpio_set_variable(Port);
                else
                    ttc_gpio_clr_variable(Port);
                uSleep((99 - LED->Brightness) * MinimumDelay);
            }
            ttc_basic_test_ok(); // used for automatic testing
        }
    }
}
void toggleLED() {
    t_u32 TickCount = ttc_systick_get_elapsed_usecs();
    if (TickCount > NoToggleBefore) {
        // simply toggle brightness
        if (el_LED2->Invert)
            el_LED2->Invert = FALSE;
        else
            el_LED2->Invert = TRUE;
        NoToggleBefore  = TickCount + 1000; // avoid flicker
    }
}
void pollSwitch1() {
    bool static SwitchValueBefore = FALSE;
    bool SwitchValue = ttc_gpio_get(TTC_SWITCH1);
    if (SwitchValueBefore != SwitchValue) { // edge detected
        SwitchValueBefore = SwitchValue;
        if (SwitchValue) // toggle on rising edge
            toggleLED();
    }
}

#ifdef EXAMPLE_LEDS_USE_INTERRUPTS

void isr_Switch1(t_physical_index PhysicalIndex, void* Argument) {

    // arguments not used
    (void) PhysicalIndex;
    (void) Argument;

    toggleLED();
}

#endif

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
