/** example_ttc_filesystem.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for dosfs free fat12/fat16/fat32 filesystem for low-end embedded applications (1kb ram, 4kb rom)
 *
 *  Basic flow of booting examples:
 *  (1) ttc_extensions.c:ttc_extensions_start() calls example_ttc_filesystem_prepare() to
 *      - initialize all required devices
 *      - initializes its global and local static variables
 *      - create all required task or statemachine instances
 *      - return immediately
 *
 *  Multitasking is available:
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_filesystem_prepare() get executed.
 *      Each task uses its private stack as given to ttc_task_create() before.
 *
 *  Multitasking is NOT available:
 *  (2) Minimalistic main loop is started
 *  (3) Main loop calls all registered statemachine functions in an endless loop.
 *      All statemachine functions use the same global stack that is maximum of all
 *      stack sizes given to ttc_task_create() before.
 *
 *  Created from template example_ttc_device.c revision 23 at 20180413 11:21:12 UTC
 *
 *  Authors: <AUTHOR>
 *
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "example_ttc_filesystem.h"
#include "../compile_options.h"       // dynamically created file defining all "COMPILE_OPTS += -D..." constants from makefiles
#include "../ttc-lib/ttc_filesystem.h"  // high-level driver of device to use
#include "../ttc-lib/ttc_basic.h"     // basic datatypes and definitions
#include "../ttc-lib/ttc_heap.h"      // dynamic memory allocations and safe arrays
#include "../ttc-lib/ttc_task.h"      // allows to use application in single- and multitasking setup
#include "../ttc-lib/ttc_systick.h"   // exact delays and timeouts

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_filesystem_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _example_filesystem_task( void* Argument );

/** Statemachine running this example
 *
 * If no multitasking is available but multiple delays must be used in parallel,
 * a statemachine can be implemented. This is basically a function that memorizes a current
 * state number and uses a switch(state) {} structure to decide which code has to be executed
 * at the moment. Statemachine functions typically return quickly and have to be called regularly.
 * This behaviour allows to run multiple statemachines intermittently. Multiple statemachine
 * functions can be created via ttc_task_create() and will be called at maximum frequency one
 * after another in an endless loop.
 * See also example_ttc_states for a safer way to implement statemachines!
 */
void _example_filesystem_statemachine( void* Argument );

//}Private Function Declarations
//{ Function Definitions *************************************************

void example_ttc_filesystem_prepare() {                   // prepare this example for being started
    /** Note: The _prepare() function is called in single task mode before the scheduler is started.
      *       It has to return to allow all extensions to prepare.
      *       After all extensions finished their preparation, the created tasks are run.
      *       If no multitasking scheduler has been activated, all created tasks are run one after
      *       another in an endless loop. See ttc_task_start_scheduler() for details.
      *       This scheme allows to enable multiple statemachine functions in a flexible way.
      */

    if ( 1 ) { // create one instance of your example (copy and adjust for multiple instances)

        // allocate data of instance as local static (allows compiler to detect if it fits into RAM)
        static t_example_filesystem_data filesystem_Data;

        // reset data
        ttc_memory_set( &filesystem_Data, 0, sizeof( filesystem_Data ) );

        // LogicalIndex = 1: use device defined as TTC_FILESYSTEM1 in makefile
        filesystem_Data.Idx_filesystem = 1;

        // using first sdcard slot as storage device
        filesystem_Data.Idx_sdcard = 1;


        // loading configuration of first filesystem device
        filesystem_Data.Config_filesystem = ttc_filesystem_get_configuration( filesystem_Data.Idx_filesystem );

        // this is only the minimum amount of cache blocks. More blocks may improve speed even more
        t_u8 AmountBlocks = filesystem_Data.Config_filesystem->Features->Scratch_AmountBlocks_Minimum;

        // central buffer required by filesystem (can be used for other purposes when filesystem is not active)
        t_u8* Scratch = ttc_heap_alloc_zeroed( filesystem_Data.Config_filesystem->Features->BlockSize_Maximum * AmountBlocks );

        // configure scratch buffer
        filesystem_Data.Config_filesystem->Init.Scratch              = Scratch;
        filesystem_Data.Config_filesystem->Init.Scratch_AmountBlocks = AmountBlocks;

        if ( 0 ) { // optional: allocate additional memory as cache to speed up filesystem
            // this is only the minimum amount of cache blocks. More blocks may improve speed even more
            AmountBlocks = filesystem_Data.Config_filesystem->Features->Cache_AmountBlocks_Minimum;

            // cache memory used to speed up filesystem operations (can be reused when filesystem is not mounted)
            t_u8* Cache = ttc_heap_alloc_zeroed( filesystem_Data.Config_filesystem->Features->BlockSize_Minimum * AmountBlocks );
            filesystem_Data.Config_filesystem->Init.Cache              = Cache;
            filesystem_Data.Config_filesystem->Init.Cache_AmountBlocks = AmountBlocks;
        }

        // A special init function is available for ttc_sdcard.
        // Check implementation of ttc_filesystem_init_sdcard() to see how you
        // can configure ttc_filesystem to work with other storage devices!
        //
        // Note: ttc_filesystem_init_sdcard() is only available if the current board
        //       defines a ttc_sdcard device.
        //       Check extensions.active/makefile.110_board_* for details!
        //
        ttc_filesystem_init_sdcard( filesystem_Data.Idx_filesystem, filesystem_Data.Idx_sdcard );

        // Register a function to be run after all system has finished booting.
        // ttc_task_create() works in single- and multitasking environments.
        ttc_task_create(
#if (TTC_TASK_SCHEDULER_AVAILABLE)
            _example_filesystem_task,           // function to start as thread
#else
            _example_filesystem_statemachine,   // function to run periodically (single tasking)
#endif
            "tFILESYSTEM",                     // thread name (just for debugging)
            256,                             // stack size (adjust to amount of local variables of _example_filesystem_task() AND ALL ITS CALLED FUNTIONS!)
            &filesystem_Data, // passed as argument to _example_filesystem_task()
            1,                               // task priority (higher values mean more process time)
            NULL                             // can return a handle to created task
        );
    }
}

// Note: If you implement both of the example functions below, your example will run in single- and multitasking configuration.

void _example_filesystem_statemachine( void* Argument ) { // example statemachine will be run if no multitasking scheduler is available
    Assert_FILESYSTEM_Writable( Argument, ttc_assert_origin_auto );                // always check pointer arguments with Assert() before dereferencing them!
    t_example_filesystem_data* Data = ( t_example_filesystem_data* ) Argument; // we're expecting this pointer type

    if ( ttc_systick_delay_expired( & Data->Delay ) ) { // example periodic delay
        ttc_systick_delay_init( & Data->Delay,
                                ttc_systick_delay_get_minimum() << 4 // minimum delay * 16 is a safe way to always use a small and valid delay
                              ); // set new delay time

        e_ttc_storage_event Event = ttc_filesystem_medium_detect( Data->Idx_filesystem );
        if ( Event == E_ttc_storage_event_media_inserted ) {
            e_ttc_filesystem_errorcode Error = ttc_filesystem_medium_mount( Data->Idx_filesystem );
            if ( !Error ) { // medium mounted successfully: read root directory
                t_u8 Directory = 0; // empty string
                Error = ttc_filesystem_open_directory( Data->Idx_filesystem, &Directory, 0 );
                //...
                ttc_filesystem_medium_unmount( Data->Idx_filesystem );
            }
        }
    }

    // implement statemachine here ...
}
void _example_filesystem_task( void* Argument ) {         // example task will be run when multitasking scheduler (e.g. FreeRTOS) has been started
    Assert_FILESYSTEM_Writable( Argument, ttc_assert_origin_auto );                // always check pointer arguments with Assert() before dereferencing them!
    //t_example_filesystem_data* Data = ( t_example_filesystem_data* ) Argument; // we're expecting this pointer type

    // get pointer to device configuration (remove if not required)
    //t_ttc_filesystem_config* Config_filesystem = Data->Config_filesystem;

    do {
        // This loop runs endlessly.

        // call statemachine or implement task here...
        _example_filesystem_statemachine( Argument );

        ttc_task_yield(); // give cpu to other processes
    }
    while ( 1 ); // Tasks run endlessly.
}

//}Function Definitions
