#ifndef EXAMPLE_USART_H
#define EXAMPLE_USART_H

/** example_ttc_USART.h **********************************************{
 *
 *                              The ToolChain
 *
 * Empty template for new header files.
 * Copy and adapt to your needs.
 *
}*/
//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_usart.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_systick.h"  // exact delays and timeouts

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *  - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_usart_prepare();

/* runs echo server on initialized USART
 */
void task_USART( void* TaskArgument );

/* takes bytes received from USART
 */
t_ttc_heap_block* eu_receiveData( void* Argument, t_ttc_usart_config* USART_Generic, t_ttc_heap_block* Data );

/* release receive buffers to be reused by USART
 */
void releaseRxBuffer_USART1( t_ttc_heap_block* RxBuffer );
void releaseRxBuffer_USART2( t_ttc_heap_block* RxBuffer );

/** called automatically during transmit/ receive activity
  */
void exu_activity_rx( t_ttc_usart_config* USART_Generic, t_u8 Byte );
void exu_activity_tx( t_ttc_usart_config* USART_Generic, t_u8 Byte );

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_USART_H
