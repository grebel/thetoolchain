#ifndef EXAMPLE_I2C_H
#define EXAMPLE_I2C_H

/** example_ttc_I2C.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2014.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_i2c.
 *  The basic flow is as follows:
 *  (1) example_ttc_i2c_start() gets called in single tasking mode from ttc_extensions.c:startExtensions() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_i2c_start() gets executed
 *
 *  Created from template example_ttc_device.h revision 11 at 20140424 05:03:58 UTC
 *
 *  Authors: <AUTHOR>
 *  
}*/
//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_i2c.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_task.h"

#ifdef EXTENSION_500_ttc_gfx
#include "../ttc-lib/ttc_gfx.h"
#include "../ttc-lib/ttc_string.h"
#endif

#ifdef EXTENSION_400_sensor_lis3lv02dl
#include "../ttc-lib/sensors/accelerometer/accelerometer_lis3lv02dl.h" // accelerometer embedded into STM32-LCD prototype board
#endif

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_i2c_t
    u8_t Foo;  // example data
    // more arguments...
} example_i2c_t;

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *
 * - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_i2c_master_start();

void task_I2C_master(void *TaskArgument);

/** simple printf() at given coordinates
  */
void printfAt(u16_t X, u16_t Y, const char* String, s32_t Value);

/** send given buffer */
void sendData(const char* Buffer, u16_t MaxLength);


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_I2C_H
