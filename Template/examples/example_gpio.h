#ifndef EXAMPLE_LEDS
#define EXAMPLE_LEDS

/*{ ExampleLeds::.h ************************************************
 
 Empty template for new header files.
 Copy and adapt to your needs.
 
}*/
//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gpio.h"
#ifdef EXTENSION_ttc_task
#  include "ttc_task.h"
#endif
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_watchdog.h"
#ifndef TARGET_ARCHITECTURE_STM32W1xx
//#ifndef TARGET_ARCHITECTURE_STM32L1xx
// some features are not yet supported by low-level driver for stm32w
//#define EXAMPLE_LEDS_USE_INTERRUPTS

#include "../ttc-lib/ttc_interrupt.h"
//#endif
#endif

//}Includes
//{ Defines/ TypeDefs ****************************************************

#ifndef TTC_LED1
  #error TTC_LED1 not defined!

  // Example header-definition:
  // #define TTC_LED2   TTC_GPIO_C6    // status led 1 on Olimex proto board P107
  //
  // Example makefile-definition:
  // COMPILE_OPTS += -DTTC_LED2=TTC_GPIO_C6
#endif

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // t_configled
  t_ttc_Port Port;       // port where LED is connected
  t_u8 Brightness;       // 0 = off, 99 = 100% on
  bool Invert;           // == TRUE: invert brightness
} t_configled;

//}Structures/ Enums
//{ Function declarations **************************************************

// basic hardware intialization
void example_leds_init();

// main entry: initializes hardware + spawns all threads
// returns immediately
void example_leds_prepare();

// task: changes brightness continuosly
void taskBrightness(void *TaskArgument);

// task: applies brightness to single LED
void taskControlLed(void *TaskArgument);
void taskLed(t_configled *LED);
void taskLedOn(void *TaskArgument);
void taskLedOff(void *TaskArgument);

// toggles TTC_LED2 between on and off
void toggleLED();

// fast output of 16 bit value onto parallel port
void parallelOutput(t_u16 Value);

// manually polls current value of TTC_SWITCH1
void pollSwitch1();

#ifdef EXAMPLE_LEDS_USE_INTERRUPTS

// interrupt service routine: automatically called when switch is pressed
void isr_Switch1(t_physical_index PhysicalIndex, void* Argument);

#endif

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_LEDS
