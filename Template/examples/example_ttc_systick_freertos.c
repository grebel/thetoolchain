/** example_ttc_systick_freertos.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for system timer for precise time measures and periodice interrupt generation
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_systick_freertos_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_systick_freertos_prepare() get executed
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_systick_freertos_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template example_ttc_device_architecture.c revision 16 at 20160926 17:51:11 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_systick_freertos.h"
#include "../ttc-lib/ttc_systick.h" // high-level driver of device to use
#include "../ttc-lib/systick/systick_freertos.h" // direct access to low-level driver
#include "../ttc-lib/ttc_heap.h"     // dynamic memory allocations during runtime
#include "../ttc-lib/ttc_task.h"     // allows to use application in single- and multitasking setup

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_systick_freertos_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _example_systick_freertos_task(void *Argument);

//}Private Function Declarations
//{ Function Definitions *************************************************

void example_ttc_systick_freertos_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    example_systick_data_t* Argument = ttc_heap_alloc_zeroed( sizeof(example_systick_data_t) );
    
    Argument->Index_SYSTICK = 1; // set logical index of device to use
    
    // get configuration of systick device
    t_ttc_systick_config* Config_SYSTICK = ttc_systick_get_configuration(Argument->Index_SYSTICK);
    
    // change configuration before initializing device
    //...
    
    // initializing our systick device here makes debugging easier (still single-tasking)
    // Logical index is stored inside each device configuration
    ttc_systick_init(Config_SYSTICK->LogicalIndex);

    ttc_task_create( _example_systick_freertos_task,       // function to start as thread
                     "tSYSTICK_FREERTOS",          // thread name (just for debugging)
                     256,                                 // stack size (adjust to amount of local variables of _example_systick_freertos_task() AND ALL ITS CALLED FUNTIONS!) 
                     &et_systick_freertos_Data,    // passed as argument to _example_systick_freertos_task()
                     1,                                   // task priority (higher values mean more process time)
                     NULL                                 // can return a handle to created task
                   );
}
void _example_systick_freertos_task(void *Argument) {
    Assert_BASIC_Writable(Argumen, ttc_assert_origin_auto);                  // always check pointer arguments with Assert() before dereferencing them!
    example_systick_freertos_data_t* Data = ( example_systick_freertos_data_t* ) Argument; // we're expecting this pointer type

    // get pointer to device configuration (remove if not required)
    t_ttc_systick_config* Config_SYSTICK = ttc_systick_get_configuration(Data->Index_SYSTICK);

    do { 
        // This loop runs endlessly if scheduler is available.
        // Otherwise it will run only onve and be called periodically from ttc_task_start_scheduler().
  
        if ( ttc_systick_delay_expired( & Data->Delay ) ) { // delay passed: send hello world (zeroed delay always expires)
            ttc_systick_delay_init( & Data->Delay, 1000 ); // set new delay to expire in 1 millisecond = 1000 microseconds
    
          // implement statemachine of this task here...
        }
        ttc_task_yield(); // if scheduler available, give cpu to other processes
    } while (TTC_TASK_SCHEDULER_AVAILABLE);
}

//}Function Definitions
