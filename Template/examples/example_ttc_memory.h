#ifndef EXAMPLE_MEMORY_H
#define EXAMPLE_MEMORY_H

/** example_ttc_MEMORY.h **********************************************{
 *
 *                              The ToolChain
 *
 * Empty template for new header files.
 * Copy and adapt to your needs.
 *
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc_memory.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_memory_t
    t_u8 Foo;  // example data
    // more arguments...
} example_memory_t;

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *  - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_memory_prepare();

/** task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void task_MEMORY(void *TaskArgument);


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_MEMORY_H
