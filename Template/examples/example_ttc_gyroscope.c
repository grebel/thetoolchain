/** example_ttc_gyroscope.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_gyroscope.
 *  The basic flow is as follows:
 *  (1) example_ttc_gyroscope_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_gyroscope_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 11 at 20150121 10:09:22 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_gyroscope.h"

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
example_gyroscope_t et_gyroscope_TaskArguments;

#define I2C_INDEX 2

DEPRECATED_ttc_i2c.t_config   *    I2C_Generic;  // architecture independent I2C configuration

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_gyroscope_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */
   et_gyroscope_TaskArguments.Foo = 1; // set some arguments to be passed to task


   /* Init I2C */
   e_ttc_i2c_errorcode Error= tie_OK;
   Assert( Error == tie_OK, ttc_assert_origin_auto);
   I2C_Generic = ttc_i2c_get_configuration(I2C_INDEX);
   I2C_Generic->Flags.Bits.Master = 1;

#ifdef EXTENSION_100_board_stm32l100C_discovery
   /*It has not importance in master mode */
   I2C_Generic->OwnAddress = 0xA0;

   /* Number of bytes to use in DMA */
   I2C_Generic->LowLevelConfig->NumBytes=6;
#endif

   Error = ttc_i2c_init(I2C_INDEX);
   Assert( Error == tie_OK, ttc_assert_origin_auto);


   ttc_task_create( task_GYROSCOPE,               // function to start as thread
                    "tGYROSCOPE",                 // thread name (just for debugging)
                    128,                         // stack size
                    &et_gyroscope_TaskArguments,  // passed as argument to task_GYROSCOPE()
                    1,                           // task priority (higher values mean more process time)
                    NULL                         // can return a handle to created task
                  );
}
void task_GYROSCOPE(void *TaskArgument) {
    Assert(TaskArgument, ttc_assert_origin_auto);                  // always check pointer arguments with Assert()!
    example_gyroscope_t* TaskArguments = (example_gyroscope_t*) TaskArgument; // we're expecting this pointer type

    /* Init Gyroscope */
    t_ttc_gyroscope_config* Gyr_Config = ttc_gyroscope_get_configuration(1);

    Gyr_Config->LogicalIndex = 1;
    Gyr_Config->LogicalIndex_Interface = I2C_INDEX;

    e_ttc_gyroscope_errorcode Error_acc;
    Error_acc= ttc_gyroscope_init(1);
    Assert( Error_acc == ec_gyroscope_OK, ttc_assert_origin_auto);

    t_ttc_gyroscope_measures *Measures;

    while (1) {
      // do something...
      Measures = ttc_gyroscope_read_measures(1);

      ttc_task_msleep(100);    // always sleep for a while to give cpu time to other tasks
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
