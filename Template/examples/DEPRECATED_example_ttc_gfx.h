#ifndef EXAMPLE_GFX_H
#define EXAMPLE_GFX_H

/** example_ttc_GFX.h **********************************************{
 *
 *                              The ToolChain
 *
 * Empty template for new header files.
 * Copy and adapt to your needs.
 *
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_gfx.h"
#include "../ttc-lib/ttc_string.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_gfx_t
    u8_t Foo;  // example data
    // more arguments...
} example_gfx_t;

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *  - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_gfx_start();

/** task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void task_GFX(void *TaskArgument);


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_GFX_H
