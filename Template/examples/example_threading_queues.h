#ifndef EXAMPLE_THREADING
#define EXAMPLE_THREADING

/*{ TemplateName::.h ************************************************
 
 Empty template for new header files.
 Copy and adapt to your needs.
 
}*/
//{ Defines/ TypeDefs ****************************************************

#define LED_STAT1 GPIOC, 6
#define LED_STAT2 GPIOC, 7

//}Defines
//{ Includes *************************************************************

 // Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_task.h"
#include "../ttc-lib/ttc_queue.h"
#include "../ttc-lib/ttc_memory.h"

#ifdef EXTENSION_ttc_gfx
#  include "ttc_string.h"
#  include "ttc_gfx.h"
#endif

//}Includes
//{ Structures/ Enums ****************************************************

typedef struct { // t_et_printjob
    // Column == -1 | Row == -1: continue at last position
    t_s8 Column;   // >= 0: Column to move cursor to
    t_s8 Row;      // >= 0: Row to move cursor to
    char Text[10]; // zero terminated string to print
    t_s32 Value;   // passed as value to printf
} t_et_printjob;

/** data being sent to taskProducer()/ taskConsumer() */
typedef struct { // et_ThreadInfo_t
    t_ttc_queue_generic QueueProducer2Consumer;
    t_ttc_queue_generic QueuePrint;
} et_ThreadInfo_t;

//}Structures/ Enums
//{ Global Variables *****************************************************



//}Global Variables
//{ Function declarations **************************************************

// basic initialization (called from example_threading_queues_prepare() )
void example_threading_queues_init();

// run demo
void example_threading_queues_prepare();

// produces tasks to do
void taskProducer(void *TaskArgument);

// consumes + executes tasks to do
void taskConsumer(void *TaskArgument);

// adds given data as new print job to given queue
// Will block if queue is full
void queuePrintJob(t_ttc_queue_generic PrintQueue, t_s8 Column, t_s8 Row, const char* Text, t_s32 Value);

// central print task (display cannot be used from multiple tasks
void taskPrint(void *TaskArgument);


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_THREADING
