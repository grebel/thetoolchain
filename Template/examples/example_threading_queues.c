/*{ ExampleThreading::.c ************************************************

 Example code demonstrating how to use inter thread communication.
 
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_threading_queues.h"


#ifdef EXTENSION_ttc_gfx
  const char* Example   = "Example";
  const char* Threading = "Threading";
  const char* Queues    = "Queues";

  // configuration data of connected display
  t_ttc_gfx_generic* DisplayConfig = NULL;
#endif

//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_threading_queues_init() {
#ifdef EXTENSION_ttc_gfx
    t_u8 MaxDisplay = ttc_gfx_get_max_display_index();
    Assert(MaxDisplay > 0, ttc_assert_origin_auto); // no displays defined by board makefile!
    DisplayConfig = ttc_gfx_get_configuration(1);
    Assert(DisplayConfig, ttc_assert_origin_auto);

    // set color to use for first screen clear
    ttc_gfx_color_bg24(GFX_COLOR24_GREEN);
    ttc_gfx_color_fg24(GFX_COLOR24_WHITE);
    ttc_gfx_init(1);

    ttc_gfx_text_solid_at(ttc_gfx_text_center(Example, -1),   3, Example,   -1);
    ttc_gfx_text_solid_at(ttc_gfx_text_center(Threading, -1), 4, Threading, -1);
#endif
}
void example_threading_queues_prepare() {
    example_threading_queues_init();

    static et_ThreadInfo_t ThreadInfo; // must be static to survive end of this function!

    // create queue Producer -> Consumer
    ThreadInfo.QueueProducer2Consumer = ttc_queue_generic_create(10, 1);

    // create queue Producer/Consumer -> Print
    ThreadInfo.QueuePrint = ttc_queue_generic_create(5, sizeof(t_et_printjob));

    Assert(NULL != ThreadInfo.QueueProducer2Consumer, ttc_assert_origin_auto);
    Assert(NULL != ThreadInfo.QueuePrint, ttc_assert_origin_auto);

    BOOL Task1 = ttc_task_create(taskProducer,                    // function to start as thread
                                 "taskProducer",                  // thread name (just for debugging)
                                 8 * TTC_TASK_MINIMAL_STACK_SIZE, // stack size
                                 (void *) &ThreadInfo,            // passed as argument to taskValue()
                                 1,                               // task priority (higher values mean more process time)
                                 NULL                             // can return a handle to created task
                                 );

    BOOL Task2 = ttc_task_create(taskConsumer,                    // function to start as thread
                                 "taskConsumer",                  // thread name (just for debugging)
                                 8 * TTC_TASK_MINIMAL_STACK_SIZE, // stack size
                                 (void *) &ThreadInfo,            // passed as argument to taskValue()
                                 1,                               // task priority (higher values mean more process time)
                                 NULL                             // can return a handle to created task
                                 );


    Assert(Task1, ttc_assert_origin_auto);
    Assert(Task2, ttc_assert_origin_auto);

#ifdef EXTENSION_ttc_gfx
    BOOL Task3 = ttc_task_create(taskPrint,                       // function to start as thread
                                 "taskPrint",                     // thread name (just for debugging)
                                 8 * TTC_TASK_MINIMAL_STACK_SIZE, // stack size
                                 (void *) &ThreadInfo,            // passed as argument to taskValue()
                                 1,                               // task priority (higher values mean more process time)
                                 NULL                             // can return a handle to created task
                                 );
    Assert(Task3, ttc_assert_origin_auto);

#endif
}
void taskProducer(void *TaskArgument) {
    et_ThreadInfo_t* ThreadInfo = (et_ThreadInfo_t*) TaskArgument;
    Assert(NULL != ThreadInfo, ttc_assert_origin_auto);
    Assert(NULL != ThreadInfo->QueueProducer2Consumer, ttc_assert_origin_auto);

#ifdef EXTENSION_ttc_gfx
    for (int Row = 0; Row < DisplayConfig->TextRows; Row++)
        queuePrintJob(ThreadInfo->QueuePrint, 0, Row, "%x", 0xf & Row);
    for (int Col = 0; Col < DisplayConfig->TextColumns; Col++)
        queuePrintJob(ThreadInfo->QueuePrint, Col, 0, "%x", 0xf & Col);

    queuePrintJob(ThreadInfo->QueuePrint, ttc_gfx_text_center(Queues, -1), 5, Queues, 0);
    t_base BytesFree = ttc_heap_get_free_size();
    queuePrintJob(ThreadInfo->QueuePrint, 2, DisplayConfig->TextRows - 1, "free: %iB", BytesFree);
    queuePrintJob(ThreadInfo->QueuePrint, 2, 7, "Producer>", 0);
#endif
#ifdef TTC_LED1
    ttc_gpio_init(TTC_LED1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min);
#endif

    t_u8 Value = 0; //(t_u16) rand() & 0xfff;
    while (1) {
        while (ttc_queue_generic_push_back(ThreadInfo->QueueProducer2Consumer, &Value, -1) != tqe_OK);

#ifdef EXTENSION_ttc_gfx
        queuePrintJob(ThreadInfo->QueuePrint, 11, 7, "%i ", Value);
#endif

        t_base QueueSize = ttc_queue_amount_waiting(ThreadInfo->QueueProducer2Consumer);
#ifdef EXTENSION_ttc_gfx
        queuePrintJob(ThreadInfo->QueuePrint, 2, 9, "|Q| = %i ", (t_u16) QueueSize);
#else
        (void) QueueSize;
#endif
#ifdef TTC_LED1
        for (int I = Value; I >= 0; I--) {
            ttc_gpio_set(TTC_LED1);
            ttc_task_msleep(100);
            ttc_gpio_clr(TTC_LED1);
            ttc_task_msleep(100);
        }
#endif
        ttc_task_msleep(500);

        Value++;
        if (Value == 10) Value = 0;
    }
}
void taskConsumer(void *TaskArgument) {
    et_ThreadInfo_t* ThreadInfo = (et_ThreadInfo_t*) TaskArgument;
    Assert(NULL != ThreadInfo, ttc_assert_origin_auto);
    Assert(NULL != ThreadInfo->QueueProducer2Consumer, ttc_assert_origin_auto);

#ifdef EXTENSION_ttc_gfx
    queuePrintJob(ThreadInfo->QueuePrint, 2, 8, "Consumer>", 0);
#endif
#ifdef TTC_LED2
    ttc_gpio_init(TTC_LED2, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_min);
#endif

    t_u8 ReceivedValue = 0;
    while (1) {
        if ( ttc_queue_generic_pull_front(ThreadInfo->QueueProducer2Consumer, &ReceivedValue,  (portTickType) -1) == tqe_OK ) {
            Assert( ReceivedValue <= 100, ttc_assert_origin_auto );
#ifdef EXTENSION_ttc_gfx
            queuePrintJob(ThreadInfo->QueuePrint, 11, 8, "%i ", ReceivedValue);
#endif
#ifdef TTC_LED2
            for (int I = ReceivedValue; I >= 0; I--) {
                ttc_gpio_set(TTC_LED2);
                ttc_task_msleep(100);
                ttc_gpio_clr(TTC_LED2);
                ttc_task_msleep(100);
            }
#endif
            ttc_basic_test_ok(); // test run successfull
            ttc_task_msleep(600); // consumer is a little bit slower than producer
        }
    }
}

#ifdef EXTENSION_ttc_gfx
void queuePrintJob(t_ttc_queue_generic PrintQueue, t_s8 Column, t_s8 Row, const char* Text, t_s32 Value) {
    t_et_printjob PrintJob;
    PrintJob.Column = Column;
    PrintJob.Row    = Row;
    PrintJob.Value  = Value;
    strncpy(PrintJob.Text, Text, 10);

    while (ttc_queue_generic_push_back(PrintQueue, &PrintJob, -1) != tqe_OK);
}
void taskPrint(void *TaskArgument) {
    et_ThreadInfo_t* ThreadInfo = (et_ThreadInfo_t*) TaskArgument;
    Assert(NULL != ThreadInfo, ttc_assert_origin_auto);
    const t_u8 tP_BufferSize = 100;
    t_u8 Buffer[tP_BufferSize];

    while (1) {
        t_et_printjob PrintJob;
        if (ttc_queue_generic_pull_front(ThreadInfo->QueuePrint, &PrintJob,  (portTickType) -1) == tqe_OK) {
#ifdef EXTENSION_ttc_gfx
            if ( (PrintJob.Column != -1) && (PrintJob.Row != -1) )
                ttc_gfx_text_cursor_set(PrintJob.Column, PrintJob.Row);

            // create fixed string
            ttc_string_snprintf(Buffer, tP_BufferSize, PrintJob.Text, PrintJob.Value);
            ttc_gfx_text_solid( (char*) Buffer, tP_BufferSize);
#endif
        }
    }
}
#endif

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
