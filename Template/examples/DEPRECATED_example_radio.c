/*{ example_radio.c ******************************************************
 
  Basic test of radio transceivers without any protocol.

  Initializes first radio and periodically transmits data.
  Received data is echoed via TTC_USART1.

}*/

#include "example_radio.h"

//{ Function definitions *************************************************

// configuration data of single radio
ttc_radio_generic_t* MyRadio1;
ttc_radio_generic_t* MyRadio2;

// randomly chosen ID of this node
u8_t MyID = 0;
u8_t TxLedState = 0;
u8_t RxLedState = 0;

#define LED_TX TTC_LED1
#define LED_RX TTC_LED2

// disable individual radios
// #undef TTC_RADIO1
// #undef TTC_RADIO2

void example_radio_start() {
    example_radio_init();
#ifdef TTC_RADIO1
    ttc_task_create( taskRadio,                    // function to start as thread
                     "taskRadio1",                 // thread name (just for debugging)
                     TTC_TASK_MINIMAL_STACK_SIZE*2,  // stack size
                     (void *) 1,                   // passed as argument to taskRadio()
                     1,                            // task priority (higher values mean more process time)
                     (void *) NULL                 // can return a handle to created task
                   );
#endif

#ifdef TTC_RADIO2
    ttc_task_create( taskRadio,                    // function to start as thread
                     "taskRadio2",                 // thread name (just for debugging)
                     TTC_TASK_MINIMAL_STACK_SIZE,  // stack size
                     (void *) 2,                   // passed as argument to taskRadio()
                     1,                            // task priority (higher values mean more process time)
                     (void *) NULL                 // can return a handle to created task
                   );
#endif
}
void example_radio_init() {
    ttc_gpio_init(LED_TX, tgm_output_push_pull);
    ttc_gpio_init(LED_RX, tgm_output_push_pull);


#if TTC_DEBUG_RADIO==1
    ttc_gpio_init(TTC_DEBUG_RADIO_PIN1, tgm_output_push_pull);
    ttc_gpio_init(TTC_DEBUG_RADIO_PIN2, tgm_output_push_pull);
    ttc_gpio_init(TTC_DEBUG_RADIO_PIN3, tgm_output_push_pull);
#endif

}
void taskRadio(void *TaskArgument) {

    u8_t RadioIndex = 0xff & (u32_t) TaskArgument;
#define ExampleRadio_TX_Buffer_SIZE 30
    ttc_heap_array_define(u8_t, ExampleRadio_TX_Buffer, ExampleRadio_TX_Buffer_SIZE);
    u16_t TxCount = 0;

    ttc_radio_init(RadioIndex,0xB5);
    ttc_radio_register_tx_function(1, txFunction_R1);
    ttc_radio_register_rx_function(1, rxFunction_R1);

    ttc_radio_set_destination(RadioIndex, 255);//Broadcast Packet

    ttc_radio_duty_cycle_config(RadioIndex,duty_cycle_limit_1_percent,duty_cycle_Timeslot_60_sek,ttc_radio_getDatarate(RadioIndex));

    for (int q = 0; q < ExampleRadio_TX_Buffer_SIZE; q++) {
        A(ExampleRadio_TX_Buffer, q)= q + 0xa;
    }
    u8_t * ExampleRadio_TX_Buffer_POINTER =  memset(&(A(ExampleRadio_TX_Buffer, 0)), 1, sizeof(u8_t));

    while (1) {

        ttc_radio_errorcode_e Error;

        Error = ttc_radio_send_raw(RadioIndex,ExampleRadio_TX_Buffer_POINTER,ExampleRadio_TX_Buffer_SIZE);
        Assert_Radio(Error == tre_OK, ec_UNKNOWN);
        TxCount++;
        ttc_task_msleep(500);
    }
}

void txFunction_R1(ttc_radio_generic_t* RadioCfg, u8_t Size, const u8_t* Buffer) {
    Assert(RadioCfg!=NULL, ec_NULL);
    (void) RadioCfg;
    TxLedState = 1 - TxLedState;
    ttc_gpio_setn(LED_TX, TxLedState);

}
void txFunction_R2(ttc_radio_generic_t* RadioCfg, u8_t Size, const u8_t* Buffer) {
    (void) RadioCfg;
    TxLedState = 1 - TxLedState;
    ttc_gpio_setn(LED_TX, TxLedState);

}
ttc_heap_block_t* rxFunction_R1(ttc_radio_generic_t* RadioCfg, ttc_heap_block_t* Block, u8_t RSSI, u8_t TargetAddress) {
    (void) RadioCfg;
    (void) Block;

    RxLedState = 1 - RxLedState;
    ttc_gpio_setn(LED_RX, RxLedState);

    return Block; // let radio reuse this block immediately
}
ttc_heap_block_t* rxFunction_R2(ttc_radio_generic_t* RadioCfg, ttc_heap_block_t* Block, u8_t RSSI, u8_t TargetAddress) {
    (void) RadioCfg;
    (void) Block;

    RxLedState = 1 - RxLedState;
    ttc_gpio_setn(LED_RX, RxLedState);

    return Block; // let radio reuse this block immediately
}

//}FunctionDefinitions

