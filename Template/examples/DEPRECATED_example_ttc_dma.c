/** example_ttc_dma.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2016.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_dma.
 *  The basic flow is as follows:
 *  (1) example_ttc_dma_start() gets called in single tasking mode from ttc_extensions.c:startExtensions() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_dma_start() gets executed
 *
 *  Created from template example_ttc_device.c revision 11 at 20150108 10:24:10 UTC
 *
 *  Authors: <AUTHOR>
 *  
}*/

#include "example_ttc_dma.h"

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
example_dma_t et_dma_TaskArguments;

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_dma_start() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */
   et_dma_TaskArguments.Foo = 1; // set some arguments to be passed to task

   ttc_task_create( task_DMA,               // function to start as thread
                    "tDMA",                 // thread name (just for debugging)
                    128,                         // stack size
                    &et_dma_TaskArguments,  // passed as argument to task_DMA()
                    1,                           // task priority (higher values mean more process time)
                    NULL                         // can return a handle to created task
                  );
}
void task_DMA(void *TaskArgument) {
    Assert(TaskArgument, ec_NULL);                  // always check pointer arguments with Assert()!
    example_dma_t* TaskArguments = (example_dma_t*) TaskArgument; // we're expecting this pointer type

    while (1) {
      // do something...
      ttc_task_msleep(100);    // always sleep for a while to give cpu time to other tasks
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
