#ifndef EXAMPLE_RADIO_SERIAL
#define EXAMPLE_RADIO_SERIAL

/*{ ExampleRadio.h *******************************************************
 
  Initializes first radio and periodically transmits data.
  Received data is echoes via TTC_USART1.

}*/
//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_task.h"
#include "../ttc-lib/ttc_random.h"
#include "../ttc-lib/ttc_watchdog.h"
#include "../ttc-lib/DEPRECATED_ttc_radio.h"
#include "../ttc-lib/ttc_usart.h"
#include "../ttc-lib/ttc_string.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************


#ifndef TTC_TX
  #ifdef TTC_LED1
    #define TTC_TX TTC_LED1
  #else
    #warning TTC_TX not defined!
  #endif
#endif
#ifndef TTC_RX
  #ifdef TTC_LED2
    #define TTC_RX TTC_LED2
  #else
    #warning TTC_RX not defined!
  #endif
#endif

//}Defines
//{ Structures/ Enums ****************************************************

//}Structures/ Enums
//{ Global Variables *****************************************************



//}Global Variables
//{ Function declarations **************************************************

// basic hardware intialization
void example_radio_serial_init();

// main entry: initializes hardware + spawns all threads
// returns immediately
void example_radio_serial_start();

// observes every outgoing packet
void txFunction_R1(ttc_radio_generic_t* RadioCfg, u8_t Size, const u8_t* Buffer);

// handles incoming packets
ttc_heap_block_t* rxFunction_R1(ttc_radio_generic_t* RadioCfg, ttc_heap_block_t* Block, u8_t RSSI, u8_t TargetAddress);

//receive function for uart
ttc_heap_block_t* receiveData(ttc_usart_config_t* USART_Generic, ttc_heap_block_t* Data);

//
/** function to reload the Watchdog
 * @param Index logical index of the task that has called this watchdog.
 */
void reloadWatchdog(u8_t Index);

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_RADIO
