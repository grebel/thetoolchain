/*{ TestRMA::.c ************************************************
 
 Empty template for new c-files.
 Copy and adapt to your needs.
 
}*/

#include "example_i2c.h"

//{ Global variables *************************************************

// read-only configuration of current display (width, height, ...)
#ifdef EXTENSION_500_ttc_gfx
ttc_gfx_generic_t* DisplayConfig = NULL;
#endif
DEPRECATED_ttc_i2c.config_t       I2C_Generic;  // architecture independent I2C configuration
ttc_i2c_architecture_t  I2C_Arch;     // architecture specific I2C configuration
#define I2C_INDEX 2                   // using first I2C available on current board
u8_t Address_InertialSensor = 29;
u16_t AmountTransfers = 0;

//}Global Variables
//{ Function definitions *********************************************

void example_i2c_init() {
#ifdef EXTENSION_500_ttc_gfx
    u8_t MaxDisplay = ttc_gfx_get_max_display_index();
    Assert(MaxDisplay > 0, ec_DeviceNotFound); // no displays defined by board makefile!
    DisplayConfig = ttc_gfx_get_configuration(1);
    Assert(DisplayConfig, ec_DeviceNotFound);

    // set color to use for first screen clear
    ttc_gfx_set_color_bg24(GFX_COLOR24_AQUA);
    ttc_gfx_init(1);
#endif

    u8_t MaxI2C = ttc_i2c_get_max_index();
    Assert(MaxI2C > 0, ec_UNKNOWN);

    ttc_i2c_errorcode_e Error;

    Error = ttc_i2c_get_defaults(I2C_INDEX, &I2C_Generic);
    Assert( Error == tie_OK, ec_UNKNOWN);

    #ifdef EXTENSION_600_example_i2c_master
    I2C_Generic.Flags.Bits.Master = 1;
    #else
    I2C_Generic.Flags.Bits.Slave = 1;
    #endif

    //I2C_Generic.Flags.Bits.
    Error = ttc_i2c_init(I2C_INDEX);
    Assert( Error == tie_OK, ec_UNKNOWN);

}
void example_i2c_start() {
    example_i2c_init();

#ifdef EXTENSION_600_example_i2c_master
    ttc_task_create(task_I2C_master,                 // function to start as thread
                    "I2Ctx",                         // thread name (just for debugging)
                    512,                             // stack size
                    (void *) NULL,                   // passed as argument to taskLEDs()
                    1,                               // task priority (higher values mean more process time)
                    (void*) NULL                     // can return a handle to created task
                    );
#else
    ttc_task_create(task_I2C_slave,                  // function to start as thread
                    "I2Crx",                         // thread name (just for debugging)
                    512,                    // stack size
                    (void *) NULL,                   // passed as argument to taskLEDs()
                    1,                               // task priority (higher values mean more process time)
                    (void*) NULL                     // can return a handle to created task
                    );
#endif
}
void printfAt(u16_t X, u16_t Y, const char* String, s32_t Value) {


#ifdef EXTENSION_500_ttc_gfx
     char Buffer[20];
    ttc_string_snprintf( (u8_t*) Buffer, 20, String, Value);
    ttc_gfx_print_solid_at(X, Y, Buffer, 20);
#endif

}
#ifdef EXTENSION_600_example_i2c_master
void task_I2C_master(void *TaskArgument) {
    TaskArgument = TaskArgument; // avoids warning: unused parameter 'TaskArgument'


#ifdef EXTENSION_400_sensor_lis3lv02dl
#  ifdef EXTENSION_500_ttc_gfx
    ttc_gfx_text_cursor_set(0,0);
    ttc_gfx_print("LIS3LV02DL ??", -1);
#  endif

    lis3lv02dl_errorcode_e SensorError;
    SensorError = i2c_lis3lv02dl_init(I2C_INDEX);
    Assert(SensorError == lie_OK, ec_UNKNOWN);

    u8_t StatusRegister = 0;
    ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_REG_WHO_AM_I, tiras_MSB_First_8Bit, &StatusRegister);
    Assert(StatusRegister == 0x3A, ec_DeviceNotFound);

#  ifdef EXTENSION_500_ttc_gfx
    ttc_gfx_print_solid_at(11, 0, "OK", -1);
    ttc_gfx_print_solid_at( 0, 2, "X:       mG", -1);
    ttc_gfx_print_solid_at( 0, 3, "Y:       mG", -1);
    ttc_gfx_print_solid_at( 0, 4, "Z:       mG", -1);
#  endif

    s16_t AccelerationX;
    s16_t AccelerationY;
    s16_t AccelerationZ;

    while (1) {
        ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_OUTX_L, tiras_MSB_First_8Bit, (u8_t*) &AccelerationX );
        ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_OUTX_H, tiras_MSB_First_8Bit, ( (u8_t*) &AccelerationX ) + 1);

        ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_OUTY_L, tiras_MSB_First_8Bit, (u8_t*) &AccelerationY );
        ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_OUTY_H, tiras_MSB_First_8Bit, ( (u8_t*) &AccelerationY ) + 1);

        ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_OUTZ_L, tiras_MSB_First_8Bit, (u8_t*) &AccelerationZ );
        ttc_i2c_read_register(I2C_INDEX, I2C_ADDRESS_LIS3LV02DL, LIS_OUTZ_H, tiras_MSB_First_8Bit, ( (u8_t*) &AccelerationZ ) + 1);

#  ifdef EXTENSION_500_ttc_gfx
        printfAt(3, 2, "%5i   ", AccelerationX);
        printfAt(3, 3, "%5i   ", AccelerationY);
        printfAt(3, 4, "%5i   ", AccelerationZ);
#  endif

        ttc_task_msleep(100);
    }
#else
    while (1) {

        // ToDo: Read in sensor data
        ttc_task_msleep(100);
    }
#endif
}
#else
void task_I2C_slave(void *TaskArgument) {
    TaskArgument = TaskArgument; // avoids warning: unused parameter 'TaskArgument'

#define Size_ReceiveBuffer 20
    // char ReceiveBuffer[Size_ReceiveBuffer];
    // ReceiveBuffer[0] = 0;
    //char* Writer = ReceiveBuffer;
    //char* WriterEnd = Writer + Size_ReceiveBuffer - 1;

    while (1) {
        // ToDo: implement I2C-slave


        uSleep(1);
    }
}
#endif
//}FunctionDefinitions
