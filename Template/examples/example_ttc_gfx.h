#ifndef EXAMPLE_GFX_H
#define EXAMPLE_GFX_H

/** example_ttc_GFX.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for ili93xx 320x240x24 color tft driver
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_gfx_ili93xx_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_gfx_ili93xx_prepare() get executed
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_gfx_ili93xx_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template example_ttc_device.h revision 15 at 20160817 15:27:21 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_GFX.c only do not belong here!
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gfx_types.h"
#include "../ttc-lib/ttc_task_types.h"
#include "../ttc-lib/ttc_systick.h"  // exact delays and timeouts
//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

#define egd_BUFFER_SIZE 20
typedef struct { // example_gfx_t
    t_u8              Index_GFX;               // logical index of device to use
    t_ttc_systick_delay  Delay;                   // this type of delay is usable in single- and multitasking setup
    t_u8              Buffer[egd_BUFFER_SIZE]; // buffer used to compile strings to display
    t_base            Seconds;                 // seconds passed since start of example
    // more arguments...
} example_gfx_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_gfx_prepare();


//}Function prototypes

#endif //EXAMPLE_GFX_H
