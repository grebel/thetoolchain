/*{ Template_FreeRTOS::.c ************************************************

  leds_start() spawns thread taskBrightness()

  taskBrightness()
      Defines brightness of led as Ton / (Ton + Toff)
      spawns thread taskControlLed()
      while (1) {
        while (Ton < 100) { Ton++; Toff--; }
        while (Ton >   0) { Ton--; Toff++; }
      }

  taskControlLed()
      while (1) {
        switch on LED
        wait for Ton
        switch off LED
        wait for Toff
      }

}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_leds.h"

//{ Global variables

t_configled  el_LED1;
t_configled* el_LED2 = NULL;
bool SwitchInterrupt = FALSE; // ==TRUE: switch is controlled via interrupt; polling otherwise
t_u32 NoToggleBefore = 0;     // time when LED is allowed to be toggled again

// define parallel banks to use for parallel output in this example
#ifdef TTC_GPIO_PARALLEL16_1_BANK
  #define PARALLEL16_BANK TTC_GPIO_PARALLEL16_1_BANK
#else
  // using 8-bit ports if no 16-bit port available
  #ifdef TTC_GPIO_PARALLEL08_1_BANK
    #define PARALLEL08_1_BANK     TTC_GPIO_PARALLEL08_1_BANK
    #define PARALLEL08_1_FIRSTPIN TTC_GPIO_PARALLEL08_1_FIRSTPIN
  #endif
  #ifdef TTC_GPIO_PARALLEL08_2_BANK
    #define PARALLEL08_2_BANK     TTC_GPIO_PARALLEL08_2_BANK
    #define PARALLEL08_2_FIRSTPIN TTC_GPIO_PARALLEL08_2_FIRSTPIN
  #endif
#endif

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_leds_init() {

#ifdef TTC_SWITCH1 //{ initialize + enable port + interrupt for external switch TTC_SWITCH1
    ttc_gpio_init(TTC_SWITCH1, E_ttc_gpio_mode_input_floating);

#ifdef EXAMPLE_LEDS_USE_INTERRUPTS // try to initialize interrupt to check switch
        if (tine_OK == ttc_interrupt_init(tit_GPIO_Rising,
                                          ttc_gpio_create_index8(TTC_SWITCH1), // convert GPIO,PIN -> PhysicalIndex
                                          isr_Switch1,
                                          0, 0, 0)
                )
        {
            SwitchInterrupt = TRUE;
            ttc_interrupt_enable(tit_GPIO_Rising, ttc_gpio_create_index8(TTC_SWITCH1), TRUE);
        }
#endif

#endif //}
    // initializing ports as variables consumes more memory than a simple ttc_gpio_init(),
    // but it allows to pass a port configuration like a variable (hence the name)
    el_LED1.Brightness = 99;
    ttc_gpio_init_variable(&(el_LED1.Port), TTC_LED1, E_ttc_gpio_mode_output_push_pull);

#ifdef TTC_LED2
    el_LED2 = ttc_heap_alloc_zeroed(sizeof(t_configled));
    el_LED2->Brightness = 0;
    ttc_gpio_init_variable(&(el_LED2->Port), TTC_LED2, E_ttc_gpio_mode_output_push_pull);
#else
    // no TTC_LED2 defined: use same LED
    el_LED2 = &el_LED1;
#endif

#ifdef EXTENSION_ttc_watchdog
    ttc_watchdog_init1(0,0);
#endif

    // initialize parallel output ports (if available on current board)
    //
    // Some boards can provide 16-bit wide ports like Olimex H107.
    // Other boards may provide only one or more 8-bit ports.
    // This implementation can handle both situations.
    //
    // See activated board makefile for details of provided parallel ports.

#ifdef PARALLEL16_BANK

    // initialize 16-bit wide output port
    ttc_gpio_parallel16_init(PARALLEL16_BANK, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max);
    #define USE_BANK16
#else
#ifdef PARALLEL08_1_BANK
    // initialize 8-bit wide output port
    ttc_gpio_parallel08_init4(PARALLEL08_1_BANK, PARALLEL08_1_FIRSTPIN,  E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max);

    #define USE_BANK8_1
#endif

#ifdef PARALLEL08_2_BANK
    // initialize 8-bit wide output port
    ttc_gpio_parallel08_init4(PARALLEL08_2_BANK, PARALLEL08_2_FIRSTPIN,  E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max);

    #define USE_BANK8_2
#endif

#endif
}
void example_leds_prepare() {
    example_leds_init();

#ifdef TARGET_ARCHITECTURE_STM32W1xx
    // stm32w currently has no FreeRTOS support
    ttc_gpio_init(TTC_LED1, E_ttc_gpio_mode_output_push_pull);
    while (1) {
        ttc_gpio_clr(TTC_LED1);
        for(int i = 0 ; i<50000;i++){}
        ttc_gpio_set(TTC_LED1);
        for(int i = 0 ; i<50000;i++){}
    }
#endif

    ttc_task_create( taskBrightness,   // function to start as thread
                     "taskBrightness", // thread name (just for debugging)
                     128,              // stack size
                     NULL,             // passed as argument to taskBrightness()
                     1,                // task priority (higher values mean more process time)
                     NULL              // can return a handle to created task
                     );
}
void reloadWatchdog() {
#ifdef EXTENSION_ttc_watchdog    
    if (0) ttc_watchdog_reload1();
#endif
}
void taskBrightness(void *TaskArgument) {
    TaskArgument = TaskArgument; // avoids warning: unused parameter 'TaskArgument'
    
    if (1) { ttc_task_create(taskControlLed,      // function to start as thread
                             "taskControlel_LED1",   // thread name (just for debugging)
                             128,                 // stack size
                             (void *) &el_LED1,      // passed as argument to taskControlLed()
                             1,                   // task priority (higher values mean more process time)
                             NULL                 // can return a handle to created task
                             );
    }
#ifdef TTC_LED2
    if (1) { ttc_task_create(taskControlLed,      // function to start as thread
                             "taskControlel_LED2",   // thread name (just for debugging)
                             128,                 // stack size
                             (void *) el_LED2,       // passed as argument to taskControlLed()
                             1,                   // task priority (higher values mean more process time)
                             NULL                 // can return a handle to created task
                             );
    }
#endif

    t_u8 Counter = 0;
    while (1) {
        while (el_LED1.Brightness > 0) {
            if (!SwitchInterrupt)
                pollSwitch1();
            reloadWatchdog();
            el_LED1.Brightness--;
            parallelOutput(el_LED1.Brightness | (Counter << 8));
            ttc_task_msleep(10);
        }
        while (el_LED1.Brightness < 99) {
            if (!SwitchInterrupt)
                pollSwitch1();
            reloadWatchdog();
            el_LED1.Brightness++;
            parallelOutput(el_LED1.Brightness | (Counter << 8));
            ttc_task_msleep(10);
        }
        Counter++;
    }
}
void taskControlLed(void *TaskArgument) {
    t_configled *LED = (t_configled*) TaskArgument;
    if (LED != NULL) {

        t_ttc_Port* Port = &(LED->Port);

        while (1) {
            if (LED->Brightness >  0) { // switch  on LED
                if (LED->Invert)
                    ttc_gpio_clr_variable(Port);
                else
                    ttc_gpio_set_variable(Port);
                uSleep(LED->Brightness);
            }
            if (LED->Brightness < 99) { // switch off LED
                if (LED->Invert)
                    ttc_gpio_set_variable(Port);
                else
                    ttc_gpio_clr_variable(Port);
                uSleep(99 - LED->Brightness);
            }
            ttc_basic_test_ok(); // used for automatic testing
        }
    }
}
void toggleLED() {
    t_u32 TickCount = ttc_systick_get_elapsed_usecs();
    if (TickCount > NoToggleBefore) {
        // simply toggle brightness
        if (el_LED2->Invert)
            el_LED2->Invert = FALSE;
        else
            el_LED2->Invert = TRUE;
        NoToggleBefore  = TickCount + 1000; // avoid flicker
    }
}
void parallelOutput(t_u16 Value) {

    // Some boards can provide 16-bit wide ports like Olimex H107.
    // Other boards may provide only one or more 8-bit ports.
    // This implementation can handle both situations.
    //
    // See activated board makefile for details of provided parallel ports.
    t_u16 CurrentValue;
    (void) CurrentValue; // variable may be unused: avoid warning

#ifdef USE_BANK16 // 1x 16-bit output
    // write 16-bit value
    ttc_gpio_parallel16_put(PARALLEL16_BANK, Value);

    // read back 16-bit value
    CurrentValue = ttc_gpio_parallel16_get(PARALLEL16_BANK);

    Assert(Value == CurrentValue, ttc_assert_origin_auto); // error in implementation of ttc_gpio_set_XX() and ttc_gpio_get_XX()
#else
  #ifdef USE_BANK8_1 // 1x 8-bit output of low-byte
    // write 8-bit value
    ttc_gpio_parallel08_put_shift3(PARALLEL08_1_BANK, PARALLEL08_1_FIRSTPIN, 0x00ff & Value);

    // read back 8-bit value
    CurrentValue = ttc_gpio_parallel08_get_shift2(PARALLEL08_1_BANK, PARALLEL08_1_FIRSTPIN);

    Assert( (Value & 0x00ff) == CurrentValue, ttc_assert_origin_auto); // error in implementation of ttc_gpio_parallel08_get_shift2() and ttc_gpio_parallel08_put_shift3()
  #endif
  #ifdef USE_BANK8_2 // 1x 8-bit output of high-byte
    // write 8-bit value
    ttc_gpio_parallel08_put_shift3(PARALLEL08_2_BANK, PARALLEL08_2_FIRSTPIN, (0xff00 & Value) >> 8);

    // read back 8-bit value
    CurrentValue = ttc_gpio_parallel08_get_shift2(PARALLEL08_2_BANK, PARALLEL08_2_FIRSTPIN);

    Assert((Value & 0xff00) >> 8 == CurrentValue, ttc_assert_origin_auto); // error in implementation of ttc_gpio_parallel08_get_shift2() and ttc_gpio_parallel08_put_shift3()
  #endif
#endif
}
void pollSwitch1() {
    bool static SwitchValueBefore = FALSE;
    bool SwitchValue = ttc_gpio_get(TTC_SWITCH1);
    if (SwitchValueBefore != SwitchValue) { // edge detected
        SwitchValueBefore = SwitchValue;
        if (SwitchValue) // toggle on rising edge
            toggleLED();
    }
}

#ifdef EXAMPLE_LEDS_USE_INTERRUPTS

void isr_Switch1(t_physical_index PhysicalIndex, void* Argument) {

    // arguments not used
    (void) PhysicalIndex;
    (void) Argument;

    toggleLED();
}

#endif

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
