/** example_ttc_adc.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_adc.
 *  The basic flow is as follows:
 *  (1) example_ttc_adc_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_adc_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 11 at 20141008 13:21:24 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_adc.h"
#include "../ttc-lib/ttc_gpio.h"

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
example_adc_t et_adc_TaskArguments;

#ifdef EXTENSION_ttc_gfx
t_ttc_gfx_config* DisplayConfig = NULL;
#endif

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

#ifdef EXTENSION_ttc_gfx
void printfAt(t_u16 X, t_u16 Y, const char* String, t_s32 Value) {
     char Buffer[20];
    ttc_string_snprintf( (t_u8*) Buffer, 20, String, Value);
    ttc_gfx_text_solid_at(X, Y, Buffer, 20);
}
#endif

void example_ttc_adc_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    //init gfx
    #ifdef EXTENSION_ttc_gfx
    //t_u8 MaxDisplay = ttc_gfx_get_max_display_index();
    //  Assert(MaxDisplay > 0, ttc_assert_origin_auto); // no displays defined by board makefile!
    DisplayConfig = ttc_gfx_get_configuration(1);
    Assert(DisplayConfig, ttc_assert_origin_auto);

    // set color to use for first screen clear
    ttc_gfx_color_bg24(TTC_GFX_COLOR24_AQUA);
    ttc_gfx_init(1);

    ttc_gfx_text_cursor_set(0,0);
    ttc_gfx_text("Potenciometer ", -1);
    #endif

    #ifdef EXTENSION_ttc_usart
    //init USART1
    ttc_usart_init(1);
    // set first logical USART as standard output (otherwise last initialized one would be used)
    ttc_usart_stdout_set(USART1_INDEX);
    // set usart as stdout interface
    ttc_string_register_stdout(ttc_usart_stdout_send_block, ttc_usart_stdout_send_string);
    printf("ADC_example\n\n ");
    #endif


    //Init analog inputs by use of logical indices (connected pin is defined in makefile as TTC_ADC<n>)
#ifdef TTC_ADC1
    ttc_adc_init(1); // initializing ADC for gpio pin defined in TTC_ADC1 (-> compile_options.h)
#  ifdef TTC_ADC1
    ttc_adc_init(2); // initializing ADC for gpio pin defined in TTC_ADC2 (-> compile_options.h)
#  endif
#else
#   warning No ADC channels defined. Define at least TTC_ADC1 as one from e_ttc_gpio_pin !
#endif

    //Init analog inputs by use of their gpio pin (LogicalIndex will be assigned dynamically)
    et_adc_TaskArguments.LogicalIndex_ADC_A8 = ttc_adc_find_configuration_by_pin(E_ttc_gpio_pin_a8)->LogicalIndex;
    ttc_adc_init(et_adc_TaskArguments.LogicalIndex_ADC_A8);

    #ifdef EXTENSION_ttc_gfx
    ttc_gfx_text_cursor_set(0,1);
    ttc_gfx_text("ADC... OK", -1);
    #endif

    #ifdef EXTENSION_ttc_usart
        printf("ADC... OK\n\n");
    #endif


    ttc_task_create( task_ADC,               // function to start as thread
                    "tADC",                  // thread name (just for debugging)
                    128,                     // stack size
                    &et_adc_TaskArguments,   // passed as argument to task_ADC()
                    1,                       // task priority (higher values mean more process time)
                    NULL                     // can return a handle to created task
                    );
}
void task_ADC(void *TaskArgument) {
    Assert(TaskArgument, ttc_assert_origin_auto);                  // always check pointer arguments with Assert()!
    example_adc_t* TaskArguments = (example_adc_t*) TaskArgument; // we're expecting this pointer type

    volatile t_u16 AD_value1, AD_value2, AD_value3;
    (void) AD_value1;
    (void) AD_value2;
    (void) AD_value3;

    while (1) {
#ifdef TTC_ADC1
        AD_value1 = ttc_adc_get_value(1);    // reading from gpio pin defined in TTC_ADC1 (-> compile_options.h)
#  endif
#ifdef TTC_ADC2
        AD_value2 = ttc_adc_get_value(2);    // reading from gpio pin defined in TTC_ADC2 (-> compile_options.h)
#  endif

        #ifdef EXTENSION_ttc_gfx
        printfAt(0, 3, "AD_Value1: %i", AD_value1);
        printfAt(0, 4, "AD_Value2: %i", AD_value2);
        #endif

        #ifdef EXTENSION_ttc_usart
        printf("AD_Value1: %i \n",AD_value1);
        printf("AD_Value2: %i \n",AD_value2);
        #endif

        AD_value2 = ttc_adc_get_value(TaskArguments->LogicalIndex_ADC_A8);    // reading from gpio pin A8
        ttc_task_msleep(100);    // always sleep for a while to give cpu time to other tasks
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
