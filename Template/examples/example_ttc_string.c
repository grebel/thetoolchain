/** example_ttc_string.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple example code to demonstrate
 *  how to use ttc_string.
 *
 *  The basic flow is as follows:
 *  (1) example_ttc_string_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_string_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 13 at 20151120 13:30:25 UTC
 *
 *  Authors: Gregor Rebel
 *
 *  ttc_string tries to be as architecture independent as possible.
 *  At the moment, only ASCII strings are supported. But the driver layout allows
 *  to add more string implementations like UTF8, UTF16, ...
 *
 *  This example shows basic string operations. Two stdout functions are registered
 *  that workd as an output channel for ttc_string_printf().
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_string.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_string.h"
#include "../ttc-lib/ttc_usart.h"
#include "../ttc-lib/ttc_heap.h"     // dynamic memory allocations during runtime
#include "../ttc-lib/ttc_task.h"     // allows to use application in single- and multitasking setup

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_string_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _task_STRING( void* Argument );

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function Definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_string_stdout_block( t_ttc_heap_block* Block ) {
    #ifdef EXTENSION_ttc_usart
    if ( 1 ) // pass memory block to usart
    { ttc_usart_send_block( 1, Block ); } // block will be released for reuse after being sent by ttc_usart automatically
    else { // pass string inside memory block to usart (shows how to managa memory blocks on your own)
        ttc_usart_send_string( 1, ( char* ) Block->Buffer, Block->MaxSize );
        ttc_heap_block_release( Block ); // must release it for reuse to avoid memory leaks
    }
    #endif
}
void example_ttc_string_stdout_string( const t_u8* String, t_base MaxSize ) {
    #ifdef EXTENSION_ttc_usart
    ttc_usart_send_string( 1, ( char* ) String, MaxSize );
    #endif
}
void example_ttc_string_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    example_string_t* Argument = ttc_heap_alloc_zeroed( sizeof( *Argument ) );
    Argument->Source = "Hello World!"; // storing pointer to constant string (stored in ROM/FLASH)
    static char Buffer[100];
    Argument->Dest = Buffer;           // storing pointer to writable memory (stored in RAM)

    #ifdef EXTENSION_ttc_usart
    // initializing first USART in its default configuration
    e_ttc_usart_errorcode Error = ttc_usart_init( 1 ); // note: first bytes might get lost until ttc_usart_register_receive()
    Assert( !Error, ttc_assert_origin_auto ); // error occured during init
    #endif

    // register functions that will provide printf() support
    ttc_string_register_stdout( example_ttc_string_stdout_block, example_ttc_string_stdout_string );

    // sending via registerd example_ttc_string_stdout_string() function
    // ttc_string_printf() uses an internal static buffer to compile the format string and its values
    ttc_string_printf( "The answer is %d!\n", 42 );

    ttc_task_create( _task_STRING,       // function to start as thread
                     "tSTRING",          // thread name (just for debugging)
                     128,                // stack size (adjust to amount of local variables of _task_STRING() AND ALL ITS CALLED FUNTIONS!)
                     Argument,  // passed as argument to _task_STRING()
                     1,                  // task priority (higher values mean more process time)
                     NULL                // can return a handle to created task
                   );
}
void _task_STRING( void* Argument ) {
    Assert( ttc_memory_is_writable( Argument ), ttc_assert_origin_auto );              // always check pointer arguments with Assert()!
    example_string_t* Data = ( example_string_t* ) Argument; // we're expecting this pointer type

    const static char A[] = "abcdef";
    const static char B[] = "abcdff";

    t_base Run = 0;

    do {
        // This loop runs endlessly if scheduler is available.
        // Otherwise it will run only onve and be called periodically from ttc_task_start_scheduler().

        if ( ttc_systick_delay_expired( & Data->Delay ) ) { // delay passed: send hello world (zeroed delay always expires)
            ttc_systick_delay_init( & Data->Delay, 1000 ); // set new delay to expire in 1 milliseconds

            // calculate string length (ttc_string_length8() works up to 255 bytes)
            t_u8 Length_Source = ttc_string_length8( Data->Source, Data->Dest_Size );

            // copy string from Source -> Dest until first 0 byte but not more than Dest_Size
            ttc_string_copy( ( t_u8* ) Data->Dest, ( const t_u8* ) Data->Source, Data->Dest_Size );

            // print copied string + its length to USART (if available)
            ttc_string_printf( "Copied string (%d bytes): '%s'\n", Length_Source, Data->Dest );

            // compare Source and Dest string
            t_base_signed Different = ttc_string_compare( Data->Source, Data->Dest, Data->Dest_Size );
            Assert( !Different, ttc_assert_origin_auto ); // will stop execution if ttc_string_copy() made something wrong

            // this line can only be reach if above Assert() was successfull
            ttc_string_printf( "String copy successfull (%d)\n", Different );

            // prepare a formatted string in a memory buffer (instead of using buffer from ttc_string_printf()
            ttc_string_snprintf( ( t_u8* ) Data->Dest, Data->Dest_Size, "Run #0x%lx  finished\n", ++Run );
            example_ttc_string_stdout_string( ( t_u8* ) Data->Dest, Data->Dest_Size );

            // comparing different strings
            if ( ttc_string_compare( A, B, -1 ) < 0 ) {
                ttc_string_printf( "compare: '%s' < '%s'\n", A, B );
            }
            else {
                ttc_string_printf( "compare: '%s' > '%s'\n", A, B );
            }

            // Check implementation
            Assert( ttc_string_compare( A, A, -1 ) == 0,      ttc_assert_origin_auto ); // compare must return zero for equal strings
            Assert( ttc_string_compare( "A", "B", -1 ) == -1, ttc_assert_origin_auto ); // short string: A < B => compare(A,B) < 0
            Assert( ttc_string_compare( "B", "A",  1 ) ==  1, ttc_assert_origin_auto ); // short string: A > B => compare(A,B) > 0

            ttc_basic_test_ok();
            ttc_task_yield();  // always sleep for a while to give cpu time to other tasks
        }
    }
    while ( TTC_TASK_SCHEDULER_AVAILABLE );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}Function Definitions
