#ifndef EXAMPLE_PWR_H
#define EXAMPLE_PWR_H

/** example_ttc_PWR.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2016.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_pwr.
 *  The basic flow is as follows:
 *  (1) example_ttc_pwr_start() gets called in single tasking mode from ttc_extensions.c:startExtensions() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_pwr_start() gets executed
 *
 *  Created from template example_ttc_device.h revision 11 at 20150130 10:43:12 UTC
 *
 *  Authors: <AUTHOR>
 *
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_pwr.h"
#include "../ttc-lib/ttc_usart.h"


//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_pwr_t
    u8_t Foo;  // example data
    // more arguments...
} example_pwr_t;

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_pwr_start();

/** task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void task_PWR(void *TaskArgument);

/* runs echo server on initialized USART
 */
void task_USART(void *TaskArgument);

/* takes bytes received from USART
 */
ttc_heap_block_t* eu_receiveData(void* Argument, ttc_usart_config_t* USART_Generic, ttc_heap_block_t* Data);


/* release receive buffers to be reused by USART
 */
void releaseRxBuffer_USART1(ttc_heap_block_t* RxBuffer);
void releaseRxBuffer_USART2(ttc_heap_block_t* RxBuffer);

/** called automatically during transmit/ receive activity
  */
void exu_activity_rx(ttc_usart_config_t* USART_Generic, u8_t Byte);
void exu_activity_tx(ttc_usart_config_t* USART_Generic, u8_t Byte);

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_PWR_H
