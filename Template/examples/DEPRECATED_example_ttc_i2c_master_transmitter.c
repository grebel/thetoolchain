/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#include "example_ttc_i2c_master_transmitter.h"
#include "../ttc-lib/ttc_i2c.h"

example_i2c_master_t et_i2c_TaskArguments;
ttc_i2c_errorcode_e Error;
DEPRECATED_ttc_i2c.config_t * I2C_Generic;

#ifdef EXTENSION_500_ttc_gfx
ttc_gfx_config_t* DisplayConfig = NULL;
#endif

#define I2C_INDEX 2

void printfAt(u16_t X, u16_t Y, const char* String, s32_t Value) {

#ifdef EXTENSION_500_ttc_gfx
     char Buffer[20];
    ttc_string_snprintf( (u8_t*) Buffer, 20, String, Value);
    ttc_gfx_print_solid_at(X, Y, Buffer, 20);
#endif

}

void example_ttc_i2c_master_transmitter_start() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */
   et_i2c_TaskArguments.Foo = 1; // set some arguments to be passed to task

#ifdef EXTENSION_500_ttc_gfx
    //u8_t MaxDisplay = ttc_gfx_get_max_display_index();
  //  Assert(MaxDisplay > 0, ec_DeviceNotFound); // no displays defined by board makefile!
    DisplayConfig = ttc_gfx_get_configuration(1);
    Assert(DisplayConfig, ec_DeviceNotFound);

    // set color to use for first screen clear
    ttc_gfx_set_color_bg24(TTC_GFX_COLOR24_AQUA);
    ttc_gfx_init(1);

#endif

   Error = tie_OK;

   I2C_Generic = ttc_i2c_get_configuration(I2C_INDEX);

   /* I2C Configurated as Master */
   I2C_Generic->Flags.Bits.Master = 1;

   /*It has not importance in master mode */
   I2C_Generic->OwnAddress = 0xA0;


#ifdef EXTENSION_100_board_stm32l100C_discovery

   /*Slave address --> Other Board*/
   I2C_Generic->LowLevelConfig->SlaveAddress = 0x30;

   /* Number of bytes to use in DMA */
   I2C_Generic->LowLevelConfig->NumBytes=6;

#endif


   Error = ttc_i2c_init(I2C_INDEX);
   Assert(Error==tie_OK, ec_UNKNOWN);

#ifdef TTC_LED1
   ttc_gpio_init(TTC_LED1, tgm_output_push_pull, tgs_Min);
#endif


   ttc_task_create( task_I2C_Master,        // function to start as thread
                    "tI2C_Master",          // thread name (just for debugging)
                    128,                    // stack size
                    &et_i2c_TaskArguments,  // passed as argument to task_SPI()
                    1,                      // task priority (higher values mean more process time)
                    NULL                    // can return a handle to created task
                  );
}



void task_I2C_Master(void *TaskArgument){


    Assert(TaskArgument, ec_NULL);                  // always check pointer arguments with Assert()!
    example_i2c_master_t* TaskArguments = (example_i2c_master_t*) TaskArgument; // we're expecting this pointer type
    (void) TaskArguments;

#ifdef TTC_LED1
        ttc_gpio_clr(TTC_LED1);
#endif

#ifdef EXTENSION_450_accelerometer_mpu6050

    // Reset MPU6050
    Error = ttc_i2c_write_register(I2C_INDEX, MPU6050_DEFAULT_ADDRESS, 0x6B, tiras_MSB_First_8Bit,0x80);

    u8_t StatusRegister = 0;
    ttc_i2c_read_register(I2C_INDEX, MPU6050_DEFAULT_ADDRESS, MPU6050_RA_WHO_AM_I, tiras_MSB_First_8Bit, &StatusRegister);
    Assert(StatusRegister == 104, ec_DeviceNotFound);


#ifdef EXTENSION_500_ttc_gfx

    ttc_gfx_print_solid_at(0, 0, "MPU6050 OK", -1);
    ttc_gfx_print_solid_at( 0, 2, "X:       mG", -1);
    ttc_gfx_print_solid_at( 0, 3, "Y:       mG", -1);
    ttc_gfx_print_solid_at( 0, 4, "Z:       mG", -1);

#endif


    do{

        //Wake up MPU6050
        Error = ttc_i2c_write_register(I2C_INDEX,MPU6050_DEFAULT_ADDRESS, 0x6B, tiras_MSB_First_8Bit,0);
        ttc_i2c_read_register(I2C_INDEX, MPU6050_DEFAULT_ADDRESS, 0x6B, tiras_MSB_First_8Bit, &StatusRegister);

    }while(StatusRegister!=0);


    s16_t AccelerationX;
    s16_t AccelerationY;
    s16_t AccelerationZ;

    u8_t AccelerationX_H;
    u8_t AccelerationX_L;
    u8_t AccelerationY_H;
    u8_t AccelerationY_L;
    u8_t AccelerationZ_H;
    u8_t AccelerationZ_L;


    while (1) {

        #ifdef TTC_LED1
                ttc_gpio_clr(TTC_LED1);
        #endif

        /* Read Acceleration in position X*/
        ttc_i2c_read_register(I2C_INDEX, MPU6050_DEFAULT_ADDRESS, MPU6050_RA_ACCEL_XOUT_L, tiras_MSB_First_8Bit, (u8_t*) &AccelerationX );
        ttc_task_msleep(50);

        ttc_i2c_read_register(I2C_INDEX, MPU6050_DEFAULT_ADDRESS, MPU6050_RA_ACCEL_XOUT_H, tiras_MSB_First_8Bit, ( (u8_t*) &AccelerationX ) + 1);
        ttc_task_msleep(50);

        /* Read Acceleration in position Y*/
        ttc_i2c_read_register(I2C_INDEX, MPU6050_DEFAULT_ADDRESS, MPU6050_RA_ACCEL_YOUT_L, tiras_MSB_First_8Bit, (u8_t*) &AccelerationY );
        ttc_task_msleep(50);

        ttc_i2c_read_register(I2C_INDEX, MPU6050_DEFAULT_ADDRESS, MPU6050_RA_ACCEL_YOUT_H, tiras_MSB_First_8Bit, ( (u8_t*) &AccelerationY ) + 1);
        ttc_task_msleep(50);

        /* Read Acceleration in position Z*/
        ttc_i2c_read_register(I2C_INDEX, MPU6050_DEFAULT_ADDRESS, MPU6050_RA_ACCEL_ZOUT_L, tiras_MSB_First_8Bit, (u8_t*) &AccelerationZ );
        ttc_task_msleep(50);

        ttc_i2c_read_register(I2C_INDEX, MPU6050_DEFAULT_ADDRESS, MPU6050_RA_ACCEL_ZOUT_H, tiras_MSB_First_8Bit, ( (u8_t*) &AccelerationZ ) + 1);
        ttc_task_msleep(50);

        #  ifdef EXTENSION_500_ttc_gfx
            printfAt(3, 2, "%6i mg  ", AccelerationX);
            printfAt(3, 3, "%6i mg  ", AccelerationY);
            printfAt(3, 4, "%6i mg  ", AccelerationZ);
        #  endif

        #ifdef TTC_LED1
                ttc_gpio_set(TTC_LED1);
        #endif


#ifdef EXTENSION_100_board_stm32l100C_discovery


        AccelerationX_H = *(((u8_t*) &AccelerationX ) + 1);
        AccelerationX_L = *(((u8_t*) &AccelerationX ));
        AccelerationY_H = *(((u8_t*) &AccelerationY ) + 1);
        AccelerationY_L = *(((u8_t*) &AccelerationY ));
        AccelerationZ_H = *(((u8_t*) &AccelerationZ ) + 1);
        AccelerationZ_L = *(((u8_t*) &AccelerationZ ));


        // Now, we are going to send the acceleration from mpu6050 to slave board
        /* Send Information to Slave Board */

        _i2c_stm32l1xx_send_byte(I2C_Generic,AccelerationX_H);
        _i2c_stm32l1xx_send_byte(I2C_Generic,AccelerationX_L);
        _i2c_stm32l1xx_send_byte(I2C_Generic,AccelerationY_H);
        _i2c_stm32l1xx_send_byte(I2C_Generic,AccelerationY_L);
        _i2c_stm32l1xx_send_byte(I2C_Generic,AccelerationZ_H);
        _i2c_stm32l1xx_send_byte(I2C_Generic,AccelerationZ_L);


#endif

         ttc_task_msleep(1000);
    }

#endif





    while(1);
    return 0;
}


