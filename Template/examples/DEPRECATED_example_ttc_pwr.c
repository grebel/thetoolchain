/** example_ttc_pwr.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2016.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_pwr.
 *  The basic flow is as follows:
 *  (1) example_ttc_pwr_start() gets called in single tasking mode from ttc_extensions.c:startExtensions() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_pwr_start() gets executed
 *
 *  Created from template example_ttc_device.c revision 11 at 20150130 10:43:12 UTC
 *
 *  Authors: <AUTHOR>
 *
}*/

#include "example_ttc_pwr.h"
#include "../ttc-lib/ttc_pwr.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_register.h"
#include "stm32l1xx_exti.h"
#include "stm32l1xx_gpio.h"
#include "stm32l1xx_syscfg.h"
#include "stm32l1xx_rtc.h"
#include "../ttc-lib/ttc_rtc.h"
//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
example_pwr_t et_pwr_TaskArguments;
RTC_TimeTypeDef   RTC_TimeStructure;
RTC_InitTypeDef   RTC_InitStructure;

#define RTC_INDEX1 1
#define PWR_INDEX1 1

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_pwr_start() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */
   et_pwr_TaskArguments.Foo = 1; // set some arguments to be passed to task

   ttc_task_create( task_PWR,               // function to start as thread
                    "tPWR",                 // thread name (just for debugging)
                    256,                         // stack size
                    &et_pwr_TaskArguments,  // passed as argument to task_PWR()
                    1,                           // task priority (higher values mean more process time)
                    NULL                         // can return a handle to created task
                  );
}
void task_PWR(void *TaskArgument) {
    Assert(TaskArgument, ec_NULL);                  // always check pointer arguments with Assert()!
    example_pwr_t* TaskArguments = (example_pwr_t*) TaskArgument; // we're expecting this pointer type


    sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
    // TTC_LED1 is first LED configured by current board
    ttc_gpio_init(TTC_LED1, tgm_output_push_pull, tgs_Max);
    ttc_gpio_set(TTC_LED1);


    /* REAL TIME CLOCK CONFIGURATION ***************************************/
    ttc_rtc_config_t * Config_Rtc = ttc_rtc_get_configuration(RTC_INDEX1);

    Config_Rtc->Flags.Bits.Alarm_A = 0b1;
    Config_Rtc->Flags.Bits.Alarm_B = 0b0;
    Config_Rtc->Flags.Bits.WakeUp = 0b0;
    Config_Rtc->Flags.Bits.Tamper = 0b0;
    Config_Rtc->Flags.Bits.Timestap = 0b0;
    Config_Rtc->Flags.Bits.Pin_line = 0b0;

    ttc_rtc_init(RTC_INDEX1);
    /*************************************************************************/


//    sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
//    ttc_gpio_init(tgp_a8,tgm_alternate_function_push_pull,tgs_40MHz);
//    sysclock_stm32l1xx_RCC_MCOConfig(ttc_sysclock_RCC_MCOSource_LSE,ttc_sysclock_RCC_MCODiv_1); // Put on MCO pin the: System clock selected


    /* POWER MODE CONFIGURATION ***************************************/
    ttc_pwr_config_t * Config_pwr = ttc_pwr_get_configuration(PWR_INDEX1);

    /* What kind of mode you want to use */
    Config_pwr->LowLevelConfig->Mode = stop_mode;

    /* Before init the pwr mode, you will have to configure the rtc interruption */
    ttc_pwr_init(PWR_INDEX1);
    /*************************************************************************/


//    sysclock_stm32l1xx_RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
//    ttc_gpio_init(tgp_a8,tgm_alternate_function_push_pull,tgs_40MHz);
//    sysclock_stm32l1xx_RCC_MCOConfig(ttc_sysclock_RCC_MCOSource_SYSCLK,ttc_sysclock_RCC_MCODiv_1); // Put on MCO pin the: System clock selected


    u8_t mode = Config_pwr->LowLevelConfig->Mode;

    if(mode == sleep_mode){

        /*
         * Any peripheral interrupt acknowledged by the nested vectored interrupt controller (NVIC) can wake up the device from Sleep mode.
         *
         */


    }else if(mode == low_power_run_mode){

        /*
         * The regulator is forced in Main regulator mode sing the PWR_EnterLowPowerRunMode() function.
         * Increase the system frequency if needed.
         *
         */

        pwr_stm32l1xx_stop_low_power_run_mode();
        ttc_sysclock_profile_switch(tsp_MaxSpeed);


    }else if(mode == low_power_sleep_mode){

        /* Any peripheral interrupt acknowledged by the nested vectored interrupt
             controller (NVIC) can wake up the device from Sleep LP mode. */



    }else if(mode == stop_mode){

        ttc_sysclock_profile_switch(tsp_MaxSpeed);


    }else if(mode == standby_mode){

        /*
         * WKUP pin rising edge, RTC alarm (Alarm A and Alarm B), RTC wakeup,
         * tamper event, time-stamp event, external reset in NRST pin, IWDG reset.
         */

    }

    u8_t count = 0;

    while (1) {

        if(count){

            ttc_gpio_clr((TTC_LED1));

        }else{

            ttc_gpio_set(TTC_LED1);
        }

        ttc_task_msleep(500);
        count = (count+1)%2;

    }

}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
