/*{ example_usb_host_hid.c ******************************************************

  Basic test of radio transceivers without any protocol.

  Initializes first radio and periodically transmits data.
  Received data is echoed via TTC_USART1.

}*/

#include "example_usb_host_hid.h"

//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_usb_host_hid_start() {
    example_usb_host_hid_init();

    //start your Tasks here

}
void example_usb_host_hid_init() {

    ttc_usb_errorcode_e Error = ttc_usb_init(1);
    Assert_USB(Error == tusbe_OK, ec_InvalidConfiguration);
 //   ttc_usb_register_rx_function(1, USB_Handler_Data_Receive);

}

///*******************************************************************************
//* Function Name  : USB_Handler_Data_Send.
//* Description    : send data out to USB Bus.
//* Input          : None.
//* Return         : none.
//*******************************************************************************/
//u8_t debugLED_TTC_TX_value;
//void USB_Handler_Data_Send(u8_t* data_buffer, u32_t Nb_bytes)
//{

//#ifdef USB_DEBUG_LED
//    //ToDo: replace by TTC_UART
//    debugLED_TTC_TX_value = 1- debugLED_TTC_TX_value;
//    ttc_gpio_setn(TTC_TX, debugLED_TTC_TX_value);
//#endif

//    ttc_usb_send_raw(1, data_buffer, Nb_bytes);
//}

///*******************************************************************************
//* Function Name  : USB_Handler_Data_Receive.
//* Description    : receive data from USB Bus.
//* Input          : data_buffer: data address.
//                   Nb_bytes: number of bytes to receive.
//* Return         : none.
//*******************************************************************************/
//u8_t debugLED_TTC_RX_value;
//void USB_Handler_Data_Receive(u8_t* data_buffer, u8_t Nb_bytes)
//{

//    uint32_t i;
//#ifdef USB_DEBUG_LED
//    //ToDo: replace by TTC_UART
//    debugLED_TTC_RX_value = 1- debugLED_TTC_RX_value;
//    ttc_gpio_setn(TTC_RX, debugLED_TTC_RX_value);
//#endif

//#ifdef USB_Handler_Data_echo
//    USB_Handler_Data_Send(data_buffer, Nb_bytes);
//#endif


//}


//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions

