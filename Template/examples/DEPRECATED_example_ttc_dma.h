#ifndef EXAMPLE_DMA_H
#define EXAMPLE_DMA_H

/** example_ttc_DMA.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2016.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_dma.
 *  The basic flow is as follows:
 *  (1) example_ttc_dma_start() gets called in single tasking mode from ttc_extensions.c:startExtensions() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_dma_start() gets executed
 *
 *  Created from template example_ttc_device.h revision 11 at 20150108 10:24:10 UTC
 *
 *  Authors: <AUTHOR>
 *  
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_dma.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_dma_t
    u8_t Foo;  // example data
    // more arguments...
} example_dma_t;

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *
 * - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_dma_start();

/** task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void task_DMA(void *TaskArgument);


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_DMA_H
