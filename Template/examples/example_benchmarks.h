#ifndef EXAMPLE_BENCHMARKS
#define EXAMPLE_BENCHMARKS

/*{ ExampleBenchmarks::.h ************************************************
 
 Empty template for new header files.
 Copy and adapt to your needs.
 
}*/
//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gpio.h"

#ifdef EXTENSION_ttc_task
#  include "ttc_task.h"
#endif

//}Includes
//{ Defines/ TypeDefs ****************************************************
//}Defines
//{ Structures/ Enums ****************************************************
//}Structures/ Enums
//{ Function declarations **************************************************

// main entry: initializes hardware + spawns all threads
// returns immediately
void example_benchmarks_prepare();

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_BENCHMARKS
