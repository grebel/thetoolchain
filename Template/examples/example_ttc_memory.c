/** example_ttc_memory.c **********************************************{
 *
 *                             The ToolChain
 *
 *
 * example_ttc_memory_prepare()
 *     fills et_memory_TaskArguments with data
 *     spawns thread task_MEMORY()
 *     returns immediately
 *
 * task_MEMORY()
 *     while (1) {
 *       do something
 *       ttc_task_msleep(100);
 *     }
 *
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_memory.h"

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
example_memory_t et_memory_TaskArguments;

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_memory_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */
   et_memory_TaskArguments.Foo = 1; // set some arguments to be passed to task

   ttc_task_create( task_MEMORY,               // function to start as thread
                    "tMEMORY",                 // thread name (just for debugging)
                    128,                         // stack size
                    &et_memory_TaskArguments,  // passed as argument to task_MEMORY()
                    1,                           // task priority (higher values mean more process time)
                    NULL                         // can return a handle to created task
                  );
}
void task_MEMORY(void *TaskArgument) {
    Assert(TaskArgument, ttc_assert_origin_auto);                  // always check pointer arguments with Assert()!
    example_memory_t* TaskArguments = (example_memory_t*) TaskArgument; // we're expecting this pointer type

    while (1) {
      // do something...
      ttc_task_msleep(100);    // always sleep for a while to give cpu time to other tasks
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
