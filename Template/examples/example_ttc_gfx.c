/** example_ttc_gfx.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for ili93xx 320x240x24 color tft driver
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_gfx_ili93xx_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_gfx_ili93xx_prepare() get executed
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_gfx_ili93xx_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
  *
 *  Created from template example_ttc_device.c revision 16 at 20160817 15:25:58 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_gfx.h"
#include "../ttc-lib/ttc_gfx.h"
#include "../ttc-lib/ttc_heap.h"     // dynamic memory allocations during runtime
#include "../ttc-lib/ttc_task.h"     // allows to use application in single- and multitasking setup

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_gfx_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _example_gfx_task( void* Argument );

//}Private Function Declarations
//{ Function Definitions *************************************************

void example_ttc_gfx_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    // Obtaining memory for task argument from dynamic memory.
    // If example_ttc_gfx_prepare() is called multiple times, each run will
    // use its own memory block for *Argument.
    example_gfx_data_t* Argument = ttc_heap_alloc_zeroed( sizeof( example_gfx_data_t ) );

    Argument->Index_GFX   = 1; // set logical index of device to use

    // get configuration of gfx device
    t_ttc_gfx_config* Config_GFX = ttc_gfx_get_configuration( Argument->Index_GFX );
    // we may read/ change configuration prior to initialization
    (void) Config_GFX;

    // initializing our gfx device here makes debugging easier (still single-tasking)
    ttc_gfx_init( Argument->Index_GFX );

    ttc_gfx_text_at( 0, 0, "example_ttc_gfx", -1 );
    ttc_gfx_text_at( 0, 2, "Seconds: ", -1 );

    ttc_task_create( _example_gfx_task,    // function to start as thread
                     "tGFX",               // thread name (just for debugging)
                     128,                  // stack size (adjust to amount of local variables of _example_gfx_task() AND ALL ITS CALLED FUNTIONS!)
                     Argument,          // passed as argument to _example_gfx_task()
                     1,                    // task priority (higher values mean more process time)
                     NULL                  // can return a handle to created task
                   );
}
void _example_gfx_task( void* Argument ) {
    Assert_BASIC_Writable( Argument , ttc_assert_origin_auto);              // always check pointer arguments with Assert() before dereferencing them!
    example_gfx_data_t* Data = ( example_gfx_data_t* ) Argument; // we're expecting this pointer type

    do {
        // This loop runs endlessly if scheduler is available.
        // Otherwise _regression_gfx_ili93xx_task()
        // will be called periodically from ttc_task_start_scheduler().

        if ( ttc_systick_delay_expired( & Data->Delay ) ) { // delay passed: send hello world (zeroed delay always expires)
            ttc_systick_delay_init( & Data->Delay, 1000000 ); // set new delay to expire in 1 second = 1 million microseconds

            ttc_string_snprintf( Data->Buffer, egd_BUFFER_SIZE, "%3i", Data->Seconds++ );
            ttc_gfx_text_solid_at( 9, 2, ( char* ) Data->Buffer, egd_BUFFER_SIZE );

            ttc_task_yield(); // if scheduler available, give cpu to other processes
        }
    }
    while ( TTC_TASK_SCHEDULER_AVAILABLE );
}

//}Function Definitions
