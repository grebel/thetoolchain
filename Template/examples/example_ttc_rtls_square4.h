#ifndef EXAMPLE_RTLS_CRTOF_SIMPLE_2D_H
#define EXAMPLE_RTLS_CRTOF_SIMPLE_2D_H

/** example_ttc_RTLS_CRTOF_SIMPLE_2D.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_rtls_crtof_simple_2d.
 *  The basic flow is as follows:
 *  (1) example_ttc_rtls_crtof_simple_2d_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_rtls_crtof_simple_2d_prepare() gets executed
 *
 *  Created from template example_ttc_device_architecture.h revision 15 at 20161208 10:46:38 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_RTLS_CRTOF_SIMPLE_2D.c only do not belong here!
#include "../ttc-lib/ttc_basic.h"                         // basic datatypes and definitions
#include "../ttc-lib/ttc_systick.h"                       // exact delays and timeouts
#include "../ttc-lib/ttc_task_types.h"                    // required for task delays
#include "../ttc-lib/ttc_rtls_types.h"               // datatypes of rtls devices
#include "../ttc-lib/rtls/rtls_square4_types.h" // datatypes of crtof_simple_2d rtls devices
#include "example_ttc_rtls_square4_types.h"          // datatypes used in this example only
#include "example_ttc_radio_ranging_common.h"             // using datatypes from ranging example
//}Includes
//{ Defines/ TypeDefs ****************************************************

#ifndef EXAMPLE_TTC_RADIO_NODE_FIXED_ANCHORS
    #define EXAMPLE_TTC_RADIO_NODE_FIXED_ANCHORS 0  // ==1: assign fixed 3D-coordinates to anchor nodes; ==0; use ttc_rtls for dynamic anchor node coordinate system build up
#endif
//}Defines
//{ Structures/ Enums ****************************************************

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_rtls_square4_prepare();


//}Function prototypes

#endif //EXAMPLE_RTLS_CRTOF_SIMPLE_2D_H
