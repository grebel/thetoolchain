#ifndef EXAMPLE_SPI_H
#define EXAMPLE_SPI_H

/*{ example_spi.h ******************************************************
 
                       The ToolChain
                       
 Example code demonstrating how to use 
 Universal Synchronous Asynchronous Receiver Transmitter (SPI)
 
 written by Gregor Rebel 2012
 
}*/
//{ Defines/ TypeDefs ****************************************************


//}Defines
//{ Includes *************************************************************

 // Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_spi.h"
#include "../ttc-lib/ttc_gpio.h"

#define USE_STDPERIPH_DRIVER
#include "stm32f10x.h"
#include "core_cm3.h"        // StdPeripheralsLibrary/CMSIS/CM3/CoreSupport/
#include "stm32f10x_conf.h"
#include "bits.h"

#include <string.h>
#include "FreeRTOS.h"
#include "task.h"

//}Includes
//{ Structures/ Enums ****************************************************

//}Structures/ Enums
//{ Global Variables *****************************************************



//}Global Variables
//{ Function declarations **************************************************

/* Call before example_spi_start() to initialize hardware.
 */
void example_spi_init();

/* initializes + starts SPI example
 */
void example_spi_start();

/* Sends "Hello World" to initialized SPI
 */
void task_SPI_sender(void *TaskArgument);

/* Reads from initialized SPI and sends answer
 */
void task_SPI_listener(void *TaskArgument);

/* processes received data */
void processData(u8_t* Data, u8_t MaxLength);

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_SPI_H
