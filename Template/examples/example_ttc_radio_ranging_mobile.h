#ifndef example_ttc_radio_ranging_mobile_h
#define example_ttc_radio_ranging_mobile_h

/** { example_ttc_radio_ranging_mobile.h *********************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.h revision 11 at 20161130 06:10:10 UTC
 *
 *  Authors: gregor
}*/
/** { Includes *********************************************************
 *
 * Put all includes here that are required to include this header file.
 * Do NOT add includes required to compile example_ttc_radio_ranging_mobile.c here!
 */

#include "../ttc-lib/ttc_basic.h"             // basic datatypes
#include "../ttc-lib/ttc_memory.h"            // memory checks and safe pointers
#include "example_ttc_radio_ranging_common.h" // functions and structures being common for all node types
// #include "../ttc-lib/ttc_heap.h"   // dynamic memory and safe arrays
// #include "../ttc-lib/ttc_string.h" // string compare and copy
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Constant Defines / Macros ****************************************
 *
 * As a convention, constant definitions are all UPPERCASED to distinguish
 * them from variables and functions.
 *
 * Examples:

 #define ABC 1
 #define calculate(A,B) (A+B)
 */

//}ConstantDefines
/** { Structure Declarations *******************************************
 *
 * This can be typedef struct or typedef enum declarations.
 * Always prefix structures and enums with filename to avoid double declarations!
 *
 * Examples:

   typedef struct s_example_ttc_radio_ranging_mobile_list {
      struct s_example_ttc_radio_ranging_mobile_list* Next;
      t_base Value;
   } example_ttc_radio_ranging_mobile_list_t;

   typedef enum {
     example_ttc_radio_ranging_mobile_None,
     example_ttc_radio_ranging_mobile_Type1
   } example_ttc_radio_ranging_mobile_types_e;

 */
typedef struct {
    t_u32 LowerDistanceCM;
    t_u32 UpperDistanceCM;
    t_u32 MedianDistanceCM;  // = (LowerDistanceCM + MedianDistanceCM) / 2
    t_u32 AvgDistanceCM;     // Cummulative Average of all distance measures
} etrr_measure_statistics_t;

//}StructureDeclarations
/** { Function Declarations ********************************************
 *
 * Declare all functions here that may be called from outside.
 * Rules of Good Code:
 * - document every function declaration
 * - prefix public function names with their filename to avoid double definitions
 */

/** do some extra initialization for this mobile node
 *
 * @param TaskInfo  pointer to same structure being handed to task function later
 */
void example_ttc_radio_ranging_prepare_mobile( etr_task_info_t* TaskInfo );

/** Task implementing mobile node with a ranging enabled radio transceiver
 *
 * A mobile node can move freely and use the nearby anchor nodes to calculate it's current location.
 *
 * @param TaskArgument (void*)  pointer to a etr_task_info_t instance
 */
void example_ttc_radio_ranging_task_mobile( void* TaskArgument );


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //example_ttc_radio_ranging_mobile_h
