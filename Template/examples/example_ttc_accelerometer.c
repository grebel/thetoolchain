/** example_ttc_accelerometer.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_accelerometer.
 *  The basic flow is as follows:
 *  (1) example_ttc_accelerometer_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_accelerometer_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 11 at 20141020 08:58:47 UTC
 *
 *  Authors: Adrián Romero, Gregor Rebel
 *
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_accelerometer.h"
#include "../ttc-lib/ttc_task.h"

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
example_accelerometer_t et_accelerometer_TaskArguments;

#ifdef EXTENSION_ttc_gfx
#include "../ttc-lib/ttc_gfx.h"
t_ttc_gfx_config* DisplayConfig = NULL;
#endif

/* If you want to use the mpu6050 you also have to use index 2 for i2c connect directly to the microcontroller */
#define I2C_INDEX 2
// using first I2C available on current board


//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************


//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void printfAt( t_u16 X, t_u16 Y, const char* String, t_s32 Value ) {

    #ifdef EXTENSION_ttc_gfx
    char Buffer[20];
    ttc_string_snprintf( ( t_u8* ) Buffer, 20, String, Value );
    ttc_gfx_text_solid_at( X, Y, Buffer, 20 );
    #endif

}
void example_ttc_accelerometer_init() {

    #ifdef EXTENSION_ttc_gfx
    //t_u8 MaxDisplay = ttc_gfx_get_max_display_index();
    //  Assert(MaxDisplay > 0, ttc_assert_origin_auto); // no displays defined by board makefile!
    DisplayConfig = ttc_gfx_get_configuration( 1 );
    Assert( DisplayConfig, ttc_assert_origin_auto );

    // set color to use for first screen clear
    ttc_gfx_color_bg24( TTC_GFX_COLOR24_AQUA );
    ttc_gfx_init( 1 );

    #endif

    e_ttc_i2c_errorcode Error = 0;

    Assert( !Error, ttc_assert_origin_auto );

    t_ttc_i2c_config* I2C = ttc_i2c_get_configuration( I2C_INDEX );

    I2C->Init.Flags.Bits.Master = 1;

    #ifdef EXTENSION_100_board_stm32l100C_discovery

    /*It is not important in master mode */
    I2C->OwnAddress = 0xA0;

    /* Number of bytes to use in DMA */
    I2C->LowLevelConfig->NumBytes = 6;

    #endif


    //I2C.Flags.Bits.
    Error = ttc_i2c_init( I2C_INDEX );
    Assert( !Error, ttc_assert_origin_auto );


}
void task_acc() {


    t_ttc_accelerometer_config* Acc_Config = ttc_accelerometer_get_configuration( 1 );

    Acc_Config->LogicalIndex = 1;
    Acc_Config->LogicalIndex_Interface = I2C_INDEX;

    e_ttc_accelerometer_errorcode Error_acc;
    Error_acc = ttc_accelerometer_init( 1 );
    Assert( Error_acc == ec_accelerometer_OK, ttc_assert_origin_auto );

    #  ifdef EXTENSION_ttc_gfx
    if ( !Error_acc ) {

        if ( Acc_Config->Architecture == ta_accelerometer_lis3lv02dl ) {
            ttc_gfx_text_solid_at( 0, 0, "LIS3LV02DL OK", -1 );
        }
        else {
            ttc_gfx_text_solid_at( 0, 0, "MPU6050 OK", -1 );
        }
    }
    ttc_gfx_text_solid_at( 0, 2, "X:       mG", -1 );
    ttc_gfx_text_solid_at( 0, 3, "Y:       mG", -1 );
    ttc_gfx_text_solid_at( 0, 4, "Z:       mG", -1 );
    #  endif


    t_ttc_accelerometer_measures* Measures; ( void ) Measures;

    while ( 1 ) {

        Measures = ttc_accelerometer_read_measures( 1 );

        #  ifdef EXTENSION_ttc_gfx
        printfAt( 3, 2, "%6i mg  ", Measures->AccelerationX );
        printfAt( 3, 3, "%6i mg  ", Measures->AccelerationY );
        printfAt( 3, 4, "%6i mg  ", Measures->AccelerationZ );
        #  endif

        ttc_task_msleep( 100 );  // always sleep for a while to give cpu time to other tasks

    }

}
void example_ttc_accelerometer_prepare() {
    example_ttc_accelerometer_init();



    ttc_task_create( task_acc,      // function to start as thread
                     "LCD",              // thread name (just for debugging)
                     250,                // stack size
                     ( void* ) NULL,     // passed as argument to taskLEDs()
                     1,                  // task priority (higher values mean more process time)
                     NULL                // can return a handle to created task
                   );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}FunctionDefinitions
