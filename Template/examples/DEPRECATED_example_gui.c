/*{ Template_FreeRTOS::.c ************************************************

  example_gfx_printf_start() spawns thread taskBrightness()

  taskBrightness()
      Defines brightness of led as Ton / (Ton + Toff)
      spawns thread taskLEDs()
      while (1) {
        while (Ton < 100) { Ton++; Toff--; }
        while (Ton >   0) { Ton--; Toff++; }
      }

  taskLEDs()
      while (1) {
        switch on LED
        wait for Ton
        switch off LED
        wait for Toff
      }

}*/

#include "example_gui.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_heap.h"
#include "../ttc-lib/ttc_string.h"


// Note: every global or static variable needs a unique prefix to identify it during debugging!
ttc_gfx_config_t* eg_DisplayConfig = NULL;
u16_t eg_Value=50;
u16_t eg_Feed=0;
u16_t eg_Radius=10;
u16_t eg_X;
u16_t eg_Y;

void example_gui_start() {
    eg_DisplayConfig = ttc_gfx_get_configuration(1);
    Assert(eg_DisplayConfig, ec_DeviceNotFound);

    // set color to use for first screen clear
    ttc_gfx_set_color_bg24(TTC_GFX_COLOR24_WHITE);
    ttc_gfx_set_color_fg24(TTC_GFX_COLOR24_RED);
    ttc_gfx_init(1); //initialisation of the lcd
    ttc_input_start(1); //starting input-area-check,
    ttc_gfx_stdout_set(1);

    // always declare single buffers static to reduce stack usage and give compiler a clue about memory usage
    static A_define(u8_t, egt_String, 14);

    // copy string into buffer
    ttc_string_copy(Aptr(egt_String), (u8_t*) "  GUI Example", egt_String.Size);

    ttc_task_create( task_gui,             // function to start as thread
                     "GUI",                // thread name (just for debugging)
                     128,                  // stack size
                     (void *) &egt_String, // passed as argument to task_gui()
                     1,                    // task priority (higher values mean more process time)
                     NULL                  // can return a handle to created task
                   );
}
void task_gui(void *TaskArgument) {
    A_dynamic_assign(u8_t, StringArray, TaskArgument);

    ttc_gfx_print_solid_at(0,0,(char*) StringArray.Data, StringArray.Size);
    ttc_gui_create_input_line(1,2,8,"Wert 1:",&eg_Value,0,4);
    ttc_gui_create_input_line(1,4,8,"Wert 2:",&eg_Radius,0,4);

    while (1) {
        function_read_position(&eg_X,&eg_Y);
        if (eg_Y>120)
            ttc_gfx_set_pixel(eg_X,eg_Y);
        ttc_task_msleep(2);
    }
    return;
}
void button_event_clear(void *TaskArgument) {
    ttc_gfx_set_color_bg24(TTC_GFX_COLOR24_BLACK);
    ttc_gui_clear();
    ttc_gfx_set_color_bg24(TTC_GFX_COLOR24_WHITE);
    ttc_gui_clear();
    ttc_gfx_text_cursor_set(0,0);
    ttc_gfx_print_boxed("Vorschub",14,0);
    ttc_gfx_text_cursor_set(4,1);
    ttc_gfx_print(" mm",6);
    ttc_gui_plus_button(190,0,239,50,&eg_Feed);
    ttc_gui_minus_button(139,0,190,50,&eg_Feed);
    ttc_gfx_text_cursor_set(0,2);
    ttc_gfx_print_boxed("Radius",14,0);
    ttc_gfx_text_cursor_set(3,3);
    ttc_gfx_print(" grad",5);
    ttc_gui_plus_button(190,50,239,100, &eg_Radius);
    ttc_gui_minus_button(139,50,190,100, &eg_Radius);
    ttc_gui_create_push_button(button_event_clear,NULL,9,260,240,320,"CLEAR");
}
