/** example_ttc_states.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for universal and debuggable statemachine for single- and multitasking environments
 *
 *  Basic flow of booting examples:
 *  (1) ttc_extensions.c:ttc_extensions_start() calls example_ttc_states_prepare() to
 *      - initialize all required devices
 *      - initializes its global and local static variables
 *      - create all required task or statemachine instances
 *      - return immediately
 *
 *  Multitasking is available:
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_states_prepare() get executed.
 *      Each task uses its private stack as given to ttc_task_create() before.
 *
 *  Multitasking is NOT available:
 *  (2) Minimalistic main loop is started
 *  (3) Main loop calls all registered statemachine functions in an endless loop.
 *      All statemachine functions use the same global stack that is maximum of all
 *      stack sizes given to ttc_task_create() before.
 *
 *  Created from template example_ttc_device.c revision 19 at 20180126 14:37:55 UTC
 *
 *  Authors: Gregor Rebel
 *
 *
 *  Basic Statemachine Usage Example
 *
 *  This example shows the implementation of a fictive but common statemachine.
 *  The statemachine starts in state_number_Init. State transisitions take place automatically
 *  after random timeouts. State state_number_CheckIO decides randomly to which state to switch to.
 *  Every state, except reset and display, calls the display state as a sub-state once when it is
 *  first entered and before switching to next state.
 *  The display state acts as an example how to implement sub-states that correspond to sub-routines
 *  (aka functions). A sub-state can be used from any other state and will return to this state
 *  automatically.
 *
 *  Possible State Transitions:
 *    state_number_Reset     -> state_number_Display, state_number_Init
 *    state_number_Init      -> state_number_Display, state_number_CheckIO
 *    state_number_CheckIO   -> state_number_Display, state_number_ProcessIO, state_number_Compute
 *    state_number_Compute   -> state_number_Display, state_number_CheckIO
 *    state_number_ProcessIO -> state_number_Display, state_number_CheckIO
 *    state_number_Display   -> state_number_Init, state_number_CheckIO, state_number_ProcessIO
 *
 *  All states and their valid transitions are declared constant to reduce RAM usage
 *  and to protect from memory corruptions.
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_states.h"
#include "../ttc-lib/ttc_states.h"    // high-level driver of device to use
#include "../ttc-lib/ttc_heap.h"      // dynamic memory allocations during runtime
#include "../ttc-lib/ttc_task.h"      // allows to use application in single- and multitasking setup
#include "../ttc-lib/ttc_random.h"    // pseudo random number generation
#include "../ttc-lib/ttc_string.h"    // string length calculation
#include "../ttc-lib/ttc_gfx_types.h" // datatypes for graphical output (e.g. colors)
#ifdef EXTENSION_ttc_gfx
#include "../ttc-lib/ttc_gfx.h"       // ttc_gfx is optional for this example and only activated if current board has a display
#endif

//}Includes
//{ Global Variables *****************************************************

#ifdef EXTENSION_ttc_gfx
const t_u32 Color_Active   = TTC_GFX_COLOR24_BLUE;  // display color for active state
const t_u32 Color_Inactive = TTC_GFX_COLOR24_GRAY;  // display color for inactive state
#else
const t_u32 Color_Active   = 0;
const t_u32 Color_Inactive = 0;
#endif

/** statemachine state functions for individual states
 *
 * Each function handles a single state as declared in ets_AllStates[] below.
 *
 * @param MyStatemachine  (t_ttc_states_config*)     pointer to an initialized ttc_states instance
 * @param StateData       (t_ttc_rtls_config*)  pointer to a rtls configuration
 * @param Transition      (e_ttc_states_transition)  ==0: state did not change since last call; >0: cause of state transition
 */
void _example_ttc_states_state_init( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );
void _example_ttc_states_state_check_io( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );
void _example_ttc_states_state_process_io( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );
void _example_ttc_states_state_compute( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );
void _example_ttc_states_state_display( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition );


/** Declare each individual state of our statemachine as static data.
 *  As everything is constant, it will be put in FLASH and does not occupy any RAM.
 *
 * Attributes of each state:
 * + StateFunction
 *   Pointer to a function that will be called while this state is active.
 *
 * + AmountReachable
 *   Amount of other states to which may be switched from this state.
 *   Any other state switch will cause an assert.
 *
 * + ReachableStates[]
 *   Array of state enums to which this state may switch to.
 *   The array must be terminated by a zero (0) for extra safety.
 *
 */
const t_ttc_states_state ets_State_Init = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) ) & _example_ttc_states_state_init,

    // Unique number identifying this state
    .Number = state_number_Init,

    // amount of reachable states listed below
    .AmountReachable = 2,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_number_CheckIO,
        state_number_Display,
        0
    }
};
const t_ttc_states_state ets_State_CheckIO   = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) ) & _example_ttc_states_state_check_io,

    // Unique number identifying this state
    .Number = state_number_CheckIO,

    // amount of reachable states listed below
    .AmountReachable = 3,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_number_Compute,
        state_number_ProcessIO,
        state_number_Display,
        0
    }
};
const t_ttc_states_state ets_State_Compute   = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) ) & _example_ttc_states_state_compute,

    // Unique number identifying this state
    .Number = state_number_Compute,

    // amount of reachable states listed below
    .AmountReachable = 2,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_number_CheckIO,
        state_number_Display,
        0
    }
};
const t_ttc_states_state ets_State_ProcessIO = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) ) & _example_ttc_states_state_process_io,

    // Unique number identifying this state
    .Number = state_number_ProcessIO,

    // amount of reachable states listed below
    .AmountReachable = 2,

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_number_CheckIO,
        state_number_Display,
        0
    }
};
const t_ttc_states_state ets_State_Display   = {

    // function being called from ttc_states_run() while corresponding state is active
    .StateFunction = ( void ( * )( void*, void*, e_ttc_states_transition ) ) & _example_ttc_states_state_display,

    // Unique number identifying this state
    .Number = state_number_Display,

    // amount of reachable states listed below
    .AmountReachable = 4,

    // This state can only be called via ttc_states_call() and will return to calling state automatically
    .Flags = { .IsSubstate = 1 },

    // List of states reachable via ttc_states_switch() (must be zero terminated and sorted in increasing order!)
    // Note: A ttc_states_return() call is no switch and states being reached via a pull are not listed in here!
    .ReachableStates = {
        state_number_Init,
        state_number_CheckIO,
        state_number_Compute,
        state_number_ProcessIO,
        0
    }
};

/** Store references of all states in a zero terminated array.
 *
 *  Entries must be sorted by their value without vacancies!
 *  my_AllStates[i] + 1 = my_AllStates[i + 1]
 *
 */
const t_ttc_states_state* ets_AllStates[] = {
    &ets_State_Init,      // statemachine always starts in state of first entry
    &ets_State_CheckIO,   // ets_State_CheckIO.Number   = ets_State_Init.Number      + 1
    &ets_State_Compute,   // ets_State_Compute.Number   = ets_State_CheckIO.Number   + 1
    &ets_State_ProcessIO, // ets_State_ProcessIO.Number = ets_State_Compute.Number   + 1
    &ets_State_Display,   // ets_State_Display.Number   = ets_State_ProcessIO.Number + 1
    NULL                  // array must be zero terminated!
};

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_states_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _example_states_task( void* Argument );

/** Statemachine running this example
 *
 * If no multitasking is available but multiple delays must be used in parallel,
 * a statemachine can be implemented. This is basically a function that memorizes a current
 * state number and uses a switch(state) {} structure to decide which code has to be executed
 * at the moment. Statemachine functions typically return quickly and have to be called regularly.
 * This behaviour allows to run multiple statemachines intermittently. Multiple statemachine
 * functions can be created via ttc_task_create() and will be called at maximum frequency one
 * after another in an endless loop.
 * See also example_ttc_states for a safer way to implement statemachines!
 */
void _example_states_statemachine( void* Argument );

/** Call sub-state state_number_Display for given text and return to current state
 *
 * @param MyStatemachine  (t_ttc_states_config*)  pointer to corresponding, initialized ttc_states instance
 * @param MyStateData   statemachine data for this example application
 * @param Text          !=NULL: zero terminated string to display
 * @param TextSize      maximum size of Text[] (for safety)
 * @param X             text x-coordinate where to display Text[]
 * @param Y             text y-coordinate where to display Text[]
 * @param Color         24-bit color to use (->ttc_gfx_types.h)
 */
void _example_ttc_states_sub_display( t_ttc_states_config* MyStatemachine,  example_states_data_t* MyStateData, const char* Text, t_u8 TextSize, t_u8 X, t_u8 Y, t_u32 Color );

/** monitor function being called for every state transition as a debugging aid
 *
 */
void _example_ttc_states_monitor( volatile t_ttc_states_config* Config, example_states_data_t* MyStateData, volatile state_number_e State_Current, volatile state_number_e State_Previous, e_ttc_states_transition Transition );

//}Private Function Declarations
//{ Function Definitions *************************************************

void example_ttc_states_prepare() {                   // prepare this example for being started
    /** Note: The _prepare() function is called in single task mode before the scheduler is started.
      *       It has to return to allow all extensions to prepare.
      *       After all extensions finished their preparation, the created tasks are run.
      *       If no multitasking scheduler has been activated, all created tasks are run one after
      *       another in an endless loop. See ttc_task_start_scheduler() for details.
      *       This scheme allows to enable multiple statemachine functions in a flexible way.
      */

    if ( 1 ) { // create and configure one statemachin instance (copy and adjust for multiple instances)

        // allocate data of instance as local static (allows compiler to detect if it fits into RAM)
        static example_states_data_t Data_states_1;

        // reset data
        ttc_memory_set( &Data_states_1, 0, sizeof( Data_states_1 ) );

        // loading configuration of first statemachine
        Data_states_1.MyStatemachine = ttc_states_create();

        // set list of all states
        Data_states_1.MyStatemachine->Init.AllStates = ets_AllStates;

        // we simply use our default task data
        Data_states_1.MyStatemachine->Init.StateData = &Data_states_1;

        // register our monitor function to allow debugging of state transitions
        // (typecast avoids compiler warning)
        Data_states_1.MyStatemachine->Init.MonitorTransition =
            ( void ( * )( volatile void*, void*, volatile TTC_STATES_TYPE, volatile TTC_STATES_TYPE, e_ttc_states_transition ) )
            &_example_ttc_states_monitor;

        // only requiring a single level stack (every ttc_state_push() is followed by a ttc_state_return() call)
        Data_states_1.MyStatemachine->Init.Stack_Size = 1;

        // prepare our new ttc_states device for operation
        ttc_states_init( Data_states_1.MyStatemachine );

        #ifdef EXTENSION_ttc_gfx
        ttc_gfx_get_configuration( 1 );

        // using default graphical settings

        ttc_gfx_init( 1 );

        // load text size of current display (extra variables are required if no display is available)
        Data_states_1.Display.TextColumns =  ttc_gfx_get_configuration( 1 )->TextColumns;
        Data_states_1.Display.TextRows    =  ttc_gfx_get_configuration( 1 )->TextRows;

        #endif

        // Register a function to be run after all system has finished booting.
        // ttc_task_create() works in single- and multitasking environments.
        ttc_task_create(
            #if (TTC_TASK_SCHEDULER_AVAILABLE)
            _example_states_task,           // function to start as thread
            #else
            _example_states_statemachine,   // function to run periodically (single tasking)
            #endif
            "tSTATES",                      // thread name (just for debugging)
            256,                            // stack size (adjust to amount of local variables of _example_states_task() AND ALL ITS CALLED FUNTIONS!)
            &Data_states_1,                 // passed as argument to _example_states_task()
            1,                              // task priority (higher values mean more process time)
            NULL                            // can return a handle to created task
        );
    }
}
void _example_states_statemachine( void* Argument ) { // example statemachine will be run if no multitasking scheduler is available
    Assert_STATES_Writable( Argument, ttc_assert_origin_auto );                // always check pointer arguments with Assert() before dereferencing them!
    example_states_data_t* Data = ( example_states_data_t* ) Argument; // we're expecting this pointer type

    // get pointer to statemachine configuration
    t_ttc_states_config* MyStatemachine = Data->MyStatemachine;

    // run our prepared statemachine
    ttc_states_run( MyStatemachine );
}
void _example_states_task( void* Argument ) {         // example task will be run when multitasking scheduler (e.g. FreeRTOS) has been started
    Assert_STATES_Writable( Argument, ttc_assert_origin_auto );                // always check pointer arguments with Assert() before dereferencing them!
    example_states_data_t* Data = ( example_states_data_t* ) Argument; // we're expecting this pointer type

    // get pointer to statemachine configuration
    t_ttc_states_config* MyStatemachine = Data->MyStatemachine;

    do {
        // run our prepared statemachine
        ttc_states_run( MyStatemachine );

        ttc_task_yield(); // give cpu to other processes
    }
    while ( 1 ); // Tasks run endlessly.
}

/** Statemachine State Functions
 *
 * The private functions below implement individual states of our statemachine.
 * State functions are typically only called from ttc_states_run() according to current state value.
 * State functions itself may not call other state functions or ttc_states_run().
 *
 * They may manipulate the current state by calling
 * - ttc_states_switch() to switch to another state.
 * - ttc_states_call()   to push a return state onto stack
 * - ttc_states_return()   to pull previous state from stack (return part of a substate)
 */
void _example_ttc_states_state_init( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition ) {
    // only we know which type of pointer is stored because we set it before
    example_states_data_t* MyStateData = ( example_states_data_t* ) StateData;
    ( void ) MyStateData;
    static const char* Text = "Init";

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state

            // calling sub-state to display text as active (will automatically return to current state
            t_u8 Length = ttc_string_length8( Text, -1 );
            _example_ttc_states_sub_display( MyStatemachine,
                                             StateData,
                                             Text,
                                             Length,
                                             ( MyStateData->Display.TextColumns / 2 ) - ( Length + 1 ) / 2, // horizontally centered text
                                             ( MyStateData->Display.TextRows / 4 ) * 1,
                                             Color_Inactive
                                           );
            break;
        }
        case states_transition_Return:   { // came back from a sub-state via ttc_state_return()
            // evaluate return value from sub-state as an example (not really usefull here)
            BOOL DisplayReturn = ( BOOL ) ttc_states_get_return( MyStatemachine );
            ttc_assert_test( DisplayReturn );

            // will switch to state_number_CheckIO in next function call..
            break;
        }
        default:                       { // state did not change since previous call

            ttc_states_switch( MyStatemachine, state_number_CheckIO );
            break;
        }
    }
}
void _example_ttc_states_state_check_io( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition ) {
    // only we know which type of pointer is stored because we set it before
    example_states_data_t* MyStateData = ( example_states_data_t* ) StateData;
    static const char* Text = "CheckIO";
    static BOOL StateActive = FALSE;

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state

            StateActive = TRUE;

            // set delay to 1 second
            ttc_systick_delay_init( &( MyStateData->Delay ), 1000000 );


            // calling sub-state to display text as inactive (will automatically return to current state
            t_u8 Length = ttc_string_length8( Text, -1 );
            _example_ttc_states_sub_display( MyStatemachine,
                                             StateData,
                                             Text,
                                             Length,
                                             ( MyStateData->Display.TextColumns / 2 ) - ( Length + 1 ) / 2, // horizontally centered text
                                             ( MyStateData->Display.TextRows / 4 ) * 2,
                                             Color_Active
                                           );
            break;
        }
        case states_transition_Return:   { // came back from a sub-state via ttc_state_return()
            // evaluate return value from sub-state as an example (not really usefull here)
            BOOL DisplayReturn = ( BOOL ) ttc_states_get_return( MyStatemachine );
            ttc_assert_test( DisplayReturn );

            break;
        }
        default:                       { // state did not change since previous call

            if ( ttc_systick_delay_expired( &( MyStateData->Delay ) ) ) { // delay expired: switch to next state

                if ( StateActive ) { // state still shown as active: call sub-state to display as inactive
                    StateActive = FALSE;

                    t_u8 Length = ttc_string_length8( Text, -1 );
                    _example_ttc_states_sub_display( MyStatemachine,
                                                     StateData,
                                                     Text,
                                                     Length,
                                                     ( MyStateData->Display.TextColumns / 2 ) - ( Length + 1 ) / 2, // horizontally centered text
                                                     ( MyStateData->Display.TextRows / 4 ) * 2,
                                                     Color_Inactive
                                                   );
                }
                else { // state now shown as inactive: switch to next state

                    // randomly decide next state
                    if ( ttc_random_generate() & 1 )
                    { ttc_states_switch( MyStatemachine, state_number_ProcessIO ); }
                    else
                    { ttc_states_switch( MyStatemachine, state_number_Compute ); }
                }
            }
        }
    }
}
void _example_ttc_states_state_process_io( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition ) {

    // only we know which type of pointer is stored because we set it before
    example_states_data_t* MyStateData = ( example_states_data_t* ) StateData;
    static const char* Text = "Process";
    static BOOL StateActive = FALSE;

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state

            StateActive = TRUE;

            // set delay to 1 second
            ttc_systick_delay_init( &( MyStateData->Delay ), 1000000 );

            // calling sub-state to display text as active (will automatically return to current state
            t_u8 Length = ttc_string_length8( Text, -1 );
            _example_ttc_states_sub_display( MyStatemachine,
                                             StateData,
                                             Text,
                                             Length,
                                             0,             // left aligned text
                                             ( MyStateData->Display.TextRows / 4 ) * 3,
                                             Color_Active
                                           );
            break;
        }
        case states_transition_Return:   { // came back from a sub-state via ttc_state_return()

            // evaluate return value from sub-state as an example (not really usefull here)
            BOOL DisplayReturn = ( BOOL ) ttc_states_get_return( MyStatemachine );
            ttc_assert_test( DisplayReturn );

            break;
        }
        default:                       { // state did not change since previous call

            if ( ttc_systick_delay_expired( &( MyStateData->Delay ) ) ) { // delay expired: switch to next state

                if ( StateActive ) { // state still shown as active: call sub-state to display as inactive
                    StateActive = FALSE;

                    // calling sub-state to display text as inactive (will automatically return to current state
                    t_u8 Length = ttc_string_length8( Text, -1 );
                    _example_ttc_states_sub_display( MyStatemachine,
                                                     StateData,
                                                     Text,
                                                     Length,
                                                     0,       // left aligned text
                                                     ( MyStateData->Display.TextRows / 4 ) * 3,
                                                     Color_Inactive
                                                   );
                }
                else { // state now shown as inactive: switch to next state
                    ttc_states_switch( MyStatemachine, state_number_CheckIO );
                }
            }
            break;
        }
    }
}
void _example_ttc_states_state_compute( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition ) {
    // only we know which type of pointer is stored because we set it before
    example_states_data_t* MyStateData = ( example_states_data_t* ) StateData;
    static const char* Text = "Compute";
    static BOOL StateActive = FALSE;

    switch ( Transition ) {
        case states_transition_Switch: { // came from different state: initialize this state
            StateActive = TRUE;

            // set delay to 1 second
            ttc_systick_delay_init( &( MyStateData->Delay ), 1000000 );

            // calling sub-state to display text as active (will automatically return to current state
            t_u8 Length = ttc_string_length8( Text, -1 );
            _example_ttc_states_sub_display( MyStatemachine,
                                             StateData,
                                             Text,
                                             Length,
                                             MyStateData->Display.TextColumns - Length , // right aligned text,
                                             ( MyStateData->Display.TextRows / 4 ) * 3,
                                             Color_Active
                                           );
            break;
        }
        case states_transition_Return:   { // came back from a sub-state via ttc_state_return()

            // evaluate return value from sub-state as an example (not really usefull here)
            BOOL DisplayReturn = ( BOOL ) ttc_states_get_return( MyStatemachine );
            ttc_assert_test( DisplayReturn );

            break;
        }
        default:                       { // state did not change since previous call

            if ( ttc_systick_delay_expired( &( MyStateData->Delay ) ) ) { // delay expired: switch to next state

                if ( StateActive ) { // state still shown as active: call sub-state to display as inactive
                    StateActive = FALSE;

                    // calling sub-state to display text as inactive (will automatically return to current state
                    t_u8 Length = ttc_string_length8( Text, -1 );
                    _example_ttc_states_sub_display( MyStatemachine,
                                                     StateData,
                                                     Text,
                                                     Length,
                                                     MyStateData->Display.TextColumns - Length , // right aligned text,
                                                     ( MyStateData->Display.TextRows / 4 ) * 3,
                                                     Color_Inactive
                                                   );
                }
                else { // state now shown as inactive: switch to next state
                    ttc_states_switch( MyStatemachine, state_number_CheckIO );
                }
            }
            break;
        }
    }
}
void _example_ttc_states_state_display( t_ttc_states_config* MyStatemachine, void* StateData, e_ttc_states_transition Transition ) {
    // not used here
    ( void ) StateData;

    #ifdef EXTENSION_ttc_gfx

    // obtaining value of Argument in ttc_states_call() call
    example_states_display_t* Display = ( example_states_display_t* ) ttc_states_get_argument( MyStatemachine );

    if ( Display->Text ) { // display given text
        ttc_gfx_color_fg24( Display->Color );
        ttc_gfx_text_cursor_set( Display->X, Display->Y );
        ttc_gfx_text_boxed( Display->Text,
                            Display->TextSize,
                            2
                          );
    }
    #endif

    // return to calling state (constant return value is only set as an example how to return a value)
    ttc_states_return( MyStatemachine, ( void* ) TRUE );
}

// helper function
void _example_ttc_states_sub_display( t_ttc_states_config* MyStatemachine,  example_states_data_t* MyStateData, const char* Text, t_u8 TextSize, t_u8 X, t_u8 Y, t_u32 Color ) {
    ttc_assert_address_writable_origin( MyStatemachine, ttc_assert_origin_auto );
    ttc_assert_address_writable_origin( MyStateData,  ttc_assert_origin_auto );

    // prepare data to display
    MyStateData->Display.Text     = Text;
    MyStateData->Display.TextSize = TextSize; // no more characters will be printed
    MyStateData->Display.X        = X;
    MyStateData->Display.Y        = Y;
    MyStateData->Display.Color    = Color;

    // call sub-state display (will automatically return to current state)
    ttc_states_call( MyStatemachine, state_number_Display, &MyStateData->Display );
}
void _example_ttc_states_monitor( volatile t_ttc_states_config* Config, example_states_data_t* MyStateData, volatile state_number_e State_Current, volatile state_number_e State_Previous, e_ttc_states_transition Transition ) {
    // place breakpoint here!

    // avoid compiler warnings about unused variables
    ( void ) Config;
    ( void ) MyStateData;
    ( void ) State_Current;
    ( void ) State_Previous;
    ( void ) Transition;

    volatile static t_u32 CallCounter = 0; // counts function calls
    const           t_u32 BreakAt     = 0; // set to value > 0 to enable breakpoint at certain function call
    CallCounter++;

    if ( BreakAt && ( CallCounter == BreakAt ) ) {
        ttc_assert_break_origin( ttc_assert_origin_auto ); // desired function call reached
    }
}


//}Function Definitions
