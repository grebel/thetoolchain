/** example_ttc_pwm.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for pulse width modulation output
 *
 *  Basic flow of booting examples:
 *  (1) ttc_extensions.c:ttc_extensions_start() calls example_ttc_pwm_prepare() to
 *      - initialize all required devices
 *      - initializes its global and local static variables
 *      - create all required task or statemachine instances
 *      - return immediately
 *
 *  Multitasking is available:
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_pwm_prepare() get executed.
 *      Each task uses its private stack as given to ttc_task_create() before.
 *
 *  Multitasking is NOT available:
 *  (2) Minimalistic main loop is started
 *  (3) Main loop calls all registered statemachine functions in an endless loop.
 *      All statemachine functions use the same global stack that is maximum of all
 *      stack sizes given to ttc_task_create() before.
 *
 *  Created from template example_ttc_device.c revision 20 at 20180502 11:57:47 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_pwm.h"
#include "../ttc-lib/ttc_pwm.h" // high-level driver of device to use
#include "../ttc-lib/ttc_heap.h"     // dynamic memory allocations during runtime
#include "../ttc-lib/ttc_task.h"     // allows to use application in single- and multitasking setup

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_pwm_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _example_pwm_task( void* Argument );

/** Statemachine running this example
 *
 * If no multitasking is available but multiple delays must be used in parallel,
 * a statemachine can be implemented. This is basically a function that memorizes a current
 * state number and uses a switch(state) {} structure to decide which code has to be executed
 * at the moment. Statemachine functions typically return quickly and have to be called regularly.
 * This behaviour allows to run multiple statemachines intermittently. Multiple statemachine
 * functions can be created via ttc_task_create() and will be called at maximum frequency one
 * after another in an endless loop.
 * See also example_ttc_states for a safer way to implement statemachines!
 */
void _example_pwm_statemachine( void* Argument );

//}Private Function Declarations
//{ Function Definitions *************************************************

void example_ttc_pwm_prepare() {                   // prepare this example for being started
    /** Note: The _prepare() function is called in single task mode before the scheduler is started.
      *       It has to return to allow all extensions to prepare.
      *       After all extensions finished their preparation, the created tasks are run.
      *       If no multitasking scheduler has been activated, all created tasks are run one after
      *       another in an endless loop. See ttc_task_start_scheduler() for details.
      *       This scheme allows to enable multiple statemachine functions in a flexible way.
      */

    if ( 1 ) { // create one instance of your example (copy and adjust for multiple instances)

        // LogicalIndex = 1: use device defined as TTC_PWM1 in makefile
        t_u8 LogicalIndex = 1;

        // allocate data of instance as local static (allows compiler to detect if it fits into RAM)
        static example_pwm_data_t Data_pwm_1;

        // reset data
        ttc_memory_set( &Data_pwm_1, 0, sizeof( Data_pwm_1 ) );

        // loading configuration of first pwm device
        Data_pwm_1.Config_PWM = ttc_pwm_get_configuration( LogicalIndex );

        Data_pwm_1.Config_PWM->Init.DutyCycle     = 300;   // set desired duty cycle in promille (1/1000)
        Data_pwm_1.Config_PWM->Init.Frequency     = 10000; // set desired frequency in Hertz
        Data_pwm_1.Config_PWM->Init.Flags.Enabled = 1;     // enable or disable PWM

        // initializing our pwm device here makes debugging easier (still single-tasking)
        // Logical index is stored inside each device configuration
        ttc_pwm_init( LogicalIndex );

        // Register a function to be run after all system has finished booting.
        // ttc_task_create() works in single- and multitasking environments.
        ttc_task_create(
            #if (TTC_TASK_SCHEDULER_AVAILABLE)
            _example_pwm_task,           // function to start as thread
            #else
            _example_pwm_statemachine,   // function to run periodically (single tasking)
            #endif
            "tPWM",                     // thread name (just for debugging)
            256,                             // stack size (adjust to amount of local variables of _example_pwm_task() AND ALL ITS CALLED FUNTIONS!)
            &Data_pwm_1, // passed as argument to _example_pwm_task()
            1,                               // task priority (higher values mean more process time)
            NULL                             // can return a handle to created task
        );
    }
}

// Note: If you implement both of the example functions below, your example will run in single- and multitasking configuration.

void _example_pwm_statemachine( void* Argument ) { // example statemachine will be run if no multitasking scheduler is available
    Assert_PWM_Writable( Argument, ttc_assert_origin_auto );                // always check pointer arguments with Assert() before dereferencing them!
    example_pwm_data_t* Data = ( example_pwm_data_t* ) Argument; // we're expecting this pointer type

    // get pointer to device configuration
    t_ttc_pwm_config* Config_PWM = Data->Config_PWM;

    ttc_pwm_set_dutycycle(1, 500); // change duty cycle of first PWM output to 50%

    if ( ttc_systick_delay_expired( & Data->Delay ) ) { // example periodic delay
        ttc_systick_delay_init( & Data->Delay,
                                ttc_systick_delay_get_minimum() << 4 // minimum delay * 16 is a safe way to always use a small and valid delay
                              ); // set new delay time

        // implement statemachine here ...
    }

    // implement statemachine here ...
}
void _example_pwm_task( void* Argument ) {       // example task will be run when multitasking scheduler (e.g. FreeRTOS) has been started
    Assert_PWM_Writable( Argument, ttc_assert_origin_auto );                // always check pointer arguments with Assert() before dereferencing them!
    example_pwm_data_t* Data = ( example_pwm_data_t* ) Argument; // we're expecting this pointer type

    // get pointer to device configuration (remove if not required)
    t_ttc_pwm_config* Config_PWM = Data->Config_PWM;

    do {
        // This loop runs endlessly.

        // call statemachine or implement task here...
        _example_pwm_statemachine( Argument );

        ttc_task_yield(); // give cpu to other processes
    }
    while ( 1 ); // Tasks run endlessly.
}

//}Function Definitions
