#ifndef EXAMPLE_THREADING
#define EXAMPLE_THREADING

/*{ TemplateName::.h ************************************************
 
 Empty template for new header files.
 Copy and adapt to your needs.
 
}*/
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Includes *************************************************************

 // Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_task.h"
#include "../ttc-lib/ttc_queue.h"
#include "../ttc-lib/ttc_semaphore.h"

#ifdef EXTENSION_ttc_gfx
#  include "ttc_gfx.h"
#endif

//}Includes
//{ Structures/ Enums ****************************************************

typedef struct { // t_et_printjob
    // Column == -1 | Row == -1: continue at last position
    t_s8 Column;   // >= 0: Column to move cursor to
    t_s8 Row;      // >= 0: Row to move cursor to
    char Text[10]; // zero terminated string to print
    t_s32 Value;   // passed as value to printf
} t_et_printjob;

/** data being sent to taskProducer()/ taskConsumer() */
typedef struct { // ets_ThreadInfo_t
    t_ttc_semaphore_smart* SemaphoreProducer2Consumer;  // semaphore to consumer tasks
          t_ttc_queue_generic  QueuePrint;                  // queue to print on display
                      int  DisplayRow;                  // row in display used for debug output
} ets_ThreadInfo_t;

//}Structures/ Enums
//{ Global Variables *****************************************************



//}Global Variables
//{ Function declarations **************************************************

// run demo
void example_threading_semaphores_prepare();

/** graphical display of current queue size
 * @param Semaphore   Semaphore/ Queue which size should be displayed
 * @param QueuePrint  queue to print to
 */
void displayQueue(t_ttc_semaphore_smart* Semaphore, t_ttc_queue_generic QueuePrint);

// produces tasks to do
void taskProducer(void *TaskArgument);

// consumes + executes tasks to do
void taskConsumer(void *TaskArgument);

// adds given data as new print job to given queue
// Will block if queue is full
void queuePrintJob(t_ttc_queue_generic PrintQueue, t_s8 Column, t_s8 Row, const char* Text);
void queuePrintJobV(t_ttc_queue_generic PrintQueue, t_s8 Column, t_s8 Row, const char* Text, t_s32 Value);

// central print task (display cannot be used from multiple tasks
void taskPrint(void *TaskArgument);


//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_THREADING
