
/*{ TemplateName::.c ************************************************

 Empty template for new c-files.
 Copy and adapt to your needs.

}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_network.h"

//{ Global variables *************************************************

t_ttc_Port LEDA, LEDB;
bool LedState = 0;
const t_u8* Message = "Hallo Welt";

//}Global Variables
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_network_prepare(){
#ifdef TTC_LED1
    ttc_gpio_init_variable(&LEDA, TTC_LED1, E_ttc_gpio_mode_output_push_pull);
#endif
#ifdef TTC_LED2
    ttc_gpio_init_variable(&LEDB, TTC_LED2, E_ttc_gpio_mode_output_push_pull);
#else
    // no TTC_LEDB defined: use same LED
   // LEDB = LEDA;
#endif

    t_ttc_network_config* NetConfig = ttc_network_get_configuration(1);
    NetConfig->LocalAddress           = 23;
    NetConfig->MacStack               = tntm_ieee802154;
    NetConfig->Protocol               = tntp_6lowpan;
    NetConfig->Stack                  = tnts_undefined;
    NetConfig->Channel                = 13;
    NetConfig->function_ReceivePacket = dummy_sent;

    ttc_network_init(1);

    ttc_task_create(task_MAC_sender, "taskMACsender", 128, (void*)NetConfig, 1, NULL);
}
void task_MAC_sender(void *TaskArgument) {
    t_ttc_network_config *Config = (t_ttc_network_config*)TaskArgument;
    t_base MessageLength = 10;
    char* ReceivedMessage;

    while(1) {
        ttc_gpio_set_variable(&LEDA);
        //ttc_network_send_broadcast(Config, Message, MessageLength);
        ttc_network_send_unicast(1, 4, Message, MessageLength);
       //t_ttc_list* NodeList = ttc_network_get_neighbours(1);
       //t_ttc_network_node* Neighbour = (t_ttc_network_node*)NodeList->First;

        /*
        for (t_u8 Repeat = 10; Repeat > 0; Repeat--) {
            while (Node->Next != NULL) {
                ttc_network_send_unicast(1, Neighbour->Address, Message, MessageLength);
                Neighbour = Neighbour->ListItem->Next;
            }*/

            ttc_task_msleep(500);

        ttc_network_receive(1);
        if(Config->RxPipe.First != NULL){
            ReceivedMessage = (char*)ttc_list_pop_front_single_wait(&(Config->RxPipe));
            //if(strcmp(ReceivedMessage, (char*)Message) == 0){
            if(ReceivedMessage != NULL){
                ttc_gpio_set_variable(&LEDB);
                ttc_task_msleep(10000);
            }
        }

        ttc_gpio_clr_variable(&LEDA);
        ttc_gpio_clr_variable(&LEDB);

        ttc_task_msleep(500);
    }
}
void receivePacket(ttc_network_packet_status* Status, t_u8* Buffer, t_base Amount) {
    (void) Status;
    (void) Buffer;
    (void) Amount;

    // do something
}

// DEPRECATED
void dummy_sent(void *ptr, int status, int num_tx){
    switch(status) {
      case MAC_TX_COLLISION:
        //PRINTF("rime: collision after %d tx\n", num_tx);
        break;
      case MAC_TX_NOACK:
        //PRINTF("rime: noack after %d tx\n", num_tx);
        break;
      case MAC_TX_OK:
        //PRINTF("rime: sent after %d tx\n", num_tx);
        ttc_gpio_clr_variable(&LEDA);
        LedState = 1;
        uSleep(1000000);
        break;
      case MAC_TX_ERR:
        ttc_gpio_set_variable(&LEDB);
        LedState = 1;
        uSleep(1000000);
        break;
      default:
        break;
        //PRINTF("rime: error %d after %d tx\n", status, num_tx);
      }
}


//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
