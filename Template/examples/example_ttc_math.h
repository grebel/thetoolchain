#ifndef EXAMPLE_MATH_H
#define EXAMPLE_MATH_H

/** example_ttc_MATH.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_math.
 *  The basic flow is as follows:
 *  (1) example_ttc_math_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_math_prepare() gets executed
 *
 *  Created from template example_ttc_device.h revision 12 at 20150724 07:49:04 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_MATH.c only do not belong here!
#include "../ttc-lib/ttc_basic_types.h"
#include "../ttc-lib/ttc_math_types.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_math_t
    ttm_number Precision; // currently set precision of ttc_math
} example_math_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_math_prepare();


//}Function prototypes

#endif //EXAMPLE_MATH_H
