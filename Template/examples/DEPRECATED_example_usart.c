/*{ TemplateName::.c ************************************************
 
 Empty template for new c-files.
 Copy and adapt to your needs.
 
}*/

#include "DEPRECATED_example_usart.h"

//{ Global variables *************************************************

const char* TextHello = "Hello, talk to me!\n";
#define SIZE_YouSaid 20
char YouSaid1[SIZE_YouSaid];
char YouSaid2[SIZE_YouSaid];

BOOL UsartAvailable[2] = {FALSE, FALSE};

#define USART1_INDEX 1                    // using first  USART available on current board
#define USART2_INDEX 2                    // using second USART available on current board

//}Global Variables
//{ Function definitions *************************************************

void example_usart_start() {
    u8_t MaxUSART = ttc_usart_get_max_logicalindex();
    Assert(MaxUSART > 0, ec_UNKNOWN); // no USARTs defined by board makefile!
    ttc_usart_errorcode_e Error;

#ifdef TTC_LED1
    ttc_gpio_init(TTC_LED1, tgm_output_push_pull, tgs_Max);
#endif
#ifdef TTC_LED2
    ttc_gpio_init(TTC_LED2, tgm_output_push_pull, tgs_Max);
#endif

    if (USART1_INDEX <= MaxUSART) { // USART defined by board: initialize it
        ttc_usart_config_t* Config = ttc_usart_get_configuration(USART1_INDEX);
        Assert(Config, ec_UNKNOWN);

        // change some settings
        Config->Flags.Bits.IrqOnRxNE = 1; // enable IRQ on Rx-buffer not empty
        Config->Char_EndOfLine = 13;      // packets end at carriage return

        if (0)
            Config->Flags.Bits.DelayedTransmits = 1; // all transmits are queued and sent in interrupt service routin parallel to this task
        else
            Config->Flags.Bits.DelayedTransmits = 0; // all transmits are blocking until data is send (can loose rx-data while waiting for transmit!)

        // register activity indicators (will be called from interrupt service routine!)
        Config->activity_rx_isr = exu_activity_rx;
        Config->activity_tx_isr = exu_activity_tx;

        Error = ttc_usart_init(USART1_INDEX); // note: first bytes might get lost until ttc_usart_register_receive()
        ttc_usart_register_receive(USART1_INDEX, eu_receiveData, (void*) USART1_INDEX);
        Assert( Error == ec_usart_OK, ec_UNKNOWN);
        UsartAvailable[0] = TRUE;
    }
    if (USART2_INDEX <= MaxUSART) { // USART defined by board: initialize it
        ttc_usart_config_t* Config = ttc_usart_get_configuration(USART2_INDEX);
        Assert(Config, ec_UNKNOWN);

        // change some settings
        Config->Flags.Bits.IrqOnRxNE = 1; // enable IRQ for incoming data
        Config->Char_EndOfLine = 13; // packets end at carriage return

        // will also initialize USART
        Error = ttc_usart_register_receive(USART2_INDEX, eu_receiveData, (void*) USART2_INDEX);
        Assert(Error == ec_usart_OK, ec_UNKNOWN);
        UsartAvailable[1] = TRUE;
    }

    // set first logical USART as standard output (otherwise last initialized one would be used)
    ttc_usart_stdout_set(USART1_INDEX);

    // set usart as stdout interface
    // ttc_string_register_stdout(ttc_channel_usart_send_block, ttc_usart_stdout_send_string);
    ttc_string_register_stdout(ttc_usart_stdout_send_block, ttc_usart_stdout_send_string);


    ttc_task_create(task_USART,                    // function to start as thread
                "USART",                           // thread name (just for debugging)
                (size_t) 512,                      // stack size
                (void *) NULL,                     // passed as argument to taskLEDs()
                (unsigned portBASE_TYPE) 1,        // task priority (higher values mean more process time)
                NULL                               // can return a handle to created task
                );

    // _start() must return (task-scheduler is started later)!
}
void task_USART(void *TaskArgument) {
    TaskArgument = TaskArgument;      // avoids warning: unused parameter 'TaskArgument'

    u8_t Count = 0;

    while (1) {

        // make use of stdout setting via ttc_string_printf()
        printf("example_usart: %i=0x%x ", Count, Count);
        Count++;

        // zero copying (string is declared as constant)
        // function queues string for transmit and returns immediately
        ttc_usart_send_string_const(USART1_INDEX, TextHello, -1);


        // waits until all bytes have been transmitted (optional)
        ttc_usart_flush_tx(USART1_INDEX);

        // function will wait for all queued strings and return after sending this string
        ttc_usart_send_string_const(USART1_INDEX, " ...", -1);

        if (0 && UsartAvailable[1]) { // other USART initialized: send hello too
            ttc_usart_send_string_const(USART2_INDEX, TextHello, -1);
           // ttc_usart_flush_tx(USART2_INDEX);
        }
        ttc_task_check_stack(); // place a breakpoint inside to see real stack-usage; will block forever if stack has overflown

        ttc_task_msleep(1000);
    }
}
ttc_memory_block_t* eu_receiveData(void* Argument, ttc_usart_config_t* USART_Generic, ttc_memory_block_t* Data) {
    Assert(USART_Generic != NULL, ec_InvalidArgument);
    Assert(Data          != NULL, ec_InvalidArgument);

    // example implementation: forwarding data USART1<->USART2
    //
    u32_t UsartIndex = (u32_t) Argument;

    switch (UsartIndex) {

    case USART1_INDEX: { // copy data USART1 -> USART2
        if (Data->Size < SIZE_YouSaid-1) {
            ttc_memory_copy(YouSaid1, Data->Buffer, Data->Size);
            Data->Buffer[Data->Size] = 0; // terminate string
        }
        else {
            ttc_memory_copy(YouSaid1, Data->Buffer, SIZE_YouSaid-1);
            Data->Buffer[SIZE_YouSaid-1] = 0; // terminate string
        }

        if (1) {
            u8_t* StrippedBuffer = Data->Buffer;
            Base_t StrippedSize  = Data->Size;
            ttc_string_strip(&StrippedBuffer, &StrippedSize);
            ttc_usart_send_string_const(USART1_INDEX, "relaying: '", -1);
            ttc_usart_send_raw(USART1_INDEX, StrippedBuffer, StrippedSize); // not constant: data will be copied
            ttc_usart_send_string_const(USART1_INDEX, "'\n", -1);
        }
        if (UsartAvailable[1]) { // other USART initialized: copy byte to it

            ttc_usart_send_block(USART2_INDEX, Data);
            Data = NULL;  // memory block now belongs to ttc_usart_send_block()
        }
        break;
    }
    case USART2_INDEX: { // copy data USART2 -> USART1

        ttc_usart_send_block(USART1_INDEX, Data);
        Data = NULL;  // memory block now belongs to ttc_usart_send_block()
        break;
    }
    default: Assert(0, ec_UNKNOWN); break; // should never happen!
    }

    return Data; // memory block can now be reused
}
void exu_activity_tx(ttc_usart_config_t* USART_Generic, u8_t Byte) {
    (void) USART_Generic;
    (void) Byte;

#ifdef TTC_LED1
    static u8_t StateTX = 0;
    if (StateTX++ & 1)
        ttc_gpio_set(TTC_LED1);
    else
        ttc_gpio_clr(TTC_LED1);
#endif
}
void exu_activity_rx(ttc_usart_config_t* USART_Generic, u8_t Byte) {
    (void) USART_Generic;
    (void) Byte;

#ifdef TTC_LED2
    static u8_t StateRX = 0;
    if (StateRX++ & 1)
        ttc_gpio_set(TTC_LED2);
    else
        ttc_gpio_clr(TTC_LED2);
#endif
}

//}FunctionDefinitions
