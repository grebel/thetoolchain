#ifndef EXAMPLE_STATES_H
#define EXAMPLE_STATES_H

/** example_ttc_STATES.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for universal and debuggable statemachine for single- and multitasking environments
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_states_none_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_states_none_prepare() get executed
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_states_none_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template example_ttc_device.h revision 16 at 20180126 14:37:55 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_STATES.c only do not belong here!
#include "../ttc-lib/ttc_basic.h"      // basic datatypes and definitions
#include "../ttc-lib/ttc_systick.h"    // exact delays and timeouts
#include "../ttc-lib/ttc_task_types.h" // required for task delays
#include "../ttc-lib/ttc_states_types.h" // datatypes of states devices

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // data used by _example_ttc_states_state_display()
    const char* Text;        // !=NULL: zero terminated string to display
    t_u8        TextSize;    // maximum size of Text[] (for safety)
    t_u8        X;           // text x-coordinate where to display Text[]
    t_u8        Y;           // text y-coordinate where to display Text[]
    t_u32       Color;       // 24-bit color to use (->ttc_gfx_types.h)
    t_u8        TextColumns; // amount of columns displayable for current font (only Gfx output)
    t_u8        TextRows;    // amount of rows displayable for current font (only Gfx output)

} example_states_display_t;

typedef struct { // example_states_data_t
    t_ttc_states_config*     MyStatemachine; // pointer to configuration of ttc_states device to use
    t_ttc_systick_delay      Delay;          // this type of delay is usable in single- and multitasking setup
    example_states_display_t Display;        // data required by _example_ttc_states_state_display()
    // more arguments...
} example_states_data_t;

typedef enum {  // named states of our statemachine improve readability
    state_number_Reset = 0,  // no real state (reset value)

    // states below provide an example setup of a fictional application
    state_number_Init,       // initialization (run only once)
    state_number_CheckIO,    // check for incoming data
    state_number_Compute,    // compute something
    state_number_ProcessIO,  // process received data
    state_number_Display,    // display current values on display
    state_number_unknown     // higher values are invalid
} state_number_e;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_states_prepare();


//}Function prototypes

#endif //EXAMPLE_STATES_H
