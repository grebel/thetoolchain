#ifndef EXAMPLE_USART_H
#define EXAMPLE_USART_H

/*{ DEPRECATED_example_usart.h ******************************************************
 
                       The ToolChain
                       
 Example code demonstrating how to use 
 Universal Synchronous Asynchronous Receiver Transmitter (USART)
 
 written by Gregor Rebel 2012
 
}*/
//{ Defines/ TypeDefs ****************************************************


//}Defines
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_usart.h"
#include "../ttc-lib/ttc_string.h"

//}Includes
//{ Structures/ Enums ****************************************************

//}Structures/ Enums
//{ Global Variables *****************************************************



//}Global Variables
//{ Function declarations **************************************************

/* initializes + starts USART example
 */
void example_usart_start();

/* runs echo server on initialized USART
 */
void task_USART(void *TaskArgument);

/* takes bytes received from USART
 */
ttc_memory_block_t* eu_receiveData(void* Argument, ttc_usart_config_t* USART_Generic, ttc_memory_block_t* Data);

/* release receive buffers to be reused by USART
 */
void releaseRxBuffer_USART1(ttc_memory_block_t* RxBuffer);
void releaseRxBuffer_USART2(ttc_memory_block_t* RxBuffer);

/** called automatically during transmit/ receive activity
  */
void exu_activity_rx(ttc_usart_config_t* USART_Generic, u8_t Byte);
void exu_activity_tx(ttc_usart_config_t* USART_Generic, u8_t Byte);

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_USART_H
