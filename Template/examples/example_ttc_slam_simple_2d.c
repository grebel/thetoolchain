/** example_ttc_slam_simple_2d.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for 2 dimensional localization and mapping storing all n! distances between n nodes
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_slam_simple_2d_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_slam_simple_2d_prepare() get executed
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_slam_simple_2d_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template example_ttc_device_architecture.c revision 15 at 20160607 12:30:10 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_slam_simple_2d.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_random.h"
#include "../ttc-lib/ttc_slam.h"
#include "../ttc-lib/ttc_math.h"
#include "../ttc-lib/ttc_semaphore.h"
#ifdef TTC_LED1
#include "../ttc-lib/ttc_gpio.h"
#endif

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_slam_simple_2d_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _task_slam_simple_2d( void* Argument );

#ifdef TTC_GFX1

#include "../ttc-lib/ttc_string.h"

A_define( t_u8, etsc2_String, 30 ); // buffer for temporary strings

// enable gfx macros
#define _etsc2_gfx_print_at(X, Y, String, MaxSize ) ttc_gfx_text_at(X, Y, (const char*) String, MaxSize )

// Define global colors
const t_u32 ets_ColorBg        = TTC_GFX_COLOR24_SILVER;
const t_u32 ets_ColorFg        = TTC_GFX_COLOR24_BLUE;
const t_u32 ets_ColorNorthpole = TTC_GFX_COLOR24_RED;
const t_u32 ets_ColorSouthpole = TTC_GFX_COLOR24_RED;
const t_u32 ets_ColorEastnode  = TTC_GFX_COLOR24_GRAY;
const t_u32 ets_ColorWestnode  = TTC_GFX_COLOR24_BLACK;
const t_u32 ets_ColorForeigner = TTC_GFX_COLOR24_GREEN;

void _etsc2_gfx_init( example_slam_simple_2d_data_t* Data ) {

    Data->GFX = ttc_gfx_get_configuration( 1 );
    Assert_Writable( Data->GFX, ttc_assert_origin_auto );

    // set color to use for first screen clear
    ttc_gfx_color_bg24( ets_ColorBg );
    ttc_gfx_color_fg24( ets_ColorFg );

    ttc_gfx_init( 1 );

    _etsc2_gfx_print_at( 0, 0, "SLAM Simple2D", -1 );
}
void _etsc2_gfx_printf1_at( t_u16 X, t_u16 Y, const char* Format, t_base Value ) {
    ttc_string_snprintf( etsc2_String.Data, etsc2_String.Size, Format, Value );
    _etsc2_gfx_print_at( X, Y, etsc2_String.Data, etsc2_String.Size );
}
void _etsc2_gfx_printf2_at( t_u16 X, t_u16 Y, const char* Format, t_base Value1, t_base Value2 ) {
    ttc_string_snprintf( etsc2_String.Data, etsc2_String.Size, Format, Value1, Value2 );
    _etsc2_gfx_print_at( X, Y, etsc2_String.Data, etsc2_String.Size );
}

typedef enum {
    etssi_None,
    etssi_Circle,           // circle of radius Xb around (Xa, Ya)
    etssi_CircleFilled,     // filled circle of radius Xb around (Xa, Ya)
    etssi_Rectangle,        // rectangle of size Xb x Yb centered around (Xa,Ya)
    etssi_RectangleFilled,  // filled rectangle of size Xb x Yb centered around (Xa,Ya)
    etssi_Line,             // line  (Xa, Ya) -- (Xb, Yb)
    etssi_ArrowA2B,         // arrow (Xa, Ya) -> (Xb, Yb)
    etssi_ArrowB2A,         // arrow (Xb, Yb) -> (Xa, Ya)

    etssi_unknown           // add more types above
} etss_item_e;

/** display graphical item using slam coordinate system
 *
 * @param Data   configuration data of current example
 * @param Item   type of graphical item to display
 * @param Xa     x-coordinate of point A
 * @param Ya     y-coordinate of point A
 * @param Xb     multi purpose value
 * @param Yb     multi purpose value
 */
void _etss_gfx_item( example_slam_simple_2d_data_t* Data, etss_item_e Item, ttm_number Xa, ttm_number Ya, ttm_number Xb, ttm_number Yb ) {

    t_u16 Gfx_Xa = Data->Gfx_CenterX + Xa * Data->Gfx_ScaleX;
    t_u16 Gfx_Ya = Data->Gfx_CenterY - Ya * Data->Gfx_ScaleY;

    switch ( Item ) {
        case etssi_None:
            break;
        case etssi_Circle:
        case etssi_CircleFilled: {
            t_u16 Radius = Xb * Data->Gfx_ScaleX;

            if ( Item == etssi_Circle )
            { ttc_gfx_circle( Gfx_Xa, Gfx_Ya, Radius ); }
            else
            { ttc_gfx_circle_fill( Gfx_Xa, Gfx_Ya, Radius ); }
            break;
        }
        case etssi_Rectangle:
        case etssi_RectangleFilled: {
            t_u16 Width  = Xb * Data->Gfx_ScaleX;
            t_u16 Height = Yb * Data->Gfx_ScaleY;
            if ( Width == 0 )  { Width = 1; }
            if ( Height == 0 ) { Height = 1; }

            if ( Item == etssi_Rectangle )
            { ttc_gfx_rect( Xa - Width / 2, Ya - Height / 2, Width, Height ); }
            else
            { ttc_gfx_rect_fill( Xa - Width / 2, Ya - Height / 2, Width, Height ); }
            break;
        }
        case etssi_Line:
        case etssi_ArrowA2B:
        case etssi_ArrowB2A: {
            t_u16 Gfx_Xb = Data->Gfx_CenterX + Xb * Data->Gfx_ScaleX;
            t_u16 Gfx_Yb = Data->Gfx_CenterY - Yb * Data->Gfx_ScaleY;

            t_u8 Length1, Length2;
            if ( Item == etssi_Line )
            { Length1 = Length2 = 0; }
            else {
                if ( Item == etssi_ArrowA2B )
                { Length1 =  0; Length2 = 10; }
                else
                { Length1 = 10; Length2 = 0; }
            }
            ttc_gfx_arrow( Gfx_Xa, Gfx_Ya, Gfx_Xb, Gfx_Yb, Length1, Length2 );
            break;
        }
        default: ttc_assert_halt_origin( ttc_assert_origin_auto ); break; // got invalid item type!
    }
}

#else

// disable gfx macros and functions
#define _etsc2_gfx_init( Data )
#define _etsc2_gfx_print_at(X, Y, String, MaxSize )
void _etsc2_gfx_printf1_at( t_u16 X, t_u16 Y, const char* Format, t_base Value ) { }
void _etsc2_gfx_printf2_at( t_u16 X, t_u16 Y, const char* Format, t_base Value1, t_base Value2 ) { }

#endif

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function Definitions *************************************************

volatile ttm_number MaxRelativeError    = 0;
const    ttm_number CoordinatePrecision = 0.1;  // required precision for coordinate values

/** checks if value is equal to compare value according to allowed error
 *
 * Note: updates global variable MaxRelativeError
 *
 * @param Value      value to check
 * @param Compare    comparison value
 * @param Precision  allowed error
 * @return           ( abs(Value-Compare) < Precision )
 */
void _etss_within( ttm_number Value, ttm_number Compare, ttm_number Precision ) {
    ttm_number Error = ttc_basic_distance( Value, Compare );
    if ( ( Value > 1000 ) && ( Compare != 0.0 ) ) { // using relative precision for large values
        Error /= Compare;
        if ( MaxRelativeError < Error ) {
            MaxRelativeError = Error;
        }
    }

    Assert( Error < Precision , ttc_assert_origin_auto );
}

/** display current node positions in indexed slam instance
 *
 * @param  IndexSlam          logical index of ttc_slam instance
 * @param  PositionForeigners !=NULL: array of positions of foreign nodes to be displayed
 * @param  AmountForeigners   size of PositionForeigner[]
 * @param  References         array of reference nodes being used to laterate position of PositionForeigners[0]
 */
void _etss_display( example_slam_simple_2d_data_t* Data, t_ttc_math_vector3d_xyz* PositionForeigners, t_u8 AmountForeigners, t_ttc_slam_distance* References ) {

#define ETSS2_AMOUNT_PREVIOUS_FOREIGNERS 4  // maximum amount of foreigners to be displayed
#define ETSS2_AMOUNT_REFERENCES          4  // maximum amount of reference nodes used for distance measures

    #ifdef TTC_GFX1  // display element on first graphics display
    // get configuration of slam device
    t_ttc_slam_config* Config_SLAM = ttc_slam_get_configuration( Data->IndexSLAM );

    ttm_number BoxWidth  = Config_SLAM->BoundingBox.X_Max - Config_SLAM->BoundingBox.X_Min;
    ttm_number BoxHeight = Config_SLAM->BoundingBox.Y_Max - Config_SLAM->BoundingBox.Y_Min;

    t_ttc_gfx_config*  Config_GFX  = ttc_gfx_get_configuration( 1 );

    t_u16 etss_NodeAreaSize = Config_GFX->Width;

    // calculating center of square rectangle at bottom of display
    Data->Gfx_CenterX = Config_GFX->Width / 2;
    Data->Gfx_CenterY = Config_GFX->Height - Data->Gfx_CenterX;

    // aspect correction for easier display scales
    const ttm_number AspectRatioX = 4.0 / 4.6;
    const ttm_number AspectRatioY = AspectRatioX;

    // scale to fit bounding box into square rectangle
    Data->Gfx_ScaleX = AspectRatioX * ( etss_NodeAreaSize - 10 ) / BoxWidth;
    Data->Gfx_ScaleY = AspectRatioY * ( etss_NodeAreaSize - 10 ) / BoxHeight;

    // clear node area
    ttc_gfx_color_fg24( ets_ColorBg );

    // storing previous foreigner positions for faster screen updates
    static struct { t_u16 X; t_u16 Y; } etss_PreviousForeigners[ETSS2_AMOUNT_PREVIOUS_FOREIGNERS];
    static t_u8                         etss_AmountPreviousForeigners = 0;

    // storing previous reference node positions to allow clearing lines
    static struct { t_u16 X; t_u16 Y; } etss_PreviousReferences[ETSS2_AMOUNT_REFERENCES];
    static t_u8                         etss_AmountPreviousReferences = 0;

    static BOOL etssd_FirstRun = TRUE;
    if ( etssd_FirstRun ) {
        ttc_memory_set( etss_PreviousForeigners, 0, sizeof( *etss_PreviousForeigners ) * ETSS2_AMOUNT_PREVIOUS_FOREIGNERS );
        ttc_memory_set( etss_PreviousReferences, 0, sizeof( *etss_PreviousReferences ) * ETSS2_AMOUNT_REFERENCES );
        etssd_FirstRun = FALSE;
    }
    if ( 1 ) { // clear previous foreigner positions and lines to reference nodes
        ttc_gfx_color_fg24( ets_ColorBg );
        for ( t_u8 Index = 0; Index < etss_AmountPreviousForeigners; Index++ ) {
            ttc_gfx_rect_fill( etss_PreviousForeigners[Index].X - 4,
                               etss_PreviousForeigners[Index].Y - 4,
                               8, 8
                             );
        }
        etss_AmountPreviousForeigners = 0;

        for ( t_u8 Index = 0; Index < etss_AmountPreviousReferences; Index++ ) {
            ttc_gfx_line( etss_PreviousForeigners[0].X,
                          etss_PreviousForeigners[0].Y,
                          etss_PreviousReferences[Index].X,
                          etss_PreviousReferences[Index].Y
                        );
        }
        etss_AmountPreviousReferences = 0;

    }

    if ( 1 ) { // display all slam nodes
        for ( t_u16 NodeIndex = Config_SLAM->Init.Amount_Nodes; NodeIndex > 0; NodeIndex-- ) {
            t_ttc_slam_node* Node = ttc_slam_get_node( 2, NodeIndex );
            t_u16 Gfx_X = Data->Gfx_CenterX + Node->Position.X * Data->Gfx_ScaleX;
            t_u16 Gfx_Y = Data->Gfx_CenterY - Node->Position.Y * Data->Gfx_ScaleY;

            if ( NodeIndex < 3 ) { // south- / northpole
                if ( NodeIndex == 1 )
                { ttc_gfx_color_fg24( ets_ColorSouthpole ); }
                else
                { ttc_gfx_color_fg24( ets_ColorNorthpole ); }

                ttc_gfx_rect_fill( Gfx_X - 3, Gfx_Y - 3, 6, 6 );
            }
            else {
                if ( NodeIndex & 1 ) {
                    ttc_gfx_color_fg24( ets_ColorEastnode );
                }
                else {
                    ttc_gfx_color_fg24( ets_ColorWestnode );
                }
                ttc_gfx_rect_fill( Gfx_X - 2, Gfx_Y - 2, 4, 4 );
            }
        }
    }
    if ( AmountForeigners > 0 ) { // display foreigners
        ttc_gfx_color_fg24( ets_ColorForeigner );
        if ( AmountForeigners <= ETSS2_AMOUNT_PREVIOUS_FOREIGNERS )
        { etss_AmountPreviousForeigners = AmountForeigners; }
        else
        { etss_AmountPreviousForeigners = ETSS2_AMOUNT_PREVIOUS_FOREIGNERS; }

        for ( t_u16 ForeignerIndex = 0; ForeignerIndex < etss_AmountPreviousForeigners; ForeignerIndex++ ) {
            t_s16 Foreigner_X = PositionForeigners[ForeignerIndex].X;
            t_s16 Foreigner_Y = PositionForeigners[ForeignerIndex].Y;
            if ( ( Foreigner_X > Config_SLAM->BoundingBox.X_Min ) &&
                    ( Foreigner_X < Config_SLAM->BoundingBox.X_Max ) &&
                    ( Foreigner_Y > Config_SLAM->BoundingBox.Y_Min ) &&
                    ( Foreigner_Y < Config_SLAM->BoundingBox.Y_Max )
               ) { // node inside bounding box: display foreigner
                Foreigner_X = Data->Gfx_CenterX + Foreigner_X * Data->Gfx_ScaleX;
                Foreigner_Y = Data->Gfx_CenterY - Foreigner_Y * Data->Gfx_ScaleY;

                ttc_gfx_circle_fill( Foreigner_X, Foreigner_Y, 3 );

                // store coordinate for fast screen clear next run
                etss_PreviousForeigners[ForeignerIndex].X = Foreigner_X;
                etss_PreviousForeigners[ForeignerIndex].Y = Foreigner_Y;

                if ( ForeignerIndex == 0 ) { // draw lines from first foreigner to reference points
                    ttc_gfx_color_fg24( ets_ColorFg );
                    for ( t_u8 ReferenceIndex = 0; ( ReferenceIndex < ETSS2_AMOUNT_REFERENCES ) && ( References[ReferenceIndex].NodeIndex > 0 ); ReferenceIndex++ ) {
                        t_ttc_slam_node* ReferenceNode = ttc_slam_get_node( 2, References[ReferenceIndex].NodeIndex );
                        t_u16 ReferenceNode_X = Data->Gfx_CenterX + ReferenceNode->Position.X * Data->Gfx_ScaleX;
                        t_u16 ReferenceNode_Y = Data->Gfx_CenterY - ReferenceNode->Position.Y * Data->Gfx_ScaleY;

                        ttc_gfx_line( Foreigner_X,
                                      Foreigner_Y,
                                      ReferenceNode_X,
                                      ReferenceNode_Y
                                    );

                        // store display coordinates of end of line for fast line clear on next call
                        etss_PreviousReferences[etss_AmountPreviousReferences].X = ReferenceNode_X;
                        etss_PreviousReferences[etss_AmountPreviousReferences].Y = ReferenceNode_Y;
                        etss_AmountPreviousReferences++;
                    }
                }
            }
        }
    }

    // back to normal fg-color
    ttc_gfx_color_fg24( ets_ColorFg );

    #endif
}

/** check if coordinates of given points match within given maximum difference
 *
 * @param Position1      first 3D position
 * @param Position2      second 3D position
 * @param MaxDifference  maximum allowed absolute difference between corresponding coordinate of both positions
 * @return ==TRUE: both positions match; ==FALSE: at least one absolute coordinate difference exceeds given maximum
 */
BOOL _etss_points_match( t_ttc_math_vector3d_xyz* Position1, t_ttc_math_vector3d_xyz* Position2, ttm_number MaxDifference ) {

    ttm_number DistanceX = ttc_math_distance( Position1->X, Position2->X );
    ttm_number DistanceY = ttc_math_distance( Position1->Y, Position2->Y );
    ttm_number DistanceZ = ttc_math_distance( Position1->Z, Position2->Z );

    return (
               ( DistanceX < MaxDifference ) &&
               ( DistanceY < MaxDifference ) &&
               ( DistanceZ < MaxDifference )
           );
}

t_base _etss_iteration( volatile t_base Count ) {
    // place breakpoint for certain loop iteration here:
    // b _etss_iteration if Count == 123
    return Count;
}

void _task_slam_simple_2d( void* Argument ) {
    Assert_Writable( Argument, ttc_assert_origin_auto );              // always check pointer arguments with Assert() before dereferencing them!
    example_slam_simple_2d_data_t* Data = ( example_slam_simple_2d_data_t* ) Argument; // we're expecting this pointer type

    // get configuration of already initialized slam device
    t_ttc_slam_config* Config_SLAM = ttc_slam_get_configuration( Data->IndexSLAM );
    ( void ) Config_SLAM; // yet unused

    _etsc2_gfx_printf1_at( 6, 2, "map:%d", Config_SLAM->Init.Amount_Nodes );
    t_u16 CounterNewReference = 0;

    t_ttc_math_vector3d_xyz  PositionsForeigner[3];
    t_ttc_math_vector3d_xyz* Foreigner_Real  = &( PositionsForeigner[0] ); // real position of a simulated foreign (mobile) node
    t_ttc_math_vector3d_xyz* Foreigner_SLAM1 = &( PositionsForeigner[1] ); // slam localized position of PositionsForeigner[0]
    t_ttc_math_vector3d_xyz* Foreigner_SLAM2 = &( PositionsForeigner[2] ); // slam localized position of PositionsForeigner[0]

    // set starting point
    Foreigner_Real->X = Foreigner_Real->Y = Foreigner_Real->Z = 0;

    const t_s16 MaxSpeedX = 50;
    const t_s16 MaxSpeedY = 50;
    t_s16 SpeedX = MaxSpeedX;
    t_s16 SpeedY = MaxSpeedY * 4.0 / 5;

    // distance measures to reference nodes
    t_ttc_slam_distance References[ETSS2_AMOUNT_REFERENCES];
    ttc_memory_set( References, 0, sizeof( References[0] ) * ETSS2_AMOUNT_REFERENCES );

    volatile t_base LoopCount = 0; // debugging aid

    #ifdef TTC_LED1
    ttc_gpio_init( TTC_LED1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    #endif

    while ( 1 ) {
        LoopCount = _etss_iteration( LoopCount++ );

        if ( 0 ) { // define fixed set of reference nodes
            References[0].NodeIndex = 2; //7;
            References[1].NodeIndex = 1; //12;
        }
        else {     // switch reference nodes randomly
            if ( CounterNewReference-- == 0 ) { // select new reference points for distance measure

                // first random index is simple
                References[0].NodeIndex = 1 + ( ttc_random_generate() % Config_SLAM->Init.Amount_Nodes );

                // select more unique random indices
                for ( t_u8 ReferenceIndex = 1; ReferenceIndex < 2; ReferenceIndex++ ) {

                    BOOL ReferenceIsNew;
                    do { // find a new reference node
                        References[ReferenceIndex].NodeIndex = 1 + ( ttc_random_generate() % Config_SLAM->Init.Amount_Nodes );

                        // check if random node index is new
                        ReferenceIsNew = TRUE;
                        for ( t_u8 ReferenceIndex2 = 0; ReferenceIndex2 < ReferenceIndex; ReferenceIndex2++ ) {
                            if ( References[ReferenceIndex2].NodeIndex == References[ReferenceIndex].NodeIndex ) {
                                ReferenceIsNew = FALSE;
                                break;
                            }
                        }
                    }
                    while ( !ReferenceIsNew );
                }
                CounterNewReference = 100;
            }
        }

        // simulate distance measures from foreigner to reference nodes and localize foreigner
        t_u8 AmountMeasures = 0;
        for ( t_u8 Index = 0; Index < ETSS2_AMOUNT_REFERENCES; Index++ ) { // measure distances to all nodes in References[]
            if ( References[Index].NodeIndex > 0 ) {
                t_ttc_slam_node* ReferenceNode = ttc_slam_get_node( Data->IndexSLAM, References[Index].NodeIndex );
                References[Index].Distance = ttc_math_distance_2d( Foreigner_Real->X, Foreigner_Real->Y,
                                                                   ReferenceNode->Position.X, ReferenceNode->Position.Y
                                                                 );
                AmountMeasures++;
            }
        }

        #ifdef TTC_LED1
        ttc_gpio_set( TTC_LED1 ); // set LED1 to indicate start of a localization
        #endif

        // use distances to reference nodes in second slam instance to laterate coordinates of test node
        // (gdb) p ttc_slam_localize_foreigner(Data->IndexSLAM,Foreigner_SLAM1,Foreigner_SLAM2,References,AmountMeasures)
        volatile e_ttc_math_errorcode Error = ttc_slam_localize_foreigner(
                    Data->IndexSLAM,
                    Foreigner_SLAM1,     // where to store first localized position
                    Foreigner_SLAM2,     // where to store first localized position
                    References,          // simulated distance measures from foreigner to selected reference nodes
                    AmountMeasures
                    );

        ttc_assert_test( Error == 0); // Could not localize foreigner. Check implementation of low-level driver!
        #ifdef TTC_LED1
        ttc_gpio_clr( TTC_LED1 ); // clear LED1 to indicate successfull end of a localization
        #endif

        ttc_task_msleep( 20 );
        _etss_display( Data, PositionsForeigner, 2, References );

        if ( 1 ) { // check if coordinates of localized foreigner node fit to real position
            if (
                ( !_etss_points_match( Foreigner_Real, Foreigner_SLAM1, Data->MaxDifference ) ) &&
                ( !_etss_points_match( Foreigner_Real, Foreigner_SLAM2, Data->MaxDifference ) )
            ) {
                #ifdef TTC_GFX1 // display current configuration
                t_ttc_slam_node* RefNode0 = ttc_slam_get_node( 1, References[0].NodeIndex );
                t_ttc_slam_node* RefNode1 = ttc_slam_get_node( 1, References[1].NodeIndex );

                _etss_gfx_item( Data, etssi_ArrowA2B,
                                RefNode0->Position.X, RefNode0->Position.Y,
                                RefNode1->Position.X, RefNode1->Position.Y
                              );

                ttc_gfx_color_fg24( ets_ColorForeigner );

                // line Origin->second laterazed location
                _etss_gfx_item( Data, etssi_Line, RefNode0->Position.X, RefNode0->Position.Y, Foreigner_SLAM1->X, Foreigner_SLAM1->Y );

                // visualize first laterated location
                _etss_gfx_item( Data, etssi_Circle, Foreigner_SLAM2->X, Foreigner_SLAM2->Y, 4 / Data->Gfx_ScaleX, 0 );
                _etss_gfx_item( Data, etssi_Line, RefNode0->Position.X, RefNode0->Position.Y, Foreigner_SLAM2->X, Foreigner_SLAM2->Y );
                #endif
                Error = ttc_slam_localize_foreigner( Data->IndexSLAM, Foreigner_SLAM1, Foreigner_SLAM2, References, AmountMeasures );

                ttc_assert_halt_origin( ttc_assert_origin_auto ); // Laterated point still does not match real one. Check implementation of low-level slam driver!
                #ifdef TTC_GFX1
                ttc_gfx_clear();
                #endif
            }
        }
        if ( 1 ) { // simulate moving foreigner
            Foreigner_Real->X += SpeedX;
            Foreigner_Real->Y += SpeedY;
            t_ttc_slam_box* Box = & ttc_slam_get_configuration( Data->IndexSLAM )->BoundingBox;

            if ( ( Foreigner_Real->X > Box->X_Max ) ) { // reflect foreigner at ríght boundary
                Foreigner_Real->X = Box->X_Max;

                goto X_Reflect;
            }
            if ( ( Foreigner_Real->X < Box->X_Min ) ) { // reflect foreigner at ríght boundary
                Foreigner_Real->X = Box->X_Min;

            X_Reflect:
                SpeedX *= -1;
                Foreigner_Real->X += ( SpeedX >> 3 );
                SpeedY += ( ttc_random_generate() % ( MaxSpeedY / 10 ) ) - ( MaxSpeedY / 20 );
                if ( SpeedY > MaxSpeedY )
                { SpeedY = MaxSpeedY; }

                ttc_assert_test( ( Foreigner_Real->X <= Box->X_Max ) &&
                                 ( Foreigner_Real->X >= Box->X_Min )
                               ); // x-coordinate corrupt. Check implementation! Maybe Memory corruption?
            }
            if ( ( Foreigner_Real->Y > Box->Y_Max ) ) { // reflect foreigner at lower boundary
                Foreigner_Real->Y = Box->Y_Max;
                goto Y_Reflect;
            }
            if ( ( Foreigner_Real->Y < Box->Y_Min ) ) { // reflect foreigner at upper boundary
                Foreigner_Real->Y = Box->Y_Min;

            Y_Reflect:
                SpeedY *= -1;
                Foreigner_Real->Y += SpeedY;
                SpeedX += ( ttc_random_generate() % ( MaxSpeedX / 10 ) ) - ( MaxSpeedX / 20 );
                if ( SpeedX > MaxSpeedX )
                { SpeedX = MaxSpeedX; }

                ttc_assert_test( ( Foreigner_Real->Y <= Box->Y_Max ) &&
                                 ( Foreigner_Real->Y >= Box->Y_Min )
                               ); // x-coordinate corrupt. Check implementation! Maybe Memory corruption?
            }
            if ( ( Foreigner_Real->Y > Box->Y_Max ) || // reflect foreigner at ríght boundary
                    ( Foreigner_Real->Y < Box->Y_Min )
               ) { // reflect foreigner at uppoer or lower boundary
                SpeedY *= -1;
                Foreigner_Real->Y += ( SpeedY >> 3 );

                SpeedX += ( ttc_random_generate() % ( MaxSpeedX / 10 ) ) - ( MaxSpeedX / 20 );
                if ( SpeedX > MaxSpeedX )
                { SpeedX = MaxSpeedX; }

                ttc_assert_test( ( Foreigner_Real->Y <= Box->Y_Max ) &&
                                 ( Foreigner_Real->Y >= Box->Y_Min )
                               ); // x-coordinate corrupt. Check implementation! Maybe Memory corruption?
            }
        }

    }
}
//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_slam_simple_2d_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    // set precision of approximation algorithms (all trigonometric functions)
    ttc_math_set_precision( tmp_10m );

    // Note: Adding a unique prefix to all global and static variable makes it
    //       easier to get an overview of memory usage via _/calculateMemoryUsage.pl
    static example_slam_simple_2d_data_t et_slam_simple_2d_Data;

    // initialize gfx display (if available)
    _etsc2_gfx_init( &et_slam_simple_2d_Data );

    // initialize pseudo random number generator
    ttc_random_seed( 23 );

    if ( 1 ) { // generate test setup to check implementation of low-level driver
        const t_s32 d = 1000;
        et_slam_simple_2d_Data.MaxDifference = 100;

        if ( 1 ) { // create slam instance #1 with nodes at known coordinates
            // get configuration of slam device
            t_ttc_slam_config* Config_SLAM = ttc_slam_get_configuration( 1 );

            // change configuration before initializing device
            Config_SLAM->Init.Amount_Nodes = 25; // 12 east nodes + 11 west nodes + north pole + south pole

            // initializing our slam device here makes debugging easier (still single-tasking)
            ttc_slam_init( 1 );

            // set coordinates of all nodes according to above drawing
            /* Generate coordinates for 12 east, 11 west nodes, one south and one north pole.
             *
             * For a predefined constant d, provided distances should result in a node distribution like this:
             *
             * South Pole:
             *   O = N1 = (0,0)
             *
             * North Pole:
             *   F = N2 = (0,d)  fixpoint north of O
             *
             * East Nodes:
             *  N3=(0,d/2),     N5=(d/2,0), N7=(d,0),   N9=(d,d/2), N11=(d,-d/2), N13=(d/2,d/2),
             * N15=(d/2,-d/2), N17=(d,d),  N19=(d,-d), N21=(d/2,d), N23=(d/2,-d), N25=(0,-d)
             *
             * West Nodes:
             *  N4=(0,-d/2),     N6=(-d/2,0),  N8=(-d,0),  N10=(-d,d/2), N12=(-d,-d/2), N14=(-d/2,d/2),
             * N16=(-d/2,-d/2), N18=(-d,d),   N20=(-d,-d), N22=(-d/2,d), N24=(-d/2,-d)
             *
             *                           Y
             *                          A
             *                          |
             *            N18           | F=N2      N17
             *               \          |/         /
             *                O    O    O    O    O
             *                    /     |     \
             *                 N22      |      N21
             *            N10           | N3        N9
             *               \          |/         /
             *                O    O    O    O    O
             *                    /     |     \
             *                  N14     |      N13
             *             N8           | O=N1      N7
             *               \          |/         /
             *            <---O----O----O----O----O--->
             *                    /     |     \        X
             *                  N6      |      N5
             *            N12           |           N11
             *               \          |          /
             *                O    O    O    O    O
             *                    /     |\    \
             *                  N16     | N4   N15
             *            N20           |           N19
             *               \          |          /
             *                O    O    O    O    O
             *                    /     |\    \
             *                 N24      | N25  N23
             *                          V
             */

            // South Pole
            ttc_slam_update_coordinates( 1, 1, 0, 0, 0 );

            // North Pole
            ttc_slam_update_coordinates( 1, 2, 0, d, 0 );

            // East Nodes
            ttc_slam_update_coordinates( 1,  3,  d,          0, 0 );
            ttc_slam_update_coordinates( 1,  5,  d / 2,      0, 0 );
            ttc_slam_update_coordinates( 1,  7,  0,      d / 2, 0 );
            ttc_slam_update_coordinates( 1,  9,  d,      d / 2, 0 );
            ttc_slam_update_coordinates( 1, 11,  d,     -d / 2, 0 );
            ttc_slam_update_coordinates( 1, 13,  d / 2,  d / 2, 0 );
            ttc_slam_update_coordinates( 1, 15,  d / 2, -d / 2, 0 );
            ttc_slam_update_coordinates( 1, 17,  d,          d, 0 );
            ttc_slam_update_coordinates( 1, 19,  d,         -d, 0 );
            ttc_slam_update_coordinates( 1, 21,  d / 2,      d, 0 );
            ttc_slam_update_coordinates( 1, 23,  d / 2,     -d, 0 );
            ttc_slam_update_coordinates( 1, 25,  0,         -d, 0 );

            // West Nodes
            ttc_slam_update_coordinates( 1,  4,  0,     -d / 2, 0 );
            ttc_slam_update_coordinates( 1,  6, -d / 2,      0, 0 );
            ttc_slam_update_coordinates( 1,  8, -d,          0, 0 );
            ttc_slam_update_coordinates( 1, 10, -d,      d / 2, 0 );
            ttc_slam_update_coordinates( 1, 12, -d,     -d / 2, 0 );
            ttc_slam_update_coordinates( 1, 14, -d / 2,  d / 2, 0 );
            ttc_slam_update_coordinates( 1, 16, -d / 2, -d / 2, 0 );
            ttc_slam_update_coordinates( 1, 18, -d,          d, 0 );
            ttc_slam_update_coordinates( 1, 20, -d,         -d, 0 );
            ttc_slam_update_coordinates( 1, 22, -d / 2,      d, 0 );
            ttc_slam_update_coordinates( 1, 24, -d / 2,     -d, 0 );
        }
        if ( 1 ) { // create slam instance #2 and feed it with distances from instance #1
            et_slam_simple_2d_Data.IndexSLAM = 2;

            // get configuration of slam device
            t_ttc_slam_config* Config_SLAM = ttc_slam_get_configuration( et_slam_simple_2d_Data.IndexSLAM );

            t_u32 BytesFreeBefore = ttc_heap_get_free_size(); // amount of free heap size before initializing slam instance

            // configure same amount of nodes
            Config_SLAM->Init.Amount_Nodes = ttc_slam_get_configuration( 1 )->Init.Amount_Nodes;

            // initializing our slam device here makes debugging easier (still single-tasking)
            ttc_slam_init( et_slam_simple_2d_Data.IndexSLAM );

            _etsc2_gfx_printf1_at( 0, 1, "n=%d",     Config_SLAM->Init.Amount_Nodes );
            _etsc2_gfx_printf1_at( 5, 1, "%d dists", ttc_semaphore_available( & Config_SLAM->Pool_Distances.BlocksAvailable ) );

            // report all distances among nodes from instance #1 to instance #2
            for ( t_u16 NodeIndexA = 1; NodeIndexA <= Config_SLAM->Init.Amount_Nodes; NodeIndexA++ ) {
                for ( t_u16 NodeIndexB = 1; NodeIndexB <= Config_SLAM->Init.Amount_Nodes; NodeIndexB++ ) {
                    if ( NodeIndexA != NodeIndexB ) {
                        ttc_slam_update_distance( 2,
                                                  NodeIndexA, NodeIndexB,
                                                  ttc_slam_calculate_distance( 1, NodeIndexA, NodeIndexB )
                                                );
                    }
                }
            }

            // mapping can now be calculated
            BOOL MappingComplete = ttc_slam_calculate_mapping( 2 );
            Assert( MappingComplete, ttc_assert_origin_auto ); // check implementation of low-level ttc_slam driver!

            // compare calculated mapping with original coordinates
            for ( t_u16 NodeIndex = 1; NodeIndex <= Config_SLAM->Init.Amount_Nodes; NodeIndex++ ) {
                t_ttc_slam_node* Node1 = ttc_slam_get_node( 1, NodeIndex );
                t_ttc_slam_node* Node2 = ttc_slam_get_node( 2, NodeIndex );

                // check if coordinates have been calculated with required precision
                _etss_within( Node1->Position.X, Node2->Position.X, CoordinatePrecision );
                _etss_within( Node1->Position.Y, Node2->Position.Y, CoordinatePrecision );
                _etss_within( Node1->Position.Z, Node2->Position.Z, CoordinatePrecision );
            }

            _etsc2_gfx_printf1_at( 0, 2, "%db", BytesFreeBefore - ttc_heap_get_free_size() );
        }
    }

    ttc_task_create( _task_slam_simple_2d,       // function to start as thread
                     "tSLAM_SIMPLE_2D",          // thread name (just for debugging)
                     256,                        // stack size (adjust to amount of local variables of _task_slam_simple_2d() AND ALL ITS CALLED FUNTIONS!)
                     &et_slam_simple_2d_Data,    // passed as argument to _task_slam_simple_2d()
                     1,                          // task priority (higher values mean more process time)
                     NULL                        // can return a handle to created task
                   );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}Function Definitions
