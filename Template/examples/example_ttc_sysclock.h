#ifndef EXAMPLE_SYSCLOCK_H
#define EXAMPLE_SYSCLOCK_H

/** example_ttc_SYSCLOCK.h **********************************************{
 *
 *                              The ToolChain
 *
 * Empty template for new header files.
 * Copy and adapt to your needs.
 *
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_sysclock.h"
#include "../ttc-lib/ttc_systick.h"  // exact delays and timeouts

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *  - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_sysclock_prepare();

/** task running this example
 *
 * @param TaskArgument  not used
 */
void task_SYSCLOCK( void* TaskArgument );


/** Task showing constant msleep function at all system clock frequencies.
 *
 * @param TaskArgument  not used
 */
void task_SYSCLOCK_Delay( void* TaskArgument );

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_SYSCLOCK_H
