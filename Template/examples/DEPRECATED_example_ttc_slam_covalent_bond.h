#ifndef EXAMPLE_SLAM_H
#define EXAMPLE_SLAM_H

/** example_ttc_SLAM.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2016.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_slam.
 *  The basic flow is as follows:
 *  (1) example_ttc_slam_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_slam_prepare() gets executed
 *
 *  Created from template example_ttc_device.h revision 13 at 20160606 11:56:00 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile example_ttc_SLAM.c only do not belong here!
#include "../ttc-lib/ttc_basic.h"

#ifdef TTC_GFX1
#include "../ttc-lib/ttc_gfx.h"
#endif

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_slam_t
    u8_t LogicalIndex;  // logical index of device to use

    #ifdef TTC_GFX1
    ttc_gfx_config_t* GFX;  // configuration of graphic display to use
    #endif

    // more arguments...
} example_slam_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_slam_covalent_bond_prepare();


//}Function prototypes

#endif //EXAMPLE_SLAM_H
