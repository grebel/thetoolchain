﻿/** { example_ttc_rtls_crtof_simple_2d_anchor.c ************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.c revision 11 at 20161130 06:10:02 UTC
 *
 *  Authors: gregor
}*/

/** { Includes ********************************************************
 *
 * Put all includes here that are required to compile this source code.
 */

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_rtls_crtof_simple_2d_anchor.h"
#include "example_ttc_radio_ranging_common.h"
#include "../ttc-lib/ttc_radio.h"
#include "../ttc-lib/ttc_string.h"
#include "../ttc-lib/ttc_slam.h"
#include "../ttc-lib/ttc_rtls.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Global Variables ************************************************
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** { Private Function Declarations ***********************************
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** { Function Definitions ********************************************
 */

void example_ttc_rtls_crtof_simple_2d_prepare_anchor( t_example_ttc_rtls_crtof_simple_2d_data* DataRTLS ) {
    // whoami?
    ttc_radio_change_local_address( 1,
                                    DataRTLS->Common.RadioUserID,
                                    DataRTLS->Common.LocalNodeID
                                  );

    // enable frame filter (only certain types of frames can pass)
    ttc_radio_configure_frame_filter( 1,
                                      DataRTLS->Common.RadioUserID,
                                      ttc_radio_frame_filter_coordinator // receive ACKs + beacons + broadcasts + all packets in same PAN
                                    );

    if ( 1 ) { // allow radio access to our location vector (simply using location vector from ttc_slam)
        t_u16 MyNodeIndex = DataRTLS->Common.LocalNodeID->Address16;

        // get access to node in slam model that represents current node
        ttc_assert_test( DataRTLS->IndexSLAM > 0 ); // invalid/ uninitialized pointer value. Check implementation!
        t_ttc_slam_node* MyNode = ttc_slam_get_node( DataRTLS->IndexSLAM, MyNodeIndex );

        // When replying to incoming localization requests, radio_common_ranging_reply_isr()
        // will read our 3D position directly from ttc_slam structures.
        DataRTLS->Common.ConfigRadio->Init.MyLocation = &( MyNode->Position );

        // ensure that both datatypes match (one may have been defined to different sized type)
        Assert( sizeof( MyNode->Position.X ) == sizeof( DataRTLS->Common.ConfigRadio->Init.MyLocation->X ), ttc_assert_origin_auto ); // ttc_slam uses a different sized data format for coordinates. Adjust one of them to match!
    }

    /** Set individual ranging reply delay for each anchor node.
     *  When a sensor node sends a ranging request as broadcast, every anchor node will
     *  answer after a different delay. This avoids reply collisions.
     *
     *  DELAY_MINIMUM = The minimum reply delay
     *  DELAY_SPACING = Minimum delay spacing (time between two incoming consecutive reply frames at requesting node.
     *                  DELAY_SPACING increase with greater distance variations.
     */
    // Delay constants as measured experimentally for STM32L100 @32MHz
    // (may need adjustment for your microcontroller)
#define DELAY_SPACING 900
#define DELAY_MINIMUM 1400
    ttc_radio_set_ranging_delay( 1,
                                 DataRTLS->Common.RadioUserID,
                                 DELAY_MINIMUM + ( DataRTLS->Common.LocalNodeID->Address16 - 1 ) * DELAY_SPACING
                               );

    example_ttc_radio_ranging_usart_send_stringf( "starting rtls anchor node #%d ", DataRTLS->Common.ConfigRadio->LocalID.Address16 );
    example_ttc_radio_ranging_usart_send_stringf( "on channel %d\n", DataRTLS->Common.ConfigRadio->ChannelRX );

    t_ttc_packet_address TargetID;

    if ( 1 ) { // must send at least single packet to enable autoacknowledge feature of DW1000 (seen on DecaWave mailing list)
        ttc_string_snprintf( DataRTLS->Common.PrepareBuffer, DataRTLS->Common.PrepareBuffer_Size, "New Anchor 0x%04x", DataRTLS->Common.LocalNodeID->Address16 );
        TargetID.Address16 = 0xffff; // broadcast address

        example_ttc_radio_ranging_transmit( &( DataRTLS->Common ),
                                            DataRTLS->Common.PrepareBuffer,
                                            ttc_string_length8( ( char* ) DataRTLS->Common.PrepareBuffer, DataRTLS->Common.PrepareBuffer_Size ) + 1, // transmit string + string terminator
                                            &TargetID,
                                            E_ttc_packet_type_802154_Data_001010,
                                            &( DataRTLS->Common.StringBuffer[0] ),
                                            SIZE_ETR_STRINGBUFFER
                                          );
    }

    /** An anchor node will wait for directed packets from sensor nodes and reply to them individually.
     *
     */
    example_ttc_radio_ranging_usart_send_string( "waiting for sensor nodes\n", -1 );
    ttc_radio_receiver( 1, DataRTLS->Common.RadioUserID, 1, 0, 0 );  // enable receiver

#if (EXAMPLE_TTC_RADIO_NODE_INDEX == 1) // this is southpole: try to measure range to northpole
#endif

    ttc_task_create(
        example_ttc_rtls_crtof_simple_2d_task_anchor, // function to start as thread
        "tAnchor",             // thread name (just for debugging)
        512,                   // stack size
        DataRTLS,              // passed as argument to example_ttc_rtls_crtof_simple_2d_mainTask()
        1,                     // task priority (higher values mean more process time)
        NULL                   // can return a handle to created task
    );
}
void example_ttc_rtls_crtof_simple_2d_task_anchor( void* TaskArgument ) { // implementation of anchor node task
    t_example_ttc_rtls_crtof_simple_2d_data* volatile TaskInfo = ( t_example_ttc_rtls_crtof_simple_2d_data* ) TaskArgument;

    t_u8 IndexRadio  = TaskInfo->Common.ConfigRadio->LogicalIndex;
    t_u8 RadioUserID = TaskInfo->Common.RadioUserID;
    do {
        if ( TaskInfo->Config_RTLS ) { // got a rtls configuration: call rtls statemachine regularly to handle protocol
            ttc_rtls_statemachine( TaskInfo->Config_RTLS );
        }
        else {                         // no ttc_rtls available: simply ignore all incoming rtls messages (ranging requests are handled by radio_common automatically)
            ttc_radio_packets_received_ignore( IndexRadio, TaskInfo->Common.RadioUserID, E_ttc_packet_pattern_socket_AnyOf_Radio );
        }

        // no application in this example: simply ignore all non radio protocol types to ensure
        // that no packet buffer is stuck in ListRX
        ttc_radio_packets_received_ignore( IndexRadio, RadioUserID, E_ttc_packet_pattern_socket_AnyOf_NonRadio );

        // allow radio to check its current state
        ttc_radio_maintenance( IndexRadio, RadioUserID );

        // give cpu time to other tasks (auto removed in single tasking)
        ttc_task_yield();
    }
    while ( TTC_TASK_SCHEDULER_AVAILABLE );
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
