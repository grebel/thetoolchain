/** { example_ttc_radio_ranging_observer.c ************************************************
 *
 *                          The ToolChain
 *
 *  Created from template ttc-lib/templates/new_file.c revision 11 at 20161130 06:10:15 UTC
 *
 *  Authors: gregor
}*/

/** { Includes ********************************************************
 *
 * Put all includes here that are required to compile this source code.
 */

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_radio_ranging_observer.h"
#include "example_ttc_radio_ranging_common.h"
#include "../ttc-lib/ttc_radio.h"
#include "../ttc-lib/ttc_string.h"
//InsertIncludes above (DO NOT REMOVE THIS LINE!)
//}Includes
/** { Global Variables ************************************************
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** { Private Function Declarations ***********************************
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** { Function Definitions ********************************************
 */

void example_ttc_radio_ranging_prepare_observer( etr_task_info_t* TaskInfo ) {
    // receive any kind of packet (aka promiscous mode)
    ttc_radio_configure_frame_filter( 1, TaskInfo->RadioUserID, ttc_radio_frame_filter_disable );

    ttc_task_create(
        example_ttc_radio_ranging_task_node_observer, // function to start as thread
        "tObserver",                // thread name (just for debugging)
        512,                        // stack size
        TaskInfo,                   // passed as argument to example_ttc_radio_ranging_mainTask()
        1,                          // task priority (higher values mean more process time)
        NULL                        // can return a handle to created task
    );
}
void example_ttc_radio_ranging_task_node_observer( void* TaskArgument ) { // implementation of observer node task
    etr_task_info_t* TaskInfo = ( etr_task_info_t* ) TaskArgument;

    example_ttc_radio_ranging_usart_send_stringf( "starting observer on channel %d\n", TaskInfo->ConfigRadio->ChannelRX );

    /* DEPRECATED
    // calculate size of buffer that can store biggest packet + META header
    t_u16 BufferSize = sizeof( t_ttc_packet_meta ) + TaskInfo->ConfigRadio->Init.MaxPacketSize;

    // ttc_packet only accepts memory buffers from a memory pool
    // create a pool for some data-packets
    t_ttc_heap_pool* Pool_Data = ttc_heap_pool_create( BufferSize, 2 );

    // Initialize all buffers in this pool to same type
    // ttc_radio will return all blocks to their original pool. So we don't have to
    // reinitialize the blocks later.
    example_ttc_radio_ranging_initialize_pool( Pool_Data, E_ttc_packet_type_802154_Data_011010 );
    */

    const t_u16 Interleave = 10000; // amount of while loops to pass until status message is updated
    t_u16 Count = 0;
    t_u16 MessageCounter = 0;
    BOOL LineFeedRequired = FALSE;
    t_u8 RadioUserID = TaskInfo->RadioUserID;

    // initialize data for ttc_radio_packet_received_tryget() calls to receive all types of application packets
    TaskInfo->SocketJob.Init.Pattern    = E_ttc_packet_pattern_socket_AnyOf_Application;
    TaskInfo->SocketJob.Init.Initialized = FALSE;

    while ( 1 ) {

        // check for application type packets
        t_ttc_packet* ReceivedPacket = ttc_radio_packet_received_tryget( 1, RadioUserID,  &( TaskInfo->SocketJob ) );
        if ( ReceivedPacket ) { // somebody wants to talk to us
            if ( 1 )
            { ttc_radio_receiver( 1, RadioUserID, TRUE, 0, 0 ); }  // re-enable receiver (is automatically disabled after receiving frame)

            if ( 1 ) { // debug received packet via usart
                MessageCounter++;
                t_u8* Append = &( TaskInfo->StringBuffer[0] );
                t_base StringBufferFree = SIZE_ETR_STRINGBUFFER;
                if ( LineFeedRequired ) { // some points have been printed: start in new line
                    Append += ttc_string_appendf( Append, &StringBufferFree, "\n" );
                    LineFeedRequired = FALSE;
                }

                Append += ttc_string_appendf( Append, &StringBufferFree, "RX #%04d: ", MessageCounter );
                Append += ttc_packet_debug( ReceivedPacket, Append, &StringBufferFree, TaskInfo->PacketDebug );
                Append += ttc_string_appendf( Append, &StringBufferFree, "\n" );
                example_ttc_radio_ranging_usart_send_string( ( char* ) & ( TaskInfo->StringBuffer[0] ), SIZE_ETR_STRINGBUFFER );
            }

            // return packet memory buffer to radio for reuse
            ttc_radio_packet_release( ReceivedPacket );
        }

        ttc_radio_maintenance( 1, RadioUserID ); // let radio cleanup buffers and check transceiver state

        if ( !ReceivedPacket ) { // no packet received: maybe its time to display an update
            if ( Count-- == 0 ) { // print status update
                Count = Interleave;

                example_ttc_radio_ranging_usart_send_string( ".", -1 );
                LineFeedRequired = TRUE;
            }
            if ( 1 ) { ttc_task_usleep( 1000 ); }
        }
    }
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
