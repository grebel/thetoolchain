/** example_ttc_heap.c **********************************************{
 *
 *                             The ToolChain
 *
 *
 * example_ttc_heap_prepare()
 *     fills et_heap_TaskArguments with data
 *     spawns thread task_HEAP()
 *     returns immediately
 *
 * task_HEAP()
 *     while (1) {
 *       do something
 *       ttc_task_msleep(100);
 *     }
 *
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_heap.h"

//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
example_heap_t et_heap_TaskArguments;

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

/** Gets memory blocks from memory pool and appends them at end of single linked list.
 *
 * This task will allocate memory blocks from pool faster than task_Processor() can process them.
 * The pool will run empty soon. When the pool is empty, task_Sender() will be throttled automatically
 * (it sleeps inside ttc_heap_pool_block_get().
 */
void task_Sender( void* TaskArgument );

/** Receives memory blocks from single linked list.
 *
 * Receives one item from list every 100 ms and returns it back to its memory pool.
 */
void task_Processor( void* TaskArgument );

typedef struct { // example data being stored in each list item
    // This struct has no next pointer.
    // The next pointer is inherited from the memory pool block and located directly before
    // this struct. Use ttc_heap_pool_to_list_item() to access the next pointer.

    t_u8  Amount;
    t_u16 B[10];
} eth_data_t;

void example_ttc_heap_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    // create a new memory pool of 20 equal sized blocks
    et_heap_TaskArguments.Pool = ttc_heap_pool_create( sizeof( eth_data_t ), 20 );
    et_heap_TaskArguments.List = ttc_list_create( "PoolBlocks" );

    ttc_task_create( task_Sender,               // function to start as thread
                     "tSend",                 // thread name (just for debugging)
                     128,                         // stack size
                     &et_heap_TaskArguments,  // passed as argument to task_HEAP()
                     1,                           // task priority (higher values mean more process time)
                     NULL                         // can return a handle to created task
                   );
    ttc_task_create( task_Processor,               // function to start as thread
                     "tSend",                 // thread name (just for debugging)
                     128,                         // stack size
                     &et_heap_TaskArguments,  // passed as argument to task_HEAP()
                     1,                           // task priority (higher values mean more process time)
                     NULL                         // can return a handle to created task
                   );
}
void task_Sender( void* TaskArgument ) {
    Assert_Writable( TaskArgument, ttc_assert_origin_auto );                // always check pointer arguments with Assert()!
    example_heap_t* TaskArguments = ( example_heap_t* ) TaskArgument; // we're expecting this pointer type

    t_u16 Counter = 0;

    while ( 1 ) {

        // generate single block of data + append it to list
        t_ttc_heap_block_from_pool* Block = ttc_heap_pool_block_get( TaskArguments->Pool );
        eth_data_t* Data = ( eth_data_t* ) Block;
        Data->B[0] = Counter++;
        Data->Amount = 1;
        ttc_list_push_back_single( TaskArguments->List, ttc_heap_pool_to_list_item( Block ) );

        // generate five single-linked, new blocks + append it to List

        t_ttc_list_item* ListHead = NULL;
        t_ttc_list_item* ListEnd  = NULL;

        for ( t_u8 I = 0; I < 5; I++ ) { // generate a single linked list of five items

            // will sleep if pool is empty
            Block = ttc_heap_pool_block_get( TaskArguments->Pool );

            // pool blocks can directly be casted to anything
            Data = ( eth_data_t* ) Block;
            Data->B[0] = Counter++;
            Data->Amount = 1;

            // every block from pool can be converted into a list item
            t_ttc_list_item*  Item = ttc_heap_pool_to_list_item( Block );

            if ( ListHead == NULL ) { // first item: set as head of list
                ListEnd = ListHead = Item;
                ListHead->Next = NULL; // make sure that list is always zero terminated
            }
            else {                  // list head already exists: append after last item
                ListEnd->Next = Item;
                ListEnd = Item;
                ListEnd->Next = NULL; // make sure that list is always zero terminated
            }
        }
        ttc_list_push_back_multiple( TaskArguments->List, ListHead, ListEnd, 5 );

        ttc_task_msleep( 100 );  // always sleep for a while to give cpu time to other tasks
    }
}
void task_Processor( void* TaskArgument ) {
    Assert_Writable( TaskArgument, ttc_assert_origin_auto );                // always check pointer arguments with Assert()!
    example_heap_t* TaskArguments = ( example_heap_t* ) TaskArgument; // we're expecting this pointer type

    while ( 1 ) {
        // sleep on list until data is pushed
        t_ttc_list_item* Item = ttc_list_pop_front_single_wait( TaskArguments->List );

        if ( Item ) {
            // do something with your data...

            // we know that this list item came from a memory pool
            t_ttc_heap_block_from_pool* Block = ttc_heap_pool_from_list_item( Item );

            // return memory block to its pool for reuse
            ttc_heap_pool_block_free( Block );
        }
        ttc_task_msleep( 100 );  // always sleep for a while to give cpu time to other tasks
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
