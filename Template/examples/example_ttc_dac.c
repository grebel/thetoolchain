/** example_ttc_dac.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_dac.
 *  The basic flow is as follows:
 *  (1) example_ttc_dac_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_dac_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 11 at 20141215 10:19:15 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_dac.h"
#include "../ttc-lib/ttc_dma.h"
#include "../ttc-lib/dac/dac_stm32l1xx.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_sysclock.h"
#include "stm32l1xx.h"
#include "../additionals/250_stm_std_peripherals/inc/stm32l1xx_tim.h"
#include "../additionals/250_stm_std_peripherals/inc/stm32l1xx_gpio.h"
#include "../additionals/250_stm_std_peripherals/inc/stm32l1xx_dac.h"
#include "../additionals/250_stm_std_peripherals/inc/stm32l1xx_dma.h"


//{ Global variables

// Note: Global and static variables require a unique prefix to avoid
//       conflicting symbol names when combined with other sources!
example_dac_t et_dac_TaskArguments;

t_ttc_dma_config *Config_dma1;
t_ttc_dac_config *Config_dac1;

t_ttc_dma_config *Config_dma2;
t_ttc_dac_config *Config_dac2;

t_u8 DmaIndex1 = 1;
t_u8 DmaIndex2 = 2;

t_u8 DacIndex1 = 1;
t_u8 DacIndex2 = 2;

#define WAVE_FREQ 5000
#define WAVE_POINTS 100
#define TIMER_PERIOD 10
#define DHR12RD_Address 0x40007420
t_u16 wave_array[100];
t_u16 j;
float pi_greco = 3.14159;
int timervalue;


const t_u16 Sine12bit[32] = {
                      2047, 2447, 2831, 3185, 3498, 3750, 3939, 4056, 4095, 4056,
                      3939, 3750, 3495, 3185, 2831, 2447, 2047, 1647, 1263, 909,
                      599, 344, 155, 38, 0, 38, 155, 344, 599, 909, 1263, 1647};

const t_u8 Escalator8bit[6] = {0x0, 0x33, 0x66, 0x99, 0xCC, 0xFF};

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_dac_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */
   et_dac_TaskArguments.Foo = 1; // set some arguments to be passed to task

       for(j=0; j<100; j++){

        wave_array[j] = (ut_int16)(2000 + 1500*math_sin((2*pi_greco*j)/100));
    }

    DMA_initialize();
    DAC_initialize();
    TIM2_initialize();
    
   ttc_task_create( task_DAC,               // function to start as thread
                    "tDAC",                 // thread name (just for debugging)
                    128,                         // stack size
                    &et_dac_TaskArguments,  // passed as argument to task_DAC()
                    1,                           // task priority (higher values mean more process time)
                    NULL                         // can return a handle to created task
                  );
}
void DAC_initialize(void) {


    ttc_gpio_init(E_ttc_gpio_pin_a5,E_ttc_gpio_mode_analog_in,E_ttc_gpio_speed_max);

    Config_dac1 = ttc_dac_get_configuration(DacIndex1);

    Config_dac1->LowLevelConfig->TriggerType = DAC_Trigger_T2_TRGO;
    Config_dac1->LowLevelConfig->WaveType = DAC_WaveGeneration_None;
    Config_dac1->LowLevelConfig->Output_Buffer_status = DAC_OutputBuffer_Enable;

    Config_dac1->Channel = (t_u32)0x00000010;

    e_ttc_dac_errorcode error_dac = ttc_dac_init(DacIndex1);

    _dac_stm32l1xx_dma(Config_dac1,1);

}
void DMA_initialize(void) {


    Config_dma1 = ttc_dma_get_configuration(DmaIndex1);

    Config_dma1->PeripheralBaseAddr = DHR12RD_Address;
    Config_dma1->MemoryBaseAddr = (t_u32)&wave_array;
    Config_dma1->DIR = 0x00000010;
    Config_dma1->BufferSize = WAVE_POINTS;
    Config_dma1->PeripheralInc = 0x00000000;
    Config_dma1->MemoryInc = 0x00000080;
    Config_dma1->PeripheralDataSize = 0x00000100;
    Config_dma1->MemoryDataSize = 0x00000400;
    Config_dma1->Mode = 0x00000020;
    Config_dma1->Priority = 0x00002000;
    Config_dma1->M2M = 0x00000000;
    Config_dma1->Channel = DMA1_Channel3;

    e_ttc_dma_errorcode error_dma = ttc_dma_init(DmaIndex1);

}
void TIM2_initialize(void) {

  #warning ToDo: Replace standard peripheral code by hardware independent one!
  
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

    TIM_TimeBaseInitTypeDef timerInitStructure;
    timerInitStructure.TIM_Prescaler = 319;
    timerInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    timerInitStructure.TIM_Period = 9;
    timerInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(TIM2, &timerInitStructure);

    TIM_SelectOutputTrigger(TIM2,TIM_TRGOSource_Update);

    TIM_Cmd(TIM2, ENABLE);

}
void task_DAC(void *TaskArgument) {
    (void) Argument;
    
    while (TTC_TASK_SCHEDULER_AVAILABLE) {
      // do something...
      
      ttc_task_yield();    // always sleep for a while to give cpu time to other tasks (does nothing if multitasking is disabled)
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
