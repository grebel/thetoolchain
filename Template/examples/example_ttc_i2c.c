/** example_ttc_i2c.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple example code to demonstrate 
 *  how to use ttc_i2c.
 *
 *  The basic flow is as follows:
 *  (1) example_ttc_i2c_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_i2c_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 13 at 20150531 19:40:52 UTC
 *
 *  Authors: Gregor Rebel
 *
 *  See header file example_ttc_i2c.h for a detailed description of this example.
 *  
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_i2c.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_i2c.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/ttc_task.h"

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

const BOOL Use_I2C_Master_Register_Write = 0; // enable I2C master sending to slave register
const BOOL Use_I2C_Master_Register_Read  = 0; // enable I2C master reading from slave register
const BOOL Use_I2C_Master_Write          = 0; // enable I2C master generic sending to slave
const BOOL Use_I2C_Master_Read           = 1; // enable I2C master generic reading from slave
      BOOL Use_I2C_Master                = 0; // enabled automatically in example_ttc_i2c_prepare()
      BOOL Use_I2C_Slave                 = 0; // enabled automatically in example_ttc_i2c_prepare()

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_i2c_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _task_I2C(void *Argument);

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function Definitions *************************************************

void _task_I2C(void *Argument) {
    Assert(ttc_memory_is_writable(Argument), ttc_assert_origin_auto);        // always check pointer arguments with Assert()!
    t_example_i2c* TaskArguments = (t_example_i2c*) Argument; // we're expecting this pointer type

#define ETI_BUFFER_SIZE 10
    static t_u8 Buffer[ETI_BUFFER_SIZE] = { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };

    e_ttc_i2c_errorcode Error = E_ttc_i2c_errorcode_OK;
    t_u8 LogicalIndexI2C = TaskArguments->ConfigI2C->LogicalIndex;
    t_u8 SlaveAddress    = TaskArguments->ConfigI2C->OwnAddress1;

    while (1) {
        Error = E_ttc_i2c_errorcode_OK;

        if (Use_I2C_Master) { // being master: check bus access
            // Get access to I2C bus (some other master or slave may still block it)
            if (! ttc_i2c_wait_until_event(LogicalIndexI2C, E_ttc_i2c_event_code_bus_free) )
                // Another master or a slave seem to block the bus: reset it
                Error = ttc_i2c_master_reset_bus(LogicalIndexI2C);
        }
        if (!Error && Use_I2C_Master_Register_Write) { // write data into slave register
            *Buffer = 0x80;
            Error = ttc_i2c_master_write_register(LogicalIndexI2C,
                                                  SlaveAddress,
                                                  0x6B,
                                                  E_ttc_i2c_address_type_MSB_First_8Bit,
                                                  1,
                                                  Buffer
                                                  );
        }
        if (!Error && Use_I2C_Master_Register_Read) {  // read from slave register
            Error = ttc_i2c_master_read_register(LogicalIndexI2C,
                                                 SlaveAddress,
                                                 0x75,
                                                 E_ttc_i2c_address_type_MSB_First_8Bit,
                                                 1,
                                                 Buffer
                                                 );
        }
        if (!Error && Use_I2C_Master_Write) {          // send data to slave (generic)

            // prepare data to send
            Buffer[0] = 0x01;
            Buffer[1] = 0x02;
            Buffer[2] = 0x03;
            Buffer[3] = 0x04;

            if (! ttc_i2c_wait_until_event(LogicalIndexI2C, E_ttc_i2c_event_code_bus_free) )
                // Another master or a slave seem to block the bus: reset it
                Error = ttc_i2c_master_reset_bus(LogicalIndexI2C);

            if (!Error)
                Error = ttc_i2c_master_condition_start(LogicalIndexI2C);
            if (!Error)
                Error = ttc_i2c_master_send_slave_address(LogicalIndexI2C, SlaveAddress, 0);
            if (!Error)
                Error = ttc_i2c_master_send_bytes(LogicalIndexI2C, 4, Buffer);
            if (!Error)
                Error = ttc_i2c_master_condition_stop(LogicalIndexI2C);
        }
        if (!Error && Use_I2C_Master_Read) {           // read data from slave (generic)

            // prepare data buffer for incomming data
            volatile t_u8 AmountBytes = ETI_BUFFER_SIZE;
            ttc_memory_set(Buffer, 0, AmountBytes);

            if (! ttc_i2c_wait_until_event(LogicalIndexI2C, E_ttc_i2c_event_code_bus_free) )
                // Another master or a slave seem to block the bus: reset it
                Error = ttc_i2c_master_reset_bus(LogicalIndexI2C);

            if (!Error)
                Error = ttc_i2c_master_condition_start(LogicalIndexI2C);
            if (!Error)
                Error = ttc_i2c_master_send_slave_address(LogicalIndexI2C, SlaveAddress, 1);
            if (!Error)
                Error = ttc_i2c_master_read_bytes(LogicalIndexI2C, AmountBytes, Buffer);
            if (!Error)
                Error = ttc_i2c_master_condition_stop(LogicalIndexI2C);

            AmountBytes += 0; // place breakpoint here (end of communication)
        }
        if (!Error && Use_I2C_Slave) {                 // check if another master wants to communicate with us
            if (ttc_i2c_slave_check_own_address_received(LogicalIndexI2C)) {

                volatile t_u8 AmountBytes = 0;

                // Master is calling us: check write mode
                //
                // Note: The following implementation can serve a register-read opration like custom I2C slaves.
                //       1) Master sends slave address + Write-Mode
                //       2) Master writes register address (>=1 byte)
                //       3) Master sends slave address + Read-Mode
                //          => ttc_i2c_slave_read_bytes() returns amount of bytes being received
                //       4) ttc_i2c_slave_check_read_mode() now returns TRUE
                //          => ttc_i2c_slave_send_bytes() is sending bytes from register to master
                if (! ttc_i2c_slave_check_read_mode(LogicalIndexI2C) ) { // master wants to write data
                    AmountBytes = ttc_i2c_slave_read_bytes( LogicalIndexI2C,
                                                            ETI_BUFFER_SIZE,
                                                            Buffer
                                                          );
                    if (AmountBytes) {
                        AmountBytes += 0; // set breakpoint here
                    }
                }
                if ( ttc_i2c_slave_check_read_mode(LogicalIndexI2C) ) { // check again: now master wants to read data
                    AmountBytes = ttc_i2c_slave_send_bytes( LogicalIndexI2C,
                                                            ETI_BUFFER_SIZE,
                                                            Buffer
                                                           );
                    if (AmountBytes) {
                        AmountBytes += 0; // set breakpoint here
                    }
                }

                // reset I2C interface as a workaround for slave not answering to its OAR1 after first successfull transmission
                ttc_i2c_slave_reset_bus(LogicalIndexI2C);
            }
        }

        ttc_task_msleep(200);    // always sleep for a while to give cpu time to other tasks
    }
}
//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_i2c_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    Use_I2C_Master                = Use_I2C_Master_Register_Write || Use_I2C_Master_Register_Read || Use_I2C_Master_Write || Use_I2C_Master_Read;
    Use_I2C_Slave                 = !Use_I2C_Master; // enable I2C slave mode if not master mode is enabled

    // Note: Adding a unique prefix to all global and static variable makes it
    //       easier to get an overview of memory usage via _/calculateMemoryUsage.pl
    static t_example_i2c et_i2c_Config;

    // Using first logical I2C device
    // Logical devices are defined in makefiles and are summarized in compile_options.h.
    // TTC_I2C1 stores the physical index of first logical I2C device.
    // A physical index enumerates the internal I2C devices of a microcontroller (0=I2C1, ...)
    // This allows to configure any order and amount of logical devices up to the amount of
    // available physical devices.
    et_i2c_Config.ConfigI2C = ttc_i2c_get_configuration(1);

    // change configuration
    et_i2c_Config.ConfigI2C->ClockSpeed = 100000;     // 100 kHz should work on cables of up to two meters (check signal quality with oscilloscope)
    et_i2c_Config.ConfigI2C->OwnAddress1 = 0b1101000; // MPU 6050 6-axis inertial sensor
    //et_i2c_Config.ConfigI2C->OwnAddress1 = 0b1100101;    // dummy address
    et_i2c_Config.ConfigI2C->AmountRetries = 100000;
    et_i2c_Config.ConfigI2C->Init.TimeOut       = 1;
    //et_i2c_Config.ConfigI2C->AmountRetries = -1; // endless retries (enable to avoid timeouts during debugging!)

    ttc_i2c_init(et_i2c_Config.ConfigI2C->LogicalIndex);

    ttc_task_create( _task_I2C,       // function to start as thread
                     "tI2C",          // thread name (just for debugging)
                     256,             // stack size (adjust to amount of local variables of _task_I2C() AND ALL ITS CALLED FUNTIONS!)
                     &et_i2c_Config,  // passed as argument to _task_I2C()
                     1,               // task priority (higher values mean more process time)
                     NULL             // can return a handle to created task
                   );
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}Function Definitions
