﻿/** example_ttc_radio_ranging.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_radio.
 *  The basic flow is as follows:
 *  (1) example_ttc_radio_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_radio_prepare() gets executed
 *
 *  Created from template example_ttc_device.c revision 11 at 20140514 03:13:59 UTC
 *
 *  Authors: Gregor Rebel
 *
 *
 *  How it works
 *
 *  This example shows how to use radios with ranging capability. These radios (like DecaWave DW1000) measure distance
 *  between transmitter and receiver by each transmission.
 *  The example provides implementation for three types of radio nodes:
 *  1) Anchor Node (EXAMPLE_TTC_RADIO_NODE_INDEX = 1..99)
 *     Stationary node that collects ranging information from sensor nodes to pinpoint their location.
 *  2) Sensor Node (EXAMPLE_TTC_RADIO_NODE_INDEX > 99)
 *     Mobile Node that communicates with anchor nodes to provide them with position and sensor data updates.
 *     Each Sensor Node will try to measure distance to all anchor nodes in or AnchorID_16[]
 *  3) Observer Node (EXAMPLE_TTC_RADIO_NODE_INDEX == 0)
 *     Will only listen and receive all types of packets.
 *     Each received packet is debugged to TTC_USART1.
 *
}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_radio_ranging.h"              //
#include "example_ttc_radio_ranging_common.h"       // functions, definitions and structures common to all node types
#include "../ttc-lib/ttc_packet.h"                  // generic network packet format
#include "../ttc-lib/packet/packet_802154_types.h"  // using IEEE 802.15.4a types of network packets
#include "../ttc-lib/ttc_usart.h"                   // serial port is used to output debug messages
#include "../ttc-lib/ttc_systick.h"                 // exact delays and timeouts
#include "../ttc-lib/ttc_radio.h"                   // radio transceiver with ranging capability
//InsertIncludes above (DO NOT REMOVE THIS LINE!)

// Each node type has its own implementation
#ifdef EXAMPLE_TTC_RADIO_NODE_IS_ANCHOR
#  include "example_ttc_radio_ranging_anchor.h"
#endif
#ifdef EXAMPLE_TTC_RADIO_NODE_IS_MOBILE
#  include "example_ttc_radio_ranging_mobile.h"
#endif
#ifdef EXAMPLE_TTC_RADIO_NODE_IS_OBSERVER
#  include "example_ttc_radio_ranging_observer.h"
#endif
//}Includes
/** { Global Variables ************************************************
 *
 * Always prefix your global variables to reflect the filename to which they belong to!
 * Example:
 * file:     my_cool_file.c
 * variable: t_u8 mcf_Index = 1;
 */

// list of available anchors
const t_ttc_packet_address AnchorIDs[EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS] = {
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 0
    {
        .Address64.Bytes = { 0xc0, 0x01, 0xba, 0xbe, 0x00, 0x00, 0x00, 1 },
        .Address16       = 1,            // south pole anchor in slam_simple_2d
        .PanID           = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID
    }
    #endif
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 1
    , {
        .Address64.Bytes = { 0xc0, 0x01, 0xba, 0xbe, 0x00, 0x00, 0x00, 2 },
        .Address16       = 2,           // north pole anchor in slam_simple_2d
        .PanID           = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID
    }
    #endif
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 2
    , {
        .Address64.Bytes = { 0xc0, 0x01, 0xba, 0xbe, 0x00, 0x00, 0x00, 3 },
        .Address16       = 3,          // east anchor in slam_simple_2d
        .PanID           = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID
    }
    #endif
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 3
    , {
        .Address64.Bytes = { 0xc0, 0x01, 0xba, 0xbe, 0x00, 0x00, 0x00, 4 },
        .Address16       = 4,          // west anchor in slam_simple_2d
        .PanID           = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID
    }
    #endif
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 4
    , {
        .Address64.Bytes = { 0xc0, 0x01, 0xba, 0xbe, 0x00, 0x00, 0x00, 5 },
        .Address16       = 5,          // east anchor in slam_simple_2d
        .PanID           = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID
    }
    #endif
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 5
    , {
        .Address64.Bytes = { 0xc0, 0x01, 0xba, 0xbe, 0x00, 0x00, 0x00, 6 },
        .Address16       = 6,          // west anchor in slam_simple_2d
        .PanID           = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID
    }
    #endif
    #if EXAMPLE_TTC_RADIO_AMOUNT_ANCHORS > 6
#  error  Not egnough data!
    #endif
};

//InsertGlobalVariables above (DO NOT DELETE THIS LINE!)

//}GlobalVariables
/** { Private Function Declarations ***********************************
 *
 * Declare all functions here that should not be called from outside.
 * You may prefix private functions by an underscore to help identify them as private:
 * E.g.: void _privateCalc();
 */

//InsertPrivateFunctionDeclarations above (DO NOT DELETE THIS LINE!)

//}PrivateFunctions
/** { Function Definitions ********************************************
 */

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_radio_ranging_prepare() {

    if ( 0 ) { // test-setup for using PanID-field as TargetID to reduce message size (-> https://groups.google.com/d/msgid/decawave_group/6d38011a-b170-4b30-96ca-6c1c37f2e0a8%40googlegroups.com)
        // configure radio filter
        ttc_radio_get_configuration( 1 )->FrameFilter.Allowed.WithoutDestinationID = 1;
        ttc_radio_get_configuration( 1 )->FrameFilter.Allowed.Data                 = 1;

        // other radio configuration...

        ttc_radio_init( 1 );

        // id of local node
        t_u8 LocalID[2]; *( ( t_u16* ) LocalID ) = 0x001;

        // id of target node
        t_u8 TargetID[2]; *( ( t_u16* ) TargetID ) = 0x002;

        t_u8 Buffer[20];

        // compile packet header
        t_packet_802154_data_000010* Packet = ( t_packet_802154_data_000010* ) Buffer;
        Packet->MHR.FCF.Bits.FrameType         = 1; // data packet
        Packet->MHR.FCF.Bits.PanID_Compression = 1; // only one PanID field
        Packet->MHR.FCF.Bits.DstAddressType    = 0; // no destination address
        Packet->MHR.FCF.Bits.FrameVersion      = 1; // IEEE 802.15.4 (2011)
        Packet->MHR.FCF.Bits.SrcAddressType    = 2; // 16 bit source address + PanID

        // set source id
        Packet->MHR.SrcNodeID_L = LocalID[0];
        Packet->MHR.SrcNodeID_H = LocalID[1];

        // use PanID of remote node as target ID
        Packet->MHR.SrcPanID_L = TargetID[0];
        Packet->MHR.SrcPanID_H = TargetID[1];

        // copy payload into packet
        t_u8 Source[10];
        t_u8 SizePayload = 2;
        ttc_memory_copy( Packet->Payload, Source, SizePayload );

        // minimal packet length + 16 bit CRC (add payload size!)
        Packet->MHR.Length = sizeof( t_packet_802154_data_000010 )
                             + SizePayload + 2;

        // transmit NewPacket ...
    }

    static etr_task_info_t TaskInfo;

    if ( 1 ) { // select which packet details should be debugged
        TaskInfo.PacketDebug.SourceID = 1;
        TaskInfo.PacketDebug.TargetID = 1;
        TaskInfo.PacketDebug.Payload  = 1;
        TaskInfo.PacketDebug.Size     = 1;
    }
    if ( 1 ) { // configure ttc_usart
        #ifdef TTC_USART1
        const char* StringBanner = "\n\nexample_ttc_radio_ranging v%d\n";
        t_ttc_usart_config* ConfigUSART = TaskInfo.ConfigUSART = ttc_usart_get_configuration( 1 );
        ConfigUSART->Init.BaudRate = 2000000; // reduce if your serial port does not support 2Mb/s
        ConfigUSART->Init.HalfStopBits = 2;
        ConfigUSART->Flags.Bits.ControlParity = 0;
        ConfigUSART->Flags.Bits.Rts = 0;
        ConfigUSART->Flags.Bits.Cts = 0;
        ttc_usart_init( 1 );
        example_ttc_radio_ranging_usart_send_stringf( StringBanner, 10 );
        #endif
    }
    if ( 1 ) { // configure ttc_radio

        t_ttc_radio_config* ConfigRadio = TaskInfo.ConfigRadio = ttc_radio_get_configuration( 1 );

        // logical index of SPI device being connected to DW1000 transceiver
        // Note: If not already initialized, low-level driver will initialize ttc_spi
        ConfigRadio->LowLevelConfig.LogicalIndex_SPI = 1;
        //ConfigRadio->Init.Flags.AutoAcknowledge      = 0;
        ConfigRadio->Init.Flags.DelayedTransmits     = 1;
        ConfigRadio->Init.Flags.RxAutoReenable       = 1; // re-enable receiver whenever it has switched off automatically
        #ifndef EXAMPLE_TTC_RADIO_NODE_IS_ANCHOR
        ConfigRadio->Init.Flags.EnableRangingReplies = 0; // only anchor nodes should answer ranging requests in our scenario
        #endif
        //? ConfigRadio->LowLevelConfig.TimeOut          = 0;//1.012;

        ConfigRadio->Init.LevelTX = ConfigRadio->Features->MaxTxLevel;
        //ConfigRadio->Init.MaxPacketSize = ConfigRadio->Features->MaxPacketSize; // ToDo: implement frame length > 127 bytes

        #if ANCHOR_INDEX > 0 // configure anchor node
        ThisNodeID_16 = AnchorIDs_16[ANCHOR_INDEX];
        #else                // configure sensor node
        #endif
        example_ttc_radio_ranging_usart_send_string( "initializing radio..\n", -1 );

        ttc_radio_init( 1 );

        ttc_gpio_init( E_ttc_gpio_pin_a10, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max ); //DEBUG  using RxD-pin as debug output


        // allocate buffer used to prepare packet payload
        TaskInfo.PrepareBuffer_Size = ttc_radio_get_configuration( 1 )->Init.MaxPacketSize;
        TaskInfo.PrepareBuffer      = ttc_heap_alloc_zeroed( TaskInfo.PrepareBuffer_Size );

        if ( 0 ) { // test RX- and TX-LEDs
            radio_dw1000_gpio_out( ConfigRadio, 2, 1 ); // RXLED
            radio_dw1000_gpio_out( ConfigRadio, 3, 1 ); // TXLED
            while ( 1 );
        }

        // store pointer to radio configuration for fast access
        TaskInfo.ConfigRadio = ConfigRadio;
    }
    if ( 1 ) { // set local node identifier

        // allocate as static allows compiler to check if it fits into ram (could also allocate via ttc_heap)
        static t_ttc_packet_address LocalNodeID;

        #ifdef EXAMPLE_TTC_RADIO_NODE_IS_ANCHOR  // load local address of an anchor node
        ttc_memory_copy( &LocalNodeID, &( AnchorIDs[EXAMPLE_TTC_RADIO_NODE_INDEX - 100] ), sizeof( LocalNodeID ) );
        #else                  // load local address of an observer or mobile node
        LocalNodeID.PanID     = EXAMPLE_TTC_RADIO_LOCAL_PAN_ID;
        LocalNodeID.Address16 = EXAMPLE_TTC_RADIO_NODE_INDEX;

        const t_u8 Address64[8] = { 0xaf, 0xfe, 0x00, 0x00, 0x00, 0x00, 0x00, EXAMPLE_TTC_RADIO_NODE_INDEX };
        ttc_memory_copy( &LocalNodeID, Address64, 8 );

        #endif

        // pass pointer to local node identifier to task and statemachine
        TaskInfo.LocalNodeID = &LocalNodeID;
    }
    if ( 1 ) { // let node prepare itself and start its task

        // let node prepare itself and start its task
        #ifdef EXAMPLE_TTC_RADIO_NODE_IS_MOBILE
        example_ttc_radio_ranging_prepare_mobile( &TaskInfo );
        #endif
        #ifdef EXAMPLE_TTC_RADIO_NODE_IS_ANCHOR
        example_ttc_radio_ranging_prepare_anchor( &TaskInfo );
        #endif
        #ifdef EXAMPLE_TTC_RADIO_NODE_IS_OBSERVER
        example_ttc_radio_ranging_prepare_observer( &TaskInfo );
        #endif
    }
}
//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//InsertPrivateFunctionDefinitions above (DO NOT DELETE THIS LINE!)

//}FunctionDefinitions
