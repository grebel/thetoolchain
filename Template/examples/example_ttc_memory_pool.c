/*{ example_ttc_heap_pool.c ************************************************

  emp_task_Sender() allocates memory blocks from pool and sends them to a list.
  emp_task_Receiver() receives memory blocks from list and returns them to their pool.

}*/

#include "../compile_options.h"       // dynamically created collection of all "COMPILE_OPTS +=-D..." definitions from all makefiles
#include "example_ttc_heap_pool.h"

//{ Global variables

// Note: Global and static variables require a unique prefix (here "el_") to avoid
//       conflicting symbol names when combined with other sources!

//}
//{ Private Function Declarations ****************************************

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function definitions *************************************************

// size of each memory block in pool
#define BLOCK_SIZE 100

// transports memory blocks from emp_task_Sender() to emp_task_Receiver()
t_ttc_list List;

// Counters for blocks being transmitted/ received
t_u32 CountTx = 0;
t_u32 CountRx = 0;


//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void example_ttc_heap_pool_prepare() {
#ifdef TTC_LED1
    ttc_gpio_init(TTC_LED1, E_ttc_gpio_mode_output_push_pull);
#endif
#ifdef TTC_LED2
    ttc_gpio_init(TTC_LED2, E_ttc_gpio_mode_output_push_pull);
#endif

    t_ttc_heap_pool* Pool = ttc_heap_pool_create(BLOCK_SIZE, 10);
    ttc_list_init(&List, "TestList");

    ttc_task_create( emp_task_Sender,  // function to start as thread
                     "Sender",         // thread name (just for debugging)
                     128,              // stack size
                     Pool,             // passed as argument to taskBrightness()
                     1,                // task priority (higher values mean more process time)
                     NULL              // can return a handle to created task
                     );
    ttc_task_create( emp_task_Receiver,// function to start as thread
                     "Receiver",       // thread name (just for debugging)
                     128,              // stack size
                     NULL,             // passed as argument to taskBrightness()
                     1,                // task priority (higher values mean more process time)
                     NULL              // can return a handle to created task
                     );

}
void emp_task_Sender(void *TaskArgument) {
    t_ttc_heap_pool* Pool = (t_ttc_heap_pool*) TaskArgument;
    t_u8 Value = 0;

    while (1) {
#ifdef TTC_LED1
        ttc_gpio_setn(TTC_LED1, Value & 1);
#endif

        // get block from pool
        t_ttc_heap_block_from_pool* NewBlock = ttc_heap_pool_block_get(Pool);

        // write something into block
        // Note: blocks from memory pools can be typecasted!
        ttc_memory_set(NewBlock, Value++, BLOCK_SIZE);

        // every block from memory pool also is a list item
        t_ttc_list_item* Item = ttc_heap_pool_to_list_item(NewBlock);

        ttc_list_push_back_single(&List, Item);
        CountTx++;

        ttc_task_msleep(30);
    }
}
void emp_task_Receiver(void *TaskArgument) {
    (void) TaskArgument; // not used

    volatile t_u8 Value;

    while (1) {
#ifdef TTC_LED2
        ttc_gpio_setn(TTC_LED2, Value & 1);
#endif

        t_ttc_list_item* ReceivedItem = ttc_list_pop_front_single_wait(&List);
        Assert(ReceivedItem, ttc_assert_origin_auto);

        // convert back to memory block from pool
        t_ttc_heap_block_from_pool* ReceivedBlock = ttc_heap_pool_from_list_item(ReceivedItem);

        // read data from block
        // Note: blocks from memory pools can be typecasted!
        t_u8* Buffer8 = (t_u8*) ReceivedBlock;
        Value = Buffer8[0];

        CountRx++;

        // return memory block to its pool for reuse
        ttc_heap_pool_block_free(ReceivedBlock);
    }
    (void) Value; // avoid warning unused..
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!) 
//}FunctionDefinitions
