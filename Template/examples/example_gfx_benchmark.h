/** { example_gfx_benchmark.h ************************************************
 *
 * Benchmark code for ttc_gfx.c/.h for current architecture and display.
 *
 * written by Gregor Rebel 2012-2014
 *
 * Actual results:
 *
 * Benchmark v1.0.53(5)
 * - new GPIO driver
 * - changed LCD_* to constants TTC_GFX1_ILI93XX_PIN_*
 * - activate.060_basic_extensions_optimize_1_<OPTIMIZE>
 *
 *                               STM32-LCD | STM32-LCD  | STM32-LCD  | Mini-STM32
 * <OPTIMIZE>                      DEBUG   |  SMALLEST  |   FASTEST  |   DEBUG
 *                              -----------+------------+------------+------------
 * Run0: benchmark_Clear(5)         536 ms |    288 ms  |        ms  |    799 ms
 * Run1: benchmark_Circle(5)      15165 ms |  15147 ms  |        ms  |   7895 ms
 * Run2: benchmark_CircleFill(5)  16695 ms |  16668 ms  |        ms  |   6462 ms
 * Run3: benchmark_Line_ortho(5)   3232 ms |   3229 ms  |        ms  |   1175 ms
 * Run4: benchmark_Line(5)        16154 ms |  16095 ms  |        ms  |  10231 ms
 * Run5: benchmark_Rect(5)         3281 ms |   3278 ms  |        ms  |   1205 ms
 * Run6: benchmark_Text(5)         4682 ms |   4581 ms  |        ms  |   2723 ms
 * Run7: benchmark_SetPixel(5)    19649 ms |  19630 ms  |        ms  |  10691 ms
 * Run8: benchmark_RectFill(5)     3088 ms |   3082 ms  |        ms  |    875 ms
 * total:                         82482 ms |  81998 ms  |        ms  |  42056 ms
 *
}*/

//{ Includes *************************************************************

// Basic set of helper functions
#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_task.h"
#include "../ttc-lib/ttc_string.h"
#include "../ttc-lib/ttc_gfx.h"
#include "../ttc-lib/ttc_systick.h"  // exact delays and timeouts

//}Includes
//{ Defines/ TypeDefs ****************************************************

#ifndef EXTENSION_ttc_gfx
    #error Missing EXTENSION_ttc_gfx !
#endif

//}Defines
//{ Structures/ Enums ****************************************************


//}Structures/ Enums
//{ Global Variables *****************************************************



//}Global Variables
//{ Function declarations **************************************************

/** basic hardware intialization
  */
void gfx_benchmark_init();

/** main entry: initializes hardware + spawns all threads
  * returns immediately
  */
void example_gfx_benchmark_prepare();

/** main task
  */
void task_gfx_benchmark( void* TaskArgument );

/** deliver defined series of random data */
t_u16 getNextRandom();
void resetRandom();

/** individual benchmarks
 *
 * @param Count   amount of iterations
 * @return        elapsed time of individual benchmark (us)
 */
t_base benchmark_Circle( t_u8 Count );
t_base benchmark_CircleFill( t_u8 Count );
t_base benchmark_Clear( t_u8 Count );
t_base benchmark_Line( t_u8 Count );
t_base benchmark_Line_ortho( t_u8 Count );
t_base benchmark_Rect( t_u8 Count );
t_base benchmark_RectFill( t_u8 Count );
t_base benchmark_SetPixel( t_u8 Count );
t_base benchmark_Text( t_u8 Count );

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations
