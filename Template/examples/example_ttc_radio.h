#ifndef EXAMPLE_RADIO_H
#define EXAMPLE_RADIO_H

/** example_ttc_RADIO.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple to understand code example
 *  how to use ttc_radio.
 *  The basic flow is as follows:
 *  (1) example_ttc_radio_prepare() gets called in single tasking mode from ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global variables
 *      - it creates all required tasks
 *      - it returns immediately (only if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_radio_prepare() gets executed
 *
 *  Created from template example_ttc_device.h revision 11 at 20140514 03:13:59 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_radio.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct {
    e_ttc_packet_pattern   SocketID;    // each application using ttc_radio has a unique identifier
    t_u8                   UserID;      // each user id can lock ttc_radio for exclusive use during a longer transaction
    t_u8                   RadioIndex;  // logical index of radio device to use
    t_ttc_radio_config*    RadioConfig; // configuration of our radio (for debugging only)
} t_etr_config;
//}Structures/ Enums
//{ Function declarations **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void example_ttc_radio_prepare();

/** task running this example
 *
 * @param TaskArgument  passed from ttc_task_create() call.
 */
void example_ttc_radio_task( void* TaskArgument );

/** sends out given data via first radio
 *
 * @param Data     buffer storing data to be sent as payload
 * @param Amount   amount of valid bytes in Data[]
 */
void example_ttc_radio_transmit( t_etr_config* Config, const t_u8* Data, t_u8 Amount );

/** Will be called from ttc_radio for each incoming packet
 *
 * @param Config   pointer to radio configuration
 * @param Packet   buffer storing data and meta information
 * @return ==TRUE: packet has been processed; ==FALSE: ttc_radio will append packet to Config->List_PacketsRx
 */
BOOL example_ttc_radio_isr( t_ttc_radio_config* Config, t_ttc_packet* Packet );

//InsertFunctionDeclarations above (DO NOT REMOVE THIS LINE!)
//}Function Declarations

#endif //EXAMPLE_RADIO_H
