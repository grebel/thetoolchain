#!/bin/bash

if [ "$1" == "" ]; then
  cat <<END_OF_INFO
$0 [DDD]

Starts a debug session fir first connected debug iinterface.

Arguments:
    DDD    if given, graphical debugger ddd is used instead of text only gdb.

END_OF_INFO
fi
if [ ! -e _ ]; then
  Recompile="1"
  echo "$0 - Target of _/ link not found: Recompiling project to recreate links.."
fi
if [ ! -e main.elf ]; then
  Recompile="1"
fi


./createLinks.sh >createLinks.log
source _/SourceMe.sh               # update path variables to binary tools
if [ -e kill_gdb.sh ]; then
  bash ./kill_gdb.sh
  rm kill_gdb.sh
fi
if [ "$Recompile" == "1" ]; then
  ./clean.sh
  ./compile.sh
else
  if [ ! -e flash.log ]; then
    echo "flashing binary main.bin onto microcontroller.."
    ./flash.sh
  fi
fi
if [ ! -e flash.log ]; then
    cat <<END_OF_INFO
    
    
$0 - ERROR: It seems as if flashing was not successful.

You may create dummy file flash.log and retry if you still want to proceed:
  touch flash.log; $0 $@
  
END_OF_INFO
    exit 10
fi
_/debug_current.sh $@
