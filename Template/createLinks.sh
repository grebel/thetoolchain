#!/bin/bash
#
# Create links to external tools/ sources provided by TheToolChain
# into current directory
#
# Usage:
# cd to yur source-code directory
# isssue createLinks.sh 
#
# Example:
# cd ~/Source/Test123
# ./createLinks.sh
#

if [ "`pwd | grep ' '`" ]; then #{ ERROR: Current path contains spaces
  PWD=`pwd`
  cat <<END_OF_ERROR
  
  
$0 ERROR: Current path contains spaces.
   pwd='$PWD'
   
   TTC projects may not be used in paths containing spaces.
   Rename folder or move this project folder somewhere else!
  
   
END_OF_ERROR
  exit 12
fi #}

Source="$HOME/Source"

echo "$0: regenerating links.."
RequiredVersion=`cat required_version`
rm 2>/dev/null $Source/TheToolChain;  ln -sv $Source/TheToolChain_${RequiredVersion} $Source/TheToolChain
rm 2>/dev/null _;                     ln -sv $Source/TheToolChain/InstallData/scripts  _
for Script in makefile clean.sh flash.sh; do
  if [ ! -e $Script ]; then
    rm 2>/dev/null $Script
    ln -sv _/$Script .
  fi
done
rm 2>/dev/null ttc-lib;       ln -sv $Source/TheToolChain/TTC-Library   ttc-lib
rm 2>/dev/null Documentation; ln -sv $Source/TheToolChain/Documentation .
#X rm 2>/dev/null regressions; ln -sv $Source/TheToolChain/Regressions   regressions

Version=`cat _/Version`

# create links to third party modules
if [ -e _/createLinks.sh ]; then
  _/createLinks.sh
fi

echo "TheToolChain v$Version"
