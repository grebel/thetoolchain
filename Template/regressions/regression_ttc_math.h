#ifndef EXAMPLE_MATH_H
#define EXAMPLE_MATH_H

/** regression_ttc_MATH.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Implementation test for mathematical high- and low-level implementations
 *  for currently activated architecture.
 *
 *  Basic flow with enabled multitasking:
 *  (1) regression_ttc_math_start() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by regression_ttc_math_start() get executed
 *
 *  Basic flow with disabled multitasking:
 *  (1) regression_ttc_math_start() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template regression_ttc_device.h revision 15 at 20160816 16:03:03 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile regression_ttc_MATH.c only do not belong here!
#include "../ttc-lib/ttc_basic.h"

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // example_math_t
    t_u8 LogicalIndex;  // logical index of device to use
    // more arguments...
} example_math_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this example
 *
 * - initializes data
 * - spawns all threads
 * - returns immediately
 */
void regression_ttc_math_start();


//}Function prototypes

#endif //EXAMPLE_MATH_H
