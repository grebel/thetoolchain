/** regression_ttc_pwm.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Implementation test for high-level driver ttc_pwm
 *  using currently activated architecture. The test should compile and run
 *  with all available low-level drivers. 
 *  Additional regressions may exist for individual low-level drivers.
 *
 *  Basic flow of regressions with enabled multitasking:
 *  (1) regression_ttc_pwm_stm32l1xx_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by regression_ttc_pwm_stm32l1xx_prepare() get executed
 *
 *  Basic flow of regressions with disabled multitasking:
 *  (1) regression_ttc_pwm_stm32l1xx_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template regression_ttc_device.c revision 18 at 20180502 11:57:47 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "regression_ttc_pwm.h"
#include "../ttc-lib/ttc_pwm.h" // high-level driver of device to test
#include "../ttc-lib/ttc_task.h"     // allows to use application in single- and multitasking setup

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this regression
 *
 * This task is created automatically from regression_ttc_pwm_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _regression_pwm_task(void *Argument);

//}Private Function Declarations
//{ Function Definitions *************************************************

void regression_ttc_pwm_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    example_pwm_data_t* Argument = ttc_heap_alloc_zeroed( sizeof(example_pwm_data_t) );
    
    Argument->Index_PWM = 1; // set logical index of device to use
    
    // get configuration of pwm device
    t_ttc_pwm_config* Config_PWM = ttc_pwm_get_configuration(Argument->Index_PWM);
    
    // change configuration before initializing device
    //...
    
    // initializing our pwm device here makes debugging easier (still single-tasking)
    // Logical index is stored inside each device configuration
    ttc_pwm_init(Config_PWM->LogicalIndex);

    ttc_task_create( _regression_pwm_task,       // function to start as thread
                     "tPWM",          // thread name (just for debugging)
                     128,                  // stack size (adjust to amount of local variables of _regression_pwm_task() AND ALL ITS CALLED FUNTIONS!) 
                     &et_pwm_Data,    // passed as argument to _regression_pwm_task()
                     1,                    // task priority (higher values mean more process time)
                     NULL                  // can return a handle to created task
                   );
}
void _regression_pwm_task(void *Argument) {
    Assert_BASIC_Writable(Argument, ttc_assert_origin_auto);                  // always check pointer arguments with Assert() before dereferencing them!
    regression_pwm_data_t* Data = ( regression_pwm_data_t* ) Argument; // we're expecting this pointer type

    // get pointer to device configuration (remove if not required)
    t_ttc_pwm_config* Config_PWM = ttc_pwm_get_configuration(Data->Index_PWM);

    do { 
        // This loop runs endlessly if scheduler is available.
        // Otherwise it will run only onve and be called periodically from ttc_task_start_scheduler().
  
        if ( ttc_systick_delay_expired( & Data->Delay ) ) { // delay passed: send hello world (zeroed delay always expires)
            ttc_systick_delay_init( & Data->Delay, 1000 ); // set new delay to expire in 1 millisecond = 1000 microseconds
    
          // implement statemachine of this task here...
        }
        ttc_task_yield(); // if scheduler available, give cpu to other processes
    } while (TTC_TASK_SCHEDULER_AVAILABLE);
}

//}Function Definitions
