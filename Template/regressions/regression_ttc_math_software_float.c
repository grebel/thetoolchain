/** example_ttc_math_software_float.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Example for software floating point support single precision
 *
 *  Basic flow of examples with enabled multitasking:
 *  (1) example_ttc_math_software_float_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by example_ttc_math_software_float_prepare() get executed
 *
 *  Basic flow of examples with disabled multitasking:
 *  (1) example_ttc_math_software_float_prepare() gets called in single tasking mode from 
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template regression_ttc_device_architecture.c revision 15 at 20160816 16:03:03 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "example_ttc_math_software_float.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_math.h"

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

/** Task running this example
 *
 * This task is created automatically from example_ttc_math_software_float_prepare() at system startup.
 * There is no need to start it from outside.
 *
 * @param Argument  passed argument #4 from ttc_task_create() call.
 */
void _task_math_software_float(void *Argument);

//}Private Function Declarations
//{ Function Definitions *************************************************

void example_ttc_math_software_float_prepare() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    // Note: Adding a unique prefix to all global and static variable makes it
    //       easier to get an overview of memory usage via _/calculateMemoryUsage.pl
    static example_math_software_float_data_t et_math_software_float_Data;
    
    et_math_software_float_Data.LogicalIndex = 1; // set logical index of device to use
    
    // get configuration of math device
    t_ttc_math_config* Config_MATH = ttc_math_get_configuration(et_math_software_float_Data.LogicalIndex);
    
    // change configuration before initializing device
    Config_MATH->Flags.Bits.Reserved1 = 1; // delete me!
    //...
    
    // initializing our math device here makes debugging easier (still single-tasking)  
    ttc_math_init(et_math_software_float_Data.LogicalIndex);

    ttc_task_create( _task_math_software_float,       // function to start as thread
                     "tMATH_SOFTWARE_FLOAT",          // thread name (just for debugging)
                     128,                                 // stack size (adjust to amount of local variables of _task_math_software_float() AND ALL ITS CALLED FUNTIONS!) 
                     &et_math_software_float_Data,    // passed as argument to _task_math_software_float()
                     1,                                   // task priority (higher values mean more process time)
                     NULL                                 // can return a handle to created task
                   );
}
void _task_math_software_float(void *Argument) {
    Assert(ttc_memory_is_writable(Argument), ttc_assert_origin_auto);                  // always check pointer arguments with Assert() before dereferencing them!
    example_math_software_float_data_t* Data = ( example_math_software_float_data_t* ) Argument; // we're expecting this pointer type

    // get configuration of already initialized math device
    t_ttc_math_config* Config_MATH = ttc_math_get_configuration(Data->LogicalIndex);
    (void) Config_MATH;   // yet unused

    while (1) {
      // do something...
      
      ttc_task_yield();    // always sleep for a while to give cpu time to other tasks
    }
}

//}Function Definitions
