#ifndef REGRESSION_PWM_H
#define REGRESSION_PWM_H

/** regression_ttc_PWM.h **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  Implementation test for high-level driver ttc_pwm
 *  using currently activated architecture. The test should compile and run
 *  with all available low-level drivers.
 *  Additional regressions may exist for individual low-level drivers.
 *
 *  Basic flow with enabled multitasking:
 *  (1) regression_ttc_pwm_stm32l1xx_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started) 
 *  (2) Scheduler is started
 *  (3) Tasks being created by regression_ttc_pwm_stm32l1xx_prepare() get executed
 *
 *  Basic flow with disabled multitasking:
 *  (1) regression_ttc_pwm_stm32l1xx_prepare() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates first task
 *  (2) First task is executed forever
 *
 *  Created from template regression_ttc_device.h revision 16 at 20180502 11:57:47 UTC
 *
 *  Authors: Gregor Rebel
 *  
}*/
//{ Includes *************************************************************

// Include all header files being required to include this file.
// Includes being required to compile regression_ttc_PWM.c only do not belong here!
#include "../ttc-lib/ttc_basic.h"      // basic datatypes and definitions
#include "../ttc-lib/ttc_task_types.h" // required for task delays

//}Includes
//{ Defines/ TypeDefs ****************************************************

//}Defines
//{ Structures/ Enums ****************************************************

typedef struct { // regression_pwm_t
    t_u8              Index_PWM;  // logical index of device to use
    t_ttc_systick_delay  Delay;          // this type of delay is usable in single- and multitasking setup

    // more arguments...
} regression_pwm_data_t;

//}Structures/ Enums
//{ Function prototypes **************************************************

/** start point of this regression
 *
 * - initializes data 
 * - spawns all threads
 * - returns immediately
 */
void regression_ttc_pwm_prepare();


//}Function prototypes

#endif //REGRESSION_PWM_H
