/** regression_ttc_math.c **********************************************{
 *
 *                              The ToolChain
 *
 *  The ToolChain has been originally developed by Gregor Rebel 2010-2018.
 *
 *  This source should provide a small and simple example code to demonstrate
 *  how to use ttc_math.
 *
 * Trignonometric functions always implement an approximation of the real result.
 * In low-power embedded applications, precision can be traded for faster computation.
 * The required precision is set in regression_ttc_math_start(). The approximating
 * implementations of ttc_math will adapt to different precisions. The approximated
 * result of each function is checked to provide the correct precision.
 *
 * One may attach an oscilloscope probe to LED1 to measure runtime of all activated
 * benchmarks. Output pin TTC_LED1 is set while functions are benchmarked and cleared
 * for 100 ms afterwards.
 *
 *  The basic flow is as follows:
 *  (1) regression_ttc_math_start() gets called in single tasking mode from
 *      ttc_extensions.c:ttc_extensions_start() at system boot
 *      - it initializes all required devices
 *      - it initializes its global and locat static variables
 *      - it creates all required tasks
 *      - it returns immediately (if multitasking should be started)
 *  (2) Scheduler is started
 *  (3) Tasks being created by regression_ttc_math_start() gets executed
 *
 *  Created from template regression_ttc_device.c revision 13 at 20150724 07:49:04 UTC
 *
 *  Authors: Gregor Rebel
 *
}*/

//{ Includes *************************************************************

// Include all header files being required to compile this file.
#include "regression_ttc_math.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_math.h"
#include "../ttc-lib/ttc_gpio.h"

//}Includes
//{ Global Variables *****************************************************

// Note: Adding a unique prefix to all global and static variable makes it
//       easier to get an overview of memory usage via _/calculateMemoryUsage.pl

//}Global Variables
//{ Private Function Declarations ****************************************
//
// Private functions should only be called from this source code, though it is possible.
// Declaring a function as private removes it from list of public functions and makes it
// easier for other developers to understand your application interface.
//

//InsertPrivateFunctionDeclarations  above (DO NOT DELETE THIS LINE!)
//}PrivateFunctionDeclarations
//{ Function Definitions *************************************************
volatile ttm_number MaxRelativeError = 0;

/** checks if value is equal to compare value according to allowed error
 *
 * Note: updates global variable MaxRelativeError
 *
 * @param Value    value to check
 * @param Compare  comparison value
 * @param Precision  allowed error
 * @return         ( abs(Value-Compare) < Precision )
 */
BOOL etm_within( ttm_number Value, ttm_number Compare, ttm_number Precision ) {
    ttm_number Error = ttc_basic_distance( Value, Compare );
    if ( Compare != 0.0 ) {
        Error /= Compare;
        if ( MaxRelativeError < Error ) {
            MaxRelativeError = Error;
        }
    }

    return ( Error < Precision );
}
BOOL etm_within_int( t_s32 Value, t_s32 Compare, t_s16 MaxDifference ) {
    t_s32 Error = ttc_basic_distance( Value, Compare );

    return ( Error <= abs( MaxDifference ) );
}

t_u8 regression_ttc_math_TextY = 1;    // current text row on graphic display
ttm_number etm_Precision    = 0;    // maximum allowed relative approximation error
const t_u16 Runs            = 100;  // may run each calculation several times for more accurate benchmarking

#ifdef TTC_GFX1
#include "../ttc-lib/ttc_gfx.h"


A_define( t_u8, etm_String, 30 ); // buffer for temporary strings

// enable gfx macros
#define _etm_gfx_print_at(X, Y, String, MaxSize ) ttc_gfx_text_solid_at(X, Y, (const char*) String, MaxSize )

const t_u32 ets_ColorBg = TTC_GFX_COLOR24_WHITE;
const t_u32 ets_ColorFg = TTC_GFX_COLOR24_BLUE;

void _etm_gfx_init( ) {

    t_ttc_gfx_config* GFX = ttc_gfx_get_configuration( 1 );
    Assert( GFX );

    // set color to use for first screen clear
    ttc_gfx_color_bg24( ets_ColorBg );
    ttc_gfx_color_fg24( ets_ColorFg );

    ttc_gfx_init( 1 );
}
void _etm_gfx_printf1_at( t_s16 X, t_s16 Y, const char* Format, t_base Value ) {
    if ( X < 0 )
    { X = ttc_gfx_get_configuration( 1 )->TextColumns + X; }
    if ( Y < 0 )
    { Y = ttc_gfx_get_configuration( 1 )->TextRows + Y; }

    ttc_string_snprintf( etm_String.Data, etm_String.Size, Format, Value );
    _etm_gfx_print_at( X, Y, etm_String.Data, etm_String.Size );
}
void _etm_gfx_printf2_at( t_s16 X, t_s16 Y, const char* Format, t_base Value1, t_base Value2 ) {
    if ( X < 0 )
    { X = ttc_gfx_get_configuration( 1 )->TextColumns + X; }
    if ( Y < 0 )
    { Y = ttc_gfx_get_configuration( 1 )->TextRows + Y; }

    ttc_string_snprintf( etm_String.Data, etm_String.Size, Format, Value1, Value2 );
    _etm_gfx_print_at( X, Y, etm_String.Data, etm_String.Size );
}

void _etm_gfx_benchmark( const char* Name ) {
    ttc_gfx_text_cursor_set( 0, regression_ttc_math_TextY );
    ttc_gfx_text_clear( -1 );
    ttc_gfx_text( Name, -1 );
    _etm_gfx_printf1_at( ttc_gfx_get_configuration( 1 )->TextColumns - 2,
                         regression_ttc_math_TextY,
                         "..",
                         0
                       );

}
void _etm_gfx_benchmark_ok() {
    _etm_gfx_printf1_at( ttc_gfx_get_configuration( 1 )->TextColumns - 3,
                         regression_ttc_math_TextY,
                         " OK",
                         0
                       );

    // proceed to next row
    regression_ttc_math_TextY++;
    if ( regression_ttc_math_TextY >= ttc_gfx_get_configuration( 1 )->TextRows )
    { regression_ttc_math_TextY  = 1; }
    ttc_task_msleep( 100 );
}
#else

// disable gfx macros
#define _etm_gfx_init( )
#define _etm_gfx_printf1_at(X, Y, String, MaxSize )
#define _etm_gfx_printf2_at(X, Y, String, MaxSize )
#define _etm_gfx_benchmark(Name)
#define _etm_gfx_benchmark_ok()

#endif

/** report end of series measures and reset MaxRelativeError
 */
void _etm_end_of_measures() {
    MaxRelativeError += 0;  // break here to read gathered maximum relative error of previous etm_within() calls
    MaxRelativeError = 0;   // reset statistics
    #ifdef TTC_GFX1
    _etm_gfx_benchmark_ok();
    #endif
}

typedef struct { // etm_test_data_t  - pair of test data for functions Y = f(A) taking one argument
    ttm_number Argument;  // argument to a ttc_math_*() function
    ttm_number Result;    // exact expected calculation result

} etm_test_data_t;

typedef struct { // etm_test_data_t  - pair of test data for functions Y = f(A,B)  taking two arguments
    ttm_number Argument1;  // argument #1 to a ttc_math_*() function
    ttm_number Argument2;  // argument #2 to a ttc_math_*() function
    ttm_number Result;     // exact expected calculation result

} etm_test_data2_t;

typedef struct { // etm_test_data_t  - pair of test data for functions Y = f(A,B,C)  taking three arguments
    ttm_number Argument1;  // argument #1 to a ttc_math_*() function
    ttm_number Argument2;  // argument #2 to a ttc_math_*() function
    ttm_number Argument3;  // argument #3 to a ttc_math_*() function
    ttm_number Result;     // exact expected calculation result

} etm_test_data3_t;

typedef struct { // etm_test_data_t  - pair of test data for functions f(ttm_number* X, ttm_number* Y, ttm_number Alpha)  taking three arguments and returning two values
    ttm_number Argument1;  // argument #1 to a ttc_math_*() function
    ttm_number Argument2;  // argument #1 to a ttc_math_*() function
    ttm_number Argument3;  // argument #1 to a ttc_math_*() function
    ttm_number Result1;    // expected return value #1 from a ttc_math_*() function
    ttm_number Result2;    // expected return value #2 from a ttc_math_*() function
} etm_test_data3x2_t;

typedef struct { // etm_test_data_t  - pair of test data for functions f(ttm_number* X, ttm_number* Y, ttm_number* Z, ttm_number Alpha)  taking four arguments and returning three values
    ttm_number Argument1;  // argument #1 to a ttc_math_*() function
    ttm_number Argument2;  // argument #2 to a ttc_math_*() function
    ttm_number Argument3;  // argument #3 to a ttc_math_*() function
    ttm_number Argument4;  // argument #4 to a ttc_math_*() function
    ttm_number Result1;    // expected return value #1 from a ttc_math_*() function
    ttm_number Result2;    // expected return value #2 from a ttc_math_*() function
    ttm_number Result3;    // expected return value #3 from a ttc_math_*() function
} etm_test_data4x3_t;
typedef struct { // etm_test_laterate2d_t - test data for ttc_math_lateration_2d()
    BOOL LeftSide;      // ==TRUE: select second mathematical solution
    t_s32 Fx;           // x-coordinate of fixed node
    t_s32 Fy;           // y-coordinate of fixed node
    t_s32 Nx;           // expected Nx value
    t_s32 Ny;           // expected Ny value
} etm_test_laterate2d_t;

/** Run set of benchmarks on given mathematical approximation functions
 *
 * Note: Will assert if return of Function() is not within current Precision
 *
 * @param Function   ttc_math_*() function to benchmark (Y = f(A); A,Y are floating point values)
 * @param TestDatas  Array of { A, Y } pairs. A = argument to Function(), Y = expected result from Function(A).
 *                   For each A, Function(A) is calculated and checked if it is close egnough to Y.
 */
void _etm_benchmark1( ttm_number( *Function )( ttm_number ), const etm_test_data_t* TestDatas, const char* Name ) {
    const etm_test_data_t* TestData = NULL;

    _etm_gfx_benchmark( Name );

    ttm_number Result_FP;
    t_u8 Index = 0;
    TestData = & ( TestDatas[Index] );
    while ( ( TestData->Argument != 0 ) || ( TestData->Result != 0 ) ) { // run all tests

        for ( t_u16 I = Runs; I > 0; I-- ) { // run function given times to allow more precise runtime measurement

            Result_FP = Function( TestData->Argument );
        }
        BOOL TestPassed = etm_within( Result_FP, TestData->Result, etm_Precision );
        if ( !TestPassed ) { _etm_gfx_printf1_at( -2, regression_ttc_math_TextY, "%02d", Index ); }
        Assert( TestPassed );

        Index++;
        TestData = & ( TestDatas[Index] );
    }
    _etm_end_of_measures();
}

/** Run set of benchmarks on given mathematical approximation functions
 *
 * Note: Will assert if return of Function() is not within current Precision
 *
 * @param Name       name of benchmarked function (written on display if available)
 * @param Function   ttc_math_*() function to benchmark (Y = f(A,B); A,B,Y are floating point values)
 * @param TestDatas  Array of { A, B, Y } pairs. A,B = argument to Function(), Y = expected result from Function(A).
 *                   For each A and B, Function(A,B) is calculated and checked if it is close egnough to Y.
 */
void _etm_benchmark2( ttm_number( *Function )( ttm_number, ttm_number ), const etm_test_data2_t* TestDatas, const char* Name ) {
    const etm_test_data2_t* TestData = NULL;

    _etm_gfx_benchmark( Name );

    ttm_number Result_FP;
    t_u8 Index = 0;
    TestData = & ( TestDatas[Index] );
    while ( ( TestData->Argument1 != 0 ) || ( TestData->Argument2 != 0 ) || ( TestData->Result != 0 ) ) { // run all tests

        for ( t_u16 I = Runs; I > 0; I-- ) { // run function given times to allow more precise runtime measurement

            Result_FP = Function( TestData->Argument1, TestData->Argument2 );
        }
        BOOL TestPassed = etm_within( Result_FP, TestData->Result, etm_Precision );
        if ( !TestPassed ) { _etm_gfx_printf1_at( -2, regression_ttc_math_TextY, "%02d", Index ); }
        Assert( TestPassed );

        Index++;
        TestData = & ( TestDatas[Index] );
    }
    _etm_end_of_measures();
}
void _etm_benchmark3( ttm_number( *Function )( ttm_number, ttm_number, ttm_number ), const etm_test_data3_t* TestDatas, const char* Name ) {
    const etm_test_data3_t* TestData = NULL;

    _etm_gfx_benchmark( Name );

    ttm_number Result_FP;
    t_u8 Index = 0;
    TestData = & ( TestDatas[Index] );
    while ( ( TestData->Argument1 != 0 ) || ( TestData->Argument2 != 0 ) || ( TestData->Result != 0 ) ) { // run all tests

        for ( t_u16 I = Runs; I > 0; I-- ) { // run function given times to allow more precise runtime measurement

            Result_FP = Function( TestData->Argument1, TestData->Argument2, TestData->Argument3 );
        }
        BOOL TestPassed = etm_within( Result_FP, TestData->Result, etm_Precision );
        if ( !TestPassed ) { _etm_gfx_printf1_at( -2, regression_ttc_math_TextY, "%02d", Index ); }
        Assert( TestPassed );

        Index++;
        TestData = & ( TestDatas[Index] );
    }
    _etm_end_of_measures();
}
void _etm_benchmark2_fu( ttm_number( *Function )( ttm_number, t_u8 ), const etm_test_data2_t* TestDatas, const char* Name ) {
    const etm_test_data2_t* TestData = TestDatas;

    _etm_gfx_benchmark( Name );

    ttm_number Result_FP;
    t_u8 Index = 0;
    TestData = & ( TestDatas[Index] );
    while ( ( TestData->Argument1 != 0 ) || ( TestData->Argument2 != 0 ) || ( TestData->Result != 0 ) ) { // run all tests

        for ( t_u16 I = Runs; I > 0; I-- ) { // run function given times to allow more precise runtime measurement
            Result_FP = Function( TestData->Argument1, ( t_u8 ) TestData->Argument2 );
        }
        BOOL TestPassed = etm_within( Result_FP, TestData->Result, etm_Precision );
        if ( !TestPassed ) { _etm_gfx_printf1_at( -2, regression_ttc_math_TextY, "%02d", Index ); }
        Assert( TestPassed );

        Index++;
        TestData = & ( TestDatas[Index] );
    }
    _etm_end_of_measures();
}
void _etm_benchmark3x2( void ( *Function )( ttm_number*, ttm_number*, ttm_number ), const etm_test_data3x2_t* TestDatas, const char* Name ) {
    const etm_test_data3x2_t* TestData = NULL;

    _etm_gfx_benchmark( Name );

    t_u8 Index = 0;
    TestData = & ( TestDatas[Index] );
    while ( ( TestData->Argument1 != 0 ) || ( TestData->Argument2 != 0 ) || ( TestData->Argument3 != 0 ) ) { // run all tests
        ttm_number Argument1;
        ttm_number Argument2;
        for ( t_u16 I = Runs; I > 0; I-- ) { // run function given times to allow more precise runtime measurement
            Argument1 = TestData->Argument1;
            Argument2 = TestData->Argument2;
            Function( &Argument1, &Argument2, TestData->Argument3 );
        }
        BOOL TestPassedX = etm_within( Argument1, TestData->Result1, etm_Precision );
        BOOL TestPassedY = etm_within( Argument2, TestData->Result2, etm_Precision );
        BOOL TestPassed = TestPassedX && TestPassedY;
        if ( !TestPassed ) { _etm_gfx_printf1_at( -2, regression_ttc_math_TextY, "%02d", Index ); }
        Assert( TestPassed );

        Index++;
        TestData = & ( TestDatas[Index] );
    }
    _etm_end_of_measures();
}
ttm_number grad2rad( t_u8 Grad ) {
    return Grad * PI / 180;
}

//InsertPrivateFunctionDefinitions  above (DO NOT DELETE THIS LINE!)

void regression_ttc_math_start() {
    /** Note: The start() function is called in single task mode before the scheduler is started.
      *       It has to return to startup multitasking and run the tasks being created here.
      *       This function may run indefinitely if multitasking is not required.
      */

    // initialize gfx display (if available)
    _etm_gfx_init( );

    #ifdef TTC_LED1
    ttc_gpio_init( TTC_LED1, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max );
    #endif

    // Set precision of all approximation algorithms.
    // Results will be approximated +- precision.
    // Check description of low-level driver for benchmark results!
    etm_Precision = ttc_math_set_precision( tmp_1m );

    _etm_gfx_printf1_at( 0, 0, "TTC_Math P=%dm", etm_Precision * 1000 );

    ttm_number Result_FP;
    t_u32 Result_u32;

    while ( 1 ) {
        #ifdef TTC_LED1
        ttc_gpio_set( TTC_LED1 );
        #endif

        if ( 1 ) { // test FP Modulo
            const etm_test_data2_t TestDatas[11] = { // data to test function with
                {  101,      10,      1   },
                {  101.0,    10.0,    1   },
                {  100,       3.33,   0.1 },
                {  1000.3,    1,      0.3 },
                {  100 * PI, PI,      0   },
                {  123 * PI, PI / 2,  0   },
                {  123456,   45,     21   },
                {  666,     660,      6   },
                {  10000 + 3, 10,     3   },
                {  0x1001,    2,      1   },
                { 0, 0, 0 }                    // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark2( &ttc_math_modulo, TestDatas, "modulo" );
        }
        if ( 1 ) { // test reducing radiant angle within -PI/2 ... +PI/2
            const etm_test_data_t TestDatas[11] = { // data to test function with
                {           PI,        PI       },
                {           PI / 2,    PI / 2   },
                {          -PI / 2,   -PI / 2   },
                {          -PI,       -PI       },
                {       3 * PI + 0.1, -PI + 0.1 },
                {       3 * PI - 0.1,  PI - 0.1 },
                {     100 * PI + 1,   -PI + 1   },
                {     100 * PI - 1,       - 1   },
                {    1000 * PI + 2,         2   },
                {    1000 * PI - 2,       - 2   },
                { 0, 0 }                    // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark1( &ttc_math_angle_reduce, TestDatas, "angle_reduce" );
        }
        if ( 1 ) { // test square root

            const etm_test_data_t TestDatas[11] = { // data to test function with
                {  100,    10               },
                {   23,     4.79583152331   },
                { 3000,    54.7722557505    },
                { 7746,    88.0113629028    },
                { 2.1234,   1.4571890749    },
                { 355521, 596.255817582     },
                { 7735,    87.9488487702    },
                { 0.001,    0.0316227766017 },
                { 0.999,    0.999499874937  },
                { 1.001,    1.00049987506   },
                { 0, 0 }                    // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark1( &ttc_math_sqrt, TestDatas, "sqrt" );
        }
        if ( 1 ) { // test length_3d

            const etm_test_data3_t TestDatas[11] = { // data to test function with
                { 10, 0, 0, 10 },
                {  0, 10, 0, 10 },
                {  0, 0, 10, 10 },
                { 10, 10, 0, 14.1421356237 },
                {  0, 10, 10, 14.1421356237 },
                { 10, 0, 10, 14.1421356237 },
                { 10, 10, 10, 17.3205080757 },
                { 1000, 2000, 3000, 3741.65738677 },
                { 2000, 1000, 3000, 3741.65738677 },
                { 3000, 1000, 2000, 3741.65738677 },
                { 0, 0, 0, 0 }                    // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark3( &ttc_math_vector3d_length, TestDatas, "length3d" );
        }
        if ( 1 ) { // test ttc_math_sin(x)
            const etm_test_data_t TestDatas[11] = { // data to test function with
                {            PI / 2,  1              },
                {          - PI / 2, -1              },
                {            PI / 4,  0.707106781171 },
                {          - PI / 4, -0.707106781171 },
                {        0.463647  ,  0.447213050793 },
                {        1         ,  0.841470984808 },
                {        2         ,  0.909297426826 },
                {      100         , -0.50636564111  },
                {     1000         ,  0.826879540532 },
                {  1000000         , -0.349993502171 },
                { 0, 0 }                    // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark1( &ttc_math_sin, TestDatas, "sin" );

            // check special value (0, 0 is end sentinel and cannot be used in TestDatas[])
            Result_FP = ttc_math_sin( 0 );
            Assert( etm_within( Result_FP, 0, etm_Precision ) );
        }
        if ( 1 ) { // test ttc_math_cos(x)
            const etm_test_data_t TestDatas[11] = { // data to test function with
                {            PI / 2,  0              },
                {          - PI / 2,  0              },
                {            PI / 4,  0.707106781171 },
                {          - PI / 4, -0.707106781171 },
                {        0.463647  ,  0.894427463353 },
                {        0         ,  1              },
                {        2         , -0.416146836547 },
                {      100         ,  0.862318872288 },
                {     1000         ,  0.562379076291 },
                {    10000         , -0.952155368259 },
                { 0, 0 }                    // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark1( &ttc_math_cos, TestDatas, "cos" );
        }
        if ( 1 ) { // test arcus tangens
            const etm_test_data_t TestDatas[11] = { // data to test function with
                {            PI / 2,  1              },
                {          - PI / 2, -1              },
                {            PI / 4,  0.665773750028 },
                {          - PI / 4, -0.665773750028 },
                {        0.463647  ,  0.43414463892  },
                {        1         ,  PI / 4         },
                {        2         ,  1.10714871779  },
                {      100         ,  1.56079666011  },
                {     1000         ,  1.56979632713  },
                {  1000000         ,  1.57079532679  },
                { 0, 0 }                    // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark1( &ttc_math_arctan, TestDatas, "arctan" );
        }
        if ( 1 ) { // test arcus sinus
            const etm_test_data_t TestDatas[11] = { // data to test function with
                {  1,     PI / 2 },
                { -1,   - PI / 2 },
                {  0.12,  0.120289882395 },
                { -0.12, -0.120289882395 },
                {  0.25,  0.252680255142 },
                { -0.25, -0.252680255142 },
                {  0.5,   0.523598775598 },
                { -0.5,  -0.523598775598 },
                {  0.75,  0.848062078981 },
                { -0.75, -0.848062078981 },
                { 0, 0 }                    // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark1( &ttc_math_arcsin, TestDatas, "arcsin" );
        }
        if ( 1 ) { // test arcus cosinus
            const etm_test_data_t TestDatas[11] = { // data to test function with
                {  1.00,  0 },
                { -1.00,  PI },
                {  0.12,  1.4505064444 },
                { -0.12, -1.4505064444 },
                {  0.25,  1.31811607165 },
                { -0.25, -1.31811607165 },
                {  0.00,  PI / 2 },
                {  0.50,  1.0471975512 },
                { -0.50,  2.09439510239 },
                {  0.75,  0.722734247813 },
                { 0, 0 }                    // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark1( &ttc_math_arccos, TestDatas, "arccos" );
        }
        if ( 1 ) { // test tangens
            const etm_test_data_t TestDatas[11] = { // data to test function with
                {        PI      ,  0              },
                {        PI / 4  ,  1              },
                {       -PI      ,  0              },
                {       -PI / 4  , -1              },
                {        0.463647,  0.5            },
                {        1       ,  1.55740772465  },
                {        2       , -2.18503986326  },
                {      100       , -0.587213915157 },
                {     1000       ,  1.4703241557   },
                {  1000000       , -0.373624453988 },
                { 0, 0 }                    // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark1( &ttc_math_tan, TestDatas, "tan" );
        }
        if ( 1 ) { // test angle 2d
            ttm_number dX = 100;

            const char* TestName = "angle_2d";
            _etm_gfx_benchmark( TestName );

            for ( ttm_number dY = 1; dY <= dX; dY += 1 ) { // check symmetry

                // calculatoing angles in degrees for easier debugging
                ttm_number Angle1 = 180 / PI * ttc_math_vector2d_angle( dX,  dY );
                ttm_number Angle2 = 180 / PI * ttc_math_vector2d_angle( dY,  dX ); // mirrored at 45° line
                ttm_number Angle3 = 180 / PI * ttc_math_vector2d_angle( dX, -dY ); // mirrored at x-axis
                ttm_number Angle4 = 180 / PI * ttc_math_vector2d_angle( dY, -dX ); // mirrored at x-axis and -45° line
                ttm_number Angle5 = 180 / PI * ttc_math_vector2d_angle( -dX,  dY ); // mirrored at y-axis
                ttm_number Angle6 = 180 / PI * ttc_math_vector2d_angle( -dY,  dX ); // mirrored at y-axis and 135° line
                ttm_number Angle7 = 180 / PI * ttc_math_vector2d_angle( -dX, -dY ); // mirrored at x- and y-axis
                ttm_number Angle8 = 180 / PI * ttc_math_vector2d_angle( -dY, -dX ); // mirrored at x- and y-axis and -135° line

                Assert( etm_within( + 90 - Angle1, Angle2, etm_Precision ) );     // check implementation for angle € ]+ 45°, + 90°[ = ]   PI/4,  PI  /2[
                Assert( etm_within( +  0 - Angle1, Angle3, etm_Precision ) );       // check implementation for angle € ]   0°, - 45°[ = ]      0, -PI  /4[
                Assert( etm_within( - 90 + Angle1, Angle4, etm_Precision ) ); // check implementation for angle € ]- 45°, - 90°[ = ]-PI  /4, -PI  /2[
                Assert( etm_within( +180 - Angle1, Angle5, etm_Precision ) );   // check implementation for angle € ]+135°, +180°[ = ] PI*3/4,  PI    [
                Assert( etm_within( + 90 + Angle1, Angle6, etm_Precision ) );     // check implementation for angle € ]+ 90°, +135°[ = ] PI  /2,  PI*3/4[
                Assert( etm_within( -180 + Angle1, Angle7, etm_Precision ) ); // check implementation for angle € ]-135°, -180°[ = ]-PI*3/4, -PI    [
                Assert( etm_within( - 90 - Angle1, Angle8, etm_Precision ) ); // check implementation for angle € ]- 90°, -135°[ = ]-PI  /2, -PI*3/4[
            }
            const etm_test_data2_t TestDatas[35] = { // data to test function with
                {  1,      1,       PI / 4             }, //  0: + 45°
                {  1,     -1,      -PI / 4             }, //  1: - 45°
                { -1,      1,       PI * 3 / 4         }, //  2:  135°
                { -1,     -1,      -PI * 3 / 4         }, //  3: -135°
                {  0,      1,       PI / 2             }, //  4: + 90°
                {  0,     -1,      -PI / 2             }, //  5: - 90°
                {  1,      0,       0                  }, //  6:    0°
                { -1,      0,      -PI                 }, //  7:  180°
                {  2,      1,       0.463647609001     }, //  8: + 26,5650511771°
                {  2,     -1,      -0.463647609001     }, //  9: - 26,5650511771°
                { -2,      1,       2.677945044588987  }, // 10: +153,4349488273075°
                { -2,     -1,      -2.677945044588987  }, // 11: -153,4349488273075°
                {  1,      2,       1.10714871779      }, // 12: + 63,4349488247°
                {  1,     -2,      -1.10714871779      }, // 13: - 63,4349488247°
                { -1,      2,       2.0344439357957027 }, // 14: +116,56505118040965°
                { -1,     -2,      -2.0344439357957027 }, // 15: -116,56505118040965°
                { -1000,  -998,    -2.3571954908590107 }, // 16: -135.05735311734995°
                {  111.11, 333.33,  1.2490457723982544 }, // 17:  71.56505117912347°
                {  1000,   998,     0.7843971627307825 }, // 18:  44.94264688779482°
                {  1000,   1002,    0.7863971640641142 }, // 19:  45.057238523218544°
                {  1000,  -998,    -0.7843971627307825 }, // 20: -44.94264688779482°
                {  1000,  -1002,   -0.7863971640641142 }, // 21: -45.057238523218544°
                { -1000,   998,     2.3571954908590107 }, // 22:  135.05735311734995°
                { -1000,   1002,    2.3551954895256793 }, // 23:  134.94276148192625°
                { -1000,  -998,    -2.3571954908590107 }, // 24: -135.05735311734995°
                { -1000,  -1002,   -2.3551954895256793 }, // 25: -134.94276148192625°
                {  1000,     2,  0.0019999973333397333 }, // 26:    0.11459140624106122°
                {  1000,    -2, -0.0019999973333397333 }, // 27:   -0.11459140624106122°
                { -1000,     2,  3.1395926562564536    }, // 28:  179.88540859890372°
                { -1000,    -2, -3.1395926562564536    }, // 29: -179.88540859890372°
                {     2,  1000,  1.5687963294615568    }, // 30:   89.88540859633132°
                {    -2,  1000,  1.5727963241282363    }, // 31:   90.11459140881345°
                {     2, -1000, -1.5687963294615568    }, // 32:  -89.88540859633132°
                {    -2, -1000, -1.5727963241282363    }, // 33:  -90.11459140881345°

                { 0, 0, 0 }                     // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark2( &ttc_math_vector2d_angle, TestDatas, TestName );
        }
        if ( 1 ) { // test rotate_2d
            const char* TestName = "rotate_2d";
            _etm_gfx_benchmark( TestName );

            const etm_test_data3x2_t TestDatas[12] = { // data to test function with
                {  1, 0,  PI / 2,    0,  1 },                        //  0: vector length 1 at +90°
                {  1, 0, -PI / 2,    0, -1 },                        //  1: vector length 1 at -90°
                {  1, 0,  PI / 1,   -1,  0 },                        //  2: vector length 1 at +180°
                {  1, 0, -PI / 1,   -1,  0 },                        //  3: vector length 1 at -180°
                {  1, 0,  PI / 4,    0.707106781187,  0.707106781187 }, //  4: vector length 1 at +45°
                {  1, 0, -PI / 4,    0.707106781187, -0.707106781187 }, //  5: vector length 1 at -45°
                {  1, 0,  PI * 3 / 4, -0.707106781187,  0.707106781187 }, //  6: vector length 1 at +135°
                {  1, 0, -PI * 3 / 4, -0.707106781187, -0.707106781187 }, //  7: vector length 1 at -135°
                { 300, 200, 23 * PI / 180, 198.005230338, 301.320309237},                       // 8: vector of length 360.555127546 and angle 33.690067526° rotated to +56.690067526°
                { 360.555127546, 0, 56.690067526 * PI / 180, 198.005230338, 301.320309237},     // 9: vector of length 360.555127546 and angle 33.690067526° rotated to +56.690067526°
                { 0, 360.555127546, ( 56.690067526 - 90 )* PI / 180, 198.005230338, 301.320309237}, //10: vector of length 360.555127546 and angle 33.690067526° rotated to +56.690067526°
                { 0, 0, 0, 0, 0 }                     // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark3x2( &ttc_math_vector2d_rotate, TestDatas, TestName );
        }
        if ( 1 ) { // test cartesian <-> polar conversians
            _etm_gfx_benchmark( "c2pol" );

            ttm_number Angle;
            ttm_number Length;

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_cartesian2polar( 0, 0, &Angle, &Length ); }
            Assert( etm_within( Angle,  0, etm_Precision ) );
            Assert( etm_within( Length, 0, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_cartesian2polar( 10, 0, &Angle, &Length ); }
            Assert( etm_within( Angle,  0,  etm_Precision ) );
            Assert( etm_within( Length, 10, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_cartesian2polar( -10, 0, &Angle, &Length ); }
            Assert( etm_within( Angle,  PI,  etm_Precision ) );
            Assert( etm_within( Length, 10, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_cartesian2polar( 0, 10, &Angle, &Length ); }
            Assert( etm_within( Angle,  PI / 2,  etm_Precision ) );
            Assert( etm_within( Length, 10, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_cartesian2polar( 10, 10, &Angle, &Length ); }
            Assert( etm_within( Angle,  PI / 4,        etm_Precision ) );
            Assert( etm_within( Length, 14.1421356237, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_cartesian2polar( -10, 10, &Angle, &Length ); }
            Assert( etm_within( Angle,  - PI / 4,      etm_Precision ) );
            Assert( etm_within( Length, 14.1421356237, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_cartesian2polar( 10, -10, &Angle, &Length ); }
            Assert( etm_within( Angle,  - PI / 4,    etm_Precision ) );
            Assert( etm_within( Length, 14.1421356237, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_cartesian2polar( -10, -10, &Angle, &Length ); }
            Assert( etm_within( Angle,  - PI * 3 / 4,  etm_Precision ) );
            Assert( etm_within( Length, 14.1421356237, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_cartesian2polar( 111.11, 333.33, &Angle, &Length ); }
            Assert( etm_within( Angle,   1.2490457724, etm_Precision ) );
            Assert( etm_within( Length, 351.363833099, etm_Precision ) );

            ttm_number X, Y, X2, Y2;

            for ( t_u8 AngleGrad = 0; AngleGrad <= 45; AngleGrad++ ) { // check symmetry of ttc_math_polar2cartesian()

                // reference vector
                ttc_math_polar2cartesian( AngleGrad         * PI / 180, 1, &X, &Y );

                // mirror vector at  0° line
                ttc_math_polar2cartesian( ( - AngleGrad )     * PI / 180, 1, &X2, &Y2 );
                Assert( etm_within( X,  X2, etm_Precision ) );
                Assert( etm_within( Y, -Y2, etm_Precision ) );

                // mirror vector at 45° line
                ttc_math_polar2cartesian( ( 90 - AngleGrad )  * PI / 180, 1, &X2, &Y2 );
                Assert( etm_within( X,  Y2, etm_Precision ) );
                Assert( etm_within( Y,  X2, etm_Precision ) );

                // mirror vector at -45° line
                ttc_math_polar2cartesian( ( -90 + AngleGrad )  * PI / 180, 1, &X2, &Y2 );
                Assert( etm_within( X, -Y2, etm_Precision ) );
                Assert( etm_within( Y, -X2, etm_Precision ) );

                // rotate vector +90°
                ttc_math_polar2cartesian( ( 90 + AngleGrad )  * PI / 180, 1, &X2, &Y2 );
                Assert( etm_within( X,  Y2, etm_Precision ) );
                Assert( etm_within( Y,  X2, etm_Precision ) );

                // rotate vector -90°
                ttc_math_polar2cartesian( ( -90 - AngleGrad )  * PI / 180, 1, &X2, &Y2 );
                Assert( etm_within( X, -Y2, etm_Precision ) );
                Assert( etm_within( Y, X2, etm_Precision ) );

                // mirror vector at 90° line
                ttc_math_polar2cartesian( ( 180 - AngleGrad )  * PI / 180, 1, &X2, &Y2 );
                Assert( etm_within( X,  -X2, etm_Precision ) );
                Assert( etm_within( Y,   Y2, etm_Precision ) );

                // mirror vector at 0° and 90° line
                ttc_math_polar2cartesian( ( 180 + AngleGrad )  * PI / 180, 1, &X2, &Y2 );
                Assert( etm_within( X, -X2, etm_Precision ) );
                Assert( etm_within( Y, -Y2, etm_Precision ) );
            }

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_polar2cartesian( 0, 0, &X, &Y ); }
            Assert( etm_within( X, 0, etm_Precision ) );
            Assert( etm_within( Y, 0, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_polar2cartesian( 0, 10, &X, &Y ); }
            Assert( etm_within( X, 10, etm_Precision ) );
            Assert( etm_within( Y, 0, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_polar2cartesian( PI, 10, &X, &Y ); }
            Assert( etm_within( X, -10, etm_Precision ) );
            Assert( etm_within( Y, 0, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_polar2cartesian( PI / 2, 10, &X, &Y ); }
            Assert( etm_within( X, 0, etm_Precision ) );
            Assert( etm_within( Y, 10, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_polar2cartesian( -PI / 2, 10, &X, &Y ); }
            Assert( etm_within( X, 0, etm_Precision ) );
            Assert( etm_within( Y, -10, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_polar2cartesian( PI / 4, 14.1421356237, &X, &Y ); }
            Assert( etm_within( X, 10, etm_Precision ) );
            Assert( etm_within( Y, 10, etm_Precision ) );

            for ( t_u8 I = Runs; I > 0; I-- )
            { ttc_math_polar2cartesian( 1.2490457724, 351.363833099, &X, &Y ); }
            Assert( etm_within( X, 111,  etm_Precision ) ); // ToDo: should be 111.11
            Assert( etm_within( Y, 333.33, etm_Precision ) );

            _etm_end_of_measures( );
        }
        if ( 1 ) { // test exponential
            const etm_test_data2_t TestDatas[11] = { // data to test function with
                {  2,     10, 1024             },
                {  10,    10,    1e10          },
                {  1.23,   4,    2.2888        },
                {  PI,     3,   31.0062766803  },
                {  PI / 2, 6,   15.0217061496  },
                {  PI,     2,    9.86960440109 },
                {  EULER,  6,  403.428793493   },
                {  GOLDEN, 5,   11.0901699437  },
                {  GOLDEN, 3,    4.2360679775  },
                {  EULER,  2,    7.38905609893 },
                { 0, 0, 0 }                    // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark2_fu( &ttc_math_pow, TestDatas, "pow" );
        }
        if ( 1 ) { // test integer logarithmic
            _etm_gfx_benchmark( "int_rank" );
            t_u8 Rank;

            Rank = ttc_basic_int_rankN( 1000000, 10 );
            Assert( Rank == 7 );

            Rank = ttc_basic_int_rankN( 999999, 10 );
            Assert( Rank == 6 );

            Rank = ttc_basic_int_rankN( 1024, 2 );
            Assert( Rank == 11 );
            _etm_end_of_measures( );
        }
        if ( 1 ) { // test safe combination of integer multiplication and division
            _etm_gfx_benchmark( "mult_div3" );

            // Result = 1000000 * 100000 / 100000
            Result_u32 = ttc_basic_multiply_divide_u32x3( 1000000, 100000, 100000 );
            Assert( Result_u32 == 1000000 );

            // Result = 10 * 500000000 / 100000
            Result_u32 = ttc_basic_multiply_divide_u32x3( 10, 500000000, 100000 );
            Assert( Result_u32 == 50000 );

            // test some special cases
            // Result = 1 * 500000000 / 100000
            Result_u32 = ttc_basic_multiply_divide_u32x3( 1, 500000000, 100000 );
            Assert( Result_u32 == 5000 );

            // Result = 500000000 * 1 / 100000
            Result_u32 = ttc_basic_multiply_divide_u32x3( 500000000, 1, 100000 );
            Assert( Result_u32 == 5000 );

            // Result = 100 * 100 / 1
            Result_u32 = ttc_basic_multiply_divide_u32x3( 100, 100, 1 );
            Assert( Result_u32 == 10000 );
            _etm_end_of_measures( );
        }
        if ( 1 ) { // test inverse square root
            const etm_test_data_t TestDatas[11] = { // data to test function with
                {  100,   0.1 },
                {   23,   0.208514414057 },
                { 3000,   0.0182574185835 },
                { 7746,   0.0113621692361 },
                { 2.1234, 0.686252743194 },
                { 355521, 0.00167713248326 },
                { 7735,   0.0113702454777 },
                { 0.001, 31.6227766017 },
                { 0.999,  1.00050037531 },
                { 1.001,  0.999500374688 },
                { 0, 0 }                    // end sentinel (DO NOT REMOVE!)
            };

            _etm_benchmark1( &ttc_math_inv_sqrt, TestDatas, "inv_sqrt" );
        }
        if ( 1 ) { // test lateration_2d
            const char* TestName = "lateration2d";
            _etm_gfx_benchmark( TestName );

            const t_s32 Range = 1000;
            const t_s32 Steps = 100;
            BOOL TestPassed_Left, TestPassed_Right;

            struct {
                ttm_number  Nx_real;  // coordinates of simulated test node
                ttm_number  Ny_real;
                ttm_number  Fx;       // coordinates of fixed node
                ttm_number  Fy;
                ttm_number  N1x;  // left side calculated coordinates of laterated node
                ttm_number  N1y;
                ttm_number  N2x; // right side calculated coordinates of laterated node
                ttm_number  N2y;
                ttm_number  DistanceFN; // distance fixed node -> node to laterate
                ttm_number  DistanceON; // distance origin -> node to laterate
            } Data;

            t_u16 Percent = 0;
            ttm_number PercentAdd = 100.0 * Steps / ( 2 * Range );

            // try all combinations of all coordinates of (Nx_real, Ny_real) and (Fx, Fy) in 2 dimensions from -Range..+Range
            for ( Data.Nx_real = -Range; Data.Nx_real < Range; Data.Nx_real += Steps ) {
                for ( Data.Ny_real = -Range; Data.Ny_real < Range; Data.Ny_real += Steps ) {
                    Data.DistanceON = ttc_math_length_2d( Data.Nx_real, Data.Ny_real );

                    if ( Data.DistanceON > 0 ) {
                        for ( Data.Fx = -Range; Data.Fx < Range; Data.Fx += Steps ) {
                            for ( Data.Fy = -Range; Data.Fy < Range; Data.Fy += Steps ) {
                                if ( ( ( Data.Fx != Data.Nx_real ) || ( Data.Fy != Data.Ny_real ) ) &&
                                        ( ( Data.Fx != 0 ) || ( Data.Fy != 0 ) )
                                   ) {
                                    // measure distances O->N, F->N
                                    Data.DistanceFN = ttc_math_length_2d( Data.Nx_real - Data.Fx, Data.Ny_real - Data.Fy );

                                    ttc_math_lateration_2d( Data.DistanceON, Data.DistanceFN, Data.Fx, Data.Fy, &Data.N1x, &Data.N1y, &Data.N2x, &Data.N2y );

                                    TestPassed_Left  = etm_within_int( Data.Nx_real, Data.N1x, 1 ) &&
                                                       etm_within_int( Data.Ny_real, Data.N1y, 1 );

                                    TestPassed_Right = etm_within_int( Data.Nx_real, Data.N2x, 1 ) &&
                                                       etm_within_int( Data.Ny_real, Data.N2y, 1 );

                                    Assert( TestPassed_Left || TestPassed_Right ); // at least one side should match!
                                }
                            }
                        }
                    }
                }
                Percent += PercentAdd;
                _etm_gfx_printf1_at( ttc_gfx_get_configuration( 1 )->TextColumns - 2,
                                     regression_ttc_math_TextY,
                                     "%02d",
                                     Percent
                                   );
            }
            _etm_end_of_measures();
        }
        if ( 1 ) {
            /* test lateration_3d
                /* regresseion test for lateration_3d()
                 *
                 * This regression uses the origin=(0,0,0) and three varying vectors:
                 *   F2 = (F2.x, F2.y, F2.z) = second fixed point
                 *   F3 = (F3.x, F3.y, F3.z) = third fixed point
                 *   N_real = (N_real.x, N_real.y, N_real.z) = moving node
                 *
                 * Each vector component x,y,z runs from -Range to +Range at step width Steps.
                 * The total amount of loop iterations is O(N^9).
                 * N = amount of individual values of each component.
                 *
                 * Each assignment of values to all vector components is called a configuration.
                 * A configuration is valid if none of O, F2, F3, N_real are equal.
                 *
                 * A regression test is applied for each valid configuation:
                 * 1) Distances are calculated:
                 *    DistanceON  = d(O, N_real)
                 *    DistanceF2N = d(F2, N_real)
                 *    DistanceF3N = d(F3, N_real)
                 *
                 * 2) ttc_math_lateration_3d() is called to recalculate 3D coordinates of N_real from
                 *    a) coordinates of F2
                 *    b) coordinates of F3
                 *    c) DistanceON
                 *    d) DistanceF2N
                 *    e) DistanceF3N
                 *
                 * 3) Results from ttc_math_lateration_3d() are stored in N1, N2
                 *    N1=(N1.x, N1.y, N1.z) = first possible mathematical solution from trilateration
                 *    N2=(N2.x, N2.y, N2.z) = second possible mathematical solution from trilateration
                 *
                 * 4) Test of current configuration passed successfully if
                 *    d(N1, N_real) < desired precision
                 *    or
                 *    d(N2, N_real) < desired precision
                 */
            const char* TestName = "lateration3d";
            _etm_gfx_benchmark( TestName );

            // Runtime = O( (Range/Steps)^9
            const t_s32 Range = 1000;
            const t_s16 Steps = 333;  // -> <262144 ttc_math_lateration_3d() calls
            BOOL TestPassed_Left, TestPassed_Right;

            struct {
                t_ttc_math_vector3d_xyz N_real; // coordinates of simulated test node
                t_ttc_math_vector3d_xyz F2;     // coordinates of second fixed point (first is origin)
                t_ttc_math_vector3d_xyz F3;     // coordinates of third  fixed point
                t_ttc_math_vector3d_xyz N1;     // first calculated coordinates of laterated node
                t_ttc_math_vector3d_xyz N2;     // second calculated coordinates of laterated node
                ttm_number DistanceON;          // distance origin        -> node to laterate
                ttm_number DistanceF2N;         // distance fixed node #2 -> node to laterate
                ttm_number DistanceF3N;         // distance fixed node #3 -> node to laterate
                ttm_number DistanceON1;         // distance origin        -> laterated node 1
                ttm_number DistanceF2N1;        // distance fixed node #2 -> laterated node 1
                ttm_number DistanceF3N1;        // distance fixed node #3 -> laterated node 1
                ttm_number DistanceON2;         // distance origin        -> laterated node 2
                ttm_number DistanceF2N2;        // distance fixed node #2 -> laterated node 2
                ttm_number DistanceF3N2;        // distance fixed node #3 -> laterated node 2
                t_u32      LoopCount;           // counts current loop run
                t_u32      LoopStop;            // >0: execute line which is reserved for breakpoint (use set Data.LoopStop=NNN to reconfigure during gdb session)
                t_u32      LoopsTotal;          // total amount of loops to run
            } Data;

            t_ttc_math_lateration_3d Cache; // required by ttc_math_lateration_3d()

            Data.LoopCount = 0;
            Data.LoopStop  = 0; // >0: used to aid debugging (allows breakpoint at exact loop iteration)
            t_u32 N = Range * 2 / Steps; // every loop adds a multiplier
            Data.LoopsTotal = N * N * N * N * N * N * N * N * N;
            t_s16 Permille = -1;
            t_s32 PermilleCount = 1;

            // Try all non trivial combinations of N_real, F2 and F3 in three dimensions from -Range..+Range
            for ( Data.F2.X = -Range; Data.F2.X < Range; Data.F2.X += Steps ) {
                for ( Data.F2.Y = -Range; Data.F2.Y < Range; Data.F2.Y += Steps ) {
                    for ( Data.F2.Z = -Range; Data.F2.Z < Range; Data.F2.Z += Steps ) {

                        for ( Data.F3.X = -Range; Data.F3.X < Range; Data.F3.X += Steps ) {
                            for ( Data.F3.Y = -Range; Data.F3.Y < Range; Data.F3.Y += Steps ) {
                                for ( Data.F3.Z = -Range; Data.F3.Z < Range; Data.F3.Z += Steps ) {
                                    ttm_number k; // Factor used to test if F3 = k * F2 (F3 and F2 must not be colinear!)
                                    if ( Data.F2.X != 0 ) { k = Data.F3.X / Data.F2.X; }
                                    else {
                                        if ( Data.F2.Y != 0 ) { k = Data.F3.Y / Data.F2.Y; }
                                        else {
                                            k = Data.F3.Z / Data.F2.Z;
                                        }
                                    }
                                    if (
                                        ( ( Data.F3.X != Data.F2.X ) || ( Data.F3.Y != Data.F2.Y ) )         && // F3 != F2
                                        ( ( Data.F3.X != 0 ) || ( Data.F3.Y != 0 ) )                         && // F3 is outside z-axis
                                        ( ( Data.F2.X * k != Data.F3.X ) || ( Data.F2.Y * k != Data.F3.Y ) || ( Data.F2.Z * k != Data.F3.Z ) ) && // F3 != k * F2
                                        ( ( Data.F2.X != 0 ) || ( Data.F2.Y != 0 ) )
                                    ) {
                                        BOOL FixPointsUnchanged = FALSE;
                                        for ( Data.N_real.X = -Range; Data.N_real.X < Range; Data.N_real.X += Steps ) {
                                            for ( Data.N_real.Y = -Range; Data.N_real.Y < Range; Data.N_real.Y += Steps ) {
                                                for ( Data.N_real.Z = -Range; Data.N_real.Z < Range; Data.N_real.Z += Steps ) {
                                                    Data.DistanceON = ttc_math_vector3d_length( Data.N_real.X, Data.N_real.Y, Data.N_real.Z );

                                                    PermilleCount--;
                                                    if ( Data.DistanceON > 0 ) {

                                                        if ( ( Data.F2.X != Data.N_real.X ) || ( Data.F2.Y != Data.N_real.Y ) ) {
                                                            // measure distance F2->N
                                                            Data.DistanceF2N = ttc_math_vector3d_distance( & Data.N_real, & Data.F2 );
                                                            if ( Data.DistanceF2N > 0 ) {
                                                                Data.LoopCount++;
                                                                while ( PermilleCount < 0 ) { // another 1/1000 th done (approximately)
                                                                    PermilleCount += Data.LoopsTotal / 1000;
                                                                    Permille++;
                                                                    _etm_gfx_printf1_at( ttc_gfx_get_configuration( 1 )->TextColumns - 3,
                                                                                         regression_ttc_math_TextY,
                                                                                         "%03d",
                                                                                         Permille
                                                                                       );
                                                                }
                                                                if (
                                                                    //( Data.LoopCount >= 0x3adc )                                          && // skip first n iterations (proceed to desired test scenario)
                                                                    ( ( Data.F3.X != Data.N_real.X ) || ( Data.F3.Y != Data.N_real.Y ) ) // F3 != N
                                                                ) {
                                                                    if ( Data.LoopCount == Data.LoopStop )
                                                                    { Data.LoopCount += 0; } // <-- place breakpoint here!

                                                                    // measure distance F2->N
                                                                    Data.DistanceF3N = ttc_math_vector3d_distance( & Data.N_real, & Data.F3 );

                                                                    // ttc_math_lateration_3d( &Cache, FALSE, &Data.F2, &Data.F3, Data.DistanceON, Data.DistanceF2N, Data.DistanceF3N, &Data.N1, &Data.N2 )
                                                                    BOOL OK = ttc_math_lateration_3d( &Cache, FixPointsUnchanged, // different fixed points on every call (cannot reuse Cache data)
                                                                                                      &Data.F2,
                                                                                                      &Data.F3,
                                                                                                      Data.DistanceON,
                                                                                                      Data.DistanceF2N,
                                                                                                      Data.DistanceF3N,
                                                                                                      &Data.N1,
                                                                                                      &Data.N2
                                                                                                    );

                                                                    if ( OK ) { // trilateration was successfull: verify calculated coordinates
                                                                        const ttm_number Precision = 100;
                                                                        if ( 1 ) { // compare laterated coordinates to real coordinates
                                                                            TestPassed_Left  = etm_within_int( Data.N_real.X, Data.N1.X, Data.N_real.X / Precision ) &&
                                                                                               etm_within_int( Data.N_real.Y, Data.N1.Y, Data.N_real.Y / Precision ) &&
                                                                                               etm_within_int( Data.N_real.Z, Data.N1.Z, Data.N_real.Z / Precision );

                                                                            TestPassed_Right = etm_within_int( Data.N_real.X, Data.N2.X, Data.N_real.X / Precision ) &&
                                                                                               etm_within_int( Data.N_real.Y, Data.N2.Y, Data.N_real.Y / Precision ) &&
                                                                                               etm_within_int( Data.N_real.Z, Data.N2.Z, Data.N_real.Z / Precision );
                                                                        }
                                                                        else { TestPassed_Left = TestPassed_Right = FALSE; }
                                                                        if ( !( TestPassed_Left || TestPassed_Right ) ) { // first test failed: compare distances from laterated coordinates to real distances (more fair test)
                                                                            Data.DistanceON1  = ttc_math_interface_vector3d_length( Data.N1.X, Data.N1.Y, Data.N1.Z );
                                                                            Data.DistanceF2N1 = ttc_math_interface_vector3d_distance( &Data .N1, &Data .F2 );
                                                                            Data.DistanceF3N1 = ttc_math_interface_vector3d_distance( &Data .N1, &Data .F3 );
                                                                            Data.DistanceON2  = ttc_math_interface_vector3d_length( Data.N2.X, Data.N2.Y, Data.N2.Z );
                                                                            Data.DistanceF2N2 = ttc_math_interface_vector3d_distance( &Data .N2, &Data .F2 );
                                                                            Data.DistanceF3N2 = ttc_math_interface_vector3d_distance( &Data .N2, &Data .F3 );

                                                                            TestPassed_Left  = etm_within_int( Data.DistanceON,  Data.DistanceON1,  Data.DistanceON  / Precision ) &&
                                                                                               etm_within_int( Data.DistanceF2N, Data.DistanceF2N1, Data.DistanceF2N / Precision ) &&
                                                                                               etm_within_int( Data.DistanceF3N, Data.DistanceF3N1, Data.DistanceF2N / Precision );
                                                                            TestPassed_Right = etm_within_int( Data.DistanceON,  Data.DistanceON2,  Data.DistanceON  / Precision ) &&
                                                                                               etm_within_int( Data.DistanceF2N, Data.DistanceF2N2, Data.DistanceF2N / Precision ) &&
                                                                                               etm_within_int( Data.DistanceF3N, Data.DistanceF3N2, Data.DistanceF2N / Precision );
                                                                        }
                                                                        else {
                                                                            Data.DistanceON1  = 0;
                                                                            Data.DistanceF2N1 = 0;
                                                                            Data.DistanceF3N1 = 0;
                                                                            Data.DistanceON2  = 0;
                                                                            Data.DistanceF2N2 = 0;
                                                                            Data.DistanceF3N2 = 0;
                                                                        }
                                                                        if ( !( TestPassed_Left || TestPassed_Right ) ) {
                                                                            _etm_gfx_printf1_at( ttc_gfx_get_configuration( 1 )->TextColumns - 2,
                                                                                                 regression_ttc_math_TextY,
                                                                                                 "EE",
                                                                                                 0
                                                                                               );
                                                                        }
                                                                        Assert( TestPassed_Left || TestPassed_Right ); // at least one side should match!
                                                                    }
                                                                    FixPointsUnchanged = TRUE; // fix points will not change for a while (makes ttc_math_lateration_3d() faster)
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    { PermilleCount--; }
                                }
                            }
                        }
                    }
                }
            }
            _etm_end_of_measures();
        }

        #ifdef TTC_LED1
        ttc_gpio_clr( TTC_LED1 );
        #endif
        ttc_task_msleep( 100 );
    }
}

//InsertFunctionDefinitions above (DO NOT DELETE THIS LINE!)
//}Function Definitions
