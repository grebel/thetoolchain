Interrupts

Motivation Interrupts

Do you remember what you did the last time, you were very eager to catch an important, incoming call on your land line phone?
You stayed in the vincinity of the phone and waited for the bell to ring. Hopefully, the call was worth the hours you spend doing virtually nothing.
A more modern approach is to forward incoming call to your mobile.

Microcontrollers get important calls all the time over dozens of lines. So they have to provide a good mechanism to listen for calls without 
spending any cpu time for it. In the computer universe, the processing of sporadic events is known as an interrupt. This means, that a special hardware unit 
in the cpu observes several input lines for incoming interrupt requests (IRQ). These requests can come from GPIO pins, timers, io-peripherals and many other sources.  
When a new interrupt request is detected, the cpu immediately stores its current context and jumps to a defined memory address to execute a so called interrupt 
service routine (ISR). This ISR has to find out what was the reason for the interrupt request, to save all related data and then inform the main application.

The golden rules for writing interrupt service routines are:
* keep it small
* keep it simple
* keep it fast

Application Side of Interrupts in TheToolChain

All available interrupt sources are handled centrally by ttc_interrupt.c/.h.
The main function for registering interrupts is ttc_interrupt_init(). This function will register a given function pointer for a single interrupt source.
The basic interrupt hardware is initialized and the given data is stored for later use. The interrupt is not enabled at this time.

<SOURCE>
<EXTRACT type="FunctionDeclaration" name="ttc_interrupt_init" src="../TTC-Library/ttc_interrupt.h">
</SOURCE>

The interrupt type is taken from a global enumeration. It lists all types of interrupt sources supported by any low-level driver.
Not all types may be available on your current hardware platform. Check return value of ttc_interrupt_init() to see if your interrupt
could be initialized successfully.

<SOURCE>
<EXTRACT type="Enum" name="e_ttc_interrupt_type" src="../TTC-Library/ttc_interrupt_types.h">
</SOURCE>

After being initialized, each interrupt has to be enabled. Enabling requires fewer data than initialization and is much faster.
Initialized interrupts can be enabled and disabled at any time. 

<SOURCE>
<EXTRACT type="FunctionDeclaration" name="ttc_interrupt_enable" src="../TTC-Library/ttc_interrupt.h">
</SOURCE>

Internals of Interrupt Handling in The ToolChain

Every microcontroller provides a vector table for interrupt handling. When an interrupt occurs, the corresponding table entry is used to jump to
the registered interrupt service routine. The ToolChain uses hardware specific low level drivers to implement basic interrupt service routines
for all entries in the interrupt table. The low level driver gathers basic data about the interrupt like its hardware index and interrupt source type.
It then uses the function pointer and argument stored for the current interrupt to call the interrupt service routine being registered earlier.
The function pointer and argument been given to ttc_interrupt_init() are stored in a dynamic structure maintained by ttc_interrupt.c. Traditionally, the
interrupt vector table would directly point to the final service routine.
The extra memory usage and calling level allow to share code among all low level interrupt drivers. It also provides a common interface for interrupt 
handling that is general for all supported hardware architectures. The induced performance overhead should be no more than two function calls and some 
table lookups. You may check the low-level implementation of your current architecture for details. 

Low Level Interrupt Driver

For every supported microcontroller architecture, a low level driver is required. Each driver provides low-level implementation to initialize, enable 
and to service some or all interrupts sources. If a low-level driver lacks some implementations, warning messages will be generated during compilation automatically.
Take a look at stm32_interrupt.c for a complete implementation.

Links

Explanation of Interrupt programming on STM32
  http://www.embedds.com/stm32-interrupts-and-programming-with-gcc/
  