H1: ToolChain Development

This chapter is intended for those who want to contribute to the development of The ToolChain.
Contribution means to write low-/ high-level drivers, install scripts or additional documentation.

One can easily download a recent version of the ToolChain and start to modify its files.
The unpractical thing about this approach is, that a ToolChain must be installed by 
starting its InstallData/installAll.sh script. This will download and create lots of 
new files and directories. After such a run, it is hard to find out, which files have
been generated and which are own creations or modifications.

ToolChain development is much easier by creating a so called development version. That 
is a copy of its directory structure + lots of symbolic links to the original ToolChain.
Every modification of a linked file is done in the original ToolChain folder. But every 
newly generated or downloaded file or directory is located inside the development folder.
As you might guess, the author does not like to create and update dozens of directories 
and hundreds of symbolic links for every new ToolChain release. Well, in the beginning 
he did. But then a script was born that does this automatically.

Important: No content tarball exists for development ToolChains!
These tarballs store files that may not be available for download from their original url anymore.
In most cases, you can use the contents from an older ToolChain version.
Just install one, if your ~/Source/TheToolChain.Contents/ folder is empty.

First you have to clone a toolchain git repository somewhere on your hard disc.
You may clone a public, readonly version by issuing
> git clone git://thetoolchain.com/TheToolChain

Then cd into this repository directory and run this script:
> InstallData/scripts/createDevelopmentVersion.pl

This will create a new development folder in ~/Source/TheToolChain_devel/.

BTW: If you don't like to have your ToolChains placed in ~/Source/ then simply 
move the folder somewhere else and create a symbolic link to it:
> cp -R ~/Source  /Data/MySource && rm -Rf ~/Source
> ln -sv /Data/MySource/ ~/Source

The newly created development ToolChain has to be installed like any other version by calling its installAll.sh:
> cd ~/Source/TheToolChain_devel/InstallData/
> ./installAll BASIC


H2: Managing GIT Repository

The script replace acts as a central management tool for the git repository. The script is self explanatory.
It can be called from anywhere in the filesystem.

<CODE>
<EXECUTE src="../../InstallData/scripts/replace">
</CODE>


H2: Updating GIT Repository

After adding or changing files in your local working copy, you may want to commit to the git repository.
As you might guess, a special script helps you with this.

<CODE>
<EXECUTE src="../../InstallData/scripts/ttc_commit.sh" arguments="--help">
</CODE>

The next step is to write your own driver as described in ->chapter_Drivers.tml .


H2: Handling Updates from other Developers

Several developers may work with a development version in parallel to you. They may rename, remove or create files any time.
When you pull an update, these changes are will lead to defective and/ or missing symbolic links in your Development ToolChain.
Every folder of your Development ToolChain contains a script called .updateLinks.sh. If you call it, it will reconstruct all symbolic 
links in its folder. Sometimes too many links have to be reconstructed or you are unsure, which folders have to be repaired. In this 
case it is easier to create a new Development ToolChain and install it. The example below shows all steps necessary to completely upgrade a Development ToolChain.

<CODE>
  cd ~/Source
  sudo rm -Rf TheToolChain_devel/
  cd ~/git/TheToolChain/
  InstallData/scripts/createDevelopmentVersion.pl
  cd ~/Source/TheToolChain_devel/InstallData/
  ./installAll.sh BASIC
</CODE>
