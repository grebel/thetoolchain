Directory Structure

   Two domains of directory structures exist:
   
   + Project Directory Structure
     This directory stores all data of a single project and a connection to 
     the corresponding toolchain release.
     All source files created for this project can be placed directly into this folder
     or into newly created subfolders. Take care to add search paths for source- and
     header-files into your extra settings makefile
     
     + subfolder additionals/
       Contains third party libraries provided by external sources.
       These libraries have been downloaded and installed by install scripts.
       The availability of these libraries depends on the corresponding install script.
       
     + subfolder ttc-lib/
       All source files provided by The ToolChain are placed here.
       The first level of ttc-lib/ stores only high-level ttc_* files.
       Low-level drivers are placed in subfolders named after their architecture
       (E.g. ttc-lib/stm32/ for stm32 microcontrollers).
       
     + subfolder configs/ 
       Various configuration files go here like linker scripts, jtag interface configuration
       and configuration source- and header-files for several third party libraries.
       You may change these configuration files to tweak your project to your needs.
       
     + subfolder examples/
       Each project comes with a set of ready to run example codes. These can be activated
       by activate.NNN_*.sh scripts in the rank NNN=600. The examples are part of the project
       and can be modified to be the starting point for your application.
       Be aware, that not all examples do work together on a hardware if activated in parallel.
     
     + subfolder extensions/
       This is just a symbolic link to the extensions/ folder of the last used toolchain extensions.
       It lists all available extensions. This folder is part of your search path by default. 
     
     + subfolder extensions.active/
       The content of this folder shows all extensions being active for the last compile run.
       The entries are symbolic links to make-, source- and header-files. They have
       been created by activate scripts from extensions/.
       All make.NNN_* files found here are automatically concatenated into file "makefile".
       The source.* files are concatenated into one source.c and source.h file too.
       These are used to autostart activated extensions.
       
     + subfolder extensions.local/
       The project can activate its own extensions. One template of a board 
       extension is stored here. This will help you to create an extension for 
       your own board. Just uncomment and change the provided fields in the makefile
       and issue the corresponding activate script.
       The file makefile.700_extra_settings is automatically activated on every compile
       run. You may use it to add your own settings to the current active makefile
       in extensions.active/.
     
     + subfolder QtCreator/
       Every compile run will automatically update the QtCreator project being stored in this
       folder. just launch ./qtcreator.sh in your project folder to load this project.
       The project will only include those files, being involved in the last successfull
       compile run. So you might want to rerun ./qtcreator.sh after changing the set of activated
       extensions.
     
   + ToolChain Directory Structure
     Newly created project folders are placed in ~/Source/ also but may be moved anywhere
     in the filesystem. 
     
     + TheToolChain_<RELEASE>
       Several toolchain releases can be installed in parallel. Each release shows its release 
       number as directory name suffix.
       
       + subfolder TTC-Library/
         This folder is linked as ttc-lib/ into each project. It stores all high- and 
         low-level libraries provided by The ToolChain itself.
         
       + subfolder Documentation/
         Official documentation of The ToolChain.
         
       + subfolder Regressions/
         Source codes of automated regression tests. If activated, these will stress test
         various implementation details of different libraries.
         
       + subfolder Template/
         A complete template used to create new projects.
       
       + subfolder InstallData/
         All install scripts and downloaded extensions go here.
         Install scripts may be rerun individually or all together by use of
         ./installAll.sh.
         
       + subfolder Updates/ 
         Important text replacements and file renames between consecutive toolchain
         releases are placed in individual update scripts. You may use these scripts
         to upgrade a project folder from its current one to the next release. 
         Simply copy the desired update scripts into your project folder and run them
         in order one after another to apply all required replacements and renames.
         
         Note: Manual changes may still be required to get your project compiled and running
               on a newer release.
       
     + ~/Source/TheToolChain
       This symbolic link always leads to the latest used release.
       
     + ~/Source/ToolChain.Contents/
       Download contents are shared among different releases in this folder to safe disc space.
     
     
     