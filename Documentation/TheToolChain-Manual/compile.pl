#!/usr/bin/perl

sub getFile {
  my $URL     = shift;
  my $OutFile = shift;
  
  if (! -e $OutFile) {
    system("wget $URL -O$OutFile");
  }
}

getFile("http://mally.stanford.edu/~sr/computing/lingmacros.sty",                               "lingmacros.sty");
getFile("http://mirrors.ctan.org/macros/latex209/contrib/trees/tree-dvips/tree-dvips.sty",      "tree-dvips.sty");
getFile("http://mirrors.ctan.org/macros/latex209/contrib/trees/tree-dvips/tree-dvips91.pro",    "tree-dvips91.pro");
getFile("http://mirrors.ctan.org/macros/latex209/contrib/trees/tree-dvips/tree-dvips91.script", "tree-dvips91.script");

system("latex TheToolChain.tex && dvipdf TheToolChain");
