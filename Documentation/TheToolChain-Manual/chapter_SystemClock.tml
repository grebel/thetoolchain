System Clock (ttc_sysclock)

The system clock facility provides the heartbeat of a micro controller. Many architectures allow to
switch between different system clock frequencies. Increasing the system clock frequency mainly has two effects:

1) Computing power increases
   More assembly instructions are being executed per second. Also the conversion time of analogue to digital converters (ADCs)
   decreases as the conversion is done faster.

2) Energy consumption increases
   Modern microcontrollers are built in a so called Complementary Metal Oxide Semiconductor (CMOS) technology.
   In CMOS circuits electric currents mostly flow when transistor gates are flipped between low and high. The more flips per second, 
   the more electric current flows through the circuit. So increases the energy consumption. Nearly every microcontroller uses the
   central system clock to let its gates flip synchronously.

Unfortunately the computing power of modern microcontrollers does not scale linearly with their system clock frequency. This is because 
of the so called processor-memory gap. The computing power of processing cores has advanced faster than the performance of memories. In 
effect fast processing cores often have to wait for memory accesses. 
Several techniques have been developed like parallel memory controllers and multi level cache hierarchies to provide faster memory systems.
But these techniques still do not scale as good as processing cores.
As a rule of thumb one can say that doubling the system clock frequency provides twice the computing power for low frequencies. For higher 
frequencies, the computing power factor is less.

In effect, a microcontroller has three main operating frequencies:
1) Maximum Stable
   Processor is running fastest and consumes most energy per second.
   
2) Minimum Stable
   Processor is running at lowest frequency and consumes least possible energy per second.
   
3) Most Efficient
   Processor is running at a frequency that provides the highest Compute Performance per Energy ratio.
   This means that it will consume least amount of energy for a defined amount of code to execute.
   
Switching the system clock frequency always requires to reconfigure devices that use the system clock for timing. Devices that depend on
the system clock frequency are all IO devices like I2C, I2S, SPI, USART, CAN and MII. Software can also be affected when it comes to exact
delay functions.
   
The ToolChain provides ttc_sysclock as a hardware independent API to control the system clock frequency.
Its main concept is that of clocking profiles. Clocking profiles are a hardware independent description of certain important operating frequencies
as described above. 

Most important functions from ttc_sysclock:

<EXTRACT type="FunctionDeclaration" name="ttc_sysclock_profile_switch" "src="ttc_sysclock.h">

<EXTRACT type="FunctionDescription" name="ttc_sysclock_profile_switch" "src="ttc_sysclock.h">


<EXTRACT type="FunctionDeclaration" name="ttc_sysclock_get_frequency" "src="ttc_sysclock.h">

<EXTRACT type="FunctionDescription" name="ttc_sysclock_get_frequency" "src="ttc_sysclock.h">


<EXTRACT type="FunctionDeclaration" name="ttc_sysclock_udelay" "src="ttc_sysclock.h">

<EXTRACT type="FunctionDescription" name="ttc_sysclock_udelay" "src="ttc_sysclock.h">


Writing a Low Level Driver for ttc_sysclock

When you want to write a new architecture driver for the sysclock device take a look at sysclock_stm32f1xx.c as an example implementation.
Many microcontrollers provide different sources for the system clock. A clock source can be built into the uC (High Speed Internal) or
attached externally (High Speed External). Some microcontrollers only allow changing the system clock frequency when using an external crystal
like the STM32F1xx family. Check the datasheet of your microcontroller for clock tree description. You will also find information about the 
procedure how to change the current frequency.
