#!/usr/bin/perl

my $Argument1 = $ARGV[0];
my $Arguments = join(" ", @ARGV);
unless ($Argument1) { $Argument1 = 'help'; }

my %Help = ( 'h'    => 1, '-h'    => 1, '--h'    => 1, 
             'help' => 1, '-help' => 1, '--help' => 1, 
             '?'    => 1, '-?'    => 1, '--?'    => 1,
             '/help'=> 1
           );
             
# Find TTC installation directiory
my $PWD = `pwd`; chomp($PWD); $PWD .= '/';
my $MySelf = substr($0, rindex($0, '/') + 1); # remove path from scriptname 
my $Pos = index($0, 'update_TheToolChain.sh');
my $DirTTC = substr($0, 0, $Pos);
if ($DirTTC eq '')  {         # script is somewhere in search path: find it
  $DirTTC = `which $0`;
  $Pos = index($0, 'update_TheToolChain.sh');
  $DirTTC = substr($0, 0, $Pos);
}
if ($DirTTC eq './') {        # script is called in current directory
  $DirTTC = $PWD;
}
if (-d "${DirTTC}.git") {     # being called inside git repository
  print "Updating git repository ${DirTTC}..\n";
  system("git pull");
  exit 0;
}
unless (-d $DirTTC) {         # ERROR: Cannot find installation directory
  die("$0 $Arguments\nERROR: Cannot find location of TheToolChain/ (PWD=$PWD)!");
}
my $VersionTTC = strip(`cat ${DirTTC}Version`);
if ($VersionTTC eq 'devel') { # ERROR: Cannot update development ToolChain
  die("$0 $Arguments\nERROR: Cannot update development ToolChain. Run this script in git repository instead!"); 
}

my $ScriptInstall = "${DirTTC}InstallData/deployment/install_TheToolChain.sh";
if ($Help{$Argument1}) {      # reuse + display help text from install_TheToolChain.sh
  my $Help = `$ScriptInstall`;
  $Help =~ s/$ScriptInstall/$0/g;
  print "$Help\n";
  exit 10;
}

# prerequisits seem to be OK: run update
print "looking for newest ToolChain version..\n";
system("$ScriptInstall $Arguments");

sub strip {                   # removes invisible characters at begin + end of given string
  my $Line = shift || "";

  my %IsWhiteChar = ( ' ' => 1, "\n" => 1, "\r" => 1, "\t" => 1 ); 

  $Line =~ s/^\s+//;
  $Line =~ s/\s+$//;
  #$Line =~ s/(?:^ +)||(?: +$)//g;
  #$Line =~ s/^\ *([A-Z,a-z,0-9]*)\ *$/\1/g;
  #$Line =~ s/^\s+//;
  #$Line  =~ s/\s+$//;
  return $Line;
  
  my $lIndex = 0;
  while ( $IsWhiteChar{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsWhiteChar{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  return substr($Line, $lIndex, $rIndex - $lIndex + 1);

}
