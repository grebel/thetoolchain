
                         The ToolChain rev. 1.0.53

                      Open Source Development ToolChain 
                          and Operating System
                          for Microcontrollers
                        
                    Last update: 20180119 15:24:00 UTC
                    
                    
This set of install scripts and software libraries has been written by Gregor Rebel 
and several students of the University of Applied Sciences Muenster 2010-2018.
This software is been released under the terms of a modified Lesser GNU Public License (LGPL).
Please read the enclosed LEGAL.txt for details.


What is it?

The ToolChain is a complete development environment and operating system for modern 32-bit microcontrollers.
It provides everything you need to write, compile, flash and debug your own embedded applications.

The ToolChain consists of
1) Microcontroller Firmware Library (ttc-lib).
2) Complete register structs for supported microcontrollers.
9) Debuggable Statemachines usable in single- and multitasking configurations.
4) Out of the box running, hardware independent example frameworks.
A) Code generators for fast creation of statemachines, additional source files, functions and drivers.
2) Glue scripts for simple installation and usage.
3) A modular extension mechanism.
6) Third party desktop applications (E.g. source code editor, cross compiler and others)
7) Third party microcontroller firmware libraries (E.g. FreeRTOS multitasking scheduler)
8) Related third party datasheets and documentations


Features of ttc-lib

The ToolChain Library forms an operating sytem like shell around low-level hardware drivers.
It allows to use the same application source code for different architectures. 
An architecture can be the type of microcontroller, radio transceivers, LCD, network protocol or other.
The library can be compiled with or without an external multitasking scheduler. 
It provides it's own 32 bit optimized task communication and synchronization via mutexes, semaphores, lists and queues.
This own implementation allows to switch of the external scheduler with few impact on your application. 
In single tasking configuration, the data structs provide synchronization and communication between main 
application and interrupt service routines by using the same function calls.
See documentation folder and header files of high-level drivers for more details (e.g. ttc-lib/ttc_usart.h).


Supported Prototype Boards

  Prototype boards for the supported architectures are offered by different vendors
  Individual boards can be configured in each local project configuration.
  
  Newbies may want to obtain a so called discovery board from ST. These are offered for
  different arcitectures for low prices. These discovery boards (e.g. STM32L100C-DISCO)
  have an onboard programming adapter. All you need to start development is an USB cable.

  A well supported board is the STM32L1 discovery board. It has a low power microcontroller that is well suited 
  for line or battery powered applications.
  Another option are the well documented STM32F1xx boards provided by Olimex Corp (http://olimex.com).
  
  This is a list of all prototype boards being supported out of the box:
  - 


Other Prototype Boards

  If your board is not listed inside activate_project.sh you might activate another board with same microcontroller or 
  define your own board. You find more details in the documentation: 
  ~/Source/TheToolChain/Documentation/TheToolChain-Manual/chapter_AddingNewPrototypeBoard.tml

  
Operating System Requirements

  The basic idea of this project is to provide a free to use and free to modify
  solution for software development in embedded systems. As a logical consequence
  of this idea, The ToolChain should run on most Linux based operating systems.
  It is not intended to run on closed source proprietary operating systems.


Hardware Requirements
  Memory   512MB RAM (tested in VirtualBox)
  Disc     30GB (fresh installed OS + The ToolChain)
  CPU      >= 1 core (multi-core support)

  
Successfully tested on

  Xubuntu/Kubuntu/Ubuntu 15.10, 16.04, 16.10, 17.04
  Note: Older *buntu versions have compatibility issues with libusb and may have issues during flashing.
  
  
Other Distributions

  Send me a patchfile for existing install scripts 
  (preferably for InstallData/scripts/installFuncs.sh).
  
  
Other Operating Systems (Windows, MacOS, ..)

  The ToolChain is an open source, royalty free development environment for open source,
  royalty free operating systems and development environments. Its legal terms do not 
  allow to port it to non open source operating systems or to integrate any of its parts
  into commercial development environments. The intention is to keep it free to use and 
  its source code open to read for every user.
  
  
Automated Installation

  The automatic installation requires to download and execute a script file
  from a website. This install script will then download a compressed archive of 
  the newest available version, unpack and install it. The ToolChain will be 
  installed into ~/TheToolChain/ where ~ means the home folder of current user.
  It will also download a 1GB tarball of third party stuff on its first run.
  Later runs will reuse this big tarball.

  copy paste into shell ----------------------

  wget -N http://thetoolchain.com/install_TheToolChain.sh
  bash ./install_TheToolChain.sh REUSE
  
  --------------------------------------------
  
  If you have trouble with your installation, it might be that some inconsistencies
  occured while reusing old binary data. You can force a recompilation of all
  sources by using the REPLACE keyword:
  
  copy paste into shell ----------------------

  wget -N http://thetoolchain.com/install_TheToolChain.sh
  bash ./install_TheToolChain.sh REPLACE

  --------------------------------------------


Installing without Password Prompts

  When installAll.sh is started as normal user, it will use sudo to get superuser permissions
  when required. This may create several password prompts and is impractical for automated installs.
  Running installAll.sh as user root requires extra arguments USER and GROUP.
  
  Example installation as user root for user user1:
  
  copy paste into shell ----------------------
  
  Version="1.0.53"
  tar xjf TheToolChain_${Version}.tar.bz
  cd TheToolChain_${Version}/InstallData
  sudo ./installAll.sh ALL user1 users
  
  --------------------------------------------


Installing outside of a home folder
 
  The folder TheToolChain/ is currently searched in ~/Source/ only!
  If the toolchain is installed somewhere else, several scripts have to be updated.
  You may use a symbolic link for ~/Source/ if you want to move Source/ somewhere else:
  
  copy paste into shell ----------------------
    
  mv ~/Source /data/
  ln -s /data/Source ~/Source
  
  --------------------------------------------


Installation Failed

  a) Check log files in $HOME/Source/TheToolChain/InstallData/Logs/
     tail -f $HOME/Source/TheToolChain/InstallData/Logs/*
  b) Manually restart failed install script
     cd $HOME/Source/TheToolChain/InstallData
     ./install_06_libftdi.sh  # Example
  c) Delete/ move away conflicting install scripts (reduces amount of supported features)
  d) After fixing the error: restart complete installation (will skip already installed parts)
     cd $HOME/Source/TheToolChain/InstallData
     ./installAll.sh

  Missing system library
    a) Start packet manager (Synaptic/ YaST) and search for package with similar name.
    b) add community repositories to your package manager to obtain access to more 
       packages (e.g. packman repository)
    c) search the net for error message
    d) search the net for missing library
    
  Broken/ outdated URL
    a) Open corresponding install-script in texteditor (vi, kate, ...).
       Search for URL-prefix (URL without file name).
       Open URL in browser and search for a newer version of desired file.
       Modify install-script.
       ReRun  install-script
    b) search the net for error message
    
    
Installation Succeeded

  After successfull installation, a new project has to be created.
  
    cd ~/Source/TheToolChain
    ./createNewProject.pl MyFirstTestProject IDontKnowWhoIAm
    cd ../MyFirstTestProject  # <-- this is your project folder
    ./compile.sh              # <-- will probably fail (wrong board, programmer, ...)
    ./qtcreator.sh            # <-- starts an integrated development environment for your project


Compile and flash (command line)

  cd YourProjectFolder/
  ./compile.sh

Compile and do not flash
  ./compile.sh NOFLASH
  

Example Frameworks

  Note: You need to cd into your project folder, where your main.c 
        resides before you can activate any extensions.
  
  Several out of the box running examples are provided by the ToolChain.
  The source code of these examples is configured in several makefiles that are called extensions.
  All available extensions are to be found in extensions.all/
  Extensions can be linked into extensions.active/ (we say these extensions get activated).
  For most extensions a dedicated activate script is available in extensions.all/.
  This folder should already being added to your PATH variable. You can check it by typing 
  "activate." into your shell and then press the tab-key twice.
  In case your shell does not show a long list of activate.NNN_XXX.sh entries, you should try
  again in a fresh shell.
  
  
  The most basic example can be activated via activate.600_example_leds.sh.
  Then you might want to activate a board that corresponds to your prototype microcontroller board.

  Proceed reading in chapter Example Frameworks in folder 
  ~/Source/TheToolChain/Documentation/TheToolChain-Manual/

  
Additional Documentation

  a) Check http://thetoolchain.com/documentation/      for more documentation and videos.
  b) Check ~/Source/TheToolChain/InstallData/Logs/     for installation log files.
  c) Check ~/Source/TheToolChain/Documentation         for official documentation + automatically downloaded stuff.
  d) Check http://microcontroller.net and other websites for general hints regarding microcontrollers.

  
Adding Additional Sources to a Project

    You can simply add new or existing source files to your project by calling _/source.pl
    from your project folder. It can also help you to insert new functions into your code.
    cd MyCoolTestProject/
    
    # create files foo.c and foo.h and add them to the current project
    _/source.pl add foo 
    
    # add public function t_u8 add(t_u8 A, t_u8 B) to foo.c and foo.h
    _/source.pl function public 't_u8 add(t_u8 A, t_u8 B)' foo
    
    Simply call source.pl without arguments get a help page:
    _/source.pl
    
      
Getting Updates

    A special update script can be found in base folder of each ToolChain installation.
    Simply run it without arguments to show its help text:
    ~/Source/TheToolChain/update_TheToolChain.sh
    
    
Upgrade existing Project from older ToolChain Version

  1) Rerun update_TheToolChain.sh to get newest version
  2) Create a new project
  3) Copy from old project into new project folder
     - configs/
     - extensions.local/
     - your source files
  4) Modify activate_project.sh to fit your settings
  5) Issue inside new project folder 
     ./clean.sh; ./compile.sh; ./qtcreator.sh
  6) Fix compilation issues
     - changed function names
     - changed attribute names inside structures
     - changed amount of function arguments
     - changed semantics (sometimes interfaces can change in how they have to be used)
     - Take a look into ~/Source/TheToolChain/Updates/ for update scripts.
       Each update script will apply all file renaming and string replacements to upgrade
       one step of version. Just copy the required update scripts into your old project 
       folder and run them one after another (Create a safety copy first!).

       
Directory structure

  ./clean.sh
    Removes all temporary files from your project folder.
    
  ./compile.sh  
    Compiles project in current directory. Pipes compile output to less in case an error occurs.
    Will flash compiled binary onto uC if all went OK.

  ./debug.sh
    Starts a debugging session using your currently activated debugging interface.
    See chapter Debug Your Code below.
    
  ./activate_project.sh
    script that will create all symbolic links in 

    extensions.active/ for current project.

  ./createLinks.sh
    Creates symbolic links to currently installed ToolChain
        
  extensions.local/
    makefile extensions that are special for this project.
    It also contains a template board description that can be used to describe your own prototype board.
    
  extensions/ -> ~/TheToolChain/InstallData/extensions/
    All extensions provided by current ToolChain.
    
  extensions.active/
    Symbolic links into extensions/ of currently activated extensions.
        
  required_version
    Version number of ToolChain being required for this project.
    A ToolChain of required version will be downloaded and installed 
    by compile.sh automatically in case it is missing.

  _/ -> ~/Source/TheToolChain/InstallData/scripts/
    This folder stores a collection of usefull scripts required to compile, program and debug software on your microcontroller.
    The symbolic link always points to the last used version of the ToolChain.

  _/automate.pl
    Automated configuration, compilation, flashing and debugging of multiple microcontrollers.
    Call it from your project folder to get a detailed help text.
  
  _/debug_current.sh
    Combination of gdb_server.sh and gdb.sh. Will automatically start a gdb server and connect to it.
    Automatically called from ./debug.sh. 

  _/flash_current.sh
    Starts openocd to connect to programmer and write given .bin file into microcontroller.

  _/findInSources.sh
    Finds given text in all files of given folder.

  _/flash_current.sh
    Programs given binary file via JTAG programmer onto uC.

  _/gdb_server.sh
    OpenOCD will connect via JTAG programmer to uC and wait for a GDB to connect.

  _/gdb.sh
    Will load debug symbols from given elf-file and connect to a running OpenOCD.
    Note: Requires _/gdb_server.sh to be started before.

      
Folder configs/

  This folder stores configuration files of several external software libraries and tools for current project.
    
  configs/memory*.ld
    Linker scripts that defines memory spaces of target uC.
    Some of them are created from activated board extension.
    
  configs/FreeRTOSConfig.h
    Compile time configuration of FreeRTOS multitasking scheduler.
    
  configs/openocd_flash.cfg
    OpenOCD-script used to program a binary file onto your microcontroller.
    
  configs/openocd_target.cfg
    OpenOCD-script that defines how to communicate with your microcontroller.
    
  configs/stm32f10x_it.c 
  configs/stm32f10x_it.h
    Compatibility layer to Standard Peripheral Library from ST Microelectronics.
  

Release Philosophy

  The ToolChain is currently developed in a closed git repository.
  Latest testing releases show a trailing b (beta) in their version number.
  These beta release are highly experimental and are released irregularly.
  All beta releases between to stable releases show the same version number.
  Check readme.History for changes.
  Even stable releases may be rereleased under same version number. You may 
  rerun update script regularly to get updates.


Known Issues

  1) SPI driver for STM32F1xx is currently not implemented because
     low-level driver has to be ported to new driver API (ask for a date and I will prioritize it).
  2) radio_dw1000 and ttc_rtls are unfinished and currently under development.
  3) Documentation is incomplete (constant issue).

  
Debug Your Code

  1) compile and flash (see above)
  
  2) ./debug.sh      # text interface debugging using gdb directly
     _/automate.pl DEBUG_DDD@1  # graphical debugging using graphical Data Display Debugger (ddd) via first connected debug interface
  
  3) Frequent used commands for gdb text interface (-> man gdb)
     Ctrl + C       Stop execution (very usefull to find when program flow is stuck somewhere)
     C + Return     Continue execution
     n              step over one line of code
     b FILE:LINE    Sets breakpoint in file FILE at line LINE  (e.g. b main.c:212)
     b function     Sets breakpoint into certain function      (e.g. b main)
                    Default breakpoints can be added to configs/debug2_general.gdb
     watch * ((t_u32*) 0x1234)
                    Sets hardware watchpoint for all 32-bit accesses to a 4 byte aligned adress.
     d NUMBER       Deletes breakpoint number NUMBER           (e.g. d 2)
     p EXPRESSION   Prints content of variable                 (e.g. p *MyPointer)
     set VAR=VALUE  Assigns a new value to given variable.
     bt             Prints backtrace that is the history of function calls.
     up             Moves one level upwards in backtrace.
                    Provides access to local variables of calling function.
     down           Moves one level downwards in backtrace.
                    Provides access to local variables of function being called.                    
     adv LINE       advances execution to line number LINE     (e.g. adv 23)
     q + Return     quit from QGB
     
  4) Check gdb tutorials for more debug features!

  5) Add gdb commands to be issued in every debug session to 
     YourProjectFolder/configs/debug2_general.gdb

  6) Problem: Ctrl+C does not halt CPU
     It has been observed for some debugging interfaces (e.g. stlink v2), that pressing Ctrl+C will
     not stop the execution, but close the remote connection. The reason seems to be a bug in openocd
     driver for stlink debugging interface.
     As a workaround, a halt command can be send to openocd via telnet session.
     a) Start debugging as described above
     b) In a second shell, connect to running OpenOCD via telnet on port 4444 and send it a halt command
        telnet 127.0.0.1 4444
        > halt
     c) Switch back to gdb shell. You should read that program has received a signal SIGINT and the source
        code line where execution has stopped

  7) Automate compilation, flashing and debugging via multiple debug interfaces via automate.pl:
     _/automate.pl   # run inside a project folder to display help text and examples
     _/automate.pl COMPILE FLASH@1 DEBUG@1 # compile, flash and debug using first debug interface

     
Code well!


This readme has been written by Gregor Rebel 2013-2018.