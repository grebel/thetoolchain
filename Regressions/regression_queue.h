/*{ regression_queue.h ************************************************
 
  Regression test for simple queues
  written by Gregor Rebel 2013
 
}*/
#ifndef regression_queue_H
#define regression_queue_H

//{ Includes *************************************************************

#include "../ttc-lib/ttc_basic.h"
#include "../ttc-lib/ttc_task.h"
#include "../ttc-lib/ttc_string.h"
#include "../ttc-lib/ttc_memory.h"
#include "../ttc-lib/ttc_random.h"
#include "../ttc-lib/ttc_queue.h"
#include "../ttc-lib/ttc_gpio.h"
#include "../ttc-lib/math/DEPRECATED_math_basic.h"
//? #include "ttc_list.h"
#ifdef EXTENSION_ttc_gfx
#include "../ttc-lib/ttc_gfx.h"
#endif

//} Includes
//{ Defines/ TypeDefs ****************************************************

#ifdef EXTENSION_ttc_heap_sdma
#include "heap_sdma.h"
#endif


/** { benchmark parameters **********************************************
 *
 * All constants in this block can be reconfigured via "COMPILE_OPTS += -D"
 * lines in makefile.
 *
 */


#ifndef Amount_Producer
#  define Amount_Producer        10   // amount of producer tasks to spawn
#endif
#ifndef Amount_Consumer
#  define Amount_Consumer        10   // amount of consumer tasks to spawn
#endif
#ifndef RQ_QUEUE_SIZE
#  define RQ_QUEUE_SIZE         100   // size of each queue
#endif
#ifndef TTC_QUEUE_TYPE
#  define TTC_QUEUE_TYPE          1   // ==1: use ttc_queue_generic_*() (FreeRTOS)
                                      // ==2: use ttc_queue_byte_*()
                                      // ==3: use ttc_queue_pointer_*()
#endif
#ifndef Amount_BytesPerTurn
#  define Amount_BytesPerTurn    10   // amount of blocks to allocate in each turn
#endif
#ifndef REGRESSION_PIN_QUEUE_PUSH
#  define REGRESSION_PIN_QUEUE_PUSH          E_ttc_gpio_pin_d0
#endif
#ifndef REGRESSION_PIN_QUEUE_PUSH_ACTIVITY
#  define REGRESSION_PIN_QUEUE_PUSH_ACTIVITY E_ttc_gpio_pin_d4
#endif
#ifndef REGRESSION_PIN_QUEUE_PULL
#  define REGRESSION_PIN_QUEUE_PULL          E_ttc_gpio_pin_d1
#endif
#ifndef REGRESSION_PIN_QUEUE_PUSH_ISR
#  define REGRESSION_PIN_QUEUE_PUSH_ISR      E_ttc_gpio_pin_d2
#endif
#ifndef REGRESSION_PIN_QUEUE_PULL_ISR
#  define REGRESSION_PIN_QUEUE_PULL_ISR      E_ttc_gpio_pin_d3
#endif
#ifndef REGRESSION_PIN_QUEUE_BYTE_TRANSFER
#  define REGRESSION_PIN_QUEUE_BYTE_TRANSFER E_ttc_gpio_pin_d5
#endif
#ifndef REGRESSION_PIN_QUEUE_TEST
#  define REGRESSION_PIN_QUEUE_TEST          E_ttc_gpio_pin_d10
#endif

//}


#  if TTC_QUEUE_TYPE == 1 // use byte queue

#  define BLOCK_SCHEDULER_DURING_QUEUE_OPERATION 0  // FreeRTOS does not allow queue operations inside critical sections
#  define Queue_PushBack(Q,D)     ttc_queue_generic_push_back(Q, &D, (t_u32) -1)
#  define Queue_PullFront(Q,D)    ttc_queue_generic_pull_front(Q, D, (t_u32) -1)
#  define Queue_PushBackISR(Q,D)  ttc_queue_generic_push_back_isr(Q, &D)
#  define Queue_PullFrontISR(Q,D) ttc_queue_generic_pull_front_isr(Q, D)
#  define Queue_Create(Size)      ttc_queue_generic_create(Size, 1)

  typedef t_u8 t_queueitem;
#endif

#  if TTC_QUEUE_TYPE == 2 // use byte queue

#  define BLOCK_SCHEDULER_DURING_QUEUE_OPERATION 1  // TTC_Queue works best inside critical sections
#  define Queue_PushBack(Q,D)     ttc_queue_byte_push_back(Q, D)
#  define Queue_PullFront(Q,D)    *D = ttc_queue_byte_pull_front(Q)
#  define Queue_PushBackISR(Q,D)  ttc_queue_byte_push_back_isr(Q, D)
#  define Queue_PullFrontISR(Q,D) ttc_queue_byte_pull_front_isr(Q, D)
#  define Queue_Create(Size)      ttc_queue_byte_create(Size)

     typedef t_u8 t_queueitem;

#endif
#  if TTC_QUEUE_TYPE == 3 // use pointer queue

#  define BLOCK_SCHEDULER_DURING_QUEUE_OPERATION 1  // TTC_Queue works best inside critical sections
#  define Queue_PushBack(Q,D)     ttc_queue_pointer_push_back(Q, D)
#  define Queue_PullFront(Q,D)    *D = ttc_queue_pointer_pull_front(Q)
#  define Queue_PushBackISR(Q,D)  ttc_queue_pointer_push_back_isr(Q, D)
#  define Queue_PullFrontISR(Q,D) ttc_queue_pointer_pull_front_isr(Q, D)
#  define Queue_Create(Size)      ttc_queue_pointer_create(Size)

     typedef void* t_queueitem;

#endif

//} Defines

typedef struct {
    // each task has a different index number
    t_u32 Index;

    // read by task
    t_base SleepTime_Max;

    // written by task
    t_base  AmountSent;         // total amount of bytes and pointers sent to queues from this task
    t_base* AmountReceived;     // != NULL: AmountReceived[I] = amount of received bytes  and pointers of value I

} TaskStatistics_t;

//{ Function prototypes **************************************************

// enqueues print job
void printAt(t_u8 X, t_u8 Y, const char* Format, t_u32 Value);

// main entry: initializes hardware + spawns all threads
// returns immediately
void regression_queue_start();

// main task running regression test
void taskRegression_Queue(void* Arg);

// checks received byte + updates given task info data
void check_ReceivedByte(t_queueitem Data, TaskStatistics_t* MyTaskInfo);

void rqu_printAt(t_u8 X, t_u8 Y, const char* Format, t_u32 Value);

// interrupt service routines
void ISR_P();
void ISR_C();

//} Function prototypes
#endif //regression_queue_H
