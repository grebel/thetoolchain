/** { regression_queue.c ************************************************
 
  regression_queue_start() spawns given amount of producer and consumer tasks.
  Each producer pushes one entry to a global queue.
  Each consumer pull one entry from this queue.

  #ifdef TTC_REGRESSION
    - A second pointer queue is created and used in parallel.
    - Additional extra safety checks (Assert()s) are activated throughout the code.
  #else
    - provides maximum speed for performance analysis
    - Only one queue used
    - no extra safety checks
    - apply oscilloscope to REGRESSION_PIN-pins to measure runtimes
  #endif

}*/

#include "regression_queue.h"

#include "../ttc-lib/ttc_timer.h"

#ifdef EXTENSION_ttc_gfx

// read-only configuration of current display (width, height, ...)
t_ttc_gfx_config* DisplayConfig = NULL;

#endif
t_u8 rqu_TextRows = 0;  // total amount of text rows on current display

//{ Function definitions *************************************************
//{ Global Variables ****************************************************

#  if TTC_QUEUE_TYPE == 1 // use byte queue
     t_ttc_queue_generic TestQueue;
#endif

#  if TTC_QUEUE_TYPE == 2 // use byte queue
     t_ttc_queue_bytes* TestQueue;
#endif
#  if TTC_QUEUE_TYPE == 3 // use pointer queue
     t_ttc_queue_pointers* TestQueue;
#endif

#define random(MAX)      ttc_random_generate() % MAX


// global variables

// amount of blocks allocated on heap
extern t_base ttc_memory_Amount_Blocks_Allocated;

// amount of bytes occupied on heap by dynamic allocations
extern t_base ttc_memory_Bytes_Allocated_Total;

// usable amount of allocated bytes
extern t_base ttc_memory_Bytes_Allocated_Usable;

void rqu_taskProducer(void* Arg);
void rqu_taskConsumer(void* Arg);
//? t_u8 rqu_DisplayY = 2;

t_base rqu_AmountHeapFreeAtStart = 0;

t_base P_AmountSent      = 0;
t_base C_ReceivedBytes   = 0;


#define Amount_Blocks_Total Amount_Producer * Amount_BytesPerTurn * 4

// each producer will halt after producing this amount of blocks
#define Amount_BlocksProduced_Per_Producer 10000000

#undef TTC_REGRESSION //D
#ifdef TTC_REGRESSION
// load defaults for a regression test
#  ifndef ENABLED_GFX
#    ifdef EXTENSION_ttc_gfc
#      define ENABLED_GFX     1  // ==1: gfx display enabled (inaccurate timings due to display time)
#    else
#      define ENABLED_GFX     0  // ==1: gfx display enabled (inaccurate timings due to display time)
#    endif
#  endif
#  ifndef ENABLED_ENDLESS
#    define ENABLED_ENDLESS 0  // ==1: run in endless loop/ ==0: use Amount_BlocksProduced_Per_Producer as limit
#  endif
#else
// load defaults for a speed test
#  ifndef ENABLED_GFX
#    define ENABLED_GFX     0  // ==1: gfx display enabled (inaccurate timings due to display time)
#  endif
#  ifndef ENABLED_ENDLESS
#    define ENABLED_ENDLESS 1  // ==1: run in endless loop/ ==0: use Amount_BlocksProduced_Per_Producer as limit
#  endif
#endif

//} Global Variables

TaskStatistics_t  rqu_ProducerTasks[Amount_Producer + 2];
TaskStatistics_t* EndOfrqu_ProducerTasks = rqu_ProducerTasks;
TaskStatistics_t  ConsumerTasks[Amount_Consumer + 2];
TaskStatistics_t* EndOfConsumerTasks = ConsumerTasks;

// statistics for producer and consumer interrupt service routines
TaskStatistics_t* ISR_P_Stats = rqu_ProducerTasks + Amount_Producer + 1;
TaskStatistics_t* ISR_C_Stats = ConsumerTasks + Amount_Producer + 1;

// we reserve 1 index for ISR_P()
t_queueitem MaxSenderIndex = (t_queueitem) Amount_Producer + 1;

const char* Text_TestQueue = "regress: Queue";

// handle of first producer/ consumer task
TaskHandle_t FirstProducer = NULL;
TaskHandle_t FirstConsumer = NULL;

// special code being send to calculate transfer time
t_queueitem Mark = (t_queueitem) 99;

void regression_queue_togglePin_QueuePushActivity() {
#ifdef REGRESSION_PIN_QUEUE_PUSH_ACTIVITY
    ttc_task_critical_begin();
    static char qgs_PinState = 0;
    if (qgs_PinState) {
        ttc_gpio_set(REGRESSION_PIN_QUEUE_PUSH_ACTIVITY);
        qgs_PinState = 0;
    }
    else  {
        ttc_gpio_clr(REGRESSION_PIN_QUEUE_PUSH_ACTIVITY);
        qgs_PinState = 1;
    }
    ttc_task_critical_end();
#endif
}
void regression_queue_setPin_QueuePushActivity(BOOL Set) {
    if (Set) {
        if (ttc_task_current_handle() == FirstProducer) {
            ttc_gpio_set(REGRESSION_PIN_QUEUE_PUSH_ACTIVITY);
        }
    }
    else {
        ttc_gpio_clr(REGRESSION_PIN_QUEUE_PUSH_ACTIVITY);
    }
}
void regression_queue_setPin_QueuePush(BOOL Set) {
    if (Set) {
        if (ttc_task_current_handle() == FirstProducer) {
            ttc_gpio_set(REGRESSION_PIN_QUEUE_PUSH);
        }
    }
    else {
        ttc_gpio_clr(REGRESSION_PIN_QUEUE_PUSH);
    }
}
void regression_queue_setPin_QueuePull(BOOL Set) {
    if (Set) {
        if (ttc_task_current_handle() == FirstConsumer) {
            ttc_gpio_set(REGRESSION_PIN_QUEUE_PULL);
        }
    }
    else {
        ttc_gpio_clr(REGRESSION_PIN_QUEUE_PULL);
    }
}
void regression_queue_setPin_QueuePushISR(BOOL Set) {
    if ((ttc_task_current_handle() == FirstProducer)) {
        if (Set) {
            ttc_gpio_set(REGRESSION_PIN_QUEUE_PUSH_ISR);
        }
        else {
            ttc_gpio_clr(REGRESSION_PIN_QUEUE_PUSH_ISR);
        }
    }
}
BOOL rq_MarkActive = 0;
void regression_queue_mark_test(t_u16 Time) {
    ttc_task_critical_begin();
    if (( (ttc_task_current_handle() == FirstConsumer))) {
        ttc_gpio_set(REGRESSION_PIN_QUEUE_TEST);
        for (volatile t_u16 Wait = Time; Wait > 0; Wait--);
        ttc_gpio_clr(REGRESSION_PIN_QUEUE_TEST);
    }
    ttc_task_critical_end();
}
void regression_queue_start() {
#ifdef EXTENSION_ttc_gfx
    DisplayConfig = ttc_gfx_get_configuration(1);
    Assert(DisplayConfig, ttc_assert_origin_auto);
    rqu_TextRows  = DisplayConfig->TextRows;
    
    // set color to use for first screen clear
    ttc_gfx_color_bg24(TTC_GFX_COLOR24_AQUA);
    ttc_gfx_init(1);

    // initialize gfx for use with printf()
    ttc_gfx_stdout_set(1);
    ttc_string_register_stdout(ttc_gfx_stdout_print_block, ttc_gfx_stdout_print_string);
#endif

    // allocate memories
    TestQueue = Queue_Create(RQ_QUEUE_SIZE);

    static t_u8 Temp3[20];
    t_base SleepMax;
    SleepMax = 10;
    for (t_u8 Index = 0; Index < Amount_Consumer; Index++) {
        snprintf(Temp3, 20, "C%02i", Index);
        EndOfConsumerTasks->SleepTime_Max = SleepMax;
        EndOfConsumerTasks->Index = Index + 101;
        EndOfConsumerTasks->AmountReceived = ttc_heap_alloc_zeroed(sizeof(t_base) * (Amount_Producer+1));

        // consumer tasks have higher prio this greatly reduces transfer time of ttc_task_resume()
        ttc_task_create(rqu_taskConsumer, (char*) Temp3, 90, (void*) EndOfConsumerTasks++, 1, NULL);
        ttc_task_check_stack();
    }

    SleepMax = 15;
    for (t_u8 Index = 0; Index < Amount_Producer; Index++) {
        snprintf(Temp3, 20, "P%02i", Index);
        EndOfrqu_ProducerTasks->SleepTime_Max = SleepMax;
        EndOfrqu_ProducerTasks->Index = Index + 1;
        ttc_task_create(rqu_taskProducer, (char*) Temp3, 90, (void*) EndOfrqu_ProducerTasks++, 0, NULL);
        ttc_task_check_stack();
    }

    ttc_task_create(taskRegression_Queue, "Queue", 200, NULL, 1, NULL);

#ifdef REGRESSION_PIN_QUEUE_PUSH
    ttc_gpio_init(REGRESSION_PIN_QUEUE_PUSH,          E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max);
#endif
#ifdef REGRESSION_PIN_QUEUE_PUSH_ACTIVITY
    ttc_gpio_init(REGRESSION_PIN_QUEUE_PUSH_ACTIVITY, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max);
#endif
#ifdef REGRESSION_PIN_QUEUE_PULL
    ttc_gpio_init(REGRESSION_PIN_QUEUE_PULL,          E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max);
#endif
#ifdef REGRESSION_PIN_QUEUE_PUSH_ISR
    ttc_gpio_init(REGRESSION_PIN_QUEUE_PUSH_ISR,      E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max);
#endif
#ifdef REGRESSION_PIN_QUEUE_PULL_ISR
    ttc_gpio_init(REGRESSION_PIN_QUEUE_PULL_ISR,      E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max);
#endif
#ifdef REGRESSION_PIN_QUEUE_BYTE_TRANSFER
    ttc_gpio_init(REGRESSION_PIN_QUEUE_BYTE_TRANSFER, E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max);
#endif
#ifdef REGRESSION_PIN_QUEUE_TEST
    ttc_gpio_init(REGRESSION_PIN_QUEUE_TEST,          E_ttc_gpio_mode_output_push_pull, E_ttc_gpio_speed_max);
#endif

    ttc_timer_set(1, ISR_P, NULL, 2000);
    ttc_timer_set(2, ISR_C, NULL, 2000);
}
void taskRegression_Queue(void* Arg) {
    (void) Arg;
    rqu_AmountHeapFreeAtStart = ttc_heap_get_free_size();

    rqu_printAt(0,0, Text_TestQueue, 0);
    rqu_printAt(2, rqu_TextRows - 1, "used ", 0);
    rqu_printAt(0, rqu_TextRows - 2, "free: %iB", ttc_heap_get_free_size());

    TaskStatistics_t* TaskInfo = NULL;

    while (1) {
        ttc_task_check_stack();
        t_u8 Row = 1;

        P_AmountSent   = 0;
        TaskInfo = rqu_ProducerTasks;
        while (TaskInfo < EndOfrqu_ProducerTasks) { // collect statistic data of all producer tasks
            P_AmountSent += TaskInfo->AmountSent;
            TaskInfo++;
        }
        if (!P_AmountSent) P_AmountSent = 1; // avoid divide by zero

        C_ReceivedBytes    = 0;  // total amount of bytes received by all consumers
        TaskInfo = ConsumerTasks;
        while (TaskInfo < EndOfConsumerTasks) { // collect statistic data of all consumer tasks
            for (t_u8 I = 1; I <= Amount_Producer; I++)
                C_ReceivedBytes += TaskInfo->AmountReceived[I];
            TaskInfo++;
        }
        if (!C_ReceivedBytes) C_ReceivedBytes = 1; // avoid divide by zero

        rqu_printAt(0, Row, "P(%2i)", Amount_Producer);
        rqu_printAt(5, Row, "#%09i",  P_AmountSent);
        Row++;
        rqu_printAt(0, Row, "C(%2i)", Amount_Consumer);
        rqu_printAt(5, Row, "#%09i",  C_ReceivedBytes);
        Row++;

        if (C_ReceivedBytes == Amount_BlocksProduced_Per_Producer * Amount_Producer) {
            Assert(P_AmountSent == C_ReceivedBytes, ttc_assert_origin_auto);
            rqu_printAt(0, rqu_TextRows - 3, "Successfull!", 0);
            ttc_basic_test_ok();
        }
        Assert(C_ReceivedBytes <= Amount_BlocksProduced_Per_Producer * Amount_Producer, ttc_assert_origin_auto); // too many blocks received!

        if (1) rqu_printAt(8, rqu_TextRows - 1, "%iB", rqu_AmountHeapFreeAtStart - ttc_heap_get_free_size());
        static t_u8 Count = 0;
        if (Count++ & 1)
            rqu_printAt(0, rqu_TextRows - 1, "+", 0);
        else
            rqu_printAt(0, rqu_TextRows - 1, "-", 0);

        while (!ENABLED_GFX) { // GFX output disabled: no further update
            ttc_task_msleep(1000);
        }
        ttc_task_msleep(1000);
    }
}
void rqu_taskProducer(void* Arg) {
    TaskStatistics_t* MyTaskInfo = (TaskStatistics_t*) Arg;
    Assert(ttc_memory_is_writable(MyTaskInfo), ttc_assert_origin_auto);

    if (MyTaskInfo->Index == 1)
        FirstProducer = ttc_task_current_handle();

    t_u8 Count = 0;
    while (1) {
        ttc_task_check_stack();

        if (ENABLED_ENDLESS || (MyTaskInfo->AmountSent < Amount_BlocksProduced_Per_Producer) ) {
            t_queueitem Byte2Send = (t_queueitem) MyTaskInfo->Index;
            t_base Blocks2Send = Amount_BytesPerTurn;
            while (Blocks2Send--) {
                if (BLOCK_SCHEDULER_DURING_QUEUE_OPERATION) { ttc_task_critical_begin(); }
                ttc_task_check_stack();
                regression_queue_togglePin_QueuePushActivity();
                if (  (ttc_task_current_handle() == FirstProducer) &&
                      (Count-- == 0)  // reduce amount of measures to avoid two gpio_set() before next gpio_clr()
                   ) {
                    rq_MarkActive = 1; //D
                    regression_queue_setPin_QueuePush(TRUE);
                    ttc_gpio_set(REGRESSION_PIN_QUEUE_BYTE_TRANSFER);
                    Queue_PushBack(TestQueue, Mark);     // sending special value to trigger measure pins
                    Count = 3;
                }
                else {
                    Queue_PushBack(TestQueue, Byte2Send);
                }
                rq_MarkActive = 0; //D
                regression_queue_setPin_QueuePush(FALSE);
                MyTaskInfo->AmountSent++;
#ifdef TTC_REGRESSION
                t_u32 Index = MyTaskInfo->Index;
                ttc_queue_pointer_push_back(&Queue_Pointers, (void*) Index);
                MyTaskInfo->AmountSent++;
#endif
                if (BLOCK_SCHEDULER_DURING_QUEUE_OPERATION) { // critical section may have been ended inside Queue_PushBack()
                    ttc_task_critical_end();
                }
            }

            // give CPU to other tasks
            ttc_task_yield();
        }
        else
            ttc_task_msleep(1000); // finished producing blocks
    }
}
void rqu_taskConsumer(void* Arg) {
    TaskStatistics_t* MyTaskInfo = (TaskStatistics_t*) Arg;
    Assert(ttc_memory_is_writable(MyTaskInfo), ttc_assert_origin_auto);
#ifdef TTC_REGRESSION
    t_base AmountReceivedPointers = 0;
#endif
    t_base AmountReceivedBytes = 0;
    t_queueitem  ReceivedByte    = 0;

    if (MyTaskInfo->Index == 101)
        FirstConsumer = ttc_task_current_handle();

    while (1) {
        ttc_task_check_stack();

        for (t_u8 I = Amount_BytesPerTurn; I > 0;) {
            ttc_task_check_stack();
            regression_queue_setPin_QueuePull(1);
            if (BLOCK_SCHEDULER_DURING_QUEUE_OPERATION) { ttc_task_critical_begin(); }
            t_base Blocks2Receive = Amount_BytesPerTurn;
            while (Blocks2Receive--) {
                Queue_PullFront(TestQueue, &ReceivedByte);
                regression_queue_setPin_QueuePull(0);
                // received byte is just the index of corresponding producer task
                if (ReceivedByte == Mark) { // received byte from task #1
                    ttc_gpio_clr(REGRESSION_PIN_QUEUE_BYTE_TRANSFER);
                }
                if (ReceivedByte != Mark)
                    check_ReceivedByte(ReceivedByte, MyTaskInfo);
                I--;
                AmountReceivedBytes++;
            }
            if (BLOCK_SCHEDULER_DURING_QUEUE_OPERATION) { ttc_task_critical_end(); }
#ifdef TTC_REGRESSION
            t_u32 ReceivedPointer = 0;
            if (ttc_queue_pointer_pull_front(&Queue_Pointers, (void**) &ReceivedPointer) == tqe_OK) {
                // received pointer is just the index of corresponding producer task
                check_ReceivedByte( ReceivedPointer, MyTaskInfo);
                I--;
                AmountReceivedPointers++;
            }
#endif
        }
        ttc_task_yield();
    }
}
void check_ReceivedByte(t_queueitem Data, TaskStatistics_t* MyTaskInfo) {
    t_u32 SenderTaskIndex = (t_u32) Data;
    Assert( (SenderTaskIndex > 0) && (SenderTaskIndex <= (t_u32) MaxSenderIndex), ttc_assert_origin_auto); // invalid producer id received!
    TaskStatistics_t* SenderTask = &(rqu_ProducerTasks[ (t_u32) SenderTaskIndex-1 ]);
    if (SenderTask) {
        Assert(ttc_memory_is_writable(SenderTask), ttc_assert_origin_auto);
        Assert(SenderTask->Index == SenderTaskIndex, ttc_assert_origin_auto);
        MyTaskInfo->AmountReceived[SenderTask->Index]++;
    }
}
void ISR_P() {
    ttc_task_critical_begin();

#ifdef REGRESSION_PIN_QUEUE_PUSH_ISR
    ttc_gpio_set(REGRESSION_PIN_QUEUE_PUSH_ISR);
#endif
    if (Queue_PushBackISR(TestQueue, MaxSenderIndex) == tqe_OK) {
#ifdef REGRESSION_PIN_QUEUE_PUSH_ISR
        ttc_gpio_clr(REGRESSION_PIN_QUEUE_PUSH_ISR);
#endif
        ISR_P_Stats->AmountSent++;
    }

    ttc_task_critical_end();
}
void ISR_C() {
    ttc_task_critical_begin();

    t_queueitem SenderTaskIndex = 0;
#ifdef REGRESSION_PIN_QUEUE_PULL_ISR
    ttc_gpio_set(REGRESSION_PIN_QUEUE_PULL_ISR);
#endif
    if (Queue_PullFrontISR(TestQueue, &SenderTaskIndex) == tqe_OK) {
#ifdef REGRESSION_PIN_QUEUE_PULL_ISR
        ttc_gpio_clr(REGRESSION_PIN_QUEUE_PULL_ISR);
#endif
        check_ReceivedByte(SenderTaskIndex, ISR_C_Stats);
    }

    ttc_task_critical_end();
}
void rqu_printAt(t_u8 X, t_u8 Y, const char* Format, t_u32 Value) {
    static TaskHandle_t MyTask = NULL;

    if (MyTask) { // check if called from invalid task
        Assert(MyTask == ttc_task_current_handle(), ttc_assert_origin_auto); // this function may only be used from one task
    }
    else {        // called first time: store owner task
        MyTask = ttc_task_current_handle();
        Assert(ttc_memory_is_writable(MyTask), ttc_assert_origin_auto);
    }

#ifdef EXTENSION_ttc_gfx    
    static t_u8 Temp1[100]; // static because there can be only one taskPrint() instance
    ttc_gfx_text_cursor_set(X, Y);
    ttc_string_snprintf(Temp1, 100, Format, Value);
    ttc_gfx_text_solid( (char*) Temp1, 100);
#endif
}

//}FunctionDefinitions
