/** regression_usart.c ************************************************{

  Regression test for ttc_usart.c/.h
  TESTED: ttc_usart, ttc_memory, ttc_queue, ttc_mutex, ttc_semaphore, ttc_task, ttc_interrupt

  written by Sascha Poggemann, Gregor Rebel 2013


  Hardware Requirements

  Any protoboard with 1 available USART device.
  Define REGRESSION_USART accordingly.
  Pins TxD and RxD must be connected together.
  Two LEDs TTC_LED1, TTC_LED2


  Regression Test

  Data will be send out. Received data is checked to match transmitted data.

  Two LEDs indicate running regression:
  TTC_LED1 - gets inverted on every buffer transmit
  TTC_LED2 - gets inverted when a buffer has been received and validated successfully

  If the regression is running successfully, both LEDs should blink synchronously with a
  little delay for data transmission.

}*/

#include "regression_usart.h"

//{ Regression settings **************************************************

#define REGRESSION_USART 2  // TTC_USARTn - index of USART to use in this regression
#define AMOUNT_REGRESSION_RUNS 50
#define SIZE_RegressionBuffer 15
#define TIMEOUT_RX 500
//} Regression settings
//{ Global Variables *****************************************************

t_u8 Buffer_TX[SIZE_RegressionBuffer];
t_u8 Buffer_RX[SIZE_RegressionBuffer];
t_ttc_semaphore_smart REG_USART_Buffer_received;

//} Global Variables
//{ Function definitions *************************************************

void regression_start_usart() {

    ttc_task_create(regression_usart_task,  // function to start as thread
                    "regressionUSART",      // thread name (just for debugging)
                    256,                    // stack size
                    (void*)NULL,            // passed as argument to firstTask()
                    (t_base) 1,             // task priority (higher values mean more process time)
                    NULL                    // can return a handle to created task
                    );
}
void regression_usart_task(void * TaskArgument){
    t_ttc_usart_config* Config;
    t_u32 Timeout = TIMEOUT_RX;
    t_u8 MaxUSART = ttc_usart_get_max_logicalindex();
    Assert(MaxUSART > 0, ttc_assert_origin_auto); // no USARTs defined by board makefile!
    e_ttc_usart_errorcode Error;

#ifdef TTC_LED1
    ttc_gpio_init(TTC_LED1, E_ttc_gpio_mode_output_push_pull);
#endif
#ifdef TTC_LED2
    ttc_gpio_init(TTC_LED2, E_ttc_gpio_mode_output_push_pull);
#endif

    ttc_semaphore_init(&REG_USART_Buffer_received);
    if (REGRESSION_USART <= MaxUSART) { // USART defined by board: initialize it
        Config = ttc_usart_get_configuration(REGRESSION_USART);
        Assert(Config, ttc_assert_origin_auto);

        // change some settings
        Config->Flags.Bits.IrqOnRxNE = 1; // enable IRQ on Rx-buffer not empty
        Config->Init.Char_EndOfLine       = 0xff;      // packets end at carriage return
        Config->Size_Queue_Rx        = SIZE_RegressionBuffer*2;
        Config->Size_Queue_Tx        = SIZE_RegressionBuffer*2;
        if (1)
            Config->Init.Flags.DelayedTransmits = 1; // all transmits are queued and sent in interrupt service routin parallel to this task
        else
            Config->Init.Flags.DelayedTransmits = 0; // all transmits are blocking until data is send

        Error = ttc_usart_init(REGRESSION_USART); // note: first bytes might get lost until ttc_usart_register_receive()
        ttc_usart_register_receive(REGRESSION_USART, reg_usart_receiveData, (void*) REGRESSION_USART);
        Assert( Error == tue_OK, ttc_assert_origin_auto);

    } else {
        Assert(FALSE, ttc_assert_origin_auto);
    }


    // set first logical USART as standard output (otherwise last initialized one would be used)
    ttc_usart_stdout_set(REGRESSION_USART);

    // set usart as stdout interface
    // ttc_string_register_stdout(ttc_channel_usart_send_block, ttc_usart_stdout_send_string);
    ttc_string_register_stdout(ttc_usart_stdout_send_block, ttc_usart_stdout_send_string);

    for (int p = 0; p < SIZE_RegressionBuffer; p++) {
        Buffer_TX[p] = p;
    }
    Buffer_TX[SIZE_RegressionBuffer-1] = Config->Init.Char_EndOfLine; //make sure buffer is terminated so that reg_usart_receiveData will be called

    t_base TotalRuns = 0;
    while (1) {
        for (int Run = 0; Run < AMOUNT_REGRESSION_RUNS; Run++) {
            ttc_semaphore_init(&REG_USART_Buffer_received);
#ifdef TTC_LED1
            static t_u8 StateTX = 1;
            if (StateTX++ & 1)
                ttc_gpio_set(TTC_LED1);
            else
                ttc_gpio_clr(TTC_LED1);
#endif
            ttc_usart_send_raw(REGRESSION_USART, Buffer_TX, SIZE_RegressionBuffer);

            if (1) { // wait until buffer has been received
                Timeout = TIMEOUT_RX ;
                t_u8 BytesReceived = 0;
                while (Timeout && (BytesReceived < SIZE_RegressionBuffer) ) {
                    ttc_task_msleep(10);
                    BytesReceived = ttc_semaphore_available(&REG_USART_Buffer_received);
                    Timeout--;
                }
                Assert(Timeout, ttc_assert_origin_auto); // data was not received within timeout!
#ifdef TTC_LED2
                static t_u8 StateRX = 1;
                if (StateRX++ & 1)
                    ttc_gpio_set(TTC_LED2);
                else
                    ttc_gpio_clr(TTC_LED2);
#endif
                t_base Mismatch = ttc_memory_compare(Buffer_RX, Buffer_TX, SIZE_RegressionBuffer);
                Assert(Mismatch == 0, ttc_assert_origin_auto); // ERROR: received data is corrupt!
                ttc_memory_set(Buffer_RX, 0, SIZE_RegressionBuffer);
            }
            else {   // wait 1 second
                ttc_task_msleep(1000);
            }
            ttc_task_msleep(100);
            TotalRuns++;
        }

        ttc_basic_test_ok(); // test passed successfully
    }
}
t_ttc_heap_block* reg_usart_receiveData(void* Argument, t_ttc_usart_config* USART_Generic, t_ttc_heap_block* Data) {
    Assert(USART_Generic != NULL, ttc_assert_origin_auto);
    Assert(Data          != NULL, ttc_assert_origin_auto);

    // example implementation: forwarding data USART1<->USART2
    //
    t_u32 UsartIndex = (t_u32) Argument;

    switch (UsartIndex) {

    case REGRESSION_USART: { // copy data USART --> Buffer_RX
        if (Data->Size <= SIZE_RegressionBuffer) {
            ttc_memory_copy(Buffer_RX, Data->Buffer, Data->Size);
            ttc_semaphore_give(&REG_USART_Buffer_received, Data->Size);
            //Data->Buffer[Data->Size-1] = 0; // terminate string
        }
        else {
            Assert(FALSE, ttc_assert_origin_auto);
        }
        break;
    }
    default: Assert(0, ttc_assert_origin_auto); break; // should never happen!
    }

    return Data; // memory block can now be reused
}

//}FunctionDefinitions
