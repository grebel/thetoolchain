/*{ regression_ttc_timer.h ************************************************
 
  Regression test for memory pool driver
  written by Gregor Rebel 2013
 
}*/
#ifndef regression_ttc_timer_H
#define regression_ttc_timer_H

//{ Includes *************************************************************

#include "ttc-lib/ttc_basic.h"
#include "ttc-lib/ttc_gpio.h"
#include "ttc-lib/ttc_task.h"
#include "ttc-lib/ttc_string.h"
#include "ttc-lib/ttc_memory.h"
#include "ttc-lib/ttc_random.h"
#include "ttc-lib/ttc_list.h"
#include "ttc-lib/ttc_timer.h"

//} Includes
//{ Defines/ TypeDefs ****************************************************

//} Defines

//{ Function prototypes **************************************************

// main entry: initializes hardware + spawns all threads
// returns immediately
void regression_ttc_timer_start();

void rti_taskProducer(void* Arg);
void rti_taskConsumer(void* Arg);

//} Function prototypes
#endif //regression_ttc_timer_H
