/** regression_list.c ************************************************{
 
  Regression test for ttc_list.c/.h

  written by Gregor Rebel 2013
}*/

#include "regression_list.h"


//{ Function definitions *************************************************
//{ RMA-Implementation ****************************************************

// structures

//} RMA-Implementation
//{ Global Variables *****************************************************

typedef struct {
    // must be first entry
    t_ttc_list_item Item;

    // value stored in this item
    t_u32 Value;
} t_valueitem;

/*
#undef TTC_LIST_TTC_GPIO_USH_BACK_SINGLE
#define TTC_LIST_TTC_GPIO_USH_BACK_SINGLE TTC_GPIO_E1
#undef TTC_LIST_TTC_GPIO_ULL_FRONT_SINGLE
#define TTC_LIST_TTC_GPIO_ULL_FRONT_SINGLE TTC_GPIO_E3
*/

#define AMOUNT_VALUE_ITEMS 100
t_valueitem ValueItems[AMOUNT_VALUE_ITEMS];
t_valueitem* NextValueItem = ValueItems;

t_base rli_ElapsedTime1 = 0;
t_base rli_ElapsedTime2 = 0;

// transports items rli_taskProducer() -> rli_taskConsumer()
t_ttc_list rli_ListProducer2Consumer;

// transports items rli_taskProducer() <- rli_taskConsumer()
t_ttc_list rli_ListConsumer2Producer;

//} Global Variables

void list_push_back_single(t_ttc_list* List, t_ttc_list_item* Item) {

    t_base Time = ttc_systick_get_elapsed_usecs() / 100000;
    if (rli_ElapsedTime1 != Time) { // another second: signal activity
        rli_ElapsedTime1 = Time;
#ifdef TTC_LIST_TTC_GPIO_USH_BACK_SINGLE
        if (rli_ElapsedTime1 & 0x1)
            ttc_gpio_set(TTC_LIST_TTC_GPIO_USH_BACK_SINGLE);
        else
            ttc_gpio_clr(TTC_LIST_TTC_GPIO_USH_BACK_SINGLE);
#endif
    }
    ttc_list_push_back_single(List, Item);
}
t_ttc_list_item* list_pop_front_single(t_ttc_list* List) {

    t_base Time = ttc_systick_get_elapsed_usecs() / 100000;
    if (rli_ElapsedTime2 != Time) { // another second: signal activity
        rli_ElapsedTime2 = Time;
#ifdef TTC_LIST_TTC_GPIO_ULL_FRONT_SINGLE
        if (rli_ElapsedTime2 & 0x1)
            ttc_gpio_set(TTC_LIST_TTC_GPIO_ULL_FRONT_SINGLE);
        else
            ttc_gpio_clr(TTC_LIST_TTC_GPIO_ULL_FRONT_SINGLE);
#endif
    }

    return ttc_list_pop_front_single_wait(List);
}

void regression_list_start() {

#ifdef TTC_LIST_TTC_GPIO_USH_BACK_SINGLE
    ttc_gpio_init(TTC_LIST_TTC_GPIO_USH_BACK_SINGLE,  E_ttc_gpio_mode_output_push_pull);
#endif
#ifdef TTC_LIST_TTC_GPIO_ULL_FRONT_SINGLE
    ttc_gpio_init(TTC_LIST_TTC_GPIO_ULL_FRONT_SINGLE, E_ttc_gpio_mode_output_push_pull);
#endif

    // TestRMA
    ttc_list_init(&rli_ListProducer2Consumer, "rli_ListProducer2Consumer");
    ttc_list_init(&rli_ListConsumer2Producer, "rli_ListConsumer2Producer");

    t_valueitem* ValueItem = ValueItems;
    for (t_u8 Count = 0; Count < AMOUNT_VALUE_ITEMS; Count++) {
        ValueItem->Value = Count;
        list_push_back_single(&rli_ListProducer2Consumer, &(ValueItem->Item));
        ValueItem++;
    }

    t_base SleepMax = 88;
    for (t_u8 Amount = 8; Amount > 0; Amount--)
        ttc_task_create(rli_taskProducer, "rli_taskProducer", 128, (void*) SleepMax, 0, NULL);

    SleepMax = 20;
    for (t_u8 Amount = 8; Amount > 0; Amount--)
        ttc_task_create(rli_taskConsumer, "rli_taskConsumer", 128, (void*) SleepMax, 0, NULL);
}
void rli_taskProducer(void* Arg) {
    t_base TimeStart = ttc_systick_get_elapsed_usecs() / 1000000;
    t_base ItemsProcessed = 1;

    while (1) {
        t_valueitem* ValueItem = (t_valueitem*) list_pop_front_single(&rli_ListConsumer2Producer);
        if (ValueItem) {
            ItemsProcessed++;
            list_push_back_single(&rli_ListProducer2Consumer, &(ValueItem->Item));
        }
        if (ItemsProcessed == 10000)
            ttc_basic_test_ok();
        t_base Time = ttc_systick_get_elapsed_usecs() / 1000000;
        Assert(Time - TimeStart < 10, ttc_assert_origin_auto); // Test took too long!
    }
}
void rli_taskConsumer(void* Arg) {
    t_base TimeStart = ttc_systick_get_elapsed_usecs() / 1000000;
    t_base ItemsProcessed = 0;

    while (1) {
        t_valueitem* ValueItem = (t_valueitem*) list_pop_front_single(&rli_ListProducer2Consumer);
        if (ValueItem) {
            ItemsProcessed++;
            list_push_back_single(&rli_ListConsumer2Producer, &(ValueItem->Item));
        }
        if (ItemsProcessed == 10000)
            ttc_basic_test_ok();
        t_base Time = ttc_systick_get_elapsed_usecs() / 1000000;
        Assert(Time - TimeStart < 10, ttc_assert_origin_auto); // Test took too long!
    }
}


//}FunctionDefinitions
