/*{ regression_heap_pools.h ************************************************
 
  Regression test for memory pool driver
  written by Gregor Rebel 2013
 
}*/
#ifndef regression_heap_pools_H
#define regression_heap_pools_H

//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_task.h"
#include "ttc_string.h"
#include "ttc_memory.h"
#include "ttc_gfx.h"
#include "ttc_random.h"
#include "ttc_list.h"
#include "ttc_queue.h"
#include "DEPRECATED_math_basic.h"

//} Includes
//{ Defines/ TypeDefs ****************************************************

#ifdef EXTENSION_ttc_heap_sdma
#  include "heap_sdma.h"
#endif

//} Defines

typedef struct {
    t_u8 X;
    t_u8 Y;
    const char* Format;
    t_base Value;
} printJob_t;

//{ Function prototypes **************************************************

// enqueues print job
void printAt(t_u8 X, t_u8 Y, const char* Format, t_u32 Value);

void taskPrint(void* Arg);

// main entry: initializes hardware + spawns all threads
// returns immediately
void regression_heap_pools_start();

// main task running regression test
// This task monitors running regression and displays current status on LCD (if available)
void taskRegression_MemoryPools(void* Arg);

//} Function prototypes
#endif //regression_heap_pools_H
