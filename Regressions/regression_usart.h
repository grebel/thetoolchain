/*{ regression_list.h ************************************************
 
  Regression test for USART communication
  written by Sascha Poggemann 2013
 
}*/

#ifndef regression_USART_H
#define regression_USART_H

/**

Hardwae requirements: You need to connect the USART RX and TX pin.

TTC_USART2_TX=TTC_GPIO_D5   # RS232 on 10-pin male header connector TX-Pin
TTC_USART2_RX=TTC_GPIO_D6   # RS232 on 10-pin male header connector RX-Pin

This Regression will assert if the received data does not arrive before a timeout
This Regression will assert if the received and transmitted data are different.

**/


//{ Includes *************************************************************

#include "ttc_basic.h"  // Include First! (basic datatypes and definitions)
#include "ttc_task.h"
#include "ttc_string.h"
#include "ttc_memory.h"
#include "ttc_random.h"
#include "ttc_list.h"
#include "ttc_usart.h"

//} Includes
//{ Defines/ TypeDefs ****************************************************

//} Defines

//{ Function prototypes **************************************************

// main entry: initializes hardware + spawns all threads
// returns immediately
void regression_start_usart();
//the main task for this regression
void regression_usart_task(void * TaskArgument);
t_ttc_heap_block* reg_usart_receiveData(void* Argument, t_ttc_usart_config* USART_Generic, t_ttc_heap_block* Data) ;
//} Function prototypes
#endif //regression_USART_H
