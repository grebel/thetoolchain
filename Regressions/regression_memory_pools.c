/*{ regression_heap_pools.c ************************************************
 
  regression_heap_pools_start() spawns thread taskBrightness()

  taskBrightness()
      Defines brightness of led as Ton / (Ton + Toff)
      spawns thread taskLEDs()
      while (1) {
        while (Ton < 100) { Ton++; Toff--; }
        while (Ton >   0) { Ton--; Toff++; }
      }

  taskLEDs()
      while (1) {
        switch on LED
        wait for Ton
        switch off LED
        wait for Toff
      }

}*/

#include "regression_heap_pools.h"

#ifdef EXTENSION_ttc_gfx
// read-only configuration of current display (width, height, ...)
t_ttc_gfx_generic* DisplayConfig = NULL;
#endif
t_u8 rmp_TextRows = 0;  // total amount of text rows on current display

//{ Function definitions *************************************************
//{ FCTMA-Implementation ****************************************************

#define mutex_create     ttc_mutex_create
#define mutex_lock       ttc_mutex_lock
#define mutex_unlock     ttc_mutex_unlock
#define random(MAX)      ttc_random_generate() % MAX

// ==1: multitasking is disabled during memory operations
#define AVOID_TASK_SWITCHES 1

// select implementation to use as Dynamic Memory Allocator
typedef enum {
    dma_FCTMA,           // Fast Constant Time Memory Allocator using memory pools + memory headers
    dma_SDMA,            // using implementation from heap/heap_sdma.c
    dma_FreeRTOS         // heap memory allocator as provided by FreeRTOS
} DMA_Algorithm_e;
#define DMA_Algorithm dma_FreeRTOS

// structures

// global variables
static t_ttc_heap_pool* Pool100 = NULL;

// amount of blocks allocated on heap
extern t_base ttc_memory_Amount_Blocks_Allocated;

// amount of bytes occupied on heap by dynamic allocations
extern t_base ttc_memory_Bytes_Allocated_Total;

// usable amount of allocated bytes
extern t_base ttc_memory_Bytes_Allocated_Usable;

//} FCTMA-Implementation
//{ Global Variables *****************************************************

static t_ttc_heap_pool* PoolPrintJobs;
void rmp_taskProducer(void* Arg);
void rmp_taskConsumer(void* Arg);
t_ttc_list rmp_BlockList;
t_base rmp_AmountHeapFreeAtStart = 0;

// amount of producer tasks to spawn
#define Amount_Producer 10

// amount of consumer tasks to spawn
#define Amount_Consumer 10

// amount of blocks to allocate in each turn
#define Amount_BlocksPerTurn  20

// size of each memory block
#define Block_Size 127


//} Global Variables

typedef struct {
    // read by task
    t_base SleepTime_Max;

    // written by task
    t_base BlocksProcessed;    // total amount of memory blocks processed by this task so far
    t_base SleepTime_Total;    // total amount of sleeps of this task so far
    t_base SleepTime_Average;  // = SleepTime_Total / BlocksProcessed
} TaskInfo_t;

TaskInfo_t  rmp_ProducerTasks[Amount_Producer + 1];
TaskInfo_t* rmp_EndOf_ProducerTasks = rmp_ProducerTasks;
TaskInfo_t  rmp_ConsumerTasks[Amount_Consumer + 1];
TaskInfo_t* rmp_EndOf_ConsumerTasks = rmp_ConsumerTasks;

void* mallocBlock() {
    ttc_gpio_init(TTC_MEMORY_DEBUG_POOL_ALLOC_PIN, E_ttc_gpio_mode_output_push_pull);
    void* Block = NULL;

    switch (DMA_Algorithm) {
    case dma_FCTMA:
        Block = (void*) ttc_heap_pool_block_get(Pool100);
        break;
    case dma_FreeRTOS:
#ifdef TTC_MEMORY_DEBUG_POOL_ALLOC_PIN
        if (0) ttc_task_critical_begin(mallocBlock);
        //ttc_gpio_set(TTC_MEMORY_DEBUG_POOL_ALLOC_PIN);
#endif
        Block = pvPortMalloc(Block_Size);
#ifdef TTC_MEMORY_DEBUG_POOL_ALLOC_PIN
        //ttc_gpio_clr(TTC_MEMORY_DEBUG_POOL_ALLOC_PIN);
        if (0) ttc_task_critical_end();
#endif
        break;
    case dma_SDMA:
#ifdef TTC_MEMORY_DEBUG_POOL_ALLOC_PIN
        if (1) ttc_task_critical_begin(mallocBlock);
        ttc_gpio_set(TTC_MEMORY_DEBUG_POOL_ALLOC_PIN);
#endif
#ifdef EXTENSION_ttc_heap_sdma
        Block = heap_sdma_alloc(Block_Size);
#else
        ttc_assert_halt_origin(ttc_assert_origin_auto); // heap_sdma must be choosen as memory allocator
#endif
#ifdef TTC_MEMORY_DEBUG_POOL_ALLOC_PIN
        ttc_gpio_clr(TTC_MEMORY_DEBUG_POOL_ALLOC_PIN);
        if (1) ttc_task_critical_end();
#endif
        break;
    default:
        ttc_assert_halt_origin(ttc_assert_origin_auto);
        break;
    }

    return Block;
}
void releaseBlock(t_ttc_heap_block_from_pool* Block) {
    ttc_gpio_init(TTC_MEMORY_DEBUG_POOL_RELEASE_PIN, E_ttc_gpio_mode_output_push_pull);

    switch (DMA_Algorithm) {
    case dma_FCTMA:
        ttc_heap_pool_block_free(Block);
        break;
    case dma_FreeRTOS:
#ifdef TTC_MEMORY_DEBUG_POOL_RELEASE_PIN
        if (0) ttc_task_critical_begin(releaseBlock);
        //ttc_gpio_set(TTC_MEMORY_DEBUG_POOL_RELEASE_PIN);
#endif
        vPortFree(Block);
#ifdef TTC_MEMORY_DEBUG_POOL_RELEASE_PIN
        ttc_gpio_clr(TTC_MEMORY_DEBUG_POOL_RELEASE_PIN);
        if (0) ttc_task_critical_end();
#endif
        break;
    case dma_SDMA:
#ifdef TTC_MEMORY_DEBUG_POOL_RELEASE_PIN
        if (1) ttc_task_critical_begin(releaseBlock);
        ttc_gpio_set(TTC_MEMORY_DEBUG_POOL_RELEASE_PIN);
#endif
#ifdef EXTENSION_ttc_heap_sdma
        heap_sdma_free(Block);
#else
        ttc_assert_halt_origin(ttc_assert_origin_auto); // heap_sdma must be choosen as memory allocator
#endif
#ifdef TTC_MEMORY_DEBUG_POOL_RELEASE_PIN
        ttc_gpio_clr(TTC_MEMORY_DEBUG_POOL_RELEASE_PIN);
        if (1) ttc_task_critical_end();
#endif
        break;
    default:
        ttc_assert_halt_origin(ttc_assert_origin_auto);
        break;
    }
}
void push2Consumer(void* Block) {

    ttc_list_push_back_single(&rmp_BlockList, (t_ttc_list_item*) Block);
}
void* popFromProducer() {
    void* Block;

    Block = (void*) ttc_list_pop_front_single_wait(&rmp_BlockList);

    return Block;
}
void regression_heap_pools_start() {

#ifdef TTC_MEMORY_DEBUG_POOL_RELEASE_PIN
#endif
#ifdef TTC_MEMORY_DEBUG_POOL_ALLOC_PIN
#endif

#ifdef EXTENSION_ttc_gfx
    t_u8 MaxDisplay = ttc_gfx_get_max_display_index();
    Assert(MaxDisplay > 0, ttc_assert_origin_auto); // no displays defined by board makefile!
    DisplayConfig = ttc_gfx_get_configuration(1);
    Assert(DisplayConfig, ttc_assert_origin_auto);
    rmp_TextRows  = DisplayConfig->rmp_TextRows;
    
    // set color to use for first screen clear
    ttc_gfx_color_bg24(GFX_COLOR24_AQUA);
    ttc_gfx_init(1);

    // initialize gfx for use with printf()
    ttc_gfx_stdout_set(1);
    ttc_string_register_stdout(ttc_gfx_stdout_print_block, ttc_gfx_stdout_print_string);
#endif

    PoolPrintJobs = ttc_heap_pool_create(sizeof(printJob_t), 20);
    ttc_list_init(&rmp_BlockList, "rmp_BlockList");

    switch (DMA_Algorithm) { // initialize dynamic data structures
    case dma_FCTMA:
        Pool100 = ttc_heap_pool_create( Block_Size, 160 );
        break;
    default:
        break;
    }

    static t_u8 Temp3[20];
    t_base SleepMax;
    SleepMax = 10;
    for (t_u8 Index = 0; Index < Amount_Consumer; Index++) {
        snprintf(Temp3, 20, "Consumer%02i", Index);
        rmp_EndOf_ConsumerTasks->SleepTime_Max = SleepMax;
        ttc_task_create(rmp_taskConsumer, (char*) Temp3, 256, (void*) rmp_EndOf_ConsumerTasks++, 0, NULL);
        ttc_task_check_stack();
    }

    SleepMax = 15;
    for (t_u8 Index = 0; Index < Amount_Producer; Index++) {
        snprintf(Temp3, 20, "Producer%02i", Index);
        rmp_EndOf_ProducerTasks->SleepTime_Max = SleepMax;
        ttc_task_create(rmp_taskProducer, (char*) Temp3, 256, (void*) rmp_EndOf_ProducerTasks++, 0, NULL);
        ttc_task_check_stack();
    }

    ttc_task_create(taskRegression_MemoryPools, "MemoryPools", 256, NULL, 1, NULL);
}
void taskRegression_MemoryPools(void* Arg) {
    (void) Arg;

    switch (DMA_Algorithm) {
    case dma_FCTMA:
        rmp_printAt(0,0, "Test: FCTMA", 0);
        break;
    case dma_FreeRTOS:
        rmp_printAt(0,0, "Test: FreeRTOS", 0);
        break;
    case dma_SDMA:
        rmp_printAt(0,0, "Test: SDMA", 0);
        break;
    default:
        ttc_assert_halt_origin(ttc_assert_origin_auto);
        break;
    }

    rmp_printAt(2, rmp_TextRows - 1, "used ", 0);
    rmp_AmountHeapFreeAtStart = ttc_heap_get_free_size();

    t_base P_BlocksProcessed, P_SleepTime_Total;
    t_base C_BlocksProcessed, C_SleepTime_Total;
    TaskInfo_t* TaskInfo = NULL;
    t_base SleepTime_Average;

    while (1) {
        t_u8 Row = 1;

        P_BlocksProcessed   = 1;
        P_SleepTime_Total   = 0;
        TaskInfo = rmp_ProducerTasks;
        while (TaskInfo < rmp_EndOf_ProducerTasks) { // collect statistic data of all producer tasks
            P_BlocksProcessed += TaskInfo->BlocksProcessed;
            P_SleepTime_Total += TaskInfo->SleepTime_Total;
            TaskInfo++;
        }

        C_BlocksProcessed   = 1;
        C_SleepTime_Total   = 0;
        TaskInfo = rmp_ConsumerTasks;
        while (TaskInfo < rmp_EndOf_ConsumerTasks) { // collect statistic data of all consumer tasks
            C_BlocksProcessed += TaskInfo->BlocksProcessed;
            C_SleepTime_Total += TaskInfo->SleepTime_Total;
            TaskInfo++;
        }

        rmp_printAt(0, Row, "P(%2i)", Amount_Producer);
        rmp_printAt(6, Row, "#%07i",  P_BlocksProcessed);
        Row++;
        SleepTime_Average = DIV(P_SleepTime_Total, P_BlocksProcessed);
        rmp_printAt( 0, Row++, "SleepAvg %3i ", SleepTime_Average);
        rmp_printAt(0, Row, "C(%2i)", Amount_Consumer);
        rmp_printAt(6, Row, "#%07i",  C_BlocksProcessed);
        Row++;
        SleepTime_Average = DIV(C_SleepTime_Total, C_BlocksProcessed);
        rmp_printAt( 0, Row++, "SleepAvg %3i ", SleepTime_Average);

        if (C_BlocksProcessed > 10000)
            ttc_basic_test_ok();

        if (DMA_Algorithm == dma_FCTMA) { // display extra pool-statistics
#if TTC_HEAP_POOL_STATISTICS == 1
            rmp_printAt( 0, Row++, "Alloc    %4i", Pool100->Statistics.AmountAllocated);
            rmp_printAt( 0, Row++, "MaxFree  %4i", Pool100->Statistics.AmountFreeMaximum);
            rmp_printAt( 0, Row++, "Available%4i", ttc_semaphore_available(&(Pool100->BlocksAvailable)));
            rmp_printAt( 0, Row++, "Free     %4i", Pool100->Statistics.AmountFreeCurrently);
#endif
        }
        rmp_printAt( 0, Row++, "Total  %6i", ttc_memory_Bytes_Allocated_Total);
        rmp_printAt( 0, Row++, "Usable %6i", ttc_memory_Bytes_Allocated_Usable);
        rmp_printAt( 0, Row++, "Alloc# %3i", ttc_memory_Amount_Blocks_Allocated);


        if (0) rmp_printAt(8, rmp_TextRows - 1, "%iB", rmp_AmountHeapFreeAtStart - ttc_heap_get_free_size());
        static t_u8 Count = 0;
        if (Count++ & 1)
            rmp_printAt(0, rmp_TextRows - 1, "+", 0);
        else
            rmp_printAt(0, rmp_TextRows - 1, "-", 0);
        ttc_task_msleep(10000);
    }
}
void rmp_taskProducer(void* Arg) {
    TaskInfo_t* MyTaskInfo = (TaskInfo_t*) Arg;
    //ttc_task_msleep(700); // Will cause hard-fault!

    void* Block = NULL;
    ttc_gpio_init(TTC_GPIO_C6, E_ttc_gpio_mode_output_push_pull); //D

    while (1) {
#ifdef TTC_REGRESSION
        ttc_task_check_stack();
#endif

        ttc_gpio_set(TTC_GPIO_C6); //D
        for (t_u8 I = Amount_BlocksPerTurn; I > 0; I--) {
            Block = mallocBlock();

            if (Block) {
                MyTaskInfo->BlocksProcessed++;
                unsigned char* Data = (unsigned char*) Block;
                (void) Data;

                push2Consumer(Block);
            }
            else {
                ttc_task_yield();
            }
        }
        ttc_gpio_clr(TTC_GPIO_C6); //D

        if (MyTaskInfo->BlocksProcessed)
            MyTaskInfo->SleepTime_Average = DIV(MyTaskInfo->SleepTime_Total, MyTaskInfo->BlocksProcessed / Amount_BlocksPerTurn);

        if (ttc_memory_Bytes_Allocated_Total > 16384) { // force allocation until certain amount of memory has been allocated
            // simulate random time required to fill Data[]
            t_base SleepTime = 1 + random(MyTaskInfo->SleepTime_Max);
            MyTaskInfo->SleepTime_Total += SleepTime;
            ttc_task_msleep(SleepTime);
        }
    }
}
void rmp_taskConsumer(void* Arg) {
    TaskInfo_t* MyTaskInfo = (TaskInfo_t*) Arg;
    //ttc_task_msleep(500); // Will cause hard-fault!

    while (1) {
        ttc_task_check_stack(); //D

        // simulate random processing time of Data[]
        t_base SleepTime = 1 + random(MyTaskInfo->SleepTime_Max);
        MyTaskInfo->SleepTime_Total += SleepTime;
        ttc_task_msleep(SleepTime);

        for (t_u8 I = Amount_BlocksPerTurn; I > 0; I--) {
            void *Block = popFromProducer();
            if (Block) {
//                if (DMA_Algorithm == dma_FCTMA) {
//                    t_ttc_list_item* Item = (t_ttc_list_item*) Block;
//                    Block = (void*) ttc_heap_pool_from_list_item(Item);
//                    Assert(ttc_memory_is_writable(Block), ttc_assert_origin_auto);
//                }
                unsigned char* Data = (unsigned char*) Block;
                (void) Data;

                releaseBlock(Block);
                MyTaskInfo->BlocksProcessed++;
            }
            else
                ttc_task_yield();
        }
        if (MyTaskInfo->BlocksProcessed)
            MyTaskInfo->SleepTime_Average = DIV(MyTaskInfo->SleepTime_Total, MyTaskInfo->BlocksProcessed / Amount_BlocksPerTurn);
    }
}
void rmp_printAt(t_u8 X, t_u8 Y, const char* Format, t_u32 Value) {
    static TaskHandle_t* MyTask = NULL;
    if (MyTask) {
        Assert(MyTask == ttc_task_current_handle(), ttc_assert_origin_auto); // this function may only be used from one task
    }
    else {
        MyTask = ttc_task_current_handle(); // called first time: store owner task
        Assert(ttc_memory_is_writable(MyTask), ttc_assert_origin_auto);
    }

#ifdef EXTENSION_ttc_gfx
    static t_u8 Temp1[100]; // there can be only one taskPrint() instance
    ttc_gfx_text_cursor_set(X, Y);
    ttc_string_snprintf(Temp1, 100, Format, Value);
    ttc_gfx_text_solid( (char*) Temp1, 100);
#endif
}

//}FunctionDefinitions
