/** regression_ttc_timer.c ************************************************{
 
  Regression test for ttc_timer.c/.h

  written by Francisco Estevez 2014
}*/

#include "regression_ttc_timer.h"


//{ Function definitions *************************************************
//{ RMA-Implementation ****************************************************

// structures

//} RMA-Implementation
//{ Global Variables *****************************************************

typedef struct {
    // must be first entry
    t_ttc_list_item Item;

    // value stored in this item
    t_u32 Value;
} t_valueitem;

#define AMOUNT_VALUE_ITEMS 100
t_valueitem ValueItems[AMOUNT_VALUE_ITEMS];
t_valueitem* NextValueItem = ValueItems;

// transports items rti_taskProducer() -> rti_taskConsumer()
t_ttc_list rti_ListProducer2Consumer;

// transports items rti_taskProducer() <- rti_taskConsumer()
t_ttc_list rti_ListConsumer2Producer;

t_base ProducerItemsProcessed = 0;
t_base ConsumerItemsProcessed = 0;
//} Global Variables

void regression_ttc_timer_start() {

#ifdef TTC_TIMER_TTC_GPIO_USH_BACK_SINGLE
    ttc_gpio_init(TTC_TIMER_TTC_GPIO_USH_BACK_SINGLE,  E_ttc_gpio_mode_output_push_pull);
#endif
#ifdef TTC_TIMER_TTC_GPIO_ULL_FRONT_SINGLE
    ttc_gpio_init(TTC_TIMER_TTC_GPIO_ULL_FRONT_SINGLE, E_ttc_gpio_mode_output_push_pull);
#endif

    // TestRMA
    ttc_list_init(&rti_ListProducer2Consumer, "rti_ListProducer2Consumer");
    ttc_list_init(&rti_ListConsumer2Producer, "rti_ListConsumer2Producer");

    t_valueitem* ValueItem = ValueItems;
    for (t_u8 Count = 0; Count < AMOUNT_VALUE_ITEMS; Count++) {
        ValueItem->Value = Count;
        ttc_list_push_back_single(&rti_ListProducer2Consumer, &(ValueItem->Item));
        ValueItem++;
    }

    ttc_hardware_timer_set_isr(TTC_TIMER2, rti_taskProducer, NULL, 1000, 1);
    ttc_hardware_timer_set_isr(TTC_TIMER3, rti_taskConsumer, NULL, 500, 1);
}
void rti_taskProducer(void* Arg) {
    (void)Arg;
    t_valueitem* ValueItem = (t_valueitem*) ttc_list_pop_front_single_wait(&rti_ListConsumer2Producer);
    if (ValueItem) {
        ProducerItemsProcessed++;
        ttc_list_push_back_single(&rti_ListProducer2Consumer, &(ValueItem->Item));
#ifdef TTC_TIMER_TTC_GPIO_USH_BACK_SINGLE
        if (ProducerItemsProcessed & 0x1)
            ttc_gpio_set(TTC_TIMER_TTC_GPIO_USH_BACK_SINGLE);
        else
            ttc_gpio_clr(TTC_TIMER_TTC_GPIO_USH_BACK_SINGLE);
#endif
    }
    if (ProducerItemsProcessed == 100)
        ttc_basic_test_ok();
    else ttc_hardware_timer_set_isr(TTC_TIMER2, rti_taskProducer, NULL, 1000, 1);
}
void rti_taskConsumer(void* Arg) {
    (void)Arg;
    t_valueitem* ValueItem = (t_valueitem*) ttc_list_pop_front_single_wait(&rti_ListProducer2Consumer);
    if (ValueItem) {
        ConsumerItemsProcessed++;
        ttc_list_push_back_single(&rti_ListConsumer2Producer, &(ValueItem->Item));
#ifdef TTC_TIMER_TTC_GPIO_ULL_FRONT_SINGLE
        if (ConsumerItemsProcessed & 0x1)
            ttc_gpio_set(TTC_TIMER_TTC_GPIO_ULL_FRONT_SINGLE);
        else
            ttc_gpio_clr(TTC_TIMER_TTC_GPIO_ULL_FRONT_SINGLE);
#endif
    }
    if (ConsumerItemsProcessed == 100)
        ttc_basic_test_ok();
    else ttc_hardware_timer_set_isr(TTC_TIMER3, rti_taskConsumer, NULL, 500, 1);
}


//}FunctionDefinitions
