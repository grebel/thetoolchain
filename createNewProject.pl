#!/usr/bin/perl -w

use strict;
use File::Spec::Functions qw(rel2abs);
use File::Basename;
my $ToolChainDir = dirname(rel2abs($0));

measureTime(10);
my $ProjectName = shift(@ARGV) || '';
my $AuthorName  = shift(@ARGV) || '';
my $Option      = shift(@ARGV) || '';
my $ResetActivateScript = { reset => 1 }->{ lc($Option) } || 0;
my $Debug = 0; # =1: activate extra debug messages

# replace problematic characters in project name 
$ProjectName =~ s/-/_/g; # gives compiler warnings when used in constant defines

my $StartPWD=`pwd`;
chomp($StartPWD);
my $ProjectDir = $ENV{HOME}.'/Source/Projects';  # where to create new projects
unless (-d $ProjectDir) {
  print "creating project folder $ProjectDir...\n";
  mkdir($ProjectDir);
  my $Script = $ProjectDir.'/createNewProject.pl';
  writeFile($Script, <<"END_OF_SCRIPT");
#!/bin/bash

cd $ENV{HOME}/Source/TheToolChain
./createNewProject.pl \$\@
END_OF_SCRIPT
  chmod 0755, $Script
}
chdir($ProjectDir);
$ProjectDir .= '/'.$ProjectName;
my @Errors;

unless ($AuthorName) {
  print <<"END_OF_HELP";
$0 <PROJECT_NAME> <AUTHOR_NAME> [RESET]\n";

Creates a new copy of Template project. Automatically sets project name and author in all files.

Arguments:
  <PROJECT_NAME>  A short, descriptive name of your new project (no spaces!)
  <AUTHOR_NAME>   If you want to get credits for your work, you have to give your real name. 
                  You may separate first and last name by an underscore.  
  RESET           If given, the template activate_project.sh is removed and recreated.
                  This is only necessary if you added new extensions.

Examples:
  $0 Test_123 Mac_Guyver

END_OF_HELP
  exit 5;
}

unless ( -d "$ToolChainDir/Template" ) {
  print STDERR "$0 ERROR: Cannot find folder Template in $ToolChainDir/ !\n";
  exit 10;
}
if (-d $ProjectDir) {
  print STDERR "$0 ERROR: Directory $ProjectDir already exists!\n";
  exit 11;
}
if (-e $ProjectName) {
  my $CurrentPWD = `pwd`; chomp($CurrentPWD);
  print STDERR "$0 ERROR: File $CurrentPWD/$ProjectName is in the way!\n";
  exit 12;
}

system("rm -f $ENV{HOME}/Source/TheToolChain");
system("ln -s $ToolChainDir $ENV{HOME}/Source/TheToolChain");

print <<"END_OF_TEXT";

Creating new project 
    Name:   $ProjectName 
    Author: $AuthorName 
    Folder: $ProjectDir
    
END_OF_TEXT
my $Error;
exitOnError(`cp 2>&1 -R '$ToolChainDir/Template' $ProjectDir/ `);

#{ remove some symbolic links and replace them by real copies to avoid trouble with developers changing files in development versions

system("rm 2>/dev/null $ProjectDir/extensions.active/*");
exitOnError(`cp 2>&1 >/dev/null '$ToolChainDir/Version' $ProjectDir/required_version`);
exitOnError(`mv '$ProjectDir/_gitignore' '$ProjectDir/.gitignore'`);

foreach my $Subfolder ("examples", "extensions.local", "configs", "main.c", "makefile", "debug.sh", "createLinks.sh", "compile.sh") {
  if ($Debug) { print "replacing $ProjectDir/$Subfolder ..\n"; }
  system("rm -Rf 2>/dev/null $ProjectDir/$Subfolder");
  exitOnError(`cp 2>&1 -R --dereference '$ToolChainDir/Template/$Subfolder' $ProjectDir/ `);
}

#}

unless (-d $ProjectDir) {
  print STDERR "$0 ERROR: Cannot create directory $ProjectDir in `pwd` !\n";
  exit 13;
}
chdir($ProjectDir);

if ($Debug) { print "renaming template-files...\n"; }
foreach my $FileName ( 'TemplateName.config',
                       'TemplateName.creator',
                       'TemplateName.files',
                       'TemplateName.includes',
                       'TemplateName.project',
                       'activate.100_board_TemplateName.sh',
                       'makefile.100_board_TemplateName*'
                     ) { # rename template files to project name
  my $OrigFileNames = `find ./ -maxdepth 3 -name $FileName`;
  my @OrigFileNames = split("\n", $OrigFileNames);
  foreach my $OrigFileName (@OrigFileNames) {
    my $NewName = $OrigFileName;
    $NewName =~ s/TemplateName/$ProjectName/;
    if ($Debug) { print "  mv $OrigFileName -> $NewName\n"; }
    rename($OrigFileName, $NewName);
  }
}
my $PWD = `pwd`; chomp($PWD);
print "searching for files to be processed in $PWD...\n";
my @Files2Process;
my %Files2Process;
foreach my $Keyword ( qw(TemplateName TemplateAuthor TemplateProjectFolder) ) { # find all files in new project folder containing placeholders
  my $FilesFound = `find ./ -maxdepth 3 -type f -exec grep -l $Keyword {} \\;`;
  my @FilesFound = split("\n", $FilesFound);
  foreach my $FileFound (@FilesFound) {
    my @Parts = split(':', $FileFound);
    my $FileName = $Parts[0];
    if ($FileName && (!$Files2Process{$FileName}) ) {
      push @Files2Process, $FileName;
      $Files2Process{$FileName} = 1;
    }
  }
}

$Error = '';
foreach my $File2Process (@Files2Process) { # replace placeholders in all found files
  my $OldError = $Error;
  if (-l $File2Process) { print "skipping symbolic link $PWD/$File2Process\n"; }
  else {
    open(IN, "<$File2Process") or $Error .= "Cannot read from file '$File2Process' ($!)\n";
    my @Lines;
    if ($OldError eq $Error) {
      @Lines = <IN>;
      close(IN);
      if ($Debug) { print "modifying $PWD/$File2Process ..\n"; }
      foreach my $Line (@Lines) {
        $Line =~ s/TemplateName/$ProjectName/g;
        $Line =~ s/TemplateAuthor/$AuthorName/g;
        $Line =~ s/TemplateProjectFolder/$PWD/g;
      }
      $OldError = $Error;
      open(OUT, ">$File2Process") or $Error .= "Cannot write to file '$File2Process' ($!)\n";
      if ($OldError eq $Error) {
        print OUT join("", @Lines);
        close(OUT);
      }
    }
  }
  if ($Error) { print $Error; }
}

my $Text_ProvideAllFeatures; # will be loaded with a list of all available features being collected from all scripts


if ($Debug) { print "creating symbolic links..\n"; }
system("./createLinks.sh >/dev/null");

if (1) { # updating project specific board extension
  my $ActivateFile = "extensions.local/activate.100_board_${ProjectName}.sh";
  my $MakeFile     = "extensions.local/makefile.100_board_${ProjectName}";

  my %AllFeatures = collectAllFeatures();

  my $MaxLength = 0;
  map { if ($MaxLength < length($_)) { $MaxLength = length($_); } } keys %AllFeatures;
  my $Spaces = ''; while (length($Spaces) < $MaxLength) { $Spaces .= ' '; }
  my @AllFeatures = stringsAlignLeft(sort keys %AllFeatures);
  
  my @ProvideFeatures = map {
                              '  # enableFeature '.$_.' # '.$AllFeatures{strip($_)};
                            } @AllFeatures;

  print "inserting ".(scalar @ProvideFeatures)." features into $ActivateFile\n";
  my $Date = strip(`date`);
  $Text_ProvideAllFeatures =  "  # This list of features has been generated by $0 from all activate scripts on $Date\n".
                              join("\n", @ProvideFeatures)."\n";

  insertIntoFile($ActivateFile, '#InsertFeatures', $Text_ProvideAllFeatures);
  
  my @AllCpuVariants = map { '#  '.$_.' = 1'; } stringsAlignLeft( collectAllCpuVariants() );
  insertIntoFile($MakeFile, '#InsertCpuVariants', 
                 "# This list of CPU-Variants has been extracted from ttc_cpu_types.h by $0 on $Date\n".
                 "# Activate exactly one of these lines:\n".
                 join("\n", @AllCpuVariants)."\n"
                );
}

my $CreateActivateScript = (-e "activate_project.sh") ? 0 : 1;
$CreateActivateScript |= $ResetActivateScript; # if argument RESET is given, we will always recreate activate_project.sh

system("_/clean.sh >/dev/null 2>&1");
if ($CreateActivateScript) {
  print "missing script activate_project.sh - creating new one from all available extensions..\n";
  measureTime(1);
  system("_/createActivateScript.pl ./ "); #>/dev/null 2>&1");
  print "activate_project.sh created after ".measureTime(1)."secs.\n";
}
else { print "Reusing $StartPWD/Template/activate_project.sh (give argument RESET to recreate).\n"; }

print "activating project...";
measureTime(1);
system("./compile.sh NOCOMPILE QUIET >prepare.log 2>&1");
system("_/clean.sh >/dev/null 2>&1");
print measureTime(1)."secs.\n";
if ($CreateActivateScript) { 
  print "Extension setup may have changed during compile.sh: recreating activate-script..";
  measureTime(1);
  system("_/createActivateScript.pl ./ >/dev/null 2>&1"); 
  insertIntoFile("activate_project.sh", '#ProvideFeatures', $Text_ProvideAllFeatures);
  print measureTime(1)."secs.\n";
}
print "compiling project...";
measureTime(1);
system("./compile.sh NOFLASH >compile.log 2>&1");
print measureTime(1)."secs.\n";

if ($CreateActivateScript) { 
  print "creating backup of activate script to speed up future project creation\n";
  system("cp activate_project.sh $StartPWD/Template/");
}
if (1) { # Compile list of activate lines for all board activates inside extensions.local/ and insert them into activate script
  my $Found = `find extensions.local/ -name "activate.100_*"`;
  my @Found = split("\n", $Found);
  my $ProjectBoardActivations = join("\n", grep { $_; }
                                        map { "  # $_ QUIET \"\$0\" || Error '$_'\n"; }
                                        @Found
                                 )."\n";
  if ($ProjectBoardActivations) {
    $ProjectBoardActivations = "  # Activate project specific boards by uncommenting a single line below\n".$ProjectBoardActivations;
  }
  my $ActivationFile = "activate_project.sh";
  my $Lines = readFile($ActivationFile);
  my $InsertMark = "#InsertProjectSpecificBoards";
  my $InsertPos = index($Lines, $InsertMark);
  if ($InsertPos == -1) { print "$0 ERROR: Cannot find insertion mark! Insert a line containing $InsertMark into file $PWD/$ActivationFile.\n"; }
  else {
    if ($Debug) { print "Inserting activates for project specific boards into $PWD/$ActivationFile at $InsertPos\n"; }
    $Lines = substr($Lines, 0, $InsertPos).
             $ProjectBoardActivations.
             substr($Lines, $InsertPos);
    writeFile($ActivationFile, $Lines);
  }
}

my $PWD2 = `pwd`; chop($PWD2);
my $LastSlashPos = rindex($PWD2, "/");
my $ContainingDir = substr($PWD2, 0, $LastSlashPos + 1);
my $Length = measureTime(10);

print <<"END_OF_INFO"; #{

Project $ProjectName has been created successfully in $ContainingDir after $Length seconds.
You may use it there or move it wherever you want.

Next steps:

1) connect JTAG programmer (e.g. Olimex ARB-USB TinyH) to USB port
2) connect prototype board to JTAG programmer.
3) Copy to your shell:
   cd $PWD2
   ./qtcreator.sh
4) Activate/Deactivate lines in activate_project.sh to configure your project

END_OF_INFO
#}

if (@Errors) { # ERROR + exit 12
  my $PWD3 = `pwd`; chomp($PWD3);
  print "\n\n$PWD3/$0 ".join(" ", @ARGV)."\n";
  foreach my $Error (@Errors) {
    print "    ERROR: $Error\n";
  }
  print "\n\n";
  exit 12;
}

sub readFile {
  my $FileName = shift;
  my @Lines;

  open(IN, "<$FileName") or die("Cannot read file '$FileName' ($!)");
  @Lines = <IN>;  
  close(IN);
  
  return join('', @Lines);
}
sub writeFile {
  my $FileName = shift;
  my $Content  = shift;
  
  open(OUT, ">$FileName") or die("Cannot write to file '$FileName' ($!)");
  print OUT $Content;  
  close(OUT);
}
sub exitOnError {
  my $Error = $_[0] || "";
  
  if ($Error) 
  { 
    while (substr($Error, -1) eq "\n") { $Error = substr($Error, 0, -1); } 
    print STDERR "$0 - ERROR: $Error\n";
    print STDERR "$0 - Project creation has been canceled.\n";
    exit 10; 
  }
}
sub insertIntoString {          # inserts given text into given string before insert mark
  my $FileName   = shift;  # used as reference in error-message 
  my $String     = shift;  # string to insert into
  my $InsertMark = shift;  # will search for position of $InsertMark in $String and insert $Text before
  my $Text       = shift;  # text to insert
  
  my $AlreadyPresent = index($String, $Text) + 1;
  unless ($AlreadyPresent) { # Text not yet included in String: insert it
    my $InsertPos = index($String, $InsertMark);
    if ($InsertPos > -1) {
      substr($String, $InsertPos, 0) = $Text;
    }
    else { # ERROR
      push @Errors, "Cannot insert text into file '$FileName'. Insert a line '$InsertMark' to this file!";
    }
  }
  
  return $String;
}
sub insertIntoFile {            # inserts given text into given file before insert mark
  my $FileName   = shift;  # used as reference in error-message 
  my $InsertMark = shift;  # will search for position of $InsertMark in $String and insert $Text before
  my $Text       = shift;  # text to insert

  if ($Debug) { print "$0 - inserting ".length($Text)." bytes into $FileName before '$InsertMark'\n"; }
  my $Content = readFile($FileName);
  $Content = insertIntoString($FileName, $Content, $InsertMark, $Text);
  writeFile($FileName, $Content);
}
sub strip {                     # removes invisible characters at begin + end of given string
  my $Line = shift || "";

  my %IsWhiteChar = ( ' ' => 1, "\n" => 1, "\r" => 1, "\t" => 1 ); 

  $Line =~ s/^\s+//;
  $Line =~ s/\s+$//;
  #$Line =~ s/(?:^ +)||(?: +$)//g;
  #$Line =~ s/^\ *([A-Z,a-z,0-9]*)\ *$/\1/g;
  #$Line =~ s/^\s+//;
  #$Line  =~ s/\s+$//;
  return $Line;
  
  my $lIndex = 0;
  while ( $IsWhiteChar{ substr($Line, $lIndex, 1) } ) { $lIndex++; }

  my $rIndex = length($Line);
  while ( $IsWhiteChar{ substr($Line, $rIndex, 1) } ) { $rIndex--; }
  
  return substr($Line, $lIndex, $rIndex - $lIndex + 1);

}
sub stringsAlignLeft {          # adds spaces at end of each given string to make all strings equal length
  my @Strings = @_;
  
  my $MaxLength = 0; 
  map { if ($MaxLength < length($_)) { $MaxLength = length($_); } } @Strings;
  
  @Strings = map {
                   while (length($_) < $MaxLength) { $_ .= ' '; }
                   $_;
                 } @Strings;
                 
  return @Strings;
}
sub collectAllFeatures {        # extract and collect all features being checked by all activate scripts
    my $AllFeatures = `grep checkFeature extensions/activate.* | grep -v 'function checkFeature' | grep -v '# checkFeature'`;
    my @AllFeatures = split("\n", $AllFeatures);
    my %AllFeatures;
    map { # collect all features being checked from all activate scripts
      my ($File, $Line) = split(':', $_);          # extensions/activate.450_tcpip_uip.sh:checkFeature "450_ethernet"  "TCP-IP Ethernet Stack"
      $Line = strip($Line);
      my $Pos = index($Line, 'checkFeature');      # checkFeature "450_ethernet"
      my $Feature;
      if ($Pos > -1) { 
        $Feature = strip(substr($Line, $Pos + 12)); 

        my $CommentPos = index($Feature, '#');
        if ($CommentPos > -1) { $Feature = strip(substr($Feature, 0, $CommentPos)); }

        my @Parts = split('"', $Feature);             # "450_ethernet" "TCP-IP Ethernet Stack"
        @Parts = grep { strip($_); } @Parts;          # ('', '450_ethernet', ' ', 'TCP-IP Ethernet Stack')
        
        $Feature = $Parts[0]; # ('450_ethernet', 'TCP-IP Ethernet Stack')
        my $Info = strip($Parts[1]); 
        if ($Feature && $Info) { # obtain info about activate script
          if (index($Feature, '*') == -1) {
            $AllFeatures{$Feature} = strip($Info); 
          } # fully qualified feature
          else { # feature contains wildcard: find all matching makefiles
            my $QualifiedFeatures = `ls extensions/*${Feature}* | grep -v /activate`;
            my @QualifiedFeatures = split("\n", $QualifiedFeatures);
            map { # append all features matching to wildcard
              my @Parts = split(/\./, $_); # extensions/makefile.150_board_extension_radio_1_cc1101
              my $FeatureName = $Parts[1]; 
              if ($FeatureName) {
                if ($Debug) { print "qualified feature: '$_' -> $FeatureName\n"; }
                $Info = strip(`extensions/activate.${FeatureName}.sh INFO`);
                $AllFeatures{$FeatureName} = $Info; 
              }
            } @QualifiedFeatures; 
          }
        }
#        else {
#          if ($Feature) { print "ignoring feature without description: '$Line'\n"; }
#          else { print "Buggy feature: '".join(',', @Parts)."'\n"; }
#        }
      }
      else { $Feature = undef; } # no feature in this line
    } @AllFeatures;

    return %AllFeatures;
}
sub collectAllCpuVariants {     # extract and collect all TTC_CPU_VARIANT_* definitions from ttc_cpu_types.h
  my $AllCpuVariants = `grep '#define TTC_CPU_VARIANT_' $ENV{HOME}/Source/TheToolChain/TTC-Library/ttc_cpu_types.h`;
  my @AllCpuVariants = split("\n", $AllCpuVariants);
  my %AllCpuVariants;
  map {
    my @Parts = split(" ", $_); # '#define TTC_CPU_VARIANT_stm32l100rct6      702'
    $AllCpuVariants{$Parts[1]} = 1;
  } 
  @AllCpuVariants;
  
  return  grep { (substr(strip($_), -6) ne '_start') } # remove special enum start-marker 
          grep { (substr(strip($_), -4) ne '_end')   } # remove special enum end-marker
          sort keys %AllCpuVariants;
}
my %MeasureTimes;
sub measureTime {               # returns time being passed since last call for same handle
  my $Handle = shift; # any unique number or string
  
  my $TimeSecs = time;
  unless (defined($MeasureTimes{$Handle})) { # first call: create structure 
    $MeasureTimes{$Handle} = $TimeSecs;
  }
  
  my $Passed = $TimeSecs - $MeasureTimes{$Handle};
  $MeasureTimes{$Handle} = $TimeSecs;
  
  return $Passed;
}
exit 0;

